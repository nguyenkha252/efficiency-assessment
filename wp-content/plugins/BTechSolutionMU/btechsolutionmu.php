<?php
/*
 * Plugin Name: BTech Solution MU
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://localhost/
 * @since             0.0.1
 * @package           BTechSolutionMU
 *
 * @wordpress-plugin
 * Plugin Name:       BTech Solution MU
 * Plugin URI:        http://localhost/wp-content/plugins/BTechSolutionMU/index.php
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Henry Nguyen
 * Author URI:        http://localhost/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       btech_solution_mu
 * Domain Path:       /languages
 */

# global $wpdb; exit('<pre>' . json_encode(['wpdb' => $wpdb->prefix, 'wpdb->users' => $wpdb->users, __LINE__ => __FILE__, 'credentials' => [$user, $username, $password]], JSON_PRETTY_PRINT) . '</pre>');

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

define('BTECH_SOLUTION_MU_LANG_DOMAIN', 'btech_solution_mu');


define('BTECH_SOLUTION_MU_VERSION', '0.0.1');


define('BTECH_SOLUTION_MU_NAME', 'BTechSolutionMU');
define('BTECH_SOLUTION_MU_LANGUAGE_PATH', __DIR__ . '/languages/');

define('BTECH_SOLUTION_MU_CUSTOM_USER', BTECH_SOLUTION_MU_NAME . '_custom_user_role');

define('BTECH_SOLUTION_MU_MODULE_DIR', __DIR__ );
define('BTECH_SOLUTION_MU_MODULE_URL', plugin_dir_url(__FILE__) );

function get_btech_solution_mu_version() {
    return get_option(BTECH_SOLUTION_MU_NAME .'_ver', '0.0.0');
}
define('BTECH_SOLUTION_MU_VERSION_NAME', sprintf(__('Beta (%s)', BTECH_SOLUTION_MU_LANG_DOMAIN), get_btech_solution_mu_version() ) );

function get_btech_solution_mu_next_version() {
    $current_version = get_btech_solution_mu_version();
    $parts = explode('.', $current_version);
    $parts = array_map('intval', $parts);
    $parts[2] ++;
    if( $parts[2] > 9 ) {
        $parts[2] = 0;
        $parts[1] ++;
    }
    if( $parts[1] > 9 ) {
        $parts[1] = 0;
        $parts[0] ++;
    }
    $next_version = "{$parts[0]}.{$parts[1]}.{$parts[2]}";
    return $next_version;
}

function set_btech_solution_mu_version($ver) {
    return update_option(BTECH_SOLUTION_MU_NAME .'_ver', $ver);
}
/**
 * The code that runs during plugin activation.
 */
function activate_btech_solution_mu() {
    # @TODO
}

/**
 * The code that runs during plugin deactivation.
 */
function deactivate_btech_solution_mu() {
    # @TODO
}

register_activation_hook( __FILE__, 'activate_btech_solution_mu' );
register_deactivation_hook( __FILE__, 'deactivate_btech_solution_mu' );

add_action('plugins_loaded', function() {
    $domain = BTECH_SOLUTION_MU_LANG_DOMAIN;
    $locale = apply_filters( 'theme_locale', is_admin() ? get_user_locale() : get_locale(), $domain );
    $path =  BTECH_SOLUTION_MU_MODULE_DIR . '/languages';
    return load_textdomain( $domain, $path . '/' . $locale . '.mo' );
});

function bts_check_table_exists($table, $force = false) {
    global $wpdb;
    static $results = [];
    if( !isset($results[$table]) || $force ) {
        $results[$table] = $wpdb->get_row( $wpdb->prepare('SHOW TABLES LIKE %s', $table) );
    }
    return !empty( $results[$table] );
}

add_action("after_setup_theme", function() {
    global $wpdb;

    # $blog_id = get_current_blog_id();
    # echo "<p>Blog ID: '{$blog_id}', Users: '{$wpdb->users}', Users Meta: '{$wpdb->usermeta}'</p><br>";
    # switch_to_blog($blog_id);
    # echo "<p>Blog ID: '{$blog_id}', Users: '{$wpdb->users}', Users Meta: '{$wpdb->usermeta}'</p><br>";
    # exit();
} );

add_action("login_init", function() {
    # if( $_SERVER["REQUEST_METHOD"] == 'POST' ) {
    # $wpdb->__users = $wpdb->users;
    # $wpdb->__usermeta = $wpdb->usermeta;
    # $wpdb->users = $wpdb->prefix . 'bte_users';
    # $wpdb->usermeta = $wpdb->prefix . 'bte_usermeta';
    # echo '<pre>'; var_dump([$wpdb->__users, $wpdb->__usermeta, $wpdb->users, $wpdb->usermeta]); echo '</pre>'; exit;
    # }
});


add_filter( 'wp_login_errors', function( $errors, $redirect_to ) {
    # echo '<pre>'; var_dump(func_get_args(), $errors, $redirect_to); exit('</pre>');
    return $errors;
}, 10, 2);

add_filter( 'login_redirect', function( $redirect_to, $requested_redirect_to, $user ) {
    global $wpdb;
    $current_blog_id = get_current_blog_id();
    if( $current_blog_id < 2 ) {
        # $redirect_to = "http://kpi.brainmark.vn/wp-admin/network/sites.php";
        $redirect_to = network_admin_url( 'sites.php' );
    } else {
        $redirect_to = site_url();
    }
    # echo '<pre>'; var_dump(func_get_args(), $current_blog_id, $redirect_to); exit('</pre>');
    return $redirect_to;
}, 10, 3 );

add_action('admin_init', function() {
    include_once __DIR__ . '/admin/init.php';
});
$GLOBALS['_create_blog_tables'] = [];
function init_site_blogs() {
    global $wpdb;
    if ( isset($_REQUEST['action']) && 'add-site' == $_REQUEST['action'] ) {
        #
    } else {
        if( !in_array('users', $wpdb->tables) ) {
            # $wpdb->tables[] = 'users';
            # $wpdb->tables[] = 'usermeta';
        }
    }
}

add_action('init', 'init_site_blogs', 0);


if( !empty($_REQUEST['action']) ) {
    if( !is_dir(__DIR__ . '/logs/') ) {
        @mkdir(__DIR__ . '/logs/', 0777);
    }
    if($_REQUEST['action'] == 'add-site') {
        require_once BTECH_SOLUTION_MU_MODULE_DIR . '/includes/add-site.php';
    } else if($_REQUEST['action'] == 'deleteblog') {
        require_once BTECH_SOLUTION_MU_MODULE_DIR . '/includes/deleteblog.php';
        after_deleteblog();
    }
}

add_action('switch_blog', function($new_blog, $prev_blog_id = 0) {
    require_once BTECH_SOLUTION_MU_MODULE_DIR . '/includes/switch_blog.php';
    after_switch_blog($new_blog, $prev_blog_id);
}, 100, 2);

add_action('validate_password_reset', function($errors, $user) {
    include BTECH_SOLUTION_MU_MODULE_DIR . '/includes/validate_password_reset.php';
}, 10, 2);
