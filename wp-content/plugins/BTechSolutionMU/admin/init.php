<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/1/17
 * Time: 02:16
 */

add_action( 'admin_menu' , 'linkedurl_function' );
function linkedurl_function() {
    global $menu;
    $menu[1][2] = "http://kpi.brainmark.vn/wp-admin/network/sites.php";
}

if ( defined('WP_NETWORK_ADMIN') && WP_NETWORK_ADMIN )
    require(BTECH_SOLUTION_MU_MODULE_DIR . '/admin/menu/network.php');
elseif ( defined('WP_USER_ADMIN') && WP_USER_ADMIN )
    require(BTECH_SOLUTION_MU_MODULE_DIR . '/admin/menu/user.php');
else
    require(BTECH_SOLUTION_MU_MODULE_DIR . '/admin/menu/admin.php');
# admin_init