<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/4/17
 * Time: 23:55
 */
global $menu;


$menu[5] = array(__('Sites', BTECH_SOLUTION_MU_LANG_DOMAIN), 'manage_sites', 'sites.php', '', 'menu-top menu-icon-site', 'menu-site', 'dashicons-admin-multisite');
$submenu['sites.php'][5]  = array( __('All Sites', BTECH_SOLUTION_MU_LANG_DOMAIN), 'manage_sites', 'sites.php' );
$submenu['sites.php'][10]  = array( _x('Add New', 'site', BTECH_SOLUTION_MU_LANG_DOMAIN), 'create_sites', 'site-new.php' );

$menu[10] = array(__('Users', BTECH_SOLUTION_MU_LANG_DOMAIN), 'manage_network_users', 'users.php', '', 'menu-top menu-icon-users', 'menu-users', 'dashicons-admin-users');
$submenu['users.php'][5]  = array( __('All Users', BTECH_SOLUTION_MU_LANG_DOMAIN), 'manage_network_users', 'users.php' );
$submenu['users.php'][10]  = array( _x('Add New', 'user', BTECH_SOLUTION_MU_LANG_DOMAIN), 'create_users', 'user-new.php' );


# $user = wp_get_current_user();
# echo ('<pre>'); var_dump($user->to_array(), $user->has_cap('manage_sites')); exit('</pre>');
