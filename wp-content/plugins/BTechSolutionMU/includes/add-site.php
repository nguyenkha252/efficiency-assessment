<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/3/17
 * Time: 23:22
 */
add_filter('dbdelta_create_queries', function($cqueries) {
    $GLOBALS['_create_blog_tables']['cqueries'] = $cqueries;
    return $cqueries;
});
add_filter('dbdelta_insert_queries', function($iqueries) {
    $GLOBALS['_create_blog_tables']['iqueries'] = $iqueries;
    return $iqueries;
});
add_filter('wp_should_upgrade_global_tables', function($should_upgrade) {
    $GLOBALS['_create_blog_tables']['should_upgrade'] = $should_upgrade;
    # $should_upgrade = true;
    return $should_upgrade;
});
add_filter('dbdelta_queries', function($queries) {
    # array_unshift($queries, $usermeta_table);
    # array_unshift($queries, $users_multi_table);
    $GLOBALS['_create_blog_tables']['queries'] = $queries;
    return $queries;
});
$blog_id = $new_blog;
# add_user_to_blog($blog_id, $user_id, 'administrator');


add_action('pre_network_site_new_created_user', function($email) {
    add_filter('wp_pre_insert_user_data', function($data, $update, $ID) {
        if( !empty($_POST['blog']['domain']) ) {
            $data['user_pass'] = $_POST['blog']['domain'] . "@123";
            $number = 4;
            while(strlen($data['user_pass']) < 8) {
                $data['user_pass'] .= $number;
                $number++;
            }
            if( !is_dir(__DIR__ . '/../logs') ) {
                @mkdir(__DIR__ . '/../logs', 0777);
            }
            @file_put_contents(__DIR__ . '/../logs/new_user.log', var_export($data, true) . "\n\n\n");
            @error_log(var_export($data, true));
            $data['user_pass'] = wp_hash_password( $data['user_pass'] );
        }
        return $data;
    }, 100, 3);
});

add_action('network_site_new_created_user', function($user_id) {
    define('CREATE_NEW_USER', $user_id);
});

add_action('wpmu_new_blog', function($blog_id, $user_id, $domain, $path, $site_id, $meta) {
    global $wpdb;
    if( is_wp_error($blog_id) ) {
        return;
    }
    $charset_collate = $wpdb->get_charset_collate();
    $max_index_length = 191;

    $prefix = $wpdb->get_blog_prefix($blog_id);
    // Multisite users table
    $users_multi_table = "CREATE TABLE `{$prefix}users` (
  ID bigint(20) unsigned NOT NULL auto_increment,
  user_login varchar(60) NOT NULL default '',
  user_pass varchar(255) NOT NULL default '',
  user_nicename varchar(50) NOT NULL default '',
  user_email varchar(100) NOT NULL default '',
  user_url varchar(100) NOT NULL default '',
  user_registered datetime NOT NULL default '0000-00-00 00:00:00',
  user_activation_key varchar(255) NOT NULL default '',
  user_status int(11) NOT NULL default '0',
  display_name varchar(250) NOT NULL default '',
  spam tinyint(2) NOT NULL default '0',
  deleted tinyint(2) NOT NULL default '0',
  PRIMARY KEY  (ID),
  KEY user_login_key (user_login),
  KEY user_nicename (user_nicename),
  KEY user_email (user_email)
) $charset_collate;\n";

    // Usermeta.
    $usermeta_table = "CREATE TABLE `{$prefix}usermeta` (
  umeta_id bigint(20) unsigned NOT NULL auto_increment,
  user_id bigint(20) unsigned NOT NULL default '0',
  meta_key varchar(255) default NULL,
  meta_value longtext,
  PRIMARY KEY  (umeta_id),
  KEY user_id (user_id),
  KEY meta_key (meta_key($max_index_length))
) $charset_collate;\n";

    # $wpdb->hide_errors();
    ob_start();
    $GLOBALS['_create_blog_tables']['users_multi_table'] = $wpdb->query($users_multi_table);
    $GLOBALS['_create_blog_tables']['users_multi_table_error'] = $wpdb->last_error;

    $GLOBALS['_create_blog_tables']['usermeta_table'] = $wpdb->query($usermeta_table);
    $GLOBALS['_create_blog_tables']['usermeta_table_error'] = $wpdb->last_error;

    $user = $wpdb->get_row( $wpdb->prepare("SELECT * FROM $wpdb->users WHERE ID = %s", $user_id ), ARRAY_A );
    $GLOBALS['_create_blog_tables']['get_user_by_id_'.$user_id] = $user;
    $GLOBALS['_create_blog_tables']['get_user_by_id_'.$user_id.'_error'] = $wpdb->last_error;

    if( is_array($user) ) {
        unset($user['ID']);
        $exists = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM `{$prefix}users` WHERE `user_email` LIKE %s", $user['user_email']) );
        $GLOBALS['_create_blog_tables']["user_email_{$user['user_email']}_exists"] = $exists;
        $GLOBALS['_create_blog_tables']["user_email_{$user['user_email']}_exists_sql"] = $wpdb->last_query;
        if( empty($exists) ) {
            $res = $wpdb->insert("{$prefix}users", $user);
            $GLOBALS['_create_blog_tables']['insert_user_by_id_'.$user_id] = $res;
            $GLOBALS['_create_blog_tables']['insert_user_by_id_'.$user_id.'_id'] = $wpdb->insert_id;
            $GLOBALS['_create_blog_tables']['insert_user_by_id_'.$user_id.'_error'] = $wpdb->last_error;

            if( $res ) {
                $metas = $wpdb->get_results( $wpdb->prepare("SELECT * FROM $wpdb->usermeta WHERE `user_id` = %s", $user_id), ARRAY_A );
                $GLOBALS['_create_blog_tables']['get_user_meta_by_id_'.$user_id] = $metas;
                $GLOBALS['_create_blog_tables']['get_user_meta_by_id_'.$user_id.'_error'] = $wpdb->last_error;
                $new_user_id = $wpdb->insert_id;
                if( !empty($metas) ) {
                    foreach ($metas as $row) {
                        $row['user_id'] = $new_user_id;
                        unset($row['umeta_id']);
                        $res = $wpdb->insert("{$prefix}usermeta", $row);

                        $GLOBALS['_create_blog_tables']['insert_usermeta_by_id_'.$user_id] = $res;
                        $GLOBALS['_create_blog_tables']['insert_usermeta_by_id_'.$user_id.'_data'] = $row;
                        $GLOBALS['_create_blog_tables']['insert_usermeta_by_id_'.$user_id.'_id'] = $wpdb->insert_id;
                        $GLOBALS['_create_blog_tables']['insert_usermeta_by_id_'.$user_id.'_error'] = $wpdb->last_error;

                    }
                }
            }
        }
    }
    update_option('permalink_structure', '/%postname%/');

    if( file_exists(__DIR__ . '/sample-data.php') ) {
        include __DIR__ . '/sample-data.php';
    }

    echo '<pre>';
    var_dump($GLOBALS['_create_blog_tables']);
    echo '</pre>';
    $output = ob_get_clean();
    @file_put_contents(__DIR__ . '/../logs/' . "{$user_id}_{$blog_id}_{$domain}_{$site_id}.log", $output);

    # $wpdb->show_errors();

}, 100, 6);


add_filter('wp_mail', function($data) {
    return $data;
});