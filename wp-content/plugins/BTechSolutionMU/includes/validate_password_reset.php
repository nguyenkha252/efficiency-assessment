<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/26/17
 * Time: 18:17
 */

add_action('after_password_reset', function($user, $new_pass) {
    if( !is_wp_error($user) ) {
        /* @var $user WP_User */
        $blog_id = get_current_blog_id();
        if( $blog_id == 1 ) {
            global $wpdb;
            $blogs = $wpdb->get_col($wpdb->prepare(
                "SELECT l.blog_id FROM `{$wpdb->registration_log}` as l ".
                "INNER JOIN `{$wpdb->blogs}` as b ON l.blog_id = b.blog_id ".
                "WHERE (l.`email` LIKE %s) AND (b.deleted = 0) AND (b.spam = 0) AND (b.blog_id != %d)", $user->user_email, $blog_id) );
            if( !empty($blogs) ) {
                foreach( $blogs as $bid) {
                    switch_to_blog($bid);
                    $prefix = $wpdb->get_blog_prefix($bid);
                    $wpdb->users = $prefix . 'users';
                    $wpdb->usermeta = $prefix . 'usermeta';
                    $uid = $wpdb->get_var( $wpdb->prepare("SELECT u.`ID` FROM {$wpdb->users} as u WHERE u.`user_email` LIKE %s", $user->user_email) );
                    if( !empty($uid) ) {
                        wp_set_password( $new_pass, $uid );
                        update_user_option( $uid, 'default_password_nag', false, true );
                    }
                    restore_current_blog();
                }
            }
            switch_to_blog($blog_id);
            $wpdb->users = $wpdb->base_prefix . 'users';
            $wpdb->usermeta = $wpdb->base_prefix . 'usermeta';
        }
    } else {
        # Error
    }
}, 100, 2);

