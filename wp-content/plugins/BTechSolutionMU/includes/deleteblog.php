<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/26/17
 * Time: 18:15
 */

function after_deleteblog() {
    add_action('delete_blog', function($blog_id, $drop) {
        global $wpdb;
        if( $blog_id > 1 ) {
            # $prefix = $wpdb->get_blog_prefix($blog_id);
            $prefix = $wpdb->base_prefix;
            $wpdb->users = $prefix . 'users';
            $wpdb->usermeta = $prefix . 'usermeta';
        }
    }, 100, 2);
    add_filter('wpmu_drop_tables', function($tables, $blog_id) {
        global $wpdb;
        if( $blog_id > 1 ) {
            $prefix = $wpdb->get_blog_prefix($blog_id);
            $tables[] = $prefix . 'users';
            $tables[] = $prefix . 'usermeta';
        }
        return $tables;
    }, 100, 2);
}

