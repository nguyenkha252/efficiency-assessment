<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/26/17
 * Time: 18:12
 */

global $wpdb;
if( is_wp_error($blog_id) ) {
    return;
}
$charset_collate = $wpdb->get_charset_collate();
$max_index_length = 191;

$prefix = $wpdb->get_blog_prefix($blog_id);
// Multisite users table
$users_multi_table = "CREATE TABLE `{$prefix}users` (
  ID bigint(20) unsigned NOT NULL auto_increment,
  user_login varchar(60) NOT NULL default '',
  user_pass varchar(255) NOT NULL default '',
  user_nicename varchar(50) NOT NULL default '',
  user_email varchar(100) NOT NULL default '',
  user_url varchar(100) NOT NULL default '',
  user_registered datetime NOT NULL default '0000-00-00 00:00:00',
  user_activation_key varchar(255) NOT NULL default '',
  user_status int(11) NOT NULL default '0',
  display_name varchar(250) NOT NULL default '',
  spam tinyint(2) NOT NULL default '0',
  deleted tinyint(2) NOT NULL default '0',
  PRIMARY KEY  (ID),
  KEY user_login_key (user_login),
  KEY user_nicename (user_nicename),
  KEY user_email (user_email)
) $charset_collate;\n";

// Usermeta.
$usermeta_table = "CREATE TABLE `{$prefix}usermeta` (
  umeta_id bigint(20) unsigned NOT NULL auto_increment,
  user_id bigint(20) unsigned NOT NULL default '0',
  meta_key varchar(255) default NULL,
  meta_value longtext,
  PRIMARY KEY  (umeta_id),
  KEY user_id (user_id),
  KEY meta_key (meta_key($max_index_length))
) $charset_collate;\n";

# $wpdb->hide_errors();
ob_start();
$GLOBALS['_create_blog_tables']['users_multi_table'] = $wpdb->query($users_multi_table);
$GLOBALS['_create_blog_tables']['users_multi_table_error'] = $wpdb->last_error;

$GLOBALS['_create_blog_tables']['usermeta_table'] = $wpdb->query($usermeta_table);
$GLOBALS['_create_blog_tables']['usermeta_table_error'] = $wpdb->last_error;

$user = $wpdb->get_row( $wpdb->prepare("SELECT * FROM $wpdb->users WHERE ID = %s", $user_id ), ARRAY_A );
$GLOBALS['_create_blog_tables']['get_user_by_id_'.$user_id] = $user;
$GLOBALS['_create_blog_tables']['get_user_by_id_'.$user_id.'_error'] = $wpdb->last_error;

if( is_array($user) ) {
    unset($user['ID']);
    $exists = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM `{$prefix}users` WHERE `user_email` LIKE %s", $user['user_email']) );
    $GLOBALS['_create_blog_tables']["user_email_{$user['user_email']}_exists"] = $exists;
    $GLOBALS['_create_blog_tables']["user_email_{$user['user_email']}_exists_sql"] = $wpdb->last_query;
    if( empty($exists) ) {
        $user['user_pass'] = md5('123456@Ab!.');
        $user['user_activation_key'] = '';
        # $wpdb->query( $wpdb->prepare("UPDATE `bmk_users` SET `user_pass` = MD5('123456@Ab!.'), `user_activation_key` = '' WHERE `bmk_users`.`user_email` LIKE %s", $user['user_email']) );
        $wpdb->query( $wpdb->prepare("UPDATE `bmk_users` SET `user_pass` = %s, `user_activation_key` = '' WHERE `bmk_users`.`user_email` LIKE %s", $user['user_pass'], $user['user_email']) );
        $res = $wpdb->insert("{$prefix}users", $user);
        $GLOBALS['_create_blog_tables']['insert_user_by_id_'.$user_id] = $res;
        $GLOBALS['_create_blog_tables']['insert_user_by_id_'.$user_id.'_id'] = $wpdb->insert_id;
        $GLOBALS['_create_blog_tables']['insert_user_by_id_'.$user_id.'_error'] = $wpdb->last_error;

        if( $res ) {
            $metas = $wpdb->get_results( $wpdb->prepare("SELECT * FROM $wpdb->usermeta WHERE `user_id` = %s", $user_id), ARRAY_A );
            $GLOBALS['_create_blog_tables']['get_user_meta_by_id_'.$user_id] = $metas;
            $GLOBALS['_create_blog_tables']['get_user_meta_by_id_'.$user_id.'_error'] = $wpdb->last_error;
            $new_user_id = $wpdb->insert_id;
            if( !empty($metas) ) {
                foreach ($metas as $row) {
                    $row['user_id'] = $new_user_id;
                    unset($row['umeta_id']);
                    $res = $wpdb->insert("{$prefix}usermeta", $row);

                    $GLOBALS['_create_blog_tables']['insert_usermeta_by_id_'.$user_id] = $res;
                    $GLOBALS['_create_blog_tables']['insert_usermeta_by_id_'.$user_id.'_data'] = $row;
                    $GLOBALS['_create_blog_tables']['insert_usermeta_by_id_'.$user_id.'_id'] = $wpdb->insert_id;
                    $GLOBALS['_create_blog_tables']['insert_usermeta_by_id_'.$user_id.'_error'] = $wpdb->last_error;

                }
            }
        }
    }
}
update_option('permalink_structure', '/%postname%/');


if( file_exists(__DIR__ . '/sample-data.php') ) {
    include __DIR__ . '/sample-data.php';
}

echo '<pre>';
var_dump($GLOBALS['_create_blog_tables']);
echo '</pre>';
$output = ob_get_clean();
if( !is_dir(__DIR__ . '/logs/') ) {
    @mkdir(__DIR__ . '/logs/', 0777);
}
@file_put_contents(__DIR__ . '/logs/' . "{$user_id}_{$blog_id}_{$domain}_{$site_id}.log", $output);

# $wpdb->show_errors();