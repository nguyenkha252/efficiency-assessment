<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/26/17
 * Time: 18:13
 */

function after_switch_blog($new_blog, $prev_blog_id) {
    global $wpdb;
    if ( isset($_REQUEST['action']) && 'add-site' == $_REQUEST['action'] ) {
        add_filter('dbdelta_create_queries', function($cqueries) {
            $GLOBALS['_create_blog_tables']['cqueries'] = $cqueries;
            return $cqueries;
        });
        add_filter('dbdelta_insert_queries', function($iqueries) {
            $GLOBALS['_create_blog_tables']['iqueries'] = $iqueries;
            return $iqueries;
        });
        add_filter('wp_should_upgrade_global_tables', function($should_upgrade) {
            $GLOBALS['_create_blog_tables']['should_upgrade'] = $should_upgrade;
            # $should_upgrade = true;
            return $should_upgrade;
        });
        add_filter('dbdelta_queries', function($queries) {
            # array_unshift($queries, $usermeta_table);
            # array_unshift($queries, $users_multi_table);
            $GLOBALS['_create_blog_tables']['queries'] = $queries;
            return $queries;
        });
        $blog_id = $new_blog;
        # add_user_to_blog($blog_id, $user_id, 'administrator');
        add_action('wpmu_new_blog', function($blog_id, $user_id, $domain, $path, $site_id, $meta) {
            include_once BTECH_SOLUTION_MU_MODULE_DIR . '/includes/wpmu_new_blog.php';
        }, 100, 6);
    } else {
        if( $new_blog < 2 ) {
            $prefix = $wpdb->base_prefix;
        } else {
            $prefix = $wpdb->get_blog_prefix($new_blog);
        }
        $wpdb->org_chart = "{$prefix}org_chart";
        $wpdb->users = "{$prefix}users";
        $wpdb->usermeta = "{$prefix}usermeta";

        # echo '<pre>'; var_dump(__LINE__, __FILE__, $new_blog, $prev_blog_id, $prefix, $wpdb->org_chart, $wpdb->users, $wpdb->usermeta); exit;
    }
}

