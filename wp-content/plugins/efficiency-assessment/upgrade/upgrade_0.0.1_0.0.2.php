<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 9/26/18
 * Time: 11:19 PM
 */

$pages_data = [
    'profile' => [
        'post_title' =>         __('Profile', EFFA_LANG_DOMAIN),
        'post_name' =>          PAGE_PROFILE_PATH,
        'post_content' =>       __('Profile', EFFA_LANG_DOMAIN),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-profile.php"
    ],
    'dashboard' => [
        'post_title' =>         __('Dashboard', EFFA_LANG_DOMAIN),
        'post_name' =>          PAGE_DASHBOARD,
        'post_content' =>       __('Dashboard', EFFA_LANG_DOMAIN),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-dashboard.php"
    ],
    'organizational-structure' => [
        'post_title' =>         __('Organizational Structure', EFFA_LANG_DOMAIN),
        'post_name' =>          PAGE_ORGANIZATIONAL_STRUCTURE,
        'post_content' =>       __('Organizational Structure', EFFA_LANG_DOMAIN),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-organizational-structure.php"
    ],
    'manage-members' => [
        'post_title' =>         __('Manage Members', EFFA_LANG_DOMAIN),
        'post_name' =>          PAGE_MANAGE_MEMBERS,
        'post_content' =>       __('Manage Members', EFFA_LANG_DOMAIN),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-manage-members.php"
    ],
    'efficiency-assessment' => [
        'post_title' =>         __('Efficiency Assessment', EFFA_LANG_DOMAIN),
        'post_name' =>          PAGE_EFFA_SYSTEM,
        'post_content' =>       __('Efficiency Assessment', EFFA_LANG_DOMAIN),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-efficiency-assessment.php"
    ],
    'news' => [
        'post_title' =>         __('News', EFFA_LANG_DOMAIN),
        'post_name' =>          PAGE_NEWS,
        'post_content' =>       __('News', EFFA_LANG_DOMAIN),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-news.php"
    ],
    'settings' => [
        'post_title' =>         __('Settings', EFFA_LANG_DOMAIN),
        'post_name' =>          PAGE_SETTINGS,
        'post_content' =>       __('Settings', EFFA_LANG_DOMAIN),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-settings.php"
    ],
    'exports' => [
        'post_title' =>         __('Exports', EFFA_LANG_DOMAIN),
        'post_name' =>          PAGE_EXPORTS,
        'post_content' =>       __('Exports', EFFA_LANG_DOMAIN),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-exports.php"
    ],
];

foreach($pages_data as $type => $attr) {
    $page = get_page_by_path($attr['post_name'], OBJECT, 'page');
    if( $page === null ) {
        $attr['ID'] = wp_insert_post($attr);

    } else if( $page instanceof WP_Post ) {
        $attr['ID'] = $page->ID;
        wp_update_post($attr);
    }
    if( $type == PAGE_EFFA_SYSTEM && is_numeric($attr['ID']) ) {
        update_option('show_on_front', 'page');
        update_option('page_on_front', $attr['ID']);
    }
}

set_effa_version(EFFA_VERSION);