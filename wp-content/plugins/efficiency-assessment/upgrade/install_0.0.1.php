<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 9/26/18
 * Time: 10:47 PM
 */

global $wpdb;
$blog_id = $blog_id ? $blog_id : get_current_blog_id();
$prefix = $wpdb->get_blog_prefix($blog_id);

$wpdb->query("START TRANSACTION;");

$sql = "CREATE TABLE IF NOT EXISTS `{$prefix}login_logs`(
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT(20) UNSIGNED NOT NULL,
  `action` ENUM('loggedin', 'logout', 'expired') CHARACTER SET utf8 NOT NULL,
  `at_time` datetime NOT NULL,
  `browser` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";


$wpdb->query($sql);
if($wpdb->last_error != '') {
    $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
    $wpdb->query("rollback;");
    return $error;
}
$wpdb->query("COMMIT;");

$wpdb->query("START TRANSACTION;");
$wpdb->query("CREATE TABLE IF NOT EXISTS `{$prefix}org_charts`(
      `id` bigint(20) NOT NULL AUTO_INCREMENT,
      `name` varchar(64) CHARACTER SET utf8 NOT NULL,
      `room` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
      `parent` bigint(20) DEFAULT NULL,
      `alias` enum('tapdoan','congty','phongban','nhanvien') DEFAULT NULL,
      `level` int(10) UNSIGNED DEFAULT 0,
      PRIMARY KEY (`id`),
      UNIQUE KEY `unique_row` (`name`,`parent`),
      KEY `name` (`name`) USING BTREE,
      KEY `room` (`room`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

if($wpdb->last_error != '') {
    $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
    $wpdb->query("rollback;");
    return $error;
}else{
    $id = $wpdb->get_var("SELECT `id` FROM `{$prefix}org_charts` WHERE `id` = 1");
    if( !$id ) {
        $wpdb->query("INSERT INTO `{$prefix}org_charts`(`id`, `name`, `room`, `parent`, `alias`, `level`) VALUES ('1', 'Root', 'Công Ty', '0', 'congty', '0');");
        if($wpdb->last_error != '') {
            $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
            $wpdb->query("rollback;");
            return $error;
        }
    }
}

$wpdb->query("START TRANSACTION;");
$sql = "
    CREATE  TABLE IF NOT EXISTS `{$prefix}assign_orgcharts` ( 
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT , 
    `post_id` BIGINT(20) NOT NULL COMMENT 'ID of table posts' , 
    `orgchart_id` BIGINT(20) NOT NULL COMMENT 'id of table org_charts' , 
    `year` INT NOT NULL , 
    `type` ENUM('group','item') NOT NULL DEFAULT 'item',
    `percent` DOUBLE NOT NULL DEFAULT '0' COMMENT 'Trọng số',
    `assign_status` ENUM('publish','trash') NOT NULL DEFAULT 'publish' COMMENT 'Status for posts',
    `created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' , 
    PRIMARY KEY (`id`), 
    UNIQUE `UNIQUE` (`post_id`, `orgchart_id`, `year`)) 
    ENGINE = InnoDB DEFAULT CHARSET=utf8;
";
$wpdb->query($sql);
if($wpdb->last_error != '') {
    $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
    $wpdb->query("rollback;");
    return $error;
}
$wpdb->query("COMMIT;");

$wpdb->query("START TRANSACTION;");
$sql = "
  CREATE TABLE IF NOT EXISTS `{$prefix}efficiency_assessment` ( 
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT , 
    `assign_id` BIGINT(20) NOT NULL COMMENT 'id of table assign_orgcharts' , 
    `user_id` BIGINT(20) NOT NULL DEFAULT '0' COMMENT 'ID of table users' , 
    `important` ENUM('1','2','3') NOT NULL DEFAULT '1' COMMENT 'Mức độ quan trọng' , 
    `standard` ENUM('1','2','3','4','5') NOT NULL DEFAULT '1' COMMENT 'Tiêu chuẩn' , 
    `manager_lv1` DOUBLE NOT NULL DEFAULT '0' COMMENT 'Quản lý đánh giá' , 
    `manager_lv0` DOUBLE NOT NULL DEFAULT '0' COMMENT 'Hội đồng đánh giá' ,
    `status` ENUM('publish','trash') NOT NULL DEFAULT 'publish' COMMENT 'Status for posts', 
    `created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' , 
    PRIMARY KEY (`id`), 
    UNIQUE `UNIQUE` (`assign_id`, `user_id`)) 
    ENGINE = InnoDB DEFAULT CHARSET=utf8;
    ";
$wpdb->query($sql);
if($wpdb->last_error != '') {
    $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
    $wpdb->query("rollback;");
    return $error;
}
$wpdb->query("COMMIT;");

/**
 * Table assign user implement quiz
 */
$wpdb->query("START TRANSACTION;");
$sql = "
  CREATE TABLE IF NOT EXISTS `{$prefix}assign_level_up` ( 
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,  
    `assign_author` BIGINT(20) NOT NULL DEFAULT '0' COMMENT 'Author assign level up' , 
    `user_id` BIGINT(20) NOT NULL DEFAULT '0' COMMENT 'ID of table users' , 
    `orgchart_id_old` BIGINT(20) NOT NULL DEFAULT '0' COMMENT 'ID of table orgcharts' ,
    `orgchart_id_new` BIGINT(20) NOT NULL DEFAULT '0' COMMENT 'ID of table orgcharts' ,
    `status` ENUM('publish', 'finish-quiz', 'failed', 'success', 'trash') NOT NULL DEFAULT 'publish' COMMENT 'Status after implement quiz', 
    `created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' , 
    PRIMARY KEY (`id`))
    ENGINE = InnoDB DEFAULT CHARSET=utf8;
    ";
$wpdb->query($sql);
if($wpdb->last_error != '') {
    $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
    $wpdb->query("rollback;");
    return $error;
}
$wpdb->query("COMMIT;");

/**
 * Table compare level up
*/
$wpdb->query("START TRANSACTION;");
$sql = "
  CREATE TABLE IF NOT EXISTS `{$prefix}efficiency_level_up` ( 
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT , 
    `assign_id` BIGINT(20) NOT NULL COMMENT 'id of table assign_orgcharts' ,
    `assign_level_up` BIGINT(20) NOT NULL COMMENT 'id of table assign_level_up' ,
    `important` ENUM('1','2','3') NOT NULL DEFAULT '1' COMMENT 'Mức độ quan trọng' , 
    `standard` ENUM('1','2','3','4','5') NOT NULL DEFAULT '1' COMMENT 'Tiêu chuẩn' ,  
    `manager_lv0` DOUBLE NOT NULL DEFAULT '0' COMMENT 'Hội đồng đánh giá' ,   
    `created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' , 
    PRIMARY KEY (`id`), 
    UNIQUE `UNIQUE` (`assign_id`, `assign_level_up`))
    ENGINE = InnoDB DEFAULT CHARSET=utf8;
    ";
$wpdb->query($sql);
if($wpdb->last_error != '') {
    $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
    $wpdb->query("rollback;");
    return $error;
}
$wpdb->query("COMMIT;");

$wpdb->query("START TRANSACTION;");
$sql = "
    CREATE TABLE `{$prefix}comments_efficiency` ( 
    `id` BIGINT NOT NULL AUTO_INCREMENT , 
    `comment_content` VARCHAR(255) NOT NULL , 
    `comment_author` BIGINT NOT NULL , 
    `user_id` BIGINT NOT NULL DEFAULT '0' , 
    `parent` BIGINT NOT NULL DEFAULT '0' , 
    `comment_type` VARCHAR(50) NOT NULL DEFAULT 'assign_efficiency'
    `comment_year` INT NOT NULL , 
    `comment_status` ENUM('publish','trash') NOT NULL DEFAULT 'publish' ,
    `created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' ,
    PRIMARY KEY (`id`)) 
    ENGINE = InnoDB DEFAULT CHARSET=utf8;
";
$wpdb->query($sql);
if($wpdb->last_error != '') {
    $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
    $wpdb->query("rollback;");
    return $error;
}
$wpdb->query("COMMIT;");

$wpdb->query("START TRANSACTION;");
#add post_code to table posts
$row = $wpdb->get_row("SHOW COLUMNS FROM `{$prefix}posts` WHERE `Field` LIKE 'post_code'");
if( empty( $row ) ) {
    $wpdb->query("ALTER TABLE `{$prefix}posts` ADD `post_code` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `comment_count`;");
}
if($wpdb->last_error != '') {
$error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
$wpdb->query("rollback;");
return $error;
}else{
    $wpdb->query("COMMIT;");
}
$wpdb->query("START TRANSACTION;");

#add post_year to table posts
$row = $wpdb->get_row("SHOW COLUMNS FROM `{$prefix}posts` WHERE `Field` LIKE 'post_year'");
if( empty( $row ) ) {
    $wpdb->query("ALTER TABLE `{$prefix}posts` ADD `post_year` INT NULL DEFAULT '0' AFTER `comment_count`;");
}
if($wpdb->last_error != '') {
$error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
$wpdb->query("rollback;");
return $error;
}else{
    $wpdb->query("COMMIT;");
}

$wpdb->query("START TRANSACTION;");
$row = $wpdb->get_row("SHOW COLUMNS FROM `{$prefix}users` WHERE `Field` LIKE 'role_id'");
if( empty( $row ) ) {
    $wpdb->query("ALTER TABLE `{$prefix}users` ADD `role_id` BIGINT UNSIGNED NULL AFTER `orgchart_id`;");
}

$row = $wpdb->get_row("SHOW COLUMNS FROM `{$prefix}users` WHERE `Field` LIKE 'role_id'");
if( empty( $row ) ) {
    $wpdb->query("ALTER TABLE `{$prefix}users` ADD `role_id` BIGINT UNSIGNED NULL AFTER `orgchart_id`;");
}

$row = $wpdb->get_row("SHOW COLUMNS FROM `{$prefix}users` WHERE `Field` LIKE 'spam'");
if( empty( $row ) ) {
    $wpdb->query("ALTER TABLE `{$prefix}users` ADD `spam` tinyint(2) NOT NULL DEFAULT '0';");
}

$row = $wpdb->get_row("SHOW COLUMNS FROM `{$prefix}users` WHERE `Field` LIKE 'deleted'");
if( empty( $row ) ) {
    $wpdb->query("ALTER TABLE `{$prefix}users` ADD `deleted` tinyint(2) NOT NULL DEFAULT '0';");
}

if($wpdb->last_error != '') {
    $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
    $wpdb->query("rollback;");
    return $error;
}else{
    $wpdb->query( "UPDATE `{$prefix}users` SET `orgchart_id` = 1 WHERE `ID` = 1;" );
    if($wpdb->last_error != '') {
        $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
        $wpdb->query("rollback;");
        return $error;
    }
}
$wpdb->query("COMMIT;");

//Add column org_chart_id in table posts
$wpdb->query("START TRANSACTION;");

$row = $wpdb->get_row("SHOW COLUMNS FROM `{$prefix}users` WHERE `Field` LIKE 'chart_id'");
if( empty( $row ) ) {
    $wpdb->query("ALTER TABLE `{$prefix}posts` 
          ADD `chart_id` BIGINT NULL AFTER `ID`;
          ");
    if($wpdb->last_error != '') {
        $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
        $wpdb->query("rollback;");
        return $error;
    }
}
$wpdb->query("COMMIT;");


set_effa_version(EFFA_VERSION);
