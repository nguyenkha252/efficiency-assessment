<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 9/26/18
 * Time: 11:19 PM
 */
global $wpdb;
$blog_id = $blog_id ? $blog_id : get_current_blog_id();
$prefix = $wpdb->get_blog_prefix($blog_id);
$wpdb->query("START TRANSACTION;");

#add post_year to table posts
$row = $wpdb->get_row("SHOW COLUMNS FROM `{$prefix}posts` WHERE `Field` LIKE 'department'");
if( empty( $row ) ) {
    $wpdb->query("ALTER TABLE `{$prefix}posts` ADD `department` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `comment_count`;");
}
if($wpdb->last_error != '') {
    $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
    $wpdb->query("rollback;");
    return $error;
}else{
    $wpdb->query("COMMIT;");
}
set_effa_version(EFFA_VERSION);