<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 8/27/18
 * Time: 10:34 AM
 */

/**
 * Retrieves a post given its path.
 *
 * @since 2.1.0
 *
 * @global wpdb $wpdb WordPress database abstraction object.
 *
 * @param string       $post_path post path.
 * @param string       $output    Optional. The required return type. One of OBJECT, ARRAY_A, or ARRAY_N, which correspond to
 *                                a WP_Post object, an associative array, or a numeric array, respectively. Default OBJECT.
 * @param string|array $post_type Optional. Post type or array of post types. Default 'post'.
 * @return WP_Post|array|null WP_Post (or array) on success, or null on failure.
 */
function get_post_by_path( $post_path, $output = OBJECT, $post_type = 'post' ) {
    global $wpdb;

    $last_changed = wp_cache_get_last_changed( 'posts' );

    $hash = md5( $post_path . serialize( $post_type ) );
    $cache_key = "get_page_by_path:$hash:$last_changed";
    $cached = wp_cache_get( $cache_key, 'posts' );
    if ( false !== $cached ) {
        // Special case: '0' is a bad `$page_path`.
        if ( '0' === $cached || 0 === $cached ) {
            return;
        } else {
            return get_post( $cached, $output );
        }
    }

    $post_path = rawurlencode(urldecode($post_path));
    $post_path = str_replace('%2F', '/', $post_path);
    $post_path = str_replace('%20', ' ', $post_path);
    $parts = explode( '/', trim( $post_path, '/' ) );
    $parts = array_map( 'sanitize_title_for_query', $parts );
    $escaped_parts = esc_sql( $parts );

    $in_string = "'" . implode( "','", $escaped_parts ) . "'";

    if ( is_array( $post_type ) ) {
        $post_types = $post_type;
    } else {
        $post_types = array( $post_type, 'attachment' );
    }

    $post_types = esc_sql( $post_types );
    $post_type_in_string = "'" . implode( "','", $post_types ) . "'";
    $sql = "
		SELECT ID, post_name, post_parent, post_type
		FROM $wpdb->posts
		WHERE post_name IN ($in_string)
		AND post_type IN ($post_type_in_string)
	";

    $posts = $wpdb->get_results( $sql, OBJECT_K );

    $revparts = array_reverse( $parts );

    $foundid = 0;
    foreach ( (array) $posts as $post ) {
        if ( $post->post_name == $revparts[0] ) {
            $count = 0;
            $p = $post;

            /*
             * Loop through the given path parts from right to left,
             * ensuring each matches the post ancestry.
             */
            while ( $p->post_parent != 0 && isset( $posts[ $p->post_parent ] ) ) {
                $count++;
                $parent = $posts[ $p->post_parent ];
                if ( ! isset( $revparts[ $count ] ) || $parent->post_name != $revparts[ $count ] )
                    break;
                $p = $parent;
            }

            $foundid = $post->ID;
            if ( $post->post_type == $post_type )
                break;
        }
    }

    // We cache misses as well as hits.
    wp_cache_set( $cache_key, $foundid, 'posts' );

    if ( $foundid ) {
        return get_post( $foundid, $output );
    }
}

function effa_register_post_type_by_item($item){
    $labels = array(
        "name" => __($item['items'], EFFA_LANG_DOMAIN),
        "singular_name" => __($item['items'], EFFA_LANG_DOMAIN),
        "menu_name" => __($item['items'], EFFA_LANG_DOMAIN),
        "all_items" => __("All " . $item['items'], EFFA_LANG_DOMAIN),
        "add_new" => __("New " . $item['item'], EFFA_LANG_DOMAIN),
        "add_new_item" => __($item['item'] . " Name", EFFA_LANG_DOMAIN),
        "edit_item" => __("Edit " . $item['item'], EFFA_LANG_DOMAIN),
        "new_item" => __("New " . $item['item'], EFFA_LANG_DOMAIN),
        "view_item" => __("View " . $item['item'], EFFA_LANG_DOMAIN),
        "view_items" => __("View " . $item['items'], EFFA_LANG_DOMAIN),
        "search_items" => __("Search " . $item['item'], EFFA_LANG_DOMAIN),
        "not_found" => __($item['item'] . " Not Found", EFFA_LANG_DOMAIN),
        "not_found_in_trash" => __($item['item'] . " Not Found in Trash", EFFA_LANG_DOMAIN),
    );

    $args = array(
        "label" => __($item['items'], EFFA_LANG_DOMAIN),
        "labels" => $labels,
        "description" => __($item['item'] . " Items List", EFFA_LANG_DOMAIN),
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => true,
        "show_in_nav_menus" => true,
        "show_admin_column" => false,
        "rest_base" => $item['slug'],
        "has_archive" => true,
        "show_in_menu" => false,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "show_in_quick_edit" => true,
        "has_multiple" => false,
        "rewrite" => array(
            "slug" => $item['slug'],
            "with_front" => true,
            'feed' => true,
            'pages' => true,
        ),
        "query_var" => $item['slug'],
        "menu_position" => $item['menu_position'],
        "supports" => $item['supports'],

    );

    register_post_type($item['post_type'], $args);
}
add_action('init', 'effa_register_post_type');
function effa_register_post_type(){
    $args_post_type = [
        'efficiency_assessment' => [
            'item' => __('Efficiency Assessment', EFFA_LANG_DOMAIN),
            'items' => __('Efficiency Assessment', EFFA_LANG_DOMAIN),
            'slug' => 'efficiency-assessment',
            'menu_position' => 6,
            'post_type' => EFFA_POST_TYPE_EFFA,
            'supports' => ["title", /*"editor", "revisions",*/ "page-attributes"],
        ],
    ];
    foreach ( $args_post_type as $key => $item ){
        effa_register_post_type_by_item( $item );
    }
    flush_rewrite_rules(true);
}

if( defined('DOING_AJAX') && DOING_AJAX ) {
    if( !function_exists('wp_ajax_callback') ){
        function wp_ajax_callback($prefix, $params) {
            $filename = EFFA_MODULE_DIR . "/ajax/{$prefix}{$_REQUEST['action']}.php";
            if( file_exists($filename) ) {
                require_once $filename;
            }

            $callback = "wp_ajax_{$prefix}".str_replace( ['-', ':'], ['_', '_'], $_REQUEST['action'] );

            if( function_exists($callback) ) {
                $callback($params);
                exit();
            }
        };
    }
    add_action('init', function() {
        if (!empty($_REQUEST['action'])) {
            $is_logged_in = is_user_logged_in();

            // Get gallery at proejct detail front - end
            $prefix = $is_logged_in ? 'wp_ajax_' : 'wp_ajax_nopriv_';
            // Register core Ajax calls.
            if ($_SERVER["REQUEST_METHOD"] == 'GET') {
                add_action($prefix . $_REQUEST['action'], function () {
                    return wp_ajax_callback('get_', $_GET);
                }, 2);
            } else if ($_SERVER["REQUEST_METHOD"] == 'POST') {
                add_action($prefix . $_REQUEST['action'], function () {
                    return wp_ajax_callback('post_', $_POST);
                }, 2);
            }
        }
    });
}