<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/2/18
 * Time: 10:32 AM
 */

defined( 'ABSPATH' ) || exit;

/**
 * Template loader class.
 */
class EFFA_Template_Loader {
    /**
     * A Unique Identifier
     */
    protected $plugin_slug;

    /**
     * A reference to an instance of this class.
     */
    private static $instance;

    /**
     * The array of templates that this plugin tracks.
     */
    protected $templates;

    /**
     * Returns an instance of this class.
     */
    public static function get_instance() {

        if( null == self::$instance ) {
            self::$instance = new EFFA_Template_Loader();
        }

        return self::$instance;

    }

    public function init() {
        $this->templates = array();
        add_filter( 'template_include', array( __CLASS__, 'template_loader' ) );
    }

    public function template_loader( $template ) {
        // Get global post
        global $post;

        // Return template if post is empty
        if ( ! $post ) {
            return $template;
        }
        $search_files = get_post_meta( $post->ID, '_wp_page_template', true );
        $template     = locate_template( $search_files );
        if( !$template ){
            $template = EFFA_MODULE_DIR . "/templates/" . $search_files;
        }
        if( file_exists( $template ) ){
            return $template;
        }
        return $template;
    }
}