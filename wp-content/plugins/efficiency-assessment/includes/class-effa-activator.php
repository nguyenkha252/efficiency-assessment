<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 9/26/18
 * Time: 10:45 PM
 */
class Effa_Activator {

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function activate() {
        $install_file = EFFA_MODULE_DIR . '/upgrade/install_0.0.1.php';
        if( file_exists($install_file) ) {
            require_once $install_file;
        }
    }

    public static function upgrade() {
        $current_version = get_effa_version();
        $next_version = get_effa_next_version();
        $upgrade_file = EFFA_MODULE_DIR . "/upgrade/upgrade_{$current_version}_{$next_version}.php";

        if( file_exists($upgrade_file) ) {
            require_once $upgrade_file;
        }

        return $next_version;
    }
}