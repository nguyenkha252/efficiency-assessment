<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 8/27/18
 * Time: 10:10 AM
 */

#add_action('admin_menu', 'effa_menu_item');
function effa_menu_item() {
    global $effa_settings_page_hook, $wpdb;
    $site_settings_title = __('Site Settings', EFFA_LANG_DOMAIN);
    $effa_settings_page_hook = add_menu_page(
        $site_settings_title,
        $site_settings_title,
        'manage_options',
        'effa_settings',
        'effa_render_settings_page'
    );

    #add_submenu_page('effa_settings', $site_settings_title, $site_settings_title, 'manage_options', 'effa_config', 'effa_render_settings_page');
    #add_submenu_page('effa_config', __('Site Settings', EFFA_LANG_DOMAIN),  __('Site Settings', EFFA_LANG_DOMAIN), 'manage_options', 'effa_settings', [$this, 'effa_render_settings_page']);
    include __DIR__ . '/views/options-panel.php';
}
