<?php
/*
The settings page
*/

function effa_scripts_styles($hook)
{
	global $effa_settings_page_hook;
	if ($effa_settings_page_hook != $hook)
		return;
    wp_enqueue_style( EFFA_NAME . 'options_panel_stylesheet' . '-options-panel', EFFA_MODULE_URL . '/admin/assets/css/options-panel.css', array(), EFFA_VERSION, 'all' );
    wp_enqueue_script( EFFA_NAME . '_options_panel_script', EFFA_MODULE_URL . '/admin/assets/js/options-panel.js', array( 'jquery' ), EFFA_VERSION, true );
	wp_enqueue_script('common');
	wp_enqueue_script('wp-lists');
	wp_enqueue_script('postbox');
}

add_action('admin_enqueue_scripts', 'effa_scripts_styles');

function effa_render_settings_page()
{
	?>
	<div class="wrap">
		<div id="icon-options-general" class="icon32"></div>
		<h2><?php _e('Site settings', EFFA_LANG_DOMAIN); ?></h2>
		<?php settings_errors(); ?>
		<div class="clearfix paddingtop20">
			<div class="first twelvecol">
				<form method="post" action="options.php">
					<?php settings_fields('effa_settings'); ?>
					<?php do_meta_boxes('effa_metaboxes', 'advanced', null); ?>
					<?php wp_nonce_field('closedpostboxes', 'closedpostboxesnonce', false); ?>
					<?php wp_nonce_field('meta-box-order', 'meta-box-order-nonce', false); ?>
				</form>
			</div>
		</div>
	</div>
<?php }

function effa_create_options()
{
	add_settings_section('effa_configuration_section', null, null, 'effa_settings');
	#add_settings_section('effa_role_section', null, null, 'effa_settings');
	#add_settings_section('effa_misc_section', null, null, 'effa_settings');

	add_settings_field(
		'site_phone_number_marketing', '', 'effa_render_settings_field', 'effa_settings', 'effa_configuration_section',
		array(
			'title' => __('Phone number marketing', EFFA_LANG_DOMAIN),
			#'desc'  => __('Phone number', EFFA_LANG_DOMAIN),
			'id'    => 'site_phone_number_marketing',
			'type'  => 'text',
			'items' => array(
				'site_phone_number_marketing' => __('Phone number marketing', EFFA_LANG_DOMAIN),
				#'max_words_title' => __('Maximum', EFFA_LANG_DOMAIN)
			),
			'group' => 'effa_configuration_section'
		)
    );
    add_settings_field(
        'site_phone_number_services', '', 'effa_render_settings_field', 'effa_settings', 'effa_configuration_section',
        array(
            'title' => __('Phone number services', EFFA_LANG_DOMAIN),
            #'desc'  => __('Phone number', EFFA_LANG_DOMAIN),
            'id'    => 'site_phone_number_services',
            'type'  => 'text',
            'items' => array(
                'site_phone_number_services' => __('Phone number services', EFFA_LANG_DOMAIN),
                #'max_words_title' => __('Maximum', EFFA_LANG_DOMAIN)
            ),
            'group' => 'effa_configuration_section'
        )
    );
    add_settings_field(
        'site_google_analytics', '', 'effa_render_settings_field', 'effa_settings', 'effa_configuration_section',
            array(
            'title' => __('Google Analytics', EFFA_LANG_DOMAIN),
            #'desc'  => __('Phone number', EFFA_LANG_DOMAIN),
            'id'    => 'site_google_analytics',
            'type'  => 'textarea',
            'items' => array(
                'site_google_analytics' => __('Google Analytics', EFFA_LANG_DOMAIN),
                #'max_words_title' => __('Maximum', EFFA_LANG_DOMAIN)
            ),
            'group' => 'effa_configuration_section'
        )
	);
    add_settings_field(
        'site_banner_news', '', 'effa_render_settings_field', 'effa_settings', 'effa_configuration_section',
        array(
            'title' => __('Banner News', EFFA_LANG_DOMAIN),
            #'desc'  => __('Phone number', EFFA_LANG_DOMAIN),
            'id'    => 'site_banner_news',
            'type'  => 'text',
            'items' => array(
                'site_banner_news' => __('Banner News', EFFA_LANG_DOMAIN),
                #'max_words_title' => __('Maximum', EFFA_LANG_DOMAIN)
            ),
            'group' => 'effa_configuration_section'
        )
    );
    add_settings_field(
        'site_link_test_drive', '', 'effa_render_settings_field', 'effa_settings', 'effa_configuration_section',
        array(
            'title' => __('Link test drive', EFFA_LANG_DOMAIN),
            #'desc'  => __('Phone number', EFFA_LANG_DOMAIN),
            'id'    => 'site_link_test_drive',
            'type'  => 'text',
            'items' => array(
                'site_link_test_drive' => __('Link test drive', EFFA_LANG_DOMAIN),
                #'max_words_title' => __('Maximum', EFFA_LANG_DOMAIN)
            ),
            'group' => 'effa_configuration_section'
        )
    );
	// Finally, we register the fields with WordPress
	register_setting('effa_settings', 'effa_configuration_section', 'effa_settings_validation');
	#register_setting('effa_settings', 'effa_role_settings', 'effa_settings_validation');
	#register_setting('effa_settings', 'effa_misc', 'effa_settings_validation');

} // end sandbox_initialize_theme_options 
add_action('admin_init', 'effa_create_options');

function effa_settings_validation($input)
{
	return $input;
}

function effa_add_meta_boxes()
{
	add_meta_box("effa_post_restrictions_metabox", __('Site Configuration', EFFA_LANG_DOMAIN), "effa_metaboxes_callback", "effa_metaboxes", 'advanced', 'default', array('settings_section' => 'effa_configuration_section'));
	#add_meta_box("effa_role_settings_metabox", __('Role Settings', EFFA_LANG_DOMAIN), "effa_metaboxes_callback", "effa_metaboxes", 'advanced', 'default', array('settings_section' => 'effa_role_section'));
	#add_meta_box("effa_misc_metabox", __('Misc Settings', EFFA_LANG_DOMAIN), "effa_metaboxes_callback", "effa_metaboxes", 'advanced', 'default', array('settings_section' => 'effa_misc_section'));
}

add_action('admin_init', 'effa_add_meta_boxes');

function effa_metaboxes_callback($post, $args)
{
	do_settings_fields("effa_settings", $args['args']['settings_section']);
	submit_button(__('Save Changes', EFFA_LANG_DOMAIN), 'secondary');
}

function effa_render_settings_field($args)
{
	$option_value = get_option($args['group']);
	?>
	<div class="row clearfix">
		<div class="col options-panel">
			<?php if ($args['type'] == 'text'): ?>
                <div class="col lbl-title"><?php echo $args['title']; ?></div>
				<div class="lbl-content">
                    <input type="text" id="<?php echo $args['id'] ?>"
                           name="<?php echo $args['group'] . '[' . $args['id'] . ']'; ?>"
                           value="<?php echo (isset($option_value[ $args['id'] ])) ? esc_attr($option_value[ $args['id'] ]) : ''; ?>">
                </div>
			<?php elseif ($args['type'] == 'select'): ?>
                <div class="col colone lbl-title"><?php echo $args['title']; ?></div>
                <div class="lbl-content">
                    <select name="<?php echo $args['group'] . '[' . $args['id'] . ']'; ?>" id="<?php echo $args['id']; ?>">
                        <?php foreach ($args['options'] as $key => $option) { ?>
                            <option <?php if (isset($option_value[ $args['id'] ])) selected($option_value[ $args['id'] ], $key);
                            echo 'value="' . $key . '"'; ?>><?php echo $option; ?></option><?php } ?>
                    </select>
                </div>
			<?php elseif ($args['type'] == 'checkbox'): ?>
				<input type="hidden" name="<?php echo $args['group'] . '[' . $args['id'] . ']'; ?>" value="0"/>
				<input type="checkbox" name="<?php echo $args['group'] . '[' . $args['id'] . ']'; ?>"
					   id="<?php echo $args['id']; ?>"
					   value="true" <?php if (isset($option_value[ $args['id'] ])) checked($option_value[ $args['id'] ], 'true'); ?> />
                <label class="col colone"><?php echo $args['title']; ?></label>
			<?php elseif ($args['type'] == 'textarea'): ?>
                <div class="col lbl-title"><?php echo $args['title']; ?></div>
                <div class="lbl-content">
                    <textarea name="<?php echo $args['group'] . '[' . $args['id'] . ']'; ?>"
                              type="<?php echo $args['type']; ?>" cols=""
                              rows=""><?php echo isset($option_value[ $args['id'] ]) ? stripslashes(esc_textarea($option_value[ $args['id'] ])) : ''; ?></textarea>
                </div>
			<?php elseif ($args['type'] == 'multicheckbox'):
				foreach ($args['items'] as $key => $checkboxitem):
					?>
					<input type="hidden" name="<?php echo $args['group'] . '[' . $args['id'] . '][' . $key . ']'; ?>"
						   value="0"/>
					<label
						for="<?php echo $args['group'] . '[' . $args['id'] . '][' . $key . ']'; ?>"><?php echo $checkboxitem; ?></label>
					<input type="checkbox" name="<?php echo $args['group'] . '[' . $args['id'] . '][' . $key . ']'; ?>"
						   id="<?php echo $args['group'] . '[' . $args['id'] . '][' . $key . ']'; ?>" value="1"
						   <?php if ($key == 'reason'){ ?>checked="checked" disabled="disabled"<?php } else {
						checked($option_value[ $args['id'] ][ $key ]);
					} ?> />
				<?php endforeach; ?>
			<?php elseif ($args['type'] == 'multitext'):
				foreach ($args['items'] as $key => $textitem):
					?>
					<div class="lbl-title" for="<?php echo $args['group'] . '[' . $key . ']'; ?>"><?php echo $textitem; ?></div>
                    <div class="lbl-content">
                        <input type="text" id="<?php echo $args['group'] . '[' . $key . ']'; ?>" class="multitext"
                               name="<?php echo $args['group'] . '[' . $key . ']'; ?>"
                               value="<?php echo (isset($option_value[ $key ])) ? esc_attr($option_value[ $key ]) : ''; ?>">
                    </div>
				<?php endforeach; endif; ?>
		</div>
		<div class="col colthree">
			<small><?php echo $args['desc'] ?></small>
		</div>
	</div>
	<?php
}

?>