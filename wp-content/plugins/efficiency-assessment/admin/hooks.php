<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 8/27/18
 * Time: 10:12 AM
 */

add_action('admin_enqueue_scripts', 'effa_admin_enqueue_scripts');
function effa_admin_enqueue_scripts(){
    wp_enqueue_style( EFFA_NAME, EFFA_MODULE_URL . '/admin/assets/css/efficiency-assessment-admin.css', array(), EFFA_VERSION, 'all' );
    wp_enqueue_script( EFFA_NAME, EFFA_MODULE_URL . '/admin/assets/js/efficiency-assessment-admin.js', array( 'jquery' ), EFFA_VERSION, true );
}

add_action('plugins_loaded', 'effa_load_plugin_textdomain');
function effa_load_plugin_textdomain(){
    load_plugin_textdomain(
        EFFA_LANG_DOMAIN,
        false,
        EFFA_LANGUAGE_PATH
    );
}
add_action('admin_init', 'define_admin_hooks', 100);
function define_admin_hooks() {
    $current_version = get_effa_version();
    # Check for upgrade version
    $counter = 10;
    if( version_compare(EFFA_VERSION, $current_version, '>') ) {
        require_once EFFA_MODULE_DIR . '/includes/class-effa-activator.php';
        while( $counter && version_compare(EFFA_VERSION, $current_version, '>') ) {
            $counter--;
            $current_version = Effa_Activator::upgrade();
        }
    }

}
