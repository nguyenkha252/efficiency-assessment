<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 8/27/18
 * Time: 10:03 AM
 */

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              #
 * @since             1.0.0
 * @package           Efficiency_Assessment
 *
 * @wordpress-plugin
 * Plugin Name:       Efficiency Assessment
 * Plugin URI:        #
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Richard
 * Author URI:        #
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       effa
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */

define('EFFA_VERSION', '0.0.3');
define('EFFA_NAME', 'efficiency_assessment');
define('EFFA_LANG_DOMAIN', 'effa');
define('EFFA_LANGUAGE_PATH', __DIR__ . '/languages/');
define('EFFA_MODULE_DIR', __DIR__ );
define('EFFA_MODULE_URL', plugin_dir_url(__FILE__) );
define('EFFA_POST_TYPE_EFFA', 'efficiency_assessment' );

define('PAGE_PROFILE_PATH',                      __('profile', EFFA_LANG_DOMAIN) );
define('PAGE_DASHBOARD',                    __('dashboard', EFFA_LANG_DOMAIN) );
define('PAGE_ORGANIZATIONAL_STRUCTURE',     __('organizational-structure', EFFA_LANG_DOMAIN) );
define('PAGE_MANAGE_MEMBERS',               __('manage-members', EFFA_LANG_DOMAIN) );
define('PAGE_EFFA_SYSTEM',             __('efficiency-assessment', EFFA_LANG_DOMAIN) );
define('PAGE_NEWS',                         __('news', EFFA_LANG_DOMAIN) );
define('PAGE_SETTINGS',                     __('settings', EFFA_LANG_DOMAIN) );
define('PAGE_USER_MANAGERS',                __('user-managers', EFFA_LANG_DOMAIN) );


function get_effa_version() {
    return get_option(EFFA_NAME .'_version', '0.0.0');
}
//define('EFFA_VERSION_NAME', sprintf(__('Beta (%s)', EFFA_LANG_DOMAIN), get_effa_version() ) );

function get_effa_next_version() {
    $current_version = get_effa_version();
    $parts = explode('.', $current_version);
    $parts = array_map('intval', $parts);
    $parts[2] ++;
    if( $parts[2] > 9 ) {
        $parts[2] = 0;
        $parts[1] ++;
    }
    if( $parts[1] > 9 ) {
        $parts[1] = 0;
        $parts[0] ++;
    }
    $next_version = "{$parts[0]}.{$parts[1]}.{$parts[2]}";
    return $next_version;
}

function set_effa_version($ver) {
    return update_option(EFFA_NAME .'_version', $ver);
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-promanland-activator.php
 */
function activate_effa() {
    require_once __DIR__ . '/includes/class-effa-activator.php';
    Effa_Activator::activate();
}

register_activation_hook( __FILE__, 'activate_effa' );

require_once EFFA_MODULE_DIR . '/admin/hooks.php';
require_once EFFA_MODULE_DIR . '/admin/settings.php';

#public for front-end
require_once EFFA_MODULE_DIR . '/public/efficiency-assessment-public.php';
