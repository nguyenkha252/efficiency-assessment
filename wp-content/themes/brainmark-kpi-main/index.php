<?php
$redirect_to = site_url();
$user = wp_get_current_user();
include_once __DIR__ . '/login.php';
$user = wp_get_current_user();

?><!DOCTYPE html>
<!--[if IE 8]> <html <?php language_attributes(); ?> class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html <?php language_attributes(); ?> class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html <?php language_attributes(); ?>>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>Competency System </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <link rel="shortcut icon" href="<?php echo THEME_URL; ?>/favicon.ico" />


        <?php wp_head(); ?>

        <link rel='stylesheet' href="<?php echo THEME_URL; ?>/style_customize.css" />
    </head>
    <!-- END HEAD -->

    <body class="<?php echo ($user->exists() ? 'login loggedin' : 'login'); ?>">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="http://www.brainmark.vn/">
                <img src="<?php echo THEME_URL; ?>/assets/images/logo-brainmark.png" alt="" />
            </a>
        </div>
        <!-- END LOGO -->

        <!-- BEGIN LOGIN -->
        <div class="content">
            <?php if( !is_user_logged_in() ): ?>
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="<?php echo site_url(); ?>" method="post" enctype="application/x-www-form-urlencoded">
                <h3 class="form-title">Đăng nhập</h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Vui lòng nhập vào tài khoản hoặc mật khẩu của bạn. </span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Tên người dùng hoặc Địa chỉ Email</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Tài khoản hoặc email" name="user_login" /> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Mật khẩu</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Mật khẩu" name="user_password" /> </div>
                </div>
                <div class="form-actions">
                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                        <input class="form-control-checkbox" type="checkbox" value="forever" <?php checked( $rememberme ); ?> id="rememberme" name="remember" /> Ghi nhớ
                        <span></span>
                    </label>
                    <button type="submit" class="btn green pull-right"> Đăng nhập </button>
                </div>

            </form>
            <!-- END LOGIN FORM -->
            <?php else: ?>
                <div class="menus">
                    <?php if( !empty($user->allcaps['manage_sites']) || $user->has_cap('manage_sites') ): ?>
                    <a href="<?php echo network_admin_url('sites.php'); ?>"><?php _e('Manage Sites', TPL_DOMAIN_LANG); ?></a>
                    <?php endif; ?>
                    <?php if( !empty($user->allcaps['create_users']) || $user->has_cap('create_users') ): ?>
                    <a href="<?php echo admin_url('users.php'); ?>"><?php _e('Manage Users', TPL_DOMAIN_LANG); ?></a>
                    <?php endif; ?>
                    <a href="<?php echo admin_url(); ?>"><?php _e('Dashboard', TPL_DOMAIN_LANG); ?></a>
                </div>
            <?php endif; ?>
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <div class="copyright"> 2017 &copy; Brainmark. </div>
        <!-- END COPYRIGHT -->
        <!--[if lt IE 9]>
        <script src="<?php echo THEME_URL; ?>/assets/global/plugins/respond.min.js"></script>
        <script src="<?php echo THEME_URL; ?>/assets/global/plugins/excanvas.min.js"></script>
        <script src="<?php echo THEME_URL; ?>/assets/global/plugins/ie8.fix.min.js"></script>
        <![endif]-->
        <?php wp_footer(); ?>

    </body>

</html>
