<?php

//Check login user
if( !is_user_logged_in() ) {
    if ( ($_SERVER["REQUEST_METHOD"] == "POST" ) && isset( $_POST['user_login'] ) && isset( $_POST['user_password'] )
        && !empty( $_POST['user_login'] ) &&  !empty( $_POST['user_password'] ) ) {
        $creds['user_login'] = $_POST['user_login'];
        $creds['user_password'] = $_POST['user_password'];
        $creds['remember'] = !empty($_POST['remember']) ? $_POST['remember'] : false;
        $redirect_to = !empty($_POST['redirect_to']) ? $_POST['redirect_to'] : '';

        global $wpdb;
        $blog_id = get_current_blog_id();
        $user = wp_signon( $creds );

        # echo "<p>Blog ID: '{$blog_id}', Users: '{$wpdb->users}', Users Meta: '{$wpdb->usermeta}'</p><br><pre>"; var_dump($creds, $user, $_COOKIE); echo "</pre>"; exit();

        if ( !empty( $user ) ) {
            if( !is_wp_error($user) ) {
                /* $session_tokens = get_user_meta($user->ID, 'session_tokens', true);
                if( $session_tokens === false ) {
                    $manager = WP_Session_Tokens::get_instance( $user->ID );
                    echo '<pre>'; var_dump(__LINE__, __FILE__, $manager); echo '</pre>'; exit;
                } */
                /* @var $user WP_User */
                if( !empty($user->allcaps['manage_sites']) || $user->has_cap('manage_sites') ) {
                    # $redirect_to = "http://".DOMAIN_CURRENT_SITE."/wp-admin/network/sites.php";
                    if( get_current_blog_id() > 1 ) {
                        $redirect_to = site_url('?type=assessment');
                    } else {
                        $redirect_to = network_admin_url('sites.php');
                    }
                } else {
                    $redirect_to = site_url('?type=assessment');
                }
                wp_redirect( $redirect_to );
                exit;
            } else {
                return $user;
                /* @var $user WP_Error */
            }
        }
    }
} else {
    $user = wp_get_current_user();

    if( is_admin() && get_current_blog_id() == 1 ) {
        $redirect_to = network_admin_url('sites.php');
        wp_redirect( $redirect_to );
        exit;
    }

    /***Log Out Function***/
    if ( isset( $_REQUEST['action'] ) && $_REQUEST['action'] == 'logout' ) {
        wp_logout();
        wp_redirect( site_url() );
        exit;
    }

}
