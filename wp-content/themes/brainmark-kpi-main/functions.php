<?php
/*
 * Define Variables
 */
if (!defined('THEME_DIR'))
    define('THEME_DIR', get_template_directory());
if (!defined('THEME_URL'))
    define('THEME_URL', get_template_directory_uri());
if (!defined('LIK_AUTHOR_URL'))
    define('LIK_AUTHOR_URL', 'http://ufx.vn');
if (!defined('LIK_AUTHOR_NAME'))
    define('LIK_AUTHOR_NAME', 'UFX Company.');

if (!defined('TPL_DOMAIN_LANG'))
    define('TPL_DOMAIN_LANG', 'brain_branch');

if ( ! function_exists( 'brain_main_setup' ) ) :
    function brain_main_setup() {
        # $locale = apply_filters( 'theme_locale', is_admin() ? get_user_locale() : get_locale(), $domain );
        $path = THEME_DIR . '/languages';
        load_theme_textdomain( TPL_DOMAIN_LANG, $path );
        # return load_textdomain( $domain, $path . '/' . $locale . '.mo' );

        define('PAGE_PROFILE_PATH',                      __('profile', TPL_DOMAIN_LANG) );
        define('PAGE_MY_CAKENDAR_PATH',                  __('calendar', TPL_DOMAIN_LANG) );
        define('PAGE_MY_INBOXS_PATH',                    __('inboxs', TPL_DOMAIN_LANG) );
        define('PAGE_MY_TODO_PATH',                      __('to-do', TPL_DOMAIN_LANG) );

        define('PAGE_DASHBOARD',                    __('dashboard', TPL_DOMAIN_LANG) );
        define('PAGE_ORGANIZATIONAL_STRUCTURE',     __('organizational-structure', TPL_DOMAIN_LANG) );
        define('PAGE_MANAGE_MEMBERS',               __('manage-members', TPL_DOMAIN_LANG) );
        define('PAGE_MANAGE_TARGETS',               __('manage-targets', TPL_DOMAIN_LANG) );
        define('PAGE_KPI_BANK',                     __('kpi-bank', TPL_DOMAIN_LANG) );
        define('PAGE_APPLY_KPI_SYSTEM',             __('apply-bsc-kpi-system', TPL_DOMAIN_LANG) );
        define('PAGE_STATISTICAL',                  __('statistical', TPL_DOMAIN_LANG) );
        define('PAGE_NEWS',                         __('news', TPL_DOMAIN_LANG) );
        define('PAGE_SETTINGS',                     __('settings', TPL_DOMAIN_LANG) );
        define('PAGE_USER_MANAGERS',                __('user-managers', TPL_DOMAIN_LANG) );
        define('PAGE_EFFA_SYSTEM',                  __('efficiency-assessment', EFFA_LANG_DOMAIN) );

        return;
    }


    if( defined('DOING_AJAX') && DOING_AJAX ) {
        function wp_ajax_callback($prefix, $params) {
            $filename = THEME_DIR . "/ajax/{$prefix}{$_REQUEST['action']}.php";
            if( file_exists($filename) ) {
                require_once $filename;
            }

            $callback = "wp_ajax_{$prefix}".str_replace( ['-', ':'], ['_', '_'], $_REQUEST['action'] );

            if( function_exists($callback) ) {
                /* add_filter('wp_doing_ajax', function($result) {
                    return false;
                }); */
                $callback($params);
                exit();
            }
        };

        if( !empty($_REQUEST['action']) ) {
            $is_logged_in = is_user_logged_in();

            // Get gallery at proejct detail front - end
            $prefix = $is_logged_in ? 'wp_ajax_' : 'wp_ajax_nopriv_';
            // Register core Ajax calls.
            if( $_SERVER["REQUEST_METHOD"] == 'GET' ) {
                add_action( $prefix . $_REQUEST['action'], function(){
                    return wp_ajax_callback('get_', $_GET);
                }, 2 );
            } else if( $_SERVER["REQUEST_METHOD"] == 'POST' ) {
                add_action( $prefix . $_REQUEST['action'], function(){
                    return wp_ajax_callback('post_', $_POST);
                }, 2 );
            }
        }
        # });
    }
endif; // twentyfifteen_setup
add_action( 'after_setup_theme', 'brain_main_setup' );
/*
 * Add theme Support
 */
if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

}

/*
 * Add Image Size for Wordpress
 */
if ( function_exists('add_image_size') ) {
    //add_image_size( 'homepage-slide', 620, 238, true ); // (cropped)
    //add_image_size( 'homepage-slide-thumb', 42, 24, true ); // (cropped)
    //add_image_size( 'thumb_post', 457, 232, true ); // (cropped)
}

/*
 *  Localisation Support
 */
//load_theme_textdomain('lik_theme', THEME_DIR . '/languages/');


/*
 *  Add page slug to body class
 */
function lik_add_slug_to_body_class($classes)
{
	global $post;
	if (is_home()) {
		$key = array_search('blog', $classes);
		if ($key > -1) {
			unset($classes[$key]);
		}
	} elseif (is_page()) {
		$classes[] = sanitize_html_class($post->post_name);
	} elseif (is_singular()) {
		$classes[] = sanitize_html_class($post->post_name);
	}

	return $classes;
}
add_filter('body_class', 'lik_add_slug_to_body_class');


/*
 *  Change login logo & url in WP Admin
 */
function lik_custom_login_logo()
{
    echo "<style type='text/css'>
body.login {
                    background: #f1f1f1;
                }
                .login #login_error, .login .message {
                    border-left: 4px solid orange;
                  }
                .login h1 a {
                    background-image: url('". THEME_URL ."/assets/images/logo-brainmark.png');
                    background-size: 280px 78px;
                    width: 280px;
                    height: 78px;
                }

                .login #login {
                  padding: 10px !important;
                  width: 300px !important;
                }


                #wp-submit {
                  background: #32c5d2;
                  color:#fff;
                  border-radius: 3px;
                  -webkit-border-radius: 3px;
                  -moz-border-radius: 3px;
                  text-shadow: none;
                  border: none;
                  box-shadow: unset;

                }
                .login form {
                    border: none !important;
                    border-radius: 3px;
                    -webkit-border-radius: 3px;
                    -moz-border-radius: 3px;
                }

                .login form {
                    margin-left: auto;
                    margin-right: auto;
                    padding: 30px;
                    border: 1px solid rgba(0, 0, 0, .2);
                    background-clip: padding-box;
                    background: rgba(255, 255, 255, 0.9);
                    overflow: hidden;
                }
                .wp-core-ui .button-primary {
                    background: #1790D1;
                    border-color: #1790D1;
                    -webkit-box-shadow: 0 1px 0 #006799;
                    box-shadow: 0 1px 0 #1790D1;
                    color: #fff;
                    text-decoration: none;
                    text-shadow: 0 -1px 1px #006799,1px 0 1px #006799,0 1px 1px #006799,-1px 0 1px #006799;
                }
                .login label {
                    color: #333;
                }

                #backtoblog, #nav {
                    display: none;
                }

    </style>";
}
add_action('login_head', 'lik_custom_login_logo');

function lik_custom_login_logo_url( $url )
{
    return  LIK_AUTHOR_URL ;
}
add_filter('login_headerurl', 'lik_custom_login_logo_url');

function lik_custom_login_logo_title()
{
    return get_bloginfo('name');
}
add_filter('login_headertitle','lik_custom_login_logo_title');


/*
 *  Remove wp-logo on admin bar
 */
function lik_wp_admin_bar_remove()
{
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');
}
add_action('wp_before_admin_bar_render', 'lik_wp_admin_bar_remove', 0);

// Custom WordPress Admin Color Scheme
function admin_css() {
	wp_enqueue_style( 'admin_css', get_template_directory_uri() . '/assets/global/css/admin-theme-core.css' );
}
add_action('admin_print_styles', 'admin_css' );

/*
 * Add Editor in back-end
 */
function lik_theme_add_editor_styles() {
    add_editor_style( THEME_URL . '/editor-style.css' );
}
add_action( 'init', 'lik_theme_add_editor_styles' );

/*
 * Add excerpt for page
 */
function lik_add_excerpts_to_pages() {
    add_post_type_support( 'page', 'excerpt' );
}
add_action( 'init', 'lik_add_excerpts_to_pages' );

/*
 * Set Editor Visual Default
 */
add_filter( 'wp_default_editor', create_function('', 'return "tinymce";') );

/*
 *  Remove generator
 */
remove_action( 'wp_head', 'wp_generator' );

/*
 * Change Footer Text in Admin
 */
function lik_change_footer_text() {
    echo "Powered by <a href='".LIK_AUTHOR_URL."' target='_blank'>".LIK_AUTHOR_NAME."</a>";
}
add_filter('admin_footer_text', 'lik_change_footer_text');

/*
 * Remove help bar
 */
function lik_wp_remove_help($old_help, $screen_id, $screen) {
    $screen->remove_help_tabs();
    return $old_help;
}
add_filter( 'contextual_help', 'lik_wp_remove_help', 999, 3 );

/*
 * Fix WP-Lockdown
 */
add_filter('LD_404', 'lik_redirect_404');
function lik_redirect_404(){
    wp_redirect( home_url().'/not-found/' ); exit;
}

/**
* Fix Gravity Form Tabindex Conflicts
*/
add_filter('gform_tabindex', 'gform_tabindexer', 10, 2);
function gform_tabindexer($tab_index, $form = false) {
    $starting_index = 1000; // if you need a higher tabindex, update this number
    if ($form)
        add_filter('gform_tabindex_' . $form['id'], 'gform_tabindexer');
    if (class_exists('GFCommon')) {
        return GFCommon::$tab_index >= $starting_index ? GFCommon::$tab_index : $starting_index;
    }
}


/*
 * Include framework files
 */
foreach( glob( THEME_DIR . "/includes/*.php") as $file_name ){
    require_once ( $file_name );
}

/*
 * Fix wp_redirect sent header
 */
function app_output_buffer() {
    ob_start();
}

// soi_output_buffer
add_action('init', 'app_output_buffer');


function admin_css_menu_hide() {
    $user = wp_get_current_user();
    if( !$user->has_cap('manage_sites')) {
        wp_enqueue_style( 'admin_css_menu_hide', get_template_directory_uri() . '/assets/global/css/admin-hide-menu.css' );
    }
}
add_action('admin_print_styles', 'admin_css_menu_hide' );


// define the network_admin_menu callback
function action_network_admin_menu() {
	# add_menu_page('Danh Sách KPI Công Ty','Danh Sách KPI Công Ty','manage_options','sites.php', '', 'dashicons-admin-multisite');
}
// add the action
# add_action( 'network_admin_menu', 'action_network_admin_menu', 10, 2 );

/*
add_action( 'admin_menu', 'linked_url' );
function linked_url() {
  add_menu_page( 'linked_url', 'External link', 'read', 'my_slug', '', 'dashicons-text', 1 );
}*/



add_action('init', function() {
    require_once __DIR__ . '/includes/custom-role.php';

    #remove_action( 'admin_menu', 'dm_add_pages' );
    #add_action( 'network_admin_menu', 'dm_network_pages' );

    if( is_admin() || is_network_admin() ) {
        $file_setup = THEME_DIR . '/../brainmark-kpi-branch/inc/handle-admin-settings.php';
        if( file_exists($file_setup) ) {
            require_once $file_setup;
        }
        require_once __DIR__ . '/includes/handle-domains-mapping.php';
    }
});
