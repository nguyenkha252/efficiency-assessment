<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/11/17
 * Time: 22:05
 */

function esc_json_attr($data) {
    $data = esc_attr( str_replace('"', '&#34;', json_encode($data) ) );
    return $data;
}

function esc_json_attr_e($data) {
    echo esc_json_attr($data);
}

global $wpdb;
# echo '<pre>'; var_dump($wpdb->blogid, get_current_blog_id()); echo '</pre>'; exit;
$has_custom_role = get_option('custom_role');
if( $has_custom_role === false ) {
    $allcaps = [
        'level_9' => [
            'Level 9',
            "create_posts" => true,
            "create_users" => true,
            "delete_others_posts" => true,
            "delete_posts" => true,
            "delete_private_posts" => true,
            "delete_published_posts" => true,
            "delete_users" => true,
            "edit_files" => true,
            "edit_others_posts" => true,
            "edit_posts" => true,
            "edit_private_posts" => true,
            "edit_published_posts" => true,
            "edit_users" => true,
            "export" => true,
            "import" => true,
            "level_0" => true,
            "level_1" => true,
            "level_2" => true,
            "level_3" => true,
            "level_4" => true,
            "level_5" => true,
            "level_6" => true,
            "level_7" => true,
            "level_8" => true,
            "level_9" => true,
            "list_users" => true,
            "manage_categories" => true,
            "manage_links" => true,
            "manage_options" => true,
            "moderate_comments" => true,
            "promote_users" => true,
            "publish_posts" => true,
            "read" => true,
            "read_private_posts" => true,
            "remove_users" => true,
            "upload_files" => true
        ],
        'level_8' => [
            'Level 8',
            "create_posts" => true,
            "delete_posts" => true,
            "delete_published_posts" => true,
            "edit_files" => true,
            "edit_others_posts" => true,
            "edit_posts" => true,
            "edit_published_posts" => true,
            "export" => true,
            "import" => true,
            "level_0" => true,
            "level_1" => true,
            "level_2" => true,
            "level_3" => true,
            "level_4" => true,
            "level_5" => true,
            "level_6" => true,
            "level_7" => true,
            "level_8" => true,
            "list_users" => true,
            "manage_categories" => true,
            "manage_options" => true,
            "moderate_comments" => true,
            "read" => true,
            "read_private_posts" => true,
            "upload_files" => true
        ],
        'level_7' => [
            'Level 7',
            "edit_files" => true,
            "edit_posts" => true,
            "export" => true,
            "import" => true,
            "level_0" => true,
            "level_1" => true,
            "level_2" => true,
            "level_3" => true,
            "level_4" => true,
            "level_5" => true,
            "level_6" => true,
            "level_7" => true,
            "list_users" => true,
            "read" => true,
            "upload_files" => true
        ],

        'level_6' => [
            'Level 6',
            "edit_files" => true,
            "edit_posts" => true,
            "export" => true,
            "import" => true,
            "level_0" => true,
            "level_1" => true,
            "level_2" => true,
            "level_3" => true,
            "level_4" => true,
            "level_5" => true,
            "level_6" => true,
            "list_users" => true,
            "read" => true,
            "upload_files" => true
        ],
        'level_5' => [
            'Level 5',
            "edit_files" => true,
            "edit_posts" => true,
            "export" => true,
            "import" => true,
            "level_0" => true,
            "level_1" => true,
            "level_2" => true,
            "level_3" => true,
            "level_4" => true,
            "level_5" => true,
            "list_users" => true,
            "read" => true,
            "upload_files" => true
        ],
        'level_4' => [
            'Level 4',
            "edit_files" => true,
            "edit_posts" => true,
            "export" => true,
            "import" => true,
            "level_0" => true,
            "level_1" => true,
            "level_2" => true,
            "level_3" => true,
            "level_4" => true,
            "list_users" => true,
            "read" => true,
            "upload_files" => true
        ],
        'level_3' => [
            'Level 3',
            "edit_files" => true,
            "edit_others_posts" => true,
            "edit_posts" => true,
            "edit_private_posts" => true,
            "edit_published_posts" => true,
            "export" => true,
            "import" => true,
            "level_0" => true,
            "level_1" => true,
            "level_2" => true,
            "level_3" => true,
            "list_users" => true,
            "read" => true,
            "read_private_posts" => true,
            "upload_files" => true
        ],
        'level_2' => [
            'Level 2',
            "edit_files" => true,
            "edit_posts" => true,
            "export" => true,
            "import" => true,
            "level_0" => true,
            "level_1" => true,
            "level_2" => true,
            "list_users" => true,
            "read" => true,
            "upload_files" => true
        ],
        'level_1' => [
            'Level 1',
            "edit_files" => true,
            "edit_posts" => true,
            "export" => true,
            "import" => true,
            "level_0" => true,
            "level_1" => true,
            "list_users" => true,
            "read" => true,
            "upload_files" => true
        ],
        'level_0' => [
            'Level 0',
            "edit_files" => true,
            "edit_posts" => true,
            "level_0" => true,
            "read" => true,
            "upload_files" => true
        ]
    ];
    global $wpdb;
    foreach( $allcaps as $name => $caps ) {
        $display = array_shift($caps);
        $role = get_role($name);
        if( !$role ) {
            $role = add_role($name, $display, $caps);
        } else {
            foreach($caps as $cap => $val) {
                $role->add_cap($cap, $val);
            }
        }
    }
    add_option('custom_role', true);
}
add_action('editable_roles', function($all_roles) {
    # echo '<pre>'; var_dump($all_roles); echo '</pre>'; exit;
    # ['Administrator', 'Editor', 'Author', 'Contributor', 'Subscriber'];
    foreach( $all_roles as $k => &$role ) {
        $role['name'] = __('User ' . $role['name'], TPL_DOMAIN_LANG);
    }
    return $all_roles;
});




/**
 * Send a JSON response back to an Ajax request.
 *
 * @since 3.5.0
 * @since 4.7.0 The `$status_code` parameter was added.
 *
 * @param mixed $response    Variable (usually an array or object) to encode as JSON,
 *                           then print and die.
 * @param int   $status_code The HTTP status code to output.
 */
function send_response_json( $response, $status_code = null, $description = '' ) {
    @header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
    if ( null !== $status_code ) {
        status_header( $status_code, $description );
        if ( ! $description ) {
            $description = get_status_header_desc( $status_code );
        }
    }
    if( !empty($description) ) {
        $description = json_encode($description);
        @header("Xhr-Message: {$description}");
    }
    echo wp_json_encode( $response, JSON_PRETTY_PRINT );
    exit;
}


/*
add_action('auth_cookie_malformed', function($cookie, $scheme) {
    echo '<pre>'; var_dump(__LINE__, __FILE__, $cookie, $scheme, $_COOKIE); echo '</pre>'; exit;
    exit;
}, 100, 2);

add_action('auth_cookie_bad_session_token', function($cookie_elements) {
    echo '<pre>'; var_dump(__LINE__, __FILE__, $cookie_elements); echo '</pre>'; exit;
    exit;
}, 100);

add_action('auth_cookie_valid', function($cookie_elements, $user) {
    echo '<pre>'; var_dump(__LINE__, __FILE__, $cookie_elements, $user); echo '</pre>'; exit;
    exit;
}, 100, 2); */
