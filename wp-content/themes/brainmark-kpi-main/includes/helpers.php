<?php

/*
 * Template get option field ACF
 * @param $key string
 * return string|object|array
 */

function lik_get_option($key) {
    if (function_exists('get_field')) {
        return get_field($key, 'option');
    } else {
        return false;
    }
}

/*
 * Template get field ACF
 * @param $field_key string
 * @param $post_id string|int
 * @param $format_value boolean
 * return string|object|array
 */

function lik_get_field($field_key, $post_id = false, $format_value = true) {
    if (function_exists('get_field')) {
        $result = get_field($field_key, $post_id, $format_value);
        return $result;
    } else {
        return '';
    }
}

/**
 * Cut string or text by number of character
 *
 * @param $string
 * @param $max_length
 * @param $more_string  '...'
 * @return string
 */
function lik_cut_tring_by_char($string, $max_length, $more_string = '...') {
    if (mb_strlen($string, "UTF-8") > $max_length) {
        $max_length = $max_length - 3;
        $string = mb_substr($string, 0, $max_length, "UTF-8");
        $pos = strrpos($string, " ");
        if ($pos === false) {
            return substr($string, 0, $max_length) . $more_string;
        }
        return substr($string, 0, $pos) . $more_string;
    } else {
        return $string;
    }
}

/**
 * Format Date Time
 *
 * @param $format string
 * @param $datetime Datetime
 * @param $locate code_string
 * @return string
 */
function lik_format_datetime($format = 'd/m/Y H:i', $datetime, $locate = "en_US") {
    if (!empty($datetime)) {
        $result = date_i18n($format, strtotime($datetime));
    } else {
        $result = '';
    }
    /*        setlocale(LC_TIME, $locate);
      $result = strftime($format, strtotime($datetime)); */
    return $result;
}

/**
 * Get facebook comment count or url
 *
 * @param $url
 * @return int
 */
function lik_get_comments_fb_count($url) {
    $cacheId = '_sbfbcm_' . md5($url);
    $result = get_transient($cacheId);
    if (false === $result) {
        $json = json_decode(file_get_contents('https://graph.facebook.com/?ids=' . $url));
        $result = isset($json->$url->comments) ? $json->$url->comments : 0;
        set_transient($cacheId, $result, 60 * 60 * 1); // cache 1 hour
    }
    return $result;
}

/*
 * Get Attachment ID from URL in Wordpress
 */

function lik_get_attachment_id_from_url($attachment_url = '') {

    global $wpdb;
    $attachment_id = false;

    // If there is no url, return.
    if ('' == $attachment_url)
        return;

    // Get the upload directory paths
    $upload_dir_paths = wp_upload_dir();

    // Make sure the upload path base directory exists in the attachment URL, to verify that we're working with a media library image
    if (false !== strpos($attachment_url, $upload_dir_paths['baseurl'])) {

        // If this is the URL of an auto-generated thumbnail, get the URL of the original image
        $attachment_url = preg_replace('/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url);

        // Remove the upload path base directory from the attachment URL
        $attachment_url = str_replace($upload_dir_paths['baseurl'] . '/', '', $attachment_url);

        // Finally, run a custom database query to get the attachment ID from the modified attachment URL
        $attachment_id = $wpdb->get_var($wpdb->prepare("SELECT wposts.ID FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta WHERE wposts.ID = wpostmeta.post_id AND wpostmeta.meta_key = '_wp_attached_file' AND wpostmeta.meta_value = '%s' AND wposts.post_type = 'attachment'", $attachment_url));
    }

    return $attachment_id;
}

/**
 * get page object (including permalink) by template file
 *
 * @param $name string template file name
 * @return bool|mixed
 * @example $page_contact = sb_get_page_by_template('template-contact.php');
 */
function sb_get_page_by_template($name) {
    $pages = get_pages(array(
        'meta_key' => '_wp_page_template',
        'meta_value' => $name,
        'hierarchical' => 0
    ));
    if (!empty($pages)) {
        $page = reset($pages);
        $page->permalink = sb_get_permalink($page);
        return $page;
    } else {
        return false;
    }
}

/*
 * Function get Feed from URL
 */

function lik_get_feed_from_url($url) {
    include_once( ABSPATH . WPINC . '/feed.php' );
    // Get a SimplePie feed object from the specified feed source.
    $rss = fetch_feed($url);
    $maxitems = 0;

    if (!is_wp_error($rss)) { // Checks that the object is created correctly
        // Figure out how many total items there are, but limit it to 5. 
        $maxitems = $rss->get_item_quantity(4);

        // Build an array of all the items, starting with element 0 (first element).
        $rss_items = $rss->get_items(0, $maxitems);
    }
    return array('maxitem' => $maxitems, 'rss_item' => $rss_items);
}

/*
 * Get Product Woocommerce from main site
 */

function lik_get_products_from_blog_id($blog_id, $post_type, $posts_per_page = 5, $category_slug) {
    global $switched;
    switch_to_blog($blog_id);
    $args = array(
        'posts_per_page' => $posts_per_page,
        'post_type' => $post_type,
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'slug',
                'terms' => $category_slug
            )
        )
    );
    $products = get_posts($args);
    foreach ($products as $key => $item) {
        $product = new WC_Product($item->ID);
        $products[$key]->price = $product->get_price_html();
        //Get product meta data
        $attributes['propelleraxel-effekt_kwhk'] = lik_get_field('propelleraxel-effekt_kwhk', $item->ID);
        $attributes['vevaxeleffekt_kwhk'] = lik_get_field('vevaxeleffekt_kwhk', $item->ID);
        $attributes['nominellt_varvtal'] = lik_get_field('nominellt_varvtal', $item->ID);
        $attributes['antal_cylindrar'] = lik_get_field('antal_cylindrar', $item->ID);
        $attributes['cylindervolym_liteln3'] = lik_get_field('cylindervolym_liteln3', $item->ID);
        $products[$key]->attributes = $attributes;
        //Get thumbnail Image
        $products[$key]->product_thumbnail = get_the_post_thumbnail_by_blog($blog_id, $item->ID, 'thumbnail');
        //Get File PDF
        $products[$key]->product_file_pdf = lik_get_file_pdf_by_blog( $blog_id, $item->ID );
    }

    restore_current_blog();
    return $products;
}

/*
 * Get File Post Thumbnail by Blog
 */
function get_the_post_thumbnail_by_blog($blog_id = NULL, $post_id = NULL, $size = 'post-thumbnail', $attrs = NULL) {
    global $current_blog;
    $sameblog = false;

    if (empty($blog_id) || $blog_id == $current_blog->ID) {
        $blog_id = $current_blog->ID;
        $sameblog = true;
    }
    if (empty($post_id)) {
        global $post;
        $post_id = $post->ID;
    }
    if ($sameblog)
        return get_the_post_thumbnail($post_id, $size, $attrs);

    if (!has_post_thumbnail_by_blog($blog_id, $post_id))
        return false;

    global $wpdb;
    $oldblog = $wpdb->set_blog_id($blog_id);

    $blogdetails = get_blog_details($blog_id);
    $thumbcode = str_replace($current_blog->domain . $current_blog->path, $blogdetails->domain . $blogdetails->path, get_the_post_thumbnail($post_id, $size, $attrs));

    $wpdb->set_blog_id($oldblog);
    return $thumbcode;
}

/*
 * Check Image Thumbnail By Blog
 */
function has_post_thumbnail_by_blog($blog_id = NULL, $post_id = NULL) {
    if (empty($blog_id)) {
        global $current_blog;
        $blog_id = $current_blog;
    }
    if (empty($post_id)) {
        global $post;
        $post_id = $post->ID;
    }

    global $wpdb;
    $oldblog = $wpdb->set_blog_id($blog_id);

    $thumbid = has_post_thumbnail($post_id);
    $wpdb->set_blog_id($oldblog);
    return ($thumbid !== false) ? true : false;
}

/*
 * Cut String by max char
 */
function the_excerpt_max_charlength( $excerpt, $charlength ) {
    $charlength++;

    if (mb_strlen($excerpt) > $charlength) {
        $subex = mb_substr($excerpt, 0, $charlength - 5);
        $exwords = explode(' ', $subex);
        $excut = - ( mb_strlen($exwords[count($exwords) - 1]) );
        if ($excut < 0) {
            echo mb_substr($subex, 0, $excut);
        } else {
            echo $subex;
        }
        echo '[...]';
    } else {
        echo $excerpt;
    }
}

/*
 * Convert domain from current blog to main blog
 */
function lik_get_file_pdf_by_blog( $blog_id, $post_id ) {
    global $current_blog;
    $sameblog = false;

    if (empty($blog_id) || $blog_id == $current_blog->ID) {
        $blog_id = $current_blog->ID;
        $sameblog = true;
    }
    if ($sameblog)
        return lik_get_field('file', $post_id);

    global $wpdb;
    $oldblog = $wpdb->set_blog_id($blog_id);

    $blogdetails = get_blog_details($blog_id);
    $thumbcode = str_replace($current_blog->domain . $current_blog->path, $blogdetails->domain . $blogdetails->path, lik_get_field('file', $post_id));

    $wpdb->set_blog_id($oldblog);
    return $thumbcode;
}

/*
 * Function gte field form lgob id
 */

function lik_get_field_by_blog( $blog_id, $post_id, $key_name){
    global $current_blog;
    $sameblog = false;
    
    if (empty($blog_id) || $blog_id == $current_blog->ID) {
        $blog_id = $current_blog->ID;
        $sameblog = true;
    }
    if ($sameblog)
        return lik_get_field( $key_name, $post_id );

    global $wpdb;
    $oldblog = $wpdb->set_blog_id($blog_id);
    
    $field_value = lik_get_field( $key_name, $post_id );
    
    $wpdb->set_blog_id($oldblog);
    return $field_value;
}

/*
 * Function get field image form blog id
 */

function lik_get_field_image_by_blog( $blog_id, $post_id, $key_name ){
    global $switched;
    global $current_blog;
    $sameblog = false;
    
    if (empty($blog_id) || $blog_id == $current_blog->ID) {
        $blog_id = $current_blog->ID;
        $sameblog = true;
    }
    if ($sameblog)
        return lik_get_field( $key_name, $post_id );

    switch_to_blog( $blog_id );
    
    $blogdetails = get_blog_details($blog_id);
    $thumbcode = str_replace($current_blog->domain . $current_blog->path, $blogdetails->domain . $blogdetails->path, lik_get_field( $key_name, $post_id));
    
    restore_current_blog();
    
    return $thumbcode;
}

/*
 * Function replace white space with char
 */

function lik_replace_whitespace( $text, $char = '-' ){
    $text = trim( $text, ' ' );
    $text = str_replace( ' ',$char,$text );
    return $text;
}


