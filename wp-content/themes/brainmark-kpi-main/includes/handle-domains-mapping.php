<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/26/17
 * Time: 15:08
 */

add_action('admin_menu', function() {
    remove_action( 'admin_menu', 'dm_add_pages' );
}, 1);

add_action('network_admin_menu', function() {
    remove_action( 'network_admin_menu', 'dm_network_pages' );
}, 1);

add_action('admin_menu', function () {
    global $current_site, $wpdb, $wp_db_version, $wp_version;
    # exit(__LINE__ . ' ' . __FILE__);
    if ( !isset( $current_site ) && $wp_db_version >= 15260 ) { // WP 3.0 network hasn't been configured
        add_action('admin_notices', 'domain_mapping_warning');
        return false;
    }
    if ( $current_site->path != "/" ) {
        wp_die( __( "The domain mapping plugin only works if the site is installed in /. This is a limitation of how virtual servers work and is very difficult to work around.", 'wordpress-mu-domain-mapping' ) );
    }

    if ( kpi_can_mapping() || (get_site_option( 'dm_user_settings' ) && $current_site->blog_id != $wpdb->blogid && !dm_sunrise_warning( false ) ) ) {
        add_menu_page( __( 'Domain Mapping', 'wordpress-mu-domain-mapping'), __( 'Domain Mapping', 'wordpress-mu-domain-mapping'), 'manage_options',
            'kpi_domainmapping', 'kpi_dm_admin_page' );
        add_submenu_page('kpi_domainmapping',  __( 'Domains', 'wordpress-mu-domain-mapping'),  __( 'Domains', 'wordpress-mu-domain-mapping'),
            'manage_options', 'kpi_dm_domains_admin', 'kpi_dm_domains_admin');
    }
    return;
}, 10000000);

add_action( 'network_admin_menu', function() {
    add_menu_page(__( 'Domain Mapping', 'wordpress-mu-domain-mapping'), __( 'Domain Mapping', 'wordpress-mu-domain-mapping'), 'manage_options',
        'kpi_domainmapping', 'kpi_dm_admin_page');
    add_submenu_page('kpi_domainmapping', __( 'Domains', 'wordpress-mu-domain-mapping'), __( 'Domains', 'wordpress-mu-domain-mapping'),
        'manage_options', 'kpi_dm_domains_admin', 'kpi_dm_domains_admin');
}, 10000000);

/*
add_action( 'admin_init', function() {
    $valid = false;
    if( !empty($_GET['page']) && ($_GET['page'] == 'kpi_dm_domains_admin') && kpi_can_mapping() ) {
        $valid = true;
    }
    if( $valid ) {
        kpi_dm_handle_actions();
    }
} ); */

function kpi_can_mapping() {
    $user = wp_get_current_user();
    if( !in_array($user->user_login, ['oct_brainmark', 'kpibrainmark']) ) {
        return false;
    }
    return true;
}

function kpi_dm_admin_page() {
    global $wpdb, $current_site;

    if( !kpi_can_mapping() ) {
        return false;
    }
    maybe_create_db();

    if ( $current_site->path != "/" ) {
        wp_die( sprintf( __( "<strong>Warning!</strong> This plugin will only work if WordPress is installed in the root directory of your webserver. It is currently installed in &#8217;%s&#8217;.", "wordpress-mu-domain-mapping" ), $current_site->path ) );
    }

    // set up some defaults
    if ( get_site_option( 'dm_remote_login', 'NA' ) == 'NA' )
        add_site_option( 'dm_remote_login', 1 );
    if ( get_site_option( 'dm_redirect_admin', 'NA' ) == 'NA' )
        add_site_option( 'dm_redirect_admin', 1 );
    if ( get_site_option( 'dm_user_settings', 'NA' ) == 'NA' )
        add_site_option( 'dm_user_settings', 1 );

    if ( !empty( $_POST[ 'action' ] ) ) {
        check_admin_referer( 'domain_mapping' );
        if ( $_POST[ 'action' ] == 'update' ) {
            $ipok = true;
            $ipaddresses = explode( ',', $_POST[ 'ipaddress' ] );
            foreach( $ipaddresses as $address ) {
                if ( ( $ip = trim( $address ) ) && !preg_match( '|^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$|', $ip ) ) {
                    $ipok = false;
                    break;
                }
            }
            if( $ipok )
                update_site_option( 'dm_ipaddress', $_POST[ 'ipaddress' ] );
            if ( intval( $_POST[ 'always_redirect_admin' ] ) == 0 )
                $_POST[ 'dm_remote_login' ] = 0; // disable remote login if redirecting to mapped domain
            update_site_option( 'dm_remote_login', intval( $_POST[ 'dm_remote_login' ] ) );
            if ( ! preg_match( '/(--|\.\.)/', $_POST[ 'cname' ] ) && preg_match( '|^([a-zA-Z0-9-\.])+$|', $_POST[ 'cname' ] ) )
                update_site_option( 'dm_cname', stripslashes( $_POST[ 'cname' ] ) );
            else
                update_site_option( 'dm_cname', '' );
            update_site_option( 'dm_301_redirect', isset( $_POST[ 'permanent_redirect' ] ) ? intval( $_POST[ 'permanent_redirect' ] ) : 0 );
            update_site_option( 'dm_redirect_admin', isset( $_POST[ 'always_redirect_admin' ] ) ? intval( $_POST[ 'always_redirect_admin' ] ) : 0 );
            update_site_option( 'dm_user_settings', isset( $_POST[ 'dm_user_settings' ] ) ? intval( $_POST[ 'dm_user_settings' ] ) : 0 );
            update_site_option( 'dm_no_primary_domain', isset( $_POST[ 'dm_no_primary_domain' ] ) ? intval( $_POST[ 'dm_no_primary_domain' ] ) : 0 );
        }
    }

    echo '<h1>' . __( 'Domain Mapping Configuration', 'wordpress-mu-domain-mapping' ) . '</h1>';
    echo '<form method="POST">';
    echo '<input type="hidden" name="action" value="update" />';
    echo "<p class='p1'>" . __( "As a super admin on this network you can set the IP address users need to point their DNS A records at <em>or</em> the domain to point CNAME record at. If you don't know what the IP address is, ping this blog to get it.", 'wordpress-mu-domain-mapping' ) . "</p>";
    echo "<p class='p2 hidden'>" . __( "If you use round robin DNS or another load balancing technique with more than one IP, enter each address, separating them by commas.", 'wordpress-mu-domain-mapping' ) . "</p>";
    _e( "Server IP Address: ", 'wordpress-mu-domain-mapping' );
    echo "<input type='text' name='ipaddress' value='" . get_site_option( 'dm_ipaddress' ) . "' size='100'/><br />";

    // Using a CNAME is a safer method than using IP adresses for some people (IMHO)
    echo "<p class='p3'>" . __( "If you prefer the use of a CNAME record, you can set the domain here. This domain must be configured with an A record or ANAME pointing at an IP address. Visitors may experience problems if it is a CNAME of another domain.", 'wordpress-mu-domain-mapping' ) . "</p>";
    echo "<p class='p4 hidden'>" . __( "NOTE, this voids the use of any IP address set above", 'wordpress-mu-domain-mapping' ) . "</p>";
    _e( "Server CNAME domain: ", 'wordpress-mu-domain-mapping' );
    echo "<input type='text' name='cname' value='" . get_site_option( 'dm_cname' ) . "' size='100'/>";// (" . dm_idn_warning() . ")<br />";
    echo '<p class="p5">' . __( 'The information you enter here will be shown to your users so they can configure their DNS correctly. It is for informational purposes only', 'wordpress-mu-domain-mapping' ) . '</p>';

    echo "<h3 class='hidden'>" . __( 'Domain Options', 'wordpress-mu-domain-mapping' ) . "</h3>";
    echo "<ol class='hidden'><li><input type='checkbox' name='dm_remote_login' value='1' ";
    echo get_site_option( 'dm_remote_login' ) == 1 ? "checked='checked'" : "";
    echo " /> " . __( 'Remote Login', 'wordpress-mu-domain-mapping' ) . "</li>";
    echo "<li><input type='checkbox' name='permanent_redirect' value='1' ";
    echo get_site_option( 'dm_301_redirect' ) == 1 ? "checked='checked'" : "";
    echo " /> " . __( "Permanent redirect (better for your blogger's pagerank)", 'wordpress-mu-domain-mapping' ) . "</li>";
    echo "<li><input type='checkbox' name='dm_user_settings' value='1' ";
    echo get_site_option( 'dm_user_settings' ) == 1 ? "checked='checked'" : "";
    echo " /> " . __( 'User domain mapping page', 'wordpress-mu-domain-mapping' ) . "</li> ";
    echo "<li><input type='checkbox' name='always_redirect_admin' value='1' ";
    echo get_site_option( 'dm_redirect_admin' ) == 1 ? "checked='checked'" : "";
    echo " /> " . __( "Redirect administration pages to site's original domain (remote login disabled if this redirect is disabled)", 'wordpress-mu-domain-mapping' ) . "</li>";
    echo "<li><input type='checkbox' name='dm_no_primary_domain' value='1' ";
    echo get_site_option( 'dm_no_primary_domain' ) == 1 ? "checked='checked'" : "";
    echo " /> " . __( "Disable primary domain check. Sites will not redirect to one domain name. May cause duplicate content issues.", 'wordpress-mu-domain-mapping' ) . "</li></ol>";
    wp_nonce_field( 'domain_mapping' );
    echo "<p><input class='button-primary' type='submit' value='" . __( "Save", 'wordpress-mu-domain-mapping' ) . "' /></p>";
    echo "</form><br />";
}

function kpi_dm_domains_admin() {
    global $wpdb, $current_site;

    $wpdb->dmtable = $wpdb->base_prefix . 'domain_mapping';
    $wpdb->dmtablelogins = $wpdb->base_prefix . 'domain_mapping_logins';

    if( !kpi_can_mapping() ) {
        return false;
    }

    dm_echo_default_updated_msg();

    if ( $current_site->path != "/" ) {
        wp_die( sprintf( __( "<strong>Warning!</strong> This plugin will only work if WordPress is installed in the root directory of your webserver. It is currently installed in &#8217;%s&#8217;.", "wordpress-mu-domain-mapping" ), $current_site->path ) );
    }

    echo '<h1>' . __( 'Domain Mapping: Domains', 'wordpress-mu-domain-mapping' ) . '</h1>';
    if ( !empty( $_REQUEST[ 'action' ] ) ) {
        check_admin_referer( 'domain_mapping' );
        $domain = strtolower( $_REQUEST[ 'domain' ] );

        global $wpdb, $parent_file;
        $url = add_query_arg( array( 'page' => 'kpi_dm_domains_admin' ), admin_url( $parent_file ) );

        if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
            switch( $_POST[ 'action' ] ) {
                case "save":
                    $heading = '';
                    if ( $_POST[ 'blog_id' ] != 0 AND
                        $_POST[ 'blog_id' ] != 1 AND
                        null == $wpdb->get_var( $wpdb->prepare( "SELECT domain FROM {$wpdb->dmtable} WHERE blog_id != %d AND domain = %s", $_POST[ 'blog_id' ], $domain ) )
                    ) {
                        if ( $_POST[ 'orig_domain' ] == '' ) {
                            $wpdb->query( $wpdb->prepare( "INSERT INTO {$wpdb->dmtable} ( `blog_id`, `domain`, `active` ) VALUES ( %d, %s, %d )", $_POST[ 'blog_id' ], $domain, $_POST[ 'active' ] ) );
                            $heading = "<p><strong>" . sprintf( __( 'Domain %s Added', 'wordpress-mu-domain-mapping' ), $domain ) . "</strong></p>";
                        } else {
                            $wpdb->query( $wpdb->prepare( "UPDATE {$wpdb->dmtable} SET blog_id = %d, domain = %s, active = %d WHERE domain = %s", $_POST[ 'blog_id' ], $domain, $_POST[ 'active' ], $_POST[ 'orig_domain' ] ) );
                            $heading = "<p><strong>" . sprintf( __( 'Domain %s Updated', 'wordpress-mu-domain-mapping' ), $domain ) . "</strong></p>";
                        }
                        # $heading .= "'{$wpdb->last_query}'<br>'{$wpdb->last_error}'<br>";
                    }
                    kpi_domain_listing( false, $heading );
                    break;
                case 'add':
                    do_action('dm_handle_actions_add', $domain);
                    if( null == $wpdb->get_row( "SELECT blog_id FROM {$wpdb->blogs} WHERE domain = '$domain'" ) && null == $wpdb->get_row( "SELECT blog_id FROM {$wpdb->dmtable} WHERE domain = '$domain'" ) ) {
                        if ( $_POST[ 'primary' ] ) {
                            $wpdb->query( $wpdb->prepare( "UPDATE {$wpdb->dmtable} SET active = 0 WHERE blog_id = %d", $wpdb->blogid ) );
                        }
                        $wpdb->query( $wpdb->prepare( "INSERT INTO {$wpdb->dmtable} ( `id` , `blog_id` , `domain` , `active` ) VALUES ( NULL, %d, %s, %d )", $wpdb->blogid, $domain, $_POST[ 'primary' ] ) );
                        wp_redirect( add_query_arg( array( 'updated' => 'add' ), $url ) );
                        exit;
                    } else {
                        wp_redirect( add_query_arg( array( 'updated' => 'exists' ), $url ) );
                        exit;
                    }
                    break;
                case 'update':
                    if ( preg_match( '|^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$|', $_POST[ 'ipaddress' ] ) )
                        update_site_option( 'dm_ipaddress', $_POST[ 'ipaddress' ] );

                    if ( ! preg_match( '/(--|\.\.)/', $_POST[ 'cname' ] ) && preg_match( '|^([a-zA-Z0-9-\.])+$|', $_POST[ 'cname' ] ) )
                        update_site_option( 'dm_cname', stripslashes( $_POST[ 'cname' ] ) );
                    else
                        update_site_option( 'dm_cname', '' );

                    update_site_option( 'dm_301_redirect', intval( $_POST[ 'permanent_redirect' ] ) );
                    break;
                case "primary":
                    do_action('dm_handle_actions_primary', $domain);
                    $wpdb->query( $wpdb->prepare( "UPDATE {$wpdb->dmtable} SET active = 0 WHERE blog_id = %d", $wpdb->blogid ) );
                    $orig_url = parse_url( get_original_url( 'siteurl' ) );
                    if( $domain != $orig_url[ 'host' ] ) {
                        $wpdb->query( $wpdb->prepare( "UPDATE {$wpdb->dmtable} SET active = 1 WHERE domain = %s", $domain ) );
                    }
                    wp_redirect( add_query_arg( array( 'updated' => 'primary' ), $url ) );
                    exit;
                    break;
            }
        } else {
            switch ($_GET['action']) {
                case "edit":
                    $row = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->dmtable} WHERE domain = %s", $domain ) );
                    $heading = '';
                    if( empty($row) ) {
                        $heading = "<h3>" . sprintf(__( 'Domain %s not found', 'wordpress-mu-domain-mapping' ), $domain) . "</h3>";
                    }
                    kpi_domain_listing( $row, $heading );
                    break;
                case "del":
                    $wpdb->query( $wpdb->prepare( "DELETE FROM {$wpdb->dmtable} WHERE domain = %s", $domain ) );
                    $heading = "<p><strong>" . sprintf( __( 'Domain %s Deleted', 'wordpress-mu-domain-mapping' ), $domain ) . "</strong></p>";
                    kpi_domain_listing( false, $heading );
                    break;
                case "search":
                    # $rows = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$wpdb->dmtable} WHERE domain LIKE %s", $domain ) );
                    # dm_domain_listing( $rows, sprintf( __( "Searching for %s", 'wordpress-mu-domain-mapping' ), esc_html( $domain ) ) );
                    kpi_domain_listing( $domain );
                    break;
            }
        }
    } else {
        kpi_domain_listing( false );
    }
}

function kpi_domain_listing($row = false, $heading = '') {
    global $wpdb;
    echo '<div class="container"><div class="col-lg-6 col-md-6 col-sm-12">';
    echo "<h3>" . __( 'Search Domains', 'wordpress-mu-domain-mapping' ) . "</h3>";
    echo "<form method='GET'>";
    echo "<input type='hidden' name='page' value='kpi_dm_domains_admin' />".
        '<input type="hidden" name="action" value="search" />';
    wp_nonce_field( 'domain_mapping' );

    echo '<p class="p1">';
    echo _e( "Domain:", 'wordpress-mu-domain-mapping' );
    echo sprintf(' <input type="text" name="domain" value="%s" size="100"/></p>', !empty($_REQUEST['domain']) ? esc_attr($_REQUEST['domain']) : '' );
    echo "<p class='p2'><input type='submit' class='button-secondary' value='" . __( 'Search', 'wordpress-mu-domain-mapping' ) . "' /></p>";
    echo '</form><br /></div><div class="col-lg-6 col-md-6 col-sm-12">';

    kpi_dm_edit_domain( is_string($row) ? $row : $row );

    echo '</div><div class="col-lg-12 col-md-12 col-sm-12">';

    # $paged = isset($_REQUEST['paged']) ? intval($_REQUEST['paged']) : 0;
    # $rows = $wpdb->get_results( "SELECT * FROM {$wpdb->dmtable} ORDER BY id DESC LIMIT 0, 500" );

    $domain = is_string($row) ? $row : '';
    $where_domain = '';
    if( !empty($domain) ) {
        $where_domain = $wpdb->prepare(" AND domain LIKE %s", $domain);
        $heading .= sprintf( __( "Searching for %s", 'wordpress-mu-domain-mapping' ), esc_html( $domain ) );
    }

    $rows = $wpdb->get_results( "SELECT * FROM {$wpdb->dmtable} WHERE 1 = 1 {$where_domain} ORDER BY id DESC LIMIT 0, 500" );
    if( empty($rows) && !empty($domain) ) {
        $heading = sprintf( __('Domain %s not found', 'wordpress-mu-domain-mapping'), $domain );
    }

    kpi_dm_domain_listing( $rows, $heading );
    echo '</div></div>';
    return;
}

function kpi_dm_domain_listing( $rows, $heading = '' ) {
    if ( $heading != '' )
        echo "<h3>$heading</h3>";
    if ( $rows ) {
        if ( file_exists( ABSPATH . 'wp-admin/network/site-info.php' ) ) {
            $edit_url = network_admin_url( 'site-info.php' );
        } elseif ( file_exists( ABSPATH . 'wp-admin/ms-sites.php' ) ) {
            $edit_url = admin_url( 'ms-sites.php' );
        } else {
            $edit_url = admin_url( 'wpmu-blogs.php' );
        }
        echo '<table class="widefat" cellspacing="0"><thead><tr><th>'.__( 'Site ID', 'wordpress-mu-domain-mapping' ).'</th>'.
            '<th>'.__( 'Domain', 'wordpress-mu-domain-mapping' ).'</th><th>'.__( 'Primary', 'wordpress-mu-domain-mapping' ).'</th>'.
            '<th>'.__( 'Edit', 'wordpress-mu-domain-mapping' ).'</th><th>'.__( 'Delete', 'wordpress-mu-domain-mapping' ).'</th></tr></thead><tbody>';

        foreach( $rows as $row ) {
            echo "<tr><td><a href='" . add_query_arg( ['action' => 'editblog', 'id' => $row->blog_id], $edit_url ) . "'>{$row->blog_id}</a></td>".
                "<td><a href='http://{$row->domain}/'>{$row->domain}</a></td><td>";
            echo $row->active == 1 ? __( 'Yes',  'wordpress-mu-domain-mapping' ) : __( 'No',  'wordpress-mu-domain-mapping' );

            echo "</td><td><form method='GET'>".
                "<input type='hidden' name='page' value='kpi_dm_domains_admin' />".
                "<input type='hidden' name='action' value='edit' />".
                "<input type='hidden' name='domain' value='{$row->domain}' />";
            wp_nonce_field( 'domain_mapping' );

            echo "<input type='submit' class='button-secondary' value='" .__( 'Edit', 'wordpress-mu-domain-mapping' ). "' /></form></td>".
                "<td><form method='GET'>".
                "<input type='hidden' name='page' value='kpi_dm_domains_admin' />".
                "<input type='hidden' name='action' value='del' />".
                "<input type='hidden' name='domain' value='{$row->domain}' />";
            wp_nonce_field( 'domain_mapping' );

            echo "<input type='submit' class='button-secondary' value='" .__( 'Del', 'wordpress-mu-domain-mapping' ). "' /></form>";
            echo "</td></tr>";
        }
        echo '</table>';
        if ( get_site_option( 'dm_no_primary_domain' ) == 1 ) {
            echo "<p>" . __( '<strong>Warning!</strong> Primary domains are currently disabled.', 'wordpress-mu-domain-mapping' ) . "</p>";
        }
    }
}

function kpi_dm_edit_domain( $row = false ) {
    if ( is_object( $row ) ) {
        echo "<h3>" . __( 'Edit Domain', 'wordpress-mu-domain-mapping' ) . "</h3>";
    }  else {
        echo "<h3>" . __( 'New Domain', 'wordpress-mu-domain-mapping' ) . "</h3>";
        $row = new stdClass();
        $row->blog_id = '';
        $row->domain = '';
        $_REQUEST[ 'domain' ] = '';
        $row->active = 1;
    }
    if( is_network_admin() ) {
        $submit_url = network_admin_url('admin.php?page=kpi_dm_domains_admin');
    } else {
        $submit_url = admin_url('admin.php?page=kpi_dm_domains_admin');
    }

    echo "<form method='POST' action='".$submit_url."'><input type='hidden' name='action' value='save' />".
        "<input type='hidden' name='orig_domain' value='" . esc_attr( !empty($_REQUEST[ 'domain' ]) ? $_REQUEST[ 'domain' ] : '' ) . "' />";
    wp_nonce_field( 'domain_mapping' );
    echo "<table class='form-table'>\n";
    echo "<tr><th>" . __( 'Site ID', 'wordpress-mu-domain-mapping' ) . "</th><td><input type='text' name='blog_id' value='{$row->blog_id}' size='20' /></td></tr>\n";
    echo "<tr><th>" . __( 'Domain', 'wordpress-mu-domain-mapping' ) . "</th><td><input type='text' name='domain' value='{$row->domain}' size='100' /></td></tr>\n";
    echo "<tr><th>" . __( 'Primary', 'wordpress-mu-domain-mapping' ) . "</th><td><input type='checkbox' name='active' value='1' ";
    echo $row->active == 1 ? 'checked=1 ' : ' ';
    echo "/></td></tr>\n";
    if ( get_site_option( 'dm_no_primary_domain' ) == 1 ) {
        echo "<tr><td colspan='2'>" . __( '<strong>Warning!</strong> Primary domains are currently disabled.', 'wordpress-mu-domain-mapping' ) . "</td></tr>";
    }
    echo "</table>";
    echo "<p><input type='submit' class='button-primary' value='" .__( 'Save', 'wordpress-mu-domain-mapping' ). "' /></p></form><br /><br />";
}

function kpi_dm_handle_actions() {
    global $wpdb, $parent_file;
    $url = add_query_arg( array( 'page' => 'kpi_dm_domains_admin' ), admin_url( $parent_file ) );
    if ( !empty( $_POST[ 'action' ] ) ) {
        $domain = $wpdb->escape( $_POST[ 'domain' ] );
        if ( $domain == '' ) {
            wp_die( "You must enter a domain", 'wordpress-mu-domain-mapping' );
        }
        check_admin_referer( 'domain_mapping' );
        do_action('dm_handle_actions_init', $domain);
        switch( $_POST[ 'action' ] ) {
            case "add":
                do_action('dm_handle_actions_add', $domain);
                if( null == $wpdb->get_row( "SELECT blog_id FROM {$wpdb->blogs} WHERE domain = '$domain'" ) && null == $wpdb->get_row( "SELECT blog_id FROM {$wpdb->dmtable} WHERE domain = '$domain'" ) ) {
                    if ( $_POST[ 'primary' ] ) {
                        $wpdb->query( $wpdb->prepare( "UPDATE {$wpdb->dmtable} SET active = 0 WHERE blog_id = %d", $wpdb->blogid ) );
                    }
                    $wpdb->query( $wpdb->prepare( "INSERT INTO {$wpdb->dmtable} ( `id` , `blog_id` , `domain` , `active` ) VALUES ( NULL, %d, %s, %d )", $wpdb->blogid, $domain, $_POST[ 'primary' ] ) );
                    wp_redirect( add_query_arg( array( 'updated' => 'add' ), $url ) );
                    exit;
                } else {
                    wp_redirect( add_query_arg( array( 'updated' => 'exists' ), $url ) );
                    exit;
                }
                break;
            case "primary":
                do_action('dm_handle_actions_primary', $domain);
                $wpdb->query( $wpdb->prepare( "UPDATE {$wpdb->dmtable} SET active = 0 WHERE blog_id = %d", $wpdb->blogid ) );
                $orig_url = parse_url( get_original_url( 'siteurl' ) );
                if( $domain != $orig_url[ 'host' ] ) {
                    $wpdb->query( $wpdb->prepare( "UPDATE {$wpdb->dmtable} SET active = 1 WHERE domain = %s", $domain ) );
                }
                wp_redirect( add_query_arg( array( 'updated' => 'primary' ), $url ) );
                exit;
                break;
        }
    } elseif( !empty($_GET[ 'action' ]) && ($_GET[ 'action' ] == 'delete') ) {
        $domain = $wpdb->escape( $_GET[ 'domain' ] );
        if ( $domain == '' ) {
            wp_die( __( "You must enter a domain", 'wordpress-mu-domain-mapping' ) );
        }
        check_admin_referer( "delete" . $_GET['domain'] );
        do_action('dm_handle_actions_del', $domain);
        $wpdb->query( "DELETE FROM {$wpdb->dmtable} WHERE domain = '$domain'" );
        wp_redirect( add_query_arg( array( 'updated' => 'del' ), $url ) );
        exit;
    }
}