<?php

/*
 *  Load scripts
 */
function lik_scripts() {

    $version = '1.0.0';

    // Load CSS
    //-- CSS HTML here

    /*---Theme Styles----*/
    /*-- BEGIN GLOBAL MANDATORY STYLES --*/
    wp_enqueue_style('font-awesome-css', THEME_URL . '/assets/global/plugins/font-awesome/css/font-awesome.min.css', array(), $version, 'screen');
    wp_enqueue_style('editor-style-css', THEME_URL . '/editor-style.css', array(), $version, 'screen');
    wp_enqueue_style('google-font-css','//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all', array(), $version, 'screen');
    wp_enqueue_style('simple-line-icons-css', THEME_URL . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css', array(), $version, 'screen');
    wp_enqueue_style('bootstrap-css', THEME_URL . '/assets/global/plugins/bootstrap/css/bootstrap.min.css', array(), $version, 'screen');
    wp_enqueue_style('bootstrap-switch-css', THEME_URL . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css', array(), $version, 'screen');
    /*-- END GLOBAL MANDATORY STYLES --*/
    
    /*-- BEGIN THEME GLOBAL STYLES --*/
    wp_enqueue_style('global-components-css', THEME_URL . '/assets/global/css/components.min.css', array(), $version, 'screen');
    wp_enqueue_style('global-plugins-css', THEME_URL . '/assets/global/css/plugins.min.css', array(), $version, 'screen');
    /*-- END THEME GLOBAL STYLES --*/

    /*-- BEGIN PAGE LEVEL STYLES --*/
    wp_enqueue_style('login-4-css', THEME_URL . '/assets/pages/css/login-4.css', array(), $version, 'screen');
    /*-- END PAGE LEVEL STYLES --*/




    // Load JS

    /*-- BEGIN CORE PLUGINS --*/
    wp_enqueue_script('plugins-bootstrap-js', THEME_URL . '/assets/global/plugins/bootstrap/js/bootstrap.min.js', array('jquery'), $version, true);
    wp_enqueue_script('plugins-cookie-js', THEME_URL . '/assets/global/plugins/js.cookie.min.js', array('jquery'), $version, true);
    wp_enqueue_script('plugins-slimscroll-js', THEME_URL . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js', array('jquery'), $version, true);
    wp_enqueue_script('plugins-blockui-js', THEME_URL . '/assets/global/plugins/jquery.blockui.min.js', array('jquery'), $version, true);
    wp_enqueue_script('plugins-bootstrap-switch-js', THEME_URL . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js', array('jquery'), $version, true);
    /*-- END CORE PLUGINS --*/

    /*-- BEGIN PAGE LEVEL PLUGINS --*/
    wp_enqueue_script('plugins-jquery-validate-js', THEME_URL . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js', array('jquery'), $version, true);
    wp_enqueue_script('plugins-additional-methods-js', THEME_URL . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js', array('jquery'), $version, true);
    wp_enqueue_script('plugins-backstretch-js', THEME_URL . '/assets/global/plugins/backstretch/jquery.backstretch.min.js', array('jquery'), $version, true);
    /*-- END PAGE LEVEL PLUGINS --*/
    
    /*-- BEGIN THEME GLOBAL SCRIPTS --*/
    wp_enqueue_script('global-scripts-app-js', THEME_URL . '/assets/global/scripts/app.js', array('jquery'), $version, true);
    /*-- END THEME GLOBAL SCRIPTS --*/
    
    /*-- BEGIN PAGE LEVEL SCRIPTS --*/
    wp_enqueue_script('pages-login-js', THEME_URL . '/assets/pages/scripts/login-4.js', array('jquery'), $version, true);
    /*-- END PAGE LEVEL SCRIPTS --*/
    
    

}

add_action('wp_enqueue_scripts', 'lik_scripts');

if( !is_admin() ) {
    show_admin_bar( false );
}


