<?php
/**
 * Template Name: Exports
 * Created by PhpStorm.
 * User: henry
 * Date: 12/27/17
 * Time: 14:29
 */


include_once __DIR__ . '/../brainmark-kpi-main/login.php';
kpi_init_chart_and_year();
$user = wp_get_current_user();
#$orgchart = user_load_orgchart( $user );
if ( !is_user_logged_in() ) {
    get_header('login');
    get_template_part( 'contents/content', 'login' );
    get_footer('login');
} else {
    require_once THEME_DIR . '/inc/phpToPDF.php';
    require_once THEME_DIR . '/inc/lib-settings.php';
    require_once THEME_DIR . '/inc/lib-kpis.php';
    require_once THEME_DIR . '/inc/lib-years.php';
    require_once THEME_DIR . '/inc/lib-users.php';
    require_once THEME_DIR . '/inc/lib-formulas.php';

    $website_settings = get_settings_website();
    $logo_url = !empty( $website_settings['logo_url'] ) ? $website_settings['logo_url'] : THEME_URL . '/assets/images/logo-brainmark.png';
    $member_role = isset($GLOBALS['member_role']) ? $GLOBALS['member_role'] : '';

    if( isset( $_GET['type'] ) && $_GET['type'] == 'kpi-nam' ){
	    get_template_part( 'contents/exports/phieu-danh-gia-kpi-capacity', $member_role );
    }else {
	    get_template_part( 'contents/exports/phieu-danh-gia-kpi', $member_role );
    }
    # require_once THEME_DIR . '/contents/exports/phieu-danh-gia-kpi-quan-ly.php';
}