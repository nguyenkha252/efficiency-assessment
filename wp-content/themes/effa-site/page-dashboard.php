<?php
/**
 * Template Name: Dashboard
 */


include_once __DIR__ . '/../brainmark-kpi-main/login.php';

if ( !is_user_logged_in() ) {
    get_header('login');
    get_template_part( 'contents/content', 'login' );
    get_footer('login');
} else {
    require_once THEME_DIR . '/ajax/get_post_info.php';
    $wpnonce_post_info = wp_create_nonce('post_info');
    get_header();
    if( !IS_AJAX ) {
        # require_once THEME_DIR . '/ajax/get_post_info.php';
        # $wpnonce_post_info = wp_create_nonce( 'post_info' );
        # @TODO begin add tempalate
        require_once ABSPATH . '_files/dashboard/content-left.php';
    }

    require_once ABSPATH . '_files/dashboard/content.php';

    get_footer();
}


