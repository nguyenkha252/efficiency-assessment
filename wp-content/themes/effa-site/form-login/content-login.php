<?php
$redirect_to = site_url();
?>
    <body class="login">
    <!-- BEGIN LOGO -->
    <div class="logo">
        <a href="<?php echo $redirect_to; ?>">
            <img src="<?php echo THEME_URL; ?>/assets/images/logo-brainmark.png" alt="" />
        </a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN LOGIN FORM -->
        <form class="login-form" action="<?php echo site_url(); ?>" method="POST" enctype="application/x-www-form-urlencoded">
            <h3 class="form-title"><?php _e('Đăng nhập', TPL_DOMAIN_LANG); # ?></h3>
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                <span><?php _e('Vui lòng nhập tài khoản hay mật khẩu của bạn.', TPL_DOMAIN_LANG); ?></span>
            </div>
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <div class="input-icon">
                    <i class="fa fa-user"></i>
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="<?php _e('Tài khoản hoặc email', TPL_DOMAIN_LANG); ?>" id="user_login" name="user_login" /> </div>
            </div>
            <div class="form-group">
                <div class="input-icon">
                    <i class="fa fa-lock"></i>
                    <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="<?php _e('Mật khẩu', TPL_DOMAIN_LANG); ?>" id="user_password" name="user_password" /> </div>
            </div>
            <div class="form-group">
                <label class="rememberme mt-checkbox mt-checkbox-outline">
                    <label class="switch">
                        <input class="form-control-checkbox checkbox-status" value="forever" <?php isset($rememberme) ? checked( $rememberme ) : ''; ?> id="rememberme" name="remember" type="checkbox">
                        <span class="checkbox-slider round fa fas"></span>
                        <span><?php _e( 'Ghi nhớ?', TPL_DOMAIN_LANG ); ?></span>
                    </label>
                </label>
                <button type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large"><?php _e('Đăng nhập', TPL_DOMAIN_LANG); ?></button>
            </div>



            <input type="hidden" name="redirect_to" value="<?php echo esc_attr($redirect_to); ?>" />
        </form>
        <!-- END LOGIN FORM -->
    </div>
    <!-- END LOGIN -->
    <!-- BEGIN COPYRIGHT -->
    <div class="copyright"> 2018 &copy; <?php
        if( is_multisite() ){
            $blogID =  get_current_blog_id();
            $blogDetailByID = get_blog_details($blogID);
            echo $blogDetailByID->blogname;
        }else{
            echo get_bloginfo('name');
        }
        ?>. </div>
    <!-- END COPYRIGHT -->
    <!--[if lt IE 9]>
    <script src="<?php echo THEME_URL; ?>/assets/global/plugins/respond.min.js"></script>
    <script src="<?php echo THEME_URL; ?>/assets/global/plugins/excanvas.min.js"></script>
    <script src="<?php echo THEME_URL; ?>/assets/global/plugins/ie8.fix.min.js"></script>
    <![endif]-->
    <?php wp_footer(); ?>

    </body>
<?php
