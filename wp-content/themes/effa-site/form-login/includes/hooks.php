<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/2/18
 * Time: 3:51 PM
 */

/*
 *  Change login logo & url in WP Admin
 */
function site_custom_login_logo() {
    echo "<style type='text/css'>
body.login {
    background: -webkit-linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* Chrome 10+, Saf5.1+ */
    background: -moz-linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* FF3.6+ */
    background: -ms-linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* IE10 */
    background: -o-linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* Opera 11.10+ */
    background: linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* W3C */
}

.login h1 a {
    background-image: url('" . THEME_URL . "/images/login-logo.png');
    background-size: 280px 78px;
    width: 280px;
    height: 78px;
}

.login form {
    margin-left: auto;
    margin-right: auto;
    padding: 30px;
    border: 1px solid rgba(0, 0, 0, .2);
    background-clip: padding-box;
    background: rgba(255, 255, 255, 0.9);
    box-shadow: 0 0 13px 3px rgba(0, 0, 0, .5);
    overflow: hidden;
}

.login label {
    color: #333;
}

#backtoblog, #nav {
    display: none;
}
</style>";
}

add_action('login_head', 'site_custom_login_logo');

function site_custom_login_logo_url($url) {
    return site_url("/");
}

add_filter('login_headerurl', 'site_custom_login_logo_url');

function site_custom_login_logo_title() {
    return get_bloginfo('name');
}

add_filter('login_headertitle', 'site_custom_login_logo_title');