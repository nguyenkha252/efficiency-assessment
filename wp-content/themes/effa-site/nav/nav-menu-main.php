<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/2/18
 * Time: 10:32 PM
 */

/**
 * Customize render navigation
 */
function top_nav_menu_css_class( $classes, $item, $args, $depth ){
    $classes[] = 'nav-item';
    if( in_array( 'menu-item-has-children', $classes ) ){
        $classes[] = 'hs-has-sub-menu u-header__nav-item';
    }
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}
function top_nav_menu_submenu_css_class($classes, $args, $depth){
    $classes[] = 'list-inline hs-sub-menu u-header__sub-menu py-3 mb-0';
    return $classes;
}
function top_nav_menu_item_title( $title, $item, $args, $depth ){
    if( array_key_exists( 'classes', $item ) && in_array( 'menu-item-has-children', $item->classes ) ){
        if( $depth == 0 ) {
            $title .= " <span class=\"fa fa-angle-down u-header__nav-link-icon\"></span>";
        }elseif( $depth == 1 ){
            $title .= " <span class=\"fa fa-angle-right u-header__nav-link-icon\"></span>";
        }
    }
    return $title;
}
function top_nav_menu_link_attributes($atts, $item, $args, $depth){
    if( isset($atts['class']) ){
        $atts['class'] .= ' nav-link u-header__nav-link';
    }else{
        $atts['class'] = 'nav-link u-header__nav-link';
    }
    if( !empty($item->menu_item_parent) ){
        $atts['class'] = 'nav-link u-header__sub-menu-nav-link u-list__link';
    }
    if( in_array( 'd-none', $item->classes ) && in_array( 'd-md-inline-block', $item->classes ) ){
        $atts['class'] = "btn btn-sm btn-try-drive btn-primary u-btn-primary transition-3d-hover";
    }
    return $atts;
}
add_filter( 'nav_menu_css_class', 'top_nav_menu_css_class',10,4);
add_filter( 'nav_menu_submenu_css_class', 'top_nav_menu_submenu_css_class',10,3);
add_filter( 'nav_menu_item_title', 'top_nav_menu_item_title',10, 4);
add_filter('nav_menu_link_attributes', 'top_nav_menu_link_attributes', 10, 4);
$menu = wp_nav_menu( array(
    'menu' => 'Main Menu',
    'fall_back'     => 'wp_page_menu',
    'items_wrap'    => '<ul class="navbar-nav mr-auto nav-tabs nav-justified navbar-main">%3$s</ul>',
    'depth'         => 0,
    'echo'          => false,
    'container_class'    => 'collapse navbar-collapse',
    'container_id' => 'navMainMenu'
) );

if( !empty($menu) ){
    echo "
        <div class=\"clearfix\"></div>
        <nav class=\"navbar navbar-expand-lg navbar-light navigation-menu-main\">
              <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navMainMenu\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                <span class=\"navbar-toggler-icon\"></span>
              </button>
            {$menu}
        </nav>
            ";


}

remove_filter('nav_menu_css_class', 'top_nav_menu_css_class');
remove_filter( 'nav_menu_submenu_css_class', 'top_nav_menu_submenu_css_class');
remove_filter( 'nav_menu_item_title', 'top_nav_menu_item_title');
remove_filter('nav_menu_link_attributes', 'top_nav_menu_link_attributes');
