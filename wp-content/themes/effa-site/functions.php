<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/2/18
 * Time: 10:48 AM
 */

if (!defined('TPL_DOMAIN_LANG'))
    define('TPL_DOMAIN_LANG', 'company');
if (!defined('THEME_DIR'))
    define('THEME_DIR', get_template_directory());
if (!defined('THEME_URL'))
    define('THEME_URL', get_template_directory_uri());

include_once THEME_DIR . '/includes/defined.php';
include_once THEME_DIR . '/includes/helpers.php';

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */

if( !function_exists('effa_site_setup') ){
    function effa_site_setup() {
        // This theme uses wp_nav_menu() in for locations.
        register_nav_menus( array(
            'primary' => __( 'Menu Main', 'site' ),
            'primary_mobile' => __( 'Menu Mobile', 'site' ),
            'primary_menu_footer' => __( 'Menu Footer', 'site' ),
            'primary_menu_left' => __( 'Menu Left', 'site' ),
            'social'  => __( 'Social Links Menu', 'site' ),
        ) );
        $domain = TPL_DOMAIN_LANG;
        $locale = apply_filters( 'theme_locale', is_admin() ? get_user_locale() : get_locale(), $domain );
        $path = THEME_DIR . '/languages';
        load_theme_textdomain( TPL_DOMAIN_LANG, $path );
        return load_textdomain( $domain, $path . '/' . $locale . '.mo' );
    }
}
add_action( 'after_setup_theme', 'effa_site_setup' );

/**
 * Enqueue scripts and styles.
 */
function effa_site_scripts() {
    $version = "1.0.0.1";
    #css
    wp_enqueue_style( 'site-style', get_stylesheet_uri() );
    wp_enqueue_style( 'bootstrap.min', THEME_URL . '/assets/libraries/bootstrap/css/bootstrap.css', [], '4.0.0' );
    wp_enqueue_style( 'bootstrap-datetimepicker', THEME_URL . '/assets/libraries/bootstrap/css/bootstrap-datetimepicker.css', [], '4.15.35' );
    wp_enqueue_style( 'font-awesome', THEME_URL . '/assets/libraries/font-awesome/css/fontawesome-all.min.css', [], '5.0' );
    wp_enqueue_style('simple-line-icons-css', THEME_URL . '/assets/libraries/simple-line-icons/simple-line-icons.min.css', array(), $version, 'screen');
    #javascript
    wp_enqueue_script( 'jquery.validate', THEME_URL . '/assets/libraries/jquery/js/jquery.validate.min.js', array( 'jquery' ), '2.1.2', true );
    wp_enqueue_script( 'popper', THEME_URL . '/assets/libraries/bootstrap/js/popper.min.js', array( 'jquery' ), '2.1.2', true );
    wp_enqueue_script( 'bootstrap', THEME_URL . '/assets/libraries/bootstrap/js/bootstrap.min.js', array( 'popper' ), '4.0.0', true );
    wp_enqueue_script( 'moment', THEME_URL . '/assets/libraries/bootstrap/js/moment-with-locales.js', array( 'bootstrap' ), '2.1.2', true );
    wp_enqueue_script( 'bootstrap-datetimepicker', THEME_URL . '/assets/libraries/bootstrap/js/bootstrap-datetimepicker.js', array( 'bootstrap', 'moment' ), '2.1.2', true );
    wp_enqueue_script( 'jquery.easing.1.3', THEME_URL . '/assets/libraries/jquery/js/jquery.easing.1.3.js', array( 'jquery' ), '2.1.2', true );
    wp_enqueue_script( 'jquery.material.form.min', THEME_URL . '/assets/libraries/jquery/js/jquery.material.form.min.js', array( 'jquery' ), '2.1.2', true );
    wp_enqueue_script( 'jquery-ui', THEME_URL . '/assets/libraries/jquery/js/jquery.ui.min.js', array('jquery', 'bootstrap'), '1.12.1', true );
    wp_enqueue_script( 'jquery-blockui', THEME_URL . '/assets/libraries/jquery/js/jquery.blockui.js', array('jquery-ui'), '1.0', true );
    if( is_user_logged_in() ){
        wp_enqueue_style( 'main-styles', THEME_URL . '/assets/css/styles.css', [], $version );
    }
}

add_action( 'wp_enqueue_scripts', 'effa_site_scripts' );

function user_is_manager($user = null) {
    if ( empty( $user ) ) {
        $user = wp_get_current_user();
    }
    if( !empty($user->allcaps['level_9']) || $user->has_cap('level_9') ){
        return true;
    }
    return false;

}

function user_load_orgchart(WP_User $user) {
    global $wpdb;
    if( !$user->__isset('orgchart') ) {
        $orgchart_id = $user->__get('orgchart_id');
        $orgchart = effa_orgchart_get_by('id', $orgchart_id, OBJECT);
        $user->__set('orgchart', $orgchart);
    }
    return $user->__get('orgchart');
}
function effa_get_list_org_charts($parentId = 0, $getThis = false) {
    static $results, $wpdb, $tree, $items;

    if( !isset($tree) ) {
        $tree = effa_load_org_charts(true);
    }
    $__key = md5( serialize( [$parentId, $getThis] ) );

    if( !isset($results[$__key]) ) {
        $items = effa_loop_get_children( $parentId, $tree, $getThis );
        $results[$__key] = [];

        if( !array_key_exists('children', $items) ) {
            foreach ($items as $id => &$item) {
                if( isset($item['id']) ) {
                    $results[$__key][$item['id']] = $item;
                }
            }
        } else {
            $results[$__key][$items['id']] = $items;
            unset($results[$__key][$items['id']]['children']);
            foreach ($items['children'] as $id => &$item) {
                if( isset($item['id']) ) {
                    $results[$__key][$item['id']] = $item;
                }
            }
        }
    }
    return $results[$__key];
}

function effa_loop_get_children($parentId, &$node, $getThis = false) {
    $id = isset($node['id']) ? $node['id'] : '';
    if( $parentId == $id ) {
        $children = [];
        if( array_key_exists('children', $node) ) {
            $children = $node['children'];
        }
        return $getThis ? $node : $children;
    } else {
        if( array_key_exists('children', $node) ) {
            foreach($node['children'] as $sub) {
                $result = effa_loop_get_children($parentId, $sub, $getThis);
                if( !empty($result) ) {
                    return $result;
                }
            }
        }
    }
    return [];
}

function effa_load_org_charts($isTree = true) {
    global $wpdb;
    static $__results = [];

    $key = is_bool($isTree) || is_numeric($isTree) ? ( (bool)$isTree ? '1' : '0') : 'query';

    if( !isset($__results[$key]) ) {
        if( !isset($__results['query']) ) {
            $blog_id = get_current_blog_id();
            $prefix = $wpdb->get_blog_prefix( $blog_id );
            $tableChart = "{$prefix}org_charts";
            $results = $wpdb->get_results("SELECT c.`id`, c.`name`, c.`parent`, c.`parent` as parentId, c.`level`, ".
                "(CASE ".
                "   WHEN (c.`alias` LIKE 'tapdoan') THEN 'bgd' ".
                "   WHEN (c.`alias` LIKE 'congty') THEN 'bgd' ".
                "   WHEN (c.`alias` LIKE 'phongban') THEN 'phongban' ".
                "   WHEN (c.`alias` LIKE 'nhanvien') THEN 'nhanvien' ".
                "END) as `role`, ".
                "c.`has_kpi`, c.`alias`, c.`room` ".
                "FROM {$tableChart} as c WHERE 1 = 1 ".
                "ORDER BY c.`parent` ASC, c.`id` ASC", ARRAY_A);

            # echo '<pre>'; var_dump(__LINE__, __FILE__, $wpdb->last_error, $wpdb->last_query); echo '</pre>';

            $__results['query'] = [];
            if( !empty($results) ) {
                foreach ($results as $id => $item) {
                    $results[$id]['id'] = (int)$item['id'];
                    $results[$id]['parent'] = (int)$item['parent'];
                    $results[$id]['parentId'] = (int)$item['parentId'];
                    $results[$id]['level'] = (int)$item['level'];
                    $results[$id]['deep'] = (int)$item['level'];
                    $results[$id]['has_kpi'] = (int)$item['has_kpi'];
                    $results[$id]['alias'] = (string)$item['alias'];
                    $results[$id]['alias_text'] = (string)$item['alias'];
                    if( !empty($results[$id]['alias']) && isset($GLOBALS['charts_alias'][$results[$id]['alias']]) ) {
                        $results[$id]['alias_text'] = $GLOBALS['charts_alias'][$results[$id]['alias']];
                    }
                    $__results['query'][$item['id']] = $results[$id];
                }
            }
        }
        $__results['1'] = $__results['0'] = $__results['query'];
        if( !empty($__results['query']) ) {
            if( $isTree ) {
                $__results['1'] = effa_make_tree($__results['query']);
            }
        }
    }
    return $__results[$key];
}

function effa_make_tree($results) {
    static $root;
    if( !isset($root) ) {
        $lists = [];
        foreach($results as $item) {
            $item['children'] = [];
            $lists[$item['id']] = $item;
        }

        foreach($lists as $id => $item) {
            if( $item['parentId'] == 0 ) {
                $root = $item;
                unset($lists[$id]);
                break;
            }
        }

        if( !is_null($root) ) {
            foreach($lists as $id => $item) {
                if( $item['parentId'] == $root['id'] ) {
                    $root['children'][] = $item;
                    unset($lists[$id]);
                }
            }
            $root['children'] = effa_make_tree_node($root['children'], $lists);
            $root['is_leaf'] = empty($root['children']);
        }
        $root = effa_make_tree_level($root, 0);
    }
    return $root;
}

function effa_make_tree_level(&$root, $level=0) {
    $root['level'] = $level;
    if( !empty($root['children']) ) {
        foreach ($root['children'] as $id => &$child) {
            $root['children'][$id] = effa_make_tree_level($child, $level + 1);
        }
    }
    return $root;
}

function effa_make_tree_node(&$items, &$lists) {
    if( !empty($lists) ) {
        foreach($items as $parentId => &$child) {
            if( !empty($lists) ) {
                foreach($lists as $id => $item) {
                    if( $child['id'] == $item['parentId'] ) {
                        $child['children'][] = $item;
                        unset($lists[$id]);
                    }
                }
            }
            $items[$parentId]['children'] = effa_make_tree_node($child['children'], $lists);
            $items[$parentId]['is_leaf'] = empty($items[$parentId]['children']);
        }
    }
    return $items;
}

function effa_get_profile_url($profile_id) {
    static $profile;
    if( !isset($profile) ) {
        $profile = get_page_by_path( PAGE_PROFILE_PATH );
    }
    $url = apply_filters( 'the_permalink', get_permalink( $profile ), $profile );
    $url = add_query_arg( 'profile', $profile_id, $url );
    return $url;
}

function get_user_orgchart_dropdown($selected_id = '', $echo = true, $types = ['', CHART_ALIAS_GROUP, CHART_ALIAS_MANAGER, CHART_ALIAS_DEPARTMENTS, CHART_ALIAS_EMPLOYEES], $field = 'name', $allow_chart_ids = [], $is_array = false ) {
    require_once THEME_DIR . '/inc/lib-orgchart.php';
    $charts = effa_load_org_charts(true);
    $html = effa_loop_render_option($selected_id, $charts, '', $types, $field, $allow_chart_ids, $is_array );
    if( $echo && !$is_array ) {
        echo $html;
        return;
    }
    return $html;
}

function effa_loop_render_option( $selected_id, $charts, $level = '', $types = [], $field = 'name', $allow_chart_ids = [], $is_array = false ) {
    $html = $is_array ? [] : '';
    if( is_array($field) ) {
        foreach ($field as $idx => $name) {
            if( !in_array($name, ['name', 'room', 'alias']) ) {
                $field[$idx] = 'name';
            }
        }
    } else {
        if( !in_array($field, ['name', 'room', 'alias']) ) {
            $field = 'name';
        }
        $field = [$field];
    }

    if( is_array($charts) ) {
        if( in_array($charts['alias'], $types) ) {
            if( empty($allow_chart_ids) || (in_array($charts['id'], $allow_chart_ids)) ) {
                $json = esc_json_attr($charts);
                $texts = [];
                foreach ($field as $name) {
                    $text = apply_filters('kpi_loop_render_option_text', $charts[$name], $charts);
                    if( !empty($text) ) {
                        $texts[] = $text;
                    }

                }
                $texts = apply_filters('kpi_loop_render_option', $texts, $charts, $field);
                if( !empty($texts) ) {
                    $texts = implode(' - ', $texts);
                    if( $is_array ) {
                        $item = $charts;
                        if( array_key_exists('children', $item) ) {
                            unset($item['children']);
                        }
                        $html[$item['id']] = [
                            'text' => sprintf('<option data-type="%s" data-level="%d" value="%d" %s data-info="%s" >%s</option>' . "\n",
                                $item['alias'], $item['level'], $item['id'], ( ( ($item['id'] == $selected_id) ) ? 'selected="selected"' : ''), $json, $level . $texts),
                            'item' => $item
                        ];
                    } else {
                        $html .= sprintf('<option data-type="%s" data-level="%d" value="%d" %s data-info="%s" >%s</option>' . "\n",
                            $charts['alias'], $charts['level'], $charts['id'], (( ($charts['id'] == $selected_id)) ? 'selected="selected"' : ''), $json, $level . $texts);
                    }
                }
            }
        }
        if( array_key_exists('children', $charts) ) {
            foreach($charts['children'] as $k => $chart) {
                if( $is_array ) {
                    $html = array_merge($html, effa_loop_render_option($selected_id, $chart, "{$level}--- ", $types, $field, $allow_chart_ids, $is_array) );
                } else {
                    $html .= effa_loop_render_option($selected_id, $chart, "{$level}--- ", $types, $field, $allow_chart_ids, $is_array);
                }
            }
        }
    }
    return $html;
}

