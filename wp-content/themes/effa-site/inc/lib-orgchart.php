<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/29/17
 * Time: 16:47
 */

/**
 * @param $field
 * @param $value
 * @return object|array|null
 */
# effa_orgchart_get_by('', $orgchart_id
function effa_orgchart_get_chart_parent_of($chart_id) {
    $chart = effa_orgchart_get_by('id', $chart_id, ARRAY_A);
    if( !empty($chart) ) {
        return effa_orgchart_get_by('id', $chart['parent'], ARRAY_A);
    }
    return null;
}

function effa_orgchart_get_by($field, $value, $returnFormat = OBJECT) {
    global $wpdb;
    static $results = [];
    $__key = "{$field}_{$value}_{$returnFormat}";
    if (!isset($results[$__key])) {

        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableChart = "{$prefix}org_charts";
        $format = "`c.{$field}` LIKE %s";
        if ($field == 'id' || $field == 'parent') {
            $format = "c.`{$field}` = %d";
        }


        $chart = $wpdb->get_row($wpdb->prepare("SELECT c.*, " .
            "(CASE " .
            "   WHEN (c.`alias` LIKE 'tapdoan') THEN 'bgd' " .
            "   WHEN (c.`alias` LIKE 'congty') THEN 'bgd' " .
            "   WHEN (c.`alias` LIKE 'phongban') THEN 'phongban' " .
            "   WHEN (c.`alias` LIKE 'nhanvien') THEN 'nhanvien' " .
            "END) as `role` " .
            "FROM `{$tableChart}` as c WHERE {$format} LIMIT 0, 1", $value), $returnFormat);
        $results[$__key] = $chart;
    }
    return $results[$__key];
}


function effa_get_user_by_chart_id($chart_id, $returnFormat = OBJECT) {
    global $wpdb;
    static $users = [];
    if (!isset($users[$chart_id])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableUser = "{$prefix}users";
        $users[$chart_id] = $wpdb->get_row($wpdb->prepare("SELECT * FROM `{$tableUser}` WHERE orgchart_id = %d", $chart_id), $returnFormat);
        if( !empty($users[$chart_id]) ) {
            $user = new WP_User();
            $user->init( $users[$chart_id] );
            $users[$chart_id] = $user;
        }
    }
    return $users[$chart_id];
}

function process_children($roots) {
    $roots = array_values($roots);
    foreach ($roots as $id => $item) {
        $roots[$id]['children'] = process_children($roots[$id]['children']);
    }
    return $roots;
}

function effa_orgchart_get_list($parent = -1) {
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableChart = "{$prefix}org_charts";
    if( $parent > -1 ) {
        $parent = $wpdb->prepare(" AND (`parent` = %d)", $parent);
    } else {
        $parent = "";
    }
    $charts = $wpdb->get_results("SELECT * FROM `{$tableChart}` WHERE 1 = 1 {$parent} ORDER BY `parent` ASC, `id` ASC, `name` DESC LIMIT 0, 10000", ARRAY_A );

    $root = [];
    if( !empty($charts) ) {
        $results = [];
        foreach($charts as $chart) {
            if( empty($chart['parent']) ) {
                $chart['parent'] = 0;
            }
            $chart['rowspan'] = 1;
            $chart['children'] = [];
            $results[$chart['id']] = $chart;
        }

        foreach($results as $id => &$child) {
            if( $child['parent'] == 0 ) {
                $root[$id] = &$results[$id];
            } else {
                $results[$child['parent']]['children'][$id] = &$results[$id];
            }
        }

        foreach($root as $id1 => &$child1) {

            foreach($child1['children'] as $id2 => &$child2) {
                $root[$id1]['children'][$id2]['rowspan'] = max(count($child2['children']), 1);
                $root[$id1]['rowspan'] += $root[$id1]['children'][$id2]['rowspan'];
                $root[$id1]['children'][$id2]['rowspan'] = max($root[$id1]['children'][$id2]['rowspan'], 1);
            }
            $root[$id1]['rowspan'] = max($root[$id1]['rowspan'], 1);
        }
    }
    return $root;
}

/**
 * @param array $orgchart
 * @return bool|int|WP_Error
 */
function effa_orgchart_insert($orgchart) {
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableChart = "{$prefix}org_charts";

    $upload = wp_upload_dir();
    if( isset($_FILES['orgchart']) && !empty($_FILES['orgchart']) ) {
        $ICON_FILE = [
            'type'      => $_FILES['orgchart']['type']['icon'],
            'error'     => $_FILES['orgchart']['error']['icon'],
            'size'      => $_FILES['orgchart']['size']['icon'],
            'name'      => $_FILES['orgchart']['name']['icon'],
            'tmp_name'  => $_FILES['orgchart']['tmp_name']['icon'],
        ];
        $key = sanitize_title($orgchart['name']);
        if( $ICON_FILE['error'] == UPLOAD_ERR_OK && $ICON_FILE['size'] > 0 ) {
            $key = strtolower( "{$orgchart['role']}/{$key}" );
            $image_icon = sanitize_title($ICON_FILE['name']);
            $image_icon = preg_replace('#\-(jpeg|png|jpg|gif)$#', '.$1', $image_icon);
            $icon_file = "{$upload['basedir']}/icons/{$key}/{$image_icon}";
            $icon_url = "{$upload['baseurl']}/icons/{$key}/{$image_icon}";

            if( !is_dir("{$upload['basedir']}/icons/{$key}") ) {
                mkdir("{$upload['basedir']}/icons/{$key}", 0777, true);
            }
            error_log($icon_url);
            error_log($icon_file);
            if( move_uploaded_file($ICON_FILE['tmp_name'], $icon_file) ) {
                $orgchart['icon'] = $icon_url;
            }

            if( !empty($orgchart['id']) ) {
                $chart = effa_orgchart_get_by('id', $orgchart['id']);
                if( !empty($chart) ) {
                    if( !empty($chart->icon) ) {
                        $old_url = $upload['basedir'] . str_replace($upload['baseurl'], '', $chart->icon);
                        if( file_exists($old_url) ) {
                            unlink($old_url);
                        }
                    }
                }
            }
        }
    }
    if( isset($orgchart['id']) && !empty($orgchart['id']) ) {
        $id = $orgchart['id'];
        unset($orgchart['id']);
        $result = $wpdb->update($tableChart, $orgchart, ['id' => $id]);
        # send_response_json($result, 400, __("Debug", TPL_DOMAIN_LANG));
        if( is_numeric($result) ) {
            return $id;
        }
    } else {
        if( $wpdb->insert($tableChart, $orgchart) ) {
            return $wpdb->insert_id;
        }
    }
    if( !empty($wpdb->last_error) ) {
        return new WP_Error(499, $wpdb->last_error);
    }
    return false;
}

function effa_orgchart_remove($id) {
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableChart = "{$prefix}org_charts";

    $upload = wp_upload_dir();
    $chart = effa_orgchart_get_by('id', $id);
    if( !empty($chart) ) {
        if( !empty($chart->icon) ) {
            $old_url = $upload['basedir'] . str_replace($upload['baseurl'], '', $chart->icon);
            if( file_exists($old_url) ) {
                unlink($old_url);
            }
        }
        if( !empty($id) ) {
            if( $wpdb->delete($tableChart, ['id' => $id]) ) {
                return $id;
            }
            if( !empty($wpdb->last_error) ) {
                return new WP_Error(499, $wpdb->last_error);
            }
        }
    }
    return false;
}

function get_orchart_children_by_id( $id ){
    global $wpdb;
    static $results = [];
    if( !isset($results[$id]) ) {
        $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
        $tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("SELECT * FROM {$tableChart} AS org WHERE org.parent = %d", $id);
        $results[$id] = $wpdb->get_results( $sql );
    }
    return $results[$id];
}

function check_user_is_node_end( $user_id = 0 ){
    if( $user_id == 0 ) {
        $user = wp_get_current_user();
        $orgchart = user_load_orgchart($user);
    }else{
        $user = get_user_by('ID', $user_id);
        if( !empty( $user )  ){
            $orgchart = user_load_orgchart($user);
        }
    }
	global $wpdb;
	static $results;
	if( !isset($results) ) {
		$prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
		$tableChart = "{$prefix}org_charts";
		$sql = $wpdb->prepare("SELECT * FROM {$tableChart} AS org WHERE org.parent = %d", $orgchart->id);
		$results = $wpdb->get_var( $sql );
	}

	return empty( $results ) ? true : false;
}

function orgchart_check_children_by_id( $id ){
    global $wpdb;
    static $results = [];
    if( !isset($results[$id]) ) {
        $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
        $tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("SELECT * FROM {$tableChart} AS org WHERE org.id = %d", $id);
        $results[$id] = $wpdb->get_row( $sql );
    }
    return $results[$id];
}

function orgchart_get_all_effa_by_parent( $id ) {
    global $wpdb;
    static $argsID = [];
    if (!isset($argsID[$id])) {
        $argsID[$id] = [];
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableChart = "{$prefix}org_charts";
        $results = $wpdb->get_results($wpdb->prepare("SELECT * FROM {$tableChart} as org WHERE org.parent = %d ", $id), ARRAY_A);
        if (!empty($results) && !is_wp_error($results)) {
            foreach ($results as $key => $item) {
                $argsID[$id][] = $item;
                $argsID[$id] = array_merge($argsID[$id], orgchart_get_all_effa_by_parent($item['id']));
            }
        }
    }
    return $argsID[$id];
}
function orgchart_get_all_effa_by_parent_not_nv( $id ) {
    global $wpdb;
    static $argsID = [];
    if (!isset($argsID[$id])) {
        $argsID[$id] = [];
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableChart = "{$prefix}org_charts";
        $results = $wpdb->get_results($wpdb->prepare("SELECT * FROM {$tableChart} as org WHERE org.parent = %d AND org.alias != 'nhanvien' ", $id));
        if (!empty($results) && !is_wp_error($results)) {
            foreach ($results as $key => $item) {
                $argsID[$id][] = $item;
                $argsID[$id] = array_merge($argsID[$id], orgchart_get_all_effa_by_parent_not_nv($item->id));
            }
        }
    }
    return $argsID[$id];
}

function user_get_phong_ban_by_user( $id, $output = [] ){
    global $wpdb;
    static $results = [];
	if( !isset($results[$id]) ) {
		#$results[$id] = $output;
		if ( empty( $output ) || $output->alias != 'phongban' ) {
			$prefix     = $wpdb->get_blog_prefix( get_current_blog_id() );
			$tableChart = "{$prefix}org_charts";
			$sql        = $wpdb->prepare( "SELECT * FROM {$tableChart} AS org WHERE org.id = %d", $id );
			$org        = $wpdb->get_row( $sql );
			if ( ! empty( $org ) ) {
				if ( $org->alias != 'phongban' ) {
					#unset( $results[ $id ] );
					$results[$id] = user_get_phong_ban_by_user( $org->parent, $org );
				} else {
					$results[$id] = $org;
				}
			}
		}
	}
    return $results[$id];
}


function orgchart_get_congty(){
	global $wpdb;
	static $results = [];
	$__key = md5( 'congty' );
	if( !isset($results[$__key]) ) {
		#$results[$id] = $output;
		if ( empty( $output ) || $output->alias != 'phongban' ) {
			$prefix     = $wpdb->get_blog_prefix( get_current_blog_id() );
			$tableChart = "{$prefix}org_charts";
			$sql        = $wpdb->prepare( "SELECT * FROM {$tableChart} AS org WHERE org.alias = %s", 'congty' );
			$results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
		}
	}
	return $results[$__key];
}
function orgchart_get_room(){
	global $wpdb;
	static $results = [];
	$__key = md5( 'room' );
	if( !isset($results[$__key]) ) {
		$prefix     = $wpdb->get_blog_prefix( get_current_blog_id() );
		$tableChart = "{$prefix}org_charts";
		$sql        = $wpdb->prepare( "SELECT * FROM {$tableChart} AS org WHERE org.alias = %s AND org.parent IN (SELECT org2.id FROM {$tableChart} AS org2 WHERE org2.alias = %s )", 'phongban', 'congty' );
		$results[$__key] = $wpdb->get_results( $sql );
	}
	return $results[$__key];
}

function orgchart_get_room_and_company_by_parent( $id ){
	global $wpdb;
	static $results = [];
	$__key = md5( 'room_and_company' . $id );
	if( !isset($results[$__key]) ) {
		$prefix     = $wpdb->get_blog_prefix( get_current_blog_id() );
		$tableChart = "{$prefix}org_charts";
		$sql        = $wpdb->prepare( "
				SELECT * 
				FROM {$tableChart} AS org 
				WHERE org.alias = %s AND org.parent IN (
					SELECT org2.id 
					FROM {$tableChart} AS org2 
					WHERE org2.alias = %s 
					) AND org.parent = %d 
				UNION 
				SELECT * 
				FROM {$tableChart} AS org 
				WHERE org.alias = %s AND org.parent = %d
				", 'phongban', 'congty', $id, 'congty', $id );
		$results[$__key] = $wpdb->get_results( $sql );
	}
	return $results[$__key];
}

function orgchart_get_ceo(){
	global $wpdb;
	static $results = [];
	$id = "get_ceo";
	if( !isset($results[$id]) ) {
		$prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
		$tableChart = "{$prefix}org_charts";
		$sql = "SELECT * FROM {$tableChart} AS org WHERE org.parent = 0";
		$results[$id] = $wpdb->get_row( $sql );
	}
	return $results[$id];
}

function orgchart_get_room_and_company(){
    global $wpdb;
    static $results = [];
    $__key = md5( 'congty_room' );
    if( !isset($results[$__key]) ) {
        #$results[$id] = $output;
        if ( empty( $output ) || $output->alias != 'phongban' ) {
            $prefix     = $wpdb->get_blog_prefix( get_current_blog_id() );
            $tableChart = "{$prefix}org_charts";
            $sql        ="SELECT * FROM {$tableChart} AS org WHERE org.alias IN ('congty', 'phongban')";
            $results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
        }
    }
    return $results[$__key];
}


function effa_get_orgchart_by_name_lv_alias_room($node_name, $node_alias,
                                            $node_parent, $node_room, $node_id){
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    static $results = [];
    $__key = md5( serialize(func_get_args()) );
    $tableChart = "{$prefix}org_charts";
    if( !isset($results[$__key]) ) {
        $where = "";
        if( !empty($node_id) && is_numeric($node_id) ){
            $where = $wpdb->prepare(" AND id <> %d", $node_id);
        }
        $sql = $wpdb->prepare("
                  SELECt * 
                  FROM {$tableChart} AS org 
                  WHERE org.name=%s AND org.room = %s 
                        AND org.parent=%d AND org.alias = %s {$where}
                  ", $node_name, $node_room, $node_parent, $node_alias);
        $results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $results[$__key];
}