<?php
$user = wp_get_current_user();

?>
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->

<div class="page-footer">
    <div class="footer-content">
        <div class="footer-left col-md-6">
            <div class="footer-item footer-logo">
                <a href="<?php echo esc_attr(site_url()); ?>">
                    <img src="<?php echo THEME_URL; ?>/assets/images/logo-brainmark.png" alt="logo" class="logo-default img-responsive" />
                </a>
            </div>
            
        </div>
        <div class="footer-right col-md-6">
            <div class="footer-item footer-email">
                <a href="mailto:consulting@brainmark.vn"> <i class="fa fa-envelope" aria-hidden="true"></i> <span>consulting@brainmark.vn</span> </a>
            </div>
            <div class="footer-item footer-phone">
                <a href="tel:0909363363"> <i class="fa fa-phone" aria-hidden="true"></i> <span>0909 363 363</span> </a>
            </div>
        </div>
    </div>

    <!-- END FOOTER -->
</div>

    <div id="confirm-popup" class="confirm-modal modal fade" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title"><?php _e('Confirm', TPL_DOMAIN_LANG); ?></h4>
            </div>
            <div class="modal-body">
                <h3 class="message" data-message=""></h3>
                <div class="response-message hidden"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"><?php _e('OK', TPL_DOMAIN_LANG); ?></button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', TPL_DOMAIN_LANG); ?></button>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php

wp_footer(); ?>

</body>
</html>
<?php
