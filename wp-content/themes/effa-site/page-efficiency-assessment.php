<?php
/**
 * Template Name: KPI Bank
 */

include_once THEME_DIR . '/form-login/includes/hooks.php';
include_once THEME_DIR . '/form-login/includes/handles.php';

if ( !is_user_logged_in() ) {
    get_template_part('form-login/header', 'login');
    get_template_part('form-login/content', 'login');
    get_template_part('form-login/footer', 'login');
} else {
    get_header();
    get_template_part('contents/content', 'efficiency-assessment');
    get_footer();
}
