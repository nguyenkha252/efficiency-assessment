<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/3/18
 * Time: 3:33 PM
 */

function esc_json_attr($data) {
    $data = esc_attr( str_replace('"', '&#34;', json_encode($data) ) );
    return $data;
}

function esc_json_attr_e($data) {
    echo esc_json_attr($data);
}