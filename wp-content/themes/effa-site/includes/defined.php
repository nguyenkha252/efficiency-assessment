<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/2/18
 * Time: 5:12 PM
 */
if( !defined('PAGE_PROFILE_PATH') ) {
    define('PAGE_PROFILE_PATH', __('profile', TPL_DOMAIN_LANG));
}
if( !defined('PAGE_DASHBOARD') ) {
    define('PAGE_DASHBOARD', __('dashboard', TPL_DOMAIN_LANG));
}
if( !defined('PAGE_ORGANIZATIONAL_STRUCTURE') ) {
    define('PAGE_ORGANIZATIONAL_STRUCTURE', __('organizational-structure', TPL_DOMAIN_LANG));
}
if( !defined('PAGE_MANAGE_MEMBERS') ) {
    define('PAGE_MANAGE_MEMBERS', __('manage-members', TPL_DOMAIN_LANG));
}
if( !defined('PAGE_EFFA_SYSTEM') ) {
    define('PAGE_EFFA_SYSTEM', __('efficiency-assessment', TPL_DOMAIN_LANG));
}
if( !defined('PAGE_NEWS') ) {
    define('PAGE_NEWS', __('news', TPL_DOMAIN_LANG));
}
if( !defined('PAGE_SETTINGS') ) {
    define('PAGE_SETTINGS', __('settings', TPL_DOMAIN_LANG));
}
if( !defined('PAGE_USER_MANAGERS') ) {
    define('PAGE_USER_MANAGERS', __('user-managers', TPL_DOMAIN_LANG));
}

define('CHART_ALIAS_BGD',               'bgd');
define('CHART_ALIAS_GROUP',             'tapdoan');
define('CHART_ALIAS_GROUP_TEXT',        'Tập Đoàn');
define('CHART_ALIAS_MANAGER',           'congty');
define('CHART_ALIAS_MANAGER_TEXT',      'Công Ty');
define('CHART_ALIAS_DEPARTMENTS',       'phongban');
define('CHART_ALIAS_DEPARTMENTS_TEXT',  'Phòng Ban');
define('CHART_ALIAS_EMPLOYEES',         'nhanvien');
define('CHART_ALIAS_EMPLOYEES_TEXT',    'Nhân Viên');