<?php
/**
 * Template Name: Manage Members
 */

require_once ABSPATH . 'wp-admin/includes/template.php';
require_once ABSPATH . 'wp-admin/includes/user.php';
#require_once THEME_DIR . '/ajax/get_users_list.php';
$user = wp_get_current_user();

$create_users = isset($user->allcaps['create_users']) ? $user->allcaps['create_users'] : false; # $user->has_cap('create_users');
$edit_users = isset($user->allcaps['edit_users']) ? $user->allcaps['edit_users'] : false; # $user->has_cap('edit_users');

$nonce_user_info = wp_create_nonce('user_info');

add_filter('editable_roles', 'kpi_filter_editable_roles', 10000);

get_header();
get_template_part('contents/users/user', 'list');
get_footer();
