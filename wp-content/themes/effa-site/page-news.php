<?php
/**
 * Template Name: News
 * Created by PhpStorm.
 * User: henry
 * Date: 12/27/17
 * Time: 14:30
 */


include_once __DIR__ . '/../brainmark-kpi-main/login.php';

if ( !is_user_logged_in() ) {
    get_header('login');
    get_template_part( 'contents/content', 'login' );
    get_footer('login');
} else {

    require_once THEME_DIR . '/ajax/get_post_info.php';

    # $cat = get_term_by('slug', __('notification', TPL_DOMAIN_LANG), 'category');
    # $params['ID'] = 38;
    # $params['post_category'] = [$cat->term_id];
    # $result = wp_update_post($params);

    get_header();
    $user = wp_get_current_user();
    $can_edit_post = false;
    if (!empty($user->allcaps['level_9']) || $user->has_cap('level_9')) {
        $can_edit_post = true;
    }

    # var_dump($cat, $result);

    $pid = empty($_REQUEST['pid']) ? 0 : intval($_REQUEST['pid']);
    if ($can_edit_post):
        $nonce = $pid ? wp_create_nonce('update_news') : wp_create_nonce('create_news');
        $action = $pid ? 'update_news' : 'create_news';
        $button = $pid ? 'Update Post' : 'Save Post';
        require_once(ABSPATH . 'wp-admin/includes/post.php');
        $news = $pid ? get_post($pid) : get_default_post_to_edit('post', false);;

        ?>
        <form id="news-post-page" class="container" onsubmit="return false;"
              action="<?php echo esc_attr(admin_url('admin-ajax.php?action=' . esc_attr($action) . '&_wpnonce=' . $nonce)); ?>"
              method="POST" enctype="application/x-www-form-urlencoded">
            <input type="hidden" name="ID" value="<?php esc_attr_e($news->ID); ?>">
            <input type="hidden" name="post_status" value="publish">

            <div class="form-group col-lg-12 col-md-12 -col-sm-12 title">
                <label for="title"><?php _e('Post Title', TPL_DOMAIN_LANG); ?></label>
                <input required class="form-control placeholder-no-fix" type="text" autocomplete="off"
                       placeholder="<?php esc_attr_e('Post Title', TPL_DOMAIN_LANG); ?>" name="post_title" id="title"
                       value="<?php esc_attr_e($news->post_title); ?>">
            </div>
            <div class="form-group col-lg-12 col-md-12 -col-sm-12 content">
                <label class="control-label visible-ie8 visible-ie9"
                       for="content"><?php _e('Post Content', TPL_DOMAIN_LANG); ?></label>
                <?php
                $_wp_editor_expand = $_content_editor_dfw = false;
                wp_editor($news->post_content, 'post_content', array(
                    '_content_editor_dfw' => $_content_editor_dfw,
                    'drag_drop_upload' => true,
                    'tabfocus_elements' => 'content-html,save-post',
                    'editor_height' => 300,
                    'media_buttons' => false,
                    'default_editor' => 'html',
                    'tinymce' => array(
                        'resize' => false,
                        'wp_autoresize_on' => $_wp_editor_expand,
                        'add_unload_trigger' => false,
                        'wp_keep_scroll_position' => !$is_IE,
                    ),
                )); ?>
            </div>
            <div class="form-group col-lg-10 col-md-10 -col-sm-8 message"></div>
            <div class="form-group col-lg-2 col-md-2 -col-sm-4 submit">
                <button class="form-control btn btn-primary" type="submit"
                        name="post_submit"><?php _e($button, TPL_DOMAIN_LANG); ?></button>
            </div>
        </form>
    <?php endif; ?>

    <ul class="lastest-posts container">
        <?php echo kpi_render_posts(); ?>
    </ul>
    <?php

    get_footer();
}
