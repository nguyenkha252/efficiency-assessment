<?php
$user = wp_get_current_user();
include_once THEME_DIR . '/form-login/includes/handles.php';

#$options = get_settings_website();

$logo_url = !empty( $options['logo_url'] ) ? $options['logo_url'] : THEME_URL . '/assets/images/logo-brainmark.png';

?>
<!DOCTYPE html>
<!--[if IE 8]> <html <?php language_attributes(); ?> class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html <?php language_attributes(); ?> class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html <?php language_attributes(); ?>>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <?php /* <title>Brainmark - KPI System </title> */ ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <link rel="shortcut icon" href="<?php echo THEME_URL; ?>/favicon.ico" />
        <script type="text/javascript">
            var THEME_URL = '<?php echo THEME_URL; ?>';
            var AJAX_URL = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
            var IMAGE_LOADING = {
                message: '<img width="40" src="<?php echo get_template_directory_uri() ?>/assets/images/loading.gif">',
                    css: {border: '1px solid #ccc', padding: 'none', width: '40px', height: '40px'}
            }
        </script>
        <?php wp_head(); ?>
    </head>
    <!-- END HEAD -->
    <body <?php
    $class = 'page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid ';
    $class .= USER_IS_ADMIN ? 'is-user-admin ' : '';
    body_class($class);
    ?>>
    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="<?php echo esc_attr(site_url()); ?>">
                    <img src="<?php echo $logo_url; ?>" alt="logo" class="logo-default img-responsive" />
                </a>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN PAGE TOP -->
            <div class="page-top">
                <div class="pull-left htitle">
                    <h2><?php echo !empty($options['title_website']) ? $options['title_website'] : __('Efficiency Assessment', TPL_DOMAIN_LANG); ?></h2>
                </div>
                <div class="top-menu">
                    <div class="nav navbar-nav pull-right">
                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <div class="dropdown dropdown-user">
                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="dropdown-menu-default">
                              <?php
                              $avatar= "";
                              if($user->exists()) {
                                $user_id = $user->ID;
                                $avatar = get_avatar( $user_id, 128 );
                              }
                              ?>
                              <?php echo $avatar; ?>
                                <span class="username username-hide-on-mobile" data-role="<?php echo $orgchart ? $orgchart->role : ''; ?>"><?php
                                    echo $user->exists() ? $user->display_name : __("User Account", TPL_DOMAIN_LANG);
                                    ?></span>
                            </button>
                            <?php
                            $myAccount = get_page_by_path( PAGE_PROFILE_PATH );
                            ?>
                            <ul class="dropdown-menu dropdown-menu-default" aria-labelledby="dropdown-menu-default">
                                <?php
                                if( user_is_manager() ):
                                    $pageUserManagers = get_page_by_path( PAGE_USER_MANAGERS, 'OBJECT', 'page' );
                                    if( !empty( $pageUserManagers ) && !is_wp_error( $pageUserManagers ) ):


                                    ?>
                                    <li>
                                        <a href="<?php echo get_the_permalink($pageUserManagers->ID); ?>"><i class="fa fa-users"></i><?php _e('Account', TPL_DOMAIN_LANG); ?></a>
                                    </li>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php
                                    $pageProfile = get_page_by_path( PAGE_PROFILE_PATH );
                                    if( !empty( $pageProfile ) && !is_wp_error( $pageProfile ) ):
                                ?>
                                    <li>
                                        <a href="<?php echo get_the_permalink($pageProfile->ID); ?>">
                                            <i class="fa fa-user"></i> <?php _e('Profile', TPL_DOMAIN_LANG); ?></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url(); ?>/?action=logout">
                                            <i class="fas fa-sign-out-alt"></i> <?php _e('Logout', TPL_DOMAIN_LANG); ?></a>
                                    </li>
                                <?php endif ?>
                            </ul>
                        </div>
                        <!-- END USER LOGIN DROPDOWN -->
                    </div>
                </div>
                <!-- END TOP NAVIGATION MENU -->

            </div>
            <!-- END PAGE TOP -->
            <?php get_template_part('nav/nav', 'menu-main'); ?>
        </div>

        <!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <?php
                    /*echo sprintf('<h1 class="page-title">%s</h1>', $kpi_contents_parts[PARAM_PART_IDX]['text']);*/
?>
