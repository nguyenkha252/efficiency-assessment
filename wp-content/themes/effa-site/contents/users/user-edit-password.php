<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/25/17
 * Time: 02:20
 */
global $wpdb;

require_once THEME_DIR . '/inc/lib-users.php';

$user = wp_get_current_user();
$notify_error = '';
$notify_success = '';
if( isset($_POST['changepass']) && ($_SERVER['REQUEST_METHOD'] == 'POST') ){

    $params = wp_unslash( $_POST );
    $user_pass = $params['user_pass'];
    $user_new_pass = $params['user_new_pass'];
    $re_user_pass = $params['re_user_pass'];
    if( $user_new_pass != $re_user_pass ){
	    $notify_error = 'Mật khẩu mới và Nhập lại mật khẩu không trùng khớp';
    }else {
	    if ( $user && wp_check_password( $user_pass, $user->user_pass, $user->ID ) ) {
		    $hash = wp_hash_password( $user_new_pass );
		    $wpdb->update($wpdb->users, array('user_pass' => $hash, 'user_activation_key' => ''), array('ID' => $user->ID) );
		    wp_set_current_user($user->ID, $user->user_login);
		    wp_set_auth_cookie($user->ID, false );
		    #remove_action('wp_login', 'custom_wp_login', 100);
		    $notify_success = 'Đổi mật khẩu thành công. Vui lòng đăng nhập lại.';
		    #echo "<script>window.location.href=\"{$_SERVER['HTTP_REFERER']}\"</script>";

	    } else {
		    $notify_error = 'Mật khẩu hiện tại không đúng';
	    }
    }
}

$referrer = str_replace('&', '%26', !empty($_REQUEST['_referrer']) ? $_REQUEST['_referrer'] : '' );

?>
<form class="changepassword user-edit-form" action="<?php echo esc_attr( kpi_get_profile_url($user->ID) ); ?>" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce('update_user'); ?>">

    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h4 class="title"><?php _e('Đổi mật khẩu', TPL_DOMAIN_LANG); ?></h4>
        </div>
        <?php
        do_action('edit_user_result');
        ?>
        <div class="col-lg-6 col-md-6 col-sm-12 change-password">
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="user_pass"><?php _e('Mật khẩu hiện tại', TPL_DOMAIN_LANG); ?></label>
                <input required class="form-control placeholder-no-fix" type="password" autocomplete="off"
                       placeholder="<?php _e('Mật khẩu hiện tại', TPL_DOMAIN_LANG); ?>" id="user_pass" name="user_pass"/>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="user_new_pass"><?php _e('Mật khẩu mới', TPL_DOMAIN_LANG); ?></label>
                <input required class="form-control placeholder-no-fix" type="password" autocomplete="off"
                       placeholder="<?php _e('Mật khẩu mới', TPL_DOMAIN_LANG); ?>" id="user_new_pass" name="user_new_pass"/>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="re_user_pass"><?php _e('Nhập lại mật khẩu mới', TPL_DOMAIN_LANG); ?></label>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off"
                       placeholder="<?php _e('Nhập lại mật khẩu mới', TPL_DOMAIN_LANG); ?>" id="re_user_pass"
                       name="re_user_pass"/>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
            <?php if( !empty( $notify_error ) ): ?>
                <span class="notification error"><?= $notify_error ?></span>
            <?php endif; ?>
	        <?php if( !empty( $notify_success ) ): ?>
                <span class="notification response-container success"><?= $notify_success ?></span>
	        <?php endif; ?>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="footer">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><?php _e('Trở về trang trước', TPL_DOMAIN_LANG); ?></button>
                <button type="submit" class="btn btn-primary" name="changepass"><?php _e('Lưu lại', TPL_DOMAIN_LANG); ?></button>
            </div>
        </div>
    </div>
</form><!-- /.user-edit-form -->
