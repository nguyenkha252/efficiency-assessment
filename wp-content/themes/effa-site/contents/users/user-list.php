<?php
/**
 * Created by PhpStorm.
 * User: Heckman
 * Date: 12/25/17
 * Time: 02:20
 */

$user = wp_get_current_user();
$orgchart = user_load_orgchart($user);
$member_role = $orgchart ? $orgchart->role : '';
if( empty($member_role) ) {
    $member_role = '';
} else {
    $member_role = strtolower($member_role);
}

require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/includes/Paginator.php';

$tim = !empty($_REQUEST['tim']) ? strip_tags($_REQUEST['tim']) : '';
$room_id = $_REQUEST['room_id'] = $_GET['room_id'] = !empty($_GET['room_id']) ? (int)$_GET['room_id'] : '';
$orgchart_id = $_REQUEST['chart_id'] = $_GET['chart_id'] = !empty($_GET['chart_id']) ? (int)$_GET['chart_id'] : '';
$result = effa_get_users($_REQUEST);
$users = $result['items'];
$total = $result['total'];
$trang = $result['trang'];
$limit = $result['limit'];
global $wpdb;
$profile = get_page_by_path( PAGE_PROFILE_PATH );
$members = get_page_by_path( PAGE_MANAGE_MEMBERS );
$profile_url = apply_filters( 'the_permalink', get_permalink( $profile ), $profile );
$current_url = apply_filters( 'the_permalink', get_permalink( $members ), $members );
# echo '<pre>'; var_dump(__LINE__, __FILE__, $user->orgchart_id, $result['sql'], $result['error'], $users[0]); echo '</pre>';

# echo '<pre>';
# global $wp_the_query;
# var_dump($_SERVER['REQUEST_URI'], $_GET, $wp_the_query);
# echo '</pre>';
$referrer = str_replace('&', '%26', urlencode($_SERVER['REQUEST_URI']) );
$charts = effa_get_list_org_charts($user->orgchart_id, true);
$listOrgchart = orgchart_get_room_and_company_by_parent($orgchart->id);

$url = esc_url( add_query_arg('_referrer',  $referrer, effa_get_profile_url('new') ) );
?>
    <div class="lists container-fluid">
        <h1 class="col-md-4" style="margin: 0;padding: 0;margin-bottom: 30px;margin-top: 20px; width: 100%">
          <?php _e('List Members', TPL_DOMAIN_LANG); ?>
          <a style="margin-top: -15px;float: right;"
            href="<?php echo $url; ?>" class="btn btn-primary"><?php _e('Create New', TPL_DOMAIN_LANG); ?>
          </a>
        </h1>
        <form class="form-user-filter col-md-10" style="margin: 0;padding: 0;" action="<?php
        echo add_query_arg(['trang' => $trang, 'limit1' => $limit, 'tim' => $tim], $current_url);
        ?>" method="get" enctype="application/x-www-form-urlencoded">
            <div class="col-md-3" style="margin: 0;padding: 0;">
                <div class="input-group">
                    <label class="input-group-addon" for="room_id" id="room_id-id"><?php _e('Label Room', TPL_DOMAIN_LANG); ?></label>
                    <select name="room_id" id="room_id" aria-describedby="room_id-id" class="orgchart-dropdown selectpicker" data-live-search="true">
                        <option value=""> -- <?php _e('Filter for room', TPL_DOMAIN_LANG); ?> -- </option>
                        <?php
                        foreach($listOrgchart as $k => $chart){
                            $json = esc_json_attr($chart);
                            echo sprintf('<option data-level="%d" value="%d" %s data-info="%s" >%s</option>'."\n",
                                $chart->level, $chart->id, (( ($chart->id == $room_id)) ? 'selected="selected"' : ''), $json, $chart->room);
                        }
                        #$departments_html = kpi_get_departments_dropdown($room_id);
                        #echo $departments_html;
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group">
                    <label class="input-group-addon" id="chart_id-id"><?php _e('Label Position', TPL_DOMAIN_LANG); ?></label>
                    <select name="chart_id" id="chart_id" aria-describedby="chart_id-id" class="orgchart-dropdown selectpicker" data-live-search="true">
                        <option value=""> -- <?php _e('Filter for position', TPL_DOMAIN_LANG); ?> -- </option>
                        <?php
                        get_user_orgchart_dropdown( $orgchart_id, true);
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-5">
                <div class="input-group">
                    <input class="form-control" type="text" name="tim" placeholder="<?php _e('Search for name, email', TPL_DOMAIN_LANG); ?>" value="<?php echo $tim; ?>" aria-describedby="tim-id">
                    <span class="input-group-addon" id="tim-id">
                        <button type="submit" class="btn btn-default"><?php _e('Filter Staff', TPL_DOMAIN_LANG); ?></button>
                    </span>
                </div>
            </div>
            <input type="hidden" name="trang" value="1">
            <input type="hidden" name="limit" value="<?php echo $limit; ?>">
            <input type="hidden" name="chinhxac" value="0">
        </form>
        <table class="user-list" cellpadding="0" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th class="id-stt"><?php _e('No.', TPL_DOMAIN_LANG); ?></th>
                <th class="id-col">
                  <?php _e('Staff ID', TPL_DOMAIN_LANG); ?>
                </th>
                <th class="avatar-img">
                  <?php _e('Avatar', TPL_DOMAIN_LANG); ?>
                </th>
                <th class="display_name">
                  <?php _e('Display Name', TPL_DOMAIN_LANG); ?>
                </th>
                <th class="email">
                  <?php _e('Email', TPL_DOMAIN_LANG); ?>
                </th>
                <th class="login">
                  <?php _e('Group', TPL_DOMAIN_LANG); ?>
                </th>
                <th class="position_name">
                  <?php _e('Label Position', TPL_DOMAIN_LANG); ?>
                </th>
                <th class="position_name">
                  <?php _e('Label Manager', TPL_DOMAIN_LANG); ?>
                </th>
                <?php if(user_is_manager()): ?>
                    <th class="action" style="width:85px">
                      <?php _e('Label Action', TPL_DOMAIN_LANG); ?>
                    </th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            <?php
            if( !empty($users) ):
                $default_avatar = THEME_URL . '/assets/images/no-photo.jpg';
                $stt = 1;
                $remove_url = esc_url( admin_url('admin-ajax.php?action=user-remove&uid=USERID&_wpnonce='.
                    wp_create_nonce('remove_user') . '&_referrer=' . $referrer) );
                foreach($users as $user):
                    $edit_url = esc_url( add_query_arg( ['profile' => $user['ID'], '_referrer' => $referrer], $profile_url ) );
                    ?>
                    <tr class="user-row">
                        <td class="id-stt"><?php echo $stt; $stt ++;?></td>
                        <td class="id-col"><label><input class="id" type="checkbox" name="user_id[]" value="<?php echo $user['ID']; ?>"><span><?php echo $user['user_nicename']; ?></span></label></td>
                        <td class="avatar-img"><img class="avatar"
                                                    onerror="this.src = this.getAttribute('data-src');"
                                                    src="<?php echo empty($user['user_url']) ? $default_avatar : $user['user_url']; ?>"
                                                    data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                                    style="max-width: 32px; max-height: 32px"></td>
                        <td class="display_name"><?php echo $user['display_name']; ?></td>
                        <td class="email"><?php echo $user['user_email']; ?></td>
                        <td class="login"><?php echo $user['room']; ?></td>
                        <td class="position_name"><?php echo __($user['orgchart_name'], TPL_DOMAIN_LANG); ?></td>
                        <td class="parent_name"><?php echo __($user['orgchart_parent_name'], TPL_DOMAIN_LANG); ?></td>
	                <?php if(user_is_manager()): ?>
                        <td class="action">
                            <a href="<?php echo $edit_url; ?>" title="<?php _e('Edit', TPL_DOMAIN_LANG); ?>"><span class="fa fa-pencil"></span></a>
                            <a href="javascript:;" data-target="#confirm-popup" data-toggle="modal"
                               data-remove-element="tr.user-row" data-loading-element="tr.user-row"
                               data-url="<?php echo str_replace('USERID', $user['ID'], $remove_url); ?>"
                               data-type="post" data-data-type="json"
                               data-message="<?php _e('Do you want to delete staff', TPL_DOMAIN_LANG); ?> <?php echo esc_attr("\"{$user['display_name']}\"") ?>?"
                               title="<?php _e('Delete', TPL_DOMAIN_LANG); ?>"><span class="fa fa-trash-o"></span></a>
                        </td>
                    <?php endif; ?>
                    </tr>
                <?php
                endforeach;
            endif;
            ?>
            </tbody>

        </table>
        <?php
        $nav = new Paginator($total, $limit, $trang, "{$current_url}?trang=(:num)");
        $nav->setPreviousText(__('Previous', TPL_DOMAIN_LANG));
        $nav->setNextText(__('Next', TPL_DOMAIN_LANG));
        echo $nav->toHtml();
        ?>
    </div>
    <!-- /.lists -->
<?php
