<?php

global $kpi_contents_parts;
$user = wp_get_current_user();
#echo "<pre>"; var_dump(__LINE__, __FILE__, $user, $_COOKIE); echo "</pre>"; exit();

include_once __DIR__ . '/../brainmark-kpi-main/login.php';

if ( !is_user_logged_in() ) {
    get_header('login');
    get_template_part( 'contents/content', 'login' );
    get_footer('login');
} else {
    if( is_single() && !is_home() ) {
        include __DIR__ . '/single.php';
    } else {
    	if( !IS_AJAX )
            get_header();

	    get_template_part( 'contents/content', PARAM_PART_IDX );
	    #get_template_part( 'contents/content', 'dashboard' );
        if( !IS_AJAX )
            get_footer();
    }
}

