<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/16/17
 * Time: 00:45
 */

global $kpi_contents_parts;

include_once __DIR__ . '/../brainmark-kpi-main/login.php';

if ( !is_user_logged_in() ) {
    get_header('login');
    get_template_part( 'contents/content', 'login' );
    get_footer('login');
} else {
    get_header();
    the_post();
    ?>
    <article class="post-entry">
        <h1 class="post-title"><?php the_title(); ?></h1>
        <div class="post-content">
            <?php the_content(); ?>
        </div>
    </article>
    <?php
    get_footer();
}