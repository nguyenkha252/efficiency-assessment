<?php
/**
 * Template Name: KPI Bank
 */

$user = wp_get_current_user();
include_once __DIR__ . '/../brainmark-kpi-main/login.php';

if ( !is_user_logged_in() ) {
    get_header('login');
    get_template_part( 'contents/content', 'login' );
    get_footer('login');
} else {
    get_header();
    echo "<div class='row'>";
        get_template_part('contents/effa/effa', 'left');
        get_template_part('contents/effa/effa', 'content');
    echo "</div>";
    get_footer();
}
