<?php
/*
 * Define Variables
 */
if (!defined('THEME_DIR'))
    define('THEME_DIR', dirname(__FILE__));
if (!defined('THEME_URL'))
    define('THEME_URL', get_template_directory_uri());
if (!defined('LIK_AUTHOR_URL'))
    define('LIK_AUTHOR_URL', 'http://btechso.com');
if (!defined('LIK_AUTHOR_NAME'))
    define('LIK_AUTHOR_NAME', 'BTECH');
if (!defined('TPL_DOMAIN_LANG'))
    define('TPL_DOMAIN_LANG', 'brain_branch');
if (!defined('IS_AJAX'))
    define('IS_AJAX', !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ? true : false);

function wp_avatar_upload_dir() {
    $upload = wp_upload_dir();
    $upload['url'] = preg_replace('#' . preg_quote($upload['subdir']) . '$#', '', $upload['url']);
    return $upload;
}

define('CHART_ALIAS_BGD',               'bgd');
define('CHART_ALIAS_GROUP',             'tapdoan');
define('CHART_ALIAS_GROUP_TEXT',        'Tập Đoàn');
define('CHART_ALIAS_MANAGER',           'congty');
define('CHART_ALIAS_MANAGER_TEXT',      'Công Ty');
define('CHART_ALIAS_DEPARTMENTS',       'phongban');
define('CHART_ALIAS_DEPARTMENTS_TEXT',  'Phòng Ban');
define('CHART_ALIAS_EMPLOYEES',         'nhanvien');
define('CHART_ALIAS_EMPLOYEES_TEXT',    'Nhân Viên');
# 'manager','departments','employees','bgd','phongban','nhanvien',''

// Translate to english
define('_TABS_Finance',   'Tài chính / Finance');
define('_TABS_Customer',   'Khách hàng / Customer');
define('_TABS_Operate',   'Vận hành / Process');
define('_TABS_Development',   'Phát triển / Learning groth');
define('_LOGOUT',   'Đăng xuất / Logout');
define('_PROFILE',   'Hồ sơ / Profile');
define('_ADMIN_ACCOUNT',   'Tài khoản / Account');
define('_MESSAGE_EMPTY_DATA_BY_KPI_GROUP',   'Vui lòng tạo trọng số cho Tài chính, Khách hàng, Vận hành, Phát triển <br/> <span style="color: #999;">Please enter your objective for Finance/Customer/Process/Learning groth. </span>');
define('_Objective_KPI',   'Mục tiêu KPI <br/> <span style="font-style:italic;color:#888;font-size: 15px;">Objective KPI</span>');
define('_LBL_YEAR',   'Năm <br/> <span style="font-style:italic;color:#888;font-size: 15px;">Year</span>');
define('_LBL_VIEW',   'Xem / View');

$GLOBALS['charts_alias'] = [
    CHART_ALIAS_GROUP => CHART_ALIAS_GROUP_TEXT,
    CHART_ALIAS_MANAGER => CHART_ALIAS_MANAGER_TEXT,
    CHART_ALIAS_DEPARTMENTS => CHART_ALIAS_DEPARTMENTS_TEXT,
    CHART_ALIAS_EMPLOYEES => CHART_ALIAS_EMPLOYEES_TEXT
];


function roman_numerals_lookups() {
    $lookup = [
        'N'     => 1000000,
        'QN'    =>  900000,
        'O'     =>  500000,
        'QO'    =>  400000,
        'Q'     =>  100000,
        'RQ'    =>   90000,
        'P'     =>   50000,
        'RP'    =>   40000,
        'R'     =>   10000,
        'MR'    =>    9000,
        'S'     =>    5000,
        'MS'    =>    4000,
        'M'     =>    1000,
        'CM'    =>     900,
        'D'     =>     500,
        'CD'    =>     400,
        'C'     =>     100,
        'XC'    =>      90,
        'L'     =>      50,
        'XL'    =>      40,
        'X'     =>      10,
        'IX'    =>       9,
        'V'     =>       5,
        'IV'    =>       4,
        'I'     =>       1
    ];
    return $lookup;
}

function convert_roman_2_number($romanNumber) {
    $romanNumber = "{$romanNumber}";
    $lookup = roman_numerals_lookups();
    $result = 0;

    $len = strlen($romanNumber);
    while($len > 0) {
        foreach($lookup as $roman => $value){
            $l = strlen($roman);
            $num = substr($romanNumber, 0, $l);
            if( $num == $roman ) {
                $result += $value;
                $romanNumber = substr($romanNumber, $l);
                break;
            }
        }
        $len = strlen($romanNumber);
    }

    return $result;
}

function convert_number_2_roman($integer) {
// Convert the integer into an integer (just to make sure)
    $integer = abs(intval($integer));
    $result = '';

    // Create a lookup array that contains all of the Roman numerals.
    $lookup = roman_numerals_lookups();

    foreach($lookup as $roman => $value){
        // Determine the number of matches
        $matches = intval($integer / $value);

        // Add the same number of characters to the string
        $result .= str_repeat($roman, $matches);

        // Set the integer to be the remainder of the integer and the value
        $integer = $integer % $value;
    }

    // The Roman numeral should be built, return it
    return $result;
}

if (!function_exists('brain_branch_setup')) :
    function brain_branch_setup()
    {
        add_theme_support( 'title-tag' );
        # $locale = apply_filters( 'theme_locale', is_admin() ? get_user_locale() : get_locale(), $domain );
        $path = THEME_DIR . '/languages';
        load_theme_textdomain(TPL_DOMAIN_LANG, $path);
        if(!defined('PAGE_PROFILE_PATH'))
            define('PAGE_PROFILE_PATH',                      __('profile', TPL_DOMAIN_LANG) );
        if(!defined('PAGE_MY_CAKENDAR_PATH'))
            define('PAGE_MY_CAKENDAR_PATH',                  __('calendar', TPL_DOMAIN_LANG) );
        if(!defined('PAGE_MY_INBOXS_PATH'))
            define('PAGE_MY_INBOXS_PATH',                    __('inboxs', TPL_DOMAIN_LANG) );
        if(!defined('PAGE_MY_TODO_PATH'))
            define('PAGE_MY_TODO_PATH',                      __('to-do', TPL_DOMAIN_LANG) );
        if(!defined('PAGE_EXPORTS'))
            define('PAGE_EXPORTS',                      __('exports', TPL_DOMAIN_LANG) );
        if(!defined('PAGE_DASHBOARD'))
            define('PAGE_DASHBOARD',                    __('dashboard', TPL_DOMAIN_LANG) );
        if(!defined('PAGE_ORGANIZATIONAL_STRUCTURE'))
            define('PAGE_ORGANIZATIONAL_STRUCTURE',     __('organizational-structure', TPL_DOMAIN_LANG) );
        if(!defined('PAGE_MANAGE_MEMBERS'))
            define('PAGE_MANAGE_MEMBERS',               __('manage-members', TPL_DOMAIN_LANG) );
        if(!defined('PAGE_MANAGE_TARGETS'))
            define('PAGE_MANAGE_TARGETS',               __('manage-targets', TPL_DOMAIN_LANG) );
        if(!defined('PAGE_KPI_BANK'))
            define('PAGE_KPI_BANK',                     __('kpi-bank', TPL_DOMAIN_LANG) );
        if(!defined('PAGE_APPLY_KPI_SYSTEM'))
            define('PAGE_APPLY_KPI_SYSTEM',             __('apply-bsc-kpi-system', TPL_DOMAIN_LANG) );
        if(!defined('PAGE_STATISTICAL'))
            define('PAGE_STATISTICAL',                  __('statistical', TPL_DOMAIN_LANG) );
        if(!defined('PAGE_NEWS'))
            define('PAGE_NEWS',                         __('news', TPL_DOMAIN_LANG) );
        if(!defined('PAGE_SETTINGS'))
            define('PAGE_SETTINGS',                     __('settings', TPL_DOMAIN_LANG) );

        if(!defined('PAGE_USER_MANAGERS'))
            define('PAGE_USER_MANAGERS',                __('user-managers', TPL_DOMAIN_LANG) );
        if( !defined('PAGE_EFFA_SYSTEM') ){
            define('PAGE_EFFA_SYSTEM',                  __('efficiency-assessment', TPL_DOMAIN_LANG) );
        }

        add_post_type_support('page', ['title', 'editor', 'author', 'thumbnail', 'excerpt', 'page-attributes']);

        # return load_textdomain( $domain, $path . '/' . $locale . '.mo' );
        return;
    }


    if (defined('DOING_AJAX') && DOING_AJAX) {
        function wp_ajax_kpi_callback($prefix, $params) {
            $action = str_replace(['-', ':'], ['_', '_'], $_REQUEST['action']);

            $user = wp_get_current_user();
            if( has_cap($user,USER_CAP_MANAGERS) || has_cap($user, USER_CAP_EMPLOYERS) ){
	            $filename = THEME_DIR . "/ajax_managers/{$prefix}{$action}.php";
            }else{
	            $filename = THEME_DIR . "/ajax/{$prefix}{$action}.php";
            }

            if (file_exists($filename)) {
                require_once $filename;
            }

            $callback = "wp_ajax_{$prefix}" . $action;

            if (function_exists($callback)) {
                /* add_filter('wp_doing_ajax', function($result) {
                    return false;
                }); */
                $callback($params);
                exit();
            }
        }

        if (!empty($_REQUEST['action'])) {
            $is_logged_in = is_user_logged_in();

            // Get gallery at proejct detail front - end
            $prefix = $is_logged_in ? 'wp_ajax_' : 'wp_ajax_nopriv_';
            // Register core Ajax calls.
            if ($_SERVER["REQUEST_METHOD"] == 'GET') {
                add_action($prefix . $_REQUEST['action'], function () {
                    return wp_ajax_kpi_callback('get_', $_REQUEST);
                }, 2);
            } else if ($_SERVER["REQUEST_METHOD"] == 'POST') {
                add_action($prefix . $_REQUEST['action'], function () {
                    return wp_ajax_kpi_callback('post_', $_REQUEST);
                }, 2);
            }
        }
    }
endif; // twentyfifteen_setup

add_action('after_setup_theme', 'brain_branch_setup');

function user_load_orgchart(WP_User $user) {
    global $wpdb;
    require_once THEME_DIR . '/inc/lib-orgchart.php';
    if( !$user->__isset('orgchart') ) {
        $orgchart_id = $user->__get('orgchart_id');
        $orgchart = kpi_orgchart_get_by('id', $orgchart_id, OBJECT);
        $user->__set('orgchart', $orgchart);
    }
    return $user->__get('orgchart');
}

add_action('set_current_user', function() {
    global $current_user;
    user_load_orgchart($current_user);
});

/*
 * Add theme Support
 */
if (function_exists('add_theme_support')) {
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

}

/*
 * Add Image Size for Wordpress
 */
if (function_exists('add_image_size')) {
    //add_image_size( 'homepage-slide', 620, 238, true ); // (cropped)
    //add_image_size( 'homepage-slide-thumb', 42, 24, true ); // (cropped)
    //add_image_size( 'thumb_post', 457, 232, true ); // (cropped)
}

/*
 *  Localisation Support
 */
//load_theme_textdomain('lik_theme', THEME_DIR . '/languages/');


/*
 *  Add page slug to body class
 */
function lik_add_slug_to_body_class($classes) {
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

add_filter('body_class', 'lik_add_slug_to_body_class');


/*
 *  Change login logo & url in WP Admin
 */
function lik_custom_login_logo() {
    echo "<style type='text/css'>
body.login {
    background: -webkit-linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* Chrome 10+, Saf5.1+ */
    background: -moz-linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* FF3.6+ */
    background: -ms-linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* IE10 */
    background: -o-linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* Opera 11.10+ */
    background: linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* W3C */
}

.login h1 a {
    background-image: url('" . THEME_URL . "/images/login-logo.png');
    background-size: 280px 78px;
    width: 280px;
    height: 78px;
}

.login form {
    margin-left: auto;
    margin-right: auto;
    padding: 30px;
    border: 1px solid rgba(0, 0, 0, .2);
    background-clip: padding-box;
    background: rgba(255, 255, 255, 0.9);
    box-shadow: 0 0 13px 3px rgba(0, 0, 0, .5);
    overflow: hidden;
}

.login label {
    color: #333;
}

#backtoblog, #nav {
    display: none;
}
</style>";
}

add_action('login_head', 'lik_custom_login_logo');

function lik_custom_login_logo_url($url) {
    return LIK_AUTHOR_URL;
}

add_filter('login_headerurl', 'lik_custom_login_logo_url');

function lik_custom_login_logo_title() {
    return get_bloginfo('name');
}

add_filter('login_headertitle', 'lik_custom_login_logo_title');

/*
 *  Remove wp-logo on admin bar
 */
function lik_wp_admin_bar_remove() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');
}

add_action('wp_before_admin_bar_render', 'lik_wp_admin_bar_remove', 0);

/*
 * Add Editor in back-end
 */
function lik_theme_add_editor_styles() {
    add_editor_style(THEME_URL . '/editor-style.css');
}

add_action('init', 'lik_theme_add_editor_styles');

/*
 * Add excerpt for page
 */
function lik_add_excerpts_to_pages() {
    add_post_type_support('page', 'excerpt');
}

add_action('init', 'lik_add_excerpts_to_pages');

/*
 * Set Editor Visual Default
 */
add_filter('wp_default_editor', create_function('', 'return "tinymce";'));

/*
 *  Remove generator
 */
remove_action('wp_head', 'wp_generator');

/*
 * Change Footer Text in Admin
 */
function lik_change_footer_text() {
    echo "Powered by <a href='" . LIK_AUTHOR_URL . "' target='_blank'>" . LIK_AUTHOR_NAME . "</a>";
}

add_filter('admin_footer_text', 'lik_change_footer_text');

/*
 * Remove help bar
 */
function lik_wp_remove_help($old_help, $screen_id, $screen) {
    $screen->remove_help_tabs();
    return $old_help;
}

add_filter('contextual_help', 'lik_wp_remove_help', 999, 3);

/*
 * Fix WP-Lockdown
 */
add_filter('LD_404', 'lik_redirect_404');
function lik_redirect_404() {
    wp_redirect(home_url() . '/not-found/');
    exit;
}

/**
 * Fix Gravity Form Tabindex Conflicts
 */

add_filter('gform_tabindex', 'gform_tabindexer', 10, 2);
function gform_tabindexer($tab_index, $form = false)
{
    $starting_index = 1000; // if you need a higher tabindex, update this number
    if ($form)
        add_filter('gform_tabindex_' . $form['id'], 'gform_tabindexer');
    return GFCommon::$tab_index >= $starting_index ? GFCommon::$tab_index : $starting_index;
}


/*
 * Include framework files
 */
foreach (glob(THEME_DIR . "/includes/*.php") as $file_name) {
    require_once($file_name);
}

add_action('init', function () {
    require_once __DIR__ . '/../brainmark-kpi-main/includes/custom-role.php';
    add_filter('wp_pre_insert_user_data', function($data, $update, $user_id) {
        if( !empty($_POST['orgchart_id']) ) {
            $data['orgchart_id'] = $_POST['orgchart_id'];
        }
        return $data;
    }, 10, 3 );
    # require_once __DIR__ . '/vendor/autoload.php';
});

function get_user_orgchart($user_id = 0) {
    if( !$user_id ) {
        $user = wp_get_current_user();
    } else {
        $user = new WP_User($user_id);
    }
    user_load_orgchart($user);
    $user->orgchart_id;
    return;
}
function get_user_chart_dropdown($user = null, $echo = true) {
    kpi_list_charts_tree();
}

function get_user_orgchart_dropdown($selected_id = '', $echo = true, $types = ['', CHART_ALIAS_GROUP, CHART_ALIAS_MANAGER, CHART_ALIAS_DEPARTMENTS, CHART_ALIAS_EMPLOYEES], $field = 'name', $allow_chart_ids = [], $is_array = false ) {
    require_once THEME_DIR . '/inc/lib-orgchart.php';
    $charts = kpi_load_org_charts(true);
    $html = kpi_loop_render_option($selected_id, $charts, '', $types, $field, $allow_chart_ids, $is_array );
    if( $echo && !$is_array ) {
        echo $html;
        return;
    }
    return $html;
}

function kpi_loop_render_option( $selected_id, $charts, $level = '', $types = [], $field = 'name', $allow_chart_ids = [], $is_array = false ) {
    $html = $is_array ? [] : '';
    if( is_array($field) ) {
        foreach ($field as $idx => $name) {
            if( !in_array($name, ['name', 'room', 'alias']) ) {
                $field[$idx] = 'name';
            }
        }
    } else {
        if( !in_array($field, ['name', 'room', 'alias']) ) {
            $field = 'name';
        }
        $field = [$field];
    }

    if( is_array($charts) ) {
        if( in_array($charts['alias'], $types) ) {
            if( empty($allow_chart_ids) || (in_array($charts['id'], $allow_chart_ids)) ) {
                $json = esc_json_attr($charts);
                $texts = [];
                foreach ($field as $name) {
                    $text = apply_filters('kpi_loop_render_option_text', $charts[$name], $charts);
                    if( !empty($text) ) {
                        $texts[] = $text;
                    }

                }
                $texts = apply_filters('kpi_loop_render_option', $texts, $charts, $field);
                if( !empty($texts) ) {
                    $texts = implode(' - ', $texts);
                    if( $is_array ) {
                        $item = $charts;
                        if( array_key_exists('children', $item) ) {
                            unset($item['children']);
                        }
                        $html[$item['id']] = [
                            'text' => sprintf('<option data-type="%s" data-level="%d" value="%d" %s data-info="%s" >%s</option>' . "\n",
                                $item['alias'], $item['level'], $item['id'], ( ( ($item['id'] == $selected_id) ) ? 'selected="selected"' : ''), $json, $level . $texts),
                            'item' => $item
                        ];
                    } else {
                        $html .= sprintf('<option data-type="%s" data-level="%d" value="%d" %s data-info="%s" >%s</option>' . "\n",
                            $charts['alias'], $charts['level'], $charts['id'], (( ($charts['id'] == $selected_id)) ? 'selected="selected"' : ''), $json, $level . $texts);
                    }
                }
            }
        }
        if( array_key_exists('children', $charts) ) {
            foreach($charts['children'] as $k => $chart) {
                if( $is_array ) {
                    $html = array_merge($html, kpi_loop_render_option($selected_id, $chart, "{$level}--- ", $types, $field, $allow_chart_ids, $is_array) );
                } else {
                    $html .= kpi_loop_render_option($selected_id, $chart, "{$level}--- ", $types, $field, $allow_chart_ids, $is_array);
                }
            }
        }
    }
    return $html;
}

function kpi_remove_chuc_danh_pho_phong($text, $chart) {
    # kpi_loop_render_option_text
    if( $chart['alias'] == 'phongban' && preg_match('#(Pho|pho|Phó|phó|ph\.|Ph.) #', $text) ) {
        # echo "{$chart['alias']}: {$text}\n";
        return '';
    }
    return $text;
}
function kpi_remove_chuc_danh_pho($texts, $charts = [], $fields = []) {
    foreach($fields as $idx => $field) {
        if( ($field == 'name') || ($field == 'room') ) {
            if( preg_match('#^(Pho|pho|Phó|phó|ph\.|Ph.) #i', $texts[$idx]) ) {
                # unset($texts[$idx]);
                return [];
            } else {
                unset($texts[$idx]);
            }
        }
    }
    return $texts;
}

function kpi_get_departments_dropdown($selected_id = '', $show_root = false ) {
    require_once THEME_DIR . '/inc/lib-orgchart.php';
    $charts = kpi_load_org_charts(false);
    $html = '';
    if( is_array($charts) ) {
        foreach($charts as $chart) {
            if( ( ($show_root && ($chart['parent'] == '0') ) ) ||
                (($chart['alias'] == 'phongban') && !(preg_match('#^(pho|phó|Pho|Phó) #', $chart['room']) || preg_match('#^(pho|phó|Pho|Phó) #', $chart['name']) ) ) ) {
                $json = esc_json_attr($chart);
                $html .= sprintf('<option data-level="%d" value="%d" %s data-info="%s" >%s</option>'."\n",
                    $chart['level'], $chart['id'], (( ($chart['id'] == $selected_id)) ? 'selected="selected"' : ''), $json, $chart['room']);
            }
        }
    }
    return $html;
}

function kpi_user_orgchart_no_kpi_dropdown($chart_id, $selected_id = '', $echo = true, $field = 'name') {
    $nodes = kpi_find_chart_nodes_of_id($chart_id);
    $html = '';
    $chart = kpi_orgchart_get_by('id', $chart_id, ARRAY_A);
    if( is_array($field) ) {
        foreach ($field as $idx => $name) {
            if( !in_array($name, ['name', 'room', 'alias']) ) {
                $field[$idx] = 'name';
            }
        }
    } else {
        if( !in_array($field, ['name', 'room', 'alias']) ) {
            $field = 'name';
        }
        $field = [$field];
    }

	$startLevel = $chart ? (int)$chart['level'] : 0;
    foreach($nodes as $node) {
        $texts = [];
        foreach ($field as $name) {
            $texts[] = $node[$name];
        }

        $texts = apply_filters('kpi_user_orgchart_no_kpi_dropdown', $texts, $node, $field);
        if( !empty($texts) ) {
            $texts = implode(' - ', $texts);
            $space = '';
            for($i = $startLevel; $i < $node['level']; $i++) {
                $space .= '--- ';
            }
            $selected = ($node['id'] == $selected_id) ? 'selected="selected"' : '';
            $json = esc_json_attr($node);
            $html .= sprintf('<option data-level="%d" value="%d" %s data-info="%s">%s</option>', $node['id'], ($node['level'] - $startLevel), $selected, $json, $space . $texts);
        }
    }

    if( $echo ) {
        echo $echo;
    } else {
        return $html;
    }
}

function kpi_get_profile_url($profile_id) {
    static $profile;
    if( !isset($profile) ) {
        $profile = get_page_by_path( PAGE_PROFILE_PATH );
    }
    $url = apply_filters( 'the_permalink', get_permalink( $profile ), $profile );
    $url = add_query_arg( 'profile', $profile_id, $url );
    return $url;
}

function kpi_get_profile_manager_url($profile_id){
	static $profile;
	if( !isset($profile) ) {
		$profile = get_page_by_path( PAGE_USER_MANAGERS );
	}
	$urlManager =  apply_filters( 'the_permalink', get_permalink( $profile ), $profile );
	$urlManager = add_query_arg('profile', $profile_id, $urlManager);
	return $urlManager;
}

function kpi_get_members_url() {
    global $kpi_contents_parts;
    return $kpi_contents_parts['manage-members']['url'];
}

/*
 * Fix wp_redirect sent header
 */
global $kpi_contents_parts;

function kpi_process_query_posts($result) {
    global $kpi_contents_parts;
    $user = wp_get_current_user();
    $is_admin = isset($user->allcaps['level_9']) ? $user->allcaps['level_9'] : false;

    $kpi_contents_parts = [
        # 'dashboard' => ['icon' => 'home', 'key' => PAGE_DASHBOARD, 'text' => __('Dashboard', TPL_DOMAIN_LANG), 'url' => '', 'active' => ''],
        //'organizational-structure' => ['icon' => 'puzzle', 'key' => PAGE_ORGANIZATIONAL_STRUCTURE, 'text' => __('Organizational Structure', TPL_DOMAIN_LANG), 'url' => '', 'active' => ''],
        'organizational-structure' => ['icon' => 'puzzle', 'key' => PAGE_ORGANIZATIONAL_STRUCTURE, 'text' => 'Cơ cấu tổ chức <br/><i>Structure</i>', 'url' => '', 'active' => ''],
        //'manage-members' => ['icon' => 'users', 'key' => PAGE_MANAGE_MEMBERS, 'text' => __('Manage Members', TPL_DOMAIN_LANG), 'url' => '', 'active' => ''],
        'manage-members' => ['icon' => 'users', 'key' => PAGE_MANAGE_MEMBERS, 'text' => 'Hồ sơ nhân viên<br/><i>Resource</i>', 'url' => '', 'active' => ''],

        # 'kpi-bank' => ['icon' => 'briefcase', 'key' => PAGE_KPI_BANK, 'text' => __('KPI Bank', TPL_DOMAIN_LANG), 'url' => '', 'active' => ''],
        # 'apply-bsc-kpi-system' => ['icon' => 'settings', 'key' => PAGE_APPLY_KPI_SYSTEM, 'text' => __('Apply BSC-KPI System', TPL_DOMAIN_LANG), 'url' => '', 'active' => ''],
        # 'statistical' => ['icon' => 'bar-chart', 'key' => PAGE_STATISTICAL, 'text' => __('Statistical', TPL_DOMAIN_LANG), 'url' => '', 'active' => ''],
        # 'news' => ['icon' => 'pencil', 'key' => PAGE_NEWS, 'text' => __('News', TPL_DOMAIN_LANG), 'url' => '', 'active' => '']
    ];
    /*if( $is_admin ) {
        //$kpi_contents_parts['kpi-bank'] = ['icon' => 'briefcase', 'key' => PAGE_KPI_BANK, 'text' => __('KPI Bank', TPL_DOMAIN_LANG), 'url' => '', 'active' => ''];
        $kpi_contents_parts['kpi-bank'] = ['icon' => 'briefcase', 'key' => PAGE_KPI_BANK, 'text' => 'Ngân hàng KPI <br/><i>KPI Bank</i>', 'url' => '', 'active' => ''];
    }*/

    //$kpi_contents_parts['apply-bsc-kpi-system'] = ['icon' => 'settings', 'key' => PAGE_APPLY_KPI_SYSTEM, 'text' => __('Apply BSC-KPI System', TPL_DOMAIN_LANG), 'url' => '', 'active' => ''];
    #$kpi_contents_parts['apply-bsc-kpi-system'] = ['icon' => 'settings', 'key' => PAGE_APPLY_KPI_SYSTEM, 'text' => 'Đăng ký & đánh giá KPI <br/><i>KPI Plan & Result</i>', 'url' => '', 'active' => ''];
    $kpi_contents_parts[PAGE_EFFA_SYSTEM] = ['icon' => 'settings', 'key' => PAGE_EFFA_SYSTEM, 'text' => 'Đánh giá năng lực <br/><i>Competency Assessment</i>', 'url' => '?type=assessment', 'active' => ''];
    //$kpi_contents_parts['news'] = ['icon' => 'pencil', 'key' => PAGE_NEWS, 'text' => __('News', TPL_DOMAIN_LANG), 'url' => '', 'active' => ''];
    $kpi_contents_parts['news'] = ['icon' => 'pencil', 'key' => PAGE_NEWS, 'text' => 'Thông báo <br/><i>Announce</i>', 'url' => '', 'active' => ''];

    # $kpi_contents_parts['kpi-bank'] = ['icon' => 'briefcase', 'key' => PAGE_KPI_BANK, 'text' => __('KPI Bank', TPL_DOMAIN_LANG), 'url' => '', 'active' => ''];

    if( $is_admin ) {
        $kpi_contents_parts['settings'] = ['icon' => 'briefcase', 'key' => PAGE_SETTINGS, 'text' => 'Cài đặt <br/><i>Settings</i>', 'url' => '', 'active' => ''];
    }


    global $wp_the_query; /* @var $wp_the_query WP_Query */

    $currentObject = $wp_the_query->get_queried_object();
    /* @var $currentObject WP_Post */
    foreach ($kpi_contents_parts as $k => $item) {
        $page = get_page_by_path($kpi_contents_parts[$k]['key']);
        if( $page instanceof WP_Post ) {
            if( $currentObject instanceof WP_Post ) {
                $kpi_contents_parts[$k]['is_page'] = $currentObject->ID == $page->ID;
            } else {
                $kpi_contents_parts[$k]['is_page'] = false;
            }

            if( $kpi_contents_parts[$k]['is_page'] ) {
                $kpi_contents_parts[$k]['active'] = 'active';
            }
            $kpi_contents_parts[$k]['url'] = apply_filters('the_permalink', get_permalink($page), $page) . $kpi_contents_parts[$k]['url'];
        } else {
        }
    }
    # echo '<pre>'; var_dump(__LINE__, __FILE__, $kpi_contents_parts, $currentObject); exit('</pre>');
    $GLOBALS['kpi_contents_parts'] = $kpi_contents_parts;

    remove_filter('pre_handle_404', 'kpi_process_query_posts');
    return $result;
}

add_action('wp_loaded', function() {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (!empty($_POST['init-pages'])) {
            require_once THEME_DIR . '/inc/init-after-theme-setup.php';
        }
    }
    if( is_admin() ) {
        return;
    } else {
        add_filter('pre_handle_404', 'kpi_process_query_posts');
    }
});

function app_output_buffer()
{

    # ob_start();
}

// soi_output_buffer
add_action('init', 'app_output_buffer');


add_action("login_form_login", function () {
    get_header('login');
    include_once __DIR__ . '/contents/content-login.php';
    get_footer('login');
    exit;
});

# include register taxonomy with post type is post
include_once "inc/register-taxonomy.php";

include_once "inc/taxonomy-types_of_post.php";

function kpi_filter_editable_roles($roles)
{
    $useRoles = get_option('allow_user_levels', ['administrator', 'level_0', 'level_1', 'level_2']);
    // ['editor', 'author', 'contributor', 'subscriber']
    foreach ($roles as $n => &$role) {
        # if( in_array( $n, ['editor', 'author', 'contributor', 'subscriber']) ) { # 'administrator',
        if (!in_array($n, $useRoles)) { # 'administrator',
            unset($roles[$n]);
        } else {
            $roles[$n]['name'] = __($roles[$n]['name'], TPL_DOMAIN_LANG);
        }
    }
    # echo '<pre class="editable_roles">'; var_dump($roles); echo '</pre>';
    return $roles;
}
function kpi_filter_editable_roles_managers( $roles ){
	$useRoles = get_option('allow_user_levels', ['user_managers', 'user_employers']);
	// ['editor', 'author', 'contributor', 'subscriber']
	foreach ($roles as $n => &$role) {
		# if( in_array( $n, ['editor', 'author', 'contributor', 'subscriber']) ) { # 'administrator',
		if (!in_array($n, $useRoles)) { # 'administrator',
			unset($roles[$n]);
		} else {
			$roles[$n]['name'] = __($roles[$n]['name'], TPL_DOMAIN_LANG);
		}
	}
	# echo '<pre class="editable_roles">'; var_dump($roles); echo '</pre>';
	return $roles;
}
add_action('init', function () {
    if (is_admin()) {
        require_once __DIR__ . '/inc/handle-admin-settings.php';
    }
});



function Gradient($HexFrom, $HexTo, $ColorSteps) {
    if( $ColorSteps == 1 ){
        $ColorSteps = 1.1;
    }

    $FromRGB['r'] = hexdec(substr($HexFrom, 0, 2));
    $FromRGB['g'] = hexdec(substr($HexFrom, 2, 2));
    $FromRGB['b'] = hexdec(substr($HexFrom, 4, 2));

    $ToRGB['r'] = hexdec(substr($HexTo, 0, 2));
    $ToRGB['g'] = hexdec(substr($HexTo, 2, 2));
    $ToRGB['b'] = hexdec(substr($HexTo, 4, 2));

    $StepRGB['r'] = ($FromRGB['r'] - $ToRGB['r']) / ($ColorSteps - 1);
    $StepRGB['g'] = ($FromRGB['g'] - $ToRGB['g']) / ($ColorSteps - 1);
    $StepRGB['b'] = ($FromRGB['b'] - $ToRGB['b']) / ($ColorSteps - 1);

    $GradientColors = array();

    for($i = 0; $i <= $ColorSteps; $i++) {
        $RGB['r'] = floor($FromRGB['r'] - ($StepRGB['r'] * $i));
        $RGB['g'] = floor($FromRGB['g'] - ($StepRGB['g'] * $i));
        $RGB['b'] = floor($FromRGB['b'] - ($StepRGB['b'] * $i));

        $HexRGB['r'] = sprintf('%02x', ($RGB['r']));
        $HexRGB['g'] = sprintf('%02x', ($RGB['g']));
        $HexRGB['b'] = sprintf('%02x', ($RGB['b']));

        $GradientColors[] = "#" . implode(NULL, $HexRGB);
    }
    $GradientColors = array_filter($GradientColors, "len");
    return $GradientColors;
}

function render_color_chart($percentItem){
	if( !empty($percentItem) ) {
		$colorStep = round($percentItem, 1);
		if( $percentItem == 1 ){
			$colorStep = 1.1;
		}
		$gradient_1 = Gradient("ff1300", "ffe100", $colorStep);
		$gradient_2 = Gradient("ffe100", "84e91c", $colorStep);
		$gradient_3 = Gradient("84e91c", "84e91c", $colorStep);

	}else{
		$gradient_1 = [];
		$gradient_2 = [];
		$gradient_3 = [];
	}
	$backgroundColor = array_merge($gradient_1, $gradient_2, $gradient_3);
	if( $percentItem == 1 ){
		$backgroundColor = array_merge($gradient_1, $gradient_2, [$gradient_3[0]]);
	}
	return $backgroundColor;
}

function len($val){
    return (strlen($val) == 7 ? true : false );
}

if( !function_exists( 'array_group_by' ) ){
    function array_group_by( $array, $key ){
        if( !is_string( $key ) && !is_float( $key ) && !is_int( $key ) && !is_callable( $key ) ){
            trigger_error( "array_group_by: key not exists in array", E_USER_ERROR );
            return false;
        }
        $func = ( !is_string( $key ) && is_callable( $key ) ? $key : null );
        $group = [];
        foreach ( $array as $k => $v ){
            $_key = null;
            if( is_callable( $func ) ){
                call_user_func( $func, $v );
            }elseif( is_object( $v ) && isset( $v->{$key} ) ){
                $_key = $v->{$key};
            } elseif( isset( $v[$key] ) ){
                $_key = $v[$key];
            }
            if( $_key === null )
                continue;
            $group[$_key][] = $v;
        }
        if( func_num_args() > 2 ){
            $args = func_get_args();
            foreach ( $group as $k => $value ){
                $params = array_merge( [$value], array_slice( $args, 2, func_num_args() ) );
                $group[$k] = call_user_func_array('array_group_by', $params );
            }
        }
        return $group;
    }
}
require_once 'inc/config.php';

#define('WP_DEBUG_ERROR', true);
function kpi_debug() {
    return defined('WP_DEBUG_ERROR') && WP_DEBUG_ERROR ? true : false;
}

if( kpi_debug() ) {
    register_shutdown_function(function() {
        $error = error_get_last();
        if( !empty($error) ) {
            echo '<pre>'; var_dump($error); echo '</pre>';
        }
    });
}

define('USER_CAP_MANAGERS',      'user_managers');
define('USER_CAP_EMPLOYERS',      'user_employers');

# 'luu-nhap','cho-duyet-dang-ky','ket-qua-kpi','cho-duyet-kq-kpi','hoan-tat'
define('KPI_STATUS_DRAFT',      'luu-nhap');
define('KPI_STATUS_PENDING',    'cho-duyet-dang-ky');
define('KPI_STATUS_RESULT',     'ket-qua-kpi');
define('KPI_STATUS_WAITING',    'cho-duyet-kq-kpi');
define('KPI_STATUS_DONE',       'hoan-tat');
# capacity
define('KPI_STATUS_PENDING_PUBLISH',     'cho-trien-khai');
# 'on_off', 'oscillate', 'add_max_kq', 'add_between'
define('KPI_FORMULA_ON_OFF', 'on_off');
define('KPI_FORMULA_OSCILLATE', 'oscillate');
define('KPI_FORMULA_ADD_MAX_KQ', 'add_max_kq');
define('KPI_FORMULA_ADD_BETWEEN', 'add_between');
# 'ty', 'so_lan', 'thoi_gian', 'ty_le', 'so_tien'
define('KPI_UNIT_TY', 'ty');
define('KPI_UNIT_SO_LAN', 'so_lan');
define('KPI_UNIT_THOI_GIAN', 'thoi_gian');
define('KPI_UNIT_TY_LE', 'ty_le');
define('KPI_UNIT_PERCENT', '%');
define('KPI_UNIT_SO_TIEN', 'so_tien');

# 'node_start','node_middle','node_end'
define('KPI_CREATE_BY_NODE_START', 'node_start');
define('KPI_CREATE_BY_NODE_MIDDLE', 'node_middle');
define('KPI_CREATE_BY_NODE_END', 'node_end');

define('KPI_CHUA_DUYET', 'Chưa duyệt');
define('KPI_DA_DUYET', 'Đã duyệt');

#Kém, Cần cải thiện, Đạt, Rất tốt, Xuất sắc
define('RANK_POOR', 'Kém');
define('RANK_NEED_TO_IMPROVE', 'Cần cải thiện');
define('RANK_REACHED', 'Đạt');
define('RANK_VERYGOOD', 'Rất tốt');
define('RANK_EXCELLENT', 'Xuất sắc');
#apply_of capacity
define('APPLY_OF_MANAGER', 'quan-ly');
define('APPLY_OF_EMPLOYEES', 'nhan-vien');
#kpi-ca-nam, tieu-chi-nang-luc, ket-qua-qua-trinh
define('CAPACITY_CA_NAM', 'kpi-ca-nam');
define('CAPACITY_TIEU_CHI', 'tieu-chi-nang-luc');
define('CAPACITY_KET_QUA_QUA_TRINH', 'ket-qua-qua-trinh');
#nhanxetchung, chitiet
define('CAPACITY_TYPE_NOTE_CT', 'chitiet');
define('CAPACITY_TYPE_NOTE_NXC', 'nhanxetchung');
#di-xuong, on-dinh, di-len
define('CAPACITY_NX_XUONG', 'di-xuong');
define('CAPACITY_NX_ON_DINH', 'on-dinh');
define('CAPACITY_NX_LEN', 'di-len');
$GLOBALS['capacity_nx'] = [
	CAPACITY_NX_XUONG => 'Đi xuống',
	CAPACITY_NX_ON_DINH => 'Ổn định',
	CAPACITY_NX_LEN => 'Đi lên',
];

define('CAPACITY_EXPORT_EMPLOYEES', 'Phieu-danh-gia-KPI-ca-nhan(Hang-nam).xlsx');
define('CAPACITY_EXPORT_MANAGER', 'Phieu-danh-gia-KPI-cap-quan-ly(Hang-nam).xlsx');

define('COMPETENCY_EXPORT', 'Nang-Luc-Ca-Nhan.xls');
define('COMPETENCY_EXPORT_LIST_STAFF', 'Danh-sach-nang-luc.xls');
define('COMPETENCY_EXPORT_FOR_COMPANY', 'Danh-sach-nang-luc-theo-cong-ty.xls');

define('BEHAVIOR', 'kpi-thai-do-hanh-vi');

$GLOBALS['kpi_all_status'] = [
    KPI_STATUS_DRAFT => 'Lưu nháp',
    KPI_STATUS_PENDING => 'Chờ duyệt đăng ký',
    KPI_STATUS_RESULT => 'Đã triển khai',
    KPI_STATUS_WAITING => 'Chờ duyệt kết quả',
    KPI_STATUS_DONE => 'Hoàn tất',
];


if ( defined('WP_USE_THEMES') && WP_USE_THEMES ) {
    add_action('template_redirect', function() {
        require_once THEME_DIR . '/inc/lib-years.php';
        # $firstYear = kpi_get_first_year();
        $user = wp_get_current_user();
        if( $user->exists() ) {
            $orgchart = user_load_orgchart($user);
        } else {
            $orgchart = null;
        }

        # $firsts = ['year' => null, 'precious' => null, 'month' => null];
	    $year = !empty( $_GET['nam'] ) ? $_GET['nam'] : date("Y", time());
        $firsts = kpi_get_firsts( 0, ($orgchart && $orgchart->role == 'bgd') ? '' : KPI_YEAR_STATUS_PUBLISH, $year );
        $nam = $_GET['nam'] = $_REQUEST['nam'] = !empty($_REQUEST['nam']) ? $_REQUEST['nam'] : ($firsts['year'] ? $firsts['year']['year'] : $year );
        $quy = $_GET['quy'] = $_REQUEST['quy'] = isset($_REQUEST['quy']) ? $_REQUEST['quy'] : '';
            #($firsts['precious'] ? $firsts['precious']['precious'] : 1 );
        $thang = $_GET['thang'] = $_REQUEST['thang'] = !empty($_REQUEST['thang']) ? $_REQUEST['thang'] : '';
        #( (!empty( $firsts['month'] ) && !empty( $firsts['month']['month'] ) ) ? $firsts['month']['month'] : 1 );

        if( !defined('TIME_YEAR_VALUE') ) {
            define('TIME_YEAR_VALUE', $nam);
        }
        if( !defined( 'TIME_PRECIOUS_VALUE' ) ){
            define('TIME_PRECIOUS_VALUE', $quy);
        }
        if( !defined( 'TIME_MONTH_VALUE' ) ){
            define('TIME_MONTH_VALUE', $thang);
        }
    });
}
if( !defined( 'TIME_YEAR_VALUE' ) || TIME_YEAR_VALUE == 0 ) {
	require_once THEME_DIR . '/inc/lib-years.php';
	$year_list = kpi_get_year_list( 0,KPI_YEAR_STATUS_PUBLISH );
	if( !empty( $year_list ) ){
        $year_first = null;
        # echo '<pre>'; var_dump($year_list); echo '</pre>'; exit;
        $now = date('Y', time());
		foreach($year_list as $year) {
		    if( $year['year'] == $now ) {
                $year_first = $year;
		        break;
            }
        }
        if( empty($year_first) ) {
            $year_first = $year_list[0];
        }
		$nam = $_GET['nam'] = $_REQUEST['nam'] = !empty($_REQUEST['nam']) ? $_REQUEST['nam'] : ($year_first['year'] ? $year_first['year'] : 0 );
		define('TIME_YEAR_VALUE', $nam);
	}
}

define('KPI_FORMAT_DATE', 'd-m-Y');
define('KPI_FORMAT_DATETIME', 'd-m-Y H:i:s');

function kpi_mysql_date($date) {
    $date = preg_replace('#^(\d{1,2})\-(\d{1,2})\-(\d{2,4})#', '$3-$2-$1', $date);
    $date = preg_replace('#^(\d{1,2})\/(\d{1,2})\/(\d{2,4})#', '$3-$2-$1', $date);
    if( strlen($date) == 10 ) {
        $date = "{$date} 00:00:00";
    }
    return $date;
}

function kpi_format_date($date) {
    return preg_replace('#^(\d{4})\-(\d{1,2})\-(\d{1,2})#', '$3-$2-$1', $date);
}
function kpi_format_dmy_to_ymd( $date ){
	return preg_replace('#^(\d{1,2})(\-|\/)(\d{1,2})(\-|\/)(\d{4})#', '$5$4$3$2$1', $date);
}

function user_is_manager($user = null) {
    if ( empty( $user ) ) {
        $user = wp_get_current_user();
    }
    if( !empty($user->allcaps['level_9']) || $user->has_cap('level_9') ){
        return true;
    }
    return false;

}

function has_cap($user, $cap){
	$caps = $user->caps;
	if( array_key_exists( $cap, $caps ) ){
		return true;
	}
	return false;
}

/*add_action('wp_logout', function() {
    $user = wp_get_current_user();
    return;
}, 100);*/
function custom_wp_login($user_login, $user) {
	global $wpdb;
	$blog_id = get_current_blog_id();
	$prefix = $wpdb->get_blog_prefix($blog_id);
	$log_table = "`{$prefix}login_logs`";
	wp_auth_check_load();
	return;
}
add_action('wp_login', 'custom_wp_login', 100, 2);

require_once THEME_DIR . '/inc/effa.php';
require_once THEME_DIR . '/inc/quiz.php';
require_once THEME_DIR . '/inc/evaluate.php';

/*register_shutdown_function(function() {
    $error = error_get_last();
    if( !empty($error) ) {
        echo '<pre>'; var_dump($error); echo '</pre>';
    }
});*/