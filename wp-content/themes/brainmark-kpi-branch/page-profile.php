<?php
/**
 * Template Name: My Account
 * Created by PhpStorm.
 * User: henry
 * Date: 12/22/17
 * Time: 22:29
 */

global $kpi_contents_parts;

$user = wp_get_current_user();
$create_users = isset($user->allcaps['create_users']) ? $user->allcaps['create_users'] : false; # $user->has_cap('create_users');
$edit_users = isset($user->allcaps['edit_users']) ? $user->allcaps['edit_users'] : false; # $user->has_cap('edit_users');

$profile_id = '';
if( isset($_GET['profile']) && ($create_users || $edit_users) && !isset($_REQUEST['changepass'])) {
    $profile_id = $_GET['profile'];
}
$templete = '';
$updateUser = false;

if( !empty($profile_id) && is_numeric($profile_id) && ($profile_id > 0) && $edit_users ) {
    $templete = 'edit';
    // Edit Profile
}else if( $create_users ) {
    // Create New
	if( !empty( $_GET['profile'] ) && $_GET['profile'] == 'new' ) {
		$templete = 'new';
	}else{
		$templete = 'edit-password';
	}
} else {
	$templete = 'edit-password';
}

if( !empty($templete) && !isset($_POST['changepass']) && ($_SERVER['REQUEST_METHOD'] == 'POST') ) {
    require_once THEME_DIR . '/inc/lib-users.php';
    $wpnonce = !empty($_POST['_wpnonce']) ? $_POST['_wpnonce'] : '';

    $referrer = !empty($_POST['_referrer']) ? str_replace('%26', '&', $_POST['_referrer']) : '';

    if( $templete == 'new' ) {
        if( wp_verify_nonce($wpnonce, 'create_user') ) {
            $user = kpi_user_create_new($_POST);
        } else {
            $user = new WP_Error(USER_ERROR_INVALID_NONCE, __("Create user: nonce invalid", TPL_DOMAIN_LANG));
        }
        if( is_wp_error($user) ) {
            add_action('create_user_result', function() use ( $user ) {
                echo sprintf('<div class="col-lg-12 col-md-12 col-sm-12 error error-%s">%s</div>', $user->get_error_code(), $user->get_error_message() );
            });
        } else {
        	$user = get_user_by("ID", $user);
	        user_send_mail_user( $user, $_POST['user_pass'] );
            wp_redirect( !empty($referrer) ? $referrer : kpi_get_members_url( ) );
            # wp_redirect( kpi_get_profile_url( $user ) );
            exit;
        }
    } else {
        if( wp_verify_nonce($wpnonce, 'update_user') ) {
            $updateUser = kpi_user_update_info($_POST);
        } else {
            $updateUser = new WP_Error(USER_ERROR_INVALID_NONCE, __("Update user: nonce invalid", TPL_DOMAIN_LANG));
        }

        if( is_wp_error($updateUser) ) {
            add_action('edit_user_result', function() use ( $updateUser ) {
                echo sprintf('<div class="col-lg-12 col-md-12 col-sm-12 error error-%s">%s</div>', $updateUser->get_error_code(), $updateUser->get_error_message() );
            });
        } else {
            if( !empty($_POST['bnt']) && ($_POST['bnt'] == 'save_exit') ) {
                wp_redirect( !empty($referrer) ? $referrer : kpi_get_members_url( ) );
            } else {
                wp_redirect( add_query_arg('_referrer', $referrer, kpi_get_profile_url( $profile_id ) ) );
            }
            exit;
        }
    }
}


get_header();

if( !empty($templete) ) {
	if( $templete == 'new' ) {
		get_template_part( 'contents/users/user', $templete );
	}elseif( $templete == 'edit' ){
		get_template_part( 'contents/users/user', $templete );
	}else{
		get_template_part( 'contents/users/user', $templete );
	}
}
get_footer();