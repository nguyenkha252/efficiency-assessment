<?php
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
    die('This example should only be run from a Web Browser');


require_once __DIR__ . '/inc/vendor/autoload.php';
# require_once __DIR__ . '/inc/phpexcel/Classes/PHPExcel.php';

require_once __DIR__ . '/inc/pdf.php';
# var_dump( class_exists('PHPExcel_Writer_PDF_PDF') ); exit;
# PHPExcel_Writer_PDF_PDF

# phpinfo(); exit;
# use Mpdf\Mpdf as mpdf;

// $objPHPExcel = new PHPExcel();
# PHPExcel_IOFactory::createReader();
$objPHPExcel = PHPExcel_IOFactory::load(__DIR__ . '/assets/template/exports/Mau 3-Phieu danh gia KPI cap quan ly (Hang quy).xlsx');
# $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
/* @var $writer PHPExcel_Writer_Excel2007 */

# $pdf = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
/* @var $pdf PHPExcel_Writer_PDF_mPDF */

# PHPExcel/Writer/Excel2007.php
/*
$rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
$rendererLibrary = 'Pdf';
# /Users/henry/Documents/PHP/Hien/brainmark-kpi/wp-content/themes/brainmark-kpi-branch/inc/phpexcel/Examples/01simple-download-pdf.php
# /Users/henry/Documents/PHP/Hien/brainmark-kpi/wp-content/themes/brainmark-kpi-branch/inc/phpexcel/Examples/
$rendererLibraryPath = ''; # __DIR__ . '/inc/vendor/mpdf/mpdf/src';


if (!PHPExcel_Settings::setPdfRenderer(
    $rendererName,
    $rendererLibraryPath
)) {
    die(
        'NOTICE: Please set the $rendererName and $rendererLibraryPath values' .
        '<br />' .
        'at the top of this script as appropriate for your directory structure'
    );
}
*/

# $mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/tmp']);
# $mpdf = new \Mpdf\Mpdf();
#$mpdf->WriteHTML('<h1>Hello world!</h1>');
#$mpdf->Output();

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/pdf');
header('Content-Disposition: attachment;filename="01simple.pdf"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Pdf');
$objWriter->save('php://output');
exit;
