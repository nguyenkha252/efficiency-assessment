<?php
/**
 * Template Name: Statistical
 */

include_once __DIR__ . '/../brainmark-kpi-main/login.php';

if ( !is_user_logged_in() ) {
    get_header('login');
    get_template_part( 'contents/content', 'login' );
    get_footer('login');
} else {
    get_header();
    ?>
    <h1>Thống Kê</h1>
    <?php
    get_footer();
}
