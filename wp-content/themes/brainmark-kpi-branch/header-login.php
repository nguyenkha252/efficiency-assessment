<!DOCTYPE html>
<!--[if IE 8]> <html <?php language_attributes(); ?> class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html <?php language_attributes(); ?> class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html <?php language_attributes(); ?>>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title><?php wp_title('');  bloginfo( 'name' ); ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <link rel="shortcut icon" href="<?php echo THEME_URL; ?>/favicon.png" />
        <?php wp_head(); ?>
        <link rel='stylesheet' href="<?php echo THEME_URL; ?>/style_customize.css" />
    </head>
    <!-- END HEAD -->
