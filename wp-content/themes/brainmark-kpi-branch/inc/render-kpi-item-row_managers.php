<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 2/5/18
 * Time: 17:46
 */

function kpi_render_ceo_row_item_managers($item, $kpi_type, $chartCurrent, $chartParent, $members = null, $user) {
    ob_start();
    require_once THEME_DIR . '/inc/lib-kpis.php';
    $unit = !empty( $item['unit'] ) ? getUnit($item['unit']) : '';
    if( is_array($unit) ) {
        $unit = '';
    }
    $item['receive'] = substr(kpi_format_date($item['receive']), 0, 10);

    $need_approve = $chartParent ? true : false;
    $actionColumn = $chartParent ? 6 : 7;

    if( !is_null($members) ) {
        $actionColumn ++;
    }

    #$userManager = wp_get_current_user();
    $orgchart = user_load_orgchart($user);
    $member_role = $orgchart ? $orgchart->role : '';
    if( empty($member_role) ) {
        $member_role = '';
    } else {
        $member_role = strtolower($member_role);
    }
    $member_role = ( ($member_role == 'bgd') || $user->has_cap('level_9') ) ? 'bgd' : '';
    ?>
    <tr class="data-item" data-info="<?php echo esc_json_attr($item); ?>">
            <?php
            $name = ($item['status'] == KPI_STATUS_RESULT) ? 'checked="checked" disabled="disabled"' :
	            ( ( ($item['status'] != KPI_STATUS_DRAFT) ? 'disabled="disabled"' : '') . 'name="register['.$item['id'].'][ID]"' );
            $status = ($item['status'] == KPI_STATUS_RESULT) ? 'disabled="disabled"' :
	            ( ( ($item['status'] != KPI_STATUS_DRAFT) ? 'disabled="disabled"' : '') . 'name="register['.$item['id'].'][status]"' );
            if( $item['status'] == KPI_STATUS_PENDING ){
                $checked = "checked";
            }else{
                $checked = '';
            }
            $checkbox = ('<input class="checkbox-status" '.$name.' value="'.$item['id'].'" type="checkbox" '.$checked.' >');
            ?>
        <td class="column-1 kpi-id align-center">
	        <?php
           if( in_array( $item['status'], [KPI_STATUS_DRAFT, KPI_STATUS_PENDING] ) ){
	           echo '<label class="form-control switch status">
                    ' . $checkbox . '
                    <span class="checkbox-slider round"></span>
                </label>';
	           echo '<input type="hidden" ' . $status . ' value="' . $item['status'] . '"/>';
           }
            echo $item['id'];
           if( !empty( $item['influence'] ) && $item['influence'] == 'yes' ){
	           $classNode = 'color-influence';
           }else {
	           $classNode = getColorStar( $item['create_by_node'] );
           }
	        if( $item['required'] == 'yes' ): ?>
                <i class="fa fa-star <?php esc_attr_e($classNode); ?>" aria-hidden="true"></i>
	        <?php endif; ?>
        </td>
        <td class="column-2 kpi-content">
            <?php echo $item['post_title']; ?>
            <?php
                $arrFormula = [
                    'html' => render_html_formula($item['formulas'], $item['formula_type']),
                    'title' => $item['formula_title'],
                    'note' => !empty($item['note']) ? $item['note'] : '',
                ]
            ?>
            <a data-target="#formula-popup" data-toggle="modal" href="javascript:;" class="view-formula" data-formula="<?= esc_json_attr($arrFormula); ?>"><i class="fa fa-info-circle"></i></a>
        </td>
        <td class="column-3 kpi-percent align-center"><?php echo $item['percent']; ?>%</td>
        <td class="column-4 kpi-plan"><?php echo "{$item['plan']} ".$unit.""; ?></td>
        <td class="column-5 kpi-receive align-center"><?php echo $item['receive']; ?></td>
        <td class="column-6 kpi-aproved align-center status-<?php echo $item['status']; ?>"><?php
            if( $item['required'] != 'yes' ) {
	           echo '<span class="text">' . $GLOBALS['kpi_all_status'][ $item['status'] ] . '</span>';
            }else{
	            echo '<span class="text">' . $GLOBALS['kpi_all_status'][ $item['status'] ] . '</span>';
            }
        ?></td><?php
        $checkChild = kpi_get_kpi_by_parent( $item['id'] );
	    $classAssign = '';
        if( !empty( $checkChild ) ){
            $classAssign = "bgcolor-orange";
        }
        if( !is_null($members) && is_array($members) ):
            foreach ($members as $idx => $u):
                $members[$idx]['kpi'] = kpi_get_kpi_by_user($u['ID'], $item['id']);
            endforeach;
            ?>
        <td class="column-<?php echo ($actionColumn-1); ?> kpi-members align-center">
            <?php
                if( $item['status'] == KPI_STATUS_RESULT ) {
	                $number     = count( $members );
	                $assign_url = esc_attr( add_query_arg( [], admin_url( 'admin-ajax.php' ) ) );
	                echo # '<span class="number-members">'.$number.' members </span>'.
		                '<a class="btn btn-sm-default '.$classAssign.'" href="javascript:;" data-url="' . $assign_url . '"  
                title="Triển khai" data-toggle="modal" data-kpi-id="' . $item['id'] . '" data-kpi-type="' . $kpi_type . '"
                data-bank-title="' . esc_attr( $item['post_title'] ) . '" data-department="' . esc_json_attr( $chartCurrent ) . '"
                data-target="#tpl-department-kpi-members" data-members="' . esc_json_attr( $members ) . '"
                data-target1="#tab-pane-popup-' . strtolower( $kpi_type ) . '">Triển khai</a>';
                }
            ?>
        </td>
        <?php endif; ?>
        <td class="column-<?php echo $actionColumn; ?> kpi-action align-center">
            <?php
                $action_type = 'department';
                if( $member_role == 'bgd' ):
                    $action_type = 'ceo';
                endif;
                ?>
                <?php
                    $load_params = [
                        'action' => 'load-'.$action_type.'-target',
                        'id' => $item['year_id'],
                        'kpi_id' => $item['id'],
                        'kpi_type' => $kpi_type,
                        'chart_id' => $item['chart_id'],
                        'bank_id' => $item['bank_id'],
                        'year' => $item['year'],
                        '_wpnonce' => wp_create_nonce('load-'.$action_type.'-target'),
                    ];
                    ?>
                <a href="javascript:;" data-type="get"
                   data-url="<?php echo esc_attr( add_query_arg($load_params, admin_url('admin-ajax.php') ) ); ?>"
                   data-id="<?= $item['id'];
                   ?>" class="action-edit" data-ac="edit" title="<?php _e('Chỉnh sửa', TPL_DOMAIN_LANG);
                ?>" data-kpi-type="<?php echo $kpi_type; ?>">
                    <span class="glyphicon glyphicon-pencil"></span>
                </a>
                <?php
                    $remove_params = [
                        'action' => 'remove-'.$action_type.'-target',
                        'id' => $item['year_id'],
                        'kpi_id' => $item['id'],
                        'kpi_type' => $kpi_type,
                        'cid' => $_GET['cid'],
                        'uid' => $_GET['uid'],
                        '_wpnonce' => wp_create_nonce('remove-'.$action_type.'-target'),
                    ];
                ?>
                <a href="javascript:;" data-url="<?php echo esc_attr( add_query_arg($remove_params, admin_url('admin-ajax.php') ) ); ?>"
                   title="Xóa" data-type="post" data-data-type="json" data-target="#confirm-popup"
                   data-loading-element="tr.data-item" data-remove-element="tr.data-item" data-toggle="modal"
                   data-message="Bạn có muốn xóa KPI này <?php echo esc_attr("\"{$item['post_title']}\"") ?> không?"
                ><span class="glyphicon glyphicon-trash"></span></a>

            <?php ?>
        </td>
    </tr>
    <?php
    $html = ob_get_clean();
    return $html;
}

function kpi_render_ceo_duyet_row_item_managers($item, $kpi_type, $chartCurrent, $chartParent, $members = null) {
    ob_start();
    require_once THEME_DIR . '/inc/lib-kpis.php';
    $unit = getUnit($item['unit']);
    if( is_array($unit) ) {
        $unit = '';
    }
    $item['receive'] = substr(kpi_format_date($item['receive']), 0, 10);

    $need_approve = $chartParent ? true : false;
    $actionColumn = $chartParent ? 6 : 7;

    if( !is_null($members) ) {
        $actionColumn ++;
    }

    $user = wp_get_current_user();
    $orgchart = user_load_orgchart($user);
    $member_role = $orgchart ? $orgchart->role : '';
    if( empty($member_role) ) {
        $member_role = '';
    } else {
        $member_role = strtolower($member_role);
    }
    $member_role = ( ($member_role == 'bgd') || $user->has_cap('level_9') ) ? 'bgd' : '';
    ?>
    <tr class="data-item" data-info="<?php echo esc_json_attr($item); ?>">
            <?php
            $name = ($item['status'] == KPI_STATUS_RESULT) ? 'checked="checked" disabled="disabled"' :
	            ( ( ($item['status'] != KPI_STATUS_DRAFT) ? '' : '') . 'name="register['.$item['id'].'][ID]"' );
            $status = ($item['status'] == KPI_STATUS_RESULT) ? '' :
	            ( ( ($item['status'] != KPI_STATUS_DRAFT) ? '' : '') . 'name="register['.$item['id'].'][status]"' );
            $checkbox = ('<input class="checkbox-status" '.$name.' value="'.$item['id'].'" type="checkbox" >');
            ?>
        <td class="column-1 kpi-id align-center">
	        <?php
           if( ( $item['required'] != 'yes' || $item['required'] != 'no' ) && in_array( $item['status'], [KPI_STATUS_DRAFT, KPI_STATUS_PENDING] ) ){
	           echo '<label class="form-control switch status">
                    ' . $checkbox . '
                    <span class="checkbox-slider round"></span>
                </label>';
	           echo '<input type="hidden" ' . $status . ' value="' . $item['status'] . '"/>';
           }
            echo $item['id'];
	        $classNode = getColorStar( $item['create_by_node'] );
	        if( $item['required'] == 'yes' ): ?>
                <i class="fa fa-star <?php esc_attr_e($classNode); ?>" aria-hidden="true"></i>
	        <?php endif; ?>
        </td>
        <td class="column-2 kpi-content">
	        <?php if( isset($_GET['approvereg']) ): ?>
		        <?php
		        $amountApproved = count_kpi_by_year_and_user( $item['year_id'], $item['id'], $item['user_id'], KPI_STATUS_PENDING );
		        if( !empty( $amountApproved ) ){
			        $amountPending = $amountApproved['amount_status'];
		        }
		        $htmlAmountPending = "";
		        if( !empty( $amountPending ) ){
			        $htmlAmountPending = "<span class='amount-pending'>{$amountPending}</span>";
		        }
		        ?>
		        <?php if( in_array( $item['status'], [KPI_STATUS_RESULT] ) ):
			        # kiểm tra KPI được tạo theo quý hay theo tháng
			        #$getYear = kpi_get_year_by_id( $item['year_id'] );
			        /*if( $getYear['kpi_time'] == 'quy' ):
				        ?>
                    <a href="javascript:;" data-action="load_personal_target_for_precious" data-method="get" data-id="<?= $item['id']; ?>" class="action-edit personal-action-edit" data-edit="" data-toggle="modal" data-target="#tpl-personal-target-precious-<?= 'work'; ?>"><span><?php esc_attr_e($item['post_title']); ?></span></a><?= $htmlAmountPending; ?>
				        <?php
			        else:
				        ?>
                    <a href="javascript:;" data-action="load_personal_target_for_month" data-method="get" data-id="<?= $item['id']; ?>" class="action-edit personal-action-edit" data-edit="" data-toggle="modal" data-target="#tpl-personal-target-<?= 'work'; ?>"><span><?php esc_attr_e($item['post_title']); ?></span></a><?= $htmlAmountPending; ?>
			        <?php endif; ?>
		        <?php else: */?>
                    <span><?php esc_attr_e($item['post_title']); ?></span>
		        <?php endif; ?>
            <?php else: ?>
                <?php echo $item['post_title']; ?>
            <?php endif; ?>
        </td>
        <td class="column-3 kpi-percent align-center"><?php echo $item['percent']; ?>%</td>
        <td class="column-4 kpi-plan"><?php echo "{$item['plan']} ".$unit.""; ?></td>
        <td class="column-5 kpi-receive align-center"><?php echo $item['receive']; ?></td>
        <td class="column-6 kpi-aproved align-center status-<?php echo $item['status']; ?>"><?php
            if( $item['required'] != 'yes' ) {
	           echo '<span class="text">' . $GLOBALS['kpi_all_status'][ $item['status'] ] . '</span>';
            }else{
	            echo '<span class="text">' . $GLOBALS['kpi_all_status'][ $item['status'] ] . '</span>';
            }
        ?></td><?php
        if( !is_null($members) && is_array($members) ):
            foreach ($members as $idx => $u):
                $members[$idx]['kpi'] = kpi_get_kpi_by_user($u['ID'], $item['id']);
            endforeach;
            ?>
        <?php endif; ?>
        <td class="column-<?php echo $actionColumn; ?> kpi-action align-center">
            <?php
                $action_type = 'department';
                if( $member_role == 'bgd' ):
                    $action_type = 'ceo';
                endif;
                ?>
                <?php if( in_array($item['status'], [KPI_STATUS_DRAFT, KPI_STATUS_PENDING] ) && ( ($user->ID == $item['user_id']) || !$item['aproved']) ):
                    $load_params = [
                        'action' => 'load-'.$action_type.'-target',
                        'id' => $item['year_id'],
                        'kpi_id' => $item['id'],
                        'kpi_type' => $kpi_type,
                        'chart_id' => $item['chart_id'],
                        'bank_id' => $item['bank_id'],
                        'year' => $item['year'],
                        '_wpnonce' => wp_create_nonce('load-'.$action_type.'-target'),
                    ];
                    ?>
                <a href="javascript:;" data-type="get"
                   data-url="<?php echo esc_attr( add_query_arg($load_params, admin_url('admin-ajax.php') ) ); ?>"
                   data-id="<?= $item['id'];
                   ?>" class="action-edit" data-ac="edit" title="<?php _e('Chỉnh sửa', TPL_DOMAIN_LANG);
                ?>" data-kpi-type="<?php echo $kpi_type; ?>">
                    <span class="glyphicon glyphicon-pencil"></span>
                </a>
	        <?php endif; ?>
                <?php if( (!in_array($item['status'], [KPI_STATUS_RESULT, KPI_STATUS_WAITING, KPI_STATUS_DONE] ) ) && ( ( $user->ID == $item['user_id']) || !$item['aproved']) ):
                    $remove_params = [
                        'action' => 'remove-'.$action_type.'-target',
                        'id' => $item['year_id'],
                        'kpi_id' => $item['id'],
                        'kpi_type' => $kpi_type,
                        'cid' => $_GET['cid'],
                        '_wpnonce' => wp_create_nonce('remove-'.$action_type.'-target'),
                    ];
                ?>
                <a href="javascript:;" data-url="<?php echo esc_attr( add_query_arg($remove_params, admin_url('admin-ajax.php') ) ); ?>"
                   title="Xóa" data-type="post" data-data-type="json" data-target="#confirm-popup"
                   data-loading-element="tr.data-item" data-remove-element="tr.data-item" data-toggle="modal"
                   data-message="Bạn có muốn xóa KPI này <?php echo esc_attr("\"{$item['post_title']}\"") ?> không?"
                ><span class="glyphicon glyphicon-trash"></span></a>
                <?php endif; ?>
            <?php ?>
        </td>
    </tr>
    <?php
    $html = ob_get_clean();
    return $html;
}
