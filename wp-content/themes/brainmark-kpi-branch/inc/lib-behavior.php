<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 07/03/2018
 * Time: 23:24
 */
function behavior_get_by_id($id){
	global $wpdb;
	static $_results = [];
	$__key = md5(serialize(func_get_args()));
	if (!isset($_results[$__key])) {
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpiBehavior = "{$prefix}behavior";
		$sql = $wpdb->prepare("
          SELECT b.*, p.post_title, p.violation_assessment
          FROM {$tableKpiBehavior} AS b  
          INNER JOIN {$wpdb->posts} AS p ON p.ID = b.bank_id 
          WHERE b.id = %d ", $id);
		$_results[$__key] = $wpdb->get_row( $sql, ARRAY_A );
	}
	return $_results[$__key];
}
function behavior_get_by_id_and_user($id, $userID){
	global $wpdb;
	static $_results = [];
	$__key = md5(serialize(func_get_args()));
	if (!isset($_results[$__key])) {
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpiBehavior = "{$prefix}behavior";
		$sql = $wpdb->prepare("
          SELECT b.*, p.post_title, p.violation_assessment
          FROM {$tableKpiBehavior} AS b  
          INNER JOIN {$wpdb->posts} AS p ON p.ID = b.bank_id 
          WHERE b.id = %d AND b.user_id = %d", $id, $userID);
		$_results[$__key] = $wpdb->get_row( $sql, ARRAY_A );
	}
	return $_results[$__key];
}
function behavior_get_by_parent($id){
	global $wpdb;
	static $_results = [];
	$__key = md5(serialize(func_get_args()));
	if (!isset($_results[$__key])) {
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpiBehavior = "{$prefix}behavior";
		$sql = $wpdb->prepare("
          SELECT b.*, p.post_title, p.violation_assessment
          FROM {$tableKpiBehavior} AS b  
          INNER JOIN {$wpdb->posts} AS p ON p.ID = b.bank_id 
          WHERE b.parent = %d ", $id);
		$_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
	}
	return $_results[$__key];
}
function behavior_get_list_behavior_of_admin_by_year( $year, $parent = 0, $status = '' ){
	global $wpdb;
	static $_results = [];
	$__key = md5(serialize(func_get_args()));
	if (!isset($_results[$__key])) {
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpiBehavior = "{$prefix}behavior";
		$tableKpiYear = "{$prefix}kpi_years";
		$whereStatus = '';
		if( !empty( $status ) ){
			$whereStatus = $wpdb->prepare(" AND b.status = %s ", $status);
		}
		$sql = $wpdb->prepare("
          SELECT b.*, p.post_title, p.violation_assessment    
          FROM {$tableKpiBehavior} AS b
          INNER JOIN {$wpdb->posts} AS p ON p.ID = b.bank_id  
          WHERE b.year_id IN (
          		SELECT id 
          		FROM {$tableKpiYear} AS y 
          		WHERE y.kpi_type = 'behavior' AND y.year = %d) 
          AND b.parent = %d {$whereStatus} 
          ORDER BY b.parent_1 ASC, b.bank_id ASC 
          ", $year, $parent);
		$_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
	}
	return $_results[$__key];
}
function behavior_get_all_behavior_by_parent( $id )
{
	global $wpdb;
	static $argsID = [];
	if (!isset($argsID[$id])) {
		$argsID[$id] = [];
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpiBehavior = "{$prefix}behavior";
		$results = $wpdb->get_results($wpdb->prepare("SELECT * FROM {$tableKpiBehavior} as b WHERE b.parent = %d ", $id), ARRAY_A);
		if (!empty($results) && !is_wp_error($results)) {
			foreach ($results as $key => $item) {
				$argsID[$id][] = $item;
				$argsID[$id] = array_merge($argsID[$id], behavior_get_all_behavior_by_parent($item['id']));
			}
		}
	}
	return $argsID[$id];
}
function behavior_get_group_behavior_by_post( $id, $year ){
	global $wpdb;
	static $results = [];
	$__key = md5(serialize(func_get_args()));
	if (!isset($results[$__key])) {
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpiBehavior = "{$prefix}behavior";
		$tableKpiYear = "{$prefix}kpi_years";
		$results[$__key] = $wpdb->get_row(
			$wpdb->prepare("
				SELECT * 
				FROM {$tableKpiBehavior} as b 
				WHERE b.bank_id = %d AND b.parent_1 = 0 
				AND b.parent = 0 AND b.year_id IN (
					SELECT id 
					FROM {$tableKpiYear} AS y
					WHERE y.year = %d AND (y.precious = 0 OR y.precious IS NULL ) 
					AND (y.month = 0 OR y.month IS NULL ) AND y.kpi_type = 'behavior' 
					)
				", $id, $year), ARRAY_A);
	}
	return $results[$__key];
}
function behavior_get_behavior_by_user( $userID, $year, $precious = 0, $month = 0 ) {
	global $wpdb;
	static $_results = [];
	$__key = md5( serialize( func_get_args() ) );
	if ( ! isset( $_results[ $__key ] ) ) {
		$prefix             = $wpdb->get_blog_prefix( get_current_blog_id() );
		$tableKpiBehavior   = "{$prefix}behavior";
		$tableKpiYear       = "{$prefix}kpi_years";
		$wherePrecious = '';
		$whereMonth = '';
		if( !empty( $precious ) && $precious != 0 ){
			$wherePrecious = $wpdb->prepare(" AND y.precious = %d ", $precious);
		}
		if( !empty( $month ) && $month != 0 ){
			$whereMonth = $wpdb->prepare(" AND y.month = %d ", $month);
		}
		$sql                = $wpdb->prepare( "
          SELECT b.*, p.post_title, p.violation_assessment, y2.behavior_percent     
          FROM {$tableKpiBehavior} AS b
          INNER JOIN {$wpdb->posts} AS p ON p.ID = b.bank_id
          INNER JOIN {$tableKpiYear} AS y2 ON y2.id = b.year_id  
          WHERE b.year_id IN (
          		SELECT id 
          		FROM {$tableKpiYear} AS y 
          		WHERE y.kpi_type = 'behavior' AND y.year = %d {$wherePrecious} {$whereMonth}) 
          AND b.user_id = %d 
          GROUP BY b.bank_id 
          ORDER BY b.parent_1 ASC
          ", $year, $userID );
		$_results[ $__key ] = $wpdb->get_results( $sql, ARRAY_A );
	}
	return $_results[$__key];
}

function behavior_insert( $data ){
	global $wpdb;
	$prefix             = $wpdb->get_blog_prefix( get_current_blog_id() );
	$tableKpiBehavior   = "{$prefix}behavior";
	$wpdb->insert($tableKpiBehavior, $data);
	if( !empty( $wpdb->last_error ) ){
		return false;
	}

	return $wpdb->insert_id;
}
function behavior_insert_for_year_by_parent( $data, $year_id, $parentID, $parent_1 ){
	global $wpdb;
	$prefix             = $wpdb->get_blog_prefix( get_current_blog_id() );
	$tableKpiBehavior   = "{$prefix}behavior";
	$getYearBehavior = kpi_get_year_by_parent( $year_id );
	$arrParentIDs = [];
	foreach ( $getYearBehavior as $key => $item ){
		$data['parent'] = $parentID;
		if( is_array( $parent_1 ) ){
			$data['parent_1'] = !empty($parent_1) ? $parent_1[$item['id']] : $data['parent_1'];
		}else{
			$data['parent_1'] = $parent_1;
		}

		$data['year_id'] = $item['id'];
		$wpdb->insert($tableKpiBehavior, $data);
		$arrParentIDs[$item['id']] = $wpdb->insert_id;
	}
	return $arrParentIDs;
}

function behavior_get_list_behavior_of_user( $chartID, $userID, $year_id, $year ){
	$getBehavior = behavior_get_list_behavior_of_admin_by_year( $year,0, KPI_STATUS_RESULT);
	$getBehaviorForUser = behavior_get_behavior_by_user( $userID, $year, TIME_PRECIOUS_VALUE, TIME_MONTH_VALUE );
	$dataAfterInsert = [];
	if( empty($getBehaviorForUser) ){
		#$groupByBehaviorAdmin = array_group_by( $getBehavior, 'id' );
		$groupByBehaviorUser = array_group_by( $getBehaviorForUser, 'parent' );
		#$arrDifference = array_diff_key( $groupByBehaviorAdmin, $groupByBehaviorUser );
		$groupByBehaviorAdmin = array_group_by( $getBehavior, 'parent_1' );
		$behaviorGroup = [];
		if( !empty( $groupByBehaviorAdmin ) ){
			$behaviorGroup = $groupByBehaviorAdmin[0];
			unset( $groupByBehaviorAdmin[0] );
		}
		#echo "<pre>";
		#print_r( $behaviorGroup );
		#echo "</pre>";
		$created = date("Y-m-d H:i:s", time());
		foreach ( $behaviorGroup as $key => $value ){
			$dataGroup = [
				"year_id" => $value['year_id'],
				"bank_id" => $value['bank_id'],
				"parent_1" => 0,
				"parent" => $value['id'],
				"chart_id" => $chartID,
				"user_id" => $userID,
				"status" => $value['status'],
				"created" => $created
			];
			$insert_id = behavior_insert($dataGroup);
			$arrParentIDs = behavior_insert_for_year_by_parent( $dataGroup, $year_id, $insert_id, 0 );
			$dataAfterInsert[$insert_id] = array_merge(
				['id'=>$insert_id, 'post_title' => $value['post_title'],
				 'violation_assessment' => $value['violation_assessment']]
				, $dataGroup );
			if( array_key_exists( $value['id'], $groupByBehaviorAdmin ) ){
				foreach ($groupByBehaviorAdmin[ $value['id'] ] as $k => $item) {
					$dataBehavior['year_id'] = $item['year_id'];
					$dataBehavior['bank_id'] = $item['bank_id'];
					$dataBehavior['status'] = $item['status'];
					$dataBehavior['created'] = $item['created'];
					$dataBehavior['parent'] = $item['id'];
					$dataBehavior['chart_id'] = $chartID;
					$dataBehavior['user_id'] = $userID;
					$dataBehavior['parent_1'] = $insert_id;
					$insert_id_item = behavior_insert( $dataBehavior );

					behavior_insert_for_year_by_parent( $dataBehavior, $year_id, $insert_id_item, $arrParentIDs );
					$dataAfterInsert[$insert_id_item] = array_merge([
						'id'=>$insert_id_item,
						'post_title' => $item['post_title'],
						'violation_assessment' => $item['violation_assessment']
					], $dataBehavior );
				}
			}

		}
	}
	if( empty( $dataAfterInsert ) ){
		$dataAfterInsert = $getBehaviorForUser;
	}
	$resultBehavior = array_group_by( $dataAfterInsert, 'parent_1' );
	return $resultBehavior;
}
function behavior_get_total_number_for_year( $year, $userID, $chartID ){
	global $wpdb;
	static $_results = [];
	$__key = md5( serialize( func_get_args() ) );
	if ( ! isset( $_results[ $__key ] ) ) {
		$prefix             = $wpdb->get_blog_prefix( get_current_blog_id() );
		$tableKpiBehavior   = "{$prefix}behavior";
		$tableKpiYear       = "{$prefix}kpi_years";
		$sqlYearParent0     = $wpdb->prepare("
								SELECT id 
								FROM {$tableKpiYear} AS y_p 
								WHERE y_p.year = %d AND y_p.parent = 0 
								AND y_p.kpi_type='behavior' 
								", $year);
		$sqlGetYearByParent = $wpdb->prepare("
								SELECT id 
								FROM {$tableKpiYear} AS y2 
								WHERE y2.year = %d 
								AND y2.parent IN ({$sqlYearParent0})  
								", $year);
		$sql                = $wpdb->prepare( "
          SELECT SUM(b.number) AS total_number, 
          	y.behavior_percent, b.*, p.post_title, p.violation_assessment    
          FROM {$tableKpiBehavior} AS b
          INNER JOIN {$wpdb->posts} AS p ON p.ID = b.bank_id  
          INNER JOIN {$tableKpiYear} AS y ON y.id = b.year_id  
          WHERE b.year_id IN ({$sqlGetYearByParent}) 
          AND b.user_id = %d 
          GROUP BY bank_id  
          ORDER BY b.parent_1 ASC
          ", $userID );
		$_results[ $__key ] = $wpdb->get_results( $sql, ARRAY_A );
	}
	return $_results[$__key];
}
function behavior_count_of_year_by_user_status_duyet( $year, $userIDs ){
	global $wpdb;
	static $_results = [];
	$__key = md5(serialize(func_get_args()));
	if( !isset( $_results[$__key] ) ){
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpiBehavior = "{$prefix}behavior";
		$tableKpiYear = "{$prefix}kpi_years";
		$sql = $wpdb->prepare(" 
          SELECT COUNT(b.id) AS amount_status  
          FROM {$tableKpiBehavior} AS b 
          INNER JOIN {$tableKpiYear} AS y ON y.id = b.year_id AND y.year = %d 
          WHERE b.status IN ('".KPI_STATUS_WAITING."') 
          AND b.user_id IN ({$userIDs}) AND y.kpi_type = 'behavior'
          ", $year);
		$_results[$__key] = $wpdb->get_row( $sql, ARRAY_A );
	}
	return $_results[$__key];
}

function behavior_get_behavior_need_approve_or_done($year, $value, $userIDs, $type = 'month'){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpiBehavior = "{$prefix}behavior";
        $tableKpiYear = "{$prefix}kpi_years";
        $where = '';
        if( $type == 'month' && $value > 0 && $value <= 12 ){
            $where = $wpdb->prepare(" AND y.month = %d", $value);
        }elseif($type == 'precious' && $value > 0 && $value <=4){
            $where = $wpdb->prepare(" AND y.precious = %d", $value);
        }
        $sql = $wpdb->prepare(" 
          SELECT COUNT(b.id) AS amount_status_approve 
          FROM {$tableKpiBehavior} AS b 
          INNER JOIN {$tableKpiYear} AS y ON y.id = b.year_id AND y.year = %d 
          WHERE b.status IN ('".KPI_STATUS_WAITING."') {$where}
          AND b.user_id IN ({$userIDs}) AND y.kpi_type = 'behavior'
          UNION ALL
          SELECT COUNT(b.id) AS amount_status_complete
          FROM {$tableKpiBehavior} AS b 
          INNER JOIN {$tableKpiYear} AS y ON y.id = b.year_id AND y.year = %d 
          WHERE b.status IN ('".KPI_STATUS_DONE."') {$where}
          AND b.user_id IN ({$userIDs}) AND y.kpi_type = 'behavior'
          ", $year, $year);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function behavior_check_behavior_approved_by_year($behavior_id){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpiBehavior = "{$prefix}behavior";
        $tableKpiYear = "{$prefix}kpi_years";
        $sql = $wpdb->prepare("SELECT b.*  
          FROM {$tableKpiBehavior} AS b 
          WHERE b.parent = %d 
                AND b.status IN('".KPI_STATUS_RESULT."', '".KPI_STATUS_WAITING."') "
            , $behavior_id);
        #echo $sql."<br>";
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}