<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/18/18
 * Time: 14:31
 */
function kpi_render_user_name($name) {
    if( empty($name) ) {
        $name = '<em>Chưa có</em>';
    }
    return $name;
}
function kpi_render_tree_chart_node($charts, $level = 0) {
    if( !empty($charts) ):
        foreach($charts as $id => &$node):
            $position_xy = sprintf("left: %dpx; top: %dpx;", (int)$node['x'], (int)$node['y']);
            ?>
            <li class="node" data-level="<?php echo $level; ?>" data-node-id="<?php echo $id; ?>" data-node="<?php echo esc_json_attr($node); ?>">
                <div class="node-info" style="<?php echo $position_xy; ?>">
                    <div class="group-ten-to-chuc">
                        <span class="name" data-id="<?php echo $node['ID']; ?>"><?php echo $node['name']; ?></span>
                        <a href="javascript:;" data-toggle="modal" data-action="remove" data-target="#confirm-dialog"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                        <a href="javascript:;" data-toggle="modal" data-action="new" data-level="<?php echo $level; ?>" data-target="#tpl-form-user-role"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                        <a href="javascript:;" data-toggle="modal" data-action="edit" data-target="#tpl-form-user-role"><i class="fa fa-edit" aria-hidden="true"></i></a>
                    </div>
                    <div class="group-chuc-vu-truong">
                        <span class="chuc-vu-truong" data-id="<?php echo $node['role_id']; ?>"><?php echo $node['chuc_danh']; ?>:</span>
                        <span class="ten-chuc-vu-truong" data-id="<?php echo $node['user_id']; ?>"><?php echo kpi_render_user_name($node['display_name']); ?></span>
                    </div>
                    <div class="group-chuc-vu-truong">
                        <span class="chuc-vu-assign-task">Được phân bổ KPI?</span>
                        <span class="ten-chuc-vu-assign-task">
                            <input class="checkbox-status hidden" readonly type="checkbox" disabled <?php echo ($node['assign_task'] ? 'checked="checked"' : ''); ?>>
                            <span class="checkbox-slider"></span>
                        </span>
                    </div>
                    <ul class="group group-chuc-vu-pho">
                        <li class="group-item input-label"><span>Tên các chức vụ phó:</span></li>
                        <?php if( !empty($node['siblings']) ): foreach ($node['siblings'] as $sib_id => &$sibling): ?>
                            <li class="group-item">
                                <div class="input-group">
                                    <span class="chuc-vu-pho" data-id="<?php echo $sibling['role_id']; ?>"><?php echo $sibling['chuc_danh']; ?>:</span>
                                    <span class="ten-chuc-vu-pho" data-id="<?php echo $sibling['user_id']; ?>"><?php echo kpi_render_user_name($sibling['display_name']); ?></span>
                                </div>
                            </li>
                        <?php endforeach; endif; ?>
                    </ul>
                </div>
                <ul class="node-children">
                    <?php
                    if( !empty($node['children']) ):
                        kpi_render_tree_chart_node($node['children'], $level + 1);
                    endif; ?>
                </ul>
            </li>
            <?php
        endforeach;
    endif;
}