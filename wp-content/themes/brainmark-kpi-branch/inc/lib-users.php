<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/25/17
 * Time: 16:46
 */

define('USER_UPDATE_SUCCESS', 200);
define('USER_DELETE_SUCCESS', 200);
define('USER_CREATE_SUCCESS', 201);

define('USER_ERROR_BAD_REQUEST',        400);
define('USER_ERROR_INVALID_NONCE',      401);
define('USER_ERROR_INVALID_PERMISSION', 403);
define('USER_ERROR_INVALID_PASSWORD',   405);
define('USER_ERROR_MIN_PASSWORD',       406);
define('USER_ERROR_NOT_MATCH_PASSWORD', 407);
define('USER_ERROR_INVALID_USER_ID',    408);
define('USER_ERROR_USER_LOGIN_TOO_LONG',  410);
define('USER_ERROR_EXISTS_USER_LOGIN',  411);
define('USER_ERROR_INVALID_USERNAME',   412);
define('USER_ERROR_USER_NICENAMe_TOO_LONG', 413);
define('USER_ERROR_EXISTING_USER_EMAIL', 414);
define('USER_ERROR_COMMON', 415);
# define('USER_ERROR_', 400);
# define('USER_ERROR_', 400);
# define('USER_ERROR_', 400);
# define('USER_ERROR_', 400);

function kpi_get_user_by_id($id) {
    static $results = [];
    if( !isset($results[$id]) ) {
        $user = get_user_by('ID', $id);
        $orgchartUser = user_load_orgchart($user);
        $results[$id] = $user;
    }
    return $results[$id];
}
function pre_user_query_by_orgchart( \WP_User_Query &$wpUser ){
    global $wpdb;
    $user = wp_get_current_user();
    $orgchart = user_load_orgchart($user);
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableChart = "{$prefix}org_charts";
    if( $orgchart->id > 0 ) {
        $wpUser->query_where .= " AND {$wpdb->users}.orgchart_id IN ( SELECT id FROM {$tableChart} AS org WHERE org.parent = " . $orgchart->id . " ) ";
    }
}
function get_user_by_list_orgchart_parent( $id ){
    static $results = [];
    if( !isset($results[$id]) ) {
        $users = new WP_User_Query();
        $user = get_user_by('ID', $id);
        $orgchartUser = user_load_orgchart($user);
        $results[$id] = $user;
    }
    return $results[$id];
}

function kpi_user_update_info($params) {
	$user = null;
	$checkCode = check_code_user( $params['user_nicename'], $params['ID'] );
	if( $checkCode ){
		$user = new WP_Error(401, __("Mã nhân viên đã được sử dụng.", TPL_DOMAIN_LANG) );
	}
    $upload = wp_upload_dir();
    if( isset($_FILES['user_url']) ) {
        $_FILES['user_url']['type'];
        $_FILES['user_url']['error'];
        $_FILES['user_url']['size'];

        if( $_FILES['user_url']['error'] == UPLOAD_ERR_OK && $_FILES['user_url']['size'] > 0 ) {
            $avatar = "{$upload['basedir']}/avatars/{$params['user_login']}_{$_FILES['user_url']['name']}";
            $params['user_url'] = "{$upload['baseurl']}/avatars/{$params['user_login']}_{$_FILES['user_url']['name']}";

            if( !is_dir("{$upload['basedir']}/avatars") ) {
                mkdir("{$upload['basedir']}/avatars", 0777, true);
            }
            move_uploaded_file($_FILES['user_url']['tmp_name'], $avatar);
            if( !empty($params['user_url']) ) {
                $old_user = new WP_User($params['ID']);
                $old_url = $old_user->user_url;
                if( !empty($old_url) ) {
                    $old_url = $upload['basedir'] . str_replace($upload['baseurl'], '', $old_url);
                    if( file_exists($old_url) ) {
                        unlink($old_url);
                    }
                }
            }
        } else {
            if( isset($params['user_url']) ) {
                unset($params['user_url']);
            }
        }
    }
    if( isset($params['user_url']) && empty($params['user_url']) ) {
        unset($params['user_url']);
    }
    $parts = explode(' ', $params['display_name']);

    $fields = ['ID', 'user_email', 'user_pass', 're_user_pass', 'role', 'display_name', 'user_url', 'description'];
    # 'user_nicename', 'user_login',
    $first_name = array_shift($parts);
    $userData = ['first_name' => $first_name, 'last_name' => implode(' ', $parts),
        'description' => isset($params['description']) ? $params['description'] : '',
        'show_admin_bar_front' => false, 'locale' => get_locale(), 'user_nicename' => $params['user_nicename']];
    foreach($fields as $field) {
        if( isset($params[$field]) ) {
            $userData[$field] = $params[$field];
        }
    }
    $userData['user_login'] = $params['user_email'];
    if( empty($userData['user_pass']) && empty($userData['re_user_pass']) ) {
        unset($userData['user_pass']);
        unset($userData['re_user_pass']);
    } else if( !empty($userData['user_pass']) && strlen($userData['user_pass']) < 4 ) {
        $user = new WP_Error(USER_ERROR_MIN_PASSWORD, __("Password at least 4 characters", TPL_DOMAIN_LANG) );
    } else if( $userData['user_pass'] != $userData['re_user_pass'] ) {
        $user = new WP_Error(USER_ERROR_NOT_MATCH_PASSWORD, __("Password and Re-Password not matching", TPL_DOMAIN_LANG) );
    }

    add_filter('insert_user_meta', function(array $meta, WP_User $user, $update) use( $params, $userData ) {
        return $meta;
    }, 10, 3);

    add_filter('wp_pre_insert_user_data', function(array $data, $update, $id) use( $params, $userData ) {
        $data['orgchart_id'] = (int)$params['orgchart_id'];
        $user = new WP_User($id);
        if( strpos($user->user_login, '@') !== false ) {
            $data['user_login'] = $userData['user_email'];
        }
        return $data;
    }, 10, 3);

    if( $user === null ) {
        $user = wp_update_user($userData);
    }

    if( is_wp_error($user) ) {
        $user = kpi_convert_user_error($user);
    }
    return $user;
}

function kpi_user_create_new($params) {
	$user = null;
	$checkCode = check_code_user( $params['user_nicename'], $params['ID'] );
	if( $checkCode ){
		$user = new WP_Error(401, __("Mã nhân viên đã được sử dụng.", TPL_DOMAIN_LANG) );
	}
    $upload = wp_upload_dir();
    if( isset($_FILES['user_url']) ) {
        $_FILES['user_url']['type'];
        $_FILES['user_url']['error'];
        $_FILES['user_url']['size'];

        if( $_FILES['user_url']['error'] == UPLOAD_ERR_OK && $_FILES['user_url']['size'] > 0 ) {
            $avatar = "{$upload['basedir']}/avatars/{$params['user_login']}_{$_FILES['user_url']['name']}";
            $params['user_url'] = "{$upload['baseurl']}/avatars/{$params['user_login']}_{$_FILES['user_url']['name']}";
            if( !is_dir("{$upload['basedir']}/avatars") ) {
                mkdir("{$upload['basedir']}/avatars", 0777, true);
            }
            move_uploaded_file($_FILES['user_url']['tmp_name'], $avatar);
        } else {
            if( isset($params['user_url']) ) {
                unset($params['user_url']);
            }
        }
    }
    $parts = explode(' ', $params['display_name']);

    $fields = ['user_email', 'role', 'display_name', 'user_url', 'description', 'user_pass', 're_user_pass'];
    # 'user_pass', 're_user_pass',
    # 'user_nicename', 'user_login',
    $first_name = array_shift($parts);
    $userData = ['first_name' => $first_name, 'last_name' => implode(' ', $parts),
        'description' => isset($params['description']) ? $params['description'] : '',
        'show_admin_bar_front' => false, 'locale' => get_locale(), 'user_nicename' => $params['user_nicename']];

    foreach($fields as $field) {
        if( isset($params[$field]) ) {
            $userData[$field] = $params[$field];
        }
    }
    $userData['user_login'] = $userData['user_email'];
    add_filter('insert_user_meta', function(array $meta, WP_User $user, $update) use( $params, $userData ) {
        return $meta;
    }, 10, 3);
    add_filter('wp_pre_insert_user_data', function(array $data, $update, $id) use( $params, $userData ) {
        $data['orgchart_id'] = (int)$params['orgchart_id'];
        $data['user_login'] = $userData['user_email'];
        return $data;
    }, 10, 3);

    if( empty($userData['user_pass']) ) {
        $site_url = preg_replace('#^http(s|)://(www\.|)#', '', site_url());
        $parts = explode('.', $site_url);
        $userData['user_pass'] = $parts[0] . '@123';
    } else if( strlen($userData['user_pass']) < 4 ) {
        $user = new WP_Error(USER_ERROR_MIN_PASSWORD, __("Password at least 4 characters", TPL_DOMAIN_LANG) );
    } else if( $userData['user_pass'] != $userData['re_user_pass'] ) {
        $user = new WP_Error(USER_ERROR_NOT_MATCH_PASSWORD, __("Password and Re-Password not matching", TPL_DOMAIN_LANG) );
    }

    if( $user === null ) {
        $user = wp_insert_user($userData);
    }
    if( is_wp_error($user) ) {
        $user = kpi_convert_user_error($user);
    }
    return $user;
}

function kpi_convert_user_error(WP_Error $err) {
    $code = $err->get_error_code();
    switch ($code) {
        case 'invalid_user_id':
            $user = new WP_Error(USER_ERROR_INVALID_USER_ID, __( 'Invalid user ID.', TPL_DOMAIN_LANG ));
            break;
        case 'empty_user_login':
            $user = new WP_Error(USER_ERROR_EMPTY_USER_LOGIN, __( 'Cannot create a user with an empty login name.', TPL_DOMAIN_LANG ));
            break;
        case 'user_login_too_long':
            $user = new WP_Error(USER_ERROR_USER_LOGIN_TOO_LONG, __( 'Username may not be longer than 60 characters.', TPL_DOMAIN_LANG ));
            break;
        case 'existing_user_login':
            $user = new WP_Error(USER_ERROR_EXISTS_USER_LOGIN, __( 'Sorry, that username already exists!', TPL_DOMAIN_LANG ));
            break;
        case 'invalid_username':
            $user = new WP_Error(USER_ERROR_INVALID_USERNAME, __( 'Sorry, that username is not allowed.', TPL_DOMAIN_LANG ));
            break;
        case 'user_nicename_too_long':
            $user = new WP_Error(USER_ERROR_USER_NICENAME_TOO_LONG, __( 'Nicename may not be longer than 50 characters.', TPL_DOMAIN_LANG ));
            break;
        case 'existing_user_email':
            $user = new WP_Error(USER_ERROR_EXISTING_USER_EMAIL, __( 'Sorry, that email address is already used!', TPL_DOMAIN_LANG ));
            break;
        default:
            if( !is_numeric($code) ) {
                $user = new WP_Error(USER_ERROR_COMMON, __( 'Sorry, an error occurred.', TPL_DOMAIN_LANG ));
            } else {
                $user = $err;
            }
    }
    return $user;
}


function kpi_get_department_users($chart_id, $kpi_id, $bank_id, $year_id = 0) {
    static $_results = [];
    if( !isset($_results[$chart_id]) ) {
        global $wpdb;
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $tableChart = "{$prefix}org_charts";
        $tableUser = "{$prefix}users";
        $tableUserMeta = "{$prefix}usermeta";
        $chart_ids = $wpdb->prepare("SELECT c.`id` FROM {$tableChart} as c WHERE c.`parent` = %d", $chart_id);
        $wherebankid = $wpdb->prepare(" AND (k.bank_id = %d) ", $bank_id);
        $wherebankid .= $wpdb->prepare(" AND (k.parent = %d) ", $kpi_id);
        $wherekpiyear = $wpdb->prepare(" AND ( (k.year_id = %d) OR (y.`parent` = %d) ) ", $year_id, $year_id);

        $_results[$chart_id] = $wpdb->get_results(
            "SELECT u.ID, CONCAT(m.meta_value, ' ', m1.meta_value) as fullname, u.user_login, u.user_email, u.user_url, u.display_name, ".
            "k.personal_plan, k.department_plan, k.unit, k.bank_id, k.receive, k.percent, k.parent, k.aproved, ".
            "c.id as chart_id, c.name as cname, c.role as crole, c.description as cdescription, k.id as kpi_item_id " .
            "FROM {$tableUser} as u " .
            "LEFT JOIN {$tableUserMeta} as m ON u.ID = m.user_id AND m.meta_key LIKE 'first_name' " .
            "LEFT JOIN {$tableUserMeta} as m1 ON u.ID = m1.user_id AND m.meta_key LIKE 'last_name' " .
            "INNER JOIN {$tableChart} as c ON u.orgchart_id = c.id " .
            "LEFT JOIN {$tableKpi} as k ON u.ID = k.user_id AND k.chart_id = c.id {$wherebankid} " .
            # "LEFT JOIN {$tableKpiYear} as y ON y.id = k.year_id AND k.year_id = c.id {$wherekpiyear} " .
            "WHERE u.orgchart_id IN ( {$chart_ids} )");
        foreach ($_results[$chart_id] as $key => $item){
            $percents = get_total_percent_for_chart( $item->chart_id, $item->ID, $year_id );
            if( !empty( $percents ) ) {
                $_results[$chart_id][$key]->total_percent = $percents['total_percent'];
            }
        }

        send_response_json(['items' => $_results[$chart_id]], 200, 'Dump');
        #send_response_json(['items' => $_results[$chart_id], 'last_query' => $wpdb->last_query, 'last_error' => $wpdb->last_error], 200, 'Dump');;
    }
    return $_results[$chart_id];
}

function get_total_percent_for_chart( $chartID, $userID, $year_id ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $sql = $wpdb->prepare("
              SELECT SUM(k.percent) AS total_percent
              FROM {$tableKpi} AS k
              WHERE k.chart_id = %d AND k.user_id = %d AND k.year_id IN ( 
                SELECT y2.id 
                FROM {$tableKpiYear} AS y2 
                WHERE y2.year IN ( 
                      SELECT y3.year 
                      FROM {$tableKpiYear} AS y3 
                      WHERE y3.id = %d
                )  AND ( y2.month = 0 OR y2.month IS NULL ) 
              )
              ", $chartID, $userID, $year_id);
        $_results[$__key] = $wpdb->get_row( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function kpi_get_department_assigned_users($chart_id, $kpi_id) {
    static $_results = [];
    $_key = "{$chart_id}_{$kpi_id}";
    if( !isset($_results[$_key]) ) {
        global $wpdb;
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $tableChart = "{$prefix}org_charts";
        $tableUser = "{$prefix}users";
        $chart_ids = $wpdb->prepare("SELECT c.`id` FROM {$tableChart} as c WHERE c.`parent` = %d", $chart_id);
        $user_ids = "SELECT u.ID " .
            "FROM {$tableUser} as u " .
            "INNER JOIN {$tableChart} as c ON u.orgchart_id = c.id " .
            "WHERE u.orgchart_id IN ( {$chart_ids} ) ";

        $_results[$_key] = $wpdb->get_results($wpdb->prepare("SELECT * FROM {$tableKpi} as k WHERE k.`parent` = %d AND k.user_id IN ({$user_ids})", $kpi_id) );
    }
    return $_results[$_key];
}

function kpi_get_all_users($params = []) {
    global $wpdb;
    static $__results = [];
    $_key = md5(serialize(func_get_args()));
    if( !isset($__results[$_key]) ) {
        $blog_id = get_current_blog_id();
        $blog_prefix = $wpdb->get_blog_prefix($blog_id);

        $where = '';
        if( !empty($params['orgchart_id']) ) {
            $where = $wpdb->prepare(" AND (u.`orgchart_id` = %d) ", $params['orgchart_id']);
        }

        if( empty($params['order']) ) {
            $params['order'] = 'DESC';
        }
        if( empty($params['orderby']) ) {
            $params['orderby'] = 'u.ID';
        }

        $order_by = "{$params['orderby']} {$params['order']}";

        $sql = "SELECT u.*, c.`name` as `chuc_danh`, c.`level` orgchart_level, c.`parent` \n".
            "FROM `{$blog_prefix}users` as u \n".
            "LEFT JOIN `{$blog_prefix}org_charts` as c ON (c.id = u.orgchart_id) \n".
            "WHERE 1 = 1 {$where} ".
            "ORDER BY {$order_by}";
        # echo "Users: {$sql}\n";
        $results = $wpdb->get_results($sql, ARRAY_A);

        foreach ($results as $id => $u) {
            $results[$id]['ID'] = (int)$u['ID'];
            $results[$id]['parent'] = (int)$u['parent'];
            $results[$id]['orgchart_id'] = (int)$u['orgchart_id'];
            $results[$id]['orgchart_level'] = (int)$u['orgchart_level'];
            $space = [];
            for( $i=0; $i < $results[$id]['orgchart_level']; $i++ ) {
                $space[] = '-- ';
            }
            $space = implode('', $space);
            $results[$id]['ten_chuc_danh'] = $space . $u['chuc_danh'];
        }
        $__results[$_key] = $results;
    }
    return $__results[$_key];
}

function kpi_is_user_ceo(){
	$user = wp_get_current_user();
	$orgchart = user_load_orgchart($user);
	$member_role = $orgchart ? $orgchart->role : '';
	return ( $member_role == 'bgd' ) ? true : false;
}

function kpi_is_user_phongban(){
	$user = wp_get_current_user();
	$orgchart = user_load_orgchart($user);
	$member_role = $orgchart ? $orgchart->role : '';
	return ( $member_role == 'phongban' ) ? true : false;
}

function kpi_is_user_nhanvien(){
	$user = wp_get_current_user();
	$orgchart = user_load_orgchart($user);
	$member_role = $orgchart ? $orgchart->role : '';
	return ( $member_role == 'nhanvien' ) ? true : false;
}

function kpi_get_list_users($params = []) {
    global $wpdb;
    # echo "<pre>{$wpdb->last_query}\n{$wpdb->last_error}</pre>";
    static $__results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset($__results[$__key]) ) {
        $__results[$__key] = [];
        $params['orderby'] = 'c.`parent`';
        $params['order'] = 'ASC';
        $users = kpi_get_all_users($params);

        $charts = kpi_get_list_org_charts();
        # echo '<pre>'; var_dump(__LINE__, __FILE__, $parentId, $wpdb->last_error, $wpdb->last_query, $charts); echo '</pre>';
        if( !empty($charts) ) {
            foreach($charts as $id => &$item) {
                $charts[$id]['users'] = [];
            }
            foreach($charts as $id => &$item) {
                foreach($users as $i => $u) {
                    if( $item['id'] == $u['orgchart_id'] ) {
                        $charts[$id]['users'][] = $u;
                        unset($users[$i]);
                    }
                }
            }
            if( empty($parentId) ) {
                foreach($charts as $id => &$item) {
                    $__results[$__key] = array_merge($__results[$__key], $item['users']);
                }
                # echo '<pre>'; var_dump($users); echo '</pre>';
                $__results[$__key] = array_merge($__results[$__key], $users);
            } else {
                foreach($charts as $id => &$item) {
                    if( $parentId == $item['parentId'] )
                        $__results[$__key] = array_merge($__results[$__key], $item['users']);
                }
                # echo '<pre>'; var_dump($users); echo '</pre>';
                $__results[$__key] = array_merge($__results[$__key], $users);
            }
        } else {
            $__results[$__key] = null;
        }
    }
    return $__results[$__key];
}
function user_list_for_kpi( $chartIDs, $year, $kpi_time, $kpi_type ){
    static $_results = [];
    $_key = md5( serialize( func_get_args() ) );
    if( !isset($_results[$_key]) ) {
        global $wpdb;
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("
            SELECT u.*, org.name AS orgchart_name, COUNT(k.id) AS count_kpi,
             org.alias, org.room
            FROM {$wpdb->users} AS u
            INNER JOIN {$tableChart} AS org ON u.orgchart_id = org.id
            LEFT JOIN {$tableKpi} AS k ON k.user_id = u.ID AND k.year_id 
            IN ( SELECT id FROM {$tableKpiYear} AS y WHERE y.year = %d AND y.kpi_time = %s AND y.kpi_type = %s )  
            WHERE u.orgchart_id IN ($chartIDs)
            GROUP BY u.ID
            ORDER BY org.level ASC, u.ID ASC, count_kpi DESC
            ", $year, $kpi_time, $kpi_type );
        $_results[$_key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$_key];
}

function user_list_for_kpi_behavior( $chartIDs, $year, $kpi_type ){
	static $_results = [];
	$_key = md5( serialize( func_get_args() ) );
	if( !isset($_results[$_key]) ) {
		global $wpdb;
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpiBehavior = "{$prefix}behavior";
		$tableKpiYear = "{$prefix}kpi_years";
		$tableChart = "{$prefix}org_charts";
		$sql = $wpdb->prepare("
            SELECT u.*, org.name AS orgchart_name, org.room, COUNT(b.id) AS count_kpi,
            org.alias, org.room 
            FROM {$wpdb->users} AS u
            INNER JOIN {$tableChart} AS org ON u.orgchart_id = org.id 
            LEFT JOIN {$tableKpiBehavior} AS b ON b.user_id = u.ID AND b.status = '".KPI_STATUS_WAITING."' AND b.year_id 
            IN ( SELECT id FROM {$tableKpiYear} AS y WHERE y.year = %d AND y.kpi_type = %s )  
            WHERE u.orgchart_id IN ($chartIDs)
            GROUP BY u.ID
            ORDER BY org.level ASC, u.ID ASC, count_kpi DESC
            ",$year, $kpi_type );
		$_results[$_key] = $wpdb->get_results( $sql, ARRAY_A );
	}
	return $_results[$_key];
}


function orgchart_get_orgchart_by_alias( $alias ){
	global $wpdb;
	static $results = [];
	if (!isset($results[$alias])) {
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableChart = "{$prefix}org_charts";
		$sql = $wpdb->prepare("
			SELECT u.* 
			FROM {$wpdb->users} AS u
			INNER JOIN {$tableChart} AS org ON org.id = u.orgchart_id    
			WHERE org.alias = %s ", $alias);
		$results[$alias] = $wpdb->get_results($sql, ARRAY_A);
	}
	return $results[$alias];
}

function user_get_users_by_alias( $alias ){
	global $wpdb;
	static $results = [];
	$__key = md5(serialize( func_get_args() ));
	if (!isset($results[$__key])) {
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableChart = "{$prefix}org_charts";
		$sql = "
			SELECT u.* 
			FROM {$wpdb->users} AS u
			INNER JOIN {$tableChart} AS org ON org.id = u.orgchart_id    
			WHERE org.alias IN ({$alias})";
		$results[$__key] = $wpdb->get_results($sql, ARRAY_A);
	}
	return $results[$__key];
}
function user_send_mail_user( \WP_User $user, $password ){
	$to_email = $user->user_email;
    $bloginfo = get_bloginfo();
    $bloginfo = mb_strtoupper($bloginfo);
	$subject = "THÔNG TIN TÀI KHOẢN TẠI [{$bloginfo}]";
	/*$message = sprintf( __( '<p>Hi <strong>%s</strong>,</p>' ), $user->display_name );
	$message .= "<h3>Thông tin tài khoản của bạn bao gồm:</h3>";
	$message .= "<ul>";
	$message .= sprintf( __( '<li style="padding-bottom: 5px;"><strong>Email:</strong> %s</li>' ), $user->user_email );
	$message .= sprintf( __( '<li style="padding-bottom: 5px;"><strong>Mật khẩu:</strong> %s</li>' ), $password );
	$message .= "</ul>";*/
	$full_name = $user->display_name;
	$username = $user->user_email;
    ob_start();
    include_once THEME_DIR . '/email/new-account.php';
    $message = ob_get_clean();
    ob_end_clean();

	$headers = array('Content-Type: text/html; charset=UTF-8');
	wp_mail( $to_email, $subject, $message, $headers );
}

/**
 * check code user in database
 * if exists code in datase then return true else return false;
 * @param WP_User $user
 * @param $code
 *
 * @return bool
 */
function check_code_user( $code, $userID = 0 ){
	$getUserCode = get_user_by( 'slug', $code );
	$getUserID = [];
	if( !empty( $userID ) ){
		$getUserID = get_user_by( 'ID', $userID );
	}
	if( !empty( $getUserCode ) && !is_wp_error( $getUserCode ) ) {
		if( !empty( $getUserID ) ){
			#Update
			if( $getUserID->ID != $getUserCode->ID ){
				return true;
			}
		}else{
			#Create
			return true;
		}
	}
	return false;
}

/**
 * @param $chartIDs
 *
 * @return mixed
 */
function user_get_user_by_chart( $chartIDs ){
	global $wpdb;
	static $results = [];
	$__key = md5(serialize( func_get_args() ));
	if (!isset($results[$__key])) {
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableChart = "{$prefix}org_charts";
		$sql =
			"SELECT u.*, c.name as orgchart_name, m1.meta_value as `first_name`, m2.meta_value as `last_name`, \n".
			"c.parent as `orgchart_parent`, c.room, c.`alias`, ".
			"m3.meta_value as `description` \n".
			"FROM {$wpdb->users} as u \n".
			"LEFT JOIN `{$wpdb->usermeta}` as m ON (m.user_id = u.ID) AND (m.meta_key LIKE 'user_parent') \n".
			"LEFT JOIN `{$wpdb->usermeta}` as m1 ON (m1.user_id = u.ID) AND (m1.meta_key LIKE 'first_name') \n".
			"LEFT JOIN `{$wpdb->usermeta}` as m2 ON (m2.user_id = u.ID) AND (m2.meta_key LIKE 'last_name') \n".
			"LEFT JOIN `{$wpdb->usermeta}` as m3 ON (m3.user_id = u.ID) AND (m3.meta_key LIKE 'description') \n".
			"INNER JOIN `{$tableChart}` as c ON (c.id = u.orgchart_id) \n".
			"WHERE u.orgchart_id IN ({$chartIDs}) ".
			"ORDER BY c.level ASC"; # exit("<pre>{$sql}</pre>");
		$results[$__key] = $wpdb->get_results($sql, ARRAY_A);
	}
	return $results[$__key];
}

function user_get_user_by_not_staff(){
	global $wpdb;
	static $results = [];
	$__key = md5(serialize( func_get_args() ));
	if (!isset($results[$__key])) {
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableChart = "{$prefix}org_charts";
		$sql =
			"SELECT u.*, c.name as orgchart_name, m1.meta_value as `first_name`, m2.meta_value as `last_name`, \n".
			"c.parent as `orgchart_parent`, c.room, c.`alias`, ".
			"m3.meta_value as `description` \n".
			"FROM {$wpdb->users} as u \n".
			"LEFT JOIN `{$wpdb->usermeta}` as m ON (m.user_id = u.ID) AND (m.meta_key LIKE 'user_parent') \n".
			"LEFT JOIN `{$wpdb->usermeta}` as m1 ON (m1.user_id = u.ID) AND (m1.meta_key LIKE 'first_name') \n".
			"LEFT JOIN `{$wpdb->usermeta}` as m2 ON (m2.user_id = u.ID) AND (m2.meta_key LIKE 'last_name') \n".
			"LEFT JOIN `{$wpdb->usermeta}` as m3 ON (m3.user_id = u.ID) AND (m3.meta_key LIKE 'description') \n".
			"INNER JOIN `{$tableChart}` as c ON (c.id = u.orgchart_id) \n".
			"WHERE c.alias != 'nhanvien' ".
			"ORDER BY c.level ASC"; # exit("<pre>{$sql}</pre>");
		$results[$__key] = $wpdb->get_results($sql, ARRAY_A);
	}
	return $results[$__key];
}

function user_get_user_by_staff(){
	global $wpdb;
	static $results = [];
	$__key = md5(serialize( func_get_args() ));
	if (!isset($results[$__key])) {
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableChart = "{$prefix}org_charts";
		$sql =
			"SELECT u.*, c.name as orgchart_name, m1.meta_value as `first_name`, m2.meta_value as `last_name`, \n".
			"c.parent as `orgchart_parent`, c.room, c.`alias`, ".
			"m3.meta_value as `description` \n".
			"FROM {$wpdb->users} as u \n".
			"LEFT JOIN `{$wpdb->usermeta}` as m ON (m.user_id = u.ID) AND (m.meta_key LIKE 'user_parent') \n".
			"LEFT JOIN `{$wpdb->usermeta}` as m1 ON (m1.user_id = u.ID) AND (m1.meta_key LIKE 'first_name') \n".
			"LEFT JOIN `{$wpdb->usermeta}` as m2 ON (m2.user_id = u.ID) AND (m2.meta_key LIKE 'last_name') \n".
			"LEFT JOIN `{$wpdb->usermeta}` as m3 ON (m3.user_id = u.ID) AND (m3.meta_key LIKE 'description') \n".
			"INNER JOIN `{$tableChart}` as c ON (c.id = u.orgchart_id) \n".
			"WHERE c.alias = 'nhanvien' ".
			"ORDER BY c.level ASC"; # exit("<pre>{$sql}</pre>");
		$results[$__key] = $wpdb->get_results($sql, ARRAY_A);
	}
	return $results[$__key];
}


function get_list_user_by_role_managers($params){
	add_action( 'pre_user_query', function (\WP_User_Query &$wpUser){
		global $wpdb;
		$wpUser->query_where .= " AND ({$wpdb->users}.deleted = 0) \n";
	});
	$paged = isset($params['trang']) ? max(1, intval($params['trang'])) : 1;
	$limit = isset($params['limit']) ? max(1, intval($params['limit'])) : 100;
	$offset = ($paged-1)*$limit;
	$args = [
		'role__in' => ['user_employers', 'user_managers'],
		'number' => $limit,
		'offset' => $offset,
		'paged' => $paged,
	];

	$users = new WP_User_Query( $args );
	return $users;
}
