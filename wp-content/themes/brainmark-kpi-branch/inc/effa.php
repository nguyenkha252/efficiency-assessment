<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/5/18
 * Time: 1:18 AM
 */

define('NUMBER_OF_QUESTION', 3);

function effa_site_mysql_date($date) {
    $date = preg_replace('#^(\d{1,2})\-(\d{1,2})\-(\d{2,4})#', '$3-$2-$1', $date);
    $date = preg_replace('#^(\d{1,2})\/(\d{1,2})\/(\d{2,4})#', '$3-$2-$1', $date);
    if( strlen($date) == 10 ) {
        $date = "{$date} 00:00:00";
    }
    return $date;
}

function effa_site_format_date($date) {
    return preg_replace('#^(\d{4})\-(\d{1,2})\-(\d{1,2})#', '$3-$2-$1', $date);
}
function effa_site_format_dmy_to_ymd( $date ){
    return preg_replace('#^(\d{1,2})(\-|\/)(\d{1,2})(\-|\/)(\d{4})#', '$5$4$3$2$1', $date);
}

function site_posts_clauses_not_department($clauses){
    global $wpdb;
    if( !isset($GLOBALS['where_year']) ){
        $year = date('Y', time());
    }else {
        $year = $GLOBALS['where_year'];
    }
    $clauses['orderby'] = "{$wpdb->posts}.post_code ASC, {$wpdb->posts}.ID ASC";
    $clauses['where'] .= $wpdb->prepare(" AND {$wpdb->posts}.post_year = %d AND ({$wpdb->posts}.department = '' OR {$wpdb->posts}.department IS NULL)", $year);
    return $clauses;
}

function site_posts_clauses_department($clauses){
    global $wpdb;
    if( !isset($GLOBALS['where_year']) ){
        $year = date('Y', time());
    }else {
        $year = $GLOBALS['where_year'];
    }
    $where = "";
    $clauses['orderby'] = "{$wpdb->posts}.post_code ASC, {$wpdb->posts}.ID ASC";
    if( isset($GLOBALS['department']) ){
        $where = $wpdb->prepare(" AND ({$wpdb->posts}.department = '' OR {$wpdb->posts}.department IS NULL OR LOWER({$wpdb->posts}.department) = %s) ", $GLOBALS['department']);
    }
    $clauses['where'] .= $wpdb->prepare(" AND {$wpdb->posts}.post_year = %d {$where}", $year);
    return $clauses;
}



function site_register_post_type_by_item( $item )
{

    $labels = array(
        "name" => __($item['items'], TPL_DOMAIN_LANG),
        "singular_name" => __($item['items'], TPL_DOMAIN_LANG),
        "menu_name" => __($item['items'], TPL_DOMAIN_LANG),
        "all_items" => __("All " . $item['items'], TPL_DOMAIN_LANG),
        "add_new" => __("New " . $item['item'], TPL_DOMAIN_LANG),
        "add_new_item" => __($item['item'] . " Name", TPL_DOMAIN_LANG),
        "edit_item" => __("Edit " . $item['item'], TPL_DOMAIN_LANG),
        "new_item" => __("New " . $item['item'], TPL_DOMAIN_LANG),
        "view_item" => __("View " . $item['item'], TPL_DOMAIN_LANG),
        "view_items" => __("View " . $item['items'], TPL_DOMAIN_LANG),
        "search_items" => __("Search " . $item['item'], TPL_DOMAIN_LANG),
        "not_found" => __($item['item'] . " Not Found", TPL_DOMAIN_LANG),
        "not_found_in_trash" => __($item['item'] . " Not Found in Trash", TPL_DOMAIN_LANG),
    );

    $args = array(
        "label" => __($item['items'], TPL_DOMAIN_LANG),
        "labels" => $labels,
        "description" => __($item['item'] . " Items List", TPL_DOMAIN_LANG),
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => true,
        "show_in_nav_menus" => true,
        "show_admin_column" => false,
        "rest_base" => $item['slug'],
        "has_archive" => true,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "show_in_quick_edit" => true,
        "has_multiple" => false,
        "rewrite" => array(
            "slug" => $item['slug'],
            "with_front" => true,
            'feed' => true,
            'pages' => true,
        ),
        "query_var" => $item['slug'],
        "menu_position" => $item['menu_position'],
        "supports" => $item['supports']
        /*
         * array(
            "title", "editor", "revisions", "thumbnail"
            #, "page-attributes", "trackbacks", "custom-fields",  "excerpt",
            # "comments",  "author", "post-formats"
        ),*/

    );

    register_post_type($item['post_type'], $args);
}

function site_register_custom_taxonomy_item($item) {

    /**
     * Taxonomy: Custom taxonomy
     */

    $labels = array(
        "name"          => __( "Categories " . $item['items'], TPL_DOMAIN_LANG ),
        "singular_name" => __( "Categories " . $item['items'], TPL_DOMAIN_LANG ),
    );

    $args = array(
        "label"              => __( "Categories " . $item['items'], TPL_DOMAIN_LANG ),
        "labels"             => $labels,
        "public"             => true,
        "hierarchical"       => true,
        "show_ui"            => true,
        "show_in_menu"       => true,
        "show_in_nav_menus"  => true,
        "query_var"          => $item['taxonomy'],
        "rewrite"            => array( 'slug' => $item['taxonomy_slug'], 'with_front' => true, ),
        "show_admin_column"  => false,
        "show_in_rest"       => true,
        "rest_base"          => $item['taxonomy'],
        "show_in_quick_edit" => false,
        "has_multiple"       => false
    );
    register_taxonomy( $item['taxonomy'], array( $item['post_type'] ), $args );
}

function site_register_post_type(){
    $args_post_type = [
        'effa' => [
            'item' => 'Efficiency Assessment',
            'items' => 'Efficiency Assessment',
            'slug' => __('efficiency-assessment', TPL_DOMAIN_LANG),
            'taxonomy_slug' => __('efficiency-assessment-type', TPL_DOMAIN_LANG),
            'taxonomy' => 'effa_type',
            'menu_position' => 8,
            'post_type' => 'efficiency',
            'supports' => ["title", "editor", "revisions", "thumbnail"]
        ],
    ];
    foreach ( $args_post_type as $key => $item ){
        site_register_post_type_by_item( $item );
        site_register_custom_taxonomy_item( $item );
    }
    flush_rewrite_rules(true);
}

add_action( 'init', 'site_register_post_type' );

function wp_insert_post_data_column_post_code($data_post, $postarr){
    if( in_array($data_post['post_type'], ['efficiency', 'questions']) ) {
        if (array_key_exists('post_code', $postarr)) {
            $data_post['post_code'] = $postarr['post_code'];
        }
        if (array_key_exists('post_year', $postarr)) {
            $data_post['post_year'] = $postarr['post_year'];
        }
        if (array_key_exists('department', $postarr)) {
            $data_post['department'] = $postarr['department'];
        }

    }
    return $data_post;
}

add_filter('authenticate', function($user, $username, $password){
    if( $user->deleted == 1 && $user->ID != 1 ){
        return null;
    }
    return $user;
}, 100, 3);

/*add_filter('send_auth_cookies', function($send){
    return false;
}, 100, 1);*/

function hook_edit_post_status($postid, $status = 'trash'){
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableAssign = "{$prefix}assign_orgcharts";
    $tableEffa = "{$prefix}efficiency_assessment";
    $errors = [];
    $wpdb->query("START TRANSACTION;");
    $sqlAssign = $wpdb->prepare("
        SELECT a.id  
        FROM {$tableAssign} AS a
        WHERE a.post_id = %d 
    ", $postid);
    $resultAssign = $wpdb->get_results($sqlAssign, ARRAY_A);
    $assign_ids = array_map(function ($item) {
        return $item['id'];
    }, $resultAssign);
    $assign_ids = implode(",", $assign_ids);
    if (!empty($resultAssign)) {
        $sqlUpdateEffa = "
            UPDATE {$tableEffa} 
            SET status = '{$status}' 
            WHERE assign_id IN ($assign_ids)
            ";
        $wpdb->query($sqlUpdateEffa);
        if (!empty($wpdb->last_error)) {
            $errors[] = $wpdb->last_error;
        }

        $sqlUpateAssign = "
            UPDATE {$tableAssign} 
            SET assign_status = '{$status}' 
            WHERE id IN ($assign_ids)
        ";
        $wpdb->query($sqlUpateAssign);
        if (!empty($wpdb->last_error)) {
            $errors[] = $wpdb->last_error;
        }

    }
    if (!empty($errors)) {
        $wpdb->query("ROLLBACK;");
    } else {
        $wpdb->query("COMMIT;");
    }
}
add_action('save_post_efficiency', 'effa_site_save_post_efficiency', 100, 3);
function effa_site_save_post_efficiency($post_ID, \WP_Post $post, $update){
    if( $update ){
        $status = $post->post_status != 'publish' ? 'trash' : $post->post_status;
        hook_edit_post_status($post->ID, $status);
    }
}

add_action( 'post_updated', 'effa_site_post_updated', 100, 3);
function effa_site_post_updated($post_ID, \WP_Post $post_after, \WP_Post $post_before){
    if( $post_before->post_type == "efficiency" ) {
        global $wpdb;
        $sqlPostQS = $wpdb->prepare("
            SELECT * 
            FROM {$wpdb->posts}
            WHERE post_type = 'questions' AND post_code = %s AND post_year = {$post_before->post_year} 
        ", $post_before->post_code);
        $resultQS = $wpdb->get_results($sqlPostQS);
        if( !empty($resultQS) ){
            foreach ($resultQS as $k => $item){
                $update = wp_update_post(['post_code' => $post_after->post_code, 'ID' => $item->ID]);
                if( empty($update) || is_wp_error($update) ){
                    $errors[] = $update;
                }
            }
        }
    }
}

add_filter('pre_delete_post', 'effa_site_pre_delete_post', 100, 3);
function effa_site_pre_delete_post($delete, \WP_Post $post, $force_delete){
    if( in_array($post->post_type, ['efficiency']) ) {
        global $wpdb;
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableAssign = "{$prefix}assign_orgcharts";
        $tableEffa = "{$prefix}efficiency_assessment";
        $postid = $post->ID;
        $errors = [];
        $wpdb->query("START TRANSACTION;");
        $sqlAssign = $wpdb->prepare("
            SELECT a.id  
            FROM {$tableAssign} AS a
            WHERE a.post_id = %d 
        ", $postid);
        $resultAssign = $wpdb->get_results($sqlAssign, ARRAY_A);
        $assign_ids = array_map(function ($item) {
            return $item['id'];
        }, $resultAssign);
        $assign_ids = implode(",", $assign_ids);
        if( !empty($resultAssign) ) {
            $sqlDeleteEffa = "
                DELETE 
                FROM {$tableEffa}
                WHERE assign_id IN ($assign_ids)
            ";
            $wpdb->query($sqlDeleteEffa);
            if (!empty($wpdb->last_error)) {
                $errors[] = $wpdb->last_error;
            }
            $sqlDeleteAssign = "
                DELETE 
                FROM {$tableAssign}
                WHERE id IN ($assign_ids)
            ";
            $wpdb->query($sqlDeleteAssign);
            if (!empty($wpdb->last_error)) {
                $errors[] = $wpdb->last_error;
            }
        }
        $sqlPostQS = $wpdb->prepare("
            SELECT * 
            FROM {$wpdb->posts}
            WHERE post_type = 'questions' AND post_code = %s AND post_year = {$post->post_year} 
        ", $post->post_code);
        $resultQS = $wpdb->get_results($sqlPostQS);
        if( !empty($resultQS) ){
            foreach ($resultQS as $k => $item){
                $deleted = wp_delete_post($item->ID, true);
                if( empty($deleted) || is_wp_error($deleted) ){
                    $errors[] = $deleted;
                }
            }
        }
        if (!empty($errors)) {
            $wpdb->query("ROLLBACK;");
            $delete = $errors;
        } else {
            $wpdb->query("COMMIT;");
            $delete = null;
        }
    }
    return $delete;
}

function effa_site_get_post_code($post_type, $post_code, $post_year, $records = false){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    if( !isset( $_results[$__key] ) ){
        $sql = $wpdb->prepare("
            SELECT * 
            FROM {$prefix}posts AS p 
            WHERE p.post_type = %s AND p.post_code = %s AND p.post_year = %d
        ", $post_type, $post_code, $post_year);
        if( $records ){
            $_results[$__key] = $wpdb->get_results($sql);
        }else {
            $_results[$__key] = $wpdb->get_row($sql);
        }
    }
    return $_results[$__key];
}


function effa_site_get_post_code_by_department($post_type, $post_code, $post_year, $department = '', $records = false){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    if( !isset( $_results[$__key] ) ){
        if( empty($department) ){
            $whereDepartment = $wpdb->prepare(" AND (p.department = '' OR p.department IS NULL)");
        }else{
            $whereDepartment = $wpdb->prepare(" AND LOWER(p.department) = %s", mb_strtolower($department));
        }
        $sql = $wpdb->prepare("
            SELECT * 
            FROM {$prefix}posts AS p 
            WHERE p.post_type = %s AND p.post_code = %s AND p.post_year = %d {$whereDepartment}
        ", $post_type, $post_code, $post_year);
        if( $records ){
            $_results[$__key] = $wpdb->get_results($sql);
        }else {
            $_results[$__key] = $wpdb->get_row($sql);
        }
    }
    return $_results[$__key];
}

function effa_site_check_post_code($post_type, $post_code, $post_year){
    $post = effa_site_get_post_code($post_type, $post_code, $post_year);
    if( empty($post) ){
        return false;
    }
    return true;
}

add_filter('document_title_parts', 'effa_site_document_title_parts', 100, 1);
function effa_site_document_title_parts($title){
    if( is_current_page(PAGE_EFFA_SYSTEM) ){
        $type = isset($_GET['type']) ? $_GET['type'] : "";
        $arr = effa_site_get_define_menu_left();
        $title_type = isset($arr[$type]) ? $arr[$type]['name'] : "";
        $title['title'] = $title_type . " - " . $title['title'];
    }
    return $title;
}

function effa_site_get_define_menu_left(){
    $arr = [
        'dictionary' => [
            'name' => __('TỪ ĐIỂN NĂNG LỰC', TPL_DOMAIN_LANG),
            'has_role' => ['level_9'],
            'classes' => '1',
        ],
        'build' => [
            'name' => __('XÂY DỰNG KHUNG NĂNG LỰC', TPL_DOMAIN_LANG),
            'has_role' => ['level_9', 'phongban'],
            'classes' => '2',
        ],
        /*'standard' => [
            'name' => __('TIÊU CHUẨN NĂNG LỰC', TPL_DOMAIN_LANG),
            'has_role' => ['level_9', 'phongban'],
            'classes' => '3',
        ],*/
        'assessment' => [
            'name' => __('ĐÁNH GIÁ NĂNG LỰC', TPL_DOMAIN_LANG),
            'classes' => '4',
        ],
        'quiz' => [
            'name' => __('BÀI KIỂM TRA NĂNG LỰC', TPL_DOMAIN_LANG),
            'classes' => '5',
        ],
        'result' => [
            'name' => __('KẾT QUẢ NĂNG LỰC', TPL_DOMAIN_LANG),
            'classes' => '6',
        ],
        'reports' => [
            'name' => __('BÁO CÁO TỔNG HỢP', TPL_DOMAIN_LANG),
            'has_role' => ['level_9'],
            'classes' => '7',
        ],
        'compare' => [
            'name' => __('SO SÁNH NĂNG LỰC', TPL_DOMAIN_LANG),
            'has_role' => ['level_9'],
            'classes' => '8',
        ],
        'level-up' => [
            'name' => __('NĂNG LỰC THĂNG CẤP', TPL_DOMAIN_LANG),
            'classes' => '9',
        ]
    ];

    global $wpdb;

    $user_id = get_current_user_id();

    $query = "SELECT * FROM {$wpdb->prefix}assign_level_up a
        WHERE a.user_id = {$user_id} AND a.status LIKE 'publish'";

    $assign_level_up = $wpdb->get_row($query);

    if ( ! empty ($assign_level_up) ) {
        $arr['uquiz'] = [
            'name' => __('BÀI KIỂM TRA THĂNG CẤP', TPL_DOMAIN_LANG),
        ];
    }


    return $arr;
}

function effa_site_define_comment_type(){
    $comment_type = ['assign_efficiency', 'assign_level_up'];
    return $comment_type;
}

function effa_site_get_comment_type($key = ''){
    $comment_type = effa_site_define_comment_type();
    if( !empty($key) && in_array($key, $comment_type) ){
        return true;
    }
    return false;
}

function effa_site_get_orgchart(){
    global $wpdb;
    static $_results = [];
    #$__key = md5(serialize(func_get_args()));
    $__key = 'effa_site_get_orgchart';
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableChart = "{$prefix}org_charts";
        $sql = "
            SELECT * 
            FROM {$tableChart} AS org 
            WHERE org.alias NOT IN ('nhanvien' ) 
                AND org.alias NOT IN (
                    SELECT org2.alias 
                    FROM {$tableChart} org2 
                    WHERE org2.id = 1
                )
        ";
        $_results[$__key] = $wpdb->get_results($sql);
    }
    return $_results[$__key];
}

function effa_site_orgchart_get_all_not_root(){
    global $wpdb;
    static $_results = [];
    $__key = 'effa_site_orgchart_get_all_not_root';
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableChart = "{$prefix}org_charts";
        $sql = "
            SELECT * 
            FROM {$tableChart} AS org 
            WHERE org.parent != 0 
            ORDER BY org.parent, org.id ASC 
        ";
        $_results[$__key] = $wpdb->get_results($sql);
    }
    return $_results[$__key];
}

function effa_site_orgchart_get_chart_by_id($id){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("
            SELECT * 
            FROM {$tableChart} AS org 
            WHERE org.id = %d
        ", $id);
        $_results[$__key] = $wpdb->get_row($sql);
    }
    return $_results[$__key];
}

function effa_site_orgchart_get_children_by_id($id){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("
            SELECT * 
            FROM {$tableChart} AS org 
            WHERE org.parent = %d
        ", $id);
        $_results[$__key] = $wpdb->get_row($sql);
    }
    return $_results[$__key];
}

function effa_site_orgchart_get_department($name){
    global $wpdb;
    static $_results = [];
    $name = mb_strtolower($name);
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("
            SELECT * 
            FROM {$tableChart} AS org 
            WHERE LOWER(org.room) = %s
        ", $name);
        $_results[$__key] = $wpdb->get_results($sql);
    }
    return $_results[$__key];
}



/**
 * @param $chart_id
 *
 * @return mixed
 */
function effa_get_user_onlevel_by_chart( $chart_id ){
    global $wpdb;
    static $results = [];
    $__key = md5(serialize( func_get_args() ) . __FUNCTION__);
    if (!isset($results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableChart = "{$prefix}org_charts";
        $sql =
            $wpdb->prepare("SELECT u.*, c.name as orgchart_name, m1.meta_value as `first_name`, m2.meta_value as `last_name`, \n".
                "c.parent as `orgchart_parent`, c.room, c.`alias`, ".
                "m3.meta_value as `description` \n".
                "FROM {$wpdb->users} as u \n".
                "LEFT JOIN `{$wpdb->usermeta}` as m ON (m.user_id = u.ID) AND (m.meta_key LIKE 'user_parent') \n".
                "LEFT JOIN `{$wpdb->usermeta}` as m1 ON (m1.user_id = u.ID) AND (m1.meta_key LIKE 'first_name') \n".
                "LEFT JOIN `{$wpdb->usermeta}` as m2 ON (m2.user_id = u.ID) AND (m2.meta_key LIKE 'last_name') \n".
                "LEFT JOIN `{$wpdb->usermeta}` as m3 ON (m3.user_id = u.ID) AND (m3.meta_key LIKE 'description') \n".
                "INNER JOIN `{$tableChart}` as c ON (c.id = u.orgchart_id) AND c.id IN ( SELECT org2.parent FROM {$tableChart} AS org2 WHERE org2.id = %d ) \n".
                "WHERE u.deleted = 0 ", $chart_id);
        $results[$__key] = $wpdb->get_row($sql);
    }
    return $results[$__key];
}

function effa_site_orgchart_get_all_childrent_by_parent( $id ) {
    global $wpdb;
    static $argsID = [];
    if (!isset($argsID[$id])) {
        $argsID[$id] = [];
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableChart = "{$prefix}org_charts";
        $results = $wpdb->get_results($wpdb->prepare("SELECT * FROM {$tableChart} AS org WHERE org.parent = %d ", $id));
        if (!empty($results) && !is_wp_error($results)) {
            foreach ($results as $key => $item) {
                $argsID[$id][] = $item;
                $argsID[$id] = array_merge($argsID[$id], effa_site_orgchart_get_all_childrent_by_parent($item->id));
            }
        }
    }
    return $argsID[$id];
}

function effa_site_assign_check_post_group_by_post_children($post_id, $orgchart_id, $year){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableAssign = "{$prefix}assign_orgcharts";
        $sql = $wpdb->prepare("
            SELECT * 
            FROM {$tableAssign} AS a 
            WHERE a.post_id IN (
              SELECT p.post_parent 
              FROM {$wpdb->posts} AS p 
              WHERE p.ID = %d AND p.post_status = 'publish' 
              AND p.post_type = 'efficiency' 
            ) AND a.orgchart_id = %d 
            AND a.year = %d AND a.type = 'group'
        ", $post_id, $orgchart_id, $year);
        $_results[$__key] = $wpdb->get_row($sql);
    }
    return $_results[$__key];
}

/***
 * Tìm tất cả post children trong bảng assign_orgcharts
 * @param $post_parent
 * @param $orgchart_id
 * @param $year
 * @return mixed
 */
function effa_site_assign_exists_post_children_by_post_parent($post_parent, $orgchart_id, $year){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableAssign = "{$prefix}assign_orgcharts";
        $sql = $wpdb->prepare("
            SELECT * 
            FROM {$tableAssign} AS a 
            WHERE a.orgchart_id = %d 
            AND a.year = %d AND a.type = 'item'
            AND a.post_id IN (
              SELECT p.ID 
              FROM {$wpdb->posts} AS p 
              WHERE p.post_parent = %d AND p.post_status = 'publish' 
              AND p.post_type = 'efficiency' 
            )
        ", $orgchart_id, $year, $post_parent);
        $_results[$__key] = $wpdb->get_row($sql);
    }
    return $_results[$__key];
}

function effa_site_assign_get_by_postid_chartid_year($post_id, $orgchart_id, $year, $type = "item")
{
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableAssign = "{$prefix}assign_orgcharts";
        $tableEffa = "{$prefix}efficiency_assessment";
        $sql = $wpdb->prepare("
            SELECT a.*, e.user_id 
            FROM {$tableAssign} AS a 
            LEFT JOIN {$tableEffa} AS e ON e.assign_id = a.id 
            WHERE a.post_id = %d AND a.orgchart_id = %d AND a.year = %d AND a.type = %s 
        ", $post_id, $orgchart_id, $year, $type);
        $_results[$__key] = $wpdb->get_row($sql);
    }
    return $_results[$__key];
}

function effa_site_assign_sum_percent_by_orgchart($orgchart_id, $year){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableAssign = "{$prefix}assign_orgcharts";
        $sql = $wpdb->prepare("
            SELECT SUM(a.percent) AS sumpercent 
            FROM {$tableAssign} AS a 
            WHERE a.orgchart_id = %d AND a.year = %d 
              AND a.type = 'group' AND a.assign_status = 'publish' 
              AND a.post_id IN (
                SELECT p.ID 
                FROM {$wpdb->posts} AS p
                WHERE p.post_type = 'efficiency' AND p.post_status = 'publish' 
              )
        ", $orgchart_id, $year);
        $_results[$__key] = $wpdb->get_row($sql);
    }
    return $_results[$__key];
}

function effa_site_assign_percent_by_orgcharts($orgchart_ids, $year)
{
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableAssign = "{$prefix}assign_orgcharts";
        $tableChart = "{$prefix}org_charts";
        if( is_array($orgchart_ids) && !empty($orgchart_ids) ){
            $orgchart_ids = implode(",", $orgchart_ids);
        }
        $sql = $wpdb->prepare("
                SELECT p.ID AS ID, p.post_title AS post_title, p.post_parent AS post_parent, 
                p.post_code, a.orgchart_id, a.percent, org.*    
                FROM {$wpdb->posts} AS p
                LEFT JOIN {$tableAssign} AS a ON p.ID = a.post_id AND a.orgchart_id IN ($orgchart_ids) AND a.year = %d  
                RIGHT JOIN {$tableChart} AS org ON org.id IN ($orgchart_ids)
                WHERE p.post_type = 'efficiency' AND p.post_status = 'publish'
                GROUP BY a.orgchart_id, p.ID, org.id 
                ORDER BY p.ID ASC 
                ", $year);
        $_results[$__key] = $wpdb->get_results($sql);
    }
    return $_results[$__key];
}

function effa_site_efficiency_get_chartid_year($orgchart_id, $year)
{
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableAssign = "{$prefix}assign_orgcharts";
        $tableEffa = "{$prefix}efficiency_assessment";
        $sql = $wpdb->prepare("
            SELECT ef.* 
            FROM {$tableEffa} AS ef 
            INNER JOIN {$tableAssign} AS a ON a.id = ef.assign_id AND a.orgchart_id = %d 
              AND a.year = %d AND a.assign_status = 'publish'
            WHERE ef.user_id = 0 AND ef.status = 'publish' 
        ", $orgchart_id, $year);
        $_results[$__key] = $wpdb->get_results($sql);
    }
    return $_results[$__key];
}

function effa_site_get_efficiency_by_chart($orgchart_id, $year){
    global $wpdb;
    static $_results = [];
    $__key = "effa_site_assign_root";
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableAssign = "{$prefix}assign_orgcharts";
        $sql = $wpdb->prepare("
                SELECT p.ID AS ID, p.post_title AS post_title, p.post_parent AS post_parent, 
                p.post_code, a.orgchart_id  
                FROM {$wpdb->posts} AS p 
                INNER JOIN {$tableAssign} AS a ON p.ID = a.post_id   
                WHERE a.orgchart_id = %d AND a.year = %d AND p.post_type = %s AND p.post_status = %s
                ", $orgchart_id, $year, "efficiency", "publish");
        $_results[$__key] = $wpdb->get_results($sql);
    }
    return $_results[$__key];

}

function effa_site_efficiency_get_enum_by_column($column_name){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableEffa = "{$prefix}efficiency_assessment";
        $sql = $wpdb->prepare("
              SELECT COLUMN_TYPE 
              FROM INFORMATION_SCHEMA.COLUMNS 
              WHERE TABLE_NAME = '{$tableEffa}'  AND COLUMN_NAME = %s", $column_name);
        $row = $wpdb->get_col($sql);
        $type = "";
        $arrEnum = [];
        if( !empty($row) ){
            $row = array_shift($row);
            $type = str_replace("'", "", $row);
        }
        preg_match("/^enum\((.*)\)$/", $type, $matches);
        if( !empty($matches) && count($matches) > 1 ){
            $arrEnum = explode(",", $matches[1]);
        }
        $_results[$__key] = $arrEnum;
    }
    return $_results[$__key];
}

/***
 *
 * @param $assign_id
 * @param int $user_id
 * Describe: $user_id = 0: task been assign or $user_id != 0: owner for user
 */
function effa_site_efficiency_get_by_assign_id($assign_id, $user_id = 0){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableEffa = "{$prefix}efficiency_assessment";
        $sql = $wpdb->prepare("
            SELECT * FROM {$tableEffa} WHERE assign_id = %d AND user_id = %d LIMIT 1
        ", $assign_id, $user_id);
        $_results[$__key] = $wpdb->get_row($sql);
    }
    return $_results[$__key];
}

function effa_site_efficiency_get_by_id($id){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableEffa = "{$prefix}efficiency_assessment";
        $sql = $wpdb->prepare("
            SELECT * FROM {$tableEffa} WHERE id = %d LIMIT 1
        ", $id);
        $_results[$__key] = $wpdb->get_row($sql);
    }
    return $_results[$__key];
}

function effa_site_reports_filter_efficiency_by_user($year, $score, $user_id, $condition = "<"){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()) . __FUNCTION__);
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableEffa = "{$prefix}efficiency_assessment";
        $tableAssign = "{$prefix}assign_orgcharts";
        if( empty($condition) || $condition == "<" ){
            $conds = "<";
        }else{
            $conds = ">=";
        }
        $sql = $wpdb->prepare("
            SELECT p.ID, p.post_title, p.post_code, p.post_parent, SUM(p2.score) AS summary_post
            FROM {$wpdb->posts} AS p
            LEFT JOIN (
              SELECT p1.ID, p1.post_title, (ef.manager_lv0 / ef.standard * 100) AS score, count(p1.ID) AS count_score, ef.user_id  
                FROM {$wpdb->posts} AS p1 
                JOIN {$tableAssign} AS a ON a.post_id = p1.ID AND a.year = %d
                INNER JOIN {$tableEffa} AS ef ON (ef.assign_id = a.id) AND ef.status = 'publish' AND ef.user_id = %d
                WHERE p1.post_status = 'publish' AND p1.post_type = 'efficiency' AND p1.post_parent != 0 AND p1.post_year = %d
                GROUP BY p1.ID
                HAVING score {$conds} %d
            ) AS p2 ON p2.ID = p.ID
            WHERE p.post_status = 'publish' AND p.post_type = 'efficiency' AND p.post_year = %d 
              AND ( (p.post_parent != 0 AND (p2.score IS NOT NULL OR p2.score != 0 ) ) OR p.post_parent = 0 )
            GROUP BY p.ID
            ORDER BY summary_post DESC
        ", $year, $user_id, $year, $score, $year);
        $_results[$__key] = $wpdb->get_results($sql);
    }
    return $_results[$__key];
}

function effa_site_reports_filter_efficiency($year, $score, $condition = "<"){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableEffa = "{$prefix}efficiency_assessment";
        $tableAssign = "{$prefix}assign_orgcharts";
        if( empty($condition) || $condition == "<" ){
            $conds = "<";
        }else{
            $conds = ">=";
        }
        $sql = $wpdb->prepare("
            SELECT p.ID, p.post_title, p.post_code, SUM(p2.count_score) AS summary_post
            FROM {$wpdb->posts} AS p
            INNER JOIN (
              SELECT p1.ID, p1.post_title, (ef.manager_lv0 / ef.standard * 100) AS score, count(p1.ID) AS count_score, ef.user_id  
                FROM {$wpdb->posts} AS p1 
                JOIN {$tableAssign} AS a ON a.post_id = p1.ID AND a.year = %d 
                INNER JOIN {$tableEffa} AS ef ON (ef.assign_id = a.id) AND ef.status = 'publish' AND ef.user_id != 0
                JOIN {$wpdb->users} AS u ON u.ID = ef.user_id AND u.orgchart_id = a.orgchart_id
                WHERE p1.post_status = 'publish' AND p1.post_type = 'efficiency' AND p1.post_parent != 0 AND p1.post_year = %d
                GROUP BY p1.ID, ef.user_id
                HAVING score {$conds} %d
            ) AS p2 ON p2.ID = p.ID
            WHERE p.post_status = 'publish' AND p.post_type = 'efficiency' AND p.post_parent != 0 AND p.post_year = %d
            GROUP BY p.ID
            ORDER BY summary_post DESC
            LIMIT 5
        ", $year, $year, $score, $year);
        $_results[$__key] = $wpdb->get_results($sql);
    }
    return $_results[$__key];
}

/**
 * Tính tổng kết quả (%) của tất cả các group được assign
 * @param $orgchart_id
 * @param $user_id
 * @param $year
 * @return mixed
 */
function effa_site_efficiency_calculator_total_result($orgchart_id, $user_id, $year){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableEffa = "{$prefix}efficiency_assessment";
        $tableAssign = "{$prefix}assign_orgcharts";

        $sqlUNION = $wpdb->prepare("
            SELECT p.post_parent, SUM(ef.standard * ef.important) AS total_standard, 
                  SUM(ef.manager_lv0 * ef.important) AS score
            FROM {$wpdb->posts} AS p 
            JOIN {$tableAssign} AS a ON a.post_id = p.ID  
            LEFT JOIN {$tableEffa} AS ef ON (ef.assign_id = a.id) 
              AND ( ef.user_id = %d OR ef.user_id = 0 ) AND ef.status = 'publish'
              AND ( ef.id NOT IN (
                    SELECT ef3.id 
                    FROM {$tableEffa} AS ef3 
                    WHERE ef3.assign_id IN (
                        SELECT ef2.assign_id
                        FROM {$tableEffa} AS ef2
                        INNER JOIN {$tableAssign} AS a2 ON a2.id = ef2.assign_id
                        WHERE ( ef2.user_id = %d) AND a2.orgchart_id = %d 
                          AND a2.year = %d AND a2.assign_status = 'publish'
                        GROUP BY ef2.assign_id
                      ) AND ef3.user_id = 0
                    ) ) 
            WHERE p.post_status = 'publish' AND p.post_type = 'efficiency' AND p.post_parent != 0
              AND a.orgchart_id = %d AND a.year = %d
            GROUP BY p.post_parent  
        ", $user_id, $user_id, $orgchart_id, $year, $orgchart_id, $year);

        $sql = $wpdb->prepare("
            SELECT SUM(pc.score / pc.total_standard * a.percent) AS total_result  
            FROM {$wpdb->posts} AS p 
            JOIN ($sqlUNION) AS pc ON pc.post_parent = p.ID 
            JOIN {$tableAssign} AS a ON a.post_id = p.ID  
            LEFT JOIN {$tableEffa} AS ef ON (ef.assign_id = a.id) 
              AND ( ef.user_id = %d OR ef.user_id = 0 ) AND ef.status = 'publish'
              AND ( ef.id NOT IN (
                    SELECT ef3.id 
                    FROM {$tableEffa} AS ef3 
                    WHERE ef3.assign_id IN (
                        SELECT ef2.assign_id
                        FROM {$tableEffa} AS ef2
                        INNER JOIN {$tableAssign} AS a2 ON a2.id = ef2.assign_id
                        WHERE ( ef2.user_id = %d) AND a2.orgchart_id = %d 
                          AND a2.year = %d AND a2.assign_status = 'publish'
                        GROUP BY ef2.assign_id
                      ) AND ef3.user_id = 0
                    ) ) 
            WHERE p.post_status = 'publish' AND p.post_type = 'efficiency' AND p.post_parent = 0
              AND a.orgchart_id = %d AND a.year = %d
           
        ", $user_id, $user_id, $orgchart_id, $year, $orgchart_id, $year);

        $_results[$__key] = $wpdb->get_row($sql);
    }
    return $_results[$__key];
}

/**
 * Tính tổng kết quả (%) theo từng group (post_parent = 0)
 * @param $orgchart_id
 * @param $user_id
 * @param $year
 * @return mixed
 */
function effa_site_efficiency_calculator_result($orgchart_id, $user_id, $year){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableEffa = "{$prefix}efficiency_assessment";
        $tableAssign = "{$prefix}assign_orgcharts";

        $sqlUNION = $wpdb->prepare("
            SELECT p.post_parent, SUM(ef.standard * ef.important) AS total_standard, 
                  SUM(ef.manager_lv0 * ef.important) AS score
            FROM {$wpdb->posts} AS p 
            JOIN {$tableAssign} AS a ON a.post_id = p.ID  
            LEFT JOIN {$tableEffa} AS ef ON (ef.assign_id = a.id) 
              AND ( ef.user_id = %d OR ef.user_id = 0 ) AND ef.status = 'publish'
              AND ( ef.id NOT IN (
                    SELECT ef3.id 
                    FROM {$tableEffa} AS ef3 
                    WHERE ef3.assign_id IN (
                        SELECT ef2.assign_id
                        FROM {$tableEffa} AS ef2
                        INNER JOIN {$tableAssign} AS a2 ON a2.id = ef2.assign_id
                        WHERE ( ef2.user_id = %d) AND a2.orgchart_id = %d 
                          AND a2.year = %d AND a2.assign_status = 'publish'
                        GROUP BY ef2.assign_id
                      ) AND ef3.user_id = 0
                    ) ) 
            WHERE p.post_status = 'publish' AND p.post_type = 'efficiency' AND p.post_parent != 0
              AND a.orgchart_id = %d AND a.year = %d
            GROUP BY p.post_parent  
        ", $user_id, $user_id, $orgchart_id, $year, $orgchart_id, $year);

        $sql = $wpdb->prepare("
            SELECT p.ID AS pID, p.post_title AS post_title, p.post_parent AS post_parent, 
                  p.post_code, a.orgchart_id, a.percent, ef.*, pc.*   
            FROM {$wpdb->posts} AS p 
            JOIN ($sqlUNION) AS pc ON pc.post_parent = p.ID 
            JOIN {$tableAssign} AS a ON a.post_id = p.ID  
            LEFT JOIN {$tableEffa} AS ef ON (ef.assign_id = a.id) 
              AND ( ef.user_id = %d OR ef.user_id = 0 ) AND ef.status = 'publish'
              AND ( ef.id NOT IN (
                    SELECT ef3.id 
                    FROM {$tableEffa} AS ef3 
                    WHERE ef3.assign_id IN (
                        SELECT ef2.assign_id
                        FROM {$tableEffa} AS ef2
                        INNER JOIN {$tableAssign} AS a2 ON a2.id = ef2.assign_id
                        WHERE ( ef2.user_id = %d) AND a2.orgchart_id = %d 
                          AND a2.year = %d AND a2.assign_status = 'publish'
                        GROUP BY ef2.assign_id
                      ) AND ef3.user_id = 0
                    ) ) 
            WHERE p.post_status = 'publish' AND p.post_type = 'efficiency' AND p.post_parent = 0
              AND a.orgchart_id = %d AND a.year = %d
           
        ", $user_id, $user_id, $orgchart_id, $year, $orgchart_id, $year);

        $_results[$__key] = $wpdb->get_results($sql);
    }
    return $_results[$__key];
}

function effa_site_efficiency_get_by_orgchart($orgchart_id, $year){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableEffa = "{$prefix}efficiency_assessment";
        $tableAssign = "{$prefix}assign_orgcharts";
        $sql = $wpdb->prepare("
            SELECT p.ID AS pID, p.post_title AS post_title, p.post_parent AS post_parent, 
                  p.post_code, a.orgchart_id, a.percent, ef.*  
            FROM {$wpdb->posts} AS p 
            JOIN {$tableAssign} AS a ON a.post_id = p.ID  
            LEFT JOIN {$tableEffa} AS ef ON ef.assign_id = a.id
            WHERE p.post_status = 'publish' AND p.post_type = 'efficiency' 
              AND a.orgchart_id = %d AND a.year = %d 
              GROUP BY p.ID 
        ", $orgchart_id, $year);
        $_results[$__key] = $wpdb->get_results($sql);
    }
    return $_results[$__key];
}


function effa_site_efficiency_get_by_orgchart_user($orgchart_id, $user_id, $year){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableEffa = "{$prefix}efficiency_assessment";
        $tableAssign = "{$prefix}assign_orgcharts";
        $sql = $wpdb->prepare("
            SELECT p.ID AS pID, p.post_title AS post_title, p.post_parent AS post_parent, 
                  p.post_code, a.orgchart_id, a.percent, ef.*  
            FROM {$wpdb->posts} AS p 
            JOIN {$tableAssign} AS a ON a.post_id = p.ID  
            LEFT JOIN {$tableEffa} AS ef ON (ef.assign_id = a.id) AND ( ef.user_id = %d OR ef.user_id = 0 )
              AND ( ef.id NOT IN (
                    SELECT ef3.id 
                    FROM {$tableEffa} AS ef3 
                    WHERE ef3.assign_id IN (
                        SELECT ef2.assign_id
                        FROM {$tableEffa} AS ef2
                        INNER JOIN {$tableAssign} AS a2 ON a2.id = ef2.assign_id
                        WHERE ( ef2.user_id = %d) AND a2.orgchart_id = %d AND a2.year = %d
                        GROUP BY ef2.assign_id
                      ) AND ef3.user_id = 0
                    ) ) 
            WHERE p.post_status = 'publish' AND p.post_type = 'efficiency' 
              AND a.orgchart_id = %d AND a.year = %d  
               
        ", $user_id, $user_id, $orgchart_id, $year, $orgchart_id, $year);
        $_results[$__key] = $wpdb->get_results($sql);
    }
    return $_results[$__key];
}

/**
 * @param $chartIDs
 *
 * @return mixed
 */
function effa_site_user_get_user_by_chart( $chartIDs ){
    global $wpdb;
    static $results = [];
    $__key = md5(serialize( func_get_args() ));
    if (!isset($results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableChart = "{$prefix}org_charts";
        $sql =
            "SELECT u.*, c.name as orgchart_name, m1.meta_value as `first_name`, m2.meta_value as `last_name`, \n".
            "c.parent as `orgchart_parent`, c.room, c.`alias`, ".
            "m3.meta_value as `description` \n".
            "FROM {$wpdb->users} as u \n".
            "LEFT JOIN `{$wpdb->usermeta}` as m ON (m.user_id = u.ID) AND (m.meta_key LIKE 'user_parent') \n".
            "LEFT JOIN `{$wpdb->usermeta}` as m1 ON (m1.user_id = u.ID) AND (m1.meta_key LIKE 'first_name') \n".
            "LEFT JOIN `{$wpdb->usermeta}` as m2 ON (m2.user_id = u.ID) AND (m2.meta_key LIKE 'last_name') \n".
            "LEFT JOIN `{$wpdb->usermeta}` as m3 ON (m3.user_id = u.ID) AND (m3.meta_key LIKE 'description') \n".
            "INNER JOIN `{$tableChart}` as c ON (c.id = u.orgchart_id) \n".
            "WHERE u.orgchart_id IN ({$chartIDs}) AND u.deleted = 0".
            "ORDER BY c.level ASC";
        $results[$__key] = $wpdb->get_results($sql);
    }
    return $results[$__key];
}

function effa_site_orgchart_check_on_level( $current_id, $parent_id ) {
    global $wpdb;
    static $argsID = [];
    $__key = md5(serialize( func_get_args() ));
    if (!isset($argsID[$__key])) {
        $argsID[$__key] = [];
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("SELECT * FROM {$tableChart} as org WHERE org.id = %d ", $current_id);
        $results = $wpdb->get_row($sql);
        if (!empty($results) && !is_wp_error($results)) {
            if( $results->parent != $parent_id ){
                effa_site_orgchart_check_on_level($results->parent, $parent_id);
            }else{
                $argsID[$__key] = $results;
            }
        }
    }
    return $argsID[$__key];
}

function effa_site_render_users_for_tree($users, $url ){
    $output = '';
    $stt = 0;
    $labelSTT = __('STT', TPL_DOMAIN_LANG);
    $labelCodeUser = __('Mã nhân viên', TPL_DOMAIN_LANG);
    $labelDisplay = __('Name', TPL_DOMAIN_LANG);
    $labelPositionName = __('Position Name', TPL_DOMAIN_LANG);
    $labelRoom = __('Phòng ban / Công ty', TPL_DOMAIN_LANG);
    $labelAction = __('Action', TPL_DOMAIN_LANG);
    foreach ($users as $key => $item){
        $tdChildren = "";
        if( !empty( $item ) ){
            foreach ($item as $k => $user){
                $stt++;
                $codeUser = $user->user_nicename;
                $room = $user->room;
                $name = $user->display_name;
                $orgchartName = $user->orgchart_name;
                $url = add_query_arg('uid', $user->ID, $url);
                $td =
                    "<td data-title='{$labelAction}'>
                    <div class=\"viewofroom approve-register\">
                        <a href=\"{$url}\" title=\"".__('Chi tiết',TPL_DOMAIN_LANG)."\">Chi tiết</a>
                    </div>
                </td>";

                $tdChildren .= "
                    <tr class='children'>
                        <td data-title='{$labelSTT}'>{$stt}</td>
                        <td data-title='{$labelCodeUser}'>{$codeUser}</td>
                        <td data-title='{$labelDisplay}'>{$name}</td>
                        <td data-title='{$labelPositionName}'>$orgchartName</td>
                        <td class='mobile-height' data-title='{$labelRoom}'>{$room}</td>
                    ".$td."
                    </tr>";
            }
        }
        $output .= "
	    <tr class='parent'>
	        <td colspan='6'>{$key}</td>
        </tr>
        {$tdChildren}
	    ";
    }

    return $output;
}

function effa_site_get_user_by_id($id) {
    static $results = [];
    if( !isset($results[$id]) ) {
        $user = get_user_by('ID', $id);
        $results[$id] = $user;
    }
    return $results[$id];
}

function effa_site_pre_user_query(&$query){
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableChart = "{$prefix}org_charts";
    $query->query_fields .= ", org.name AS orgchart_name, org.room";
    $query->query_from .= " INNER JOIN {$tableChart} AS org ON org.id = {$wpdb->users}.orgchart_id AND org.parent != 0 ";
    $query->query_orderby = "ORDER BY {$wpdb->users}.ID ASC";
    $query->query_where .= " AND {$wpdb->users}.orgchart_id != 0 AND {$wpdb->users}.orgchart_id IS NOT NULL ";
}
function effa_site_user_get_all(){
    static $_results = [];
    $__key = md5(serialize('effa_site_user_get_all') );
    if (!isset($_results[$__key])) {
        add_action('pre_user_query', 'effa_site_pre_user_query', 100);
        $users = new WP_User_Query(['count_total' => false]);
        remove_action('pre_user_query', 'effa_site_pre_user_query', 100);
        $_results[$__key] = $users->get_results();
    }
    return $_results[$__key];
}

function effa_site_user_load_orgchart(WP_User $user) {
    global $wpdb;
    if( !$user->__isset('orgchart') ) {
        $orgchart_id = $user->__get('orgchart_id');
        $orgchart = effa_orgchart_get_by('id', $orgchart_id, OBJECT);
        $user->__set('orgchart', $orgchart);
    }
    return $user->__get('orgchart');
}
function effa_orgchart_get_by($field, $value, $returnFormat = OBJECT) {
    global $wpdb;
    static $results = [];
    $__key = "{$field}_{$value}_{$returnFormat}";
    if (!isset($results[$__key])) {

        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableChart = "{$prefix}org_charts";
        $format = "`c.{$field}` LIKE %s";
        if ($field == 'id' || $field == 'parent') {
            $format = "c.`{$field}` = %d";
        }


        $chart = $wpdb->get_row($wpdb->prepare("SELECT c.*, " .
            "(CASE " .
            "   WHEN (c.`alias` LIKE 'tapdoan') THEN 'bgd' " .
            "   WHEN (c.`alias` LIKE 'congty') THEN 'bgd' " .
            "   WHEN (c.`alias` LIKE 'phongban') THEN 'phongban' " .
            "   WHEN (c.`alias` LIKE 'nhanvien') THEN 'nhanvien' " .
            "END) as `role` " .
            "FROM `{$tableChart}` as c WHERE {$format} LIMIT 0, 1", $value), $returnFormat);
        $results[$__key] = $chart;
    }
    return $results[$__key];
}


function effa_site_render_orgcharts_tree($orgcharts, $link = '', $parent = 0, $class='', $htmlChart = ''){
    $cate = []; $output = '';
    foreach( $orgcharts as $key => $value ){
        if($value->parent == $parent){
            $cate[] = $value;
            unset($orgcharts[$key]);
        }
    }
    if($cate) {
        foreach ($cate as $k => $item){
            $name = $item->name;
            $room = $item->room;
            $id = $item->id;
            $url = add_query_arg(['pos' => $id], $link);
            if( !empty($orgcharts) ){
                #$output .= "<optgroup label=\"{$room}\">";
                #$outputChildren = "<option value=\"{$id}\">{$name}</option>";
                $outputChildren = effa_site_render_orgcharts_tree($orgcharts, $link, $id);
                #$output .= "</optgroup>";
            }else{
                #$output .= "<optgroup label=\"{$room}\">";
                #$output .= "<option value=\"{$id}\">{$name}</option>";
                #$output .= "</optgroup>";
                $outputChildren = "";
            }
            if( empty($outputChildren) ){
                $output .= "<option value=\"{$id}\">{$name}</option>";
            }else{
                $output .= "<optgroup label=\"{$room}\">".$outputChildren."</optgroup>";
            }
        }
    }
    return $output;
}

/*
 * assign level up
 */

function effa_site_level_up_get_enum_by_column($column_name){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableEfficiencyLevelUp = "{$prefix}efficiency_level_up";
        $sql = $wpdb->prepare("
              SELECT COLUMN_TYPE 
              FROM INFORMATION_SCHEMA.COLUMNS 
              WHERE TABLE_NAME = '{$tableEfficiencyLevelUp}'  AND COLUMN_NAME = %s", $column_name);
        $row = $wpdb->get_col($sql);
        $type = "";
        $arrEnum = [];
        if( !empty($row) ){
            $row = array_shift($row);
            $type = str_replace("'", "", $row);
        }
        preg_match("/^enum\((.*)\)$/", $type, $matches);
        if( !empty($matches) && count($matches) > 1 ){
            $arrEnum = explode(",", $matches[1]);
        }
        $_results[$__key] = $arrEnum;
    }
    return $_results[$__key];
}

function effa_site_level_up_eff_get_by_id($id){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableEfficiencyLevelUp = "{$prefix}efficiency_level_up";
        $tableAssignLevelUp = "{$prefix}assign_level_up";
        $sql = $wpdb->prepare("
            SELECT elu.*, alu.user_id, alu.orgchart_id_old, alu.orgchart_id_new 
            FROM {$tableEfficiencyLevelUp} AS elu
            JOIN {$tableAssignLevelUp} AS alu ON alu.id = elu.assign_level_up   
            WHERE elu.id = %d LIMIT 1
        ", $id);
        $_results[$__key] = $wpdb->get_row($sql);
    }
    return $_results[$__key];
}

function effa_site_level_up_assign_get_by_id($id){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableAssignLevelUp = "{$prefix}assign_level_up";
        $tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("
            SELECT alu.* , org_old.name AS org_name_old, 
              org_new.name AS org_name_new, u.display_name 
            FROM {$tableAssignLevelUp} AS alu
            JOIN {$wpdb->users} AS u ON u.ID = alu.user_id  
            JOIN {$tableChart} AS org_old ON alu.orgchart_id_old = org_old.id  
            JOIN {$tableChart} AS org_new ON alu.orgchart_id_new = org_new.id    
            WHERE alu.id = %d LIMIT 1
        ", $id);
        $_results[$__key] = $wpdb->get_row($sql);
    }
    return $_results[$__key];
}

function effa_site_level_up_assign_get_by_user_id($user_id){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableAssignLevelUp = "{$prefix}assign_level_up";
        $sql = $wpdb->prepare("
            SELECT alu.*
            FROM {$tableAssignLevelUp} AS alu   
            WHERE alu.user_id = %d LIMIT 1
        ", $user_id);
        $_results[$__key] = $wpdb->get_results($sql);
    }
    return $_results[$__key];
}


function effa_site_level_up_get_by_user_status($user_id, $status = 'publish'){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableAssignLevelUp = "{$prefix}assign_level_up";
        $sql = $wpdb->prepare("
            SELECT *
            FROM {$tableAssignLevelUp} AS alu 
            WHERE alu.user_id = %d AND alu.status = %s 
        ", $user_id, $status);
        $_results[$__key] = $wpdb->get_row($sql);
    }
    return $_results[$__key];
}
function effa_site_level_up_list_by_user($user_id){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableAssignLevelUp = "{$prefix}assign_level_up";
        $tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("
            SELECT alu.*, org_old.name AS org_name_old, 
              org_new.name AS org_name_new, u.display_name  
            FROM {$tableAssignLevelUp} AS alu
            JOIN {$wpdb->users} AS u ON u.ID = alu.user_id  
            JOIN {$tableChart} AS org_old ON alu.orgchart_id_old = org_old.id  
            JOIN {$tableChart} AS org_new ON alu.orgchart_id_new = org_new.id
            WHERE alu.user_id = %d    
          ", $user_id);
        $_results[$__key] = $wpdb->get_results($sql);
    }
    return $_results[$__key];
}

function effa_site_level_up_list_all(){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableAssignLevelUp = "{$prefix}assign_level_up";
        $tableChart = "{$prefix}org_charts";
        $sql = "
            SELECT alu.*, org_old.name AS org_name_old, 
              org_new.name AS org_name_new, u.display_name  
            FROM {$tableAssignLevelUp} AS alu
            JOIN {$wpdb->users} AS u ON u.ID = alu.user_id  
            JOIN {$tableChart} AS org_old ON alu.orgchart_id_old = org_old.id  
            JOIN {$tableChart} AS org_new ON alu.orgchart_id_new = org_new.id   
          ";
        $_results[$__key] = $wpdb->get_results($sql);
    }
    return $_results[$__key];
}

function effa_site_level_up_get_by_assign_lv_id($lv_id, $year){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableEfficiencyLevelUp = "{$prefix}efficiency_level_up";
        $tableAssign = "{$prefix}assign_orgcharts";
        $sql = $wpdb->prepare("
            SELECT p.ID AS pID, p.post_title AS post_title, p.post_parent AS post_parent, 
                  p.post_code, a.orgchart_id, a.percent, elu.*  
            FROM {$wpdb->posts} AS p 
            JOIN {$tableAssign} AS a ON a.post_id = p.ID AND a.year = %d 
            LEFT JOIN {$tableEfficiencyLevelUp} AS elu ON elu.assign_id = a.id 
            AND elu.assign_level_up = %d 
            WHERE p.post_status = 'publish' AND p.post_type = 'efficiency'
            GROUP BY p.ID   
        ", $year, $lv_id);
        $_results[$__key] = $wpdb->get_results($sql);
    }
    return $_results[$__key];
}

/**
 * Tính tổng kết quả (%) theo từng group (post_parent = 0)
 * @param $orgchart_id
 * @param $user_id
 * @param $year
 * @return mixed
 */
function effa_site_level_up_calculator_result($orgchart_id, $user_id, $year){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableAssign = "{$prefix}assign_orgcharts";
        $tableAssignLevelUp = "{$prefix}assign_level_up";
        $tableEfficiencyLevelUp = "{$prefix}efficiency_level_up";
        $sqlUNION = $wpdb->prepare("
            SELECT p.post_parent, SUM(elu.standard * elu.important) AS total_standard, 
                  SUM(elu.manager_lv0 * elu.important) AS score
            FROM {$wpdb->posts} AS p 
            JOIN {$tableAssign} AS a ON a.post_id = p.ID AND a.year = %d
            JOIN {$tableEfficiencyLevelUp} AS elu ON (elu.assign_id = a.id) AND elu.assign_level_up IN ( 
                SELECT alu.id 
                FROM {$tableAssignLevelUp} AS alu 
                WHERE alu.user_id = %d 
            )
            WHERE p.post_status = 'publish' AND p.post_type = 'efficiency' AND p.post_parent != 0 
            GROUP BY p.post_parent 
        ", $year, $user_id );
        $sql = $wpdb->prepare("
            SELECT p.ID AS pID, p.post_title AS post_title, p.post_parent AS post_parent, 
                  p.post_code, a.orgchart_id, a.percent, pc.*  
            FROM {$wpdb->posts} AS p 
            JOIN ($sqlUNION) AS pc ON pc.post_parent = p.ID 
            JOIN {$tableAssign} AS a ON a.post_id = p.ID AND a.year = %d AND a.orgchart_id = %d  
            WHERE p.post_status = 'publish' AND p.post_type = 'efficiency' AND p.post_parent = 0
        ", $year, $orgchart_id );

        $_results[$__key] = $wpdb->get_results($sql);
    }
    return $_results[$__key];
}

/**
 * Comment efficiency
*/

function effa_site_comments_by_user( $user_id, $year, $status, $parent = 0, $type = 'assign_efficiency' ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableCommentEf = "{$prefix}comments_efficiency";
        $sql = $wpdb->prepare("
            SELECT *
            FROM {$tableCommentEf} AS cmt 
            WHERE cmt.user_id = %d AND cmt.comment_year = %d 
              AND cmt.comment_status = %s AND cmt.parent = %d AND cmt.comment_type = %s
        ", $user_id, $year, $status, $parent, $type);
        $_results[$__key] = $wpdb->get_row($sql);
    }
    return $_results[$__key];
}

/**
 * @param $data
 * @return array|int The comment ID on success. The value array on failure.
 */
function effa_site_comments_insert($data){
    global $wpdb;
    $errors = [];
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableCommentEf = "{$prefix}comments_efficiency";
    $wpdb->insert($tableCommentEf, $data);
    if( !empty($wpdb->last_error) ){
        $errors[] = $wpdb->last_error;
        return $errors;
    }
    return $wpdb->insert_id;
}
/**
 * @param $data
 * @return array|int 1 on success. The value array on failure.
 */
function effa_site_comments_update($data, $where){
    global $wpdb;
    $errors = [];
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableCommentEf = "{$prefix}comments_efficiency";
    $update = $wpdb->update($tableCommentEf, $data, $where);
    if( !empty($wpdb->last_error) ){
        $errors[] = $wpdb->last_error;
        return $errors;
    }
    return $update;
}

/*
 * Send mail report
 * */
function send_mail_report($user, $attachment){
    $settings = get_settings_website();
    $to_email = $user->user_email;
    $full_name = $user->display_name;
    $email_hr_manager = isset($settings['email_hr_manager']) && !empty($settings['email_hr_manager']) ? $settings['email_hr_manager'] : [];
    $subject = isset($settings['email_template_subject']) ? $settings['email_template_subject'] : __("BÁO CÁO", TPL_DOMAIN_LANG);
    $message = isset($settings['email_template_content']) ? $settings['email_template_content'] : "";
    $message = str_replace("[full_name]", $full_name, $message);
    $message .= sprintf("<p style='margin-top: 20px;'>Tập tin năng lực: <a href=\"%s\" style='text-decoration: none;padding: 10px 30px;background: #15c;color: #FFF;'>Tải về</a></p>", $attachment);
    $headers = [
        "Content-Type: text/html; charset=UTF-8",
        "From: {$to_email}",
    ];
    if( !empty($email_hr_manager) ){
        $emails = explode(",", $email_hr_manager);
        $headers_cc = array_map(function($item){
            return "CC: {$item}";
        },$emails);
    }
    $headers = array_merge($headers, $headers_cc);

    return wp_mail( $to_email, $subject, $message, $headers );

}