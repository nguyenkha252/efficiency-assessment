<?php
/**
 * This is class use for pagination
 *
 */

class Paging {
    protected $rowPerPage = 32;
    protected $currentPage = 1;
    protected $startRecord = 0;
    protected $totalPages;
    protected $resourceDBMS;
    protected $countField;
    protected $countTables;
    protected $pagePerSegment = 5;
    protected $adjacentSegment = 5;
    protected $pageUrl;
    public $queryString;
    protected $navBar = null;

    // Các thuộc tính dùng để cài đặt các tính chất của thanh trạng thái
    /**
     * Holds the text before navigation bar
     * @var navBarLabel
     **/
    public $navBarLabel = '';
    /**
     * Holds the text of button First
     * @var firstButtonText
     **/
    public $firstButtonText = '<i class="fa fa-angle-double-left" aria-hidden="true"></i>';
    /**
     * Holds the text of button End
     * @var endButtonText
     **/
    public $endButtonText = '<i class="fa fa-angle-double-right" aria-hidden="true"></i>';
    /**
     * Holds status to show or hide siblings of current segment
     * @var showSiblingsSegment
     **/
    public $showSiblingsSegment = false;
    /**
     * Holds the text before textbox
     * @var moveToPageText
     **/
    public $moveToPageText = 'Đến trang';
    /**
     * Holds the text after total pages number
     * @var totalPagesText
     **/
    public $totalPagesText = 'trang';

    // Các phương thức
    /**
     * Set number of data rows in one page
     * @param int rowPerPage
     **/
    protected function setRowPerPage($rowPerPage) {
        if (is_numeric($rowPerPage)) {
            $this->rowPerPage = (int)$rowPerPage;
        }
    }

    /**
     * Identify current page number
     */
    protected function setCurrentPage() {
        if (isset($_GET["pg"]) && is_numeric($_GET["pg"]) && $_GET["pg"] > 0) {
            $this->currentPage = (int)$_GET["pg"];
        }
    }

    /**
     * Retun current page number
     * @return int
     */
    public function getCurrentPage() {
        return $this->currentPage;
    }

    /**
     * Set startRecord param of LIMIT statement in SQL
     */
    protected function setStartRecord() {
        $this->startRecord = ($this->currentPage - 1) * $this->rowPerPage;
    }

    /**
     * Return startRecord param of LIMIT statement in SQL
     * @return int
     */
    public function getStartRecord() {
        return $this->startRecord;
    }

    /**
     * Return LIMIT Statement in SQL
     * @return string
     **/
    public function getLimitStatement() {
        return 'LIMIT ' .$this->startRecord. ', ' .$this->rowPerPage;
    }

    /**
     * Calculation total page when using pagination
     * @param resource resourceDBMS - MySQL Connection
     * @param string countField - Name of field use to count
     * @param string countTables - Name of tables use to show all data
     */
    protected function calcTotalPages($total) {
        $totalRecord = $total;
        $totalPages = ceil($totalRecord/$this->rowPerPage);
        $this->totalPages = (int)$totalPages;
    }

    /**
     * Return total page after using paging
     * @return int
     */
    public function getTotalPages() {
        return $this->totalPages;
    }

    /**
     * Identify number of pages each segement in paging navigation bar
     * @param int|null pagePerSegment
     **/
    protected function setPagePerSegment($pagePerSegment) {
        if (is_numeric($pagePerSegment)) {
            $this->pagePerSegment = (int)$pagePerSegment;
        }
    }

    /**
     * Identify back or next segment in paging navigation bar
     * @param int adjacentSegment
     **/
    public function setAdjacentSegment($adjacentSegment) {
        if (is_numeric($adjacentSegment)) {
            $this->adjacentSegment = (int)$adjacentSegment;
        }
    }

    /**
     * Set URL of link in paging navigation bar
     * @param string|null pageURL
     **/
    protected function setPageUrl($pageUrl) {
        if (empty($pageUrl)) {
            $this->pageUrl = '';
        } else {
            $this->pageUrl = $pageUrl;
        }
    }

    /**
     * Set query string at the end of URL of link in paging navigation bar
     * @param string|null queryString
     **/
    public function setQueryString($queryString) {
        if (!empty($queryString)) {
            $this->queryString = $queryString.'&';
        }
    }

    /**
     * Render navigation bar of pagination
     * @param int|null pagePerSegment
     * @param string|null pageURL
     * @param string|null queryString
     */
    public function renderNavBar($pagePerSegment=null, $pageUrl=null, $queryString=null) {
        $this->setPagePerSegment($pagePerSegment);
        $this->setPageUrl($pageUrl);
        $this->setQueryString($queryString);
        // Chỉ hiển thị thanh phân trang khi số trang lớn hơn 1 và trang hiện tại nhỏ hơn hoặc bằng trang cuối
        if ($this->totalPages > 1 && $this->currentPage <= $this->totalPages) {
            // Phân đoạn thanh phân trang
            if ($this->totalPages < $this->pagePerSegment) {
                $startSegment = 1;
                $endSegment = $this->totalPages;
            } else {
                // Số trang bên trái trang hiện tại
                if ($this->pagePerSegment%2 != 0) {
                    $leftCurrent = floor($this->pagePerSegment/2);
                } else {
                    $leftCurrent = ($this->pagePerSegment/2) - 1;
                }
                // Số trang bên phải trang hiện tại
                $rightCurrent = $this->pagePerSegment - $leftCurrent - 1;
                $startSegment = $this->currentPage - $leftCurrent;
                $endSegment = $this->currentPage + $rightCurrent;
                if ($startSegment < 1) {
                    $startSegment = 1;
                    // Tính lại trang cuối đoạn
                    $endSegment = $this->pagePerSegment;
                }
                if ($endSegment > $this->totalPages) {
                    $endSegment = $this->totalPages;
                    // Tính lại trang đầu đoạn
                    $startSegment = $this->totalPages - $this->pagePerSegment + 1;
                }
            }

            // Bắt đầu thanh phân trang //////////////////////////////////////////
            $this->navBar .= '<ul class="paginate">';
            /*if (!is_null($this->queryString)) {
                $queryStringSet = trim($this->queryString, '&');
                $queryStringSet = explode('&', $queryStringSet);
                foreach ($queryStringSet as $queryString) {
                    $queryStringPart = explode('=', $queryString);
                    if( count($queryStringPart) > 1 )
                        $this->navBar .= '<input type="hidden" name="' .$queryStringPart[0]. '" value="' .$queryStringPart[1]. '" />';
                }
            }*/
            $this->navBar .= $this->navBarLabel. '';
            //////////////////////////////////////////////////////////////////////

            // Nút trang đầu /////////////////////////////////////////////////////
            if ($startSegment > 1) {
                $this->navBar .= '<li class=""><a class="prev" data-paged="1" href="' .$this->pageUrl. '?' .$this->queryString. 'pg=1">' .$this->firstButtonText. '</a></li>';
            }
            //////////////////////////////////////////////////////////////////////

            // Những phân đoạn trước đoạn hiện tại ///////////////////////////////
            if ($this->showSiblingsSegment) {
                $backSegmentSet = null;
                $backSegment = $this->currentPage - $this->adjacentSegment;
                while ($backSegment > 1) {
                    if ($backSegment >= $startSegment - $rightCurrent) {
                        $backSegment -= 1;
                        continue;
                    }
                    $backSegmentSet = '<li class=""><a data-paged="' . $backSegment . '" href="' .$this->pageUrl. '?' .$this->queryString. 'pg=' .$backSegment. '">' .$backSegment. '</a></li>' .$backSegmentSet;
                    $backSegment -= $this->adjacentSegment;
                }
                if (!is_null($backSegmentSet) && $this->showAdjacent) {
                    $this->navBar .= $backSegmentSet;
                }
            }
            //////////////////////////////////////////////////////////////////////

            // Danh sách trang đoạn hiện tại /////////////////////////////////////
            for ($page=$startSegment; $page<=$endSegment; $page++) {
                if ($page == $this->currentPage) {
                    $this->navBar .= '<li class=""><span class="page current">' .$page. '</span></li>';
                } else {
                    $this->navBar .= '<li class=""><a data-paged="' . $page . '" class="page"href="' .$this->pageUrl. '?' .$this->queryString. 'pg=' .$page. '">' .$page. '</a></li>';
                }
            }
            //////////////////////////////////////////////////////////////////////

            // Những phân đoạn sau đoạn hiện tại /////////////////////////////////
            if ($this->showSiblingsSegment) {
                $nextSegmentSet = null;
                $nextSegment = $this->currentPage + $this->adjacentSegment;
                while ($nextSegment < $this->totalPages) {
                    if ($nextSegment <= $endSegment + $leftCurrent) {
                        $nextSegment += 1;
                        continue;
                    }
                    $nextSegmentSet .= '<li class=""><a class="page" data-paged="' . $nextSegment . '" href="' .$this->pageUrl. '?' .$this->queryString. 'pg=' .$nextSegment. '">' .$nextSegment. '</a></li>';
                    $nextSegment += $this->adjacentSegment;
                }
                if (!is_null($nextSegmentSet)) {
                    $this->navBar .= '<li class="">...</li>' .$nextSegmentSet;
                }
            }
            //////////////////////////////////////////////////////////////////////

            // Nút trang cuối ////////////////////////////////////////////////////
            if ($endSegment < $this->totalPages) {
                $this->navBar .= '<li class=""><a class="next" data-paged="' . $this->totalPages . '" href="' .$this->pageUrl. '?' .$this->queryString. 'pg=' .$this->totalPages. '">' .$this->endButtonText. '</a></li>';
            }

            // Show tổng số trang ////////////////////////////////////////////////
            //$this->navBar .= ' / '.$this->totalPages. ' ' .$this->totalPagesText;
            //////////////////////////////////////////////////////////////////////

            // Kết thúc thanh phân trang
            $this->navBar .= '</ul>';
            //////////////////////////////////////////////////////////////////////
        }
    }

    /**
     * Return navigation bar of pagination
     * @return string
     **/
    public function getNavBar() {
        return $this->navBar;
    }

    /**
     * Constructor
     * @param int rowPerPage - Number of data rows in one page
     * @param resource resourceDBMS - MySQL Connection
     * @param string countField - Name of field use to count
     * @param string countTables - Name of tables use to show all data
     **/
    public function __construct($rowPerPage, $total) {
        $this->setRowPerPage($rowPerPage);
        $this->setCurrentPage();
        $this->setStartRecord();
        $this->calcTotalPages($total);
    }

}
