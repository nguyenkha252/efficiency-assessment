<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/24/17
 * Time: 23:01
 */

$pages_data = [
    'profile' => [
        'post_title' =>         __('Profile', TPL_DOMAIN_LANG),
        'post_name' =>          PAGE_PROFILE_PATH,
        'post_content' =>       __('Profile', TPL_DOMAIN_LANG),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-profile.php"
    ],
    'calendar' => [
        'post_title' =>         __('Calendar', TPL_DOMAIN_LANG),
        'post_name' =>          PAGE_MY_CAKENDAR_PATH,
        'post_content' =>       __('Calendar', TPL_DOMAIN_LANG),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-my-calendar.php"
    ],
    'inboxs' => [
        'post_title' =>         __('Inboxs', TPL_DOMAIN_LANG),
        'post_name' =>          PAGE_MY_INBOXS_PATH,
        'post_content' =>       __('Inboxs', TPL_DOMAIN_LANG),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-my-inboxs.php"
    ],
    'todo' => [
        'post_title' =>         __('To Do', TPL_DOMAIN_LANG),
        'post_name' =>          PAGE_MY_TODO_PATH,
        'post_content' =>       __('To Do', TPL_DOMAIN_LANG),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-my-to-do.php"
    ],


    'dashboard' => [
        'post_title' =>         __('Dashboard', TPL_DOMAIN_LANG),
        'post_name' =>          PAGE_DASHBOARD,
        'post_content' =>       __('Dashboard', TPL_DOMAIN_LANG),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-dashboard.php"
    ],
    'organizational-structure' => [
        'post_title' =>         __('Organizational Structure', TPL_DOMAIN_LANG),
        'post_name' =>          PAGE_ORGANIZATIONAL_STRUCTURE,
        'post_content' =>       __('Organizational Structure', TPL_DOMAIN_LANG),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-organizational-structure.php"
    ],
    'manage-members' => [
        'post_title' =>         __('Manage Members', TPL_DOMAIN_LANG),
        'post_name' =>          PAGE_MANAGE_MEMBERS,
        'post_content' =>       __('Manage Members', TPL_DOMAIN_LANG),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-manage-members.php"
    ],
    'manage-targets' => [
        'post_title' =>         __('Manage Targets', TPL_DOMAIN_LANG),
        'post_name' =>          PAGE_MANAGE_TARGETS,
        'post_content' =>       __('Manage Targets', TPL_DOMAIN_LANG),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-manage-targets.php"
    ],
    'kpi-bank' => [
        'post_title' =>         __('KPI Bank', TPL_DOMAIN_LANG),
        'post_name' =>          PAGE_KPI_BANK,
        'post_content' =>       __('KPI Bank', TPL_DOMAIN_LANG),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-kpi-bank.php"
    ],
    'apply-bsc-kpi-system' => [
        'post_title' =>         __('Apply BSC-KPI System', TPL_DOMAIN_LANG),
        'post_name' =>          PAGE_APPLY_KPI_SYSTEM,
        'post_content' =>       __('Apply BSC-KPI System', TPL_DOMAIN_LANG),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-apply-kpi-system.php"
    ],
    'statistical' => [
        'post_title' =>         __('Statistical', TPL_DOMAIN_LANG),
        'post_name' =>          PAGE_STATISTICAL,
        'post_content' =>       __('Statistical', TPL_DOMAIN_LANG),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-statistical.php"
    ],
    'news' => [
        'post_title' =>         __('News', TPL_DOMAIN_LANG),
        'post_name' =>          PAGE_NEWS,
        'post_content' =>       __('News', TPL_DOMAIN_LANG),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-news.php"
    ],
    'settings' => [
        'post_title' =>         __('Settings', TPL_DOMAIN_LANG),
        'post_name' =>          PAGE_SETTINGS,
        'post_content' =>       __('Settings', TPL_DOMAIN_LANG),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-settings.php"
    ],
    'exports' => [
        'post_title' =>         __('Exports', TPL_DOMAIN_LANG),
        'post_name' =>          PAGE_EXPORTS,
        'post_content' =>       __('Exports', TPL_DOMAIN_LANG),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-exports.php"
    ],
	'user-managers' => [
		'post_title' =>         __('User Managers', TPL_DOMAIN_LANG),
		'post_name' =>          PAGE_USER_MANAGERS,
		'post_content' =>       __('', TPL_DOMAIN_LANG),
		'post_type' =>          'page',
		'post_status' =>        'publish',
		'page_template' =>      "page-user-managers.php"
	],
    'efficiency-assessment' => [
        'post_title' =>         __('Efficiency Assessment', TPL_DOMAIN_LANG),
        'post_name' =>          PAGE_EFFA_SYSTEM,
        'post_content' =>       __('Efficiency Assessment', TPL_DOMAIN_LANG),
        'post_type' =>          'page',
        'post_status' =>        'publish',
        'page_template' =>      "page-efficiency-assessment.php"
    ],
];
# exit(__LINE__ . ' ' . __FILE__);

$lists = [];
foreach($pages_data as $type => $attr) {
    $page = get_page_by_path($attr['post_name'], OBJECT, 'page');
    $lists[$attr['post_name']] = $page;
    if( $page === null ) {
        $attr['ID'] = wp_insert_post($attr);

    } else if( $page instanceof WP_Post ) {
        $attr['ID'] = $page->ID;
        wp_update_post($attr);
    }
    if( $type == 'apply-bsc-kpi-system' && is_numeric($attr['ID']) ) {
        update_option('show_on_front', 'page');
        update_option('page_on_front', $attr['ID']);
    }
}




/*
$query = new WP_Query(['fields' => '*', 'posts_per_page' => 100, 'post_status' => 'publish', 'post_type' => 'page', ]);
echo '<pre>';
var_dump($lists, $query->posts);
echo '</pre>';
exit(__LINE__ . ' ' . __FILE__);
*/