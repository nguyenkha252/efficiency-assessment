<?php
/**
 * Created by PhpStorm.
 * User: BCM_dev
 * Date: 10/10/18
 * Time: 11:22 AM
 */
function get_questions() {
    $current_user = wp_get_current_user();

    return get_questions_by_orgchart_id($current_user->orgchart_id);
}

function get_uquestions() {
    $user_id = get_current_user_id();

    global $wpdb;

    $query = "SELECT * FROM {$wpdb->prefix}assign_level_up a
        WHERE a.user_id = {$user_id} AND a.status LIKE 'publish'";

    $assign_level_up = $wpdb->get_row($query);

    if ( ! empty ($assign_level_up) ) {
       return get_questions_by_orgchart_id($assign_level_up->orgchart_id_new);
    }

    return null;
}

function get_questions_by_orgchart_id($orgchart_id) {
    global $wpdb;

    $query = "SELECT p1.* FROM {$wpdb->prefix}posts p1
                    JOIN (SELECT p2.post_code FROM {$wpdb->prefix}assign_orgcharts a
                    JOIN {$wpdb->prefix}posts p2
                    ON p2.ID = a.post_id
                    WHERE a.orgchart_id = {$orgchart_id}) b
                    ON p1.post_code = b.post_code
                    WHERE p1.post_type = 'questions'";

    return $wpdb->get_results($query);
}

function render_uquiz() {
    if (isset($_POST['nonce'])) {
        finish_quiz('up_efficiency');
    }
    $questions = get_uquestions();

    return render($questions, 'uquiz');
}

function render_quiz() {
    if (isset($_POST['nonce'])) {
        finish_quiz('efficiency');
    }

    $questions = get_questions();

    return render($questions);
}

function render($questions, $type = "quiz") {
    if( !empty($questions) ) {
        $html = '<div class="quiz">';
        $html .= '<h2>' . __('Bài kiểm tra năng lực của bạn', TPL_DOMAIN_LANG);
        $html .= '</h2>';
        $html .= '<form class="frm-quiz" method="post" action="' . admin_url("admin-ajax.php") . '">';


        shuffle($questions);

        $list_codes = array();

        $stt = 1;

        foreach ($questions as $k => $question) {
            if (in_array($question->post_code, $list_codes)) {
                continue;
            }
            $list_codes[] = $question->post_code;
            $html .= render_question($question, $stt);
            $stt++;
        }

        $html .= '<input type="hidden" name="action" value="finish_' . $type . '">';

        $nonce = wp_create_nonce('finish_' . $type . '');

        $html .= '<div class="response-container"></div>';
        $html .= '<input type="hidden" name="nonce" value="' . $nonce . '">';
        $html .= '<input type="submit" id="send-answers" class="btn btn-primary" value="' . __('Hoàn thành', TPL_DOMAIN_LANG) . '">';
        $html .= '</form>';
        $html .= '</div>';
    }else{
        $html = '<div class="quiz">';
        $html .= '<h2>' . __('Không có bài kiểm tra', TPL_DOMAIN_LANG);
        $html .= '</h2>';
        $html .= '</div>';
    }

    return $html;
}

function render_question($question, $stt) {
    $class_in = $stt == 1 ? "in" : "";
    $aria_expanded = $stt == 1 ? "true" : "false";
    $question_html = '<div class="question">';
    $question_html .= '<h3 class="question-title"><span data-toggle="collapse" href="#answer-' . $question->ID . '" role="button" aria-expanded="' . $aria_expanded .'" aria-controls="answer-' . $question->ID . '">' . $stt . '. ' . $question->post_title . '</span></h3>';

    $option_ids = range(1, 5);
    shuffle($option_ids);
    $answer_html = "";
    foreach ($option_ids as $option_id) {
        $option_content = get_post_meta($question->ID, '_effa_anwser_' . $option_id, true);
        $answer_html .= '<div class="option form-group"><label class="switch article-quiz">';
        $answer_html .= '<input type="radio" class="form-control-checkbox checkbox-status form-check-input" name="question_' . $question->post_code . '" value="' . $option_id . '">';
        $answer_html .= '<span class="checkbox-slider round fa fas"></span><span>'.$option_content.'</span></label></div>';
    }
    if( !empty($answer_html) ){
        $question_html .= "<div class=\"collapse {$class_in}\" id=\"answer-{$question->ID}\">{$answer_html}</div>";
    }

    $question_html .= '</div>';

    return $question_html;
}

function finish_quiz($efficiency) {
    $nonce = $_POST['nonce'];
    $year = date('Y', time());
    if ( ! wp_verify_nonce( $nonce, 'finish_quiz') ) {
        return false;
    }

    if ( ! is_user_logged_in() ) {
        return false;
    }

    $user = wp_get_current_user();
    $answers = array();

    foreach ($_POST as $key => $param) {
        if ( preg_match('/question_(\w+)/', $key, $matches) ) {
            $answers[$matches[1]] = $param;
        }
    }

    update_user_meta($user->ID, "{$efficiency}_{$year}", $answers);

    return true;
}

function get_assign_orgcharts($user) {
    global $wpdb;

    $query = "SELECT * FROM {$wpdb->prefix}assign_orgcharts a
        LEFT JOIN {$wpdb->prefix}efficiency_assessment e
        ON e.assign_id = a.id AND e.user_id = 0
        JOIN {$wpdb->prefix}posts p
        ON (p.ID = a.post_id AND p.post_parent != 0 AND p.post_parent IS NOT NULL)
        WHERE a.orgchart_id = {$user->orgchart_id}";

    return $wpdb->get_results($query, ARRAY_A);
}

function get_result_of_user($user) {
    global $wpdb;

    $query = "SELECT * FROM {$wpdb->prefix}assign_orgcharts a
        LEFT JOIN {$wpdb->prefix}efficiency_assessment e
        ON e.assign_id = a.id AND e.user_id = {$user->ID}
        JOIN {$wpdb->prefix}posts p
        ON (p.ID = a.post_id AND p.post_parent != 0 AND p.post_parent IS NOT NULL)
        WHERE a.orgchart_id = {$user->orgchart_id}";

    return $wpdb->get_results($query, ARRAY_A);
}

function filter_points_by_field($assign_orgcharts, $name) {
    $values = array_column($assign_orgcharts, $name);

    return array_map(function($val){
        return !empty($val) ? $val : 0;
    }, $values);
}