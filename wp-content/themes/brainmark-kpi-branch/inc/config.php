<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/5/18
 * Time: 09:29
 */

/**
 * @param string $key
 * @return array|bool|mixed
 */
function getMeasurementFormulas( $key = '' ){
	$measurementFormulas = [
		'<'     => __('KH < KQ', TPL_DOMAIN_LANG),
		'>'     => __('KH > KQ', TPL_DOMAIN_LANG),
		'<='    => __('KH <= KQ', TPL_DOMAIN_LANG),
		'=='     => __('KH = KQ', TPL_DOMAIN_LANG),
		'>='    => __('KH >= KQ', TPL_DOMAIN_LANG),
	];
	if( !empty( $key ) ){
	    if( array_key_exists( $key, $measurementFormulas ) ){
            return $measurementFormulas[$key];
        }else{
	        return false;
        }
    }
	return $measurementFormulas;
}

function getConditions( $key = '' ){
    $conditions = [
        '0'  => '',
        '<' => '<',
        '>' => '>',
        '=' => '=',
        '<=' => '<=',
        '>=' => '>=',
    ];
    if( !empty( $key ) || $key == 0 ){
        if( array_key_exists( $key, $conditions ) ){
            return $conditions[$key];
        }else{
            return false;
        }
    }
    return $conditions;
}

function getUnit( $key = '' ){
    $units = [
        'ty'     => __('Tỷ', TPL_DOMAIN_LANG),
        'so_lan'     => __('Số lần', TPL_DOMAIN_LANG),
        'thoi_gian'    => __('Thời gian (DD/MM/YYYY)', TPL_DOMAIN_LANG),
        'ty_le'     => __('Tỷ lệ', TPL_DOMAIN_LANG),
        'so_tien'    => __('Số tiền', TPL_DOMAIN_LANG),
        'so_vu'    => __('Số vụ', TPL_DOMAIN_LANG),
        '%'    => __('%', TPL_DOMAIN_LANG),
        'm2'    => __('m<sub>2</sub>', TPL_DOMAIN_LANG),
        'giay'    => __('Giấy', TPL_DOMAIN_LANG),
        'khoa'    => __('Khóa', TPL_DOMAIN_LANG),
	    'khach_hang' => __('Khách hàng', TPL_DOMAIN_LANG),
	    'nhan_vien' => __('Nhân viên', TPL_DOMAIN_LANG),
	    'chuong_trinh' => __('Chương trình', TPL_DOMAIN_LANG),
	    'sang_kien' => __('Sáng kiến', TPL_DOMAIN_LANG),
	    'nguoi' => __('Người', TPL_DOMAIN_LANG),
    ];
    if( !empty( $key ) ){
        if( array_key_exists( $key, $units ) ){
            return $units[$key];
        }else{
            return false;
        }
    }
    return $units;
}

function getTypeFormulas( $key = '' ){
    $definedTypeFormula = [
        'on_off' => ['name' => __('On/Off', TPL_DOMAIN_LANG) ],
        'oscillate' => ['name' => __('Dao động', TPL_DOMAIN_LANG) ],
        'add_max_kq' => ['name' => __('Thêm max KQ KPI', TPL_DOMAIN_LANG) ],
        'add_between' => [
            'name' => __('Thêm trong khoảng', TPL_DOMAIN_LANG),
        ],
    ];
    if( !empty( $key ) ){
        if( array_key_exists( $key, $definedTypeFormula ) ){
            return $definedTypeFormula[$key];
        }else{
            return false;
        }
    }
    return $definedTypeFormula;
}

function getUnitFormulas( $key = '' ){
    $unitFormulas = [
        'ty' => __('Tỷ', TPL_DOMAIN_LANG),
        'so_lan' => __('Số ', TPL_DOMAIN_LANG),
        'thoi_gian' => __('Thời gian', TPL_DOMAIN_LANG),
        'ty_le' => __('Tỷ lệ', TPL_DOMAIN_LANG),
    ];
    if( !empty( $key ) ){
        if( array_key_exists( $key, $unitFormulas ) ){
            return $unitFormulas[$key];
        }else{
            return false;
        }
    }
    return $unitFormulas;
}

function getColor4Column(){
    $color = [
        'finance' => '#008fd5',
        'customer' => '#ee3f3f',
        'operate' => '#7fc45d',
        'development' => '#fea055',
    ];
    return $color;
}

function getColorColumnNode( $key ){
    $colors = [
        '#008fd5',
        '#ee3f3f',
        '#7fc45d',
        '#fea055',
        '#c3a614',
    ];
    if( isset( $key ) ){
        if( array_key_exists( $key, $colors ) ){
            return $colors[$key];
        }else{
            return false;
        }
    }
    return $colors;
}

/**
 * get background color of node this
 * @param $create_by_node
 * @return string
 */
function getColorStar( $create_by_node ){
    if( $create_by_node == KPI_CREATE_BY_NODE_START ){
        $classNode = "color-ceo";
    }elseif( $create_by_node == KPI_CREATE_BY_NODE_MIDDLE ){
        $classNode = "color-finance";
    }else{
        $classNode = "";
    }
    return $classNode;
}

function createDataChartNode( $data ){
    $labelsArr = [];
    $dataArr = [];
    $backgroundColorArr = [];
    if( !empty( $data ) ){
        foreach( $data as $key => $item ){
            array_push( $labelsArr, $item['post_title'] );
            array_push( $dataArr, round($item['percent'], 1) );
            array_push( $backgroundColorArr, getColorColumnNode( $key ) );
        }
    }
    $dataCharts = ['labels' => $labelsArr, 'datasets' => [['data' => $dataArr, 'backgroundColor' => $backgroundColorArr ] ] ];
    return $dataCharts;
}