<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/9/18
 * Time: 10:42
 */

/**
 * @param $term
 * @param int $lv
 * @return int
 */
$pagination_file = THEME_DIR . '/inc/paging.php';
if( file_exists($pagination_file) )
    require_once $pagination_file;

function getLevelCategories( $term, $lv = -1 ){
    if( !($term instanceof WP_Term) || empty( $term ) || is_wp_error( $term ) ){
        return $lv;
    }
    $parent = $term ->parent;
    $cat = get_term_by( 'id', $parent, 'category' );
    if( !is_wp_error($cat) && is_object( $cat ) ){
        $level = $lv + 1;
        $lv = getLevelCategories( $cat, $level );
    }
    return $lv;
}
function getParentTerm( $currentTerm, $level, $taxonomy = 'category' ){
    if( $level > 0  && !empty($currentTerm) && !is_wp_error( $currentTerm )){
        $level --;
        $tempTerm = get_term_by( 'id', $currentTerm->parent, $taxonomy );
        $currentTerm = getParentTerm( $tempTerm, $level, $taxonomy );
    }
    return $currentTerm;
}

/**
 * @return array|false|WP_Term
 */
function getRootKPICat() {
    static $term_kpi;
    if( !isset($term_kpi) ) {
        $term_kpi = get_term_by('slug', __('kpi', TPL_DOMAIN_LANG), 'category');
    }
    return $term_kpi;
}


/**
 * @return array|int|WP_Error
 */
function kpiListCategories() {
    static $kpiByCategories;
    if( !isset($kpiByCategories) ) {
        $term_kpi = getRootKPICat();
        $kpiByCategories = get_terms([
            'taxonomy' => 'category',
            'hide_empty' => false,
            'orderby' => 'term_id',
            'order' => 'ASC',
            'child_of' => (int)$term_kpi->term_id
        ]);
    }

    return $kpiByCategories;
}

/**
 * @return array|false|WP_Term
 */
function getRootKPICapacity( $slug ) {
    static $term_kpiCapacity;
    if( !isset($term_kpiCapacity) ) {
        $term_kpiCapacity = get_term_by('slug', $slug, 'category');
    }
    return $term_kpiCapacity;
}

/**
 * @return array
 */
function kpiTreeCategories() {
    static $results;
    if( !isset($results) ) {
        $cats = kpiListCategories();
        $catRoot = getRootKPICat();
        $items = [];
        $results = [];
        foreach($cats as &$cat) {
            $cat->children = [];
            $items[$cat->term_id] = &$cat;

        }
        foreach($items as $id => &$cat) {
            if( $cat->parent == $catRoot->term_id ) {
                $results[$cat->term_id] = &$items[$id];
            } else {
                if( isset($items[$cat->parent]) ) {
                    $items[$cat->parent]->children[$cat->term_id] = &$items[$id];
                }
            }
        }
    }
    return $results;
}

/**
 * @param $value
 * @param $items
 * @param string $level
 * @param string $space
 * @return string
 */
function loop_categories_level($value, &$items, $level = '', $space = '    '){
    $html = '';
    if( count($items) > 0 ) {
        foreach($items as $id => &$cat) {
            $selected = '';
            if( ($value == $cat->term_id) || ($value == $cat->name) || ($value == $cat->slug) ) {
                $selected = ' selected="selected"';
            }
            $html .= sprintf('<option value="%s" %s>%s %s</option>', $cat->term_id, $selected, $level, $cat->name);
            $html .= loop_categories_level($value, $items[$id]->children, $level . $space, $space);
        }
    }

    return $html;
}

/**
 * @param string $value
 * @param bool $echo
 * @param string $space
 * @return string
 */
function kpiRenderCategoryDropdown($value = '', $echo = true, $space = '    ') {
    $tree = kpiTreeCategories();
    $selected = ($value == '') ? ' selected="selected"' : '';
    $html = sprintf('<option value="" %s>%s%s</option>', $selected, '', __('Select Category KPI', TPL_DOMAIN_LANG) );
    $html .= loop_categories_level($value, $tree, '', $space);
    if( $echo ) {
        echo $html;
    }
    return $html;
}

/**
 * @param $argsCate
 * @param int $parent
 * @return string
 */
function renderCategoryDropdown( $argsCate, $parent = 0 ){
    $cat = [];
    $html ='';
    foreach ( $argsCate as $key => $value ){
        if( $value->parent == $parent ){
            $cat[] = $value;
            unset( $argsCate[$key] );
        }
    }
    if( $cat ){
        foreach( $cat as $k => $item ){
            $outputChild = '';
            if( $argsCate ){
                $outputChild = renderCategoryDropdown($argsCate, $item->term_id);
                if( $outputChild !== '' ){
                    $html .= "<optgroup label=\"" . $item->name . "\"> " . $outputChild . " </optgroup>";
                }
            }
            if( $outputChild === '' ) {
                $html .= "<option value='" . $item->term_id . "'>" . $item->name . "</option>";
            }

        }
    }
    return $html;
}

function renderPostDropdown( $argsPost, $parent ){
    $pst = [];
    $html ='';
    echo $parent;
    foreach ( $argsPost as $key => $value ){
        if( $value->post_parent == $parent ){
            $pst[] = $value;
            unset( $argsPost[$key] );
        }
    }
    if( $pst ){
        foreach( $pst as $k => $item ){
            $outputChild = '';
            if( $argsPost ){
                $outputChild = renderPostDropdown($argsPost, $item->ID);
                if( $outputChild !== '' ){
                    $html .= "<optgroup label=\"" . $item->post_title . "\"> " . $outputChild . " </optgroup>";
                }
            }
            if( $outputChild === '' ) {
                $html .= "<option value='" . $item->ID . "'>" . $item->post_title . "</option>";
            }

        }
    }
    return $html;
}

/**
 * @param $cat
 * @param array $status
 * @return WP_Query
 */
function getKpisByCategory($cat, $status = ['draft', 'publish'], $args = []) {

    $args_post = [
        # 'category_name' => $cat,
	    'posts_per_page' => '-1',
        'order' => 'ASC',
        'orderby' => 'ID',
        'post_type' => 'post',
        'post_status' => $status
    ];
    $child_cats = (array) get_term_children( (int)$cat, 'category' );
    $args_post['category__not_in'] = $child_cats;
    # $args_post['cat'] = $cat;
    $args_post['category__and'] = $cat;
    $args = wp_parse_args($args, $args_post);
    $kpiQuery = new WP_Query();
    $kpiQuery->query($args);
    return $kpiQuery;
}

function kpi_get_kpi_by_id($id) {
    global $wpdb;
    static $_results = [];
    $__key = $id;
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $tableChart = "{$prefix}org_charts";
        $_results[$__key] = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$tableKpi} as k WHERE id = %d", $id), ARRAY_A);
    }
    return $_results[$__key];
}

function kpi_get_kpi_by_user($uid, $parentId) {
    global $wpdb;
    static $_results = [];
    $__key = "{$uid}_{$parentId}";
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $tableChart = "{$prefix}org_charts";
        $_results[$__key] = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$tableKpi} as k ".
            # (k.`bank_id` = %d) AND $bankId,
            "WHERE (k.`user_id` = %d) AND (k.`parent` = %d)", $uid, $parentId), ARRAY_A);
    }
    return $_results[$__key];
}

function get_kpi_and_post_by_kpi_id( $id ) {
    global $wpdb;
    static $_results = [];
    $__key = $id;
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $_results[$__key] = $wpdb->get_row($wpdb->prepare("
            SELECT k.*, p.post_title, k2.plan AS plan_on_level, y.year
            FROM {$tableKpi} as k 
            LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent
            INNER JOIN {$tableKpiYear} AS y ON y.id = k.year_id 
            INNER JOIN {$wpdb->posts} AS p ON k.bank_id = p.ID 
            WHERE k.id = %d", $id), ARRAY_A);
    }
    return $_results[$__key];
}

/***
 * @param $type
 * @param $time_type
 * @param $time_value
 * @param int $user_id
 * @param int $orgchart_id
 * @param string $approved ('all' || 'on' || 'off')
 * @return mixed
 */
function kpi_get_list($type, $time_type, $time_value, $user_id, $orgchart_id = 0, $approved = 'all', $chart_parent_ids = [], $status = '', $owner = '') {
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize([$type, $time_type, $time_value, $user_id, $orgchart_id, $approved, $chart_parent_ids, $status]));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $tableChart = "{$prefix}org_charts";
	    $tableKpiFormulas = "{$prefix}kpi_formulas";

        $time_type = strtolower($time_type);
        $whereTime = '';
        if (!in_array($time_type, ['year', 'precious', 'month'])) {
            $time_type = 'year';
            $whereTime = $wpdb->prepare("AND (y.`{$time_type}` = %d)", (int)$time_value);
        } else if( in_array($time_type, ['precious', 'month']) ) {
            list($year, $time_type) = (array)$time_value;
            $whereTime = $wpdb->prepare("AND (y.`year` = %d) AND (y.`{$time_type}` = %d)", (int)$year, (int)$time_type);
        }else{
	        $time_type = 'year';
	        $whereTime = $wpdb->prepare("AND (y.`{$time_type}` = %d)", (int)$time_value);
        }
        $whereApproved = '';
        $whereStatus = '';
        if( !empty( $status ) ){
        	$whereStatus = $wpdb->prepare(" AND k.status LIKE %s ", $status);
        }
        if( !empty( $owner ) ){
        	$whereStatus .= $wpdb->prepare(" AND k.owner = %s ", $owner);
        }

        $where = '';
        if( !empty($chart_parent_ids) ) {
            $chart_parent_ids = array_map('intval', $chart_parent_ids);
            if( !empty($chart_parent_ids) && ($orgchart_id > 0) && ($user_id > 0) ) {
                $chart_parent_ids = implode(', ', $chart_parent_ids);
                $where .= $wpdb->prepare(" AND ( ( (k.`chart_id` = %d) AND (k.`user_id` = %d) ) OR (k.`chart_id` IN ($chart_parent_ids) ) )", $orgchart_id, $user_id);
            } else {
                if( $orgchart_id > 0 ) {
                    $where .= $wpdb->prepare(" AND (c.`id` = %d)", $orgchart_id);
                }
                if( $user_id > 0 ) {
                    $where .= $wpdb->prepare(' AND (k.`user_id` = %d) ', $user_id);
                }
            }
        } else {
            if( $orgchart_id > 0 ) {
                $where .= $wpdb->prepare(" AND (c.`id` = %d)", $orgchart_id);
            }
            if( $user_id > 0 ) {
                $where .= $wpdb->prepare(' AND (k.`user_id` = %d) ', $user_id);
            }
        }

        $sql = $wpdb->prepare("SELECT k.*, c.name, c.name as chuc_danh, y.`year`, y.`precious`, y.`month`, p.formula_id, p.post_title, p.post_parent as bank_parent, fm.title AS formula_title, fm.type AS formula_type, fm.formulas, fm.note AS formula_note ".
            "FROM {$tableKpi} as k " .
            "INNER JOIN {$tableKpiYear} as y ON (k.`year_id` = y.`id`) " .
            "INNER JOIN {$wpdb->posts} as p ON (p.`ID` = k.`bank_id`) " .
            "LEFT JOIN {$tableKpiFormulas} as fm ON (p.`formula_id` = fm.`id`) " .
            "INNER JOIN {$tableChart} as c ON (k.`chart_id` = c.`id`) " .
            "WHERE (1 = 1) {$where} AND (k.`type` LIKE %s) {$whereTime} {$whereApproved} 
            {$whereStatus} GROUP BY k.`bank_id` ORDER BY k.`required` ASC, k.id ASC",
            $type);

        # echo "<pre>"; var_dump(!empty($chart_parent_ids), ($orgchart_id > 0), ($user_id > 0), $type, $time_type, $time_value, $user_id, $orgchart_id, $approved, $chart_parent_ids); echo "\n\n{$sql}</pre>"; # exit();
        $results = $wpdb->get_results($sql, ARRAY_A);
        $_results[$__key] = [];
        if( !empty($results) ) {
            foreach($results as $id => $item) {
                $item['bank_parent']    = (int)$item['bank_parent'];
                $item['year']           = (int)$item['year'];
                $item['formula_id']     = (int)$item['formula_id'];
                $item['bank_id']        = (int)$item['bank_id'];
                $item['year_id']        = (int)$item['year_id'];
                $item['id']             = (int)$item['id'];
                $_results[$__key][$item['id']] = $item;
            }
        }
    }
    return $_results[$__key];
}

function kpi_get_list_if_not_and_get_from_parent($type, $time_type, $time_value, $user_id, $orgchart_id = 0, $approved = 'all', $chart_parent_ids = [], $status = '', $owner = '') {
    $kpis = kpi_get_list($type, $time_type, $time_value, $user_id, $orgchart_id, $approved, $chart_parent_ids, $status, $owner);
    /* if( false && $orgchart_id > 0 ) {
        $parentChart = kpi_orgchart_get_chart_parent_of($orgchart_id);
        if( !empty($parentChart) ) {
            $parentKpis = kpi_get_list($type, $time_type, $time_value, $user_id, $parentChart['id'], $approved, $chart_parent_ids, $status);
            if( empty($parentKpis) ) {
                $parentKpis = kpi_get_list_if_not_and_get_from_parent($type, $time_type, $time_value, $user_id, $parent
    Chart['id'], $approved, $chart_parent_ids, $status);
            }
            if( !empty($parentKpis) ) {
                if( !empty($kpis) ) {
                    foreach ($kpis as $id => $kpi) {
                        if( isset($parentKpis[$kpi['id']]) ) {
                            unset($parentKpis[$kpi['id']]);
                        }
                    }
                }
            }
            $kpis = array_merge($kpis, $parentKpis);
        }
    } */
    return $kpis;
}

/**
 * @param $type
 * @param $time_type
 * @param $time_value
 * @param $user_id
 * @param $orgchart_id
 * @param string $approved
 */
function kpi_get_list_of_charts($type, $time_type, $time_value, $user_id, $orgchart_id, $bank_id = 0, $parent = 0) {
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $tableChart = "{$prefix}org_charts";

        $time_type = strtolower($time_type);
        if (!in_array($time_type, ['year', 'precious', 'month'])) {
            $time_type = 'year';
        }
        $whereParent = '';
        if ($parent !== 0) {
            $whereParent = $wpdb->prepare('AND (k.`parent` = %d) ', $parent);
        }
        $whereOrgchartName = '';
        if( is_numeric($orgchart_id) ) {
            $orgchart_id = intval($orgchart_id);
            if ($orgchart_id > 0) {
                $whereOrgchartName = $wpdb->prepare(" AND (c.`parent` = %d)", $orgchart_id);
            }
        } else if( is_array($orgchart_id) ) {
            $orgchart_id = array_map('intval', $orgchart_id);
            $whereOrgchartName = " AND (c.`id` IN (".implode(', ', $orgchart_id).") )";
        }


        $whereUser = '';
        if ($user_id > 0) {
            $whereUser = $wpdb->prepare(" AND (k.`user_id` = %d) ", $user_id);
        }
        $whereBank = '';
        if ($bank_id > 0) {
            $whereBank = $wpdb->prepare(" AND (k.`bank_id` = %d) ", $bank_id);
        }
        $whereTime = '';
        if( $time_type == '') {
            if( !empty($time_value) ) {
                $whereTime = $wpdb->prepare(" AND (y.id = %d)", $time_value);
            }
        } else if( !empty($time_value) ) {
            $whereTime = $wpdb->prepare("AND (y.`{$time_type}` = %d)", $time_value);
        }
        $sql = $wpdb->prepare("SELECT k.*, t.term_id as bank_cat, t.name as group_name, t.slug as group_slug, c.name, c.name as chuc_danh, ".
            "y.`year`, y.`precious`, y.`month`, p.post_title as title, p.post_parent as `parent` " .
            "FROM {$tableKpi} as k " .
            "INNER JOIN {$tableKpiYear} as y ON (k.year_id = y.id) " .
            "INNER JOIN {$wpdb->posts} as p ON (p.ID = k.bank_id) {$whereBank} " .
            "INNER JOIN {$wpdb->term_relationships} as tr ON (p.ID = tr.object_id) " .
            "INNER JOIN {$wpdb->term_taxonomy} as tt ON (tt.term_taxonomy_id = tr.term_taxonomy_id) AND (tt.taxonomy = 'category') " .
            "INNER JOIN {$wpdb->terms} as t ON (t.term_id = tt.term_id) " .
            "INNER JOIN {$tableChart} as c ON (k.chart_id = c.id) {$whereOrgchartName} " .
            "WHERE (1 = 1) {$whereUser} AND (k.`type` LIKE %s) {$whereTime} {$whereParent} ".
            # "ORDER BY c.name " .
            # "GROUP BY k.chart_id"
            "",
            $type);

        $results = $wpdb->get_results($sql, ARRAY_A);
        $_results[$__key] = $results;
        # $line = __LINE__ . ' ' . __FILE__;
        # echo "<pre class=\"role-dang-ky-kpi\" style=\"display: block !important;\">{$line}\n{$wpdb->last_query}\n{$wpdb->last_error}</pre>";
    }
    return $_results[$__key];
}

function kpi_get_list_for_room($type, $time_type, $time_value, $department_id = 0) {
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $tableChart = "{$prefix}org_charts";

        $time_type = strtolower($time_type);
        if (!in_array($time_type, ['year', 'precious', 'month'])) {
            $time_type = 'year';
        }
	    $yearParent = $wpdb->prepare("SELECT y2.id FROM {$tableKpi} as k " .
	                                 "INNER JOIN {$tableKpiYear} as y2 ON k.year_id = y2.id " .
	                                 "WHERE k.`parent` = 0 AND k.`type` LIKE %s AND y2.`{$time_type}` = %d AND y2.`parent` = 0
            ", $type, $time_value);
        $yearChild = $wpdb->prepare("
            SELECT y.id FROM {$tableKpiYear} as y 
            WHERE y.parent IN ({$yearParent})
            ", $type, $time_value);
        $sql = $wpdb->prepare("
        SELECT k.*, c.name, y.`year`, y.`precious`, y.`month`, p.post_title FROM {$tableKpi} AS k
        INNER JOIN {$tableChart} AS c ON c.id = k.chart_id
        INNER JOIN {$tableKpiYear} AS y ON y.id = k.year_id
        INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
        WHERE ( k.year_id IN ({$yearChild}) ) AND k.chart_id = %d AND k.type = %s
        ", $department_id, $type);
        $_results[$__key] = $wpdb->get_results($sql, ARRAY_A);
    }
    #echo $sql.'<br>';
    return $_results[$__key];
}

function get_kpi_by_month_year_orgchart($time_value, $month, $department_id){
	global $wpdb;
	static $_results = [];
	$__key = md5(serialize(func_get_args()));
	if( !isset( $_results[$__key] ) ){
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpi = "{$prefix}kpis";
		$tableKpiYear = "{$prefix}kpi_years";
		#$tableChart = "{$prefix}org_charts";
		$sql = $wpdb->prepare("SELECT k.*, p.post_title 
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          WHERE k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %s AND y.month = %d ) 
              AND k.chart_id = %d AND ( k.company_plan = 0 OR k.company_plan IS NULL) AND ( k.department_plan = 0 OR k.department_plan IS NULL )"
			, $time_value, $month, $department_id);
		$_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
	}
	return $_results[$__key];
}

function get_kpi_by_year_orgchart($time_value, $userID){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        #$tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("SELECT k.*, p.post_title 
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          WHERE k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %s ) 
              AND k.user_id = %d "
            , $time_value, $userID);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function kpi_get_total_to_month( $year_id, $month, $userID ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $arrMonth = '';
        for ($i = (int)$month; $i > 0; $i--) {
            $arrMonth[] = $i;
        }
        $strMonth = implode(',', $arrMonth);
        $sql = $wpdb->prepare(" 
                      SELECT SUM(k.actual_gain) AS total_plan
                      FROM {$tableKpi} AS k
                      WHERE k.year_id IN (
                          SELECT y.id 
                          FROM {$tableKpiYear} AS y 
                          WHERE y.month IN ({$strMonth}) AND y.parent IN (
                            SELECT y2.parent 
                            FROM {$tableKpiYear} AS y2 
                            WHERE y2.id = %d )
                          AND k.user_id = %d 
                          ) "
                    , $year_id, $userID);
        $_results[$__key] = $wpdb->get_row( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function kpi_get_total_by_cty(){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";

    }
    return $_results[$__key];
}

function get_kpi_by_year_orgchart_for_month($time_value, $month, $userID){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $tableKpiFormulas = "{$prefix}kpi_formulas";

        #$tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("SELECT k.*, k2.plan AS plan_for_year, p.post_title, 
          fm.type AS formula_type, fm.formulas 
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id
          LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent
          WHERE k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %d AND y.month = %d ) 
              AND k.user_id = %d "
            , $time_value, $month, $userID);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}
function get_kpi_by_year_orgchart_for_month_and_status($time_value, $month, $userID){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $tableKpiFormulas = "{$prefix}kpi_formulas";

        #$tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("SELECT k.*, k2.plan AS plan_for_year, p.post_title, 
          fm.type AS formula_type, fm.formulas, fm.title AS formula_title, fm.note AS formula_note 
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id
          LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent
          WHERE k.status IN ('".KPI_STATUS_RESULT."', '".KPI_STATUS_WAITING."', '".KPI_STATUS_DONE."') AND k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %d AND y.month = %d ) 
              AND k.user_id = %d "
            , $time_value, $month, $userID);

        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function get_kpi_by_year_orgchart_for_precious_and_status($time_value, $precious, $userID){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $tableKpiFormulas = "{$prefix}kpi_formulas";

        #$tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("SELECT k.*, k2.plan AS plan_for_year, p.post_title, 
          fm.type AS formula_type, fm.formulas, fm.title AS formula_title, fm.note AS formula_note  
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id
          LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent
          WHERE k.status IN ('".KPI_STATUS_RESULT."', '".KPI_STATUS_WAITING."', '".KPI_STATUS_DONE."') AND k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %d AND y.precious = %d ) 
              AND k.user_id = %d "
            , $time_value, $precious, $userID);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

/**
 * @param $type
 * @param $time_value
 * @param $userID
 * @param string $isOwner
 * @return mixed
 */
function get_kpi_by_year_orgchart_for_year($type, $time_value, $userID, $isOwner = 'all'){
	global $wpdb;
	static $_results = [];
	$__key = md5(serialize(func_get_args()));
	if( !isset( $_results[$__key] ) ){
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpi = "{$prefix}kpis";
		$tableKpiYear = "{$prefix}kpi_years";
		$tableKpiFormulas = "{$prefix}kpi_formulas";
		#$tableChart = "{$prefix}org_charts";
        if( $isOwner == 'all' ){
            $whereOwner = "";
        }elseif( $isOwner == 'no' ){
            $whereOwner = " k.owner = 'no' AND ";
        }else{
            $whereOwner = " k.owner = 'yes' AND ";
        }
		$sql = $wpdb->prepare("SELECT k.*, k2.plan AS plan_for_year, p.post_title, fm.type AS formula_type, fm.formulas 
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id
          LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent
          WHERE {$whereOwner} k.type = %s AND k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %d ) 
              AND k.user_id = %d "
			, $type, $time_value, $userID);
		$_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
	}
	return $_results[$__key];
}

function kpi_get_group_kpi_by_year_orgchart_for_year($types, $time_value, $userID, $isOwner = 'all'){
	global $wpdb;
	static $_results = [];
	$__key = md5(serialize(func_get_args()));
	if( !isset( $_results[$__key] ) ){
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpi = "{$prefix}kpis";
		$tableKpiYear = "{$prefix}kpi_years";
		$tableKpiFormulas = "{$prefix}kpi_formulas";
		#$tableChart = "{$prefix}org_charts";
        if( $isOwner == 'all' ){
            $whereOwner = "";
        }elseif( $isOwner == 'no' ){
            $whereOwner = " k.owner = 'no' AND ";
        }else{
            $whereOwner = " k.owner = 'yes' AND ";
        }
		$sql = $wpdb->prepare("SELECT k.*, k2.plan AS plan_for_year, p.post_title, fm.type AS formula_type, fm.formulas 
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id
          LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent
          WHERE {$whereOwner} k.type IN ({$types}) AND k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %d ) 
              AND k.user_id = %d "
			, $time_value, $userID);
		$_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
	}
	return $_results[$__key];
}
function kpi_get_group_kpi_by_year_orgchart_for_year_of_users($types, $time_value, $userIDs, $kpi_time, $isOwner = 'all'){
	global $wpdb;
	static $_results = [];
	$__key = md5(serialize(func_get_args()));
	if( !isset( $_results[$__key] ) ){
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpi = "{$prefix}kpis";
		$tableKpiYear = "{$prefix}kpi_years";
		$tableKpiFormulas = "{$prefix}kpi_formulas";
		#$tableChart = "{$prefix}org_charts";
        if( $isOwner == 'all' ){
            $whereOwner = "";
        }elseif( $isOwner == 'no' ){
            $whereOwner = " k.owner = 'no' AND ";
        }else{
            $whereOwner = " k.owner = 'yes' AND ";
        }
		$sql = $wpdb->prepare("SELECT k.*, k2.plan AS plan_for_year, p.post_title, fm.type AS formula_type, fm.formulas 
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id
          LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent
          WHERE {$whereOwner} k.type IN ({$types}) AND k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %d AND y.kpi_time = %s ) 
              AND k.user_id IN ({$userIDs}) "
			, $time_value, $kpi_time);
		$_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
	}
	return $_results[$__key];
}
function kpi_get_group_kpi_by_year_orgchart_for_year_company($time_value){
	global $wpdb;
	static $_results = [];
	$__key = md5(serialize(func_get_args()));
	if( !isset( $_results[$__key] ) ){
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpi = "{$prefix}kpis";
		$tableKpiYear = "{$prefix}kpi_years";
		$tableKpiFormulas = "{$prefix}kpi_formulas";
		#$tableChart = "{$prefix}org_charts";
		$sql = $wpdb->prepare("SELECT k.*, k2.plan AS plan_for_year, p.post_title, fm.type AS formula_type, fm.formulas 
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id
          LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent
          WHERE k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %d AND y.parent = 0 AND y.kpi_type = 'congty' ) "
			, $time_value);
		$_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
	}
	return $_results[$__key];
}

function kpi_get_group_kpi_by_year_orgchart_for_year_by_lower_grade($types, $time_value, $userIDs, $isOwner = 'all'){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $tableKpiFormulas = "{$prefix}kpi_formulas";
	    $tableChart = "{$prefix}org_charts";
        #$tableChart = "{$prefix}org_charts";
        if( $isOwner == 'all' ){
            $whereOwner = "";
        }elseif( $isOwner == 'no' ){
            $whereOwner = " k.owner = 'no' AND ";
        }else{
            $whereOwner = " k.owner = 'yes' AND ";
        }
        $sql = $wpdb->prepare("SELECT k.*, k2.plan AS plan_for_year, p.post_title, fm.type AS formula_type, fm.formulas, org.alias  
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          INNER JOIN {$tableChart} AS org ON org.id = k.chart_id 
          LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id
          LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent
          WHERE {$whereOwner} k.type IN ({$types}) AND k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %d ) 
              AND k.user_id IN ({$userIDs}) "
            , $time_value);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}
function kpi_get_group_kpi_by_year_orgchart_for_year_by_lower_grade_pb($types, $time_value, $userIDs, $isOwner = 'all'){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $tableKpiFormulas = "{$prefix}kpi_formulas";
	    $tableChart = "{$prefix}org_charts";
        #$tableChart = "{$prefix}org_charts";
        if( $isOwner == 'all' ){
            $whereOwner = "";
        }elseif( $isOwner == 'no' ){
            $whereOwner = " k.owner = 'no' AND ";
        }else{
            $whereOwner = " k.owner = 'yes' AND ";
        }
        $sql = $wpdb->prepare("SELECT k.*, k2.plan AS plan_for_year, p.post_title, fm.type AS formula_type, fm.formulas, org.alias, fm.title AS formula_title, fm.note AS fomula_note   
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          INNER JOIN {$tableChart} AS org ON org.id = k.chart_id 
          LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id
          LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent
          WHERE {$whereOwner} k.type IN ({$types}) AND k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %d ) 
              AND k.user_id IN ({$userIDs}) 
              GROUP BY k.chart_id "
            , $time_value);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}
function kpi_get_group_kpi_by_year_orgchart_for_year_by_lower_grade_pb_chart($types, $time_value, $chartIDs, $isOwner = 'all' ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $tableKpiFormulas = "{$prefix}kpi_formulas";
	    $tableChart = "{$prefix}org_charts";
        #$tableChart = "{$prefix}org_charts";
        if( $isOwner == 'all' ){
            $whereOwner = "";
        }elseif( $isOwner == 'no' ){
            $whereOwner = " k.owner = 'no' AND ";
        }else{
            $whereOwner = " k.owner = 'yes' AND ";
        }

        $sql = $wpdb->prepare("SELECT k.*, k2.plan AS plan_for_year, p.post_title, fm.type AS formula_type, fm.formulas, org.alias, fm.title AS formula_title, fm.note AS fomula_note, 
	inf.post_title AS influence_title, inf.fm2_formula_title, inf.fm2_formula_type, inf.fm2_formulas, inf.fm2_formula_note 
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          INNER JOIN {$tableChart} AS org ON org.id = k.chart_id 
          LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id
          LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent
          LEFT JOIN (
                SELECT p2.post_title, k3.id AS id_child, fm2.title AS fm2_formula_title,  
                fm2.type AS fm2_formula_type, fm2.formulas AS fm2_formulas, 
                fm2.note AS fm2_formula_note  
                FROM {$wpdb->posts} as p2
                INNER JOIN {$tableKpi} AS k3 ON k3.bank_id = p2.ID 
                LEFT JOIN {$tableKpiFormulas} AS fm2 ON (p2.`formula_id` = fm2.`id`)   
                ) AS inf ON inf.id_child = k.influence
          WHERE {$whereOwner} k.type IN ({$types}) AND k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %d ) 
              AND k.chart_id IN ({$chartIDs})
              AND k.status IN ('".KPI_STATUS_RESULT."', '".KPI_STATUS_WAITING."', '".KPI_STATUS_DONE."')  
              "
            , $time_value);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function kpi_get_group_kpi_by_year_orgchart_for_year_by_lower_grade_ceo($types, $time_value, $orgcharts, $isOwner = 'all', $influence = ''){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $tableKpiFormulas = "{$prefix}kpi_formulas";
        #$tableChart = "{$prefix}org_charts";
        if( $isOwner == 'all' ){
            $whereOwner = "";
        }elseif( $isOwner == 'no' ){
            $whereOwner = " k.owner = 'no' AND ";
        }else{
            $whereOwner = " k.owner = 'yes' AND ";
        }
        if( !empty($influence) ){
        	$whereOwner .= $wpdb->prepare(" k.influence = %s AND ", $influence);
        }
        $sql = $wpdb->prepare("SELECT k.*, k2.plan AS plan_for_year, p.post_title, fm.type AS formula_type, fm.formulas, fm.title AS formula_title, fm.note AS formula_note  
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id
          LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent
          WHERE {$whereOwner} k.type IN ({$types}) AND k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %d ) AND k.parent = 0  
              AND k.chart_id IN ({$orgcharts}) "
            , $time_value);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function capacity_get_group_kpi_by_year_orgchart_for_year_by_lower_grade($types, $time_value, $orgcharts, $isOwner = 'all'){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $tableKpiFormulas = "{$prefix}kpi_formulas";
        #$tableChart = "{$prefix}org_charts";
        if( $isOwner == 'all' ){
            $whereOwner = "";
        }elseif( $isOwner == 'no' ){
            $whereOwner = " k.owner = 'no' AND ";
        }else{
            $whereOwner = " k.owner = 'yes' AND ";
        }
        $sql = $wpdb->prepare("SELECT k.*, k2.plan AS plan_for_year, p.post_title, fm.type AS formula_type, fm.formulas 
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id
          LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent
          WHERE {$whereOwner} k.type IN ({$types}) AND k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %d ) AND k.parent = 0  
              AND k.chart_id IN ({$orgcharts}) "
            , $time_value);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function kpi_get_group_kpi_by_year_orgchart_for_year_by_lower_grade_nv($types, $time_value, $userIDs, $isOwner = 'all'){
	global $wpdb;
	static $_results = [];
	$__key = md5(serialize(func_get_args()));
	if( !isset( $_results[$__key] ) ){
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpi = "{$prefix}kpis";
		$tableKpiYear = "{$prefix}kpi_years";
		$tableKpiFormulas = "{$prefix}kpi_formulas";
		#$tableChart = "{$prefix}org_charts";
        if( $isOwner == 'all' ){
            $whereOwner = "";
        }elseif( $isOwner == 'no' ){
            $whereOwner = " k.owner = 'no' AND ";
        }else{
            $whereOwner = " k.owner = 'yes' AND ";
        }
		$sql = $wpdb->prepare("SELECT k.*, k2.plan AS plan_for_year, p.post_title, fm.type AS formula_type, fm.formulas, fm.title AS formula_title, fm.note AS formula_note  
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id
          LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent
          WHERE {$whereOwner} k.type IN ({$types}) AND k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %d AND ( y.precious IS NULL OR y.precious = 0 ) ) 
              AND k.user_id IN ({$userIDs}) "
			, $time_value);
		$_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
	}
	return $_results[$__key];
}

function kpi_sum_kpi_by_parent( $id ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $sql = $wpdb->prepare("
          SELECT SUM(k.actual_gain) AS total_actual_gain
          FROM {$tableKpi} AS k 
          WHERE k.parent = %d "
            , $id);
        $_results[$__key] = $wpdb->get_row( $sql, ARRAY_A );
    }
    return $_results[$__key];
}
/**
 * Lấy kết quả KPI theo mỗi tháng đăng ký
*/
function kpi_unit_percent_sum_kpi_by_parent( $id ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $sql = $wpdb->prepare("
          SELECT AVG(k.actual_gain) AS total_actual_gain
          FROM {$tableKpi} AS k 
          WHERE k.parent = %d AND k.status IN ('".KPI_STATUS_RESULT."', '".KPI_STATUS_WAITING."', '".KPI_STATUS_DONE."' )"
            , $id);
        $_results[$__key] = $wpdb->get_row( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function kpi_approve_sum_kpi_by_parent( $id ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $sql = $wpdb->prepare("
          SELECT SUM(k.actual_gain) AS total_actual_gain
          FROM {$tableKpi} AS k 
          WHERE k.parent = %d AND k.status IN ('".KPI_STATUS_DONE."') "
            , $id);
        $_results[$__key] = $wpdb->get_row( $sql, ARRAY_A );
    }
    return $_results[$__key];
}
/**
 * Lấy kết quả KPI theo mỗi tháng đăng ký
 */
function kpi_unit_pecent_approve_sum_kpi_by_parent( $id ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $sql = $wpdb->prepare("
          SELECT AVG(k.actual_gain) AS total_actual_gain
          FROM {$tableKpi} AS k 
          WHERE k.parent = %d AND k.status IN ('".KPI_STATUS_DONE."') "
            , $id);
        $_results[$__key] = $wpdb->get_row( $sql, ARRAY_A );
    }
    return $_results[$__key];
}


function kpi_get_file_kpi_by_parent( $parentID ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $sql = $wpdb->prepare("
          SELECT k.*
          FROM {$tableKpi} AS k 
          WHERE k.files != '' AND k.parent = %d", $parentID);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}



function kpi_get_group_kpi_by_year_cty($types, $time_value, $isOwner = 'all'){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $tableKpiFormulas = "{$prefix}kpi_formulas";
        #$tableChart = "{$prefix}org_charts";
        if( $isOwner == 'all' ){
            $whereOwner = "";
        }elseif( $isOwner == 'no' ){
            $whereOwner = " k.owner = 'no' AND ";
        }else{
            $whereOwner = " k.owner = 'yes' AND ";
        }
        $sql = $wpdb->prepare("SELECT k.*, k2.plan AS plan_for_year, p.post_title, fm.type AS formula_type, fm.formulas 
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id
          LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent
          WHERE {$whereOwner} k.required = %s AND k.create_by_node = 'node_start' AND 
              k.type IN ({$types}) AND k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %d AND y.kpi_type = %s ) 
               "
            , 'yes', $time_value, 'congty');
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}


function get_kpi_by_year_orgchart_not_month($time_value, $userID){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        #$tableChart = "{$prefix}org_charts";
	    $tableKpiFormulas = "{$prefix}kpi_formulas";
        $sql = $wpdb->prepare("SELECT k.*, k2.plan AS plan_for_year, p.post_title, 
				fm.type AS formula_type, fm.formulas 
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent
          LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id
          WHERE k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %s AND ( y.month IS NULL OR y.month = 0 ) ) 
              AND k.user_id = %d "
            , $time_value, $userID);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}
function get_kpi_by_year_orgchart_not_month_not_precious($time_value, $userID){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        #$tableChart = "{$prefix}org_charts";
	    $tableKpiFormulas = "{$prefix}kpi_formulas";
        $sql = $wpdb->prepare("SELECT k.*, k2.plan AS plan_for_year, p.post_title,
			 fm.type AS formula_type, fm.formulas  
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent
          LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id 
          WHERE k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %s AND ( y.month IS NULL OR y.month = 0 ) AND (y.precious  IS NULL OR y.precious = 0) ) 
              AND k.user_id = %d "
            , $time_value, $userID);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}
function get_kpi_by_year_orgchart_not_month_not_precious_myself($time_value, $userID){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        #$tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("SELECT k.*, k2.plan AS plan_for_year, p.post_title 
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent
          WHERE k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %s AND ( y.month IS NULL OR y.month = 0 ) AND (y.precious  IS NULL OR y.precious = 0) ) 
              AND k.user_id = %d AND k.owner='yes' "
            , $time_value, $userID);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function kpi_get_list_kpi_by_user($year, $userID, $precious = 0, $month = 0){
	global $wpdb;
	static $_results = [];
	$__key = md5(serialize(func_get_args()));
	if( !isset( $_results[$__key] ) ){
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpi = "{$prefix}kpis";
		$tableKpiYear = "{$prefix}kpi_years";
		$sqlYear = '';
		if( empty($precious) && empty($month) ){
			$sqlYear = $wpdb->prepare("SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %s AND ( y.month IS NULL OR y.month = 0 ) AND (y.precious  IS NULL OR y.precious = 0)", $year);
		}elseif( !empty($precious) ){
			$sqlYear = $wpdb->prepare("SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %s AND y.precious = %d", $year, $precious);
		}elseif( !empty($month) ){
			$sqlYear = $wpdb->prepare("SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %s AND y.precious = %d AND y.month = %d", $year, $precious, $month);
		}
		$sql = $wpdb->prepare("SELECT k.*, k2.plan AS plan_for_year, p.post_title 
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent 
          WHERE k.year_id IN (
          		{$sqlYear}
               ) 
              AND k.user_id = %d "
			, $userID);
		$_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
	}
	return $_results[$__key];
}


function get_kpi_by_year( $year_id, $kpiID, $userID ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        #$tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("SELECT k.plan, k.id, k.year_id, y.month, k.status, k.percent  
          FROM {$tableKpi} AS k 
          INNER JOIN {$tableKpiYear} AS y ON y.id = k.year_id AND y.parent IN (%d)
          WHERE k.parent = %d AND k.user_id = %d
          ORDER BY y.month ASC 
          "
            , $year_id, $kpiID, $userID);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}
function get_kpi_by_year_precious( $year_id, $kpiID, $userID ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        #$tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("SELECT k.plan, k.id, k.year_id, y.precious, k.status, k.percent  
          FROM {$tableKpi} AS k 
          INNER JOIN {$tableKpiYear} AS y ON y.id = k.year_id AND y.parent IN (%d)
          WHERE k.parent = %d AND k.user_id = %d
          ORDER BY y.precious ASC 
          "
            , $year_id, $kpiID, $userID);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

/**
 * lấy số lượng kpi cần duyệt
 * @param $year_id
 * @param $kpiID
 * @param $userIDs integer || array
 * @param $status string || array
 * @return mixed
 */

function count_kpi_by_year_and_user( $year_id, $kpiID, $userIDs, $status ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        #$tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("SELECT COUNT(k.id) AS amount_status 
          FROM {$tableKpi} AS k 
          INNER JOIN {$tableKpiYear} AS y ON y.id = k.year_id AND y.parent IN (%d)
          WHERE k.status IN ('{$status}') AND k.parent = %d AND k.user_id IN ({$userIDs})
          ORDER BY y.month ASC 
          "
            , $year_id, $kpiID);
        $_results[$__key] = $wpdb->get_row( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function count_kpi_of_year_by_user( $year, $userIDs, $status ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $sql = $wpdb->prepare("SELECT COUNT(k.id) AS amount_status 
          FROM {$tableKpi} AS k 
          INNER JOIN {$tableKpiYear} AS y ON y.id = k.year_id AND y.year IN (%d)
          WHERE k.status IN ('{$status}') AND k.user_id IN ({$userIDs}) 
          		AND ( (k.plan != '' AND k.plan != 0) AND (k.percent != '' AND k.percent != 0 ) )
          ORDER BY y.month ASC 
          ", $year);
        $_results[$__key] = $wpdb->get_row( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function count_kpi_of_year_by_user_status_duyet( $year, $userIDs ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $sql = $wpdb->prepare("
		  SELECT COUNT(k.id) AS amount_status, 'pending' AS status_type
          FROM {$tableKpi} AS k 
          INNER JOIN {$tableKpiYear} AS y ON y.id = k.year_id AND y.year = %d
          WHERE k.status IN ('".KPI_STATUS_PENDING."') AND k.user_id IN ({$userIDs})
                AND ( (k.plan != '' AND k.plan != 0) AND (k.percent != '' AND k.percent != 0 ) ) 
          UNION 
          SELECT COUNT(k.id) AS amount_status, 'waiting' AS status_type  
          FROM {$tableKpi} AS k 
          INNER JOIN {$tableKpiYear} AS y ON y.id = k.year_id AND y.year = %d
          WHERE k.status IN ('".KPI_STATUS_WAITING."') AND k.user_id IN ({$userIDs})
                AND ( (k.plan != '' AND k.plan != 0) AND (k.percent != '' ANd k.percent != 0 ) ) 
          ", $year, $year);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function get_total_percent_for_member( $chartID, $userID, $year ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $sql = $wpdb->prepare("
              SELECT * 
              FROM {$tableKpi} AS k
              INNER JOIN {$tableKpiYear} AS y ON y.id = k.year_id
              WHERE  k.chart_id = %d AND k.user_id = %d AND y.year = %d AND ( y.month = 0 OR y.month IS NULL ) AND ( y.precious = 0 OR y.precious IS NULL )
              ", $chartID, $userID, $year);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function get_kpi_asign_for_member_backup( $time_value, $department_id ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        #$tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("SELECT k.*, p.post_title 
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          WHERE k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y WHERE y.year = %s) 
              AND k.chart_id = %d AND ( k.company_plan != 0 OR k.department_plan != 0)"
            , $time_value, $department_id);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function get_kpi_asign_for_member( $time_value, $department_id ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $sql = $wpdb->prepare("SELECT k.*, k2.plan AS plan_on_level ,p.post_title 
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent
          WHERE k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y WHERE y.year = %s) 
              AND k.user_id = %d "
            , $time_value, $department_id);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function get_kpi_by_user_orgchart_kpi_id( $userID, $orgchartID, $kpiID ){
	global $wpdb;
	static $_results = [];
	$__key = md5(serialize(func_get_args()));
	if( !isset( $_results[$__key] ) ) {
		$prefix   = $wpdb->get_blog_prefix( get_current_blog_id() );
		$tableKpi = "{$prefix}kpis";
		$sql = $wpdb->prepare("
		SELECT id FROM {$tableKpi} AS k WHERE k.user_id = %d AND k.chart_id = %d
		AND k.id = %d
		", $userID, $orgchartID, $kpiID);
		$_results[$__key] = $wpdb->get_var($sql);
	}
	return $_results[$__key];
}

function get_kpi_year_orgchart_by_bank($time_value, $department_id, $bank_id){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        #$tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("SELECT k.*, p.post_title 
          FROM {$tableKpi} AS k 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id
          WHERE k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y WHERE y.year = %s) 
              AND k.chart_id = %d AND k.bank_id = %d "
            , $time_value, $department_id, $bank_id);
        $_results[$__key] = $wpdb->get_row( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function count_level_kpi($parent){
    global $wpdb;
    static $levels = 0;
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableKpi = "{$prefix}kpis";
    $sql = $wpdb->prepare(
        "SELECT parent
    FROM {$tableKpi} 
    WHERE id = %d 
    ", $parent);
    $row = $wpdb->get_row( $sql );
    if( !empty( $row ) ){
        $parent = $row->parent;
        $levels++;
        $levels = count_level_kpi($parent, $levels);

    }
    return $levels;

}

/**
 * @param $postID
 * @param $chartID
 * @param $yearID
 * @param $type
 *
 * @return mixed
 */
function get_kpi_by_conditions( $postID, $chartID, $yearID, $type ){
    global $wpdb;
    static $results = [];
    $_key = md5( serialize( func_get_args() ) );
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableKpi = "{$prefix}kpis";
    $tableKpiYear = "{$prefix}kpi_years";
    $tableChart = "{$prefix}org_charts";
    if( !isset( $results[$_key] ) ){
        $sql = $wpdb->prepare("
        SELECT k.*, y.id AS yearID, y.year, y.precious, y.month, y.finance, y.operate, y.customer,
        y.development, y.status AS year_status, y.created AS year_created, y.parent AS year_parent FROM {$tableKpi} AS k
        INNER JOIN {$tableKpiYear} AS y ON y.id = k.year_id
        WHERE k.chart_id = %d AND k.type = %s AND k.bank_id = %d AND k.year_id = %d
        ", $chartID, $type, $postID, $yearID);
        $results[$_key] = $wpdb->get_row( $sql, ARRAY_A );
    }
    return $results[$_key];
}


function get_kpi_by_parent($parentID){
	global $wpdb;
	static $_results = [];
	$__key = $parentID;
	if (!isset($_results[$__key])) {
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpi = "{$prefix}kpis";
		$tableKpiYear = "{$prefix}kpi_years";
		$tableChart = "{$prefix}org_charts";
		$_results[$__key] = $wpdb->get_results($wpdb->prepare("SELECT * FROM {$tableKpi} as k WHERE parent = %d", $parentID), ARRAY_A);
	}
	return $_results[$__key];
}

function kpi_get_all_kpi_by_parent( $id, $influence = '' )
{
    global $wpdb;
    static $argsID = [];
    if (!isset($argsID[$id])) {
        $argsID[$id] = [];
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $whereInfluence = '';
        if( !empty($influence) ){
			$whereInfluence = $wpdb->prepare(" AND k.influence = %s ", $influence);
        }
        $sql = $wpdb->prepare("SELECT * FROM {$tableKpi} as k WHERE k.parent = %d {$whereInfluence}", $id);
        $results = $wpdb->get_results($sql, ARRAY_A);
        if (!empty($results) && !is_wp_error($results)) {
            foreach ($results as $key => $item) {
                $argsID[$id][] = $item;
                $argsID[$id] = array_merge($argsID[$id], kpi_get_all_kpi_by_parent($item['id'], $influence));
            }
        }
    }
    return $argsID[$id];
}

function kpi_aprrove_get_all_kpi_by_parent( $id, $influence = '' )
{
	global $wpdb;
	static $argsID = [];
	if (!isset($argsID[$id])) {
		$argsID[$id] = [];
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpi = "{$prefix}kpis";
		$whereInfluence = '';
		if( !empty($influence) ){
			$whereInfluence = $wpdb->prepare(" AND k.influence = %s ", $influence);
		}
		$sql = $wpdb->prepare("SELECT * FROM {$tableKpi} as k WHERE k.parent = %d AND k.status IN ('".KPI_STATUS_DONE."') {$whereInfluence}", $id);
		$results = $wpdb->get_results($sql, ARRAY_A);
		if (!empty($results) && !is_wp_error($results)) {
			foreach ($results as $key => $item) {
				$argsID[$id][] = $item;
				$argsID[$id] = array_merge($argsID[$id], kpi_get_all_kpi_by_parent($item['id'], $influence));
			}
		}
	}
	return $argsID[$id];
}

function kpi_get_all_kpi_by_parent_year( $id )
{
    global $wpdb;
    static $argsID = [];
    if (!isset($argsID[$id])) {
        $argsID[$id] = [];
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
	    $tableKpiYear = "{$prefix}kpi_years";
	    $sql = $wpdb->prepare("
				SELECT k.* FROM {$tableKpi} as k
				INNER JOIN {$tableKpiYear} AS y ON y.id = k.year_id 
				WHERE k.parent = %d AND ( y.precious IS NULL OR y.precious = 0 ) AND ( y.month IS NULL OR y.month = 0 ) 
				", $id);
        $results = $wpdb->get_results( $sql, ARRAY_A );
        if (!empty($results) && !is_wp_error($results)) {
            foreach ($results as $key => $item) {
                $argsID[$id][] = $item;
                $argsID[$id] = array_merge($argsID[$id], kpi_get_all_kpi_by_parent_year($item['id']));
            }
        }
    }
    return $argsID[$id];
}

/**
 * get news feed for position
 * @param $type_time
 * @param $time_value
 * @param int $room
 * @param int $user_id
 * @return array|null|object
 */
function get_newsfeed($time_type, $time_value, $paged = 0, $limit = 8, $room = 0, $user_id = 0){
    global $wpdb;
    static $_results = [];
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableKpi = "{$prefix}kpis";
    $tableKpiYear = "{$prefix}kpi_years";
    $tableChart = "{$prefix}org_charts";
    if (!in_array($time_type, ['year', 'precious', 'month'])) {
        $time_type = 'year';
    }
    if( empty( $room ) && empty( $user ) ){
        # orgchart is CEO
        $whereOrgChart = '';
    }elseif( empty( $user_id )){
        # orgchart is Room
        $whereOrgChart = $wpdb->prepare('k.chart_id = %d AND', $room);
    }else{
        #orgchart is Staff
        $whereOrgChart = '';
        $whereOrgChart = $wpdb->prepare('k.chart_id = %d AND k.user_id = %d AND', $room, $user_id);
    }
    $sql = $wpdb->prepare("
        SELECT *
        FROM {$tableKpi} AS k
        INNER JOIN {$tableKpiYear} as y ON k.year_id = y.id
        INNER JOIN {$wpdb->posts} as p ON p.ID = k.bank_id
        WHERE {$whereOrgChart} y.`{$time_type}` = %d
        GROUP BY k.id
        ORDER BY k.created DESC
        LIMIT %d, %d
    ", $time_value, $paged, $limit );
    $sqlTotal = $wpdb->prepare("
        SELECT COUNT(*) AS total
        FROM {$tableKpi} AS k
        INNER JOIN {$tableKpiYear} as y ON k.year_id = y.id
        INNER JOIN {$wpdb->posts} as p ON p.ID = k.bank_id
        WHERE {$whereOrgChart} y.`{$time_type}` = %d
        ", $time_value);
    $totalRecords = $wpdb->get_var($sqlTotal);
    $_results['posts'] = $wpdb->get_results( $sql );
    $_results['founds'] = $totalRecords;

    if( class_exists('Paging') ) {
        $paging = new Paging( $limit, $totalRecords);
        if( !empty( $_GET['type'] ) ){
            $type = '&type='.$_GET['type'];
        }else{
            $type = '&type=ket-qua-kpi';
        }

        $uID = '';
        $action = 'action=news_feed';
        if( !empty( $_GET['uid'] ) ){
            $uID = "&uid=".$_GET['uid'];
        }
        $queryStr = "{$action}{$type}{$uID}";
        $paging->renderNavBar(5, admin_url('admin-ajax.php'), $queryStr);
        $_results['pages'] = $paging->getNavBar();
    }

    return $_results;
}

function kpi_get_item_parent_root($parent_id) {
    static $_results = [];
    $key = $parent_id;
    $parent_id = intval($parent_id);
    if( !isset($_results[$key]) ) {
        $_results[$key] = [];
        $parent = null;
        while( $parent_id > 0 ) {
            $row = get_kpi_by_parent($parent_id);
            if( !empty($row) ) {
                $parent_id = (int)$row['parent'];
                if( !is_null($parent) ) {
                    $row['child'] = &$parent;
                    $parent['parentItem'] = &$row;
                    $parent = &$row;
                } else {
                    $row['child'] = null;
                    $parent = &$row;
                }
                $_results[$key] = &$parent;
            } else {
                break;
            }
        }
    }
    if( !isset($_results[$key]) ) {
        $_results[$key] = null;
    }
    return $_results[$key];
}

function list_kpi_need_approved( $year, $orgchart_id, $user_id ){
    global $wpdb;
    static $_results = [];
    $__key = md5( serialize( func_get_args() ) );
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableKpi = "{$prefix}kpis";
    $tableKpiYear = "{$prefix}kpi_years";
    if( !isset( $_results[$__key] ) ){
        #AND k.required = 'no'
        $sql = $wpdb->prepare( "SELECT k.*, p.post_title ".
        "FROM {$tableKpi} AS k ".
        "INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id ".
        "WHERE k.status != %s AND k.user_id = %d AND k.chart_id = %d ".
        "AND k.year_id IN (SELECT id FROM {$tableKpiYear} AS y WHERE y.year = %d ".
        "AND ( y.month = 0 OR y.month IS NULL ) AND ( y.precious = 0 OR y.precious IS NULL ) )",
            KPI_STATUS_DRAFT, $user_id, $orgchart_id, $year );

        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function list_kpi_by_status_done_create_by_start( $user_id, $orgchart_id, $year ){
    global $wpdb;
    static $_results = [];
    $__key = md5( serialize( func_get_args() ) );
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableKpi = "{$prefix}kpis";
    $tableKpiYear = "{$prefix}kpi_years";
    $tableKpiFormulas = "{$prefix}kpi_formulas";
    if( !isset( $_results[$__key] ) ){
        $sql = $wpdb->prepare( "SELECT COUNT(k.actual_gain) AS total_plan, k.plan AS plan_for_year, p.post_title, 
          fm.type AS formula_type, fm.formulas, y2.month, y2.precious  ".
            "FROM {$tableKpi} AS k ".
            "INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id ".
            "INNER JOIN $tableKpiYear AS y2 ON y2.id = k.year_id ".
            "LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id " .
            "LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent " .
            "WHERE k.status IN ('".KPI_STATUS_DONE."') AND k.user_id = %d AND k.chart_id = %d AND k.create_by_node = %s ".
            "AND k.year_id IN (SELECT id FROM {$tableKpiYear} AS y WHERE y.year = %d ".
            ")
            GROUP BY k.parent
            ",
            $user_id, $orgchart_id, KPI_CREATE_BY_NODE_START, $year);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}
function list_kpi_by_status_done_create_by_start_and_month( $user_id, $orgchart_id, $year, $month ){
    global $wpdb;
    static $_results = [];
    $__key = md5( serialize( func_get_args() ) );
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableKpi = "{$prefix}kpis";
    $tableKpiYear = "{$prefix}kpi_years";
    $tableKpiFormulas = "{$prefix}kpi_formulas";
    if( !isset( $_results[$__key] ) ){
        $sql = $wpdb->prepare( "SELECT COUNT(k.actual_gain) AS total_plan, k.actual_gain, k.unit, k.plan, k.percent, 
          fm.type AS formula_type, fm.formulas, y2.month, y2.precious  ".
            "FROM {$tableKpi} AS k ".
            "INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id ".
            "INNER JOIN $tableKpiYear AS y2 ON y2.id = k.year_id ".
            "LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id " .
            "WHERE k.status IN ('".KPI_STATUS_DONE."', '".KPI_STATUS_WAITING."', '".KPI_STATUS_RESULT."') AND k.user_id = %d AND k.chart_id = %d AND k.create_by_node = %s ".
            "AND k.year_id IN (SELECT id FROM {$tableKpiYear} AS y WHERE y.year = %d AND y.month = %d".
            ")
            GROUP BY k.parent
            ",
            $user_id, $orgchart_id, KPI_CREATE_BY_NODE_START, $year, $month);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}


function list_kpi_by_status_create_by_start_and_precious( $user_id, $orgchart_id, $year, $precious ){
    global $wpdb;
    static $_results = [];
    $__key = md5( serialize( func_get_args() ) );
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableKpi = "{$prefix}kpis";
    $tableKpiYear = "{$prefix}kpi_years";
    $tableKpiFormulas = "{$prefix}kpi_formulas";
    if( !isset( $_results[$__key] ) ){
        if( !empty( $precious ) ){
            $wherePrecious = $wpdb->prepare(" AND y.precious = %d ", $precious);
        }else{
            $wherePrecious = $wpdb->prepare(" AND ( y.precious IS NULL OR y.precious = 0 ) ", $precious);
        }
        # SUM(k.actual_gain) AS total_actual_gain, SUM(k.plan) AS total_plan, SUM(k.percent) AS total_percent,
        $sql = $wpdb->prepare( "SELECT SUM(k.percent) AS total_percent, SUM(k.actual_gain) AS total_kq, k.id, k.actual_gain, k.unit, k.plan, k.percent, k.year_id, k.type, k.owner, 
          fm.type AS formula_type, fm.formulas, y2.month, y2.precious  ".
            "FROM {$tableKpi} AS k ".
            "INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id ".
            "INNER JOIN $tableKpiYear AS y2 ON y2.id = k.year_id ".
            "LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id " .
            "WHERE k.status IN ('". KPI_STATUS_RESULT ."', '".KPI_STATUS_DONE."', '".KPI_STATUS_WAITING."') AND k.user_id = %d AND k.chart_id = %d AND k.create_by_node = %s ".
            "AND k.year_id IN (SELECT id FROM {$tableKpiYear} AS y WHERE y.year = %d AND y.kpi_type = 'congty' {$wherePrecious}".
            ") GROUP BY y2.precious
            ",
            $user_id, $orgchart_id, KPI_CREATE_BY_NODE_START, $year);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}


function list_kpi_by_status_create_by( $user_id, $orgchart_id, $year, $precious, $month = '' ){
    global $wpdb;
    static $_results = [];
    $__key = md5( serialize( func_get_args() ) );
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableKpi = "{$prefix}kpis";
    $tableKpiYear = "{$prefix}kpi_years";
    $tableKpiFormulas = "{$prefix}kpi_formulas";
    if( !isset( $_results[$__key] ) ){
        if( !empty( $precious ) ){
            $wherePrecious = $wpdb->prepare(" AND y.precious = %d ", $precious);
        }else{
            $wherePrecious = $wpdb->prepare(" AND ( y.precious IS NULL OR y.precious = 0 ) ", $precious);
        }
	    if( !empty( $month ) ){
		    $wherePrecious .= $wpdb->prepare(" AND y.month = %d ", $month);
	    }else{
		    $wherePrecious .= $wpdb->prepare(" AND ( y.month IS NULL OR y.month = 0 ) ", $precious);
	    }
        # SUM(k.actual_gain) AS total_actual_gain, SUM(k.plan) AS total_plan, SUM(k.percent) AS total_percent,
        $sql = $wpdb->prepare( "SELECT SUM(k.percent) AS total_percent, SUM(k.actual_gain) AS total_kq, k.id, k.actual_gain, k.unit, k.plan, k.percent, k.year_id, k.type, k.owner, 
          fm.type AS formula_type, fm.formulas, y2.month, y2.precious  ".
            "FROM {$tableKpi} AS k ".
            "INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id ".
            "INNER JOIN $tableKpiYear AS y2 ON y2.id = k.year_id ".
            "LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id " .
            "WHERE k.status IN ('". KPI_STATUS_RESULT ."', '".KPI_STATUS_DONE."', '".KPI_STATUS_WAITING."') AND k.user_id = %d AND k.chart_id = %d ".
            "AND k.year_id IN (SELECT id FROM {$tableKpiYear} AS y WHERE y.year = %d AND y.kpi_type = 'congty' {$wherePrecious}".
            " AND y.chart_id=%d ) GROUP BY y2.precious
            ",
            $user_id, $orgchart_id, $year, $orgchart_id);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}



function list_kpi_by_status_done_create_by_start_and_precious( $ids, $year, $precious, $month = '' ){
    global $wpdb;
    static $_results = [];
    $__key = md5( serialize( func_get_args() ) );
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableKpi = "{$prefix}kpis";
    $tableKpiYear = "{$prefix}kpi_years";
    $tableKpiFormulas = "{$prefix}kpi_formulas";
    if( !isset( $_results[$__key] ) ){
        $wherePrecious = '';
        if( !empty( $precious ) ){
            $wherePrecious = $wpdb->prepare(" AND y.precious = %d ", $precious);
        }
        if( !empty( $month ) ){
        	$wherePrecious .= $wpdb->prepare(" AND y.month = %d ", $month);
        }
	    $whereStatus = "AND k.status IN ('".KPI_STATUS_DONE."')";
        if( empty( $precious ) && empty( $month ) ){
        	$whereStatus = "";
        }

        # SUM(k.actual_gain) AS total_actual_gain, SUM(k.plan) AS total_plan, SUM(k.percent) AS total_percent,
        $sql = $wpdb->prepare( "SELECT k.id, k.actual_gain, k.unit, k.plan, k.percent, 
          fm.type AS formula_type, fm.formulas, y2.month, y2.precious, k.status ".
            "FROM {$tableKpi} AS k ".
            "INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id ".
            "INNER JOIN $tableKpiYear AS y2 ON y2.id = k.year_id ".
            "LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id " .
            "WHERE k.id IN ({$ids}) {$whereStatus} AND k.create_by_node = %s ".
            "AND k.year_id IN (SELECT id FROM {$tableKpiYear} AS y WHERE y.year = %d {$wherePrecious}".
            ")
            ",
            KPI_CREATE_BY_NODE_START, $year);
        #echo $sql . ' <br>';
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}


function list_kpi_unit_percent_by_status_done_create_by_start_and_precious( $ids, $year, $precious, $month = '' ){
    global $wpdb;
    static $_results = [];
    $__key = md5( serialize( func_get_args() ) );
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableKpi = "{$prefix}kpis";
    $tableKpiYear = "{$prefix}kpi_years";
    $tableKpiFormulas = "{$prefix}kpi_formulas";
    if( !isset( $_results[$__key] ) ){
        $wherePrecious = '';
        if( !empty( $precious ) ){
            $wherePrecious = $wpdb->prepare(" AND y.precious = %d ", $precious);
        }
        if( !empty( $month ) ){
            $wherePrecious .= $wpdb->prepare(" AND y.month = %d ", $month);
        }
        $whereStatus = "AND k.status IN ('".KPI_STATUS_DONE."', '".KPI_STATUS_RESULT."', '".KPI_STATUS_WAITING."')";
        /*if( empty( $precious ) && empty( $month ) ){
            $whereStatus = "";
        }*/

        # SUM(k.actual_gain) AS total_actual_gain, SUM(k.plan) AS total_plan, SUM(k.percent) AS total_percent,
        $sql = $wpdb->prepare( "SELECT k.id, k.actual_gain, k.unit, k.plan, k.percent, 
          fm.type AS formula_type, fm.formulas, y2.month, y2.precious ".
            "FROM {$tableKpi} AS k ".
            "INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id ".
            "INNER JOIN $tableKpiYear AS y2 ON y2.id = k.year_id ".
            "LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id " .
            "WHERE k.id IN ({$ids}) {$whereStatus} AND k.create_by_node = %s ".
            "AND k.year_id IN (SELECT id FROM {$tableKpiYear} AS y WHERE y.year = %d {$wherePrecious}".
            ")
            ",
            KPI_CREATE_BY_NODE_START, $year);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}


function list_kpi_need_approved_results( $year, $orgchart_id, $user_id, $precious = '', $month = '', $owner = '' ){
    global $wpdb;
    static $_results = [];
    $__key = md5( serialize( func_get_args() ) );
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableKpi = "{$prefix}kpis";
    $tableKpiYear = "{$prefix}kpi_years";
    $tableKpiFormulas = "{$prefix}kpi_formulas";
    if( !isset( $_results[$__key] ) ){
        if( $precious ){
            $wherePrecious = $wpdb->prepare(" AND y.precious = %d AND k.status IN ('".KPI_STATUS_WAITING."', '".KPI_STATUS_DONE."')", $precious);
        }else{
	        $wherePrecious = $wpdb->prepare(' AND ( y.precious IS NULL OR y.precious = 0) ', $precious);
        }
        if( !empty($month) ){
        	$wherePrecious .= $wpdb->prepare(" AND y.month = %d ", $month);
        }else{
	        $wherePrecious .= $wpdb->prepare(' AND ( y.month IS NULL OR y.month = 0) ', $precious);
        }
        $whereOwner = '';
        if( !empty( $owner ) ){
        	$whereOwner = $wpdb->prepare(" AND k.owner = %s ", $owner);
        }
        $sql = $wpdb->prepare( "SELECT k.*, k2.plan AS plan_for_year, p.post_title, 
          fm.type AS formula_type, fm.formulas, y2.month, y2.precious  ".
            "FROM {$tableKpi} AS k ".
            "INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id ".
            "INNER JOIN $tableKpiYear AS y2 ON y2.id = k.year_id ".
            "LEFT JOIN {$tableKpiFormulas} AS fm ON fm.id = p.formula_id " .
            "LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent " .
            "WHERE k.user_id = %d AND k.chart_id = %d ".
            "AND k.year_id IN (SELECT id FROM {$tableKpiYear} AS y WHERE y.year = %d ".
            " $wherePrecious ) {$whereOwner} 
            ORDER BY k.create_by_node, k.bank_id DESC, y2.month ASC ",
            $user_id, $orgchart_id, $year );
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function kpi_get_list_approved_register( $type, $year, $user_id, $precious = '' ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $tableKpiFormulas = "{$prefix}kpi_formulas";
        $wherePrecious = '';
        if( $precious ){
            $wherePrecious = $wpdb->prepare(' AND y.precious = %d ', $precious);
        }
        $sql = $wpdb->prepare( "SELECT k.*, k2.plan AS plan_for_year, p.post_title, y2.month, y2.precious  ".
            "FROM {$tableKpi} AS k ".
            "INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id ".
            "INNER JOIN $tableKpiYear AS y2 ON y2.id = k.year_id ".
            "LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent " .
            "WHERE k.type = %s AND k.status != %s AND k.user_id = %d AND k.required='no' ".
            "AND k.year_id IN (SELECT id FROM {$tableKpiYear} AS y WHERE y.year = %d ".
            " $wherePrecious )",
            $type, KPI_STATUS_DRAFT, $user_id, $year );
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function kpi_get_list_register_room( $type, $year_id, $chartID ){
	global $wpdb;
	static $_results = [];
	$__key = md5(serialize(func_get_args()));
	if (!isset($_results[$__key])) {
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpi = "{$prefix}kpis";
		$tableKpiYear = "{$prefix}kpi_years";
		$tableKpiFormulas = "{$prefix}kpi_formulas";
		$sql = $wpdb->prepare( "SELECT k.*, k2.plan AS plan_for_year, p.post_title, y2.month, y2.precious, y2.year ".
		                       "FROM {$tableKpi} AS k ".
		                       "INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id ".
		                       "INNER JOIN $tableKpiYear AS y2 ON y2.id = k.year_id ".
		                       "LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent " .
		                       "WHERE k.type = %s AND k.status != %s AND k.chart_id = %d ".
		                       "AND k.year_id = %d ",
			$type, KPI_STATUS_DRAFT, $chartID, $year_id );

		$_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
	}
	return $_results[$__key];
}
function kpi_get_list_register( $type, $year_id, $chartID ){
	global $wpdb;
	static $_results = [];
	$__key = md5(serialize(func_get_args()));
	if (!isset($_results[$__key])) {
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpi = "{$prefix}kpis";
		$tableKpiYear = "{$prefix}kpi_years";
		$tableKpiFormulas = "{$prefix}kpi_formulas";
		$sql = $wpdb->prepare( "SELECT k.*, k2.plan AS plan_for_year, p.post_title, y2.month, y2.precious, y2.year ".
		                       "FROM {$tableKpi} AS k ".
		                       "INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id ".
		                       "INNER JOIN $tableKpiYear AS y2 ON y2.id = k.year_id ".
		                       "LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent " .
		                       "WHERE k.owner = 'yes' AND k.type = %s AND k.status != %s AND k.chart_id = %d ".
		                       "AND k.year_id IN ( SELECT id FROM {$tableKpiYear} AS y WHERE y.parent = %d ) ",
			$type, KPI_STATUS_DRAFT, $chartID, $year_id );

		$_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
	}
	return $_results[$__key];
}
function kpi_de_quy_search_node_0( $IDs, $output = [] ){
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableKpi = "{$prefix}kpis";
    if( !empty( $IDs ) ){
        $sql = "
              SELECT k.* 
              FROM {$tableKpi} AS k 
              WHERE k.id IN ($IDs) ";
        $res = $wpdb->get_results( $sql, ARRAY_A );
        if( !empty( $res ) ){
            $arrID = [];
            $output = $res;
            foreach ( $res as $key => $item ){
                if( $item['parent'] == 0 ){
                    return $item;
                }else {
                    $arrID[] = $item['parent'];
                }
            }
            $IDs = implode( ",", $arrID );
            kpi_de_quy_search_node_0( $IDs, $output );
        }else{
            return $output;
        }
    }
}

/**
 * @param $kpiID
 *
 * @return bool|mixed
 */
function kpi_get_kpi_node_0( $kpiID ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        if( $kpiID != 0 ){
            $sql = $wpdb->prepare("
          SELECT k.* 
          FROM {$tableKpi} AS k 
          WHERE k.id = %d ", $kpiID);
            $results = $wpdb->get_results( $sql, ARRAY_A );
            if( !empty( $results ) ){
                $arrID = [];
                foreach ( $results as $key => $item ){
                    if( $item['parent' == 0] ){
                        return $item;
                    }else {
                        $arrID[] = $item['parent'];
                    }
                }
                $IDs = implode( ",", $arrID );
                $_results[$__key] = kpi_de_quy_search_node_0( $IDs );
            }else{
                $_results[$__key] = $results;
            }
        }else{
            return false;
        }
    }
    return $_results[$__key];
}

/**
 * @param $item
 * @param string $influence
 *
 * @return float|int|string
 */
function get_actual_gain( $item, $influence = '' ){
	$ttactual_gain = 0;
	$checkChangeActual_gain = false;
	$id = $item['id'];
	if( !empty( $item['formulas'] ) ){
		$formulas = maybe_unserialize( $item['formulas'] );
	}else{
		$formulas = [];
	}
	if( $item['owner'] == 'no' ) {
		$kpiAll = kpi_get_all_kpi_by_parent( $id, $influence );
		$arrIDs = [];
		if ( ! empty( $kpiAll ) ) {
			foreach ( $kpiAll as $k => $v ) {
				$arrIDs[] = $v['id'];
			}
			$arrIDs  = implode( ", ", $arrIDs );
			$results = list_kpi_by_status_done_create_by_start_and_precious( $arrIDs, TIME_YEAR_VALUE, TIME_PRECIOUS_VALUE, TIME_MONTH_VALUE );
			if ( !empty( $results ) ) {
				$arrPercentForTime = [];
				foreach ( $results as $ks => $vs ) {
					$ttplan        = $vs['plan'];
					#$ttpercent     = $vs['percent'];
					if( $vs['unit'] == KPI_UNIT_THOI_GIAN ){
						$arrPercentForTime[] = getPercentForMonth(  $vs['unit'], $item['formula_type'], $formulas, $item['plan'], $vs['actual_gain']  );
						if( !$checkChangeActual_gain ) {
							$checkChangeActual_gain = true;
						}
					}else {
						if( $vs['actual_gain'] != '' ) {
							$ttactual_gain += $vs['actual_gain'];
							if( !$checkChangeActual_gain ) {
								$checkChangeActual_gain = true;
							}
						}
					}
				}
				if( !empty( $arrPercentForTime ) ){
					$ttactual_gain = array_sum( $arrPercentForTime ) / count( $arrPercentForTime );
				}
			} else {
				$ttactual_gain = '';
			}
		} else {
			$ttactual_gain = '';
		}
	}elseif( $item['alias'] == 'phongban' && $item['owner'] == 'yes' ) {
		if( $item['actual_gain'] != '' ){
			$plan = $item['plan'];
			if( $item['unit'] == KPI_UNIT_THOI_GIAN ){
				getPercentForMonth(  $item['unit'], $item['formula_type'], $formulas, $plan, $item['actual_gain']  );
			}else {
				$ttactual_gain += $item['actual_gain'];
				if ( ! $checkChangeActual_gain ) {
					$checkChangeActual_gain = true;
				}
			}
		}
	}
	return $checkChangeActual_gain ? $ttactual_gain : '';
}

/**
 * @param $groupDatas
 *
 * @return array
 */
function kpi_get_total_percent_congty( $groupDatas ){
	$tabs = ["'Finance'", "'Customer'", "'Operate'", "'Development'"];
	$groupDatasType = array_group_by($groupDatas, 'type');
	$arrTabs = [
		'total_percent' => ['Finance' => 0, 'Customer' => 0, 'Operate' => 0, 'Development' => 0],
		'name' => [
			'Finance' => _TABS_Finance,
			'Customer' => _TABS_Customer,
			'Operate' => _TABS_Operate,
			'Development' => _TABS_Development
		],
	];

	if( !empty( $groupDatasType ) ){
		foreach ( $tabs as $tab ){
			$tab = str_replace("'", '', $tab);
			$groupDatasTypeItem = [];
			if( array_key_exists( $tab, $groupDatasType ) ){
				$groupDatasTypeItem = $groupDatasType[$tab];
			}
			if( !empty( $groupDatasTypeItem ) ){
				foreach ( $groupDatasTypeItem as $key => $item ){
					$ttactual_gain = 0;
					$percentForMonth = 0;
					$id = $item['id'];
					$plan = $item['plan'];
					if( !empty( $item['formulas'] ) ){
						$formulas = maybe_unserialize( $item['formulas'] );
					}else{
						$formulas = [];
					}
					$ttactual_gain = get_actual_gain( $item, 'no' );
					if( $ttactual_gain !== '' && $item['unit'] != KPI_UNIT_THOI_GIAN ):
						$percentForMonth = getPercentForMonth( $item['unit'], $item['formula_type'], $formulas, $plan, $ttactual_gain );
						$percentForMonth = $percentForMonth * $item['percent'] / 100;
					elseif( $item['unit'] == KPI_UNIT_THOI_GIAN ):
						$percentForMonth = $ttactual_gain * $item['percent'] / 100;
					endif;
					#$keyTab = strtolower( $tab );
					$arrTabs['total_percent'][$tab] += $percentForMonth ;//* ( !empty($getYear) ? $getYear[$keyTab] : 0 ) / 100;
				}
			}
		}
	}

	return $arrTabs;
}

/**
 * @param $groupDatas
 * @param string $influence
 *
 * @return array
 */
function kpi_get_total_percent( $groupDatas, $influence = '' ){
	$tabs = ["'Finance'", "'Customer'", "'Operate'", "'Development'"];
    $groupDatasType = array_group_by($groupDatas, 'type');
    $arrTabs = [
        'total_percent' => ['Finance' => 0, 'Customer' => 0, 'Operate' => 0, 'Development' => 0],
        'name' => [
	        'Finance' => _TABS_Finance,
	        'Customer' => _TABS_Customer,
	        'Operate' => _TABS_Operate,
	        'Development' => _TABS_Development
        ],
    ];

    if( !empty( $groupDatasType ) ){
        foreach ( $tabs as $tab ){
            $tab = str_replace("'", '', $tab);
            $groupDatasTypeItem = [];
            if( array_key_exists( $tab, $groupDatasType ) ){
                $groupDatasTypeItem = $groupDatasType[$tab];
            }
            if( !empty( $groupDatasTypeItem ) ){
                foreach ( $groupDatasTypeItem as $key => $item ){
	                $getYear = kpi_get_year_by_id( $item['year_id'] );
                    $ttactual_gain = 0;
                    $percentForMonth = 0;
                    $id = $item['id'];
                    $plan = $item['plan'];
                    if( $plan == '' ){
                    	$plan = $item['plan_for_year'];
                    }
	                if( !empty( $item['formulas'] ) ){
		                $formulas = maybe_unserialize( $item['formulas'] );
	                }else{
		                $formulas = [];
	                }
	                $ttactual_gain = get_actual_gain( $item, $influence );
                    if( $ttactual_gain !== '' && $item['unit'] != KPI_UNIT_THOI_GIAN ):
                        $percentForMonth = getPercentForMonth( $item['unit'], $item['formula_type'], $formulas, $plan, $ttactual_gain );
                        $percentForMonth = $percentForMonth * $item['percent'] / 100;
                    elseif( $item['unit'] == KPI_UNIT_THOI_GIAN ):
						$percentForMonth = $ttactual_gain * $item['percent'] / 100;
                    endif;
	                $keyTab = strtolower( $tab );
	                $arrTabs['total_percent'][$tab] += $percentForMonth * ( !empty($getYear) ? $getYear[$keyTab] : 0 ) / 100;
                }
            }
        }
    }

    return $arrTabs;
}

/**
 * @param $arrTabs
 * @param $arrYears
 *
 * @return ArrayObject
 */
function kpi_get_total_for_congty( $arrTabs, $arrYears ){
	$arrOutput = new ArrayObject($arrTabs);
	if( array_key_exists('total_percent', $arrTabs) ){
		foreach ( $arrTabs['total_percent'] as $k => $item ){
			$percent = array_key_exists($k, $arrYears) ? $arrYears[$k] : 0;
			$arrOutput['total_percent'][$k] = $item * $percent / 100;
		}
	}
	return $arrOutput;
}

/**
 * @param $year
 * @param $userID
 *
 * @return mixed
 */
function kpi_get_list_kpi_of_year_for_user( $year, $userID ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $sql = $wpdb->prepare("
          SELECT k.*  
          FROM {$tableKpi} AS k  
          WHERE k.user_id = %d AND k.create_by_node = 'node_start' AND k.required = 'yes' 
          AND k.year_id IN (SELECT id FROM {$tableKpiYear} AS y WHERE y.year = %d) ", $userID, $year);
        $_results[$__key] = $wpdb->get_row( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function get_kpi_in_ids( $ids ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $sql = "
          SELECT k.* 
          FROM {$tableKpi} AS k 
          WHERE k.id IN ($ids) ";
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}
function capacity_get_list_kpi($year_id, $apply_of, $type, $type_note){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpiCP = "{$prefix}kpis_capacity";
        $tableKpiYear = "{$prefix}kpi_years";
        $sql = $wpdb->prepare("
          SELECT kc.*, p.post_title, p.level_1, p.level_2, p.level_3 
          FROM {$tableKpiCP} AS kc 
          INNER JOIN {$tableKpiYear} AS y ON y.id = kc.year_id 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = kc.bank_id 
          WHERE kc.type_note = %s AND kc.type = %s AND kc.year_id = %d AND y.kpi_type = %s AND kc.apply_of = %s ",
            $type_note, $type, $year_id, 'canhan', $apply_of);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function capacity_get_kpi_year_and_total($year_id, $apply_of, $user_id){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpiCP = "{$prefix}kpis_capacity";
        $tableKpiYear = "{$prefix}kpi_years";
        $sql = $wpdb->prepare("
          SELECT kc.*, 'kpi_nam' AS 'type_of' 
          FROM {$tableKpiCP} AS kc 
          WHERE kc.type = %s AND kc.year_id = %d AND kc.apply_of = %s AND kc.user_id = %d 
          UNION 
          SELECT kc.* , 'kpi_kq' AS 'type_of' 
          FROM {$tableKpiCP} AS kc  
          WHERE kc.type = %s AND kc.year_id = %d AND kc.apply_of = %s AND kc.user_id = %d 
          ", CAPACITY_CA_NAM, $year_id, $apply_of, $user_id,
            CAPACITY_KET_QUA_QUA_TRINH, $year_id, $apply_of, $user_id);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}
function capacity_get_kpi_year_and_total_not_user($year_id, $apply_of, $status = ''){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpiCP = "{$prefix}kpis_capacity";
        $tableKpiYear = "{$prefix}kpi_years";
        $whereStatus = '';
        if( !empty( $status ) ){
        	$whereStatus = $wpdb->prepare(" AND kc.status = %s ", $status);
        }
        $sql = $wpdb->prepare("
          SELECT kc.*, 'kpi_nam' AS 'type_of' 
          FROM {$tableKpiCP} AS kc 
          WHERE kc.type = %s AND kc.year_id = %d AND kc.apply_of = %s AND kc.bank_id = 0 {$whereStatus}
          UNION 
          SELECT kc.* , 'kpi_kq' AS 'type_of' 
          FROM {$tableKpiCP} AS kc  
          WHERE kc.type = %s AND kc.year_id = %d AND kc.apply_of = %s AND kc.bank_id = 0 {$whereStatus}
          ", CAPACITY_CA_NAM, $year_id, $apply_of,
            CAPACITY_KET_QUA_QUA_TRINH, $year_id, $apply_of);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}


function capacity_year_list(){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpiYear = "{$prefix}kpi_years";
        $sql = "SELECT *  
          FROM {$tableKpiYear} AS y 
          WHERE y.kpi_type = 'canhan' AND y.kpi_time = 'nam' ";
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function capacity_get_list_kpi_by_parent( $year, $apply_of, $type, $status, $parent = 0 ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpiCP = "{$prefix}kpis_capacity";
        $tableKpiYear = "{$prefix}kpi_years";
        $sql = $wpdb->prepare("
          SELECT kc.*, p.post_title, p.level_1, p.level_2, p.level_3 
          FROM {$tableKpiCP} AS kc 
          INNER JOIN {$tableKpiYear} AS y ON y.id = kc.year_id
          INNER JOIN {$wpdb->posts} AS p ON p.ID = kc.bank_id 
          WHERE kc.parent = 0 AND kc.type_note = 'chitiet' AND y.year = %d AND y.kpi_type = %s AND kc.type = %s AND kc.status = %s AND kc.apply_of = %s ",
            $year, 'canhan', $type, $status, $apply_of);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}
function capacity_get_list_kpi_by_user( $year, $apply_of, $userID, $status ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpiCP = "{$prefix}kpis_capacity";
        $tableKpiYear = "{$prefix}kpi_years";
        $sql = $wpdb->prepare("
          SELECT kc.*, p.post_title, p.level_1, p.level_2, p.level_3 
          FROM {$tableKpiCP} AS kc 
          INNER JOIN {$tableKpiYear} AS y ON y.id = kc.year_id
          INNER JOIN {$wpdb->posts} AS p ON p.ID = kc.bank_id 
          WHERE kc.user_id = %d AND kc.type_note = 'chitiet' AND kc.year_id = %d AND y.kpi_type = %s AND kc.status = %s AND kc.apply_of = %s ",
            $userID, $year, 'canhan', $status, $apply_of);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function capacity_get_kpi_and_bank_by_id($id){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpiCP = "{$prefix}kpis_capacity";
        $sql = $wpdb->prepare("
          SELECT kc.*, p.post_title, p.level_1, p.level_2, p.level_3  
          FROM {$tableKpiCP} AS kc 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = kc.bank_id 
          WHERE kc.id = %d ", $id);
        $_results[$__key] = $wpdb->get_row( $sql, ARRAY_A );
    }
    return $_results[$__key];
}
function capacity_get_kpi_by_id( $id ){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpiCP = "{$prefix}kpis_capacity";
        $sql = $wpdb->prepare("
          SELECT kc.*  
          FROM {$tableKpiCP} AS kc  
          WHERE kc.id = %d ", $id);
        $_results[$__key] = $wpdb->get_row( $sql, ARRAY_A );
    }
    return $_results[$__key];
}
function capacity_delete_by_id($id){
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableKpiCP = "{$prefix}kpis_capacity";
    $wpdb->delete( $tableKpiCP, ['id' => $id] );
    if( !empty( $wpdb->last_error ) ){
        return false;
    }
    return true;
}

function capacity_get_nx_by_user( $userID, $yearID ){
	global $wpdb;
	static $_results = [];
	$__key = md5(serialize(func_get_args()));
	if (!isset($_results[$__key])) {
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpiCP = "{$prefix}kpis_capacity";
		$sql = $wpdb->prepare("
          SELECT kc.*  
          FROM {$tableKpiCP} AS kc  
          WHERE kc.type_note = 'nhanxetchung' AND kc.user_id = %d AND kc.year_id = %d", $userID, $yearID);
		$_results[$__key] = $wpdb->get_row( $sql, ARRAY_A );
	}
	return $_results[$__key];
}

function kpi_get_kpi_of_company(  ){
	global $wpdb;
	static $_results = [];
	$__key = md5( 'congty' );
	$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
	$tableKpi = "{$prefix}kpis";
	$tableKpiYear = "{$prefix}kpi_years";
	$tableChart = "{$prefix}org_charts";
	if( !isset( $_results[$__key] ) ){
		#AND k.required = 'no'
		$sql = "SELECT k.*, y.finance, y.customer, y.operate, y.development, y.kpi_time
               FROM {$tableKpi} AS k 
               INNER JOIN {$tableKpiYear} AS y ON y.id = k.year_id 
               WHERE y.kpi_type = 'congty' AND k.chart_id IN (
                SELECT id 
                FROM {$tableChart} AS org 
                WHERE org.alias = 'congty' ) 
                ";
		$_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
	}
	return $_results[$__key];
}
function kpi_get_total_percent_by_user( $userID, $chartID, $year, $owner ){
	global $wpdb;
	static $_results = [];
	$__key = md5( serialize( func_get_args() ) );
	$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
	$tableKpi = "{$prefix}kpis";
	$tableKpiYear = "{$prefix}kpi_years";
	if( !isset( $_results[$__key] ) ){
		if( $owner == 'yes' ){
			$whereOwner = " AND k.owner = 'yes'";
		}elseif( $owner == 'no' ){
			$whereOwner = " AND k.owner = 'no'";
		}else{
			$whereOwner = '';
		}
		$sql = $wpdb->prepare("SELECT SUM(k.percent) as total_percent_task 
               FROM {$tableKpi} AS k  
               WHERE k.year_id IN (
               		SELECT id 
               		FROM {$tableKpiYear} AS y 
               		WHERE y.year = %d AND y.chart_id = %d 
               		AND ( y.precious IS NULL OR y.precious = 0 ) AND ( y.month IS NULL OR y.month = 0 ) AND y.kpi_type = 'congty'
               		) 
               	AND k.user_id = %d AND k.chart_id = %d 
               	AND k.required = 'yes' {$whereOwner}
               GROUP BY k.user_id
                ", $year, $chartID, $userID, $chartID);

		$_results[$__key] = $wpdb->get_row( $sql, ARRAY_A );

	}
	return $_results[$__key];
}

function kpi_get_list_if_not_and_get_from_parent_not_room( $type, $year_parent, $chartID, $userID ){
	global $wpdb;
	static $_results = [];
	$__key = md5(serialize(func_get_args()));
	if (!isset($_results[$__key])) {
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpi = "{$prefix}kpis";
		$tableKpiYear = "{$prefix}kpi_years";
		$tableKpiFormulas = "{$prefix}kpi_formulas";
		$sql = $wpdb->prepare( "SELECT k.*, k2.plan AS plan_for_year, p.post_title, y2.month, y2.precious  ".
		                       "FROM {$tableKpi} AS k ".
		                       "INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id ".
		                       "INNER JOIN $tableKpiYear AS y2 ON y2.id = k.year_id ".
		                       "LEFT JOIN {$tableKpi} AS k2 ON k2.id = k.parent " .
		                       "WHERE k.type = %s AND k.status != %s AND k.user_id = %d AND k.required='yes' ".
		                       "AND k.year_id IN (SELECT id FROM {$tableKpiYear} AS y WHERE y.parent = %d )
		                       ",
			$type, KPI_STATUS_DRAFT, $userID, $year_parent );
		#echo $sql . '<br>';
		$_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
	}
	return $_results[$__key];
}

function get_total_percent_kpi_by_phongban( $userID, $chartID, $year, $type, $owner = 'no' ){
	global $wpdb;
	static $_results = [];
	$__key = md5(serialize(func_get_args()));
	if (!isset($_results[$__key])) {
		$prefix       = $wpdb->get_blog_prefix( get_current_blog_id() );
		$tableKpi     = "{$prefix}kpis";
		$tableKpiYear = "{$prefix}kpi_years";
		$whereOwner = '';
		if( !empty( $owner ) ){
			$whereOwner = $wpdb->prepare("k.owner = %s AND ", $owner);
		}
		$sql = $wpdb->prepare("
			SELECT SUM(k.percent) AS total_percent  
			FROM {$tableKpi} AS k
			INNER JOIN {$tableKpiYear} AS y ON y.id = k.year_id 
			INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id 
			WHERE {$whereOwner} k.type = %s AND k.user_id = %d
			AND k.year_id IN (	SELECT y2.id 
								FROM {$tableKpiYear} AS y2
								WHERE y2.year = %d AND (y2.precious IS NULL OR y2.precious = 0) AND (y2.month IS NULL OR y2.month = 0) 
							 ) 
		", $type, $userID, $year, $chartID);
		$_results[$__key] = $wpdb->get_row( $sql, ARRAY_A );
	}
	return $_results[$__key];

}
function kpi_get_kpi_by_parent($id) {
	global $wpdb;
	static $_results = [];
	$__key = $id;
	if (!isset($_results[$__key])) {
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpi = "{$prefix}kpis";
		$_results[$__key] = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$tableKpi} as k WHERE k.parent = %d", $id), ARRAY_A);
	}
	return $_results[$__key];
}
function kpi_get_kpi_by_bank( $id ){
	global $wpdb;
	static $_results = [];
	$__key = $id;
	if (!isset($_results[$__key])) {
		$prefix       = $wpdb->get_blog_prefix( get_current_blog_id() );
		$tableKpi     = "{$prefix}kpis";
		$sql = $wpdb->prepare("SELECT k.id FROM {$tableKpi} AS k WHERE k.bank_id IN (%d) ", $id);
		$_results[$__key] = $wpdb->get_results($sql);
	}
	return $_results[$__key];
}
function kpi_get_kpi_by_chart_id( $charID, $year, $type ) {
	global $wpdb;
	static $_results = [];
	$__key = md5( serialize( func_get_args() ) );
	if (!isset($_results[$__key])) {
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpi = "{$prefix}kpis";
		$tableKpiYear = "{$prefix}kpi_years";
		$tableChart = "{$prefix}org_charts";
		$sql = $wpdb->prepare("
				SELECT k.*, p.post_title  
				FROM {$tableKpi} as k 
				INNER JOIN {$tableKpiYear} AS y ON y.id = k.year_id 
				INNER JOIN {$wpdb->posts} AS p ON p.ID = k.bank_id 
				WHERE k.chart_id = %d AND k.owner = 'no'
				AND k.type = %s  
				AND k.create_by_node = 'node_start' 
				AND k.required = 'yes' AND y.year = %d 
				AND ( y.precious IS NULL OR y.precious = 0 ) 
				AND ( y.month IS NULL OR y.month = 0 ) 
				", $charID, $type, $year);
		$_results[$__key] = $wpdb->get_results($sql, ARRAY_A);
	}
	return $_results[$__key];
}

function kpi_check_approve_all_by_parent( $kpiID ){
	global $wpdb;
	static $_results = [];
	$__key = md5( serialize( func_get_args() ) );
	if (!isset($_results[$__key])) {
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpi = "{$prefix}kpis";
		$sql = $wpdb->prepare("
				SELECT k.*   
				FROM {$tableKpi} as k 
				WHERE k.parent = %d AND k.percent != '' AND k.plan != ''   
				", $kpiID);
		$_results[$__key] = $wpdb->get_results($sql, ARRAY_A);
	}
	return $_results[$__key];
}

function kpi_get_kpi_by_month_and_year($year, $month){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        #$tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("SELECT k.*  
          FROM {$tableKpi} AS k 
          WHERE k.year_id IN (
              SELECT y.id 
              FROM {$tableKpiYear} AS y 
              WHERE y.year = %d AND y.month = %d ) "
            , $year, $month);
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}

function kpi_get_kpi_and_year_by_kpi_id($id) {
    global $wpdb;
    static $_results = [];
    $__key = $id;
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $_results[$__key] = $wpdb->get_row($wpdb->prepare("
            SELECT k.*, y.kpi_time 
            FROM {$tableKpi} as k 
            INNER JOIN {$tableKpiYear} AS y ON y.id = k.year_id 
            WHERE k.id = %d", $id), ARRAY_A);
    }
    return $_results[$__key];
}

function kpi_check_kpi_approved_by_year($kpi_id, $year_id){
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if( !isset( $_results[$__key] ) ) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $sql = $wpdb->prepare("SELECT k.*  
          FROM {$tableKpi} AS k 
          WHERE k.parent = %d 
                AND k.status IN('".KPI_STATUS_RESULT."', '".KPI_STATUS_WAITING."') 
                AND k.year_id IN (
                  SELECT y.id 
                  FROM {$tableKpiYear} AS y 
                  WHERE y.parent = %d AND (y.precious != NULL OR y.precious != 0 OR y.month != NULL OR y.month != 0) ) "
                , $kpi_id, $year_id);
        #echo $sql."<br>";
        $_results[$__key] = $wpdb->get_results( $sql, ARRAY_A );
    }
    return $_results[$__key];
}