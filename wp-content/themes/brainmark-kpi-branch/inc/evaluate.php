<?php
function get_reported_users($rid, $res, $effa) {
    global $wpdb;

    $query = "SELECT * FROM {$wpdb->prefix}users u
        JOIN {$wpdb->prefix}org_charts o
        ON u.orgchart_id = o.id AND o.parent != 0";

    if ( !empty($rid) ) {
        $query .= $wpdb->prepare(" WHERE  o.id = %d OR o.parent = %d", $rid, $rid);
    }

    $users = $wpdb->get_results($query);

    $settings_website = get_option('settings_website');
    $standard_benchmark = $settings_website['standard_benchmark'];

    foreach ($users as $key => $user) {
        if ( !empty($effa) ) {
           $assign_orgchart = get_assign_orgchart($user, $effa);

           if ( empty($assign_orgchart) ) {
               unset($users[$key]);
           }
           else {
               $score = $assign_orgchart['important'] * $assign_orgchart['manager_lv0'];
               $standard = $assign_orgchart['important'] * $assign_orgchart['standard'];

               $user->total_percent = $standard == 0 ? 0 : round($score / $standard * 100, 0);

               if ($res == 2 &&  $user->total_percent >= 100) {
                   unset($users[$key]);
               }
               elseif ($res == 1 &&  $user->total_percent < 100) {
                   unset($users[$key]);
               }
           }

           continue;
        }

        $user->total_percent = 0;

        $assign_orgcharts = get_all_assign_orgcharts($user);

        $parents = array_filter($assign_orgcharts, function($orgchart){
            return (empty($orgchart['post_parent']));
        });

        foreach ($parents as $parent) {
            $children  = array_filter($assign_orgcharts, function($orgchart) use ($parent) {
                return ($orgchart['post_parent'] == $parent['post_id']);
            });

            $total_scores = 0;
            $total_standards = 0;

            foreach ($children as $child) {
                $new_child = get_assign_orgchart($user, $child['post_id']);

                $child = !empty($new_child) ? $new_child : $child;

                $total_scores += $child['important'] * $child['manager_lv0'];
                $total_standards += $child['important'] * $child['standard'];
            }

            $user->total_percent += $total_standards == 0 ? 0 : round($total_scores / $total_standards * $parent['percent'], 0);
        }

        if ($res == 2 &&  $user->total_percent >= $standard_benchmark) {
            unset($users[$key]);
        }
        elseif ($res == 1 &&  $user->total_percent < $standard_benchmark) {
            unset($users[$key]);
        }
    }

    return $users;
}

function get_all_assign_orgcharts($user) {
    global $wpdb;

    $query = "SELECT * FROM {$wpdb->prefix}assign_orgcharts a
        LEFT JOIN {$wpdb->prefix}efficiency_assessment e
        ON e.assign_id = a.id AND e.user_id = 0
        JOIN {$wpdb->prefix}posts p
        ON (p.ID = a.post_id)
        WHERE a.orgchart_id = {$user->orgchart_id}";

    return $wpdb->get_results($query, ARRAY_A);
}

function get_all_rooms(){
    global $wpdb;

    $query = "SELECT * FROM {$wpdb->prefix}org_charts o
        WHERE o.alias = 'phongban'";

    return $wpdb->get_results($query);
}

function get_assign_orgchart($user, $effa) {
    global $wpdb;

    $query = "SELECT * FROM {$wpdb->prefix}assign_orgcharts a
        JOIN {$wpdb->prefix}efficiency_assessment e
        ON e.assign_id = a.id
        WHERE a.orgchart_id = {$user->orgchart_id} AND a.post_id = {$effa} AND e.user_id = {$user->ID}";
    return $wpdb->get_row($query, ARRAY_A);
}
