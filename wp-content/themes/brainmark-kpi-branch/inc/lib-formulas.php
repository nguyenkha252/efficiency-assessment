<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 21/01/2018
 * Time: 16:38
 */

function get_formula_by_id( $id ){
	global $wpdb;
	static $_results = [];
	$__key = md5(serialize(func_get_args()));
	if (!isset($_results[$__key])) {
		$prefix       = $wpdb->get_blog_prefix( get_current_blog_id() );
		#$tableKpi     = "{$prefix}kpis";
		$tableKpiFormulas     = "{$prefix}kpi_formulas";
		#$tableRelationship     = "{$prefix}relationship_posts_kpi_formulas";
		#$tableKpiYear = "{$prefix}kpi_years";
		#$tableChart   = "{$prefix}org_charts";
		$sql = $wpdb->prepare("SELECT * FROM {$tableKpiFormulas} AS fm WHERE fm.id = %d", $id);
		$_results[$__key] = $wpdb->get_row( $sql );
	}
	return $_results[$__key];
}

function get_list_formula()
{
    global $wpdb;
    static $_results = [];
    $__key = md5(serialize(func_get_args()));
    if (!isset($_results[$__key])) {
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpiFormulas = "{$prefix}kpi_formulas";
        $sql = "SELECT * FROM {$tableKpiFormulas} AS fm ORDER BY id DESC";
        $_results[$__key] = $wpdb->get_results($sql);
    }
    return $_results[$__key];
}

function calculator_on_off_by_number($formulas, $plan, $actual_gain){
    $percentForMonth = 0;
    $throwError = '';
    if( !empty( $formulas ) ) {
        foreach ($formulas as $key => $fm) {
            $condition = $fm['condition'];
            $kpi_percent = $fm['value'];
	        $kpi_percent = str_replace("%", '', $kpi_percent);
            if ($condition != '0') {
                if( $plan == 0 ){
                    $print_Error = __('Kế hoạch bằng 0 nên không thể áp dụng công thức này.', TPL_DOMAIN_LANG);
                    echo '<label class="error notification">' . $print_Error . '</label>';
                    return false;
                }else {
                    if (preg_match('/(KH)/', $kpi_percent, $matchKH)) {
                        $fml = preg_replace('/(KH)/', $plan, $kpi_percent);
                    }
                    if (preg_match('/(KQ)/', $kpi_percent, $matchKQ)) {
                        $fml = preg_replace('/(KQ)/', $actual_gain, $fml);
                    }
                }
                $strConditionMonth = "return {$plan} {$condition} {$actual_gain};";
                try {
                    if (!empty($matchKH) || !empty($matchKQ)) {
                        $percentForMonth = eval("return {$fml};");
                    } else {
                        $evalCondition = eval($strConditionMonth);
                        if ($evalCondition) {
                            $percentForMonth = $kpi_percent;
                            break;
                        }
                    }
                } catch (Throwable $throw) {
                	if( $actual_gain != '' ) {
		                $throwError = $throw;
	                }
                }
            } else {
                if( empty($actual_gain) )
                    $actual_gain = 0;
                if (preg_match('/(KH)/', $kpi_percent, $match)) {
                    $fml = preg_replace('/(KH)/', $plan, $kpi_percent);
                }
                if (preg_match('/(KQ)/', $kpi_percent, $match)) {
                    $fml = preg_replace('/(KQ)/', $actual_gain, $fml);
                }
                try {
                    $percentForMonth = eval("return {$fml};");
                } catch (Throwable $throw) {
                    $throwError = $throw;
                }
            }
        }
    }
    if( !empty( $throwError ) ){
        echo '<label class="error notification">' . __('Lỗi quy trình. Vui lòng cập nhật công thức') . '</label>';
        return false;
    }
    return round($percentForMonth, 2);
}
function calculator_oscillate_by_number($formulas, $plan, $actual_gain){
    $percentForMonth = 0;
    $throwError = '';
    if( !empty( $formulas ) ) {
        foreach ($formulas as $key => $fm) {
            $condition = $fm['condition'];
            $kpi_percent = $fm['value'];
	        $kpi_percent = str_replace("%", '', $kpi_percent);
            $oscillate = !empty($fm['oscillate']) ? $fm['oscillate'] : 0;
            $strCondition = "return {$plan} {$condition} ({$actual_gain} + {$oscillate});";
            try {
                $evalCondition = eval($strCondition);
                if ($evalCondition) {
                    $percentForMonth = $kpi_percent;
                    break;
                }
            } catch (Throwable $throw) {
                $throwError = $throw;
                #echo '<label class="error notification">' . __('Lỗi quy trình. Vui lòng cập nhật công thức') . '</label>';
            }

        }
    }
    if( !empty( $throwError ) ){
        echo '<label class="error notification">' . __('Lỗi quy trình. Vui lòng cập nhật công thức') . '</label>';
        return false;
    }
    return round($percentForMonth, 2);
}
function calculator_add_max_kq_by_number($formulas, $plan, $actual_gain){
    $percentForMonth = 0;
    $throwError = '';
    if( !empty( $formulas ) ) {
        foreach ($formulas as $key => $fm) {
            $condition = $fm['condition'];
            $fml = $fm['value'];#cong thuc
	        $fml = str_replace("%", '', $fml);
            $strCondition = "return {$plan} {$condition} {$actual_gain};";
            $max = $fm['max'];
            try {
                $evalCondition = eval($strCondition);
                if ($evalCondition) {
                    if (preg_match('/(KH)/', $fml, $match)) {
                        $fml = preg_replace('/(KH)/', $plan, $fml);
                    }
                    if (preg_match('/(KQ)/', $fml, $match)) {
                        $fml = preg_replace('/(KQ)/', $actual_gain, $fml);
                    }
                    $percentForMonth = eval("return {$fml};");
                    if ($percentForMonth > $max) {
                        $percentForMonth = $max;
                    }
                    break;
                }
            } catch (Throwable $throw) {
                $throwError = $throw;
                #echo '<label class="error notification">' . __('Lỗi quy trình. Vui lòng cập nhật công thức') . '</label>';
            }
        }
    }
    if( !empty( $throwError ) ){
        echo '<label class="error notification">' . __('Lỗi quy trình. Vui lòng cập nhật công thức') . '</label>';
        return false;
    }
    return round($percentForMonth, 2);
}
function calculator_add_between_by_number($formulas, $actual_gain)
{
    $percentForMonth = 0;
    $throwError = '';
    if (!empty($formulas)) {
        foreach ($formulas as $key => $fm) {
            $kq_min = $fm['kq_min'];
            $kq_max = $fm['kq_max'];
            $condition_1 = $fm['condition_1'];
            $condition_2 = $fm['condition_2'];
            $kpi_percent = $fm['value'];
	        $kpi_percent = str_replace("%", '', $kpi_percent);

            $strCondition = "return {$kq_min} {$condition_1} {$actual_gain} && {$actual_gain} {$condition_2} {$kq_max};";
            try {
                $evalCondition = eval($strCondition);
                if ($evalCondition) {
                    $percentForMonth = $kpi_percent;
                    break;
                }
            } catch (Throwable $throw) {
                $throwError = $throw;
                #echo '<label class="error notification">' . __('Lỗi quy trình. Vui lòng cập nhật công thức') . '</label>';
            }
        }
    }
    if( !empty( $throwError ) ){
        echo '<label class="error notification">' . __('Lỗi quy trình. Vui lòng cập nhật công thức') . '</label>';
        return false;
    }
    return (int)$percentForMonth;
}

/**
 * @param $unit
 * @param $formula_type
 * @param $formulas
 * @param $plan
 * @param $actual_gain
 * @return bool|float|int
 */
function getPercentForMonth( $unit, $formula_type, $formulas, $plan, $actual_gain ){
    static $__results = [];
    $__key = md5( serialize( func_get_args() ) );
    if( !isset($__results[$__key]) ) {
	    $percentForMonth = 0;
        $__results[$__key] = 0;
        if( empty( $formulas ) ){
            if( $unit == KPI_UNIT_THOI_GIAN ){
                $dateUnit = explode("/", $plan);
                $dateActualAgain = explode("/", $actual_gain);
                if( count( $dateUnit ) > 2 ){
                    #DD/MM/YYYY
                    $dayUnit = $dateUnit[0];
                    $monthUnit = $dateUnit[1];
                    $yearUnit = $dateUnit[2];
                    if( is_numeric( $monthUnit ) && is_numeric( $yearUnit ) ){
                        $planDateFormatYMD = $yearUnit . "/" . $monthUnit . "/" . $dayUnit;
                        $KQDateFormatYMD = $dateActualAgain[2] . "/" . $dateActualAgain[1] . "/" . $dateActualAgain[0];
                        $plan = strtotime($planDateFormatYMD);
                        $actual_gain = strtotime($KQDateFormatYMD);
                    }elseif( is_numeric($monthUnit) && !is_numeric( $yearUnit ) ){
                        $plan = (int)$dateUnit;
                        $actual_gain = (int)$dateActualAgain[0];
                    }
                }else{
                    $plan = (int)$dateUnit;
                    $actual_gain = (int)$dateActualAgain[0];
                }
                return ( $actual_gain >= $plan ) ? 100 : 0;
            }else{
                if( is_numeric($plan) && $plan != 0 ){
                    $percentForMonth = $actual_gain * 100 / $plan;
                }elseif( is_numeric($plan) && $plan == 0 ){
                    return ( $actual_gain >= $plan ) ? 100 : 0;
                }
            }

        }else {
            if ($actual_gain === '')
                return false;
            if ($unit == KPI_UNIT_THOI_GIAN) {
                $dateUnit = explode("/", $plan);
                $dateActualAgain = explode("/", $actual_gain);

                if (count($dateUnit) > 2) {
                    #DD/MM/YYYY
                    $dayUnit = $dateUnit[0];
                    $monthUnit = $dateUnit[1];
                    $yearUnit = $dateUnit[2];
                    $planDateFormatYMD = $yearUnit . "/" . $monthUnit . "/" . $dayUnit;
                    $KQDateFormatYMD = $dateActualAgain[2] . "/" . $dateActualAgain[1] . "/" . $dateActualAgain[0];
                    if (strtotime($planDateFormatYMD)) {
                        $planUnit = strtotime($planDateFormatYMD);
                        $resultActual = strtotime($KQDateFormatYMD);
                    } else {
                        $planUnit = (int)$dayUnit;
                        $resultActual = (int)$dateActualAgain[0];
                    }
                    if ($formula_type == KPI_FORMULA_ON_OFF) {
                        $percentForMonth = calculator_on_off_by_number($formulas, $planUnit, $resultActual);
                    } elseif ($formula_type == KPI_FORMULA_OSCILLATE) {
                        $percentForMonth = calculator_oscillate_by_number($formulas, $planUnit, $resultActual);
                    } elseif ($formula_type == KPI_FORMULA_ADD_MAX_KQ) {
                        $percentForMonth = calculator_add_max_kq_by_number($formulas, $planUnit, $resultActual);
                    } else {
                        $percentForMonth = calculator_add_between_by_number($formulas, $resultActual);
                    }
                } elseif (count($dateUnit) > 1) {
                    #DD/MM
                    $dayUnit = $dateUnit[0];
                    $monthUnit = $dateUnit[1];
                    if (is_numeric($monthUnit)) {

                    } else {
                        $planUnit = (int)$dayUnit;
                        $resultActual = (int)$dateActualAgain[0];
                    }
                    if ($formula_type == KPI_FORMULA_ON_OFF) {
                        $percentForMonth = calculator_on_off_by_number($formulas, $planUnit, $resultActual);
                    } elseif ($formula_type == KPI_FORMULA_OSCILLATE) {
                        $percentForMonth = calculator_oscillate_by_number($formulas, $planUnit, $resultActual);
                    } elseif ($formula_type == KPI_FORMULA_ADD_MAX_KQ) {
                        $percentForMonth = calculator_add_max_kq_by_number($formulas, $planUnit, $resultActual);
                    } else {
                        $percentForMonth = calculator_add_between_by_number($formulas, $resultActual);
                    }
                } else {
                    #DD
                    $dayUnit = (int)$dateUnit[0];
                    $planUnit = (int)$dayUnit;
                    $resultActual = (int)$dateActualAgain[0];
                    if ($formula_type == KPI_FORMULA_ON_OFF) {
                        $percentForMonth = calculator_on_off_by_number($formulas, $planUnit, $resultActual);
                    } elseif ($formula_type == KPI_FORMULA_OSCILLATE) {
                        $percentForMonth = calculator_oscillate_by_number($formulas, $planUnit, $resultActual);
                    } elseif ($formula_type == KPI_FORMULA_ADD_MAX_KQ) {
                        $percentForMonth = calculator_add_max_kq_by_number($formulas, $planUnit, $resultActual);
                    } else {
                        #KPI_FORMULA_ADD_BETWEEN
                        $percentForMonth = calculator_add_between_by_number($formulas, $resultActual);
                    }
                }
            } else {
                if ($formula_type == KPI_FORMULA_ON_OFF) {
                    #KH is plan, KQ is actual_gain
                    $percentForMonth = calculator_on_off_by_number($formulas, $plan, $actual_gain);
                } elseif ($formula_type == KPI_FORMULA_OSCILLATE) {
                    $percentForMonth = calculator_oscillate_by_number($formulas, $plan, $actual_gain);
                } elseif ($formula_type == KPI_FORMULA_ADD_MAX_KQ) {
                    $percentForMonth = calculator_add_max_kq_by_number($formulas, $plan, $actual_gain);
                } else {
                    #KPI_FORMULA_ADD_BETWEEN
                    $percentForMonth = calculator_add_between_by_number($formulas, $actual_gain);
                }
            }
        }
        $__results[$__key] = $percentForMonth;
    }
    return $__results[$__key];
}

function getPercentForMonthNotFormula( $unit, $plan, $actual_gain ){

	$dateUnit = explode("/", $plan);
	$dateActualAgain = explode("/", $actual_gain);
	$planDateFormatYMD = '';
	$KQDateFormatYMD = '';
	if( $unit == KPI_UNIT_THOI_GIAN ){
		if (count($dateUnit) > 2) {
			#DD/MM/YYYY
			$dayUnit           = $dateUnit[0];
			$monthUnit         = $dateUnit[1];
			$yearUnit          = $dateUnit[2];
			$planDateFormatYMD = $yearUnit . "/" . $monthUnit . "/" . $dayUnit;
			$KQDateFormatYMD   = $dateActualAgain[2] . "/" . $dateActualAgain[1] . "/" . $dateActualAgain[0];
		}else{
			$dayUnit = $plan;
		}
		if (strtotime($planDateFormatYMD)) {
			$planUnit = strtotime($planDateFormatYMD);
			$resultActual = strtotime($KQDateFormatYMD);
		} else {
			$planUnit = (int)$dayUnit;
			$resultActual = (int)$dateActualAgain[0];
		}
	}else{
		$resultActual = $actual_gain;
		$dayUnit = $plan;
	}
	if( $dayUnit == 0 ){
		echo '<label class="error notification">' . __('Lỗi quy trình. Vui lòng cập nhật công thức') . '</label>';
		return false;
	}
	$percentForMonth = $resultActual * 100 / $dayUnit;
	return $percentForMonth;
}