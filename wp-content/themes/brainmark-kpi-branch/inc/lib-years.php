<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/9/18
 * Time: 19:13
 */

define('KPI_YEAR_STATUS_PUBLISH', 'publish');
define('KPI_YEAR_STATUS_DRAFT', 'draft');

function kpi_get_year_list($year_parent, $status = '', $chart_id = '', $year = 0) {
    global $wpdb;
    static $results = [];
    $__key = "{$year_parent}_{$status}_{$chart_id}";

    if( !isset($results[$__key]) ) {
        $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
        $tableKpiYear = "{$prefix}kpi_years";
        $swhere = '';
        if( !empty($chart_id) ) {
            $swhere .= $wpdb->prepare(" AND (y.`chart_id` = %d)", $chart_id);
        }
        if( !empty($status) ) {
            $swhere .= $wpdb->prepare(" AND (y.`status` LIKE %s)", $status);
        }
        if( !empty( $year ) ){
        	$swhere .= $wpdb->prepare(" AND y.year = %d", $year);
        }
        $sql = $wpdb->prepare("SELECT y.* FROM {$tableKpiYear} as y ".
            "WHERE (y.`parent` = %d) {$swhere} AND y.kpi_type = 'congty' ORDER BY `year` DESC", $year_parent);

        $results[$__key] = $wpdb->get_results($sql, ARRAY_A);
        # echo '<pre>'; var_dump(__LINE__, __FILE__, $wpdb->last_query, $wpdb->last_error, $GLOBALS['parent_year_id'], $results[$__key]); echo '</pre>'; # exit;
    }

    return $results[$__key];
}

function kpi_get_precious_list($year_parent, $status = '') {
    global $wpdb;
    static $results = [];
    $_key = "{$year_parent}_{$status}";
    if( !isset($results[$_key]) ) {
        $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
        $tableKpiYear = "{$prefix}kpi_years";
        $swhere = '';
        if( !empty($status) ) {
            $swhere = $wpdb->prepare(" AND `status` LIKE %s", $status);
        }
        $results[$_key] = $wpdb->get_results($wpdb->prepare("SELECT * FROM {$tableKpiYear} WHERE parent = %d {$swhere} ORDER BY `precious` DESC", $year_parent), ARRAY_A);
    }
    return $results[$_key];
}

function kpi_get_months_list($year_parent, $status = '') {
    global $wpdb;
    static $results = [];
    $_key = "{$year_parent}_{$status}";
    if( !isset($results[$_key]) ) {
        $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
        $tableKpiYear = "{$prefix}kpi_years";
        $swhere = '';
        if( !empty($status) ) {
            $swhere = $wpdb->prepare(" AND `status` LIKE %s", $status);
        }
        $results[$_key] = $wpdb->get_results($wpdb->prepare("SELECT * FROM {$tableKpiYear} WHERE parent = %d {$swhere} ORDER BY `month` DESC", $year_parent), ARRAY_A);
    }
    return $results[$_key];
}

function kpi_get_yearinfo_by($parentId, $chart_id, $status = '') {
    global $wpdb;
    static $results = [];
    $_key = "{$parentId}_{$chart_id}_{$status}";
    if( !isset($results[$_key]) ) {
        $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
        $tableKpiYear = "{$prefix}kpi_years";
        $swhere = '';
        if( !empty($status) ) {
            $swhere .= $wpdb->prepare(" AND y.`status` LIKE %s", $status);
        }
        $results[$_key] = $wpdb->get_row($wpdb->prepare("SELECT y.* FROM {$tableKpiYear} as y ".
            "WHERE (y.`parent` = %d) AND (y.`chart_id` = %d) {$swhere} ".
            "ORDER BY `month` DESC", $parentId, $chart_id), ARRAY_A);
    }
    return $results[$_key];
}

function kpi_get_first_year($parentId, $status = '', $chart_id = 0, $year_value = 0) {
    static $results = [];
    $user = wp_get_current_user();
    if( !$user->exists() ) {
        return null;
    }
    $orgchart = user_load_orgchart($user);
    $member_role = $orgchart ? $orgchart->role : '';
    if( empty($member_role) ) {
        $member_role = '';
    } else {
        $member_role = strtolower($member_role);
    }
    $member_role = ( ($member_role == 'bgd') || $user->has_cap('level_9') ) ? 'bgd' : '';
    if( empty($chart_id) ) {
        $chart_id = $user->orgchart_id;
    }
    $_key = "_{$parentId}_{$status}_{$member_role}_{$chart_id}";
    if( !isset($results[$_key]) ) {
        $years = kpi_get_year_list($parentId, $status, $chart_id, $year_value);
        if( !empty($years) ) {
            foreach($years as $year) {
                if( !empty($status) ) {
                    if( $status == $year['status'] ) {
                        $results[$_key] = $year;
                        return $results[$_key];
                    }
                } else {
                    $results[$_key] = $year;
                    return $results[$_key];
                }
            }
        } else {
            $results[$_key] = null;
        }
    }
    return $results[$_key];
}

function year_get_first_year(){

}

function kpi_get_first_precious($year_parent, $status = '') {
    static $results = [];
    $_key = "{$year_parent}_{$status}";
    if( !isset($results[$_key]) ) {
        $years = kpi_get_precious_list($year_parent, $status);
        if( !empty($years) ) {
            foreach($years as $year) {
                if( !empty($status) ) {
                    if( $status == $year['status'] ) {
                        $results[$_key] = $year;
                        return $results[$_key];
                    }
                } else {
                    $results[$_key] = $year;
                    return $results[$_key];
                }
            }
        } else {
            $results[$_key] = null;
        }
    }
    return $results[$_key];
}

function kpi_get_first_month($year_parent, $status = '') {
    static $results = [];
    $_key = "{$year_parent}_{$status}";
    if( !isset($results[$_key]) ) {
        $years = kpi_get_months_list($year_parent, $status);
        if( !empty($years) ) {
            foreach($years as $year) {
                if( !empty($status) ) {
                    if( $status == $year['status'] ) {
                        $results[$_key] = $year;
                        return $results[$_key];
                    }
                } else {
                    $results[$_key] = $year;
                    return $results[$_key];
                }
            }
        } else {
            $results[$_key] = null;
        }
    }
    return $results[$_key];
}

function kpi_get_firsts($parentId, $status = '', $year) {
    static $results = [];
    if( !isset($results[$status]) ) {
        $firstYear = kpi_get_first_year($parentId, $status, 0, $year);
        $firstPrecious = kpi_get_first_precious($firstYear ? $firstYear['id'] : -1, $status);
        $firstMonth = kpi_get_first_month($firstPrecious ? $firstPrecious['id'] : -1, $status);
        $results[$status] = ['year' => $firstYear, 'precious' => $firstPrecious, 'month' => $firstMonth];
    }
    return $results[$status];
}

function kpi_get_year_by_id( $id ){
    global $wpdb;
    $id = (int)$id;
    static $results = [];
    if( !isset($results[$id]) ) {
        $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
        $tableKpiYear = "{$prefix}kpi_years";
        $results[$id] = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$tableKpiYear} WHERE id = %d", $id), ARRAY_A);
    }
    return $results[$id];
}
function kpi_get_year_by_ids( $ids ){
    global $wpdb;
    $ids = (int)$ids;
    static $results = [];
    if( !isset($results[$ids]) ) {
        $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
        $tableKpiYear = "{$prefix}kpi_years";
        $results[$ids] = $wpdb->get_row("SELECT * FROM {$tableKpiYear} WHERE id IN ($ids)", ARRAY_A);
    }
    return $results[$ids];
}
function year_get_year_by_year( $year ){
    global $wpdb;
    static $results = [];
    if( !isset($results[$year]) ) {
        $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
        $tableKpiYear = "{$prefix}kpi_years";
        $results[$year] = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$tableKpiYear} AS y WHERE y.year = %d AND y.parent = 0 AND y.kpi_type='congty'", $year), ARRAY_A);
    }
    return $results[$year];
}

function kpi_get_year_by_parent( $id ){
    global $wpdb;
    $id = (int)$id;
    static $results = [];
    if( !isset($results[$id]) ) {
        $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
        $tableKpiYear = "{$prefix}kpi_years";
        $results[$id] = $wpdb->get_results($wpdb->prepare("SELECT * FROM {$tableKpiYear} WHERE parent = %d", $id), ARRAY_A);
    }
    return $results[$id];
}


function kpi_get_year_by_parent_by_chart_month( $id, $chartID ){
	global $wpdb;
	$id = (int)$id;
	static $results = [];
	$_key = md5(serialize( func_get_args() ));
	if( !isset($results[$_key]) ) {
		$prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
		$tableKpiYear = "{$prefix}kpi_years";
		$sql = $wpdb->prepare("SELECT * FROM {$tableKpiYear} AS y WHERE y.parent = %d AND y.chart_id = %d AND (y.month != NULL OR y.month != 0)", $id, $chartID);
		$results[$_key] = $wpdb->get_results($sql, ARRAY_A);
	}
	return $results[$_key];
}
function kpi_get_year_by_parent_by_chart_precious( $id, $chartID ){
	global $wpdb;
	$id = (int)$id;
	static $results = [];
	$_key = md5(serialize( func_get_args() ));
	if( !isset($results[$_key]) ) {
		$prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
		$tableKpiYear = "{$prefix}kpi_years";
		$results[$_key] = $wpdb->get_results($wpdb->prepare("SELECT * FROM {$tableKpiYear} AS y WHERE y.parent = %d AND y.chart_id = %d AND (y.precious != NULL OR y.precious != 0)", $id, $chartID), ARRAY_A);
	}
	return $results[$_key];
}

function kpi_get_year_by_parent_kpi( $id, $chartID = '' ){
    global $wpdb;
    $id = (int)$id;
    static $results = [];
    if( !isset($results[$id]) ) {
        $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
        $tableKpiYear = "{$prefix}kpi_years";
        $tableKpi = "{$prefix}kpis";
        $whereChart = '';
        if( !empty($chartID) ){
        	$whereChart = $wpdb->prepare(" AND k.chart_id = %d ", $chartID);
        }
        $results[$id] = $wpdb->get_results($wpdb->prepare("
              SELECT y.* 
              FROM {$tableKpiYear} AS y
              INNER JOIN {$tableKpi} as k ON k.year_id = y.id
              WHERE k.parent = %d {$whereChart} ", $id), ARRAY_A);
    }
    return $results[$id];
}

function year_get_year_by_chart_id( $id, $year, $year_parent = '' ){
	global $wpdb;
	static $results = [];
	$__key = md5( serialize( func_get_args() ) );
	if( !isset($results[$__key]) ) {
		$prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
		$tableKpiYear = "{$prefix}kpi_years";
		$whereParent = '';
		if( !empty( $year_parent ) ){
			$whereParent = $wpdb->prepare(" y.parent = %d AND ", $year_parent);
		}
		$sql = $wpdb->prepare("
              SELECT y.* 
              FROM {$tableKpiYear} AS y
              WHERE {$whereParent} y.chart_id = %d AND ( y.month IS NULL OR y.month = 0 ) AND ( y.precious IS NULL OR y.precious = 0 ) AND y.year = %d AND y.kpi_type = 'congty'", $id, $year);
		$results[$__key] = $wpdb->get_row($sql, ARRAY_A);
	}
	return $results[$__key];
}


function year_get_year_by_chart_ids( $ids, $year ){
	global $wpdb;
	static $results = [];
	if( !isset($results[$ids]) ) {
		$prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
		$tableKpiYear = "{$prefix}kpi_years";
		$results[$ids] = $wpdb->get_row($wpdb->prepare("
              SELECT y.* 
              FROM {$tableKpiYear} AS y
              WHERE y.chart_id IN ({$ids}) AND ( y.month IS NULL OR y.month = 0 ) AND ( y.precious IS NULL OR y.precious = 0 ) AND y.year = %d AND y.kpi_type = 'congty'", $year), ARRAY_A);
	}
	return $results[$ids];
}

function kpi_get_first_year_capacity($year){
    global $wpdb;
    static $results = [];
    if( !isset($results[$year]) ) {
        $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
        $tableKpiYear = "{$prefix}kpi_years";
        $results[$year] = $wpdb->get_row($wpdb->prepare("
              SELECT y.* 
              FROM {$tableKpiYear} AS y
              WHERE y.year = %d AND y.kpi_type = %s AND y.parent = 0", $year, 'canhan'), ARRAY_A);
    }
    return $results[$year];
}

function kpi_get_list_year_capacity(){
    global $wpdb;
    static $results = [];
    $__key = md5( serialize( func_get_args() ) );
    if( !isset($results[$__key]) ) {
        $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
        $tableKpiYear = "{$prefix}kpi_years";
        $results[$__key] = $wpdb->get_results($wpdb->prepare("
                  SELECT y.* 
                  FROM {$tableKpiYear} AS y
                  WHERE y.kpi_type = %s AND y.parent = 0", 'canhan'), ARRAY_A);
    }
    return $results[$__key];
}

function year_get_year_by_kpi_time($kpi_time){
	global $wpdb;
	static $results = [];
	$__key = md5( serialize( func_get_args() ) );
	if( !isset($results[$__key]) ) {
		$prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
		$tableKpiYear = "{$prefix}kpi_years";
		$results[$__key] = $wpdb->get_results($wpdb->prepare("
                  SELECT y.* 
                  FROM {$tableKpiYear} AS y
                  WHERE y.kpi_type = %s AND y.kpi_time = %s AND y.parent = 0", 'congty', $kpi_time), ARRAY_A);
	}
	return $results[$__key];
}

function year_get_list_year_behavior(){
	global $wpdb;
	static $results = [];
	$__key = md5( serialize( func_get_args() ) );
	if( !isset($results[$__key]) ) {
		$prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
		$tableKpiYear = "{$prefix}kpi_years";
		$results[$__key] = $wpdb->get_results($wpdb->prepare("
                  SELECT y.* 
                  FROM {$tableKpiYear} AS y
                  WHERE y.kpi_type = %s AND y.parent = 0
                  ORDER BY y.year DESC
                  ", 'behavior' ), ARRAY_A);
	}
	return $results[$__key];
}
function year_get_first_year_behavior($year){
	global $wpdb;
	static $results = [];
	if( !isset($results[$year]) ) {
		$prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
		$tableKpiYear = "{$prefix}kpi_years";
		$results[$year] = $wpdb->get_row($wpdb->prepare("
              SELECT y.* 
              FROM {$tableKpiYear} AS y
              WHERE y.year = %d AND y.kpi_type = %s AND y.parent = 0", $year, 'behavior'), ARRAY_A);
	}
	return $results[$year];
}
function year_get_list_year_ceo(){
	global $wpdb;
	$blog_id = get_current_blog_id();
	$prefix = $wpdb->get_blog_prefix( $blog_id );
	$tableKpiYear = "{$prefix}kpi_years";
	$tableChart = "{$prefix}org_charts";
	$_key = 'list_year_ceo';
	static $_result = [];
	if( !isset($_result[$_key]) ){
		$sql = "SELECT * FROM {$tableKpiYear} AS y 
					WHERE y.chart_id IN (SELECT org.id FROM {$tableChart} AS org WHERE org.parent = 0) 
					AND y.parent = 0 AND y.kpi_type = 'congty' 
					ORDER BY y.year DESC";
		$_result[$_key] = $wpdb->get_results( $sql, ARRAY_A );
	}
	return $_result[$_key];
}

function year_get_list_year_ceo_by_year( $year ){
	global $wpdb;
	$blog_id = get_current_blog_id();
	$prefix = $wpdb->get_blog_prefix( $blog_id );
	$tableKpiYear = "{$prefix}kpi_years";
	$tableChart = "{$prefix}org_charts";
	$_key = md5( serialize( func_get_args() ) );
	static $_result = [];
	if( !isset($_result[$_key]) ){
		$sql = $wpdb->prepare("SELECT * FROM {$tableKpiYear} AS y 
					WHERE y.chart_id IN (SELECT org.id FROM {$tableChart} AS org WHERE org.parent = 0) 
					AND y.parent = 0 AND y.year = %d AND y.kpi_type = 'congty' 
					ORDER BY y.year DESC", $year);
		$_result[$_key] = $wpdb->get_row( $sql, ARRAY_A );
	}
	return $_result[$_key];
}

function year_get_firt_year_by_congty( $year ){
	global $wpdb;
	static $results = [];
	$id = 'first_year_by_congty';
	if( !isset($results[$id]) ) {
		$prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
		$tableKpiYear = "{$prefix}kpi_years";
		$sql = $wpdb->prepare("
              SELECT y.* 
              FROM {$tableKpiYear} AS y
              WHERE y.parent = 0 AND ( y.month IS NULL OR y.month = 0 ) AND ( y.precious IS NULL OR y.precious = 0 ) AND y.year = %d AND y.kpi_type = 'congty'", $year);
		$results[$id] = $wpdb->get_row($sql, ARRAY_A);
	}
	return $results[$id];
}

function year_get_firt_year_by_chart_is_congty( $year ){
    global $wpdb;
    static $results = [];
    $id = 'firt_year_by_chart_is_congty';
    if( !isset($results[$id]) ) {
        $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
        $tableKpiYear = "{$prefix}kpi_years";
        $tableChart = "{$prefix}org_charts";
        $sql = $wpdb->prepare("
              SELECT y.* 
              FROM {$tableKpiYear} AS y
              WHERE y.chart_id IN (SELECT c.id FROM {$tableChart} AS c WHERE c.parent = 0 AND c.level = 0 ) AND y.parent = 0 AND ( y.month IS NULL OR y.month = 0 ) AND ( y.precious IS NULL OR y.precious = 0 ) AND y.year = %d AND y.kpi_type = 'congty'", $year);
        $results[$id] = $wpdb->get_row($sql, ARRAY_A);
    }
    return $results[$id];
}

function year_get_year_by_onlevel( $orgchart, $year, $parentYearID ){
	$firstYear = year_get_year_by_chart_id( $orgchart->parent, $year, $parentYearID );
	if( empty( $firstYear ) ){
		$orgchartParent = kpi_get_orgchart_by_id( $orgchart->parent );
		if( !empty( $ortchartParent ) && $orgchartParent->role == 'phongban' ) {
			$firstYear = year_get_year_by_onlevel( $orgchartParent, $year, $parentYearID );
		}
	}
	return $firstYear;
}

function year_get_month_settings( $year_id ){
    global $wpdb;
    static $results = [];
    $id = md5( serialize( func_get_args() ) );
    if( !isset($results[$id]) ) {
        $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
        $tableKpiYear = "{$prefix}kpi_years";
        $sql = $wpdb->prepare("
              SELECT y.* 
              FROM {$tableKpiYear} AS y
              WHERE y.parent = %d AND y.kpi_type = 'settings'", $year_id);
        $results[$id] = $wpdb->get_results($sql, ARRAY_A);
    }
    return $results[$id];
}
