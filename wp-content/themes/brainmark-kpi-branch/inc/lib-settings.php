<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/12/18
 * Time: 16:25
 */

function get_settings_website(){
    static $_results = [];
    $key = 'settings_website';
    if( empty( $_results[$key] ) ) {
        $options = get_option($key, true);
        $default = ['logo_url' => '', 'title_website' => ''];
        $_results[$key] = wp_parse_args($options, $default);
        $blogname = get_option('blogname', '');
        if( empty($_results[$key]['title_website']) ) {
            $_results[$key]['title_website'] = $blogname;
        }
    }
    return $_results[$key];
}