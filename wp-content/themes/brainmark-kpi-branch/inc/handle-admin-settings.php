<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/26/17
 * Time: 23:02
 */


add_action('admin_menu', function () {
    add_menu_page( __( 'KPI Settings', TPL_DOMAIN_LANG), __( 'KPI Settings', TPL_DOMAIN_LANG), 'manage_options',
        'kpi_settings', 'kpi_settings_page' );
});

function kpi_init_db($blog_id = null) {
    global $wpdb;
    $blog_id = $blog_id ? $blog_id : get_current_blog_id();
    $prefix = $wpdb->get_blog_prefix( $blog_id );

    require_once __DIR__ . '/init-after-theme-setup.php';

    # $page = get_page_by_path(PAGE_APPLY_KPI_SYSTEM, OBJECT, 'page');
    # if( $page ) {
    #    update_option('show_on_front', 'page');
    #    update_option('page_on_front', $page->ID);
    # }


    $categories = [
        1 => ['name' => __('KPI', TPL_DOMAIN_LANG), 'slug' => __('kpi', TPL_DOMAIN_LANG), 'parent' => 0, 'description' => '',
            'children' => [
                2 => ['name' => __('KPI Behavior', TPL_DOMAIN_LANG), 'slug' => __('kpi-behavior', TPL_DOMAIN_LANG), 'parent' => 1, 'description' => ''],
                3 => ['name' => __('Criteria of Management Capability', TPL_DOMAIN_LANG), 'slug' => __('criteria-of-management-capability', TPL_DOMAIN_LANG), 'parent' => 1, 'description' => ''],
                4 => ['name' => __('Criteria of Employee Capacity', TPL_DOMAIN_LANG), 'slug' => __('criteria-of-employee-capacity', TPL_DOMAIN_LANG), 'parent' => 1, 'description' => ''],
                5 => ['name' => __('KPI By Position System', TPL_DOMAIN_LANG), 'slug' => __('kpi-by-position-system', TPL_DOMAIN_LANG), 'parent' => 1, 'description' => '',
                    'children' => [
                        6 => ['name' => __('General manager', TPL_DOMAIN_LANG), 'slug' => __('general-manager', TPL_DOMAIN_LANG), 'parent' => 5, 'description' => ''],
                        7 => ['name' => __('Deputy General Director Regular', TPL_DOMAIN_LANG), 'slug' => __('deputy-general-director-regular', TPL_DOMAIN_LANG), 'parent' => 5, 'description' => ''],
                        8 => ['name' => __('Deputy General Director', TPL_DOMAIN_LANG), 'slug' => __('deputy-general-director', TPL_DOMAIN_LANG), 'parent' => 5, 'description' => ''],
                        9 => ['name' => __('Finance Division Department', TPL_DOMAIN_LANG), 'slug' => __('finance-division-department', TPL_DOMAIN_LANG), 'parent' => 5, 'description' => '',
                            'children' => [
                                15 => ['name' => __('Manager FD Department', TPL_DOMAIN_LANG), 'slug' => __('manager-fd-department', TPL_DOMAIN_LANG), 'parent' => 9, 'description' => ''],
                                16 => ['name' => __('Deputy FD Department', TPL_DOMAIN_LANG), 'slug' => __('deputy-fd-department', TPL_DOMAIN_LANG), 'parent' => 9, 'description' => ''],
                                17 => ['name' => __('Tax Specialist', TPL_DOMAIN_LANG), 'slug' => __('tax-specialist', TPL_DOMAIN_LANG), 'parent' => 9, 'description' => ''],
                                18 => ['name' => __('Financial Officer', TPL_DOMAIN_LANG), 'slug' => __('financial-officer', TPL_DOMAIN_LANG), 'parent' => 9, 'description' => ''],
                                19 => ['name' => __('Administrative Officer', TPL_DOMAIN_LANG), 'slug' => __('administrative-officer', TPL_DOMAIN_LANG), 'parent' => 9, 'description' => ''],
                            ]
                        ],
                        10 => ['name' => __('Human Resource Department', TPL_DOMAIN_LANG), 'slug' => __('human-resource-department', TPL_DOMAIN_LANG), 'parent' => 5, 'description' => '',
                            'children' => [
                                13 => ['name' => __('Manager HR Department', TPL_DOMAIN_LANG), 'slug' => __('manager-hr-department', TPL_DOMAIN_LANG), 'parent' => 10, 'description' => ''],
                                14 => ['name' => __('Deputy HR Department', TPL_DOMAIN_LANG), 'slug' => __('deputy-hr-department', TPL_DOMAIN_LANG), 'parent' => 10, 'description' => ''],
                                20 => ['name' => __('Specialist recruiting and training', TPL_DOMAIN_LANG), 'slug' => __('specialist-recruiting-and-training', TPL_DOMAIN_LANG), 'parent' => 10, 'description' => ''],
                            ]
                        ],
                        11 => ['name' => __('Project Advisory Office Department', TPL_DOMAIN_LANG), 'slug' => __('project-advisory-office-department', TPL_DOMAIN_LANG), 'parent' => 5, 'description' => ''],
                        12 => ['name' => __('Customer Care Department', TPL_DOMAIN_LANG), 'slug' => __('customer-care-department', TPL_DOMAIN_LANG), 'parent' => 5, 'description' => ''],
                    ]
                ]
            ],
        ],
        21 => ['name' => __('Notification', TPL_DOMAIN_LANG), 'slug' => __('notification', TPL_DOMAIN_LANG), 'parent' => 0, 'description' => '', 'children' => []],
        22 => ['name' => __('Loại Mục Tiêu', TPL_DOMAIN_LANG), 'slug' => __('loai-muc-tieu', TPL_DOMAIN_LANG), 'parent' => 0, 'description' => '',
            'children' => [
                ['name' => __('Tài Chính', TPL_DOMAIN_LANG), 'slug' => __('finance', TPL_DOMAIN_LANG), 'parent' => 22, 'description' => '', 'children' => []],
                ['name' => __('Khách Hàng', TPL_DOMAIN_LANG), 'slug' => __('customer', TPL_DOMAIN_LANG), 'parent' => 22, 'description' => '', 'children' => []],
                ['name' => __('Vận Hành', TPL_DOMAIN_LANG), 'slug' => __('operate', TPL_DOMAIN_LANG), 'parent' => 22, 'description' => '', 'children' => []],
                ['name' => __('Học Hỏi & Phát Triển', TPL_DOMAIN_LANG), 'slug' => __('development', TPL_DOMAIN_LANG), 'parent' => 22, 'description' => '', 'children' => []],
            ]
        ]
    ];

    add_categories($categories, 0);

    return add_more_tables($blog_id);
}

function add_more_tables($blog_id = null) {
    global $wpdb;

    $blog_id = $blog_id ? $blog_id : get_current_blog_id();
    $prefix = $wpdb->get_blog_prefix($blog_id);


    $wpdb->query("START TRANSACTION;");

    $sql = "CREATE TABLE IF NOT EXISTS `{$prefix}login_logs`(
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT(20) UNSIGNED NOT NULL,
  `action` ENUM('loggedin', 'logout', 'expired') CHARACTER SET utf8 NOT NULL,
  `at_time` datetime NOT NULL,
  `browser` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";


    $wpdb->query($sql);
    if($wpdb->last_error != '') {
        $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
        $wpdb->query("rollback;");
        return $error;
    }
    $wpdb->query("COMMIT;");

    $wpdb->query("START TRANSACTION;");
    $wpdb->query("CREATE TABLE IF NOT EXISTS `{$prefix}org_charts`(
      `id` bigint(20) NOT NULL AUTO_INCREMENT,
      `name` varchar(64) CHARACTER SET utf8 NOT NULL,
      `room` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
      `parent` bigint(20) DEFAULT NULL,
      `alias` enum('tapdoan','congty','phongban','nhanvien') DEFAULT NULL,
      `has_kpi` tinyint(4) DEFAULT 0,
      `level` int(10) UNSIGNED DEFAULT 0,
      PRIMARY KEY (`id`),
      UNIQUE KEY `unique_row` (`name`,`parent`),
      KEY `name` (`name`) USING BTREE,
      KEY `room` (`room`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
    );

    if($wpdb->last_error != '') {
        $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
        $wpdb->query("rollback;");
        return $error;
    }

    $row = $wpdb->get_row("SHOW COLUMNS FROM `{$prefix}users` WHERE `Field` LIKE 'orgchart_id'");
    if( empty( $row ) ) {
        $wpdb->query("ALTER TABLE `{$prefix}users` 
        ADD `orgchart_id` BIGINT NULL DEFAULT NULL AFTER `display_name`,
        ADD `role_id` BIGINT UNSIGNED NULL AFTER `orgchart_id`;");
    }

    $row = $wpdb->get_row("SHOW COLUMNS FROM `{$prefix}users` WHERE `Field` LIKE 'role_id'");
    if( empty( $row ) ) {
        $wpdb->query("ALTER TABLE `{$prefix}users` ADD `role_id` BIGINT UNSIGNED NULL AFTER `orgchart_id`;");
    }

    $row = $wpdb->get_row("SHOW COLUMNS FROM `{$prefix}users` WHERE `Field` LIKE 'spam'");
    if( empty( $row ) ) {
        $wpdb->query("ALTER TABLE `{$prefix}users` ADD `spam` tinyint(2) NOT NULL DEFAULT '0';");
    }

    $row = $wpdb->get_row("SHOW COLUMNS FROM `{$prefix}users` WHERE `Field` LIKE 'deleted'");
    if( empty( $row ) ) {
        $wpdb->query("ALTER TABLE `{$prefix}users` ADD `deleted` tinyint(2) NOT NULL DEFAULT '0';");
    }

    if($wpdb->last_error != '') {
        $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
        $wpdb->query("rollback;");
        return $error;
    }
    $wpdb->query("COMMIT;");


    $wpdb->query("START TRANSACTION;");

    $sql = "CREATE TABLE IF NOT EXISTS `{$prefix}kpis`(
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `year_id` BIGINT(20) UNSIGNED NOT NULL,
  `bank_id` BIGINT(20) NOT NULL COMMENT 'Post ID',
  `plan` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `unit` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `actual_gain` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status_result` enum('draft','publish') CHARACTER SET utf8 NOT NULL,
  `receive` datetime NOT NULL,
  `percent` double NOT NULL,
  `parent` bigint(20) UNSIGNED DEFAULT NULL,
  `chart_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `aproved` int(11) NOT NULL,
  `type` enum('Finance','Customer','Operate','Development','') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` enum('luu-nhap','cho-duyet-dang-ky','ket-qua-kpi','cho-duyet-kq-kpi','hoan-tat') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `required` enum('yes','no') NOT NULL,
  `owner` enum('no','yes') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_by_node` enum('node_start','node_middle','node_end') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created` datetime NOT NULL,
  `files` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `influence` ENUM('no','yes') NOT NULL COMMENT 'Mục tiêu ảnh hưởng',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_row` (`year_id`,`bank_id`,`chart_id`,`user_id`,`parent`,`type`) USING BTREE,
  KEY `required` (`required`),
  KEY `plan` (`plan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

    $wpdb->query($sql);
    if($wpdb->last_error != '') {
        $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
        $wpdb->query("rollback;");
        return $error;
    }
    $wpdb->query("INSERT INTO `{$prefix}kpis` (`id`, `name`, `room`, `parent`, `alias`, `has_kpi`, `level`) VALUES ('1', 'Root', 'Công Ty', '0', 'congty', '0', '0');");
    $wpdb->query("COMMIT;");

    $wpdb->query("START TRANSACTION;");
    $sql = "CREATE TABLE IF NOT EXISTS `{$prefix}kpi_years` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL,
  `precious` tinyint(4) DEFAULT NULL,
  `month` tinyint(4) DEFAULT NULL,
  `behavior_percent` DOUBLE UNSIGNED NOT NULL DEFAULT '0',
  `apply_for` TEXT NULL,
  `finance` double DEFAULT 0,
  `customer` double DEFAULT 0,
  `operate` double DEFAULT 0,
  `development` double DEFAULT 0,
  `parent` bigint(20) UNSIGNED DEFAULT NULL,
  `chart_id` bigint(20) UNSIGNED NOT NULL,
  `kpi_time` enum('nam','quy','thang') DEFAULT NULL,
  `kpi_type` ENUM('congty','canhan','behavior','settings') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` enum('draft','publish') DEFAULT NULL,
  `date_expired` DATETIME NULL DEFAULT '0000-00-00 00:00:00,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_row` (`year`,`precious`,`month`,`parent`,`chart_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";


    $wpdb->query($sql);
    if($wpdb->last_error != '') {
        $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
        $wpdb->query("rollback;");
        return $error;
    }
    $wpdb->query("COMMIT;");

/*
    $wpdb->query("START TRANSACTION;");
    $wpdb->query("CREATE TABLE IF NOT EXISTS `{$prefix}kpis_meta`(
`id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT, 
`kpi_id` BIGINT UNSIGNED NOT NULL DEFAULT '0',
`meta_key` VARCHAR(255) NULL DEFAULT NULL,
`meta_value` LONGTEXT NULL DEFAULT NULL,
UNIQUE `unique_row` (`kpi_id`, `meta_key`),
PRIMARY KEY (`id`)) ENGINE = InnoDB DEFAULT CHARSET=utf8;");

    if($wpdb->last_error != '') {
        $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
        $wpdb->query("rollback;");
        return $error;
    }
    $wpdb->query("COMMIT;");
*/

    $wpdb->query("START TRANSACTION;");
    $wpdb->query("CREATE TABLE IF NOT EXISTS `{$prefix}kpis_capacity`(
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `year_id` BIGINT UNSIGNED NOT NULL,
  `bank_id` BIGINT UNSIGNED NOT NULL,
  `apply_of` ENUM('quan-ly','nhan-vien') NOT NULL,
  `actual_gain` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `plan` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `percent` DOUBLE NOT NULL,
  `parent` BIGINT UNSIGNED NOT NULL DEFAULT '0',
  `chart_id` BIGINT UNSIGNED NOT NULL,
  `user_id` BIGINT UNSIGNED NOT NULL,
  `note` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `type_note` ENUM('chitiet','nhanxetchung') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trend` ENUM('di-xuong','on-dinh','di-len') NULL DEFAULT NULL COMMENT 'Xu hướng',
  `type` ENUM('kpi-ca-nam','tieu-chi-nang-luc','ket-qua-qua-trinh','') NOT NULL,
  `type_kpis` ENUM('','cong-ty','phong-ban') NOT NULL,
  `status` ENUM('luu-nhap','cho-trien-khai','ket-qua-kpi','hoan-tat') NOT NULL,
  `modified` DATETIME NOT NULL,
  PRIMARY KEY (`id`)) ENGINE = InnoDB DEFAULT CHARSET=utf8;");

    if($wpdb->last_error != '') {
        $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
        $wpdb->query("rollback;");
        return $error;
    }
    $wpdb->query("COMMIT;");


    $wpdb->query("START TRANSACTION;");

    $row = $wpdb->get_row("SHOW COLUMNS FROM `{$prefix}users` WHERE `Field` LIKE 'formula_id'");
    if( empty( $row ) ) {
        $wpdb->query("ALTER TABLE `{$prefix}posts` 
          ADD `formula_id` BIGINT(20) UNSIGNED NOT NULL AFTER `ID`,
          ADD `level_1` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Cần cải thiện' AFTER `formula_id`,
          ADD `level_2` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Hiệu quả' AFTER `level_1`, 
          ADD `level_3` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Nổi bật' AFTER `level_2`,
          ADD `violation_assessment` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `level_3`,
          ADD `chart_id` BIGINT NULL AFTER `violation_assessment`;
          ");
        if($wpdb->last_error != '') {
            $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
            $wpdb->query("rollback;");
            return $error;
        }
    }
    $wpdb->query("COMMIT;");


    $wpdb->query("START TRANSACTION;");
    $wpdb->query("CREATE TABLE IF NOT EXISTS `{$prefix}behavior`(
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `year_id` BIGINT UNSIGNED NOT NULL,
  `bank_id` BIGINT UNSIGNED NOT NULL,
  `number` DOUBLE NOT NULL DEFAULT '0' COMMENT 'Số lần vi phạm',
  `parent_1` BIGINT UNSIGNED NOT NULL COMMENT 'Thái độ hành vi của admin tạo',
  `parent` BIGINT UNSIGNED NOT NULL COMMENT 'Cấp con của Group Thái độ hành vi',
  `chart_id` BIGINT UNSIGNED NOT NULL,
  `user_id` BIGINT UNSIGNED NOT NULL,
  `object_user` LONGTEXT NOT NULL COMMENT 'Khi admin thay đổi sẽ lưu tất cả user vào record này để từng user có thể update lại thái độ hành vi',
  `status` ENUM('luu-nhap','ket-qua-kpi','cho-duyet-kq-kpi','hoan-tat') NOT NULL,
  `is_update` BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'Bật cờ khi admin update nếu tđhv đã triển khai',
  `created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)) ENGINE = InnoDB DEFAULT CHARSET=utf8;");

    if($wpdb->last_error != '') {
        $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
        $wpdb->query("rollback;");
        return $error;
    }
    $wpdb->query("COMMIT;");

    $wpdb->query("START TRANSACTION;");
    $wpdb->query("CREATE TABLE IF NOT EXISTS `{$prefix}kpi_formulas` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `plan` varchar(255) NOT NULL,
  `unit` text NOT NULL,
  `type` text NOT NULL,
  `formulas` longtext NOT NULL,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

    if($wpdb->last_error != '') {
        $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
        $wpdb->query("rollback;");
        return $error;
    }
    $wpdb->query("COMMIT;");

    $wpdb->query("START TRANSACTION;");
    $wpdb->query("CREATE TABLE IF NOT EXISTS `{$prefix}relationship_posts_kpi_formulas` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) NOT NULL,
  `kpi_formula_id` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`,`post_id`,`kpi_formula_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

    if($wpdb->last_error != '') {
        $error = new WP_Error("dberror", __("Database query error"), $wpdb->last_error);
        $wpdb->query("rollback;");
        return $error;
    }
    $wpdb->query("COMMIT;");
    return;
}

add_filter('wpmu_drop_tables', function($tables, $blog_id) {
    if( $blog_id > 1 ) {
        global $wpdb;
        $prefix = $wpdb->get_blog_prefix($blog_id);
        $tables[] = "{$prefix}kpis";
        $tables[] = "{$prefix}kpi_years";
        $tables[] = "{$prefix}behavior";
        $tables[] = "{$prefix}org_charts";
        $tables[] = "{$prefix}kpis_capacity";
        $tables[] = "{$prefix}kpi_formulas";
        $tables[] = "{$prefix}relationship_posts_kpi_formulas";
    }
    return $tables;
}, 100, 2);

function add_categories($categories, $catParentId = 0) {
    $taxonomy = 'category';
    foreach($categories as $idx => &$args) {
        $args['parent'] = $catParentId;
        $term = get_term_by('slug', $args['slug'], $taxonomy);

        $result = false;
        $children = [];
        if( isset($args['children']) ) {
            $children = $args['children'];
            unset($args['children']);
        }


        if( $term === false ) {
            $result = wp_insert_term($args['name'], $taxonomy, $args);
        } else if( $term instanceof WP_Term ) {
            $result = wp_update_term($term->term_id, $taxonomy, $args);
        }
        if( $result && !is_wp_error($result) ) {
            $args['term_id'] = $result['term_id'];
            $args['term_taxonomy_id'] = $result['term_taxonomy_id'];
            add_categories($children, $args['term_id']);
        }
    }
}

# newblog_notify_siteadmin();
/*
add_action('network_site_new_created_user', function($user_id) {

}); */

add_action('wpmu_new_blog', function($blog_id, $user_id, $domain, $path, $site_id, $meta) {
    global $wpdb;
    $error = kpi_init_db($blog_id);
    if( is_wp_error($error) ) {
        error_log($error->get_error_message());
        error_log($error->get_error_data());
    }
    if( !empty($wpdb->last_error) ) {
        error_log($wpdb->last_error);
    }
    $prefix = $wpdb->get_blog_prefix( $blog_id );
    $wpdb->query( $wpdb->prepare("UPDATE `{$prefix}users` SET `orgchart_id` = 1 WHERE `ID` = %d;", 1) );

    $id = $wpdb->get_var("SELECT `id` FROM `{$prefix}org_charts` WHERE `id` = 1");
    if( !$id ) {
        $wpdb->query("INSERT INTO `{$prefix}org_charts`(`id`, `name`, `room`, `parent`, `alias`, `has_kpi`, `level`) VALUES ('1', 'Root', 'Công Ty', '0', 'congty', '0', '0');");
    }
    return;
}, 101, 6);

add_action('admin_init', function() {
    $taxonomy_per_page = function($tags_per_page) {
        return 1000;
    };
    $screen = get_current_screen();

    if( is_object($screen) ) {
        add_filter('edit_' . $screen->taxonomy . '_per_page', $taxonomy_per_page);
    }

    add_filter('edit_tags_per_page', $taxonomy_per_page);
    add_filter('edit_categories_per_page', $taxonomy_per_page);
});

function kpi_settings_page() {
    global $wpdb;
    $messages = [];
    if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
        $wpnonce = $_POST['_wpnonce'];
        if( wp_verify_nonce($wpnonce, 'save_settings') ) {
            if( !empty($_POST['init-pages']) ) {
                require_once __DIR__ . '/init-after-theme-setup.php';
            }

            if( !empty($_POST['init-user']) ) {
                $blog_id = get_current_blog_id();

                # $prefix = $wpdb->get_blog_prefix( 1 );
                # $wpdb->query( $wpdb->prepare("UPDATE `{$prefix}users` SET `user_pass` = %s WHERE `ID` = %d;", $user_pass, $user_id) );

                $prefix = $wpdb->get_blog_prefix( $blog_id );
                $id = $wpdb->get_var( "SELECT `ID` FROM `{$prefix}users` WHERE `ID` = 1" );
                if( !$id ) {
                    $wpdb->query( $wpdb->prepare("UPDATE `{$prefix}users` SET `orgchart_id` = 1 WHERE `ID` = %d", 1) );
                }
                $id = $wpdb->get_var("SELECT `id` FROM `{$prefix}org_charts` WHERE `id` = 1");
                if( !$id ) {
                    $wpdb->query("INSERT INTO `{$prefix}org_charts`(`id`, `name`, `room`, `parent`, `alias`, `has_kpi`, `level`) VALUES ('1', 'Root', 'Công Ty', '0', 'congty', '0', '0');");
                }
            }

            if( !empty($_POST['init-db']) ) {
                $blog_id = get_current_blog_id();
                $error = kpi_init_db($blog_id);
                if( is_wp_error($error) ) {
                    error_log($error->get_error_message());
                }
                if( !empty($wpdb->last_error) ) {
                    error_log($wpdb->last_error);
                }
            }
            if( !empty($_POST['email-hr-manager']) ){
                $blog_id = get_current_blog_id();

            }
        } else {
            $messages['invalid_nonce'] = __('Invalid nonce', TPL_DOMAIN_LANG);
        }
    }
    kpi_render_pages_settings($messages);
}

function kpi_render_pages_settings($messages) {
    $wpnonce = wp_create_nonce('save_settings');
    $form_url = admin_url('admin.php?page=kpi_settings');

    if( !empty($messages) ) {
        echo '<ul class="messages">';
        foreach($messages as $key => $message) {
            echo sprintf('<li class="message-%s">%s</li>', $key, $message);
        }
        echo '</ul>';
    }
    ?>
    <form action="<?php echo $form_url; ?>" method="POST" enctype="application/x-www-form-urlencoded">
        <input type="hidden" name="action" value="save_settings" />
        <input type="hidden" name="_wpnonce" value="<?php echo $wpnonce; ?>" />
        <div class="list">
            <div class="form-group">
                <label class="control-label" for="init-pages"><?php _e('Init Pages', TPL_DOMAIN_LANG); ?></label>
                <input class="control-input" type="checkbox" name="init-pages" id="init-pages" value="1" onclick="this.value = this.checked ? 1 : 0">
            </div>
            <div class="form-group">
                <label class="control-label" for="init-db">Init Databases</label>
                <input class="control-input" type="checkbox" name="init-db" id="init-db" value="1" onclick="this.value = this.checked ? 1 : 0">
            </div>
            <div class="form-group">
                <label class="control-label" for="init-user">Init User</label>
                <input class="control-input" type="checkbox" name="init-user" id="init-user" value="1" onclick="this.value = this.checked ? 1 : 0">
            </div>
            <?php /*
            <div class="form-group">
                <label for=""></label>
                <input name="" id="" value="">
            </div>
            <div class="form-group">
                <label for=""></label>
                <input name="" id="" value="">
            </div> */ ?>
            <button type="submit"><?php _e('Submit', TPL_DOMAIN_LANG); ?></button>
        </div>
    </form>
<?php
}