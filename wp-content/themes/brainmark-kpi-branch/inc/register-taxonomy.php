<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 15/12/2017
 * Time: 21:55
 */


add_action( 'init', 'kpi_register_taxonomy_post' );
function kpi_register_taxonomy_post(){
	$labelsTypes = array(
		"name" => __( "Type" ),
		"singular_name" => __( "Type" ),
	);

	$argsTypes = array(
		"label" => __( "Type" ),
		"labels" => $labelsTypes,
		"public" => false,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => 'types_of_post',
		"rewrite" => array( 'slug' => 'types-of-post', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "types_of_post",
		"show_in_quick_edit" => false,
		"has_multiple" => false
	);

	$labelsTypesKPI = array(
		"name" => __( "Type KPI" ),
		"singular_name" => __( "Type KPI" ),
	);

	$argsTypesKPI = array(
		"label" => __( "Type KPI" ),
		"labels" => $labelsTypesKPI,
		"public" => false,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => 'types_kpi',
		"rewrite" => array( 'slug' => 'types-kpi', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "types_kpi",
		"show_in_quick_edit" => false,
		"has_multiple" => false
	);

	register_taxonomy( "types_of_post", array( "post"), $argsTypes );

	register_taxonomy( "types_kpi", array( "post"), $argsTypesKPI );

	flush_rewrite_rules();
}
