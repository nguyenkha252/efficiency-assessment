<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/7/18
 * Time: 14:20
 */

$roleAccess = "bgd";
$user = wp_get_current_user();
$orgchart = user_load_orgchart($user);
$member_role = mb_strtolower( $orgchart ? $orgchart->role : '' );

if( !( ($member_role == $roleAccess) || ( empty($user->allcaps['level_9']) && !$user->has_cap('level_9') ) ) ){
    return;
}

require_once THEME_DIR . '/inc/lib-settings.php';
$options = get_settings_website();
?>
<div id="settings">
    <form action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" enctype="multipart/form-data">
    <div class="container">
        <h2 class="title">
            <?php _e( 'Cấu hình website' ,TPL_DOMAIN_LANG); ?>
        </h2>
            <div class="settings-item input-group">
                <label class="control-label"
                       for="avatar"><?php _e('Logo Công ty', TPL_DOMAIN_LANG); ?></label>
                <div class="form-group">
                    <div class="preview-avatar"><img
                            class="preview-avatar-image" onerror="this.src = this.getAttribute('data-src'); this.parentNode.className += ' hidden';"
                            src="<?php echo esc_attr($options['logo_url']); ?>"
                            data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                            style="max-width: 213px; min-height: 1px;">
                        <?php
                        if(isset($options['logo_url']) && !empty($options['logo_url'])){
                            echo "<button style=\"background: red!important;margin-left: 20px;\" type=\"button\" class=\"btn-brb-default btn-primary remove-logo\"><span class=\"glyphicon glyphicon-trash\"></span> ".__('Xóa Logo', TPL_DOMAIN_LANG)."</button>";
                        }
                        ?>
                    </div>

                    <div class="file-loading">
                        <input id="avatar" name="logo_url" type="file" class="file"
                               placeholder="<?php _e('Avatar', TPL_DOMAIN_LANG); ?>" data-show-upload="false">
                    </div>
                </div>
            </div>
            <div class="settings-item input-group">
                <span class="input-group-addon" id=""><?php _e('Tiêu đề', TPL_DOMAIN_LANG); ?></span>
                <input type="text" name="title_website" class="form-control" value="<?php esc_attr_e( $options['title_website'] ); ?>"
                       placeholder="<?php _e('Tiêu đề', TPL_DOMAIN_LANG); ?>" aria-describedby="">
            </div>
            <div class="settings-item input-group">
                <span class="input-group-addon" id=""><?php _e('Màu nền (header)', TPL_DOMAIN_LANG); ?></span>
                <div data-colorpicker="" class="input-group colorpicker-component">
                    <input type="text" name="background_header" class="form-control" value="<?php esc_attr_e( !empty($options['background_header']) ? $options['background_header'] : "#008fd5" ); ?>"
                           placeholder="<?php _e('Màu nền (header)', TPL_DOMAIN_LANG); ?>" aria-describedby=""> <span class="input-group-addon background-eeeeee"><i></i></span>
                </div>
            </div>
            <div class="settings-item input-group">
                <span class="input-group-addon" id=""><?php _e('Màu text (header)', TPL_DOMAIN_LANG); ?></span>
                <div data-colorpicker="" class="input-group colorpicker-component">
                    <input type="text" name="color_header" class="form-control" value="<?php esc_attr_e( !empty($options['color_header']) ? $options['color_header'] : "#FFFFFF" ); ?>"
                           placeholder="<?php _e('Màu text được chọn', TPL_DOMAIN_LANG); ?>" aria-describedby=""> <span class="input-group-addon background-eeeeee"><i></i></span>
                </div>
            </div>
            <div class="settings-item input-group">
                <span class="input-group-addon" id=""><?php _e('Màu nền tiêu đề của Table', TPL_DOMAIN_LANG); ?></span>
                <div data-colorpicker="" class="input-group colorpicker-component">
                    <input type="text" name="background_header_table" class="form-control" value="<?php esc_attr_e( !empty($options['background_header_table']) ? $options['background_header_table'] : "#008ED5" ); ?>"
                           placeholder="<?php _e('Màu tiêu đề của Table', TPL_DOMAIN_LANG); ?>" aria-describedby=""> <span class="input-group-addon background-eeeeee"><i></i></span>
                </div>
            </div>
            <div class="settings-item input-group">
                <span class="input-group-addon" id=""><?php _e('Màu text table', TPL_DOMAIN_LANG); ?></span>
                <div data-colorpicker="" class="input-group colorpicker-component">
                    <input type="text" name="color_header_table" class="form-control" value="<?php esc_attr_e( !empty($options['color_header_table']) ? $options['color_header_table'] : "#FFFFFF" ); ?>"
                           placeholder="<?php _e('Màu text được chọn', TPL_DOMAIN_LANG); ?>" aria-describedby=""> <span class="input-group-addon background-eeeeee"><i></i></span>
                </div>
            </div>

            <div class="settings-item input-group">
                <span class="input-group-addon" id=""><?php _e('Điểm đạt trên (%)', TPL_DOMAIN_LANG); ?></span>
                <input type="tel" name="standard_benchmark" class="form-control" value="<?php esc_attr_e( $options['standard_benchmark'] ); ?>"
                       placeholder="<?php _e('Điểm đạt trên (%)', TPL_DOMAIN_LANG); ?>" aria-describedby="">
            </div>
            <div class="settings-item input-group">
                <span class="input-group-addon" id=""><?php _e('Mô tả (trường hợp đạt)', TPL_DOMAIN_LANG); ?></span>
                <input type="tel" name="description_reach" class="form-control" value="<?php esc_attr_e( $options['description_reach'] ); ?>"
                       placeholder="<?php _e('Mô tả (trường hợp đạt)', TPL_DOMAIN_LANG); ?>" aria-describedby="">
            </div>
            <div class="settings-item input-group">
                <span class="input-group-addon" id=""><?php _e('Mô tả (trường hợp không đạt)', TPL_DOMAIN_LANG); ?></span>
                <input type="tel" name="description_not_reach" class="form-control" value="<?php esc_attr_e( $options['description_not_reach'] ); ?>"
                       placeholder="<?php _e('Mô tả (trường hợp không đạt)', TPL_DOMAIN_LANG); ?>" aria-describedby="">
            </div>
            <div class="settings-item input-group">
                <label class="input-group-addon" for="email-hr-manager">Email Quản lý nhân sự</label>
                <input class="form-control" size="100%" type="text" name="email_hr_manager" id="email-hr-manager" value="<?php echo isset($options['email_hr_manager']) ? $options['email_hr_manager'] : ""; ?>">
            </div>
            <div class="settings-item input-group">
                <label class="input-group-addon" for="email_template_subject">Tiêu đề email</label>
                <input class="form-control" size="100%" type="text" name="email_template_subject" id="email_template_subject" value="<?php echo isset($options['email_template_subject']) ? $options['email_template_subject'] : ""; ?>">
            </div>
            <div class="settings-item input-group">
                <label class="input-group-addon" for="email_template_content">Cấu hình nội dung gửi mail</label>
                <textarea class="form-control" size="100%" type="text" name="email_template_content" id="email_template_content" ><?php echo isset($options['email_template_content']) ? $options['email_template_content'] : ""; ?></textarea>
            </div>
        <input type="hidden" value="create-settings-website" name="action">
        <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce('manage_options'); ?>">
            <div class="save-setting">
                <button type="submit" name="save-settings" class="btn btn-primaryy"><?php _e('Lưu lại' ,TPL_DOMAIN_LANG); ?></button>
            </div>

    </div>
    </form>
</div>
<?php
