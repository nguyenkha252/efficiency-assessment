<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/01/2018
 * Time: 01:14
 */

require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-orgchart.php';

$userID = 0;
global $wpdb;
if( array_key_exists( 'uid', $_GET ) ){
    $userID = (int)wp_unslash( $_GET['uid'] );
}
$cid = !empty( $_GET['cid'] ) ? (int)wp_unslash($_GET['cid']) : 0;

$getOrgchart = [];
$user = wp_get_current_user();
$orgchart = user_load_orgchart($user);
$currentOrgID = $orgchart ? $orgchart->id : '';
$member_role = $orgchart ? $orgchart->role : '';

$orgChartParent = 0;
$member_role_user = '';
$orgchartUser = [];
if( $userID > 0 ) {
    $getUser = kpi_get_user_by_id($userID);
    $orgchartUser = $getUser->__get('orgchart');
    $orgChartParent = $orgchartUser->parent;
    $member_role_user = $orgchartUser->role;
}
if( $cid > 0 ){
    $getOrgchart = kpi_orgchart_get_by('id', $cid);
}



echo '<div id="confirm-register-kpi" class="confirm-register-kpi">';

/**
 * @TODO note condition
 *condition 1: exists user and exists cid in URL
 * @condition: !preg_match("#(cid=)#i", $_SERVER['REQUEST_URI']) && !empty($getUser) && !is_wp_error( $getUser )
 *condition 2: if user have role: managers and not have position: congty or tapdoan
 * @condition: ( ( has_cap($user, USER_CAP_MANAGERS) && !in_array($orgchartUser->role, ['congty', 'tapdoan']) )
 *condition 3: if user have role: employers and have position: nhanvien
 * @condition: (has_cap($user, USER_CAP_EMPLOYERS) && in_array($orgchartUser->role, ['nhanvien']))
 * @TODO end note condition
 */
if( !preg_match("#(cid=)#i", $_SERVER['REQUEST_URI']) && !empty($getUser) && !is_wp_error( $getUser ) && ( ( has_cap($user, USER_CAP_MANAGERS) && !in_array($orgchartUser->role, ['congty', 'tapdoan']) ) || (has_cap($user,USER_CAP_EMPLOYERS) && in_array($orgchartUser->role, ['nhanvien'])) ) ):
    #if( $member_role_user == 'phongban' || $member_role_user == 'bgd' ) {
	    #get_template_part( 'contents/confirms/registers/render-4-column', $member_role );
    #} else {
    if( !empty( $_GET['type'] ) && $_GET['type'] == 'dang-ky-kpi' ){
	    get_template_part( 'contents-user-managers/registers/new-kpi', "nhanvien" );
    }else{
	    get_template_part( 'contents-user-managers/results/kpi-target', "nhanvien" );
	    get_template_part( 'contents-user-managers/results/kpi-dashboard-news', "nhanvien" );
	    get_template_part( 'contents-user-managers/results/kpi-detail', "nhanvien" );
    }

    #}
    #get_template_part( 'contents/confirms/registers/register-kpi', 'detail-' . $member_role );
elseif( !empty( $getOrgchart ) && has_cap($user, USER_CAP_MANAGERS) ):
    #if( $getOrgchart->role == 'bgd' ){
	#    get_template_part( 'contents-user-managers/registers/', $member_role );
    #}else{

    #}

	if( !empty( $_GET['type'] ) && $_GET['type'] == 'dang-ky-kpi' ) {
		get_template_part( 'contents-user-managers/registers/new-kpi', $getOrgchart->role );
	}else{
		get_template_part( 'contents-user-managers/results/kpi-target', $getOrgchart->role );
		get_template_part( 'contents-user-managers/results/kpi-dashboard-news', $getOrgchart->role );
		get_template_part( 'contents-user-managers/results/kpi-detail', $getOrgchart->role );
    }
else:
    require_once THEME_DIR . '/ajax/get_users_list.php';
    require_once THEME_DIR . '/ajax/get_user_info.php';
    #$users = kpi_get_user_info(['id' => 0, 'single' => false, 'exclude' => []]);
    $chartChild = orgchart_get_all_kpi_by_parent( $currentOrgID );

	$arrCharts = [];
	if( !empty( $chartChild ) ){
		foreach ( $chartChild as $key => $item ){
			$arrCharts[] = $item['id'];
		}
	}
    $arrCharts = implode(", ", $arrCharts);

	if( has_cap($user,USER_CAP_MANAGERS) ) {
		$users = user_get_user_by_not_staff( );
		if( !empty( $users ) ){
			$groupUsers = array_group_by($users, 'room');
		}else{
			$groupUsers = [];
		}
		$output = render_users_by_lv_managers( $groupUsers, $_GET['type'] );
	}else{
		$users = user_get_user_by_staff( );
		if( !empty( $users ) ){
			$groupUsers = array_group_by($users, 'room');
		}else{
			$groupUsers = [];
		}
		$output = render_users_by_lv_employee( $groupUsers, $_GET['type'] );
    }
    ?>
    <div class="lists confirm-register-list-user block-item <?php echo empty($orgChartParent) ? 'approved-kpi' : ''; ?>">
        <h1><?php _e('Danh sách nhân viên', TPL_DOMAIN_LANG); ?></h1>
        <table class="user-list list-staff" cellpadding="0" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="id-col">STT</th>
                    <th>Mã nhân viên</th>
                    <th class="name"><?php _e('Name', TPL_DOMAIN_LANG); ?></th>
                    <th class="position_name"><?php _e('Position Name', TPL_DOMAIN_LANG); ?></th>
                    <th>Phòng ban / Công ty</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    echo $output;
                ?>
            </tbody>
        </table>
    </div>
<?php
endif;
echo '</div>';