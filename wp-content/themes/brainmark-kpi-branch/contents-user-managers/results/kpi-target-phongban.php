<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 26/12/2017
 * Time: 23:13
 */
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-formulas.php';
$uid = !empty( $_GET['uid'] ) ? (int) wp_unslash($_GET['uid']) : 0;
$cid = !empty($_GET['cid']) ? (int) wp_unslash($_GET['cid']) : 0;
$user = get_user_by("ID", $uid);//wp_get_current_user();
$arrUsers = [$user->ID];
$fullName = $user->first_name .' '. $user->last_name;
$orgchart = user_load_orgchart($user);
$memberRoleName = $orgchart ? $orgchart->name : '';
$member_role = $orgchart ? $orgchart->role : '';
if( empty($member_role) ) {
	$member_role = '';
} else {
	$member_role = '-' . strtolower($member_role);
}
if( array_key_exists( 'uid', $_GET ) ){
	$uID = (int)wp_slash( $_GET['uid'] );
} else {
    $uID = 0;
}
if( $uID > 0 ){
	$user = kpi_get_user_by_id($uID);
	$orgchart = user_load_orgchart($user);
}

#$year_list = kpi_get_year_list(0,KPI_YEAR_STATUS_PUBLISH);
$year_list = year_get_list_year_ceo();
$firstYear = kpi_get_first_year(0, KPI_YEAR_STATUS_PUBLISH);
$tabs = ["'Finance'", "'Customer'", "'Operate'", "'Development'"];
$strTabs = implode( ", ", $tabs );
if( isset($_GET['viewkpipb']) ) {
	$users = kpi_get_user_info( [ 'id' => 0, 'single' => false, 'exclude' => [] ] );
	if ( ! empty( $users ) && ! empty( $users['items'] ) ) {
		foreach ( $users['items'] as $key => $value ) {
			if ( $value['parent'] == $orgchart->id ) {
				$arrUsers[] = $value['id'];
			}
		}
	}
}
$arrUsers = implode(", ", $arrUsers);

$chartChild = orgchart_get_all_kpi_by_parent( $orgchart->id );
$arrChartIDs = [$orgchart->id];
if( !empty( $chartChild ) ){
	foreach ( $chartChild as $key => $item ){
		$arrChartIDs[] = $item['id'];
	}
}
$arrChartIDs = implode(", ", $arrChartIDs);
$groupDatas = kpi_get_group_kpi_by_year_orgchart_for_year_by_lower_grade_pb_chart( $strTabs, TIME_YEAR_VALUE, $orgchart->id, 'no' );

$arrTabs = kpi_get_total_percent( $groupDatas );

$firstYear = year_get_year_by_chart_id( $orgchart->id, TIME_YEAR_VALUE );
$arrChart['Finance'] = !empty( $firstYear ) ? $firstYear['finance'] : 0;
$arrChart['Customer'] = !empty( $firstYear ) ? $firstYear['customer'] : 0;
$arrChart['Operate'] = !empty( $firstYear ) ? $firstYear['operate'] : 0;
$arrChart['Development'] = !empty( $firstYear ) ? $firstYear['development'] : 0;
$data = [];
$totalColorLoop  = 30;
for( $i = 0; $i<=$totalColorLoop; $i++ ){
    $data[$i] = 10;
}
$totalPercent = array_sum( $arrTabs['total_percent'] );
$percentItem = $totalPercent / 10;
$backgroundColor = render_color_chart($percentItem);
$dataSet = ['datasets' => [['backgroundColor' => $backgroundColor, 'data' => array_map('intval', $data)]], 'total' => round( $totalPercent, 1 ) ];
$kpi_for = '';
?>
<div class="col-md-6">
    <div id="block-target-kpi" class="block-shadow block-item border-radius-default">
        <form action="get" data-chart-report="">
            <div class="header-target-kpi header-block">
                <div class="for-date">
                    <div class="for-year" style="float:right;">
                        <label for="select-for-year"><?php _e('Năm', TPL_DOMAIN_LANG); ?></label>
                        <select class="select-for-year selectpicker" id="select-for-year" name="nam">
				            <?php foreach ($year_list as $item):
					            if( empty($kpi_for) && $item['year'] == TIME_YEAR_VALUE ){
						            $kpi_for = $item['kpi_time'];
					            }
					            $selected = $item['year'] == TIME_YEAR_VALUE ? 'selected="selected"' : '';
					            echo sprintf('<option value="%s" %s>%s</option>', $item['year'], $selected, $item['year']);
				            endforeach;
				            ?>
                        </select>
                    </div>
		            <?php if( $kpi_for == 'quy' ): ?>
                        <div class="for-quarter">
                            <label for="select-for-quarter">KPI</label>
                            <select class="select-for-quarter selectpicker" id="select-for-quarter" name="">
                                <option value="0"><?php _e('Cả năm', TPL_DOMAIN_LANG); ?></option>
					            <?php
					            for ( $i = 1; $i <= 4; $i++ ){
						            $selected = $i == TIME_PRECIOUS_VALUE ? 'selected="selected"' : '';
						            echo sprintf('<option value="%s" %s>Quý %s</option>', $i, $selected, $i);
					            }
					            ?>
                            </select>
                        </div>
		            <?php elseif( $kpi_for == 'thang' ): ?>
                        <div class="for-quarter">
                            <label for="select-for-quarter">KPI</label>
                            <select class="select-for-month select-for-month-nv selectpicker" id="select-for-month" name="">
                                <option value="0"><?php _e( 'Cả năm', TPL_DOMAIN_LANG ); ?></option>
					            <?php
					            for ( $i = 1; $i <= 12; $i ++ ) {
						            $selected = $i == TIME_MONTH_VALUE ? 'selected="selected"' : '';
						            echo sprintf( '<option value="%s" %s>Tháng %s</option>', $i, $selected, $i );
					            }
					            ?>
                            </select>
                        </div>
		            <?php endif; ?>
                </div>
                <div class="name-block">
                    <h2><?php echo _Objective_KPI; //_e('Mục tiêu KPI', TPL_DOMAIN_LANG); ?></h2>
                </div>
            </div>
            <div class="content-block content-block-target-kpi">
                <div class="report-target-kpi room-report-target-kpi">
                    <div class="report-item">
                        <canvas data-type="derivedDoughnut" width="220" height="110" data-datasets="<?php esc_json_attr_e($dataSet); ?>" data-report="" id="canvas-report-kpi" class="data-report"></canvas>
                    </div>
                </div>
			    <?php
			    foreach ( $arrTabs['name'] as $k => $value ):
				    $nameTabs = $arrTabs['name'][$k];
				    #$percentTabs = round( $arrTabs['total_percent'][$k], 1 );
				    $percentTabs = $arrTabs['total_percent'][$k];
				    $class = mb_strtolower( $k );
				    $percent = array_key_exists($k, $arrChart) ? $arrChart[$k] : 0;
                    $percentShow = array_key_exists($k, $arrChart) ? $arrChart[$k] : 0;
                    if(  $percent == 0 ){
                        $percent = 1;
                    }
				    ?>
                    <div class="progress-item">
                        <div class="header-progress">
                            <span class="title"><?php esc_attr_e($nameTabs); ?> <strong>(<?= $percentShow; ?>)</strong></span>
                            <span class="percent"><?= round($percentTabs * 100 / $percent, 1); ?>%</span>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-<?= $class; ?> color-<?= $class; ?>" role="progressbar" aria-valuenow="<?= round($percentTabs * 100 / $percent, 1); ?>" aria-valuemin="0" aria-valuemax="100" ></div>
                        </div>
                    </div>
				    <?php
			    endforeach;
			    ?>

            </div>
        </form>
    </div>
</div>
