<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 26/12/2017
 * Time: 23:42
 */

require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/ajax/get_user_info.php';
#get_newsfeed($type_time, $time_value, $paged = 1, $limit = 8, $room = 0, $user_id = 0)
$paged = isset( $_GET['pg'] ) ? esc_sql($_GET['pg']) : 0;

$year = TIME_YEAR_VALUE;
$results = get_newsfeed( 'year', $year, $paged);
?>
<div class="col-md-6">
    <div class="block-dashboard-kpi block-shadow block-item border-radius-default content-paging">
        <div class="header-dashboard-kpi header-block">
            <div class="name-block">
                <h2><?php _e('Bản tin', TPL_DOMAIN_LANG); ?></h2>
                <strong class="connect-for-time"> - </strong>
                <span class="show-for-time">
				<?php _e('Ngày', TPL_DOMAIN_LANG); ?> <?php echo date('d/m/Y', time()); ?>
			</span>
            </div>
        </div>
        <div class="content-block content-block-dashboard-kpi">
            <table class="list-news-feed" cellpadding="0" cellspacing="0">
                <tbody>
			    <?php
			    if( !empty( $results ) && !is_wp_error( $results ) && $results['founds'] > 0 ){

				    foreach ( $results['posts'] as $key => $nf ){
					    $date = preg_replace('#(\d{2,4})(\/|\-)(\d{1,2})(\/|\-)(\d{1,2})[\s](\d{1,2})(\:)(\d{1,2})(\:)(\d{1,2})#', '$5/$3/$1', $nf->created);
					    ?>
                        <tr>
                            <td class="date"><?php esc_attr_e( $date ); ?></td>
                            <td class="title"><?php esc_attr_e( str_replace(str_split("\|"), "", $nf->post_title ) ); ?></td>
                        </tr>
					    <?php
				    }
			    }

			    ?>

                </tbody>
            </table>
		    <?php
		    if(!empty($results) && !empty($results['pages'])):
			    ?>
                <div class="pagination">
				    <?php echo $results['pages']; ?>
                </div>
		    <?php endif; ?>
        </div>
    </div>
</div>
