<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 12/28/17
 * Time: 4:26 PM
 */
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-formulas.php';
$uid = !empty( $_GET['uid'] ) ? (int) wp_unslash($_GET['uid']) : 0;
$cid = !empty($_GET['cid']) ? (int) wp_unslash($_GET['cid']) : 0;
$user = get_user_by("ID", $uid);//wp_get_current_user();
#$user = wp_get_current_user();
$orgchart = user_load_orgchart( $user );
	$add_target = wp_create_nonce( 'add_target' );
	$department_id = $orgchart->id;
	$chartChild = orgchart_get_all_kpi_by_parent( $orgchart->id );
	$arrChartIDs = [$orgchart->id];
	if( !empty( $chartChild ) ){
	    foreach ( $chartChild as $key => $item ){
            $arrChartIDs[] = $item['id'];
        }
    }
    $arrChartIDs = implode(", ", $arrChartIDs);
    #$firstYear = kpi_get_first_year();
	$firstYear    = year_get_year_by_chart_id( $orgchart->id, TIME_YEAR_VALUE );
	$currentLevel = 1;
	$users        = kpi_get_user_info( [ 'id' => 0, 'single' => false, 'exclude' => [] ] );
	$arrUsers = [$user->ID];
	if ( ! empty( $users ) && ! empty( $users['items'] ) ) {
		foreach ( $users['items'] as $key => $value ) {
			if ( $value['parent'] == $orgchart->id ) {
				$arrUsers[] = $value['id'];
			}
		}
		$arrUsers = implode( ", ", $arrUsers );
	}

	$tabs = [
		'finance'     => [
			'title'      => __( 'Finance', TPL_DOMAIN_LANG ),
			'type'       => 'Finance',
			'percent'    => $firstYear ? $firstYear['finance'] : '',
			'img'        => 'tai-chinh.png',
			'time_type'  => 'PRECIOUS',
			'time_value' => TIME_PRECIOUS_VALUE,
			'year_id'    => $firstYear
		],
		'customer'    => [
			'title'      => __( 'Customer', TPL_DOMAIN_LANG ),
			'type'       => 'Customer',
			'percent'    => $firstYear ? $firstYear['customer'] : '',
			'img'        => 'customer.png',
			'time_type'  => 'PRECIOUS',
			'time_value' => TIME_PRECIOUS_VALUE,
			'year_id'    => $firstYear
		],
		'operate'     => [
			'title'      => __( 'Operate', TPL_DOMAIN_LANG ),
			'type'       => 'Operate',
			'percent'    => $firstYear ? $firstYear['operate'] : '',
			'img'        => 'van-hanh.png',
			'time_type'  => 'PRECIOUS',
			'time_value' => TIME_PRECIOUS_VALUE,
			'year_id'    => $firstYear
		],
		'development' => [
			'title'      => __( 'Development', TPL_DOMAIN_LANG ),
			'type'       => 'Development',
			'percent'    => $firstYear ? $firstYear['development'] : '',
			'img'        => 'phat-trien.png',
			'time_type'  => 'PRECIOUS',
			'time_value' => TIME_PRECIOUS_VALUE,
			'year_id'    => $firstYear
		]
	];

	?>
    <div class="clearfix"></div>
    <div class="block-detail block-result-kpi-detail">
        <div id="room-target-kpi" data-content-management="" style="display: block">
            <ul id="" class="list-room-target-kpi clearfix nav nav-pills">
				<?php
				foreach ( $tabs as $tabkey => $tabData ):
					$cls = ( 'finance' == $tabkey ) ? 'active' : '';
					?>
                    <li class="room-target-kpi-item <?php echo $cls; ?> room-<?php echo $tabkey; ?>">
                        <a href="#room-<?php echo $tabkey; ?>" class="" title="" data-toggle="tab">
                            <img src="<?php echo THEME_URL; ?>/assets/images/<?php echo $tabData['img']; ?>"/>
							<?php echo $tabData['title']; ?>
                        </a>
                    </li>
				<?php endforeach; ?>
            </ul>
            <div class="list-detail tab-content clearfix">
				<?php foreach ( $tabs as $tabkey => $tabData ):
					$cls = ( 'finance' == $tabkey ) ? 'active' : '';
					?>
                    <div class="tab-pane <?php echo $cls; ?>" id="room-<?php echo $tabkey; ?>">
                        <div class="list-detail">
                            <div class="top-list-detail">
                                <h3 class="title">
									<?php echo $tabData['title']; ?>
                                    <a href="javascript:;"
                                       data-year="<?php echo esc_json_attr( $tabData['year_id'] ); ?>"
                                       data-toggle="modal"
                                       data-target="#tpl-ceo-kpi-year">
                                        <span class="icon-percent" contenteditable="false"
                                              data-kpi-percent="<?php echo $tabkey; ?>"><?php echo $tabData['percent']; ?>%</span>
                                        <i class="glyphicon glyphicon-pencil hidden" aria-hidden="true"></i>
                                    </a>
                                </h3>
								<?php /*
                            <a href="javascript:;" title="" class="check-all" data-check-all="">
                                <i class="fa fa-check"
                                 aria-hidden="true"></i> <?php _e('Đồng ý tất cả', TPL_DOMAIN_LANG); ?>
                            </a>
                            */ ?>
                            </div>
                        </div>

                        <form data-update-result=""
                              action="<?php echo admin_url( 'admin-ajax.php?action=personal_update_result' ); ?>"
                              class=""
                              id="form-<?php echo $tabkey; ?>" method="post" enctype="application/x-www-form-urlencoded"
                              onsubmit="return false">
                            <input value="<?php echo $add_target; ?>" type="hidden" name="_wpnonce">
                            <input value="<?php echo $tabData['type']; ?>" type="hidden" name="type">
                            <table class="table-list-detail table-managerment-target-kpi" cellpadding="0"
                                   cellspacing="0">
                                <thead>
                                <tr>
                                    <th class="column-1 kpi-id align-center"><?php _e( 'Mã KPI', TPL_DOMAIN_LANG ); ?></th>
                                    <th class="column-2 kpi-content"><?php _e( 'Nội dung mục tiêu', TPL_DOMAIN_LANG ); ?></th>
                                    <th class="column-3 kpi-company_plan align-center"><?php _e( 'Trọng số (%)', TPL_DOMAIN_LANG ); ?></th>
                                    <th class="column-4 kpi-unit align-center"><?php _e( 'Kế hoạch', TPL_DOMAIN_LANG ); ?></th>
                                    <th class="column-5 kpi-receive align-center"><?php _e( 'Thực hiện', TPL_DOMAIN_LANG ); ?></th>
                                    <th class="column-7 align-center"><?php _e( 'Hoàn thành', TPL_DOMAIN_LANG ); ?></th>
                                    <th class="column-7 align-center"><?php _e( 'Kết quả', TPL_DOMAIN_LANG ); ?></th>
                                    <th class="column-8 align-center"><?php _e( 'Chứng minh', TPL_DOMAIN_LANG ); ?></th>
                                </tr>
                                </thead>
                                <tbody>
								<?php
								$groupDatas              = kpi_get_group_kpi_by_year_orgchart_for_year_by_lower_grade_pb_chart( $wpdb->prepare( "%s", $tabData['type'] ), TIME_YEAR_VALUE, $orgchart->id, 'no' );
								if ( ! empty( $groupDatas ) ):
									foreach ( $groupDatas as &$item ):
										$plan = $item['plan'];
										$actual_gain = $item['actual_gain'];
										$planForYear = $item['plan_for_year'];
										$percentForMonth = 0;
										$percentForYear = 0;
										$timeNotDone = [];
										$id = $item['id'];
										$ttactual_gain = 0;
										#$ttactual_gain = get_actual_gain( $item );
										if( $item['owner'] == 'no' ) {
											$kpiAll = kpi_get_all_kpi_by_parent( $id, 'no' );
											$arrIDs = [];
											if ( ! empty( $kpiAll ) ) {
												foreach ( $kpiAll as $k => $v ) {
													$arrIDs[] = $v['id'];
												}
												$arrIDs  = implode( ", ", $arrIDs );
                                                if( $item['unit'] == KPI_UNIT_PERCENT ){
                                                    # @TODO Note
                                                    # đơn vị là % thì lấy tổng kết quả của các tháng chia cho số tháng nhập KPI
                                                    # Các tháng có KPI trong mục đăng ký KPI tháng
                                                    # Update feature on 01/10/2018
                                                    # Link request: https://trello.com/c/CPoMsHN1/24-imagepng
                                                    $results = list_kpi_unit_percent_by_status_done_create_by_start_and_precious($arrIDs, TIME_YEAR_VALUE, TIME_PRECIOUS_VALUE);
                                                }else {
                                                    $results = list_kpi_by_status_done_create_by_start_and_precious($arrIDs, TIME_YEAR_VALUE, TIME_PRECIOUS_VALUE);
                                                }
												if ( ! empty( $results ) ) {
													$arrPercentForTime = [];
													foreach ( $results as $ks => $vs ) {
														$ttplan    = $vs['plan'];
														$ttpercent = $vs['percent'];
														$formulas  = maybe_unserialize( $vs['formulas'] );
														#$ttactual_gain += $vs['actual_gain'];
														if ( $vs['unit'] == KPI_UNIT_THOI_GIAN ) {
															$percentTime = getPercentForMonth( $vs['unit'], $item['formula_type'], $formulas, $plan, $vs['actual_gain'] );
															$arrPercentForTime[] = $percentTime;
															if( !empty( $vs['actual_gain'] && $percentTime <= 0 ) ){
																$timeNotDone[] = $vs['actual_gain'];
															}
														} else {
															$ttactual_gain += $vs['actual_gain'];
														}
                                                        if( $vs['unit'] == KPI_UNIT_PERCENT ){
                                                            $i_unit_percent++;
                                                        }
													}
													if ( ! empty( $arrPercentForTime ) ) {
														$ttactual_gain = array_sum( $arrPercentForTime ) / count( $arrPercentForTime );
													}
                                                    if( $item['unit'] == KPI_UNIT_PERCENT ){
                                                        if( $i_unit_percent > 0 ) {
                                                            $ttactual_gain = round($ttactual_gain / $i_unit_percent, 1);
                                                        }
                                                    }
												} else {
													$ttactual_gain = '';
												}
											} else {
												$ttactual_gain = '';
											}
										}elseif( $item['alias'] == 'phongban' && $item['owner'] == 'yes' ) {
											$formulas = maybe_unserialize( $item['formulas'] );
											if( !empty($item['actual_gain'] ) ){
												if( $item['unit'] == KPI_UNIT_THOI_GIAN ){
													getPercentForMonth(  $item['unit'], $item['formula_type'], $formulas, $plan, $item['actual_gain']  );
												}else{
													$ttactual_gain += $item['actual_gain'];
												}
											}
										}
										?>
                                        <tr>
                                            <td class="column-1 kpi-id align-center">
												<?php echo $item['id']; ?>
												<?php
												$classStar = "";
                                                if( !empty( $item['influence'] ) && $item['influence'] == 'yes' ){
	                                                $classStar = 'color-influence';
                                                }else {
                                                    if ( $item['create_by_node'] == KPI_CREATE_BY_NODE_START ) {
                                                        $classStar = "color-node-0";
                                                    } elseif ( $item['create_by_node'] == KPI_CREATE_BY_NODE_MIDDLE ) {
                                                        $classStar = "color-node-1-2";
                                                    }
                                                }
												if ( $item['required'] == 'yes' ): ?>
                                                    <span class="<?php esc_attr_e( $classStar ); ?> glyphicon glyphicon-star"></span>
												<?php endif; ?>
                                            </td>
                                            <td class="column-2 kpi-content">
                                                <?php echo str_replace(str_split("\|"), "", $item['post_title']); ?>
	                                            <?php
	                                            $arrFormula = [
		                                            'html' => render_html_formula($item['formulas'], $item['formula_type']),
		                                            'title' => $item['formula_title'],
		                                            'note' => $item['note']
	                                            ]
	                                            ?>
                                                <a data-target="#formula-popup" data-toggle="modal" href="javascript:;" class="view-formula" data-formula="<?= esc_json_attr($arrFormula); ?>"><i class="fa fa-info-circle"></i></a>
                                            </td>
                                            <td class="column-3 kpi-company_plan align-center"><?php echo $item['percent']; ?>%</td>
                                            <td class="column-4 kpi-unit align-center"><?php esc_attr_e( $plan ); ?> <?php esc_attr_e( !empty( $item['unit'] ) ? getUnit( $item['unit'] ) : '' ); ?> </td>
                                            <td class="column-5 kpi-receive align-center">
												<?php
										if( $item['unit'] == KPI_UNIT_THOI_GIAN ){
											if( !empty($timeNotDone) ){
												echo "<span class='error'>Thời gian không hoàn thành: <br></span>";
												echo implode("<br> ", $timeNotDone);
											}else{
												echo "<span class='notification success'>Hoàn thành đúng kế hoạch</span>";
											}
											$percentForMonth = $ttactual_gain;
										}else {
												if ( $ttactual_gain != '' ):
													$formulas        = maybe_unserialize( $item['formulas'] );
													$percentForMonth = getPercentForMonth( $item['unit'], $item['formula_type'], $formulas, $plan, $ttactual_gain );
												endif;
												?>
												<?php esc_attr_e( $ttactual_gain ); ?>
												<?php esc_attr_e( !empty( $item['unit'] ) ? getUnit( $item['unit'] ) : '' ); ?>
                                            <?php } ?>
                                            </td>
                                            <td class="column-7 kpi-percent align-center"><?php echo round( $percentForMonth, 1 ); ?>
                                                %
                                            </td>
                                            <td class="column-8 kpi-percent align-center"><?php echo round( $percentForMonth = $percentForMonth * $item['percent'] / 100, 1 ); ?>
                                                %
                                            </td>
                                            <td class="column-9 align-center">
												<?php
												if ( ! empty( $item['files'] ) ) {
													$files = maybe_unserialize( $item['files'] );
													if ( ! empty( $files ) && ! empty( $files['files_url'] ) ) {
														echo sprintf( "<a href='javascript:;' data-action='%s' data-id='%s' class='view-file-upload' data-target='#tpl-personal-view-file' >%s</a>", admin_url( 'admin-ajax.php?action=load_file_upload&id=' . $item['id'] ), $item['id'], __( 'Xem danh sách tập tin', TPL_DOMAIN_LANG ) );
													}
												}
												?>
                                            </td>
                                        </tr>
									<?php endforeach;
								endif;
								?>
                                </tbody>
                            </table>

                            <div class="action-bot">
                                <?php
                                    $urlExport = add_query_arg('room', 1, PAGE_EXPORTS_URL);
                                ?>
                                <button type="button" class="btn-brb-default btn-print btn-action-item" name="btn-print" onclick="location.href='<?php
	                            echo esc_url( $urlExport );
	                            ?>'">
                                    <i class="fa fa-print" aria-hidden="true"></i>
		                            <?php _e('Xuất báo cáo',TPL_DOMAIN_LANG); ?>
                                </button>
                            </div>
                        </form>
                        <div id="tpl-personal-view-file" class="modal fade" data-keyboard="false" data-backdrop="static"
                             role="dialog">
                            <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
                                  action="<?php echo admin_url( 'admin-ajax.php?action=update_upload_file' ) ?>"
                                  method="post">
                                <input type="hidden" name="id" value="">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title"><?php _e( 'Danh sách tập tin tải lên', TPL_DOMAIN_LANG ); ?></h4>
                                    </div>
                                    <div class="modal-body clearfix">
                                        <table class="table-list-files table-list-detail">
                                            <thead>
                                            <tr>
                                                <th class="column-1"><?php _e( 'STT', TPL_DOMAIN_LANG ); ?></th>
                                                <th class="column-2"><?php _e( 'Tên tập tin', TPL_DOMAIN_LANG ); ?></th>
                                                <th class="column-3"><?php _e( 'Chọn', TPL_DOMAIN_LANG ); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody class="tbody-main">

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <button type="submit" class="btn btn-cancel">Xoá</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="tpl-ceo-target-<?php echo $tabkey; ?>" class="modal fade" data-keyboard="false"
                             data-backdrop="static" role="dialog">
                            <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
                                  action="<?php echo admin_url( 'admin-ajax.php?action=add_ceo_target' ) ?>"
                                  method="post">
                                <input type="hidden" name="_wpnonce" value="<?php echo $add_target; ?>">
                                <input type="hidden" name="type" value="<?php echo $tabData['type']; ?>">
                                <input type="hidden" name="year_id"
                                       value="<?php echo $firstYear ? $firstYear['id'] : 0; ?>">
                                <input type="hidden" name="time_type" value="<?php echo $tabData['time_type']; ?>">
                                <input type="hidden" name="time_value" value="<?php echo $tabData['time_value']; ?>">
                                <input type="hidden" name="chart_id" value="<?php echo $orgchart->id; ?>">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title"><?php echo $tabData['title']; ?>
                                            : Thêm KPI</h4>
                                    </div>
                                    <div class="modal-body clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group input-group input-group-select">
                                                <label class="input-group-addon" for="year_id"
                                                       id="chart_name-<?php echo $tabkey; ?>">Chỉ mục KPI: </label>
                                                <select data-loading=".form-group.input-group.input-group-select"
                                                        data-target="#list-kpis-<?php echo $tabkey; ?>"
                                                        aria-describedby="chart_name-<?php echo $tabkey; ?>"
                                                        class="kpi-category selectpicker input-select" name="cat"><?php
													kpiRenderCategoryDropdown( '', true, '--- ' );
													?></select>
                                            </div>
                                            <div class="form-group input-group-list">
                                                <div class="department-container hidden"></div>
                                                <ul id="list-kpis-<?php echo $tabkey; ?>" class="list-group"></ul>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group form-plans">
                                                <div class="input-group">
                                                    <span class="input-group-addon"
                                                          id="company_plan-<?php echo $tabkey; ?>">Nhập kế hoạch</span>
                                                    <input type="text" name="company_plan" class="form-control"
                                                           placeholder="Kế hoạch công ty"
                                                           aria-describedby="company_plan-<?php echo $tabkey; ?>">
                                                </div>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="unit-<?php echo $tabkey; ?>">Đơn vị tính</span>
                                                    <input type="text" name="unit" class="form-control"
                                                           placeholder="Đơn vị tính"
                                                           aria-describedby="unit-<?php echo $tabkey; ?>">
                                                </div>
                                                <div class="input-group group-date">
                                                <span class="input-group-addon"
                                                      id="receive-<?php echo $tabkey; ?>">Thời điểm nhận kết quả</span>
                                                    <input type="text" name="receive" class="form-control"
                                                           placeholder="Thời điểm nhận kết quả" data-lang="vi"
                                                           data-format="<?php echo KPI_FORMAT_DATE; ?>"
                                                           data-min-date="01-01-<?php echo $tabData['time_value']; ?>"
														<?php /* data-max-date="" */ ?>
                                                           data-group-date=".group-date" data-timepicker="false"
                                                           data-btn-date=".input-group-addon.date-btn"
                                                           data-ctrl-date
                                                           aria-describedby="receive-<?php echo $tabkey; ?>">
                                                    <span class="input-group-addon date-btn">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                                </div>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="percent-<?php echo $tabkey; ?>">Trọng số tối đa</span>
                                                    <input type="text" name="percent" class="form-control"
                                                           placeholder="Trọng số tối đa"
                                                           aria-describedby="percent-<?php echo $tabkey; ?>">
                                                    <span class="input-group-addon">%</span>
                                                </div>
                                            </div>
                                            <div class="form-group phongban-list">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <button type="submit" class="btn btn-primaryy">Lưu</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
				<?php endforeach; ?>
            </div>
        </div>

    </div>
	<?php
