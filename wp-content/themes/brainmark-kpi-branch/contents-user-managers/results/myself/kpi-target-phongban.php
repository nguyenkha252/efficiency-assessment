<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 26/12/2017
 * Time: 23:13
 */

require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-formulas.php';
require_once THEME_DIR . '/inc/lib-behavior.php';

if( !empty( $_GET['uid'] ) && (int)$_GET['uid'] > 0 ){
    $user = kpi_get_user_by_id($_GET['uid']);
}else{
    $user = wp_get_current_user();
}
$orgchart = user_load_orgchart( $user );
$year = $_GET['nam'];
#$listKPI = list_kpi_by_status_create_by( $user->ID, $orgchart->id, $year, TIME_PRECIOUS_VALUE, TIME_MONTH_VALUE );

if( empty( TIME_PRECIOUS_VALUE ) ){
	$tabsarr = ["'Finance'", "'Customer'", "'Operate'", "'Development'"];
	$strTabs = implode( ", ", $tabsarr );
	$listKPI = kpi_get_group_kpi_by_year_orgchart_for_year_by_lower_grade_nv($strTabs, TIME_YEAR_VALUE, $user->ID);
}else{
	if( !empty( TIME_MONTH_VALUE ) ){
		$listKPI = get_kpi_by_year_orgchart_for_month_and_status(TIME_YEAR_VALUE, TIME_MONTH_VALUE, $user->ID);

	}else{
		$listKPI = get_kpi_by_year_orgchart_for_precious_and_status(TIME_YEAR_VALUE, TIME_PRECIOUS_VALUE, $user->ID);
	}
}

$year_list_behavior = year_get_list_year_behavior();
$firstYearBehavior = year_get_first_year_behavior( TIME_YEAR_VALUE );
if( empty( $firstYearBehavior ) && !empty(  $year_list_behavior) ){
	$firstYearBehavior = $year_list_behavior[0];
}
$year_id_behavior = !empty( $firstYearBehavior ) && !empty($firstYearBehavior['id']) ? $firstYearBehavior['id'] : 0;
$year_behavior = !empty( $firstYearBehavior ) && !empty($firstYearBehavior['year']) ? $firstYearBehavior['year'] : 0;
$arrApply_for = !empty( $firstYearBehavior ) && !empty($firstYearBehavior['apply_for']) ? maybe_unserialize($firstYearBehavior['apply_for']) : [];
if( empty($arrApply_for) ) {
    $arrApply_for = [];
}
if( !in_array( $orgchart->id, $arrApply_for ) ) {
	$percentBehavior = $firstYearBehavior && !empty( $firstYearBehavior['behavior_percent'] ) ? $firstYearBehavior['behavior_percent'] : 0;
}else{
    $percentBehavior = 0;
}
$percentKPI = 100 - $percentBehavior;
$totalPercent = 0;
$totalPercentItem = 0;
$totalPercentYear = 0;
if( !empty( $listKPI ) ){
    foreach ( $listKPI as $key => $item ){
        $plan = $item['plan'];
	    $unit = $item['unit'];
        $percent = $item['percent'];
	    $actual_gain = $item['actual_gain'];
	    if( !empty( $item['formulas'] ) ){
		    $formulas = maybe_unserialize( $item['formulas'] );
	    }else{
		    $formulas = [];
	    }
	    if( $totalPercent == 0 ){
		    $totalPercent =  $item['total_percent'];
	    }
        if( empty( TIME_PRECIOUS_VALUE ) ){

	        if( $item['unit'] == KPI_UNIT_THOI_GIAN ){
		        $results = kpi_get_all_kpi_by_parent( $item['id'] );
		        if ( ! empty( $results ) ) {
			        $arrPercentForTime = [];
			        foreach ( $results as $ks => $vs ) {
				        $ttplan        = $vs['plan'];
				        if( $vs['unit'] == KPI_UNIT_THOI_GIAN ){

					        if( !empty( $vs['actual_gain'] ) ) {
						        $arrPercentForDateTime[] = $vs['actual_gain'];
					        }
					        $arrPercentForTime[] = getPercentForMonth(  $vs['unit'], $item['formula_type'], $formulas, $ttplan, $vs['actual_gain']  );
				        }else {
					        $ttactual_gain += $vs['actual_gain'];
				        }
			        }
			        if( !empty( $arrPercentForTime ) ){
				        $ttactual_gain = array_sum( $arrPercentForTime ) / count( $arrPercentForTime );
			        }
		        } else {
			        $ttactual_gain = '';
		        }
	        }else {
		        $total_actual_gain = kpi_sum_kpi_by_parent( $item['id'] );
		        if ( ! empty( $total_actual_gain ) && array_key_exists( 'total_actual_gain', $total_actual_gain ) ) {
			        $ttactual_gain = $total_actual_gain['total_actual_gain'];
		        } else {
			        $ttactual_gain = '';
		        }
	        }
	        if( $ttactual_gain != '' && $item['unit'] != KPI_UNIT_THOI_GIAN ):
		        $percentForMonth = getPercentForMonth( $unit, $item['formula_type'], $formulas, $plan, $ttactual_gain );
            elseif( $item['unit'] == KPI_UNIT_THOI_GIAN ):
		        $percentForMonth = $ttactual_gain;
	        endif;
        }else{
	        if( $actual_gain != '' ):
		        $percentForMonth = getPercentForMonth( $item['unit'], $item['formula_type'], $formulas, $plan, $actual_gain );

	        endif;
            #$actual_gain = $item['total_kq'];
            #$totalPercent += $percent;
        }
        #$percentForMonth = getPercentForMonth( $item['unit'], $item['formula_type'], $formulas, $plan, $actual_gain );
        $percentForMonth = $percentForMonth * $percent / 100;
        $totalPercentItem += $percentForMonth;
    }
}
$totalPercentItem = round($totalPercentItem, 1);
if( $totalPercent == 0 ){
    $totalPercent = 1;
}
$data = [];
$totalColorLoop  = 30;
for( $i = 0; $i<=$totalColorLoop; $i++ ){
    $data[$i] = 10;
}

$totalPercentMonth = ($totalPercentItem / $totalPercent ) * 100;
if( $totalPercentMonth % 1 === 0 ){
    $totalPercentMonth = round($totalPercentMonth, 1);
}
$percentChartItem = $totalPercentItem * $percentKPI / 100;

#behavior

if( !in_array( $orgchart->id, $arrApply_for ) ) {
	if ( empty( TIME_PRECIOUS_VALUE ) ) {
		$getBehaviorForUser = behavior_get_total_number_for_year( TIME_YEAR_VALUE, $user->ID, $orgchart->id );
	} else {
		$getBehaviorForUser = behavior_get_behavior_by_user( $user->ID, $year_behavior, TIME_PRECIOUS_VALUE, TIME_MONTH_VALUE );
	}


	$totalPercentBehavior     = 0;
	$totalPercentBehaviorItem = 0;
	$behaviorPercent          = 0;
	if ( ! empty( $getBehaviorForUser ) ) {
		foreach ( $getBehaviorForUser as $key => $item ) {
			if ( $item['parent_1'] != 0 ) {
				if ( $behaviorPercent == 0 ) {
					$behaviorPercent = $item['behavior_percent'];
				}
				$number                   = empty( TIME_PRECIOUS_VALUE ) ? $item['total_number'] : $item['number'];
				$percentItem              = $number * $item['violation_assessment'];
				$totalPercentBehaviorItem += $percentItem;
			}
		}
	}
	if ( $behaviorPercent == 0 ) {
		$behaviorPercent = 1;
	}
}else{
	$totalPercentBehavior     = 0;
	$totalPercentBehaviorItem = 0;
	$behaviorPercent          = 0;
}
$totalPercentBehavior = 100 - $totalPercentBehaviorItem;
$total = ( $percentBehavior - ($totalPercentBehaviorItem * $percentBehavior) ) + $percentChartItem;
#$totalPercentBehaviorItem = $totalPercentBehaviorItem * $percentBehavior / 100;
$percentItem = $total / 10;
$backgroundColor = render_color_chart($percentItem);
$dataSet = ['datasets' => [['backgroundColor' => $backgroundColor, 'data' => array_map('intval', $data)]], 'total' => (double)$total ];

?>
<div class="col-md-6">
    <div id="block-target-kpi" class="block-shadow block-item border-radius-default ">
        <form action="get" data-chart-report="">
            <div class="header-target-kpi header-block">

                <div class="name-block">
                    <h2><?php echo _Objective_KPI; //_e('Mục tiêu KPI'); ?></h2>
                </div>
            </div>
            <div class="content-block content-block-target-kpi">
                <div class="report-target-kpi">
                    <div class="report-item">
                        <canvas data-type="derivedDoughnut" width="200" height="100" data-datasets="<?php echo str_replace('"', '&#34;', json_encode( $dataSet ) ); ?>" data-report="" id="canvas-report-kpi" class="data-report"></canvas>
                    </div>
                </div>
                <div class="progress-item">
                    <div class="header-progress">
                        <span class="title"><?php _e('Mục tiêu công việc', TPL_DOMAIN_LANG); ?> <strong>(<?= 100 - $percentBehavior; ?>)</strong></span>
                        <span class="percent"><?= $totalPercentItem; ?>%</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-cty" role="progressbar" aria-valuenow="<?= $totalPercentItem; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>

                <div class="progress-item">
                    <div class="header-progress">
                        <span class="title"><?php _e('Thái độ hành vi', TPL_DOMAIN_LANG); ?> <strong>(<?= $percentBehavior; ?>)</strong></span>
                        <span class="percent"><?= $totalPercentBehavior; ?>%</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-room" role="progressbar" aria-valuenow="<?= $totalPercentBehavior; ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?= $totalPercentBehaviorItem; ?>%"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
