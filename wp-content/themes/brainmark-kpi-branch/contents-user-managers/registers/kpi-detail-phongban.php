<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/8/18
 * Time: 17:44
 */

require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-users.php';

$add_target = wp_create_nonce('add_department_target');
$assign_target = wp_create_nonce('assign_kpi_target');
$change_status = wp_create_nonce('kpi_change_status');
$user = wp_get_current_user();
$orgchart = user_load_orgchart($user);

#$firstYear = kpi_get_first_year(KPI_YEAR_STATUS_PUBLISH);
$firstYear = year_get_year_by_chart_id( $orgchart->id, TIME_YEAR_VALUE );
$user = wp_get_current_user();
$orgchart = user_load_orgchart($user);

$tabs = [
    'finance' => [
        'title' => __('Finance', TPL_DOMAIN_LANG),
        'type' => 'Finance',
        'percent' => $firstYear ? $firstYear['finance'] : '',
        'img' => 'tai-chinh.png',
        'time_type' => 'YEAR',
        'time_value' => TIME_YEAR_VALUE,
        'year_id' => $firstYear
    ],
    'customer' => [
        'title' => __('Customer', TPL_DOMAIN_LANG),
        'type' => 'Customer',
        'percent' => $firstYear ? $firstYear['customer'] : '',
        'img' => 'customer.png',
        'time_type' => 'YEAR',
        'time_value' => TIME_YEAR_VALUE,
        'year_id' => $firstYear
    ],
    'operate' => [
        'title' => __('Operate', TPL_DOMAIN_LANG),
        'type' => 'Operate',
        'percent' => $firstYear ? $firstYear['operate'] : '',
        'img' => 'van-hanh.png',
        'time_type' => 'YEAR',
        'time_value' => TIME_YEAR_VALUE,
        'year_id' => $firstYear
    ],
    'development' => [
        'title' => __('Development', TPL_DOMAIN_LANG),
        'type' => 'Development',
        'percent' => $firstYear ? $firstYear['development'] : '',
        'img' => 'phat-trien.png',
        'time_type' => 'YEAR',
        'time_value' => TIME_YEAR_VALUE,
        'year_id' => $firstYear
    ]
];
# echo '<pre>'; var_dump('PRECIOUS', TIME_PRECIOUS_VALUE); echo '</pre>';
$parentChart = kpi_get_orgchart_by_id((int)$orgchart->parent);
# echo '<pre>'; var_dump($orgchart, $parentChart); echo '</pre>';
?>

<div class="clearfix"></div>
<div class="block-detail block-result-kpi-detail">
    <div id="room-target-kpi" data-type="room" data-content-management="" style="display: block">
        <ul id="" class="list-room-target-kpi clearfix nav nav-pills">
            <?php
            foreach ($tabs as $tabkey => $tabData):
                $cls = ('finance' == $tabkey) ? 'active' : '';
                ?>
                <li class="room-target-kpi-item <?php echo $cls; ?> room-<?php echo $tabkey; ?>">
                    <a href="#room-<?php echo $tabkey; ?>" class="" title="" data-toggle="tab" data-tab-type="room">
                        <img src="<?php echo THEME_URL; ?>/assets/images/<?php echo $tabData['img']; ?>"/>
                        <?php echo $tabData['title']; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>

        <div class="list-detail tab-content clearfix">
            <?php foreach ($tabs as $tabkey => $tabData):
                $charts = kpi_get_list_org_charts($user->orgchart_id, true);
                $cls = ('finance' == $tabkey) ? 'active' : '';
                ?>
                <div class="tab-pane <?php echo $cls; ?>" id="room-<?php echo $tabkey; ?>">
                    <div class="list-detail">
                        <div class="top-list-detail">
                            <h3 class="title">
                                <?php echo $tabData['title']; ?>
                                <a href="javascript:;" data-year="<?php echo esc_json_attr($tabData['year_id']); ?>" data-toggle="modal"
                                   data-target="#tpl-ceo-kpi-year" >
                                    <span class="icon-percent" contenteditable="false" data-kpi-percent="<?php echo $tabkey; ?>"><?php echo $tabData['percent']; ?>%</span>
                                    <i class="glyphicon glyphicon-pencil hidden" aria-hidden="true"></i>
                                </a>
                            </h3>
                        </div>
                    </div>
                    <form data-approved="" action="<?php echo admin_url('admin-ajax.php?action=department_change_status'); ?>" class="form-trien-khai-kpi form-trien-khai-kpi-phongban"
                          id="form-<?php echo $tabkey; ?>" method="post" enctype="application/x-www-form-urlencoded"
                          onsubmit="return false">
                        <input value="<?php echo $change_status; ?>" type="hidden" name="_wpnonce">
                        <input value="<?php echo $tabData['type']; ?>" type="hidden" name="type">
                        <input type="hidden" name="_wp_http_referer" value="<?php esc_attr_e( wp_unslash( $_SERVER['REQUEST_URI'] ) ); ?>#room-<?= $tabkey; ?>">
                        <table class="table-list-detail table-managerment-target-kpi kpi-phongban" cellpadding="0" cellspacing="0">
                            <colgroup class="column-1" style="width: 4%"></colgroup>
                            <colgroup class="column-2" style="width: 30%"></colgroup>
                            <colgroup class="column-3" style="width: 8%"></colgroup>
                            <colgroup class="column-4" style="width: 8%"></colgroup>
                            <colgroup class="column-6" style="width: 8%"></colgroup>
                            <colgroup class="column-7 trien-khai" style="width: 14%"></colgroup>
                            <colgroup class="column-8" style="width: 4%"></colgroup>
                            <colgroup class="column-9" style="width: 8%"></colgroup>
                            <colgroup class="column-10" style="width: 10%"></colgroup>
                            <thead>
                            <tr>
                                <th class="column-1 kpi-id align-center"><?php _e('Mã KPI', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-2 kpi-content"><?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-3 kpi-parent_plan"><?php
                                    _e('Kế hoạch <br>', TPL_DOMAIN_LANG);
                                    echo (!empty($parentChart) ? $parentChart['name'] : '');
                                ?></th>
                                <th class="column-4 kpi-plan"><?php _e('Kế hoạch ', TPL_DOMAIN_LANG); _e("<br><span>{$orgchart->name}</span>"); ?></th>
                                <th class="column-6 kpi-receive align-center"><?php _e('Thời điểm <br>nhận kết quả', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-7 trien-khai align-center"><?php _e('Triển khai <br>cho cấp dưới', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-8 align-center"><?php _e('Trọng số (%)', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-9 align-center"><?php _e('Trạng thái', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-10 align-center"><?php _e('Thao Tác', TPL_DOMAIN_LANG); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $groupDatas = kpi_get_list($tabData['type'], 'YEAR', TIME_YEAR_VALUE, $user->ID, $user->orgchart_id, 1);
                            $totalPercent = 0.0;
                            if (!empty($groupDatas)):
                                foreach ($groupDatas as &$item):
                                    $totalPercent += doubleval($item['percent']);
                                    $parentItem = kpi_get_kpi_by_id((int)$item['parent']);
                                    $subcharts = kpi_get_list_of_charts($tabData['type'], $tabData['time_type'], $tabData['time_value'], 0, $user->orgchart_id, $item['bank_id'], $item['id']);
                                    ?>
                                    <tr>
                                        <td class="column-1 kpi-id align-center"><?php echo $item['id'];
                                            $classStar = "";
                                            if( $item['create_by_node'] == KPI_CREATE_BY_NODE_START ){
                                                $classStar = "color-node-0";
                                            }elseif( $item['create_by_node'] == KPI_CREATE_BY_NODE_MIDDLE ){
                                                $classStar = "color-node-1-2";
                                            }
                                            if( $item['required'] == 'yes' ): ?>
                                                <span class="<?php esc_attr_e($classStar); ?> glyphicon glyphicon-star"></span>
                                            <?php endif; ?>
                                        </td>
                                        <td class="column-2 kpi-content"><?php echo $item['post_title']; ?></td>
                                        <td class="column-3 kpi-parent_plan"><?php echo !empty($parentItem) ? "{$parentItem['plan']} <br>".getUnit($item['unit'])."" : ''; ?></td>
                                        <td class="column-4 kpi-plan"><?php echo "{$item['plan']} <br>".getUnit($item['unit']).""; ?></td>
                                        <td class="column-6 kpi-receive align-center"><?php echo substr(kpi_format_date($item['receive']), 0, 10); ?></td>
                                        <td class="column-7 kpi-departments align-center"><div class="clearfix"><?php
                                            if( ($item['status'] == KPI_STATUS_RESULT) ):
                                                $load_params = [
                                                    'action' => 'load-members-kpi-target',
                                                    'id' => $firstYear['id'],
                                                    'kpi_id' => $item['id'],
                                                    'kpi_type' => $tabData['type'],
                                                    'chart_id' => $item['chart_id'],
                                                    'bank_id' => $item['bank_id'],
                                                    'year' => $tabData['time_value'],
                                                    '_wpnonce' => wp_create_nonce('load-members-kpi-target'),
                                                ];
                                                $items = [];
                                                if( !empty($subcharts) ):
                                                    foreach($subcharts as $dep):
                                                        $checked = $dep['aproved'] ? 'checked' : '';
                                                        # <input type=\"hidden\" name=\"register[".$dep['id']."][id]\" value=\"".$dep['id']."\" />
                                                        # name=\"register[".$dep['id']."][aproved]\" value="1"
                                                        $approveInput = "
                                                    <span class='switch'>
                                                    <input class=\"checkbox-status hidden\" disabled=\"disabled\" type=\"checkbox\" ".$checked."  /><span class=\"checkbox-slider\"></span>
                                                    </span>";
                                                        #  . ' ' . $approveInput
                                                        $items[] = "<p>" . $dep['name'] . "</p>";
                                                    endforeach;
                                                endif;
                                                $items = implode("", $items);
                                                if( $item['owner'] == 'no' ):
                                                ?>
                                                    <a href="javascript:;" data-type="get" data-url="<?php
                                                    echo esc_attr( add_query_arg($load_params, admin_url('admin-ajax.php') ) ); ?>" data-id="<?= $item['id'];
                                                    ?>" class="action-assign" title="<?php _e('Triển khai cấp dưới', TPL_DOMAIN_LANG);
                                                    ?>" data-list-target="#list-assign-kpis-<?php echo $tabkey; ?>"
                                                       data-action="assign" data-toggle="modal" data-target="#tpl-department-assign-target-<?= $tabkey; ?>">
                                                        <span class="fa fa-group"></span>
                                                    </a><?php
                                                    echo "<div class='children'>{$items}</div>";
                                                    endif;
                                                endif;
                                            ?></div>
                                        </td>
                                        <td class="column-8 kpi-percent align-center"><?php echo $item['percent']; ?></td>

                                        <td class="column-9 align-center">
                                            <?php
                                            if( $item['required'] == 'no' ):
                                            $disabled = '';
                                            $name = 'name="kpiIDs['.$item['id'].']"';
                                            $valueInput = $item['id'];
                                            $textSendApproved = '';
                                            if( $item['status'] == KPI_STATUS_RESULT ) {
                                                $disabled = 'disabled';
	                                            $name ='';
	                                            $valueInput = '';
                                                $textSendApproved = __('Đã duyệt', TPL_DOMAIN_LANG);
                                            } elseif( $item['status'] == KPI_STATUS_PENDING ) {
                                                $disabled = 'disabled';
                                                $name ='';
                                                $valueInput = '';
                                                $textSendApproved = __('Chờ duyệt', TPL_DOMAIN_LANG);
                                            }
                                            ?>
                                            <label class="switch">
                                            <input class="checkbox-status hidden" type="checkbox" <?= $name; ?> <?= $disabled; ?> <?php
                                            echo ( ($item['status'] != KPI_STATUS_DRAFT) ? 'checked="checked"' : '');
                                            ?> value="<?= $valueInput; ?>"/>
                                            <span class="checkbox-slider"></span>
                                            </label>
                                            <span><?= $textSendApproved; ?></span>
                                            <?php endif;  ?>
                                        </td>

                                        <td class="column-10 align-center">
                                            <?php if( $firstYear && empty($parentItem) && ($item['status'] == KPI_STATUS_DRAFT) ):
                                                $remove_params = [
                                                    'action' => 'remove_target_department',
                                                    'year_id' => $firstYear['id'],
                                                    'id' => $item['id'],
                                                    'kpi_type' => $tabData['type'],
                                                    '_wpnonce' => wp_create_nonce('remove-target'),
                                                ];
                                                $load_params = [
                                                    'action' => 'load-kpi-target',
                                                    'id' => $firstYear['id'],
                                                    'kpi_id' => $item['id'],
                                                    'kpi_type' => $tabData['type'],
                                                    'chart_id' => $item['chart_id'],
                                                    'bank_id' => $item['bank_id'],
                                                    'year' => $tabData['time_value'],
                                                    '_wpnonce' => wp_create_nonce('load-kpi-target'),
                                                ];
                                                ?>
                                                <a href="javascript:;" data-type="get" data-url="<?php
                                                echo esc_attr( add_query_arg($load_params, admin_url('admin-ajax.php') ) );
                                                ?>" data-id="<?= $item['id'];
                                                ?>" class="action-edit" title="<?php _e('Chỉnh sửa', TPL_DOMAIN_LANG);
                                                ?>" data-action="edit" data-toggle="modal" data-target="#tpl-department-add-target-<?= $tabkey; ?>">
                                                    <span class="glyphicon glyphicon-pencil"></span>
                                                </a>
                                                <a href="<?php echo esc_attr( add_query_arg($remove_params, admin_url('admin-ajax.php') ) ); ?>" title="Xóa"
                                                   data-ajax="remove-department-target" data-method="post"><span class="glyphicon glyphicon-trash"></span></a>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php

                                endforeach;
                            endif;
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="9">
                                    <?php if( $totalPercent < 100.0 ): ?>
                                    <a href="javascript:;" data-toggle="modal"
                                        data-target="#tpl-department-add-target-<?php echo $tabkey; ?>" data-show="show-row"><i
                                        class="fa fa-plus-circle" data-acrion="add"
                                        aria-hidden="true"></i> <?php _e('Thêm mục tiêu', TPL_DOMAIN_LANG); ?>
                                    </a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                        <div class="action-bot">
                            <button type="submit" class="btn-brb-default btn-save-and-public btn-action-item"
                                name="btn-save-public"><i class="fa fa-floppy-o"
                                  aria-hidden="true"></i> <?php _e('Lưu & triển khai', TPL_DOMAIN_LANG); ?>
                            </button>
                            <button type="button" class="btn-brb-default btn-print btn-action-item" name="btn-print"><i
                                        class="fa fa-print"
                                        aria-hidden="true"></i> <?php _e('Xuất báo cáo', TPL_DOMAIN_LANG); ?></button>
                        </div>
                    </form>

                    <div id="tpl-department-assign-target-<?php echo $tabkey; ?>" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
                        <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
                              action="<?php echo admin_url('admin-ajax.php?action=assign_department_target') ?>" method="post">
                            <input type="hidden" name="_wpnonce" value="<?php echo $assign_target; ?>">
                            <input type="hidden" name="kpiID" value="">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"><?php echo $tabData['title']; ?> : Triển khai xuống cấp dưới</h4>
                                </div>
                                <div class="modal-body clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group input-group-list">
                                            <div class="department-container hidden"></div>
                                            <ul id="list-assign-kpis-<?php echo $tabkey; ?>" class="list-group"></ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group form-plans">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="plan-<?php echo $tabkey; ?>">Nhập kế hoạch</span>
                                                <input required type="text" readonly="readonly" class="plan form-control"
                                                       placeholder="Kế hoạch phòng" aria-describedby="plan-<?php echo $tabkey; ?>">
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="unit-<?php echo $tabkey; ?>">Đơn vị tính</span>
                                                <input required type="text" readonly="readonly" class="unit form-control"
                                                       placeholder="Đơn vị tính" aria-describedby="unit-<?php echo $tabkey; ?>">
                                            </div>
                                            <div class="input-group group-date">
                                                <span class="input-group-addon"
                                                      id="receive-<?php echo $tabkey; ?>">Thời điểm nhận kết quả</span>
                                                <input required type="text" readonly="readonly" class="receive form-control"
                                                       placeholder="Thời điểm nhận kết quả" data-lang="vi" data-format="<?php echo KPI_FORMAT_DATE; ?>"
                                                       data-min-date="01-01-<?php echo $tabData['time_value']; ?>"
                                                    <?php /* data-max-date="" */ ?>
                                                       data-group-date=".group-date" data-timepicker="false"
                                                       data-btn-date=".input-group-addon.date-btn"
                                                       data-ctrl-date aria-describedby="receive-<?php echo $tabkey; ?>">
                                                <span class="input-group-addon date-btn">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="percent-<?php echo $tabkey; ?>">Trọng số tối đa</span>
                                                <input required type="number" min="0" readonly="readonly" class="percent form-control"
                                                       placeholder="Trọng số tối đa" aria-describedby="percent-<?php echo $tabkey; ?>">
                                                <span class="input-group-addon">%</span>
                                            </div>
                                        </div>
                                        <div class="form-group phongban-list">
                                            <table class="table phongban">
                                                <thead>
                                                <tr>
                                                    <th class="chuc-danh" style="width: 50%">Chức Danh</th>
                                                    <th class="ke-hoach" style="width: 25%">Kế Hoạch</th>
                                                    <th class="trong-so" style="width: 25%">Trọng Số (%)</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if( !empty($charts) ):
                                                foreach($charts as $chart_id => $chart): ?>
                                                    <tr>
                                                        <td class="chuc-danh chuc-danh-<?php echo $chart['id']; ?>">
                                                            <label class="form-control switch chuc_danh" id="chart_<?php echo "{$chart['id']}_id-{$tabkey}"; ?>">
                                                                <input class="checkbox-status" name="chuc_danh<?php echo "[{$chart['id']}][id]"; ?>" value="<?php echo $chart['id']; ?>" type="checkbox">
                                                                <span class="checkbox-slider round"></span>
                                                                <span class="text"><?php echo $chart['name']; ?></span>
                                                            </label>
                                                        </td>
                                                        <td class="ke-hoach ke-hoach-<?php echo $chart['id']; ?>">
                                                            <input class="form-control chuc-danh-plan" name="chuc_danh<?php echo "[{$chart['id']}][plan]"; ?>" size="10" type="text" value="">
                                                        </td>
                                                        <td class="trong-so trong-so-<?php echo $chart['id']; ?>">
                                                            <input class="form-control chuc-danh-percent" name="chuc_danh<?php echo "[{$chart['id']}][percent]"; ?>" size="10" type="text" value="">
                                                        </td>
                                                    </tr>
                                                <?php endforeach; endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <button type="submit" class="btn btn-primaryy">Lưu</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <?php if( $totalPercent < 100.0 ): ?>
                    <div id="tpl-department-add-target-<?php echo $tabkey; ?>" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
                        <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
                              action="<?php echo admin_url('admin-ajax.php?action=add_department_target') ?>" method="post">
                            <input type="hidden" name="_wpnonce" value="<?php echo $add_target; ?>">
                            <input type="hidden" name="type" value="<?php echo $tabData['type']; ?>">
                            <input type="hidden" name="year_id" value="<?php echo $firstYear ? $firstYear['id'] : 0; ?>">
                            <input type="hidden" name="time_type" value="<?php echo $tabData['time_type']; ?>">
                            <input type="hidden" name="time_value" value="<?php echo $tabData['time_value']; ?>">
                            <input type="hidden" name="chart_id" value="<?php echo $user->orgchart_id; ?>">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"><?php echo $tabData['title']; ?>
                                        : <span data-title-add="Thêm KPI" data-title-edit="Chỉnh sửa KPI"></span></h4>
                                </div>
                                <div class="modal-body clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group input-group input-group-select">
                                            <label class="input-group-addon" for="year_id" id="chart_name-<?php echo $tabkey; ?>">Chỉ mục KPI: </label>
                                            <select data-loading=".form-group.input-group.input-group-select"
                                                    data-live-search="true" data-target="#list-kpis-<?php echo $tabkey; ?>"
                                                    aria-describedby="chart_name-<?php echo $tabkey; ?>"
                                                    class="kpi-category selectpicker input-select" name="cat"><?php
                                                kpiRenderCategoryDropdown('', true, '--- ');
                                                ?></select>
                                        </div>
                                        <div class="form-group input-group-list">
                                            <div class="department-container hidden"></div>
                                            <ul id="list-kpis-<?php echo $tabkey; ?>" class="list-group"></ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group form-plans">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="plan-<?php echo $tabkey; ?>">Nhập kế hoạch</span>
                                                <input required type="text" name="plan" class="form-control"
                                                       placeholder="Kế hoạch" aria-describedby="plan-<?php echo $tabkey; ?>">
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="unit-<?php echo $tabkey; ?>">Đơn vị tính</span>
                                                <select data-none-selected-text="Đơn vị tính" id="unit-<?php echo $tabkey; ?>" class="select-unit selectpicker" name="unit">
		                                            <?php
		                                            $unitArr = getUnit();
		                                            foreach( $unitArr as $kUnit => $vUnit ) {
			                                            echo sprintf( "<option value='%s'>%s</option>", $kUnit, $vUnit );
		                                            }
		                                            ?>
                                                </select>
                                            </div>
                                            <div class="input-group group-date">
                                                <span class="input-group-addon"
                                                      id="receive-<?php echo $tabkey; ?>">Thời điểm nhận kết quả</span>
                                                <input required type="text" name="receive" class="form-control"
                                                       placeholder="Thời điểm nhận kết quả" data-lang="vi" data-format="<?php echo KPI_FORMAT_DATE; ?>"
                                                       data-min-date="01-01-<?php echo $tabData['time_value']; ?>"
                                                    <?php /* data-max-date="" */ ?>
                                                       data-group-date=".group-date" data-timepicker="false"
                                                       data-btn-date=".input-group-addon.date-btn"
                                                       data-ctrl-date aria-describedby="receive-<?php echo $tabkey; ?>">
                                                <span class="input-group-addon date-btn">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="percent-<?php echo $tabkey; ?>">Trọng số tối đa</span>
                                                <input required type="number" min="0" name="percent" class="form-control"
                                                       placeholder="Trọng số tối đa" aria-describedby="percent-<?php echo $tabkey; ?>">
                                                <span class="input-group-addon">%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <button type="submit" class="btn btn-primary" data-text-add="Thêm KPI" data-text-edit="Lưu">Lưu</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>

        <div id="tpl-department-kpi-members" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
            <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded" method="post" onsubmit="return false"
                action="<?php echo admin_url('admin-ajax.php?action=asign_kpi_members&_wpnonce='. wp_create_nonce('asign_kpi_members') ); ?>">
                <input type="hidden" name="kpi_id" value="">
                <input type="hidden" name="bank_id" value="">
                <input type="hidden" name="kpi_type" value="">
                <input type="hidden" name="chart_id" value="">
                <input type="hidden" name="year_id" value="">
                <input type="hidden" name="department_plan" value="">
                <input type="hidden" name="department_unit" value="">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><?php _e('Danh sách nhân viên phòng ', TPL_DOMAIN_LANG); ?><span class="phongban-name"></span></h4>
                    </div>
                    <div class="modal-body clearfix">
                        <div class="title-muctieu-phong col-12">
                            <div class="col-lg-2 col-md-3 col-sm-5">Mục Tiêu: </div>
                            <div class="col-lg-10 col-md-9 col-sm-7">
                                <div class="col-12 loai-muctieu-kpi"></div>
                                <div class="col-12 muctieu-kpi"></div>
                            </div>
                        </div>
                        <div class="title-kehoach-phong col-12">
                            <div class="col-lg-2 col-md-3 col-sm-5">Kế hoạch phòng: </div>
                            <div class="col-lg-10 col-md-9 col-sm-7 department-plan-unit"></div>
                        </div>
                        <div class="title-nhanvien-phong col-12">
                            <div class="col-lg-10 col-md-10 col-sm-9">Danh sách nhân viên tham gia thực hiện mục tiêu:</div>
                            <div class="col-lg-2 col-md-2 col-sm-3 danhsach"></div>
                        </div>
                        <table class="table list-members">
                            <thead>
                            <thead>
                            <th class="idx" width="5%" align="center">STT</th>
                            <th class="fullname" width="20%" align="center">Họ Tên</th>
                            <th class="id" width="10%" align="center">Mã Số NV</th>
                            <th class="cname" width="20%" align="center">Vị Trí</th>
                            <th class="select" align="center"><label class="switch">
                                    <span class="text">Chọn</span>
                                    <input type="checkbox" class="checkbox-status"><span class="checkbox-slider round"></span>
                                </label>
                            </th>
                            <th width="20%" align="center">Mục tiêu<br>ảnh hưởng</th>
                            <th class="personal_plan" width="20%" align="center">Kế Hoạch cá nhân (<span class="plan-unit"></span>)</th>
                            <th class="percent" width="20%" align="center">Trọng số (%)</th>
                            </thead>
                            <tbody>
                                <tr class="template">
                                    <td class="idx"></td>
                                    <td class="fullname"></td>
                                    <td class="id"></td>
                                    <td class="cname"></td>
                                    <td class="total_percent"></td>
                                    <td class="select"><label class="switch">
                                            <input type="checkbox" name="users[IDX][ID]" value="" class="input-id checkbox-status"><span class="checkbox-slider round"></span>
                                        </label>
                                    </td>
                                    <td align="center" class="influence">
                                        <label class="switch">
                                            <input type="checkbox" name="users[IDX][influence]" value="" class="input-id checkbox-status"><span class="checkbox-slider round"></span>
                                        </label>
                                    </td>
                                    <td class="plan"><input disabled class="input-plan" type="number" name="users[IDX][plan]" value=""></td>
                                    <td class="percent">
                                        <input disabled class="input-percent" type="number" name="users[IDX][percent]" value="">
                                        <input class="input-chart_id" type="hidden" name="users[IDX][chart_id]" value="">
                                        <input class="input-kpi_item_id" type="hidden" name="users[IDX][kpi_item_id]" value="">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <button type="submit" class="btn btn-primaryy">Chọn</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
