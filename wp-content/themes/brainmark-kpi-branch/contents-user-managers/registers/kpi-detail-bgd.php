<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/8/18
 * Time: 17:44
 */

global $wpdb;
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';

$add_target = wp_create_nonce('add_target');
$user = wp_get_current_user();
$orgchart = user_load_orgchart($user);
$member_role = $orgchart ? $orgchart->role : '';

$year_status = ( ($member_role == 'bgd') || $user->has_cap('level_9') ) ? '' : KPI_YEAR_STATUS_PUBLISH;
$year_list = kpi_get_year_list( $GLOBALS['parent_year_id'], $year_status );
$firstYear = kpi_get_first_year_role( kpi_get_first_year( $GLOBALS['parent_year_id'], $year_status ) );


$tabs = [
    'finance' => [
        'title' => __('Finance', TPL_DOMAIN_LANG),
        'type' => 'Finance',
        'percent' => $firstYear ? $firstYear['finance'] : '',
        'img' => 'tai-chinh.png',
        'time_type' => 'YEAR',
        'time_value' => TIME_YEAR_VALUE,
        'year_id' => $firstYear
    ],
    'customer' => [
        'title' => __('Customer', TPL_DOMAIN_LANG),
        'type' => 'Customer',
        'percent' => $firstYear ? $firstYear['customer'] : '',
        'img' => 'customer.png',
        'time_type' => 'YEAR',
        'time_value' => TIME_YEAR_VALUE,
        'year_id' => $firstYear
    ],
    'operate' => [
        'title' => __('Operate', TPL_DOMAIN_LANG),
        'type' => 'Operate',
        'percent' => $firstYear ? $firstYear['operate'] : '',
        'img' => 'van-hanh.png',
        'time_type' => 'YEAR',
        'time_value' => TIME_YEAR_VALUE,
        'year_id' => $firstYear
    ],
    'development' => [
        'title' => __('Development', TPL_DOMAIN_LANG),
        'type' => 'Development',
        'percent' => $firstYear ? $firstYear['development'] : '',
        'img' => 'phat-trien.png',
        'time_type' => 'YEAR',
        'time_value' => TIME_YEAR_VALUE,
        'year_id' => $firstYear
    ]
];

?>

<div class="clearfix"></div>
<div class="block-detail block-result-kpi-detail">
    <div id="room-target-kpi" data-type="bgd" data-content-management="" style="display: block">
        <ul id="" class="list-room-target-kpi clearfix nav nav-pills">
            <?php
            foreach ($tabs as $tabkey => $tabData):
                $cls = ('finance' == $tabkey) ? 'active' : '';
                ?>
                <li class="room-target-kpi-item <?php echo $cls; ?> room-<?php echo $tabkey; ?>">
                    <a href="#room-<?php echo $tabkey; ?>" class="" title="" data-toggle="tab" data-tab-type="bgd">
                        <img src="<?php echo THEME_URL; ?>/assets/images/<?php echo $tabData['img']; ?>"/>
                        <?php echo $tabData['title']; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>

        <div class="list-detail tab-content clearfix">
            <?php foreach ($tabs as $tabkey => $tabData):
                $charts = kpi_get_list_org_charts($user->orgchart_id, false);
                $cls = ('finance' == $tabkey) ? 'active' : '';
                ?>
                <div class="tab-pane <?php echo $cls; ?>" id="room-<?php echo $tabkey; ?>">
                    <div class="list-detail">
                        <div class="top-list-detail">
                            <h3 class="title">
                                <?php echo $tabData['title']; ?>
                                <a href="javascript:;" data-year="<?php echo esc_json_attr($tabData['year_id']); ?>" data-toggle="modal"
                                   data-target="#tpl-kpi-year" data-action="update" >
                                    <span class="icon-percent" contenteditable="false" data-kpi-percent="<?php echo $tabkey; ?>"><?php echo $tabData['percent']; ?>%</span>
                                    <i class="glyphicon glyphicon-pencil hidden" aria-hidden="true"></i>
                                </a>
                            </h3>
                        </div>
                    </div>
                    <form data-approved="" action="<?php echo admin_url('admin-ajax.php?action=ceo_approved_kpi'); ?>" class="form-trien-khai-kpi form-trien-khai-kpi-bgd"
                          id="form-<?php echo $tabkey; ?>" method="post" enctype="application/x-www-form-urlencoded"
                          onsubmit="return false">
                        <input value="<?php echo wp_create_nonce('ceo_approved_kpi'); ?>" type="hidden" name="_wpnonce">
                        <input value="<?php echo $tabData['type']; ?>" type="hidden" name="type">
                        <input type="hidden" name="_wp_http_referer" value="<?php esc_attr_e( wp_unslash( $_SERVER['REQUEST_URI'] ) ); ?>#room-<?= $tabkey; ?>">
                        <table class="table-list-detail table-managerment-target-kpi" cellpadding="0" cellspacing="0">
                            <colgroup style="max-width: 100px"></colgroup>
                            <colgroup style=""></colgroup>
                            <colgroup style="max-width: 140px"></colgroup>
                            <colgroup style="max-width: 100px"></colgroup>
                            <colgroup style="min-width: 270px"></colgroup>
                            <colgroup style="max-width: 100px"></colgroup>
                            <colgroup style="max-width: 85px"></colgroup>
                            <thead>
                            <tr>
                                <th class="column-1 kpi-id align-center"><?php _e('Mã KPI', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-2 kpi-content"><?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-3 kpi-company_plan"><?php _e('Kế hoạch<br>công ty', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-5 kpi-receive align-center"><?php _e('Thời điểm<br>nhận kết quả', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-6 align-center"><?php _e('Chức Danh', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-7 align-center"><?php _e('Trọng số (%)', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-8 align-center"><?php _e('Thao Tác', TPL_DOMAIN_LANG); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $totalPercent = 0.0;
                            global $wpdb;
                            $groupDatas = kpi_get_list($tabData['type'], $tabData['time_type'], $tabData['time_value'], $user->ID, $user->orgchart_id);

                            if (!empty($groupDatas)):
                                foreach ($groupDatas as &$item):
                                    $totalPercent += doubleval($item['percent']);

                                    $subcharts = kpi_get_list_of_charts($tabData['type'], $tabData['time_type'], $tabData['time_value'], 0, $user->orgchart_id, 0, $item['id']);
                                    # echo "<pre>"; var_dump(__LINE__, __FILE__, "{$wpdb->last_query}\n{$wpdb->last_error}"); echo "</pre>";
                                    ?>
                                    <tr>
                                        <td class="column-1 kpi-id align-center">
                                            <?php echo $item['id']; ?>
                                        </td>
                                        <td class="column-2 kpi-content">
                                            <?php echo $item['post_title']; ?>
                                        </td>
                                        <td class="column-3 kpi-plan"><?php echo "{$item['plan']} <br>".getUnit($item['unit']).""; ?></td>
                                        <td class="column-5 kpi-receive align-center"><?php echo substr(kpi_format_date($item['receive']), 0, 10); ?></td>
                                        <td class="column-6 kpi-departments"><?php
                                            $items = [];
                                            if( !empty($subcharts) ):
                                                foreach($subcharts as $dep):
                                                    $checked = $dep['aproved'] ? 'checked' : '';
                                                    $approveInput = "
                                                    <input type=\"hidden\" name=\"register[".$dep['id']."][id]\" value=\"".$dep['id']."\" />
                                                    <label class='switch'>
                                                    <input class=\"checkbox-status hidden\" name=\"register[".$dep['id']."][aproved]\" type=\"checkbox\" ".$checked." value=\"1\" /><span class=\"checkbox-slider\"></span>
                                                    </label>";
                                                    $items[] = "<p>" . $dep['name'] . ' ' . $approveInput . "</p>";
                                                endforeach;
                                            endif;
                                            echo implode("", $items);
                                            ?></td>
                                        <td class="column-7 kpi-percent align-center"><?php echo $item['percent']; ?></td>
                                        <td class="column-8 align-center">
                                            <?php if( $firstYear ):
                                                $remove_params = [
                                                    'action' => 'remove-ceo-target',
                                                    'id' => $firstYear['id'],
                                                    'kpi_id' => $item['id'],
                                                    'kpi_type' => $tabData['type'],
                                                    '_wpnonce' => wp_create_nonce('remove-ceo-target'),
                                                ];
                                                $load_params = [
                                                    'action' => 'load-ceo-target',
                                                    'id' => $firstYear['id'],
                                                    'kpi_id' => $item['id'],
                                                    'kpi_type' => $tabData['type'],
                                                    'chart_id' => $item['chart_id'],
                                                    'bank_id' => $item['bank_id'],
                                                    'year' => $tabData['time_value'],
                                                    '_wpnonce' => wp_create_nonce('load-ceo-target'),
                                                ];
                                                ?>
                                            <a href="javascript:;" data-type="get" data-url="<?php echo esc_attr( add_query_arg($load_params, admin_url('admin-ajax.php') ) ); ?>" data-id="<?= $item['id'];
                                            ?>" class="action-edit" title="<?php _e('Chỉnh sửa', TPL_DOMAIN_LANG);
                                            ?>" data-edit="" data-toggle="modal" data-kpi-type="<?php echo $tabData['type']; ?>" data-target="#tpl-ceo-target-<?= $tabkey; ?>">
                                                <span class="glyphicon glyphicon-pencil"></span>
                                            </a>
                                            <a href="<?php echo esc_attr( add_query_arg($remove_params, admin_url('admin-ajax.php') ) ); ?>" title="Xóa"
                                               data-ajax="remove-ceo-target" data-method="get"><span class="glyphicon glyphicon-trash"></span></a>
                                            <?php endif; ?>
                                        </td>

                                    </tr>
                                <?php endforeach;
                            endif;
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="8">
                                    <?php if( $totalPercent < 100.0 ): ?>
                                    <a href="javascript:;" data-toggle="modal"
                                       data-target="#tpl-ceo-target-<?php echo $tabkey; ?>" data-show="show-row"><i
                                                class="fa fa-plus-circle"
                                                aria-hidden="true"></i> <?php _e('Thêm mục tiêu', TPL_DOMAIN_LANG); ?>
                                    </a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                        <div class="action-bot">
                            <button type="submit" class="btn-brb-default btn-save-and-public btn-action-item"
                                    name="btn-save-public"><i class="fa fa-floppy-o"
                                                              aria-hidden="true"></i> <?php _e('Lưu & triển khai', TPL_DOMAIN_LANG); ?>
                            </button>
                            <button type="button" class="btn-brb-default btn-print btn-action-item" name="btn-print"><i
                                        class="fa fa-print"
                                        aria-hidden="true"></i> <?php _e('Xuất báo cáo', TPL_DOMAIN_LANG); ?></button>
                        </div>
                    </form>
                    <?php #if( $totalPercent < 100.0 ): ?>
                    <div id="tpl-ceo-target-<?php echo $tabkey; ?>" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
                        <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
                              action="<?php echo admin_url('admin-ajax.php?action=add_ceo_target') ?>" method="post">
                            <input type="hidden" name="_wpnonce" value="<?php echo $add_target; ?>">
                            <input type="hidden" name="type" value="<?php echo $tabData['type']; ?>">
                            <input type="hidden" name="year_id" value="<?php echo $firstYear ? $firstYear['id'] : 0; ?>">
                            <input type="hidden" name="time_type" value="<?php echo $tabData['time_type']; ?>">
                            <input type="hidden" name="time_value" value="<?php echo $tabData['time_value']; ?>">
                            <input type="hidden" name="chart_id" value="<?php echo $user->orgchart_id; ?>">
                            <input type="hidden" name="kpiID" value="">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"><?php echo $tabData['title']; ?> : <span data-title-add="Thêm KPI" data-title-edit="Chỉnh sửa KPI">Thêm KPI</span></h4>
                                </div>
                                <div class="modal-body clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="input-group input-group-select">
                                            <label class="input-group-addon" for="cat_name-<?php echo $tabkey; ?>" id="cat_name-<?php echo $tabkey; ?>_lb">Nhóm mục tiêu: </label>
                                            <select data-loading=".input-group.input-group-select" data-target="select#list-kpis-<?php echo $tabkey; ?>"
                                                    aria-describedby="cat_name-<?php echo $tabkey; ?>_lb" id="cat_name-<?php echo $tabkey; ?>"
                                                    data-live-search="true" data-live-search-placeholder="Lọc Nhóm Mục Tiêu"
                                                    data-none-selected-text="Chưa chọn"
                                                    data-none-results-text="Không có kết quả cho {0}"
                                                    data-select-all-text="Chọn tất cả"
                                                    data-deselect-all-text="Bỏ chọn"
                                                    class="kpi-category selectpicker input-select" name="cat"><?php
                                                kpiRenderCategoryDropdown('', true, '--- ');
                                                ?></select>
                                        </div>
                                        <div class="input-group input-select">
                                            <label class="input-group-addon" for="list-kpis-<?php echo $tabkey; ?>"
                                                   id="kpi_name-<?php echo $tabkey; ?>">Mục tiêu: </label>
                                            <select aria-describedby="kpi_name-<?php echo $tabkey; ?>" id="list-kpis-<?php echo $tabkey; ?>"
                                                    data-live-search="true" data-live-search-placeholder="Lọc Mục Tiêu"
                                                    data-none-selected-text="Chưa chọn"
                                                    data-none-results-text="Không có kết quả cho {0}"
                                                    data-select-all-text="Chọn tất cả"
                                                    data-deselect-all-text="Bỏ chọn"
                                                    class="kpis selectpicker input-select" name="bank_id"></select>
                                        </div>
                                        <div class="input-group input-percent">
                                            <label class="input-group-addon" id="percent-<?php echo $tabkey; ?>">Trọng số tối đa</label>
                                            <input type="text" name="percent" class="form-control"
                                                   placeholder="Trọng số tối đa" aria-describedby="percent-<?php echo $tabkey; ?>">
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group form-plans">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="plan-<?php echo $tabkey; ?>">Nhập kế hoạch</span>
                                                <input type="text" name="plan" class="form-control"
                                                       placeholder="Kế hoạch công ty" aria-describedby="plan-<?php echo $tabkey; ?>">
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="unit-<?php echo $tabkey; ?>">Đơn vị tính</span>
                                                <select data-none-selected-text="Đơn vị tính" id="unit-<?php echo $tabkey; ?>" class="select-unit selectpicker" name="unit">
                                                    <?php
                                                        $unitArr = getUnit();
                                                        foreach( $unitArr as $kUnit => $vUnit ) {
	                                                        echo sprintf( "<option value='%s'>%s</option>", $kUnit, $vUnit );
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="input-group group-date">
                                                <span class="input-group-addon"
                                                      id="receive-<?php echo $tabkey; ?>">Thời điểm nhận kết quả</span>
                                                <input type="text" name="receive" class="form-control"
                                                       placeholder="Thời điểm nhận kết quả" data-lang="vi" data-format="<?php echo KPI_FORMAT_DATE; ?>"
                                                       data-min-date="01-01-<?php echo $tabData['time_value']; ?>"
                                                       <?php /* data-max-date="" */ ?>
                                                       data-group-date=".group-date" data-timepicker="false"
                                                       data-btn-date=".input-group-addon.date-btn"
                                                       data-ctrl-date aria-describedby="receive-<?php echo $tabkey; ?>">
                                                <span class="input-group-addon date-btn">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <input type="hidden" class="" name="kpi_name">
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group phongban-list">
                                            <table class="table phongban">
                                                <thead>
                                                <tr>
                                                    <th class="chuc-danh" style="width: 20%">Chức Danh</th>
                                                    <th class="nhom-muc-tieu" style="width: 20%">Nhóm Mục Tiêu</th>
                                                    <th class="muc-tieu" style="width: 30%">Mục Tiêu</th>
                                                    <th class="ke-hoach" style="width: 10%">Kế Hoạch</th>
                                                    <th class="ke-hoach" style="width: 10%">Đơn Vị Tính</th>
                                                    <th class="trong-so" style="width: 10%">Trọng Số (%)</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if( !empty( $charts ) ):
                                                    # kpi_get_years_from_chart_ids( $firstYear ? $firstYear['id'] : 0, );

                                                    $year_id = $firstYear ? $firstYear['id'] : 0;
                                                    $chuc_danh_years = $firstYear ? $firstYear['chuc_danh'] : [];

                                                    foreach($charts as $chart_id => $chart):
                                                        if( $chart['is_leaf'] ) continue;
                                                        ?>
                                                        <tr>
                                                            <td class="chuc-danh chuc-danh-<?php echo $chart['id']; ?>">
                                                                <label class="form-control switch chuc_danh" id="chart_<?php echo "{$chart['id']}_id-{$tabkey}"; ?>">
                                                                    <input class="checkbox-status" name="chuc_danh<?php echo "[{$chart['id']}][id]"; ?>"
                                                                           value="0" type="checkbox">
                                                                    <span class="checkbox-slider round"></span>
                                                                    <span class="text"><?php echo $chart['name']; ?></span>
                                                                </label>
                                                                <input type="hidden" class="remove-<?php echo $chart['id']; ?>" name="chuc_danh<?php echo "[{$chart['id']}][deleted]"; ?>" value="">
                                                            </td>
                                                            <td class="nhom-muc-tieu nhom-muc-tieu-<?php echo $chart['id']; ?>">
                                                                <select data-loading="tr" data-target="select#list-kpis-<?php echo "{$tabkey}-{$chart['id']}"; ?>"
                                                                        data-live-search="true" data-live-search-placeholder="Lọc Nhóm Mục Tiêu"
                                                                        data-none-selected-text="Chưa chọn"
                                                                        data-none-results-text="Không có kết quả cho {0}"
                                                                        data-select-all-text="Chọn tất cả"
                                                                        data-deselect-all-text="Bỏ chọn"
                                                                        id="cat_name_<?php echo $chart['id']; ?>" class="kpi-category selectpicker input-select"><?php
                                                                    kpiRenderCategoryDropdown('', true, '--- ');
                                                                    ?></select>
                                                            </td>
                                                            <td class="muc-tieu muc-tieu-<?php echo $chart['id']; ?>">
                                                                <select data-live-search="true" data-live-search-placeholder="Lọc Mục Tiêu"
                                                                        class="kpis selectpicker input-select" name="chuc_danh[<?php echo $chart['id']; ?>][bank_id]"
                                                                        data-none-selected-text="Chưa chọn"
                                                                        data-none-results-text="Không có kết quả cho {0}"
                                                                        data-select-all-text="Chọn tất cả"
                                                                        data-deselect-all-text="Bỏ chọn"
                                                                        id="list-kpis-<?php echo "{$tabkey}-{$chart['id']}"; ?>"></select>
                                                            </td>
                                                            <td class="ke-hoach ke-hoach-<?php echo $chart['id']; ?>">
                                                                <input class="form-control chuc-danh-plan" name="chuc_danh<?php echo "[{$chart['id']}][plan]"; ?>" size="10" type="text" value="">
                                                            </td>
                                                            <td class="unit unit-<?php echo $chart['id']; ?>">
                                                                <select data-none-selected-text="Đơn vị tính" class="chuc-danh-unit selectpicker" name="chuc_danh<?php echo "[{$chart['id']}][unit]"; ?>">
                                                                    <?php
                                                                    $unitArr = getUnit();
                                                                    foreach( $unitArr as $kUnit => $vUnit ) {
                                                                        echo sprintf( "<option value='%s'>%s</option>", $kUnit, $vUnit );
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </td>
                                                            <td class="trong-so trong-so-<?php echo $chart['id']; ?>">
                                                                <input class="form-control chuc-danh-percent" name="chuc_danh<?php echo "[{$chart['id']}][percent]"; ?>" size="10" type="text" value="">
                                                                <input type="hidden" class="year_id" name="chuc_danh<?php echo "[{$chart['id']}][year_id]"; ?>"
                                                                       value="<?php echo isset($chuc_danh_years[$chart['id']]) ? $chuc_danh_years[$chart['id']]['id'] : $year_id; ?>">
                                                            </td>
                                                        </tr>
                                                    <?php endforeach;
                                                endif;
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <button type="submit" class="btn btn-primaryy" data-title-add="Thêm" data-title-edit="Lưu">Lưu</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php #endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>