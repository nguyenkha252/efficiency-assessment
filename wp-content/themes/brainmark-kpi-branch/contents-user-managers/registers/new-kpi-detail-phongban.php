<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/8/18
 * Time: 17:44
 */

global $wpdb, $orgchart, $member_role, $user, $chartRoot, $chartCurrent, $chartParent, $chartParentName, $chartCurrentName;
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-orgchart.php';
$uid = !empty( $_GET['uid'] ) ? (int) wp_unslash($_GET['uid']) : 0;
$cid = !empty($_GET['cid']) ? (int) wp_unslash($_GET['cid']) : 0;
$add_target = wp_create_nonce('add_target');
#$user = wp_get_current_user();
$user = get_user_by( "ID", $uid );
$orgchart = user_load_orgchart($user);
$member_role = $orgchart ? $orgchart->role : '';

if( $orgchart->id != $cid ){
    $orgchart = kpi_orgchart_get_by('id', $cid);
    if( $orgchart ){
        $member_role = $orgchart->role;
    }
}
$year_status = ( ($member_role == 'bgd') || $user->has_cap('level_9') ) ? '' : KPI_YEAR_STATUS_PUBLISH;

$firstYear = kpi_get_first_year( $GLOBALS['parent_year_id'], $year_status );
$firstYear = kpi_get_first_year_role( $firstYear );
if( empty( $firstYear ) ){
    $firstYear = year_get_year_by_onlevel( $orgchart, TIME_YEAR_VALUE, $GLOBALS['parent_year_id'] );
}

$yearInfo = $GLOBALS['yearInfo'];
if( empty( $yearInfo ) ){
	$yearInfo  = year_get_year_by_chart_id( $chartCurrent['parent'], TIME_YEAR_VALUE );
}
$chartParent = $GLOBALS['chartParent'];
#$time_values = $GLOBALS['time_values'];
$time_values = TIME_YEAR_VALUE;
$tabs = [
    'finance' => [
        'title' => __('Finance', TPL_DOMAIN_LANG),
        'type' => 'Finance',
        'percent' => $yearInfo ? $yearInfo['finance'] : '',
        'img' => 'tai-chinh.png',
        'time_type' => 'YEAR',
        'time_value' => $time_values,
        'year_id' => $yearInfo
    ],
    'customer' => [
        'title' => __('Customer', TPL_DOMAIN_LANG),
        'type' => 'Customer',
        'percent' => $yearInfo ? $yearInfo['customer'] : '',
        'img' => 'customer.png',
        'time_type' => 'YEAR',
        'time_value' => $time_values,
        'year_id' => $yearInfo
    ],
    'operate' => [
        'title' => __('Operate', TPL_DOMAIN_LANG),
        'type' => 'Operate',
        'percent' => $yearInfo ? $yearInfo['operate'] : '',
        'img' => 'van-hanh.png',
        'time_type' => 'YEAR',
        'time_value' => $time_values,
        'year_id' => $yearInfo
    ],
    'development' => [
        'title' => __('Development', TPL_DOMAIN_LANG),
        'type' => 'Development',
        'percent' => $yearInfo ? $yearInfo['development'] : '',
        'img' => 'phat-trien.png',
        'time_type' => 'YEAR',
        'time_value' => $time_values,
        'year_id' => $yearInfo
    ]
];

require_once THEME_DIR . '/inc/render-kpi-item-row_managers.php';
$members = kpi_get_users(['room_id' => $orgchart->id, 'chart_id' => 0, 'trang' => 1, 'limit' => 100, 'tim' => '']);
$members = $members['items'];

?>
<div class="clearfix"></div>
<div class="block-detail block-result-kpi-detail">
    <div id="room-target-kpi" data-type="bgd" data-content-management="" style="display: block">
        <ul id="" class="list-room-target-kpi clearfix nav nav-pills">
            <?php
            foreach ($tabs as $tabkey => $tabData):
                $cls = ('finance' == $tabkey) ? 'active' : '';
                ?>
                <li class="room-target-kpi-item <?php echo $cls; ?> room-<?php echo $tabkey; ?>">
                    <a href="#room-<?php echo $tabkey; ?>" class="" title="" data-toggle="tab" data-tab-type="room">
                        <img src="<?php echo THEME_URL; ?>/assets/images/<?php echo $tabData['img']; ?>"/>
                        <?php echo $tabData['title']; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>

        <div class="list-detail tab-content clearfix">
            <?php
            $orgchart_id = !empty($_GET['cid']) ? (int)$_GET['cid'] : $user->orgchart_id;
            $charts = kpi_get_list_org_charts($orgchart_id, false);

            foreach ($tabs as $tabkey => $tabData):
                $cls = ('finance' == $tabkey) ? 'active' : '';
                $actionColumn = $chartParent ? 7 : 8;

                $totalPercent = 0.0;
                global $wpdb;
                # $groupDatas = kpi_get_list_if_not_and_get_from_parent($tabData['type'], $tabData['time_type'], $tabData['time_value'], 0, $orgchart_id, 1, [], KPI_STATUS_RESULT);
                $groupDatas = kpi_get_list_if_not_and_get_from_parent($tabData['type'], $tabData['time_type'], $tabData['time_value'], 0, $orgchart_id, 'all', [], KPI_STATUS_RESULT);
                $groupDatasOwner = kpi_get_list_if_not_and_get_from_parent($tabData['type'], $tabData['time_type'], $tabData['time_value'], $user->ID, $orgchart_id, 'all', [], '', 'no');

                if( !empty($groupDatasOwner) ) {
                    foreach ($groupDatasOwner as $id => &$item) {
                        if( !isset($groupDatas[$id]) ) {
                            $groupDatas[$id] = $item;
                        }
                    }
                }
                # $groupDatas = array_merge( $groupDatas, $groupDatasOwner );

                ?>
                <div class="tab-pane <?php echo $cls; ?>" id="room-<?php echo $tabkey; ?>">
                    <div class="list-detail">
                        <div class="top-list-detail">
                            <h3 class="title">
                                <?php echo $tabData['title']; ?>
                                <a href="javascript:;"<?php if(USER_IS_ADMIN): ?> data-year="<?php echo esc_json_attr($tabData['year_id']); ?>" data-toggle="modal"
                                   data-target="#tpl-kpi-year" data-action="update"<?php endif; ?>>
                                    <span class="icon-percent" data-kpi-percent="<?php echo $tabkey; ?>"><?php echo $tabData['percent']; ?>%</span>
                                    <i class="glyphicon glyphicon-pencil hidden" aria-hidden="true"></i>
                                </a>
                            </h3>
                        </div>
                    </div>
                    <form data-approved="" action="<?php echo admin_url('admin-ajax.php?action=department_send_approved'); ?>" class="form-trien-khai-kpi form-trien-khai-kpi-bgd"
                          id="form-<?php echo $tabkey; ?>" method="post" enctype="application/x-www-form-urlencoded"
                          onsubmit="return false">
                        <input value="<?php echo wp_create_nonce('department_send_approved'); ?>" type="hidden" name="_wpnonce">
                        <input value="<?php echo $tabData['type']; ?>" type="hidden" name="type">
                        <input type="hidden" name="_wp_http_referer" value="<?php esc_attr_e( wp_unslash( $_SERVER['REQUEST_URI'] ) ); ?>#room-<?= $tabkey; ?>">
                        <table class="table-list-detail table-managerment-target-kpi" cellpadding="0" cellspacing="0">
                            <thead>
                            <tr>
                                <th class="column-1 kpi-id align-center"><?php _e('Mã KPI', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-2 kpi-content"><?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-3 kpi-percent align-center"><?php _e('Trọng số (%)', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-4 kpi-plan"><?php echo sprintf(__('Kế hoạch<br>%s', TPL_DOMAIN_LANG), $chartParent ? $chartParent['room'] : 'Công Ty'); ?></th>
                                <th class="column-5 kpi-receive align-center"><?php _e('Thời điểm<br>nhận kết quả', TPL_DOMAIN_LANG); ?></th>
                                <?php if( $chartParent ): ?>
                                    <th class="column-6 kpi-aproved align-center"><?php _e('Trạng Thái', TPL_DOMAIN_LANG); ?></th>
                                <?php endif; ?>
                                <th class="column-<?php echo ($actionColumn-1); ?> kpi-members align-center"><?php _e('Cá nhân thực hiện', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-<?php echo $actionColumn; ?> kpi-action align-center"><?php _e('Thao Tác', TPL_DOMAIN_LANG); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($groupDatas)):
                                foreach ($groupDatas as &$item):
                                    if( $item['required'] == 'yes' && $item['owner'] == 'yes' ){
                                    continue;
                                    }
                                    $totalPercent += doubleval($item['percent']);
                                    $html = kpi_render_ceo_row_item_managers($item, $tabData['type'], $chartCurrent, $chartParent, $members, $user);
                                    echo $html;
                                endforeach;
                            endif;

                            $addData = [
	                            'id' => '',
	                            'year_id' => $yearInfo ? $yearInfo['id'] : 0,
	                            'formula_id' => 0,
	                            'bank_id' => '',
	                            'unit' => '',
	                            'actual_gain' => '',
	                            'status_result' => 'draft',
	                            'name' => $chartCurrent ? $chartCurrent['name'] : '',
	                            'receive' => date('31-12-Y', time()),
	                            'percent' => 0,
	                            'parent' => 0,
	                            'chart_id' => $_GET['cid'],
	                            'user_id' => $user->ID,
	                            'aproved' => 1,
	                            'plan' => '',
	                            'post_title' => '',
	                            'type' => $tabData['type'],
	                            'status' => KPI_STATUS_DRAFT,
	                            'required' => 'yes',
	                            'owner' => 'no',
	                            'create_by_node' => 'node_start',
	                            'bank_parent' => 0,
	                            'influence' => '',
                            ];
                            $load_params = [
	                            'action' => 'load-parent-bank-list',
	                            'kpi_type' => $tabData['type'],
	                            'cat_slug' => $tabkey,
	                            'year_id' => $yearInfo ? $yearInfo['id'] : 0,
	                            'cid' => $_GET['cid'],
	                            'uid' => $_GET['uid'],
	                            'year' => $yearInfo ? $yearInfo['year'] : 0,
	                            '_wpnonce' => wp_create_nonce('load-parent-bank-list'),
                            ];
                            ?>
                            <tr class="editer-item hidden" data-tabkey="<?php echo $tabkey; ?>" data-add-info="<?php echo esc_json_attr($addData); ?>">
                                <td class="column-1 kpi-id align-center">
                                    <input type="hidden" name="postdata[id]" value="">
                                    <span class="item-id"></span>
                                </td>
                                <td class="column-2 kpi-content">
                                    <input type="text" class="form-control" name="postdata[post_title]" placeholder="Nội dung mục tiêu" value="">
                                    <div class="input-group">
                                        <span class="input-group-addon">Công thức tính: </span><!-- data-container="this.parentNode.parentNode.parentNode" -->
                                        <select class="selectpicker" data-live-search="true" name="formula_id" data-use-bootstrap="">
                                            <option value="0" ><?php _e('Không áp dụng công thức',TPL_DOMAIN_LANG); ?></option>
				                            <?php
				                            require_once THEME_DIR . '/inc/lib-formulas.php';
				                            $listFormulas = get_list_formula();
				                            if( !empty( $listFormulas ) && !is_wp_error( $listFormulas ) ) {
					                            foreach ( $listFormulas as $key => $formula ){
						                            $formulaTitle = $formula->title;
						                            $formulaID = $formula->ID;
						                            echo "<option value='".esc_attr($formulaID)."'>".esc_attr($formulaID)." - " . esc_attr( $formulaTitle ) . "</option>";
					                            }
				                            }
				                            ?>
                                        </select>
                                    </div>
		                            <?php
		                            if( !(!empty($chartCurrent) && !empty($chartRoot) && $chartCurrent['id'] == $chartRoot['id']) ):

			                            $items = kpi_get_list($tabData['type'], $tabData['time_type'], $tabData['time_value'], 0, $chartParent ? $chartParent['id'] : $user->orgchart_id);
			                            ?>
                                        <div class="input-group group-influence">
                                            <label class="form-control switch influence" id="influence-<?php echo "{$item['id']}_id-{$tabkey}"; ?>">
                                                <input class="checkbox-status" name="postdata[influence]"
                                                       value="1" type="checkbox">
                                                <span class="checkbox-slider round"></span>
                                                <span class="text">Mục tiêu ảnh hưởng</span>
                                            </label>
                                        </div>
                                        <h4>Mục Tiêu <?php echo $chartParentName; ?></h4>
                                        <ul class="list-group">
			                            <?php
			                            foreach($items as $item):
				                            $item['receive'] = substr(kpi_format_date($item['receive']), 0, 10);
				                            ?>
                                            <li class="list-group-item" data-kpi-parent="<?php echo esc_json_attr($item); ?>">
                                                <label class="form-control switch bank-parent" id="bank-parent-<?php echo "{$item['id']}_id-{$tabkey}"; ?>">
                                                    <input class="checkbox-status" name="postdata[parent]"
                                                           value="<?php echo $item['id']; ?>" type="radio">
                                                    <span class="checkbox-slider round"></span>
                                                    <span class="text"><?php echo str_replace(str_split("\|"), "", $item['post_title']); ?></span>
                                                </label>
                                            </li>
			                            <?php endforeach; ?>
                                        </ul><?php
		                            endif;
		                            ?>
                                </td>
                                <td class="column-3 kpi-percent align-center">
                                    <input class="form-control" placeholder="Tỷ trọng (%)"
                                           name="postdata[percent]" value=""></td>
                                <td class="column-4 kpi-plan">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="postdata[plan]" placeholder="Kế hoạch" value="">
                                        <select name="postdata[unit]" class="selectpicker" data-none-selected-text="Chưa chọn đơn vị">
                                            <option value=""> - Đơn Vị Tính - </option>
				                            <?php
				                            $units = getUnit();
				                            foreach($units as $val => $text):
					                            echo sprintf('<option value="%s">%s</option>', $val, $text);
				                            endforeach;
				                            ?>
                                        </select>
                                    </div>
                                </td>

                                <td class="column-5 kpi-receive">
                                    <div class="input-group">
                                        <input class="form-control" name="postdata[receive]" placeholder="Ngày hết hạn" value=""
                                               data-lang="vi" btn-date=".input-group-addon.date-btn" data-group-date=".input-group"
                                               data-format="<?php echo KPI_FORMAT_DATE; ?>"
                                               data-min-date="01-01-<?php echo $tabData['time_value'][0]; ?>"
				                            <?php /* data-max-date="" */ ?>
                                               data-group-date=".group-date" data-timepicker="false"
                                               data-btn-date=".input-group-addon.date-btn"
                                               data-ctrl-date>
                                        <span class="input-group-addon date-btn">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </td>
                                <td class="column-6 kpi-aproved align-center">
		                            <?php if( $yearInfo ):
			                            $load_params = [
				                            'action' => 'save-ceo-target',
				                            'kpi_type' => $tabData['type'],
				                            'cat_slug' => $tabkey,
				                            'year_id' => $yearInfo ? $yearInfo['id'] : 0,
				                            'cid' => $_GET['cid'],
				                            'uid' => $_GET['uid'],
				                            'year' => $yearInfo ? $yearInfo['year'] : 0,
				                            '_wpnonce' => wp_create_nonce('save-ceo-target'),
			                            ];
			                            ?>
                                        <a href="javascript:;" data-type="post"
                                           data-url="<?php echo esc_attr( add_query_arg($load_params, admin_url('admin-ajax.php') ) ); ?>"
                                           class="action-save btn btn-sm" title="<?php _e('Lưu', TPL_DOMAIN_LANG);
			                            ?>" data-kpi-type="<?php echo $tabData['type']; ?>">Lưu</a>
		                            <?php endif; ?>
                                </td>
                                <td></td>
                                <td class="column-<?php echo $actionColumn; ?> kpi-action align-center">
		                            <?php if( $yearInfo ): ?>
                                        <a href="javascript:;" class="action-cancel btn btn-sm">Bỏ qua</a>
		                            <?php endif; ?>
                                </td>
                            </tr>
                            </tbody>
                            <?php
                            /*
                            <tfoot>
                            <tr>
                                <td colspan="8">
                                    <a href="javascript:;" class="action-add" data-ac="add" data-show="show-row"><i
                                                class="fa fa-plus-circle"
                                                aria-hidden="true"></i> <?php _e('Thêm mục tiêu', TPL_DOMAIN_LANG); ?>
                                    </a>
                                    <div class="container-message hidden"></div>
                                </td>
                            </tr>
                            </tfoot>
                            */ ?>
                        </table>
                        <?php /*
                        <div class="action-bot">
                            <button type="submit" class="btn-brb-default btn-save-and-public btn-action-item"
                                    name="btn-save-public"><i class="fa fa-floppy-o"
                                                              aria-hidden="true"></i> <?php _e('Gửi xét duyệt', TPL_DOMAIN_LANG); ?>
                            </button>

                            <button type="button" class="btn-brb-default btn-print btn-action-item" name="btn-print"><i
                                        class="fa fa-print"
                                        aria-hidden="true"></i> <?php _e('Xuất báo cáo', TPL_DOMAIN_LANG); ?></button>
                        </div> */?>
                    </form>
                </div>
            <?php endforeach; ?>
        </div>
        <div id="tpl-department-kpi-members" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
            <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded" method="post" onsubmit="return false"
                  action="<?php echo admin_url('admin-ajax.php?action=asign_kpi_members&nam='.$_GET['nam'].'&_wpnonce='. wp_create_nonce('asign_kpi_members') ); ?>">
                <input type="hidden" name="uid" value="<?= $_GET['uid']; ?>">
                <input type="hidden" name="kpi_id" value="">
                <input type="hidden" name="bank_id" value="">
                <input type="hidden" name="kpi_type" value="">
                <input type="hidden" name="chart_id" value="">
                <input type="hidden" name="year_id" value="">
                <input type="hidden" name="plan" value="">
                <input type="hidden" name="unit" value="">
                <input type="hidden" name="_wp_http_referer" value="">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><?php _e('Danh sách nhân viên ', TPL_DOMAIN_LANG); ?><span class="phongban-name"></span></h4>
                    </div>
                    <div class="modal-body clearfix">
                        <div class="title-muctieu-phong col-12">
                            <div class="col-lg-2 col-md-3 col-sm-5">Mục Tiêu: </div>
                            <div class="col-lg-10 col-md-9 col-sm-7">
                                <div class="col-12 loai-muctieu-kpi"></div>
                                <div class="col-12 muctieu-kpi"></div>
                            </div>
                        </div>
                        <div class="title-kehoach-phong col-12">
                            <div class="col-lg-2 col-md-3 col-sm-5">Kế hoạch phòng: </div>
                            <div class="col-lg-10 col-md-9 col-sm-7 department-plan-unit"></div>
                        </div>
                        <div class="title-nhanvien-phong col-12">
                            <div class="col-lg-10 col-md-10 col-sm-9">Danh sách nhân viên tham gia thực hiện mục tiêu:</div>
                            <div class="col-lg-2 col-md-2 col-sm-3 danhsach"></div>
                        </div>
                        <table class="table list-members">
                            <thead>
                            <th class="idx" width="5%" align="center">STT</th>
                            <th class="fullname" width="20%" align="center">Họ Tên</th>
                            <th class="id" width="10%" align="center">Mã Số NV</th>
                            <th class="cname" width="20%" align="center">Vị Trí</th>
                            <th class="select" align="center"><label class="switch">
                                    <span class="text">Chọn</span>
                                    <input type="checkbox" class="checkbox-status"><span class="checkbox-slider round"></span>
                                </label>
                            </th>
                            <th width="20%" align="center">Mục tiêu<br>ảnh hưởng</th>
                            <th class="personal_plan" width="20%" align="center">Kế Hoạch cá nhân (<span class="plan-unit"></span>)</th>
                            <th class="percent" width="20%" align="center">Trọng số (%)</th>
                            </thead>
                            <tbody>
                            <tr class="template">
                                <td class="idx"></td>
                                <td class="fullname"></td>
                                <td class="id"></td>
                                <td class="cname"></td>
                                <td class="select">
                                    <label class="switch">
                                        <input type="checkbox" name="users[IDX][ID]" value="" class="input-id checkbox-status"><span class="checkbox-slider round"></span>
                                    </label>
                                </td>
                                <td align="center" class="influence">
                                    <label class="switch">
                                        <input type="checkbox" name="users[IDX][influence]" value="" class="input-id checkbox-status"><span class="checkbox-slider round"></span>
                                    </label>
                                </td>
                                <td class="plan"><input class="input-plan" type="text" name="users[IDX][plan]" value=""></td>
                                <td class="percent">
                                    <input class="input-percent" type="number" name="users[IDX][percent]" value="">
                                    <input class="input-chart_id" type="hidden" name="users[IDX][chart_id]" value="">
                                    <input class="input-kpi_item_id" type="hidden" name="users[IDX][kpi_item_id]" value="">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <button type="submit" class="btn btn-primaryy">Chọn</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>