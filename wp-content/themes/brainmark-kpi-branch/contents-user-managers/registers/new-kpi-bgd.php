<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 26/12/2017
 * Time: 23:13
 */

global $tabsTitlem, $wpdb, $orgchart, $member_role, $user, $chartRoot, $chartCurrent, $chartParent, $chartParentName, $chartCurrentName;

/**
 * Kiểm tra nêú không tìm thấy chức danh thì thông báo cho người dùng
 * Kiểm tra nếu năm của người dùng đang chọn chưa được tạo thì thông báo ra màn hình (Tránh trường hợp công ty chưa tạo KPI mà có thể vào từng chức danh tạo KPI cho năm đó)
 * Nếu chưa tạo năm thì chỉ màn hình của admin thấy được chức năng tạo KPI cho năm đó, những chức danh khác sẽ thông báo KPI của năm đó chưa được tạo.
*/
$yearForYear = year_get_year_this_of_congty( TIME_YEAR_VALUE );
if( empty( $chartCurrent ) || ( empty( $yearForYear ) && ( $chartCurrent['parent'] != 0 || !user_is_manager() ) ) ){
    if( empty( $chartCurrent ) ){
        echo "<h3 class='notification error'>Không tìm thấy chức danh</h3>";
    }elseif( empty( $yearForYear ) ){
	    echo "<h3 class='notification error'>Không tìm thấy KPI của năm ". TIME_YEAR_VALUE .". Vui lòng tạo KPI cho năm ". TIME_YEAR_VALUE ."</h3>";
    }
}else {

	require_once THEME_DIR . '/inc/lib-kpis.php';
	require_once THEME_DIR . '/inc/lib-years.php';
	require_once THEME_DIR . '/inc/lib-orgchart.php';

	$member_role = $orgchart ? $orgchart->role : '';
	if ( empty( $member_role ) ) {
		$member_role = '';
	} else {
		$member_role = strtolower( $member_role );
	}
	$year_status = USER_IS_ADMIN ? '' : KPI_YEAR_STATUS_PUBLISH;

#$year_list = kpi_get_year_list( 0, $year_status );
	$year_list = year_get_list_year_ceo();
#print_r( $year_list );
# echo '<pre class="role-dang-ky-kpi" style="display: block !important;">'; var_dump(__LINE__, $year_list, $firstYear); echo '</pre>';

	$chartRoot    = $GLOBALS['chartRoot'];
	$chartParent  = $GLOBALS['chartParent'];
	$chartCurrent = $GLOBALS['chartCurrent'];
	$yearInfo     = $GLOBALS['yearInfo'];
	$yearEmpty    = $GLOBALS['yearEmpty'];
	if ( !empty( $yearInfo ) && $yearInfo['year'] != TIME_YEAR_VALUE ) {
		#$yearInfo = [];
		$yearInfo  = year_get_year_by_chart_id( $chartCurrent['id'], TIME_YEAR_VALUE );
		$yearEmpty = [];
	}

	$user_id     = 0;
	$orgchart_id = $chartCurrent ? $chartCurrent['id'] : ( $orgchart ? $orgchart->id : 0 );
#$time_values = $GLOBALS['time_values'];
	$time_values = TIME_YEAR_VALUE;
	$groups      = [
		'finance'     => [
			'key'     => 'finance',
			'title'   => __( 'Finance', TPL_DOMAIN_LANG ),
			'percent' => $yearInfo ? $yearInfo['finance'] : '0',
			'time'    => TIME_YEAR_VALUE,
			'year_id' => $yearInfo ? $yearInfo : $yearEmpty,
			# 'items' => kpi_get_list('Finance', 'YEAR', $time_values, $user_id, $orgchart_id)
			'items'   => kpi_get_list_if_not_and_get_from_parent( 'Finance', 'YEAR', $time_values, $user_id, $orgchart_id )
		],
		'operate'     => [
			'key'     => 'operate',
			'title'   => __( 'Operate', TPL_DOMAIN_LANG ),
			'percent' => $yearInfo ? $yearInfo['operate'] : '0',
			'time'    => TIME_YEAR_VALUE,
			'year_id' => $yearInfo ? $yearInfo : $yearEmpty,
			# 'items' => kpi_get_list('Operate', 'YEAR', $time_values, $user_id, $orgchart_id)
			'items'   => kpi_get_list_if_not_and_get_from_parent( 'Operate', 'YEAR', $time_values, $user_id, $orgchart_id )
		],
		'customer'    => [
			'key'     => 'customer',
			'title'   => __( 'Customer', TPL_DOMAIN_LANG ),
			'percent' => $yearInfo ? $yearInfo['customer'] : '0',
			'time'    => TIME_YEAR_VALUE,
			'year_id' => $yearInfo ? $yearInfo : $yearEmpty,
			# 'items' => kpi_get_list('Customer', 'YEAR', $time_values, $user_id, $orgchart_id)
			'items'   => kpi_get_list_if_not_and_get_from_parent( 'Customer', 'YEAR', $time_values, $user_id, $orgchart_id )
		],
		'development' => [
			'key'     => 'development',
			'title'   => __( 'Development', TPL_DOMAIN_LANG ),
			'percent' => $yearInfo ? $yearInfo['development'] : '0',
			'time'    => TIME_YEAR_VALUE,
			'year_id' => $yearInfo ? $yearInfo : $yearEmpty,
			# 'items' => kpi_get_list('Development', 'YEAR', $time_values, $user_id, $orgchart_id)
			'items'   => kpi_get_list_if_not_and_get_from_parent( 'Development', 'YEAR', $time_values, $user_id, $orgchart_id )
		],
	];


	function render_group_list( $item, $yearInfo ) {
		?>
        <div class="content-list-register-<?php echo $item['key']; ?> content-list-register">
            <table class="table-block-list-register table-list-<?php echo $item['key']; ?>" cellspacing="0"
                   cellpadding="0">
                <thead>
                <tr>
                    <th class="column-1-2" colspan="2"><?php echo $item['title']; ?></th>
                    <th class="percent column-3">
                        <a href="javascript:;"<?php if ( USER_IS_ADMIN ): ?> data-action="update" data-year="<?php echo esc_json_attr( $item['year_id'] ); ?>" data-toggle="modal"
                            data-target="#tpl-kpi-year"<?php endif; ?>>
                            <span class="icon-percent"
                                  data-kpi-percent="<?php echo $item['key']; ?>"><?php echo $item['percent']; ?>%</span>
                            <i class="glyphicon glyphicon-pencil hidden" aria-hidden="true"></i>
                        </a>
                        <input type="text" class="txt-percent-kpi hidden" value="<?php echo $item['percent']; ?>"/>
                    </th>
                </tr>
                </thead>
                <tbody>
				<?php
				$items = $item['items'];
				# for( $i = 0, $n = count($items); $i < $n; $i++ ) {
				$i = 0;
				foreach ( $items as $id => $kpi ) {
					?>
                    <tr data-id="<?php echo $kpi['id']; ?>">
                        <td class="column-1"><?= ( $i + 1 ); ?>.</td>
                        <td class="column-2"><?php echo( str_replace(str_split("\|"), "", $kpi['post_title']) ); ?></td>
                        <td class="column-3"><?= $kpi['percent']; ?>%</td>
                    </tr>
					<?php
					$i ++;
				}
				?>
                </tbody>
            </table>
        </div>
		<?php
	}

	global $kpi_contents_parts;
	$current_url = $kpi_contents_parts['apply-bsc-kpi-system']['url'];

	$current_url = add_query_arg( $_GET, $current_url );
	$alias_url   = remove_query_arg( 'cid', $current_url );

	$current_url = remove_query_arg( 'name', $current_url );
	$current_url = remove_query_arg( 'quy', $current_url );
	$current_url = remove_query_arg( 'thang', $current_url );

	$chartRoot = isset( $GLOBALS['chartRoot'] ) ? $GLOBALS['chartRoot'] : null;
# $chartParent = isset($GLOBALS['chartParent']) ? $GLOBALS['chartParent'] : null;
	$chartCurrent = isset( $GLOBALS['chartCurrent'] ) ? $GLOBALS['chartCurrent'] : null;
# echo '<pre>'; var_dump($chartRoot, $chartCurrent); echo '</pre>';

	?>

    <div id="block-register-kpi" class="block-register-kpi-bgd blocik-item block-shadow border-radius-default">
        <div class="header-register-kpi header-block container-fluid">
            <div class="name-block1 col-md-6">
                <h2 class=""><?php echo _Objective_KPI; //echo sprintf( __( 'Mục tiêu KPI', TPL_DOMAIN_LANG ) ); ?></h2>
				<?php
                # Nếu chưa tạo KPI của năm thì ẩn dropdown chức danh.
               /* if ( ! empty( $yearInfo ) || !empty( $yearEmpty ) ): ?>
                    <div class="selectpicker-all">
						<?php
						$listOrgchart = orgchart_get_list_orgchart_not_nv();
						?>
                        <select class="selectpicker" data-change-alias="true"
                                data-redirect-url="<?php echo esc_attr( $alias_url ); ?>">
							<?php
                            if( !empty($listOrgchart) ):
							foreach ( $listOrgchart as $k => $item ){
								if( $item->level > 1 )
									continue;
                                if( $item->parent == 0 ){
                                    $name = $item->name;
                                }else{
                                    $name = $item->room;
                                }
								echo "<option value='{$item->id}'>{$name}</option>";
							}
							endif;
                            ?>
                        </select>
                    </div>
				<?php endif; */ ?>
            </div>
            <form class="col-md-6" enctype="application/x-www-form-urlencoded" method="get"
                  action="<?php echo esc_attr( $current_url ); ?>">
				<?php
                /*
				if ( ( $chartCurrent && $chartRoot && ( $chartRoot['id'] == $chartCurrent['id'] ) ) ):
					?>
                    <a href="javascript:;" class="create-new-year" id="create-kpi-new-year" data-year-id="new"
                         data-toggle="modal"
                         data-target="#tpl-kpi-year"><i class="fa fa-plus-circle"
                                                        aria-hidden="true"></i></a><?php endif; */ ?>
                <input type="hidden" name="type" value="<?php echo isset( $_GET['type'] ) ? $_GET['type'] : '' ?>"/>
                <input type="hidden" name="cid" value="<?php echo isset( $_GET['cid'] ) ? $_GET['cid'] : '' ?>"/>
                <button type="submit" class="btn btn-sm btn-primary view-new-year"><?php echo _LBL_VIEW;?></button>
                <div class="selectpicker-nam">
                    <div class="input-group">
                        <label class="input-group-addon" for="select-for-nam"
                               id="select-for-nam-id"><?php echo _LBL_YEAR; //_e( 'Năm', TPL_DOMAIN_LANG ); ?></label>
                        <select class="selectpicker form-control select-for-nam" id="select-for-nam" name="nam"
                                aria-describedby="select-for-nam-id">
                            <option value="">-- Năm --</option>
							<?php foreach ( $year_list as $item ):
								$selected = ( isset( $_GET['nam'] ) && ( $_GET['nam'] == $item['year'] ) ) ? 'selected="selected"' : '';
								echo sprintf( '<option value="%s" %s>Năm %s</option>', $item['year'], $selected, $item['year'] );
							endforeach;
							?>
                        </select>
                    </div>
                </div>
            </form>
        </div>
        <div class="top-register-kpi">
            <div class="col-md-4 register-kpi-1">
				<?php get_template_part( 'contents/block-' . $member_role . '/chart' ); ?>
            </div>
            <div class="col-md-8">
                <div class="col-md-6 register-kpi-2"><?php
					render_group_list( $groups['finance'], $yearInfo );
					render_group_list( $groups['operate'], $yearInfo );
					?></div>
                <div class="col-md-6 register-kpi-3"><?php
					render_group_list( $groups['customer'], $yearInfo );
					render_group_list( $groups['development'], $yearInfo );
					?></div>

            </div>
        </div>

		<?php get_template_part( 'contents-user-managers/registers/new-kpi', 'detail-' . $member_role ); ?>
    </div>
	<?php
}
