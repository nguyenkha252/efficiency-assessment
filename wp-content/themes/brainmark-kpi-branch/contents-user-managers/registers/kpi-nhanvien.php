<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 26/12/2017
 * Time: 23:13
 */
require_once THEME_DIR . '/inc/lib-orgchart.php';
require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-kpis.php';
$definedBgColor4Column = [
        'tai-chinh' => '#008fd5',
        'khach-hang' => '#ee3f3f',
        'van-hanh' => '#7fc45d',
        'phat-trien' => '#fea055',
];
$user = wp_get_current_user();
$fullName = $user->first_name .' '. $user->last_name;
$fullName = !empty( trim( $fullName ) ) ? $fullName : $user->display_name;
$orgchart = user_load_orgchart($user);
$memberRoleName = $orgchart ? $orgchart->name : '';
$member_role = $orgchart ? $orgchart->role : '';
$year = $_GET['nam'];
if( empty($member_role) ) {
    $member_role = '';
} else {
    $member_role = '-' . strtolower($member_role);
}
$year_list = kpi_get_year_list($GLOBALS['parent_year_id'], KPI_YEAR_STATUS_PUBLISH);
$firstYear = kpi_get_first_year($GLOBALS['parent_year_id'], KPI_YEAR_STATUS_PUBLISH);
$kpiForStaff = get_kpi_by_year_orgchart_not_month($firstYear['year'], $user->ID);
$data = createDataChartNode( $kpiForStaff );
?>
<div id="block-register-kpi" class=" block-shadow border-radius-default">
    <div class="header-register-kpi header-block">
        <div class="for-date">
            <div class="for-year">
                <label for="select-for-year"><?php echo _LBL_YEAR; //_e('Năm', TPL_DOMAIN_LANG); ?></label>
                <select class="select-for-year selectpicker" id="select-for-year" name="nam">
                    <?php foreach ($year_list as $item):
                        $selected = $item['year'] == TIME_YEAR_VALUE ? 'selected="selected"' : '';
                        echo sprintf('<option value="%s" %s>%s</option>', $item['year'], $selected, $item['year']);
                    endforeach;
                    ?>
                </select>
            </div>
        </div>
        <div class="name-block">
            <h2><?php echo sprintf( __('Đăng ký mục tiêu năm %s', TPL_DOMAIN_LANG),TIME_YEAR_VALUE ); ?></h2>
        </div>
    </div>
    <div class="top-register-kpi role-member">
        <div class="head-member">
            <?php /* <div class="expried-register"><?php echo sprintf( __('Hạn chót đăng ký: %s', TPL_DOMAIN_LANG), '20/12/2017' ); ?></div> */ ?>
            <div class="title-usermember">
                <?php
                echo sprintf( __('Nhân viên: %s - Chức danh: %s', TPL_DOMAIN_LANG), $fullName, $memberRoleName );
                ?>
            </div>
        </div>
        <div class="col-md-4 register-kpi-1">
            <div class="canvas-item">
                <canvas width="200" height="200" class="data-report" data-type="pie" data-datasets="<?php echo esc_json_attr($data); ?>"></canvas>
            </div>
        </div>
        <div class="col-md-8 register-kpi-2">
            <table class="table-annotate-list" cellspacing="0" cellpadding="0">
                <tbody>
                    <?php
                        if( !empty( $kpiForStaff ) ):
                            foreach ( $kpiForStaff as $keyKPI => $itemKPI ):
                                $classNode = getColorStar( $itemKPI['create_by_node'] );
                                $star = !empty( $classNode ) ? "<i class=\"fa fa-star {$classNode}\" aria-hidden=\"true\"></i>" : "";
                                ?>
                                <tr>
                                    <td class="column-1 column-text-center">
                                        <span class="color-annotate " style="background-color: <?php echo getColorColumnNode( $keyKPI ); ?>;"></span>
                                    </td>
                                    <td class="column-2 column-text-center">
                                        <?php echo $star; ?>
                                    </td>
                                    <td class="column-3">
                                        <?php esc_attr_e( $itemKPI['post_title'] ); ?>
                                    </td>
                                    <td class="column-4"><?php esc_attr_e($itemKPI['plan']);?> <?php esc_attr_e( getUnit( $itemKPI['unit'] ) );  ?></td>
                                    <td class="column-5 column-text-center"><?php esc_attr_e( $itemKPI['percent'] ); ?>%</td>
                                </tr>
                        <?php
                            endforeach;
                        endif;
                    ?>
                </tbody>
            </table>
        </div>

    </div>

    <?php get_template_part( 'contents/registers/kpi', 'detail' . $member_role ); ?>
</div>
