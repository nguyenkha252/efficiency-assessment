<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 26/12/2017
 * Time: 23:13
 */

global $tabsTitlem, $wpdb, $orgchart, $member_role, $user, $chartRoot, $chartCurrent, $chartParent, $chartParentName, $chartCurrentName;

require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-orgchart.php';
$cid = !empty($_GET['cid']) ? (int) wp_unslash($_GET['cid']) : 0;
if( $cid > 0 ){
    $orgchart = kpi_orgchart_get_by("id", $cid );
    #print_r( $orgchart->role );
    if( !empty( $orgchart ) ){
        $member_role = $orgchart->role;
    }
    #echo $member_role;
}else {
	$member_role = $orgchart ? $orgchart->role : '';
	if ( empty( $member_role ) ) {
		$member_role = '';
	} else {
		$member_role = strtolower( $member_role );
	}
}
$year_status = ( ($member_role == 'bgd') || $user->has_cap('level_9') ) ? '' : KPI_YEAR_STATUS_PUBLISH;
#$year_list = kpi_get_year_list( 0, $year_status );
$year_list = year_get_list_year_ceo();

# echo '<pre class="role-dang-ky-kpi" style="display: block !important;">'; var_dump(__LINE__, $year_list, $firstYear); echo '</pre>';

$chartRoot = $GLOBALS['chartRoot'];
$chartParent = $GLOBALS['chartParent'];
$chartCurrent = $GLOBALS['chartCurrent'];
$yearInfo = $GLOBALS['yearInfo'];
$yearEmpty    = $GLOBALS['yearEmpty'];
if( empty( $yearInfo ) ){
	$yearInfo  = year_get_year_by_chart_id( $chartCurrent['parent'], TIME_YEAR_VALUE );
}

$user_id = 0;
$orgchart_id = $chartCurrent ? $chartCurrent['id'] : ($orgchart ? $orgchart->id : 0);

$time_values = $GLOBALS['time_values'];
$groups = [
        'finance' => [
            'key' => 'finance',
            'type' => 'Finance',
            'title' => __('Finance', TPL_DOMAIN_LANG),
            'percent' => $yearInfo ? $yearInfo['finance'] : '',
            'time' => TIME_YEAR_VALUE,
            'year_id' => $yearInfo
        ],
        'operate' => [
            'key' => 'operate',
            'type' => 'Operate',
            'title' => __('Operate', TPL_DOMAIN_LANG),
            'percent' => $yearInfo ? $yearInfo['operate'] : '',
            'time' => TIME_YEAR_VALUE,
            'year_id' => $yearInfo
        ],
        'customer' => [
            'key' => 'customer',
            'type' => 'Customer',
            'title' => __('Customer', TPL_DOMAIN_LANG),
            'percent' => $yearInfo ? $yearInfo['customer'] : '',
            'time' => TIME_YEAR_VALUE,
            'year_id' => $yearInfo
        ],
        'development' => [
            'key' => 'development',
            'type' => 'Development',
            'title' => __('Development', TPL_DOMAIN_LANG),
            'percent' => $yearInfo ? $yearInfo['development'] : '',
            'time' => TIME_YEAR_VALUE,
            'year_id' => $yearInfo
        ],
];


function render_group_list($item, $yearInfo, $orgchart_id) {
    #$user = wp_get_current_user();
    ?>
    <div class="content-list-register-<?php echo $item['key']; ?> content-list-register">
        <table class="table-block-list-register table-list-<?php echo $item['key']; ?>" cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <th class="column-1-2" colspan="2"><?php echo $item['title']; ?></th>
                <th class="percent column-3">
                    <a href="javascript:;"<?php if(USER_IS_ADMIN): ?> data-action="update" data-year="<?php echo esc_json_attr($yearInfo); ?>" data-toggle="modal"
                       data-target="#tpl-kpi-year"<?php endif; ?>>
                        <span class="icon-percent" data-kpi-percent="<?php echo $item['key']; ?>"><?php echo $item['percent']; ?>%</span>
                        <i class="glyphicon glyphicon-pencil hidden" aria-hidden="true"></i>
                    </a>
                    <input type="text" class="txt-percent-kpi hidden" value="<?php echo $item['percent']; ?>" />
                </th>
            </tr>
            </thead>
            <tbody>
            <?php
            $user = wp_get_current_user();
            $items = kpi_get_list_if_not_and_get_from_parent($item['type'], 'YEAR', $item['time'], 0, $orgchart_id, 'all', [], KPI_STATUS_RESULT);
            $groupDatasOwner = kpi_get_list_if_not_and_get_from_parent($item['type'], 'YEAR', $item['time'], $user->ID, $orgchart_id, 'all', [], KPI_STATUS_RESULT, 'no');
            if( !empty($groupDatasOwner) ) {
                foreach ($groupDatasOwner as $id => &$item) {
                    if( !isset($items[$id]) ) {
                        $items[$id] = $item;
                    }
                }
            }
            $i = 0;
            foreach( $items as $id => $kpi ) {
	            $classNode = getColorStar( $kpi['create_by_node'] );
	            if( $kpi['required'] == 'yes' ){
                    $required = "<i class=\"fa fa-star ".esc_attr($classNode)."\" aria-hidden=\"true\"></i>";
	            }
                #$required = ($kpi['required'] == 'yes') ? '<span class="glyphicon glyphicon-star"></span>' : '';
                ?>
                <tr data-id="<?php echo $kpi['id']; ?>">
                    <td class="column-1"><?php echo (($i+1) ." ". $required); ?>.</td>
                    <td class="column-2"><?php echo (str_replace(str_split("\|"), "", $kpi['post_title'])); ?></td>
                    <td class="column-3"><?= $kpi['percent']; ?>%</td>
                </tr>
                <?php
                $i++;
            }
            ?>
            </tbody>
        </table>
    </div>
    <?php
}

global $kpi_contents_parts;
$current_url = $kpi_contents_parts['apply-bsc-kpi-system']['url'];

$current_url = add_query_arg($_GET, $current_url);
$alias_url = remove_query_arg('cid', $current_url);

$current_url = remove_query_arg('name', $current_url);
$current_url = remove_query_arg('quy', $current_url);
$current_url = remove_query_arg('thang', $current_url);

$chartRoot = isset($GLOBALS['chartRoot']) ? $GLOBALS['chartRoot'] : null;
# $chartParent = isset($GLOBALS['chartParent']) ? $GLOBALS['chartParent'] : null;
$chartCurrent = isset($GLOBALS['chartCurrent']) ? $GLOBALS['chartCurrent'] : null;
# echo '<pre>'; var_dump($chartRoot, $chartCurrent); echo '</pre>';
$listOrgchart = orgchart_get_list_orgchart_not_nv();

?>
<div id="block-register-kpi" class="block-register-kpi-bgd blocik-item block-shadow border-radius-default">
    <div class="header-register-kpi header-block container-fluid">
        <div class="name-block1 col-md-6">
            <h2 class=""><?php echo _Objective_KPI; //echo sprintf(__('Mục tiêu KPI', TPL_DOMAIN_LANG) ); ?></h2>
	        <?php
	        # Nếu chưa tạo KPI của năm thì ẩn dropdown chức danh.
	        /*if ( ( ! empty( $yearInfo ) || !empty( $yearEmpty ) ) && $orgchart->level < 2 ) : ?>
                <div class="selectpicker-all">
			        <?php
			        $listOrgchart = orgchart_get_list_orgchart_not_nv();
			        ?>
                    <select class="selectpicker" data-change-alias="true"
                            data-redirect-url="<?php echo esc_attr( $alias_url ); ?>">
				        <?php
				        if( !empty($listOrgchart) ):
					        foreach ( $listOrgchart as $k => $item ){
                                if( $item->level > 1 )
                                    continue;
						        if( $item->parent == 0 ){
							        $name = $item->name;
						        }else{
							        $name = $item->room;
						        }
						        echo "<option value='{$item->id}'>{$name}</option>";
					        }
				        endif;
				        ?>
                    </select>
                </div>
	        <?php endif; */ ?>
        </div>
        <form class="col-md-6" enctype="application/x-www-form-urlencoded" method="get" action="<?php echo esc_attr($current_url); ?>">
            <?php

            if( ($chartCurrent && $chartRoot && ($chartRoot['id'] == $chartCurrent['id']) ) ):
                /* ?><a href="javascript:;" class="create-new-year" id="create-kpi-new-year" data-year-id="new" data-toggle="modal"
               data-target="#tpl-kpi-year" ><i class="fa fa-plus-circle" aria-hidden="true"></i></a><?php */ endif; ?>
            <?php /*<button type="submit" class="btn btn-sm btn-primary view-new-year">Xem</button> */ ?>
            <div class="selectpicker-nam">
                <div class="input-group">
                    <label class="input-group-addon" for="select-for-nam" id="select-for-nam-id"><?php _e('Năm', TPL_DOMAIN_LANG); ?></label>
                    <select class="selectpicker form-control select-for-nam" id="select-for-year" name="nam" aria-describedby="select-for-nam-id">
                        <option value="">-- Năm --</option>
                        <?php foreach ($year_list as $item):
                            $selected = (isset($_GET['nam']) && ($_GET['nam'] == $item['year']) ) ? 'selected="selected"' : '';
                            echo sprintf('<option value="%s" %s>Năm %s</option>', $item['year'], $selected, $item['year']);
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
        </form>
    </div>
    <div class="top-register-kpi">
        <div class="col-md-4 register-kpi-1">
            <div class="viewkpiofroom approve-register">
                <?php
                $argsMyself = [
	                'type' => 'dang-ky-kpi',
	                'nam' => TIME_YEAR_VALUE,
	                #'regmyself' => true,
                    'uid' => $_GET['uid'],
                    #'cid' => $_GET['cid'],
                ];
                $urlMyself = add_query_arg( $argsMyself, site_url() );
                ?>
                    <a href="<?= $urlMyself; ?>" title="Đăng ký KPI cá nhân">KPI cá nhân</a>
            </div>
            <?php get_template_part('contents/block-' . $member_role . '/chart'); ?>
        </div>
        <div class="col-md-8">
            <div class="col-md-6 register-kpi-2"><?php
                render_group_list($groups['finance'], $yearInfo, $orgchart_id);
                render_group_list($groups['operate'], $yearInfo, $orgchart_id);
            ?></div>
            <div class="col-md-6 register-kpi-3"><?php
                render_group_list($groups['customer'], $yearInfo, $orgchart_id);
                render_group_list($groups['development'], $yearInfo, $orgchart_id);
            ?></div>

        </div>
    </div>
    <?php get_template_part( 'contents-user-managers/registers/new-kpi', 'detail-' . $member_role ); ?>
</div>
