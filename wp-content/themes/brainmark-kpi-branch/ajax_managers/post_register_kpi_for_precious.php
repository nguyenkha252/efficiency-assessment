<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/24/18
 * Time: 14:13
 */

function wp_ajax_post_register_kpi_for_precious( $params ){
    require_once THEME_DIR . '/inc/lib-years.php';
    require_once THEME_DIR . '/inc/lib-kpis.php';
    global $wpdb;
	$userManagers = wp_get_current_user();
	if( empty($userManagers) && is_wp_error( $userManagers ) ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],401, $message);
	}
	if( !has_cap($userManagers, USER_CAP_MANAGERS) && !has_cap( $userManagers, USER_CAP_EMPLOYERS ) ){
		$message = __('Bạn không có quyền thực hiện chức năng này', TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],403, $message);
	}
	$params = wp_slash($params);
	$user = get_user_by("ID", $params["uid"]);#wp_get_current_user();
	if ( !empty( $user ) && is_wp_error( $user ) || empty( $user ) ) {
		$message  = __( 'Không tìm thấy tài khoản để thực hiện chức năng này. Vui lòng tải lại trang.', TPL_DOMAIN_LANG );
		$httpCode = 401;
		send_response_json( [ 'code' => $httpCode, 'message' => $message ], $httpCode, $message );
	}
    $orgchart = user_load_orgchart($user);
    $params = wp_slash($params);
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
        $httpCode = 401;
        send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
    }
    $result = kpi_get_kpi_by_id($params['parent']);
    if( empty( $result ) ){
        $message = __('Không tìm thấy KPI của năm', TPL_DOMAIN_LANG );
        $httpCode = 404;
        send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
    }
	$unit = $result['unit'];
    if( array_key_exists( 'plan_precious', $params ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $planPrecious = $params['plan_precious'];
        $yearParent = $result['year_id'];
        $year = kpi_get_year_by_id( $yearParent );
        if ( empty($year) ) {
            $message = __('Không tìm thấy năm', TPL_DOMAIN_LANG);
            $httpCode = 404;
            send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
        }
	    $KpiChildren = kpi_get_year_by_parent_kpi( $params['parent'], $result['chart_id'] );
	    $yearChildren = kpi_get_year_by_parent_by_chart_precious( $year['id'], $result['chart_id'] );
        if( !empty( $KpiChildren ) ){
            #update
            $wpdb->query("START TRANSACTION;");
            $lastError = [];
            foreach ($planPrecious as $key => $precious){
	            $plan = $precious['plan'];
                if( $precious['status'] == 1 ){
                    $dataKPI['status'] = KPI_STATUS_RESULT;
                }else{
                    $dataKPI['status'] = KPI_STATUS_DRAFT;
                }
	            $plan = $precious['plan'];
	            $idKPI = $precious['id'];
	            $validatePlan = validate_input_before_save_db( $unit, $precious['plan'], 'plan' );
	            if( !empty($validatePlan) ){
		            $httpCode = 401;
		            $message = $validatePlan['textErrorUnit'];
		            send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
	            }
	            if( !empty($plan) ) {
		            $percent = $precious['percent'];
		            $percent = preg_replace( "#(%)#i", "", $percent );
		            $percent = ! empty( $percent ) ? (int) $percent : $result['percent'];
		            if ( ! is_int( $percent ) ) {
			            $httpCode = 401;
			            $message  = __( 'Trọng số là số nguyên', TPL_DOMAIN_LANG );
			            send_response_json( [ 'code' => $httpCode, 'message' => $message ], $httpCode, $message );
		            }
	            }else{
		            $plan = '';
		            $percent = '';
	            }
	            $dataKPI['percent'] = !empty($percent) ? $percent : '';
	            $dataKPI['plan'] = $plan;
                $getKPI = kpi_get_kpi_by_id( $idKPI );
                if( empty( $getKPI ) ){
                    $message = __('Không tìm thấy KPI', TPL_DOMAIN_LANG );
                    $httpCode = 404;
                    send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
                }else{
                    if( $getKPI['plan'] != $dataKPI['plan'] || $getKPI['status'] != $dataKPI['status'] || $getKPI['percent'] != $dataKPI['percent'] ) {
                        $wpdb->update($tableKpi, $dataKPI, ['id' => $idKPI]);
                        if (!empty($wpdb->last_error)) {
                            $lastError[] = $wpdb->last_error;
                        }
                    }
                }
            }
            if( !empty( $lastError ) ){
                $wpdb->query("ROLLBACK;");
                $message = __('Đã xảy ra lỗi vui lòng thử lại', TPL_DOMAIN_LANG );
                $httpCode = 415;
                send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
            }else{
                $wpdb->query("COMMIT;");
                $message = __('Lưu thành công', TPL_DOMAIN_LANG );
                $httpCode = 201;
                send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
            }
        }else {
            #insert
            $wpdb->query("START TRANSACTION;");
            $createTime = date('Y-m-d H:i:s', time());
            $dataYear = ['year' => $year['year'], 'finance' => $year['finance'], 'customer' => $year['customer'], 'operate' => $year['operate'], 'development' => $year['development'], 'status' => $year['status'], 'parent' => $year['id'], 'created' => $createTime, 'kpi_time' => $year['kpi_time'], 'kpi_type' => $year['kpi_type'] ];
            $dataKPI = ['bank_id' => $result['bank_id'], 'unit' => $result['unit'], 'receive' => $result['receive'], 'parent' => $result['id'], 'chart_id' => $orgchart->id, 'user_id' => $user->ID, 'type' => $result['type'], 'status' => KPI_STATUS_PENDING, 'required' => $result['required'], 'percent' => $result['percent'], 'created' => $createTime,  'owner' => 'yes', 'create_by_node' => $result['create_by_node'] ];
            foreach ($planPrecious as $key => $precious) {
	            $plan = $precious['plan'];
	            $preciousValue = (int)$precious['precious'];
	            $dataYear['precious'] = (int)$precious['precious'];
	            $validatePlan = validate_input_before_save_db( $unit, $plan, 'plan' );
	            if( !empty($validatePlan) ){
		            $httpCode = 401;
		            $message = $validatePlan['textErrorUnit'];
		            send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
	            }
	            if( !empty($plan) ) {
		            $percent = $precious['percent'];
		            $percent = preg_replace( "#(%)#i", "", $percent );
		            $percent = ! empty( $percent ) ? (int)$percent : $result['percent'];
		            if ( ! is_int( $percent ) ) {
			            $httpCode = 401;
			            $message  = __( 'Trọng số là số nguyên', TPL_DOMAIN_LANG );
			            send_response_json( [ 'code' => $httpCode, 'message' => $message ], $httpCode, $message );
		            }
	            }else{
		            $plan = '';
		            $percent = '';
	            }

	            if( empty($yearChildren) ) {
		            $wpdb->insert( $tableKpiYear, $dataYear );
		            $idYear = $wpdb->insert_id;
	            }else{
		            $filterArray = array_filter($yearChildren, function($item) use ($preciousValue){
			            return $preciousValue == $item['precious'];
		            });
		            $idYear = 0;
		            if( count($filterArray) == 1 ){
			            $filterArray = array_shift($filterArray);
			            $idYear = $filterArray['id'];
		            }
	            }
	            if( !empty( $wpdb->last_error ) ){
		            $wpdb->query("ROLLBACK;");
		            $message = __('Đã xảy ra lỗi vui lòng thử lại', TPL_DOMAIN_LANG );
		            $httpCode = 415;
		            send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
	            }else{
		            $dataKPI['year_id'] = $idYear;
		            $dataKPI['plan'] = $plan;
		            $dataKPI['percent'] = !empty($percent) ? $percent : '';
		            $wpdb->insert( $tableKpi, $dataKPI );
		            if( !empty( $wpdb->last_error ) ){
			            $wpdb->query("ROLLBACK;");
			            $message = __('Đã xảy ra lỗi vui lòng thử lại', TPL_DOMAIN_LANG );
			            $httpCode = 415;
			            send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
		            }
	            }
            }
            $wpdb->query("COMMIT;");
            $message = __('Lưu thành công', TPL_DOMAIN_LANG );
            $httpCode = 201;
            send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
        }
    }
}