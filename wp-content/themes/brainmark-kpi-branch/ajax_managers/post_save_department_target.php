<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 2/5/18
 * Time: 16:26
 */

function wp_ajax_post_save_department_target($params) {
    global $wpdb;
    $wpnonce = isset($params['_wpnonce']) ? $params['_wpnonce'] : '';
	$userManagers = wp_get_current_user();
	if( empty($userManagers) && is_wp_error( $userManagers ) ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],401, $message);
	}
	if( !has_cap($userManagers, USER_CAP_MANAGERS) ){
		$message = __('Bạn không có quyền thực hiện chức năng này', TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],403, $message);
	}
    if( !wp_verify_nonce($wpnonce, 'save-department-target') ) {
        $msg = "Mã bảo vệ không hợp lệ";
        send_response_json(['code' => 403, 'message' => $msg], 403, $msg);
    }
    $cat_slug = $params['cat_slug'];
    $kpi_type = $params['kpi_type'];
    $year_id = $params['year_id'];
    $postdata = wp_slash( $params['postdata'] );

    add_filter('wp_insert_post_data', function($data, $postarr) use($params, $postdata) {
        if( isset($postdata['formula_id']) ) {
            $data['formula_id'] = intval($postdata['formula_id']);
        }
        return $data;
    }, 10, 2);

    // Insert Bank ID
    /*
    if( empty($postdata['bank_id']) ) {
        $insertData = [
            'post_title' => $postdata['post_title'],
            'post_status' => 'publish',
            'post_parent' => isset($postdata['bank_parent']) ? (int)$postdata['bank_parent'] : 0,
        ];
        $term = get_term_by('slug', $cat_slug, 'category', OBJECT);
        if( $term !== false && !is_wp_error($term) ) {
            $term_id = $term->term_id;
            $insertData['post_category'] = [(int)$term_id];
        }
        $bank = get_page_by_title( $postdata['post_title'], OBJECT, 'post' );
        if( $bank instanceof WP_Post ) {
            $insertData['ID'] = $bank->ID;
            if( $insertData['post_parent'] != $bank->ID ) {
                $postdata['bank_id'] = wp_update_post($insertData);
            } else {
                $postdata['bank_id'] = $bank->ID;
            }
        } else {
            $postdata['bank_id'] = wp_insert_post($insertData);
        }
    } else {
        // Update Bank ID
        $updateData = [
            'ID' => $postdata['bank_id'],
            'post_title' => $postdata['post_title'],
            'post_status' => 'publish',
            'post_parent' => isset($postdata['bank_parent']) ? (int)$postdata['bank_parent'] : 0,
        ];
        if( $postdata['bank_id'] != $updateData['post_parent'] ) {
            $postdata['bank_id'] = wp_update_post($updateData);
        }
    } */
    $status = 201;
    $msg = "Lưu thành công";
    if( is_numeric($postdata['bank_id']) ) {
        # $postdata['bank_id']
        $user = get_user_by("ID", $params['uid']);
	    if ( !empty( $user ) && is_wp_error( $user ) || empty( $user ) ) {
		    $message  = __( 'Không tìm thấy tài khoản để thực hiện chức năng này. Vui lòng tải lại trang.', TPL_DOMAIN_LANG );
		    $httpCode = 401;
		    send_response_json( [ 'code' => $httpCode, 'message' => $message ], $httpCode, $message );
	    }
        $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
        $tableKpi = "{$prefix}kpis";
        $tableChart = "{$prefix}org_charts";
        $tableKpiYear = "{$prefix}kpi_years";

        if( empty($params['year_id']) || !is_numeric($params['year_id']) ) {
            $msg = "Tạo trọng số cho từng loại mục tiêu trước khi triển khai";
            send_response_json(['code' => 400, 'message' => $msg], 400, $msg);
        }

        $dataInsert = [
            'bank_id' => $postdata['bank_id'], 'plan' => $postdata['plan'], 'unit' => $postdata['unit'], 'percent' => $postdata['percent'],
            'parent' => !empty($postdata['parent']) ? $postdata['parent'] : 0, 'receive' => kpi_mysql_date($postdata['receive']),
            'chart_id' => !empty($postdata['chart_id']) ? $postdata['chart_id'] : (!empty($params['cid']) ? $params['cid'] : $user->orgchart_id),
            'type' => $postdata['type'], 'status' => $postdata['status'], 'required' => $postdata['required'], 'owner' => $postdata['owner'],
            'create_by_node' => $postdata['create_by_node'], 'aproved' => $postdata['aproved'], 'user_id' => $postdata['user_id'],
            'year_id' => !empty($postdata['year_id']) ? $postdata['year_id'] : $year_id,
        ];

        $sql = $wpdb->prepare("SELECT k.* FROM `{$tableKpi}` as k ".
            "WHERE (k.`year_id` = %d) AND (k.`bank_id` = %d) AND (k.`chart_id` = %d) ".
            "AND (k.`user_id` = %d) AND (k.`parent` = %d) AND (k.`type` LIKE %s)",
            $dataInsert['year_id'], $dataInsert['bank_id'], $dataInsert['chart_id'],
            $dataInsert['user_id'], $dataInsert['parent'], $dataInsert['type']);
        $exists = $wpdb->get_row( $sql, ARRAY_A );
        if( !empty($exists) ) {
            $postdata['id'] = $exists['id'];
        }
        # var_dump($exists);
        # exit(__LINE__ . ' ' . __FILE__ . "\n" . $sql);

        if( !empty($postdata['id']) ) {
            // Update KPI
            $kpiResult = $wpdb->update($tableKpi, $dataInsert, ['id' => $postdata['id']]);
        } else {
            // Insert KPI
            $dataInsert['created'] = date('Y-m-d H:i:s', time());
            $kpiResult = $wpdb->insert($tableKpi, $dataInsert);
            $postdata['id'] = $wpdb->insert_id;
        }

        if( $kpiResult === false && $wpdb->last_error ) {
            $msg = "Có lỗi xảy ra khi lưu KPI";
            send_response_json(['code' => 400, 'message' => $msg, 'error' => $wpdb->last_error], 400, $msg);
        }
        $sql = $wpdb->prepare("SELECT k.*, c.name, c.name as chuc_danh, y.`year`, y.`precious`, y.`month`, p.post_title, p.post_parent as bank_parent, p.formula_id ".
            "FROM {$tableKpi} as k " .
            "INNER JOIN {$tableKpiYear} as y ON (k.year_id = y.id) " .
            "INNER JOIN {$wpdb->posts} as p ON (p.ID = k.bank_id) " .
            "INNER JOIN {$tableChart} as c ON (k.chart_id = c.id) " .
            "WHERE k.`id` = %d", $postdata['id']);

        $html = '';
        $postdata['kpi'] = $wpdb->get_row($sql, ARRAY_A);
        $item_html = '';
        if( !empty($postdata['kpi']) ) {
            require_once THEME_DIR . '/inc/render-kpi-item-row_managers.php';
            require_once THEME_DIR . '/inc/lib-orgchart.php';
            require_once THEME_DIR . '/ajax/get_user_info.php';

            $postdata['kpi']['bank_parent'] = (int)$postdata['kpi']['bank_parent'];
            $postdata['kpi']['year'] = (int)$postdata['kpi']['year'];
            $postdata['kpi']['bank_id'] = (int)$postdata['kpi']['bank_id'];
	        $postdata['kpi']['formula_id'] = (int)$postdata['kpi']['formula_id'];
            $postdata['kpi']['year_id'] = (int)$postdata['kpi']['year_id'];
            $postdata['kpi']['id'] = (int)$postdata['kpi']['id'];
            $chartParent = kpi_orgchart_get_chart_parent_of($_GET['cid']);
            $chartCurrent = kpi_get_orgchart_by_id($_GET['cid']);

            $members = kpi_get_users(['room_id' => $user->orgchart_id, 'chart_id' => 0, 'trang' => 1, 'limit' => 100, 'tim' => '']);
            $members = $members['items'];
            $html = kpi_render_ceo_row_item_managers($postdata['kpi'], $postdata['type'], $chartCurrent, $chartParent, $members, $user);
            $item_html = '<tr data-id="'.$postdata['kpi']['id'].'">
                    <td class="column-1">{IDX}.</td>
                    <td class="column-2">'.($postdata['kpi']['post_title']) . '</td>
                    <td class="column-3">'.$postdata['kpi']['percent'].'%</td>
                </tr>';
        }
        return send_response_json(['code' => $status, 'message' => $msg,
            'id' => !empty($postdata['kpi']) ? $postdata['kpi']['id'] : $postdata['id'],
            'sql' => $sql, 'last_query' => $wpdb->last_query,
            'last_error' => $wpdb->last_error, 'dataInsert' => $dataInsert,
            'html' => $html, 'item_html' => $item_html], $status, $msg);
    } else if( is_wp_error($postdata['bank_id']) ) {
        $status = 400;
        $msg = $postdata['bank_id']->get_error_message();
        send_response_json(['code' => 400, 'message' => $msg, 'error' => $wpdb->last_error], 400, $msg);
    }
}

