<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/19/18
 * Time: 14:49
 */

function update_result( $params ) {
	require_once THEME_DIR . '/inc/lib-kpis.php';
	global $wpdb;
	$userManagers = wp_get_current_user();
	if ( empty( $userManagers ) && is_wp_error( $userManagers ) ) {
		$message = __( 'Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
		send_response_json( [ 'error' => $message ], 401, $message );
	}
	if( !has_cap($userManagers, USER_CAP_MANAGERS) && !has_cap( $userManagers, USER_CAP_EMPLOYERS ) ){
		$message = __( 'Bạn không có quyền thực hiện chức năng này', TPL_DOMAIN_LANG );
		send_response_json( [ 'error' => $message ], 403, $message );
	}
	$user     = get_user_by( "ID", $params["uid"] );#wp_get_current_user();
	$orgchart = user_load_orgchart( $user );
	if ( !empty( $user ) && is_wp_error( $user ) || empty( $user ) ) {
		$message  = __( 'Không tìm thấy tài khoản để thực hiện chức năng này. Vui lòng tải lại trang.', TPL_DOMAIN_LANG );
		$httpCode = 401;
		send_response_json( [ 'code' => $httpCode, 'message' => $message ], $httpCode, $message );
	}

	$allowType = [
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'image/png',
		'image/jpeg',
		'image/jpg',
		'image/bmp',
		'application/zip',
		'application/vnd.ms-excel',
		'application/x-rar',
		'application/msword',
		'application/octet-stream',
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
	];
	$lastError = [];
	if ( ! empty( $params['resultKPI'] ) ) {
		$prefix   = $wpdb->get_blog_prefix( get_current_blog_id() );
		$tableKpi = "{$prefix}kpis";
		$upload   = wp_upload_dir();
		$files    = isset( $_FILES['resultKPI'] ) ? $_FILES['resultKPI'] : null;
		$wpdb->query( "START TRANSACTION;" );
		foreach ( $params['resultKPI'] as $key => $item ) {
			$dataUpdateMeta       = [];
			$params['files_path'] = [];
			$params['files_url']  = [];
			$resultKPI            = kpi_get_kpi_by_id( $item['ID'] );
			if ( empty( $resultKPI ) ) {
				$message  = __( 'Không tìm thấy KPI', TPL_DOMAIN_LANG );
				$httpCode = 404;
				send_response_json( [ 'code' => $httpCode, 'message' => $message ], $httpCode, $message );
			}

			$typeFiles  = [];
			$nameFiles  = [];
			$errorFiles = [];
			$sizeFiles  = [];

			if ( ! empty( $files ) ) {
				$typeFiles  = $files['type'][ $key ]['files'];
				$nameFiles  = $files['name'][ $key ]['files'];
				$errorFiles = $files['error'][ $key ]['files'];
				$sizeFiles  = $files['size'][ $key ]['files'];
			}

			if ( ! empty( $typeFiles ) ) {
				foreach ( $typeFiles as $ktype => $typefile ) {
					if ( ! in_array( $typefile, $allowType ) || $sizeFiles[ $ktype ] > 5242880 || $errorFiles[ $ktype ] > 0 ) {
						$msg      = ! in_array( $typefile, $allowType ) ? __( 'Tải tập tin không đúng định dạng', TPL_DOMAIN_LANG ) : __( 'Vui lòng tải tập tin < 5M', TPL_DOMAIN_LANG );
						$httpCode = 420;
						send_response_json( [ 'code' => $httpCode, 'message' => $msg ], $httpCode, $msg );
					}
				}
			}

			if ( ! empty( $nameFiles ) ) {
				foreach ( $nameFiles as $kname => $namefile ) {
					$fileName  = explode( ".", $namefile );
					$_typefile = '';
					if ( count( $fileName ) > 1 ) {
						$_typefile = $fileName[ count( $fileName ) - 1 ];
						unset( $fileName[ count( $fileName ) - 1 ] );
					}
					if ( ! empty( $_typefile ) ) {
						$fileName = implode( "-", $fileName );
						$fileName .= '-' . time();
						$fileName .= "." . $_typefile;
					} else {
						$fileName = $namefile;
					}

					$path                           = "/files/{$fileName}";
					$params['files_path'][ $kname ] = $path;
					$params['files_url'][ $kname ]  = "/files/{$fileName}";

					if ( ! is_dir( "{$upload['basedir']}/files" ) ) {
						mkdir( "{$upload['basedir']}/files", 0777, true );
					}
					$isUpload = move_uploaded_file( $files['tmp_name'][ $key ]['files'][ $kname ], $upload['basedir'] . $path );
					if ( ! $isUpload ) {
						$msg      = __( "Tải tập tin {$fileName} thất bại", TPL_DOMAIN_LANG );
						$httpCode = 410;
						send_response_json( [ 'code' => $httpCode, 'message' => $msg ], $httpCode, $msg );
					}
				}
			}
			$dataUpdateMeta['files_path'] = $params['files_path'];
			$dataUpdateMeta['files_url']  = $params['files_url'];
			$data['actual_gain']          = $item['actual_gain'];

			#$wpdb->update($tableKpi, $data, ['id' => $item['ID'] ] );
			#if( !empty( $dataUpdateMeta['files_url'] ) ){
			#$KPIMeta = kpi_get_meta_by_key( $item['ID'], 'files' );
			if ( ! empty( $resultKPI['files'] ) ) {
				$dataMeta = maybe_unserialize( $resultKPI['files'] );
				if ( ! empty( $dataMeta ) && ! empty( $dataMeta['files_url'] && ! empty( $dataMeta['files_path'] ) ) ) {
					foreach ( $dataUpdateMeta['files_url'] as $k => $it ) {
						array_push( $dataMeta['files_url'], $it );
						array_push( $dataMeta['files_path'], $dataUpdateMeta['files_path'][ $k ] );
					}

				} else {
					$dataMeta['files_url']  = $dataUpdateMeta['files_url'];
					$dataMeta['files_path'] = $dataUpdateMeta['files_path'];
				}
				$dataFiles = maybe_serialize( $dataMeta );
			} else {
				$dataFiles = maybe_serialize( $dataUpdateMeta );
			}

			#if( !empty( $KPIMeta ) ){
			$data['files'] = $dataFiles;
			$wpdb->update( $tableKpi, $data, [ 'id' => $resultKPI['id'] ] );
			/*}else{
				$wpdb->insert($tableKpiMeta, ['kpi_id' => $item['ID'], 'meta_key' => 'files', 'meta_value' => $dataMeta ] );
			}*/

			/*}elseif( !empty( $wpdb->last_error ) ){
				$lastError[] = $wpdb->last_error;
			}*/

			if ( ! empty( $wpdb->last_error ) ) {
				$lastError[] = $wpdb->last_error;
			}
			unset( $params['files_path'] );
			unset( $params['files_url'] );
		}
		if ( empty( $lastError ) ) {
			$wpdb->query( "COMMIT;" );

			return true;
		} else {
			$wpdb->query( "ROLLBACK;" );

			return false;
		}
	}
}
function wp_ajax_post_personal_update_result( $params ){
	$params = wp_slash( $params );
	$isUpdate = update_result( $params );
    if( $isUpdate ){
        $msg = __('Thành công', TPL_DOMAIN_LANG);
        $httpCode = 201;
        send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
    }else{
        $msg = __('Có lỗi xảy ra vui lòng thử lại.', TPL_DOMAIN_LANG);
        $httpCode = 405;
        send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
    }
}