<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/03/2018
 * Time: 16:25
 */
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-behavior.php';
function wp_ajax_post_update_result_behavior( $params ){
	global $wpdb;
	$userManagers = wp_get_current_user();
	if( empty($userManagers) && is_wp_error( $userManagers ) ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],401, $message);
	}
	if( !has_cap($userManagers, USER_CAP_MANAGERS) && !has_cap( $userManagers, USER_CAP_EMPLOYERS ) ){
		$message = __('Bạn không có quyền thực hiện chức năng này', TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],403, $message);
	}
	$user = get_user_by("ID", $params['uid']);//wp_get_current_user();
	if ( !empty( $user ) && is_wp_error( $user ) || empty( $user ) ) {
		$message  = __( 'Không tìm thấy tài khoản để thực hiện chức năng này. Vui lòng tải lại trang.', TPL_DOMAIN_LANG );
		$httpCode = 401;
		send_response_json( [ 'code' => $httpCode, 'message' => $message ], $httpCode, $message );
	}
	$orgchart = user_load_orgchart($user);
	$params = wp_slash( $params );
	if( !$user ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],401, $message);
	}
	if( !wp_verify_nonce($params['_wpnonce'], 'update_result_behavior') ){
		$message = __('Phiên bản đã hết hạn. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],408, $message);
	}
	if( array_key_exists('behavior', $params) ){
		$arrBehavior = $params['behavior'];
		if( !empty( $arrBehavior ) ){
			foreach ( $arrBehavior as $key => $item ){
				$id = $item['id'];
				$number = $item['number'];
				$result = behavior_get_by_id( $id );
				if( !empty( $result ) ){
					$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
					$tableKpiBehavior = "{$prefix}behavior";
					$wpdb->update( $tableKpiBehavior, ['number' => $number], ['id' => $id] );
					if( !empty( $wpdb->last_error ) ){
						$msg = "Có lỗi xảy ra khi lưu";
						$httpCode = 400;
						send_response_json(['code' => $httpCode, 'message' => $msg, 'error' => $wpdb->last_error], $httpCode, $msg);
					}
				}

			}
		}
	}
	$msg = "Thành công";
	$httpCode = 201;
	$location = '';
	if( !empty( $params ) && !empty( $params['_wp_http_referer'] ) ){
		$query_params = wp_get_referer();
		$location = site_url($query_params);
	}
	send_response_json(['code' => $httpCode, 'message' => $msg, 'location' => $location], $httpCode, $msg);
}