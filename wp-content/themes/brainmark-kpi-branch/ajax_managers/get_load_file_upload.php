<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/22/18
 * Time: 15:00
 */
function wp_ajax_get_load_file_upload( $params ){
    require_once THEME_DIR . '/inc/lib-kpis.php';
    $params = wp_unslash($params);
	$userManagers = wp_get_current_user();
	if( empty($userManagers) && is_wp_error( $userManagers ) ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],401, $message);
	}
	if( !has_cap($userManagers, USER_CAP_MANAGERS) && !has_cap( $userManagers, USER_CAP_EMPLOYERS ) ){
		$message = __('Bạn không có quyền thực hiện chức năng này', TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],403, $message);
	}
	$user = get_user_by("ID", $params['uid']);//wp_get_current_user();
	if ( !empty( $user ) && is_wp_error( $user ) || empty( $user ) ) {
		$message  = __( 'Không tìm thấy tài khoản để thực hiện chức năng này. Vui lòng tải lại trang.', TPL_DOMAIN_LANG );
		$httpCode = 401;
		send_response_json( [ 'code' => $httpCode, 'message' => $message ], $httpCode, $message );
	}
    $kpiID = (int) $params['id'];
    $result = kpi_get_kpi_by_id( $kpiID );
    if( empty( $result ) ){
        $message = __('Không tìm thấy KPI', TPL_DOMAIN_LANG );
        $httpCode = 401;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }
    $statusKPI = '';
    $files = $result['files'];
    $files = maybe_unserialize( $files );
    if( !empty( $files ) && !empty( $files['files_url'] ) ){
        $stt = 1;
        $output = '';
        $upload = wp_upload_dir();
        foreach ( $files['files_url'] as $key_file => $file ){
            $fileName = explode( "/", $file );
            if( count( $fileName ) > 1 ) {
                $fileName = $fileName[ count($fileName) - 1 ];
            }else {
	            $fileName = $file;
            }
            $showInput = sprintf("<td class='column-3'>
            <label class=\"switch\">
                <input class=\"checkbox-status hidden\" name='file_id[]' type=\"checkbox\" value=\"%s\">
                <span class=\"checkbox-slider\"></span>
            </label>
        </td>", ($key_file + 1));

            $urlFile = $upload['baseurl'] . '/files/' . $fileName;
            $output .= "<tr>";
            $output .=  sprintf("<td class='column-1'>%d</td>", $stt);
            $output .= sprintf("<td class='column-2'><a href='%s' title='%s'>%s</a></td>", $urlFile, $fileName, $fileName);
            $output .= $showInput;
            $output .= "</tr>";
            $stt++;
        }
        send_response_json(['code' => 201, 'data' => [$output], 'complete' => $statusKPI], 201, __('Thành công', TPL_DOMAIN_LANG));
    }
}