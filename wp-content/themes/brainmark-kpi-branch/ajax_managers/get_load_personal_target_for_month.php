<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/19/18
 * Time: 11:02
 */

function wp_ajax_get_load_personal_target_for_month( $params ){
    require_once THEME_DIR . '/inc/lib-years.php';
    require_once THEME_DIR . '/inc/lib-kpis.php';
    require_once THEME_DIR . '/inc/lib-users.php';
	$userManagers = wp_get_current_user();
	if( empty($userManagers) && is_wp_error( $userManagers ) ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],401, $message);
	}
	if( !has_cap($userManagers, USER_CAP_MANAGERS) && !has_cap( $userManagers, USER_CAP_EMPLOYERS ) ){
		$message = __('Bạn không có quyền thực hiện chức năng này', TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],403, $message);
	}
    $params = wp_unslash($params);
    $kpiID = (int) $params['id'];
    $user = get_user_by("ID", $params['uid']);//wp_get_current_user();
	if ( !empty( $user ) && is_wp_error( $user ) || empty( $user ) ) {
		$message  = __( 'Không tìm thấy tài khoản để thực hiện chức năng này. Vui lòng tải lại trang.', TPL_DOMAIN_LANG );
		$httpCode = 401;
		send_response_json( [ 'code' => $httpCode, 'message' => $message ], $httpCode, $message );
	}
    $orgchart = user_load_orgchart( $user );
    #$userID = $user->ID;
    $result = get_kpi_and_post_by_kpi_id($kpiID);
    if( empty( $result ) ){
	    $message = __('Không tìm thấy KPI', TPL_DOMAIN_LANG );
	    $httpCode = 403;
	    send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }

    $data['plan'] = $result['plan'];
    $data['post_title'] = $result['post_title'];
    $data['plan_on_level'] = !empty( $result['plan_on_level'] ) ? $result['plan_on_level'] : '';
    $data['parent'] = $kpiID;
    $data['receive'] = substr( kpi_format_date( $result['receive'] ), 0, 10 );
    $data['percent'] = $result['percent'];
    $data['unit'] = getUnit( $result['unit'] );
    $data['_wpnonce'] = wp_create_nonce('create_kpi_for_end_level');
    $data['year'] = $result['year'];
    # lấy id của year và năm của id sau đó lấy con của year đó
    #$resultsOrgChart = orgchart_check_children_by_id( $result['chart_id'] );

	$checkChartChild = check_subordinates_chart($orgchart->id, $result['chart_id']);
    #if( !empty($checkChartChild) || $userID == $result['user_id'] || user_is_manager() ){
        $userID = $result['user_id'];
        $kpiMonth = get_kpi_by_year( $result['year_id'], $result['id'], $userID );
        $output = ""; $outputTemp = "";
        $outputLeft = ""; $outputRight = "";
        if( !empty( $kpiMonth ) ){
            foreach ( $kpiMonth as $key => $month ){
            	$percent = '';
            	if( !empty($month['plan']) ){
	                $percent = !empty($month['percent']) ? $month['percent'] : '';
	            }
	            $showStatusApprove = '';
                #if( $userID != $user->ID ){
	                if( empty($month['plan']) )
		                continue;
                    if( !in_array( $month['status'], [KPI_STATUS_PENDING, KPI_STATUS_DRAFT] ) ){
                        $statusText = KPI_DA_DUYET;
                        $checked = "checked";
                    }else{
                        $statusText = KPI_CHUA_DUYET;
                        $checked = "";
                    }
                    $showStatus = "
                        <input name=\"plan_month[". $month['month'] ."][month]\" value=\"". $month['month'] ."\" type=\"hidden\" />
                        <input name=\"plan_month[". $month['month'] ."][id]\" value=\"". $month['id'] ."\" type=\"hidden\" />
                        <span class=\"input-group-addon\" for=\"plan\" id=\"plan-". $month['month'] ."-". $month['month'] ."\">".__('Tháng ', TPL_DOMAIN_LANG) . $month['month'] ."</span>
                        <input type=\"text\" name=\"plan_month[". $month['month'] ."][plan]\" id=\"plan-". $month['month'] ."-". $month['month'] ."\" class=\"plan-". $month['month'] ."-". $month['month'] ." form-control confirm-month-form-control\" placeholder=\"".__('Kế hoạch', TPL_DOMAIN_LANG)."\" value=\"". $month['plan'] ."\" />
                        ";
        $showStatusApprove = "<div class=\"right-status-confirm-month\">
                            <label class=\"switch\">
                                <input class=\"checkbox-status hidden\" type=\"checkbox\" name=\"plan_month[{$month['month']}][status]\" {$checked} value=\"1\"/>
                                <span class=\"checkbox-slider\"></span>
                            </label>
                            <span>{$statusText}</span>
                        </div>";

                #if( $userID != $user->ID ){
                	$outputTemp = "
					<div class=\"item-form-group-for group-onlevel-approve\">
						<div class='onlevel-approve'>
							<div class=\"form-group input-group left-target-for\">
                                {$showStatus}
                            </div>
	                        <div class=\"form-group input-group right-target-percent\">
	                                                                    <span class=\"input-group-addon\" for=\"plan\" id=\"plan-".$month['month']."-".$month['month']."\">". __('Trọng số ', TPL_DOMAIN_LANG) ."</span>
	                                                                    <input type=\"text\" name=\"plan_month[".$month['month']."][percent]\" id=\"plan-".$month['month']."-".$month['month']."\" class=\"plan-".$month['month']." form-control\" placeholder=\"". __('Trọng số ', TPL_DOMAIN_LANG) ."\" value=\"".$percent."\" />
	                        </div>
						</div>
						{$showStatusApprove}
					</div>
					";
                	if( $month['month'] <= 6 ){
		                $outputLeft .= $outputTemp;
	                }else{
		                $outputRight .= $outputTemp;
	                }

            }
            $output = "
                <table class=\"table-field-list\" border=\"0\">
                	<tr>
                		<td class=\"column-left\" valign=\"top\">{$outputLeft}</td>
                		<td class=\"column-right\" valign=\"top\">{$outputRight}</td>
					</tr>
				</table>
            ";
        }
        $message = __('Thành công', TPL_DOMAIN_LANG);
        $httpCode = 201;
        send_response_json(['code' => $httpCode, 'data_personal' => $data, 'kpi_month' => $output], $httpCode, $message);


}