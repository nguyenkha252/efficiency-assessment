<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/11/18
 * Time: 19:06
 */

function wp_ajax_post_asign_kpi_members($params) {
    require_once THEME_DIR . '/inc/lib-users.php';
    require_once THEME_DIR . '/inc/lib-kpis.php';
    $params = wp_slash($params);
	$userManagers = wp_get_current_user();
	if( empty($userManagers) && is_wp_error( $userManagers ) ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],401, $message);
	}
	if( !has_cap($userManagers, USER_CAP_MANAGERS) ){
		$message = __('Bạn không có quyền thực hiện chức năng này', TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],403, $message);
	}
    $user = get_user_by("ID", $params['uid']);//wp_get_current_user();
	if ( !empty( $user ) && is_wp_error( $user ) || empty( $user ) ) {
		$message  = __( 'Không tìm thấy tài khoản để thực hiện chức năng này. Vui lòng tải lại trang.', TPL_DOMAIN_LANG );
		$httpCode = 401;
		send_response_json( [ 'code' => $httpCode, 'message' => $message ], $httpCode, $message );
	}
    $nonce = isset($params['_wpnonce']) ? $params['_wpnonce'] : '';
    if( !wp_verify_nonce($nonce, 'asign_kpi_members') ) {
        $msg = "Sai mã bảo vệ";
        send_response_json(['code' => 403, 'message' => $msg], 403, $msg);
    }
    $params['kpi_id'];
    $params['kpi_type'];
    $params['chart_id'];
    $params['year_id'];
    $params['plan'];
    $params['unit'];
    $params['bank_id'];
    # $params['users'][];

    global $wpdb;
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableKpi = "{$prefix}kpis";
    $tableKpiYear = "{$prefix}kpi_years";
    $kpi = kpi_get_kpi_by_id($params['kpi_id']);

    if( empty($kpi) ) {
        $msg = "Không tìm thấy KPI tương ứng";
        send_response_json(['code' => 404, 'message' => $msg], 404, $msg);
    }
    $resultParent = kpi_get_all_kpi_by_parent( $params['kpi_id'] );
    $arrKPI = [];
    if( !empty( $resultParent ) ){
    	foreach ( $resultParent as $key => $item ){
				$arrKPI[$item['id']] = $item['id'];
	    }
    }
	$location = '';
	if( !empty( $params ) && !empty( $params['_wp_http_referer'] ) ){
		$query_params = wp_get_referer();
		$location = site_url($query_params);

	}
    if( !empty($params['users']) ) {
        foreach ($params['users'] as $uid => $u) {
            if (empty($u['ID']) || empty($u['plan']) || empty($u['percent']) || empty($u['chart_id'])) {
                unset($params['users'][$uid]);
                continue;
            }
        }
        #if( empty($params['users']) ) {
        #    $msg = "Vui lòng điền đủ thông tin của những nhân viên được chọn";
        #    send_response_json(['code' => 404, 'message' => $msg], 404, $msg);
        #}
        $results = [];
        $errors = [];
        $success = [];
        $created = date('Y-m-d H:i:s', time());
        foreach ($params['users'] as $uid => $u) {
            if( empty( $u['ID'] ) ) {
                continue;
            }

            $insertData = $kpi;
            $insertData['parent'] = $kpi['id'];
            unset($insertData['id']);
            $_user = get_user_by("ID", $uid);
            $insertData['plan'] = $u['plan'];
            $insertData['chart_id'] = $u['chart_id'];
            $insertData['percent'] = $u['percent'];
            #$insertData['type'] = $params['kpi_type'];
            $insertData['status'] = KPI_STATUS_RESULT;
            # $insertData['required'] = 'no';
	        if( !empty($u['influence']) ){
		        $insertData['influence'] = 'yes';
	        }else{
		        $insertData['influence'] = 'no';
	        }
            $insertData['user_id'] = $uid;
            $insertData['created'] = $created;
	        # @TODO edit by Kha
			if( $uid == $user->ID ){
				$insertData['owner'] = 'yes';
			}else{
				$insertData['owner'] = 'no';
			}
			$orgchartUser = user_load_orgchart( $_user );
	        $total_percent = 0;
			if( $orgchartUser->alias == 'phongban' ) {
				$getTotalPercent = get_total_percent_kpi_by_phongban( $uid, $u['chart_id'], $params['nam'], $params['kpi_type'], $insertData['owner'] );
				if ( ! empty( $getTotalPercent ) ) {
					$total_percent = $getTotalPercent['total_percent'];
				}
			}
	        # @TODO end edit
            $kpiItemID = $u['kpi_item_id'];
            /* $id = $wpdb->get_var($wpdb->prepare("SELECT id FROM {$tableKpi} ".
                "WHERE `bank_id` = %d AND `user_id` = %d AND `parent` = %d AND `chart_id` = %d AND `year_id` = %d",
                $params['bank_id'], $uid, $kpi['id'], $_user->orgchart_id, $params['id']) ); */
            $user_kpi_id = '';

            $percent = 0;
            if( !empty( $kpiItemID ) ){
                $rowKPI = $wpdb->get_row($wpdb->prepare("SELECT id, percent FROM {$tableKpi} ".
                    "WHERE id = %d ", $kpiItemID ), ARRAY_A );
                if( $rowKPI ){
                    $user_kpi_id = $rowKPI['id'];
                    $percent = $rowKPI['percent'];
                }
            }
            $year = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$tableKpiYear} AS y WHERE y.id = %d", $params['year_id'] ), ARRAY_A );
            # check year
            $yearData = [];
            if( !empty( $year ) ){
                $yearData = $year;
                $insertData['year_id'] = $yearData['id'];
                unset($yearData['id']);
                $yearData['parent'] = $year['id'];
                # @TODO edit by Kha
                $yearData['chart_id'] = $u['chart_id'];
                # @TODO end edit
                $yearData['created'] = date('Y-m-d H:i:s', time());
            }else{
                $msg = "Không tìm thấy năm của KPI. Vui lòng thử lại";
                send_response_json(['code' => 404, 'message' => $msg], 404, $msg);
            }

            if( empty($user_kpi_id) ) {
				if( $orgchartUser->alias == 'phongban' ) {
					if ( $total_percent == 100 ) {
						$getUser      = get_user_by( "ID", $uid );
						$display_name = $getUser->display_name;
						$msg          = "Tổng trọng số của <strong>{$display_name}</strong> ở <strong>" . __( $params['kpi_type'], TPL_DOMAIN_LANG ) . "</strong> đã đủ 100%";
						send_response_json( [ 'code' => 401, 'message' => $msg ], 401, $msg );
					} elseif ( $total_percent + $insertData['percent'] > 100 ) {
						$getUser      = get_user_by( "ID", $uid );
						$display_name = $getUser->display_name;
						$msg          = "Tổng trọng số của <strong>{$display_name}</strong> ở <strong>" . __( $params['kpi_type'], TPL_DOMAIN_LANG ) . "</strong> vượt mức 100 (Trọng số còn lại có thể triển khai: <strong>" . ( 100 - $total_percent ) . "</strong>)";
						send_response_json( [ 'code' => 401, 'message' => $msg ], 401, $msg );
					}
				}

                $wpdb->query("START TRANSACTION;");
                $lastError = [];
                # INSERT INTO `bmk_17_kpi_years`
                # (`year`, `precious`, `month`, `behavior_percent`, `finance`, `customer`, `operate`, `development`, `parent`, `chart_id`, `kpi_time`, `kpi_type`, `status`, `created`)
                # VALUES ('2018', '0', '0', '0', '25', '25', '25', '25', 9, '3', 'quy', 'congty', 'publish', '2018-03-13 08:57:45')
                $sql = $wpdb->prepare("SELECT `id` FROM {$tableKpiYear} as y ".
                    "WHERE (y.year = %d) AND (y.`precious` = %d OR y.`precious` IS NULL) AND (y.`month` = %d OR y.`month` IS NULL) AND (y.`parent` = %d) AND (y.`chart_id` = %d)",
                    $yearData['year'], $yearData['precious'], $yearData['month'], $yearData['parent'], $yearData['chart_id']);

                # $msg = "Debug";
                # send_response_json(['code' => 400, 'message' => $msg, 'yearData' => $yearData, 'sql' => $sql], 400, $msg);
                $year_id = $wpdb->get_var( $sql );
                if( empty($year_id) ) {
                    $year_id = $wpdb->insert( $tableKpiYear, $yearData );
                    if( !empty($year_id) ) {
                        $year_id = $wpdb->insert_id;
                    }
                }
                # $insertData[''];
                # $msg = "Debug";
                # send_response_json(['code' => 400, 'message' => $msg, 'insertData' => $insertData], 400, $msg);

                if( $year_id ){
                    $insert_id = $insertData['year_id'] = $year_id;
                    $res = $wpdb->insert($tableKpi, $insertData);
                    if( !empty( $wpdb->last_error ) ){
                        $lastError[] = $wpdb->last_error;
                    }
                    $results[$uid] = "Insert {$insert_id}";
                    $success[] = $insert_id;
                    # $wpdb->query("COMMIT;");
                }else{
                    $lastError[] = $wpdb->last_error . "\n" . $wpdb->last_query;
                }

                if( empty($lastError) ) {
                    $results[$uid] = "Insert {$insert_id}";
                    $success[] = $insert_id;
                    $wpdb->query("COMMIT;");
                }else{
                    $wpdb->query("ROLLBACK;");
                }
            } else {
	            if( $orgchartUser->alias == 'phongban' ) {
		            $total_percent = $total_percent - $percent;
		            if( $total_percent == 100 ){
			            $getUser = get_user_by("ID", $uid );
			            $display_name = $getUser->display_name;
			            $msg = "Tổng trọng số của <strong>{$display_name}</strong> ở <strong>".__($params['kpi_type'], TPL_DOMAIN_LANG)."</strong> đã đủ 100%";
			            send_response_json(['code' => 401, 'message' => $msg], 401, $msg);
		            }elseif ( $total_percent + $insertData['percent'] > 100 ) {
			            $getUser      = get_user_by( "ID", $uid );
			            $display_name = $getUser->display_name;
			            $msg          = "Tổng trọng số của <strong>{$display_name}</strong> ở <strong>".__($params['kpi_type'], TPL_DOMAIN_LANG)."</strong> vượt mức 100 (Trọng số còn lại có thể triển khai: <strong>" . ( 100 - $total_percent ) . "</strong>)";
			            send_response_json( [ 'code' => 401, 'message' => $msg ], 401, $msg );
		            }
	            }
                $res = $wpdb->update($tableKpi, $insertData, ['id' => $user_kpi_id]);
                if( $res || $res === 0 ) {
                    $results[$uid] = "Updated {$user_kpi_id}";
                    $success[] = $user_kpi_id;
	                unset($arrKPI[$user_kpi_id]);
                } else {
                    $lastError[] = $wpdb->last_error . "\n" . $wpdb->last_query;
                }
                $insert_id = $user_kpi_id;
            }
            if( !empty($lastError) ) {
                $lastError = implode( "\n", $lastError);
                # $errors[$uid] = $wpdb->last_error ? $wpdb->last_error : "Lỗi khi lưu user_id {$uid} " . var_export($res, true);
                $errors[$uid] = $lastError;
                # $errors[$uid] .= "\n{$wpdb->last_query}";
            }
        }
        if( !empty( $arrKPI ) ){
			$strKPIs = implode(", ", $arrKPI);
			$sqlDel = $wpdb->prepare( " DELETE FROM {$tableKpi} WHERE id IN ({$strKPIs})" );
			$del = $wpdb->query($sqlDel);
        }

        if( empty($errors) ) {
            $where_ids = '';
            if( !empty($success) ) {
                $where_ids = " AND `id` NOT IN (" . implode(', ', $success) . ") ";
            }
            $results = $wpdb->get_results($wpdb->prepare("DELETE FROM {$tableKpi} ".
                "WHERE `bank_id` = %d {$where_ids}  AND `parent` = %d AND `chart_id` = %d AND `year_id` = %d",
                $params['bank_id'], $kpi['id'], $u['chart_id'], $params['year_id']), ARRAY_A );
            send_response_json(array_merge($results, ['location' => $location] ), 201, "Success");
        } else {
            $msg = "Có lỗi xảy ra trong quá trình lưu KPI cho nhân viên";
            send_response_json(['code' => 400, 'message' => $msg, 'errors' => $errors], 400, $msg);
        }
    } else {
        $msg = "Vui lòng chọn nhân viên và điền đủ thông tin của những nhân viên được chọn";
        send_response_json(['code' => 404, 'message' => $msg], 404, $msg);
    }
    return;
}


