<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/22/18
 * Time: 15:49
 */
function wp_ajax_post_update_upload_file( $params ){
    global $wpdb;
    require_once THEME_DIR . '/inc/lib-kpis.php';
    $params = wp_unslash($params);
	$userManagers = wp_get_current_user();
	if( empty($userManagers) && is_wp_error( $userManagers ) ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],401, $message);
	}
	if( !has_cap($userManagers, USER_CAP_MANAGERS) && !has_cap( $userManagers, USER_CAP_EMPLOYERS ) ){
		$message = __('Bạn không có quyền thực hiện chức năng này', TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],403, $message);
	}
	$user = get_user_by("ID", $params['uid']);//wp_get_current_user();
	if ( !empty( $user ) && is_wp_error( $user ) || empty( $user ) ) {
		$message  = __( 'Không tìm thấy tài khoản để thực hiện chức năng này. Vui lòng tải lại trang.', TPL_DOMAIN_LANG );
		$httpCode = 401;
		send_response_json( [ 'code' => $httpCode, 'message' => $message ], $httpCode, $message );
	}
    $kpiID = (int) $params['id'];
    $result = kpi_get_kpi_by_id( $kpiID );
    if( empty( $result ) ){
        $message = __('Không tìm thấy KPI', TPL_DOMAIN_LANG );
        $httpCode = 401;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }
    $files = $result['files'];
    $files = maybe_unserialize( $files );
    $files_id = $params['file_id'];
    foreach ( $files_id as $key => $file ){
        $idx = ( (int)$file ) - 1;
        if( array_key_exists($idx, $files['files_url']) ){
            unset($files['files_path'][$idx]);
            unset($files['files_url'][$idx]);
        }
    }
    $files = maybe_serialize( $files );
    $data = ['files' => $files];
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableKpi = "{$prefix}kpis";
    $wpdb->query('START TRANSACTION');
    $wpdb->update($tableKpi, $data, [ 'id' => $kpiID ]);
    if( empty( $wpdb->last_error ) ){
        $wpdb->query("COMMIT;");
        $msg = __('Thành công', TPL_DOMAIN_LANG);
        $httpCode = 201;
        send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
    }else{
        $wpdb->query("ROLLBACK;");
        $msg = __('Có lỗi xảy ra vui lòng thử lại.', TPL_DOMAIN_LANG);
        $httpCode = 405;
        send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
    }
}