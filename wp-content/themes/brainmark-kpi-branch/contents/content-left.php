<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 15/12/2017
 * Time: 23:35
 */

require_once THEME_DIR . '/inc/lib-kpis.php';

$term_kpi = getRootKPICat();

$kpiByCategories = kpiListCategories();

function menu_left_by_taxonomy($kpiByCategories, $link = '', $parent = 0, $class='parent-categories navbar-left', $htmlChart = ''){
	$cate = []; $output = '';
    $kpiByCategories = ($kpiByCategories instanceof WP_Term) ? [$kpiByCategories] : $kpiByCategories;

    foreach( $kpiByCategories as $key => $value ){
		if($value->parent == $parent){
			$cate[] = $value;
			unset($kpiByCategories[$key]);
		}
	}
	if($cate) {
            #$kpi_by_position_system = __('kpi-by-position-system', TPL_DOMAIN_LANG);
            $output = '<ul class="' . $class . '">';
            foreach ($cate as $key => $value) {
                $termName = $value->name;
                $termID = $value->term_id;
                if( empty( $link ) ) {
                    $termLink = get_category_link( $termID ) ;
                }else{
                    $termSlug = $value->slug;
                    # $termLink = $link . $termSlug;
                    $termLink = add_query_arg('cat', $termSlug, $link);
                }
                $system_class = '';
                if (!empty($kpiByCategories)) {
                    $outputChildren = menu_left_by_taxonomy( $kpiByCategories, $link, $termID, 'children '.$termSlug );
                }else {
                    $outputChildren = '';
                }
                $icon_dropdown = '';
	            $classDropdown = '';
	            if( !empty($outputChildren) ){
		            $icon_dropdown = "<i class=\"fa fa-caret-down\" aria-hidden=\"true\"></i>";
		            $classDropdown = "event-dropdown";
	            }
                $cat_slug = !empty($_REQUEST['cat']) ? $_REQUEST['cat'] : '';
                $active = '';
                if( $cat_slug == $termSlug ) {
                    $system_class .= ' active';
                }
                if( !preg_match("#(he-thong-chuc-danh)#", $termSlug) ) {
	                $output .= sprintf( '<li class="cat-item %s"><a data-cat="%s" class="%s" href="%s" title="%s">%s %s</a>',
		                $system_class, esc_attr( $termSlug ), $classDropdown, esc_attr( $termLink ), esc_attr( $termName ), $termName, $icon_dropdown );
	                $output .= $outputChildren;
	                $output .= '</li>';
                }else{
	                $output .= sprintf( '<li class="cat-item %s"><a data-cat="%s" class="%s" href="%s" title="%s">%s %s</a>',
		                $system_class, esc_attr( $termSlug ), $classDropdown, esc_attr( $termLink ), esc_attr( $termName ), $termName, $icon_dropdown );
	                $output .= $htmlChart;
	                $output .= '</li>';
                }
            }
            $output .= '</ul>';
	}
	return $output;
}

$kpi_bank = get_page_by_path(PAGE_KPI_BANK);
$kpi_bank_url = apply_filters( 'the_permalink', get_permalink( $kpi_bank ), $kpi_bank );
if( user_is_manager() ){
	$listOrgChart = orgchart_get_list_orgchart();
}else {
	$listOrgChart = orgchart_get_list_orgchart_skip_TGD();
}

$kpiCD = '';
$cat_slug = !empty($_REQUEST['cat']) ? $_REQUEST['cat'] : '';
if( !empty( $listOrgChart ) ){
	$groupOrgChart = array_group_by($listOrgChart, 'room');
	foreach ( $groupOrgChart as $key => $itemGroup ) {
		$ic             = 0;
		$output = '';
		foreach ( $itemGroup as $k => $item ) {
			$ic ++;
			$system_class = '';
			if ( $cat_slug == $item->id ) {
				$system_class .= ' active';
			}
			$url    = add_query_arg( 'cat', $item->id, $kpi_bank_url );
			$output .= sprintf( '<li class="cat-item %s"><a data-cat="%s" href="%s" title="%s">%s</a></li>',
				$system_class, esc_attr( $item->id ), esc_attr( $url ), $item->name, $item->name );
		}
		#$urlGroup = add_query_arg( 'cat', $itemGroup->id, $kpi_bank_url );
		if ( $ic > 1 ) {
			$icon_dropdown = "<i class=\"fa fa-caret-down\" aria-hidden=\"true\"></i>";
			$classDropdown = "event-dropdown";
			$output = "<ul class='children'>{$output}</ul>";
			$kpiCD         .= sprintf( '<li class="cat-item %s"><a data-cat="%s" class="%s" href="javascript:;" title="%s">%s %s</a> %s </li>',
				$system_class, sanitize_title( $key ), $classDropdown, $key, $key, $icon_dropdown, $output );
		} elseif ( $ic == 1 ) {
			$kpiCD .= $output;
		}
	}
	if( !empty( $kpiCD ) ){
	    $kpiCD = "<ul class='children'>{$kpiCD}</ul>";
    }
}
#echo $kpiCD;
?>
<div id="menu-left" class="col-sm-3">
<?php
    #echo $kpiCD;
    if( is_array( $kpiByCategories ) && !is_wp_error( $kpiByCategories ) ):
        echo menu_left_by_taxonomy($kpiByCategories, $kpi_bank_url, $term_kpi->term_id, 'parent-categories navbar-left', $kpiCD);

    endif;
?>

    <ul class="parent-categories navbar-left navbar-formula">
        <?php
            $formula = add_query_arg('type', 'formula');
            $title = __('Phương pháp đo', TPL_DOMAIN_LANG);
        ?>
        <li class="cat-item">
            <a href="<?= $formula; ?>" title="<?= esc_attr($title); ?>"><?php echo $title; ?></a>

        </li>
    </ul>
</div>
<?php
#require_once ABSPATH . '_files/kpi-bank/content-left.php';
