<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 26/12/2017
 * Time: 23:13
 */

global $wpdb;

require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';

$user = wp_get_current_user();
$orgchart = user_load_orgchart($user);
if( !isset($_REQUEST['cid']) ) {
    $chart = kpi_get_orgchart_by_id($user->orgchart_id);
    $_GET['cid'] = $_REQUEST['cid'] = !is_null($chart) ? (int)$chart['id'] : 0;
}
$member_role = $orgchart ? $orgchart->role : '';
if( empty($member_role) ) {
    $member_role = '';
} else {
    $member_role = strtolower($member_role);
}
$year_status = ( ($member_role == 'bgd') || $user->has_cap('level_9') ) ? '' : KPI_YEAR_STATUS_PUBLISH;
#$year_list = kpi_get_year_list( $GLOBALS['parent_year_id'], $year_status );
$year_list = year_get_list_year_ceo();
$firstYear = kpi_get_first_year_role( kpi_get_first_year( 0, $year_status ) );
# echo '<pre class="role-dang-ky-kpi" style="display: block !important;">'; var_dump(__LINE__, $year_list, $firstYear); echo '</pre>';

$orgchart_id = !empty($_GET['cid']) ? (int)$_GET['cid'] : $user->orgchart_id;
$user_id = $user->ID;
$user_id = 0;
$groups = [
        'finance' => [
            'key' => 'finance',
            'title' => __('Finance', TPL_DOMAIN_LANG),
            'percent' => $firstYear ? $firstYear['finance'] : '',
            'time' => TIME_YEAR_VALUE,
            'year_id' => $firstYear,
            'items' => kpi_get_list('Finance', 'YEAR', TIME_YEAR_VALUE, $user_id, $orgchart_id)
        ],
        'operate' => [
            'key' => 'operate',
            'title' => __('Operate', TPL_DOMAIN_LANG),
            'percent' => $firstYear ? $firstYear['operate'] : '',
            'time' => TIME_YEAR_VALUE,
            'year_id' => $firstYear,
            'items' => kpi_get_list('Operate', 'YEAR', TIME_YEAR_VALUE, $user_id, $orgchart_id)
        ],
        'customer' => [
            'key' => 'customer',
            'title' => __('Customer', TPL_DOMAIN_LANG),
            'percent' => $firstYear ? $firstYear['customer'] : '',
            'time' => TIME_YEAR_VALUE,
            'year_id' => $firstYear,
            'items' => kpi_get_list('Customer', 'YEAR', TIME_YEAR_VALUE, $user_id, $orgchart_id)
        ],
        'development' => [
            'key' => 'development',
            'title' => __('Development', TPL_DOMAIN_LANG),
            'percent' => $firstYear ? $firstYear['development'] : '',
            'time' => TIME_YEAR_VALUE,
            'year_id' => $firstYear,
            'items' => kpi_get_list('Development', 'YEAR', TIME_YEAR_VALUE, $user_id, $orgchart_id)
        ],
];

function render_group_list($item) {
    ?>
    <div class="content-list-register-<?php echo $item['key']; ?> content-list-register">
        <table class="table-block-list-register table-list-<?php echo $item['key']; ?>" cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <th class="column-1-2" colspan="2"><?php echo $item['title']; ?></th>
                <th class="percent column-3">
                    <a href="javascript:;" data-action="update" data-year="<?php echo esc_json_attr($item['year_id']); ?>" data-toggle="modal"
                       data-target="#tpl-kpi-year" >
                        <span class="icon-percent" contenteditable="false" data-kpi-percent="<?php echo $item['key']; ?>"><?php echo $item['percent']; ?>%</span>
                        <i class="glyphicon glyphicon-pencil hidden" aria-hidden="true"></i>
                    </a>
                    <input type="text" class="txt-percent-kpi hidden" value="30" />
                </th>
            </tr>
            </thead>
            <tbody>
            <?php
            $items = $item['items'];
            for( $i = 0, $n = count($items); $i < $n; $i++ ) {
                $kpi = &$items[$i];
                ?>
                <tr>
                    <td class="column-1"><?= ($i+1); ?>.</td>
                    <td class="column-2"><?php echo ($kpi['post_title']); ?></td>
                    <td class="column-3"><?= $kpi['percent']; ?>%</td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
    <?php
}

?>
<div id="block-register-kpi" class="block-register-kpi-bgd blocik-item block-shadow border-radius-default">
    <div class="header-register-kpi header-block">
        <div class="for-date">
            <div class="for-year">
                <label for="select-for-year"><?php echo _LBL_YEAR;//_e('Năm', TPL_DOMAIN_LANG); ?></label>
                <select class="selectpicker select-for-year" id="select-for-year" name="">
                    <option value="">-- Năm --</option>
                    <?php foreach ($year_list as $item):
                        $selected = $item['year'] == TIME_YEAR_VALUE ? 'selected="selected"' : '';
                        echo sprintf('<option value="%s" %s>%s</option>', $item['year'], $selected, $item['year']);
                    endforeach;
                    ?>
                </select>
                <a href="javascript:;" id="create-kpi-new-year" data-year-id="new" data-toggle="modal"
                   data-target="#tpl-kpi-year" ><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="name-block">
            <h2><?php echo _Objective_KPI; //_e('Mục tiêu KPI', TPL_DOMAIN_LANG); ?></h2>
            <select class="selectpicker" data-change-alias="true">
                <?php get_user_orgchart_dropdown( $user, true,[CHART_ALIAS_MANAGER, CHART_ALIAS_DEPARTMENTS]); ?>
            </select>
        </div>
    </div>
    <div class="top-register-kpi">
        <div class="col-md-4 register-kpi-1">
            <?php get_template_part('contents/block-' . $member_role . '/chart'); ?>
        </div>
        <div class="col-md-8">
            <div class="col-md-6 register-kpi-2"><?php
                render_group_list($groups['finance']);
                render_group_list($groups['operate']);
            ?></div>
            <div class="col-md-6 register-kpi-3"><?php
                render_group_list($groups['customer']);
                render_group_list($groups['development']);
            ?></div>

        </div>
    </div>

    <?php get_template_part( 'contents/registers/kpi', 'detail-' . $member_role ); ?>
</div>
