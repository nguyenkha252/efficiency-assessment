<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/8/18
 * Time: 17:44
 */

global $wpdb, $orgchart, $member_role, $user, $chartRoot, $chartCurrent, $chartParent, $chartParentName, $chartCurrentName;
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-orgchart.php';

$add_target = wp_create_nonce('add_target');
$user = wp_get_current_user();
$orgchart = user_load_orgchart($user);
$member_role = $orgchart ? $orgchart->role : '';
$yearEmpty = $GLOBALS['yearEmpty'];
$firstYear = $yearInfo = $GLOBALS['yearInfo'];
$chartParent = $GLOBALS['chartParent'];
$time_values = TIME_YEAR_VALUE;
$tabs = [
    'finance' => [
        'title' => _TABS_Finance,
        'type' => 'Finance',
        'percent' => $yearInfo ? $yearInfo['finance'] : '0',
        'img' => 'tai-chinh.png',
        'time_type' => 'YEAR',
        'time_value' => $time_values,
        'year_id' => $yearInfo ? $yearInfo : $yearEmpty
    ],
    'customer' => [
        'title' => _TABS_Customer,
        'type' => 'Customer',
        'percent' => $yearInfo ? $yearInfo['customer'] : '0',
        'img' => 'customer.png',
        'time_type' => 'YEAR',
        'time_value' => $time_values,
        'year_id' => $yearInfo ? $yearInfo : $yearEmpty
    ],
    'operate' => [
        'title' => _TABS_Operate,
        'type' => 'Operate',
        'percent' => $yearInfo ? $yearInfo['operate'] : '0',
        'img' => 'van-hanh.png',
        'time_type' => 'YEAR',
        'time_value' => $time_values,
        'year_id' => $yearInfo ? $yearInfo : $yearEmpty
    ],
    'development' => [
        'title' => _TABS_Development,
        'type' => 'Development',
        'percent' => $yearInfo ? $yearInfo['development'] : '0',
        'img' => 'phat-trien.png',
        'time_type' => 'YEAR',
        'time_value' => $time_values,
        'year_id' => $yearInfo ? $yearInfo : $yearEmpty
    ]
];
$chartCurrent = $GLOBALS['chartCurrent'];
$firstYearCty = year_get_firt_year_by_congty( TIME_YEAR_VALUE );
$year_parent = !empty($firstYearCty) ? $firstYearCty['id'] : 0;
if( !isset( $_GET['cid'] ) || $chartCurrent['parent'] == 0 ){
	$firstYearCurrent = $firstYearCty;
}else{
    $firstYearCty = $GLOBALS['yearInfo'];
    $year_parent = !empty($firstYearCty) ? $firstYearCty['id'] : 0;
	$firstYearCurrent = year_get_year_by_chart_id( $chartCurrent['id'], TIME_YEAR_VALUE, $year_parent );
}

$yearID = !empty( $firstYearCurrent ) ? $firstYearCurrent['id'] : 0;

require_once THEME_DIR . '/inc/render-kpi-item-row.php';

?>
<div class="clearfix"></div>
<div class="block-detail block-result-kpi-detail">
    <div id="room-target-kpi" data-type="bgd" data-content-management="" style="display: block">
        <ul id="" class="list-room-target-kpi clearfix nav nav-pills">
            <?php
            foreach ($tabs as $tabkey => $tabData):
                $cls = ('finance' == $tabkey) ? 'active' : '';
                ?>
                <li class="room-target-kpi-item <?php echo $cls; ?> room-<?php echo $tabkey; ?>">
                    <a href="#room-<?php echo $tabkey; ?>" class="" title="" data-toggle="tab" data-tab-type="bgd">
                        <img src="<?php echo THEME_URL; ?>/assets/images/<?php echo $tabData['img']; ?>"/>
                        <?php echo $tabData['title']; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>

        <div class="list-detail tab-content clearfix">
            <?php foreach ($tabs as $tabkey => $tabData):
                $orgchart_id = !empty($_GET['cid']) ? (int)$_GET['cid'] : $orgchart->id;
                $charts = kpi_get_list_org_charts($orgchart_id, false);
                $cls = ('finance' == $tabkey) ? 'active' : '';
                $actionColumn = 7; # $chartParent ? 6 : 7;
                ?>
                <div class="tab-pane <?php echo $cls; ?>" id="room-<?php echo $tabkey; ?>">
                    <div class="list-detail">
                        <div class="top-list-detail">
                            <h3 class="title">
                                <?php echo $tabData['title']; ?>
                                <a href="javascript:;"<?php if(USER_IS_ADMIN): ?> data-action="update"
                                    data-year="<?php echo esc_json_attr($tabData['year_id']); ?>" data-toggle="modal"
                                   data-target="#tpl-kpi-year"<?php endif; ?>>
                                    <span class="icon-percent" contenteditable="false" data-kpi-percent="<?php echo $tabkey; ?>"><?php echo $tabData['percent']; ?>%</span>
                                    <i class="glyphicon glyphicon-pencil hidden" aria-hidden="true"></i>
                                </a>
                            </h3>
                        </div>
                    </div>
                    <form data-approved="" action="<?php echo admin_url('admin-ajax.php?action=ceo_deployment'); ?>" class="form-trien-khai-kpi form-trien-khai-kpi-bgd"
                          id="form-<?php echo $tabkey; ?>" method="post" enctype="application/x-www-form-urlencoded"
                          onsubmit="return false">
                        <input value="<?php echo wp_create_nonce('ceo_deployment'); ?>" type="hidden" name="_wpnonce">
                        <input value="<?php echo $tabData['type']; ?>" type="hidden" name="type">
                        <input type="hidden" name="_wp_http_referer" value="<?php esc_attr_e( wp_unslash( $_SERVER['REQUEST_URI'] ) ); ?>#room-<?= $tabkey; ?>">
                        <div class="container-message hidden"></div>
                        <table class="table-list-detail table-managerment-target-kpi" cellpadding="0" cellspacing="0">
                            <thead>
                            <tr>
                                <th class="column-1 kpi-id align-center"><?php _e('Mã KPI', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-2 kpi-content"><?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-3 kpi-percent align-center"><?php _e('Trọng số', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-4 kpi-plan"><?php echo    __('Kế hoạch', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-5 kpi-receive align-center"><?php _e('Thời điểm<br>nhận kết quả', TPL_DOMAIN_LANG); ?></th>
                                <?php #if( $chartParent ): ?>
                                    <th class="column-6 kpi-aproved align-center"><?php _e('Trạng Thái', TPL_DOMAIN_LANG); ?></th>
                                <?php #endif; ?>
                                <th class="column-<?php echo $actionColumn; ?> kpi-action align-center"><?php _e('Thao Tác', TPL_DOMAIN_LANG); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $totalPercent = 0.0;
                            global $wpdb;
                            $groupDatas = kpi_get_list_if_not_and_get_from_parent($tabData['type'], $tabData['time_type'], $tabData['time_value'], 0, $orgchart_id);

                            if (!empty($groupDatas)):
                                foreach ($groupDatas as &$item):
                                    $totalPercent += doubleval($item['percent']);
                                    $html = kpi_render_ceo_row_item($item, $tabData['type'], $chartCurrent, $chartParent, null);
                                    echo $html;
                                endforeach;
                            endif;

                            $addData = [
                                'id' => '',
                                'year_id' => $yearInfo ? $yearInfo['id'] : 0,
                                'formula_id' => 0,
                                'bank_id' => '',
                                'unit' => '',
                                'actual_gain' => '',
                                'status_result' => 'draft',
                                'name' => $chartCurrent ? $chartCurrent['name'] : '',
                                'receive' => date('31-12-Y', time()),
                                'percent' => 0,
                                'parent' => 0,
                                'chart_id' => $_GET['cid'],
                                'user_id' => $user->ID,
                                'aproved' => 1,
                                'plan' => '',
                                'post_title' => '',
                                'type' => $tabData['type'],
                                'status' => KPI_STATUS_DRAFT,
                                'required' => 'yes',
                                'owner' => 'no',
                                'create_by_node' => 'node_start',
                                'bank_parent' => 0,
                                'influence' => '',
                            ];
                            $load_params = [
                                'action' => 'load-parent-bank-list',
                                'kpi_type' => $tabData['type'],
                                'cat_slug' => $tabkey,
                                'year_id' => $yearInfo ? $yearInfo['id'] : 0,
                                'cid' => $_GET['cid'],
                                'year' => $yearInfo ? $yearInfo['year'] : 0,
                                '_wpnonce' => wp_create_nonce('load-parent-bank-list'),
                            ];
                            ?>
                            <tr class="editer-item hidden" data-tabkey="<?php echo $tabkey; ?>" data-add-info="<?php echo esc_json_attr($addData); ?>">
                                <td class="column-1 kpi-id align-center">
                                    <input type="hidden" name="postdata[id]" value="">
                                    <span class="item-id"></span>
                                </td>
                                <td class="column-2 kpi-content">
                                    <input type="text" class="form-control" name="postdata[post_title]" placeholder="Nội dung mục tiêu" value="">
                                    <div class="input-group">
                                        <span class="input-group-addon">Công thức tính: </span><!-- data-container="this.parentNode.parentNode.parentNode" -->
                                        <select class="selectpicker" data-live-search="true" name="formula_id" data-use-bootstrap="">
                                            <option value="0" ><?php _e('Không áp dụng công thức',TPL_DOMAIN_LANG); ?></option>
                                            <?php
                                            require_once THEME_DIR . '/inc/lib-formulas.php';
                                            $listFormulas = get_list_formula();
                                            if( !empty( $listFormulas ) && !is_wp_error( $listFormulas ) ) {
                                                foreach ( $listFormulas as $key => $formula ){
                                                    $formulaTitle = $formula->title;
                                                    $formulaID = $formula->ID;
                                                    echo "<option value='".esc_attr($formulaID)."'>".esc_attr($formulaID)." - " . esc_attr( $formulaTitle ) . "</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <?php
                                    if( !(!empty($chartCurrent) && !empty($chartRoot) && $chartCurrent['id'] == $chartRoot['id']) ):

	                                    $items = kpi_get_list($tabData['type'], $tabData['time_type'], $tabData['time_value'], 0, $chartParent ? $chartParent['id'] : $user->orgchart_id);
                                        ?>
                                    <div class="input-group group-influence">
                                        <label class="form-control switch influence" id="influence-<?php echo "{$item['id']}_id-{$tabkey}"; ?>">
                                            <input class="checkbox-status" name="postdata[influence]"
                                                   value="1" type="checkbox">
                                            <span class="checkbox-slider round"></span>
                                            <span class="text">Mục tiêu ảnh hưởng</span>
                                        </label>
                                    </div>
                                        <h4>Mục Tiêu <?php echo $chartParentName; ?></h4>
                                        <ul class="list-group">
                                            <?php
                                            foreach($items as $item):
                                                $item['receive'] = substr(kpi_format_date($item['receive']), 0, 10);
                                                ?>
                                                <li class="list-group-item" data-kpi-parent="<?php echo esc_json_attr($item); ?>">
                                                    <label class="form-control switch bank-parent" id="bank-parent-<?php echo "{$item['id']}_id-{$tabkey}"; ?>">
                                                        <input class="checkbox-status" name="postdata[parent]"
                                                               value="<?php echo $item['id']; ?>" type="radio">
                                                        <span class="checkbox-slider round"></span>
                                                        <span class="text"><?php echo $item['post_title']; ?></span>
                                                    </label>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul><?php
                                    endif;
                                    ?>
                                </td>
                                <td class="column-3 kpi-percent align-center">
                                    <input class="form-control" placeholder="Tỷ trọng (%)"
                                           name="postdata[percent]" value=""></td>
                                <td class="column-4 kpi-plan">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="postdata[plan]" placeholder="Kế hoạch" value="">
                                        <select name="postdata[unit]" class="selectpicker" data-none-selected-text="Chưa chọn đơn vị">
                                            <option value=""> - Đơn Vị Tính - </option>
                                            <?php
                                            $units = getUnit();
                                            foreach($units as $val => $text):
                                                echo sprintf('<option value="%s">%s</option>', $val, $text);
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </td>

                                <td class="column-5 kpi-receive">
                                    <div class="input-group">
                                        <input class="form-control" name="postdata[receive]" placeholder="Ngày hết hạn" value=""
                                               data-lang="vi" btn-date=".input-group-addon.date-btn" data-group-date=".input-group"
                                               data-format="<?php echo KPI_FORMAT_DATE; ?>"
                                               data-min-date="01-01-<?php echo $tabData['time_value'][0]; ?>"
                                            <?php /* data-max-date="" */ ?>
                                               data-group-date=".group-date" data-timepicker="false"
                                               data-btn-date=".input-group-addon.date-btn"
                                               data-ctrl-date>
                                        <span class="input-group-addon date-btn">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </td>
                                <td class="column-6 kpi-aproved align-center">
                                    <?php if( $yearInfo ):
                                        $load_params = [
                                            'action' => 'save-ceo-target',
                                            'kpi_type' => $tabData['type'],
                                            'cat_slug' => $tabkey,
                                            'year_id' => $yearInfo ? $yearInfo['id'] : 0,
                                            'cid' => $_GET['cid'],
                                            'year' => $yearInfo ? $yearInfo['year'] : 0,
                                            '_wpnonce' => wp_create_nonce('save-ceo-target'),
                                        ];
                                    ?>
                                    <a href="javascript:;" data-type="post"
                                       data-url="<?php echo esc_attr( add_query_arg($load_params, admin_url('admin-ajax.php') ) ); ?>"
                                       class="action-save btn btn-sm" title="<?php _e('Lưu', TPL_DOMAIN_LANG);
                                    ?>" data-kpi-type="<?php echo $tabData['type']; ?>">Lưu nháp</a>
                                    <?php endif; ?>
                                </td>
                                <td class="column-<?php echo $actionColumn; ?> kpi-action align-center">
                                    <?php if( $yearInfo ): ?>
                                        <a href="javascript:;" class="action-cancel btn btn-sm">Bỏ qua</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            </tbody>
                            <?php if( !empty($yearInfo) && $totalPercent < 100): ?>
                            <tfoot>
                            <tr>
                                <td colspan="<?php echo (6 + ( $chartParent ? 1 : 0 ) ); ?>">
                                    <a href="javascript:;" class="action-add" data-ac="add" data-show="show-row"><i
                                                class="fa fa-plus-circle"
                                                aria-hidden="true"></i> <?php _e('Thêm mục tiêu', TPL_DOMAIN_LANG); ?>
                                    </a>
                                </td>
                            </tr>
                            </tfoot>
                            <?php endif; ?>
                        </table>
                        <div class="action-bot">
                            <?php if( empty( $yearID ) ): ?>
                                <span class="notification warning"><i class="fa fa-warning"></i> <?php echo _MESSAGE_EMPTY_DATA_BY_KPI_GROUP; ?></span>
                                <?php else: ?>
                                <button type="submit" class="btn-brb-default btn-save-and-public btn-action-item"
                                                                                        name="btn-save-public"><i class="fa fa-floppy-o"
                                                                                                                  aria-hidden="true"></i> <?php _e('Triển khai', TPL_DOMAIN_LANG); ?>
                                </button>
                            <?php endif; ?>
                            <?php /* <button type="button" class="btn-brb-default btn-print btn-action-item" name="btn-print"><i
                                        class="fa fa-print"
                                        aria-hidden="true"></i> <?php _e('Xuất báo cáo', TPL_DOMAIN_LANG); ?></button> */ ?>
                        </div>
                    </form>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    </div>
</div>
