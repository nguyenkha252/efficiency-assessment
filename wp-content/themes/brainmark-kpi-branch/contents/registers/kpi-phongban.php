<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 26/12/2017
 * Time: 23:13
 */

global $tabsTitle;

require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';

$user = wp_get_current_user();
$orgchart = user_load_orgchart($user);
$member_role = $orgchart ? $orgchart->role : '';
if( empty($member_role) ) {
    $member_role = '';
} else {
    $member_role = strtolower($member_role);
}

$data = [];
for( $i = 0; $i<=30; $i++ ){
    $data[$i] = 10;
}
$gradient_1 = Gradient( "ff1300", "ffe100" , 7 );
$gradient_2 = Gradient( "ffe100", "84e91c" , 7 );
$gradient_3 = Gradient( "84e91c", "84e91c" , 7 );
$dataSet = ['datasets' => [['backgroundColor' => array_merge($gradient_1, $gradient_2, $gradient_3 ), 'data' => array_map('intval', $data)]] ];

global $wpdb;
$year_list = kpi_get_year_list($GLOBALS['parent_year_id'], KPI_YEAR_STATUS_PUBLISH);
#$firstYear = kpi_get_first_year($GLOBALS['parent_year_id'], KPI_YEAR_STATUS_PUBLISH);
$firstYear = year_get_year_by_chart_id( $orgchart->id, TIME_YEAR_VALUE );
if( $firstYear ) {
    $precious_list = kpi_get_precious_list($firstYear ? $firstYear['id'] : -1, '');
    $firstPrecious = kpi_get_first_precious($firstYear ? $firstYear['id'] : -1, '');
} else {
    $precious_list = null;
    $firstPrecious = null;
}

$groups = [
    'finance' => [
        'key' => 'finance',
        'title' => _TABS_Finance,
        'percent' => $firstYear ? $firstYear['finance'] : '',
        'time' => TIME_YEAR_VALUE,
        'year_id' => $firstYear,
        'items' => kpi_get_list('Finance', 'YEAR', TIME_YEAR_VALUE, $user->ID, $user->orgchart_id, 1)
            # kpi_get_list('Finance', 'PRECIOUS', TIME_PRECIOUS_VALUE, $user->ID, $user->orgchart_id, 1)
    ],
    'operate' => [
        'key' => 'operate',
        'title' => _TABS_Operate,
        'percent' => $firstYear ? $firstYear['operate'] : '',
        'time' => TIME_YEAR_VALUE,
        'year_id' => $firstYear,
        'items' => kpi_get_list('Operate', 'YEAR', TIME_YEAR_VALUE, $user->ID, $user->orgchart_id, 1)
            # kpi_get_list('Operate', 'PRECIOUS', TIME_PRECIOUS_VALUE, $user->ID, $user->orgchart_id, 1)
    ],
    'customer' => [
        'key' => 'customer',
        'title' => _TABS_Customer,
        'percent' => $firstYear ? $firstYear['customer'] : '',
        'time' => TIME_YEAR_VALUE,
        'year_id' => $firstYear,
        'items' => kpi_get_list('Customer', 'YEAR', TIME_YEAR_VALUE, $user->ID, $user->orgchart_id, 1)
            # kpi_get_list('Customer', 'PRECIOUS', TIME_PRECIOUS_VALUE, $user->ID, $user->orgchart_id, 1)
    ],
    'development' => [
        'key' => 'development',
        'title' => _TABS_Development,
        'percent' => $firstYear ? $firstYear['development'] : '',
        'time' => TIME_YEAR_VALUE,
        'year_id' => $firstYear,
        'items' =>
            kpi_get_list('Development', 'YEAR', TIME_YEAR_VALUE, $user->ID, $user->orgchart_id, 1)
            # kpi_get_list('Development', 'PRECIOUS', TIME_PRECIOUS_VALUE, $user->ID, $user->orgchart_id, 1)
    ],
];

function render_group_list($item) {
    ?>
    <div class="content-list-register-<?php echo $item['key']; ?> content-list-register">
        <table class="table-block-list-register table-list-<?php echo $item['key']; ?>" cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <th class="column-1-2" colspan="2"><?php echo $item['title']; ?></th>
                <th class="percent column-3">
                    <a href="javascript:;" data-year="<?php echo esc_json_attr($item['year_id']); ?>" data-toggle="modal"
                       data-target="#tpl-ceo-kpi-year" >
                        <span class="icon-percent" contenteditable="false" data-kpi-percent="<?php echo $item['key']; ?>"><?php echo $item['percent']; ?>%</span>
                        <i class="glyphicon glyphicon-pencil hidden" aria-hidden="true"></i>
                    </a>
                    <input type="text" class="txt-percent-kpi hidden" value="30" />
                </th>
            </tr>
            </thead>
            <tbody>
            <?php
            $items = $item['items'];
            for( $i = 0, $n = count($items); $i < $n; $i++ ) {
                $kpi = &$items[$i];
                ?>
                <tr>
                    <td class="column-1"><?= ($i+1); ?>. <?php if( ($kpi['required'] == 'yes') ): ?> <span class="bgd glyphicon glyphicon-star"></span><?php endif; ?></td>
                    <td class="column-2"><?php echo ($kpi['post_title']); ?></td>
                    <td class="column-3"><?= $kpi['percent']; ?>%</td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
    <?php
}

?>
<div id="block-register-kpi" class=" block-shadow block-item border-radius-default">
    <div class="header-register-kpi header-block">
        <div class="name-block col-lg-8 col-md-6 col-sm-12">
            <h2><?php echo _Objective_KPI; //_e('Mục tiêu KPI', TPL_DOMAIN_LANG); ?></h2>
        </div>
        <form id="form-choose-year-precious" class="for-date1 col-lg-4 col-md-6 col-sm-12" action="" method="get" enctype="application/x-www-form-urlencoded">
            <div class="col-lg-4 offset-lg-4 col-md-5 offset-md-2 col-year">
                <label for="select-for-year-phongban"><?php echo _LBL_YEAR;//_e('Năm', TPL_DOMAIN_LANG); ?></label>
                <select class="selectpicker select-for-year" id="select-for-year-phongban" name="nam">
                    <option value="">-- Năm --</option>
                    <?php foreach ($year_list as $item):
                        $selected = $item['year'] == TIME_YEAR_VALUE ? 'selected="selected"' : '';
                        echo sprintf('<option value="%s" %s>%s</option>', $item['year'], $selected, $item['year']);
                    endforeach;
                    ?>
                </select>
            </div>
            <div class="col-lg-4 col-md-5 col-precious">
                <label for="select-for-precious-phongban"><?php _e('Quý', TPL_DOMAIN_LANG); ?></label>
                <select class="selectpicker select-for-year" id="select-for-precious-phongban" name="quy">
                    <option value="">-- Quý --</option>
                    <?php
                    for ( $i = 1; $i <= 4; $i++ ){
                        $selected = $i == TIME_PRECIOUS_VALUE ? 'selected="selected"' : '';
                        echo sprintf('<option value="%s" %s>%s</option>', $i, $selected, $i);
                    }
                    #if( !empty($precious_list) ):
                        #foreach ($precious_list as $item):
                        #    $selected = $item['precious'] == TIME_PRECIOUS_VALUE ? 'selected="selected"' : '';
                        #    echo sprintf('<option value="%s" %s>%s</option>', $item['precious'], $selected, $item['precious']);
                        #endforeach;
                    #endif;
                    ?>
                </select>
            </div>
            <div class="hidden">
                <a href="javascript:;" id="create-kpi-new-precious" data-year="#select-for-year-phongban"
                   data-precious="#select-for-precious-phongban" data-year-id="new" data-toggle="modal"
                   data-target="#tpl-ceo-kpi-precious" ><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
            </div>
        </form>
        <div id="tpl-ceo-kpi-precious--" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
            <form class="modal-dialog modal-sm" enctype="application/x-www-form-urlencoded" method="post"
                  data-create-url="<?php echo admin_url('admin-ajax.php?action=add_phongban_kpi_year&_wpnonce='. wp_create_nonce('update_phongban_kpi_year') ); ?>"
                  data-update-url="<?php echo admin_url('admin-ajax.php?action=update_phongban_kpi_year&_wpnonce='. wp_create_nonce('update_phongban_kpi_year') ); ?>">

                <input type="hidden" name="year_id" value="">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><?php _e('Tạo KPI năm', TPL_DOMAIN_LANG); ?></h4>
                    </div>
                    <div class="modal-body clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group input-group input-group-select">
                                <label class="input-group-addon" for="year_id" id="year"><?php echo _LBL_YEAR;?> </label>
                                <select class="form-control selectpicker" required aria-describedby="year" id="year_id" name="year" style="min-width: 100px">
                                    <option value="">-- Năm --</option>
                                    <?php
                                    $years = intval(date('Y', time()));
                                    $years = [$years - 1, $years + 3];
                                    for($y = $years[0]; $y < $years[1]; $y++ ) {
                                        echo sprintf('<option value="%d">Năm %d</option>', $y, $y);
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group input-group input-group-select">
                                <label class="input-group-addon" for="precious_id" id="precious">Quý: </label>
                                <select class="form-control selectpicker" required aria-describedby="precious" id="precious_id" name="precious" style="min-width: 100px">
                                    <option value="">-- Quý --</option>
                                    <?php
                                    $quy = [1, 5];
                                    for($y = $quy[0]; $y < $quy[1]; $y++ ) {
                                        echo sprintf('<option value="%d">Quý %d</option>', $y, $y);
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <button type="submit" class="btn btn-primaryy">Lưu</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="top-register-kpi">
        <div class="col-md-4 register-kpi-1">
            <?php get_template_part('contents/block-' . $member_role . '/chart'); ?>
        </div>
        <div class="col-md-8">
            <div class="col-md-6 register-kpi-2">
                <?php
                render_group_list($groups['finance']);
                render_group_list($groups['operate']);
                ?>
            </div>
            <div class="col-md-6 register-kpi-3">
                <?php
                render_group_list($groups['customer']);
                render_group_list($groups['development']);
                ?>
            </div>
        </div>
    </div>

    <?php

    # var_dump(THEME_DIR . '/contents/registers/kpi-detail-' . $member_role);
    get_template_part( 'contents/registers/kpi', 'detail-' . $member_role ); ?>
</div>
