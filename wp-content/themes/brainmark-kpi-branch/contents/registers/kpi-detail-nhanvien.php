<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/8/18
 * Time: 17:44
 */

require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-formulas.php';
$user = wp_get_current_user();
$orgchart = user_load_orgchart($user);
$orgchartID = $orgchart->id;
$add_target = wp_create_nonce('add_personal_target');
$departments = kpi_orgchart_get_list();
$departments = process_children($departments);

$firstYear = kpi_get_first_year(0,KPI_YEAR_STATUS_PUBLISH);
$parentChart = kpi_get_orgchart_by_id( (int)$orgchart->parent );

$kpiForStaff = get_kpi_by_year_orgchart_not_month_not_precious(TIME_YEAR_VALUE, $user->ID);
#$kpiForStaffPrecious = get_kpi_by_year_orgchart_not_month($firstYear['year'], $user->ID);
$listAsign = get_kpi_asign_for_member(TIME_YEAR_VALUE, $user->ID );

$firtKpiForStaff = [];

global $wpdb;

$tabs = [
    'target_work' => [
        'title' => __('Mục tiêu công việc', TPL_DOMAIN_LANG),
        'type' => 'target_work',
        'percent' => $firstYear ? $firstYear['target_work'] : '',
        'img' => 'tai-chinh.png',
        'time_type' => 'MONTH',
        'time_value' => TIME_MONTH_VALUE,
        'year_id' => $firstYear
    ],
    /*'behavior' => [
        'title' => __('Thái độ hành vi', TPL_DOMAIN_LANG),
        'type' => 'behavior',
        'percent' => $firstYear ? $firstYear['behavior'] : '',
        'img' => 'customer.png',
        'time_type' => 'MONTH',
        'time_value' => TIME_MONTH_VALUE,
        'year_id' => $firstYear
    ],
    'comments' => [
        'title' => __('Nhận xét', TPL_DOMAIN_LANG),
        'type' => 'comments',
        'percent' => $firstYear ? $firstYear['comments'] : '',
        'img' => 'van-hanh.png',
        'time_type' => 'MONTH',
        'time_value' => TIME_MONTH_VALUE,
        'year_id' => $firstYear
    ],*/
];

$tabsGroup = [
	'finance' => [
		'title' => _TABS_Finance,
		'type' => 'Finance',
		'percent' => $firstYear ? $firstYear['finance'] : '',
		'img' => 'tai-chinh.png',
		'time_type' => 'PRECIOUS',
		'time_value' => TIME_PRECIOUS_VALUE,
		'year_id' => $firstYear
	],
	'customer' => [
		'title' => _TABS_Customer,
		'type' => 'Customer',
		'percent' => $firstYear ? $firstYear['customer'] : '',
		'img' => 'customer.png',
		'time_type' => 'PRECIOUS',
		'time_value' => TIME_PRECIOUS_VALUE,
		'year_id' => $firstYear
	],
	'operate' => [
		'title' => _TABS_Operate,
		'type' => 'Operate',
		'percent' => $firstYear ? $firstYear['operate'] : '',
		'img' => 'van-hanh.png',
		'time_type' => 'PRECIOUS',
		'time_value' => TIME_PRECIOUS_VALUE,
		'year_id' => $firstYear
	],
	'development' => [
		'title' => _TABS_Development,
		'type' => 'Development',
		'percent' => $firstYear ? $firstYear['development'] : '',
		'img' => 'phat-trien.png',
		'time_type' => 'PRECIOUS',
		'time_value' => TIME_PRECIOUS_VALUE,
		'year_id' => $firstYear
	]
];
?>

<div class="clearfix"></div>
<div class="block-detail block-result-kpi-detail">
    <div id="room-target-kpi" data-type="nv" data-content-management="">
        <ul id="" class="list-room-target-kpi clearfix nav nav-pills">
            <?php
            foreach ($tabs as $tabkey => $tabData):
                $cls = ('target_work' == $tabkey) ? 'active' : '';
                ?>
                <li class="room-target-kpi-item <?php echo $cls; ?> room-<?php echo $tabkey; ?>">
                    <a href="#room-<?php echo $tabkey; ?>" class="" title="" data-toggle="tab" data-tab-type="nv">
                        <img src="<?php echo THEME_URL; ?>/assets/images/<?php echo $tabData['img']; ?>"/>
                        <?php echo $tabData['title']; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>

        <div class="list-detail tab-content clearfix">
        <?php foreach ($tabs as $tabkey => $tabData):
            $cls = ('target_work' == $tabkey) ? 'active' : '';
            ?>
            <div class="tab-pane <?php echo $cls; ?>" id="room-<?php echo $tabkey; ?>">
                <?php /*
                <div class="list-detail">
                    <div class="top-list-detail">
                        <h3 class="title">
                            <?php echo $tabData['title']; ?>
                            <a href="javascript:;" data-year="<?php echo esc_json_attr($tabData['year_id']); ?>" data-toggle="modal"
                               data-target="#tpl-ceo-kpi-year" >
                                <span class="icon-percent" contenteditable="false" data-kpi-percent="<?php echo $tabkey; ?>"><?php echo $tabData['percent']; ?>%</span>
                                <i class="glyphicon glyphicon-pencil hidden" aria-hidden="true"></i>
                            </a>
                        </h3>
                    </div>
                </div> */?>
                <form data-approved="" action="<?php echo admin_url('admin-ajax.php?action=personal_send_approved'); ?>" class=""
                      id="form-<?php echo $tabkey; ?>" method="post" enctype="application/x-www-form-urlencoded"
                      onsubmit="return false">
                    <input value="<?= wp_create_nonce('personal_change_status'); ?>" type="hidden" name="_wpnonce">
                    <input value="<?php echo $tabData['type']; ?>" type="hidden" name="type">
                    <table class="table-list-detail table-managerment-target-kpi" cellpadding="0" cellspacing="0">
                        <thead>
                        <tr>
                            <th class="column-1 kpi-id align-center"><?php _e('Mã KPI', TPL_DOMAIN_LANG); ?></th>
                            <th class="column-2 kpi-content"><?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?></th>
                            <th class="column-3 kpi-company_plan"><?php _e('Kế hoạch <br> cấp trên', TPL_DOMAIN_LANG); ?></th>
                            <th class="column-4 kpi-department_plan"><?php _e('Kế hoạch <br> cá nhân', TPL_DOMAIN_LANG); ?></th>
                            <th class="column-6 kpi-receive align-center"><?php _e('Thời điểm <br> nhận kết quả', TPL_DOMAIN_LANG); ?></th>
                            <th class="column-8 align-center"><?php _e('Trọng số', TPL_DOMAIN_LANG); ?></th>
                            <th class="column-9 align-center"><?php _e('Tình trạng', TPL_DOMAIN_LANG); ?></th>
                            <th class="column-10 align-center"><?php _e('Thao tác', TPL_DOMAIN_LANG); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $groupDatas = [];
                        if( $tabData['type'] == 'target_work' ){
                            $groupDatas = $kpiForStaff;
                        }
                        if (!empty($groupDatas)):
                            foreach ($groupDatas as &$item):
	                            $unit = !empty($item['unit']) ? getUnit($item['unit']) : '';
                                ?>

                                <tr>
                                    <td class="column-1 kpi-id align-center">
	                                    <?php
	                                    $textSendApproved = '';
	                                    if( $item['required'] == 'no' ):
		                                    $disabled = '';
		                                    $name = 'name="kpi['.$item['id'].'][ID]"';
		                                    $valueInput = $item['id'];
		                                    $nameCheck = 'name="kpi['.$item['id'].'][status]"';
		                                    $textSendApproved = 'Chưa duyệt';
		                                    if( in_array( $item['status'], [KPI_STATUS_RESULT, KPI_STATUS_WAITING,KPI_STATUS_DONE] ) ){
			                                    $disabled = 'disabled';
			                                    $name ='';
			                                    $valueInput = '';
			                                    $nameCheck = '';
			                                    $textSendApproved = __('Đã duyệt', TPL_DOMAIN_LANG);
		                                    }elseif( $item['status'] == KPI_STATUS_PENDING ){
			                                    $disabled = 'disabled';
			                                    $name ='';
			                                    $valueInput = '';
			                                    $nameCheck = '';
			                                    $textSendApproved = "<span class='notification error'>" . __( 'Chờ duyệt', TPL_DOMAIN_LANG ) . "</span> ";
		                                    }
		                                    ?>
                                            <label class="switch">
                                                <input class="checkbox-status hidden" type="checkbox" <?= $nameCheck; ?> <?= $disabled; ?> <?php echo !($item['status'] == KPI_STATUS_DRAFT) ? 'checked="checked"' : ''; ?> value="1"/>
                                                <span class="checkbox-slider"></span>
                                            </label>
                                            <input type="hidden" <?= $name; ?> value="<?= $valueInput; ?>" />
	                                    <?php endif; ?>

                                        <?php echo $item['id'];
                                        $classNode = getColorStar( $item['create_by_node'] );
                                        if( $item['required'] == 'yes' ): ?>
                                            <i class="fa fa-star <?php esc_attr_e($classNode); ?>" aria-hidden="true"></i>
                                        <?php endif; ?>
                                    </td>
                                    <td class="column-2 kpi-content">
                                        <?php if( in_array( $item['status'], [KPI_STATUS_RESULT] ) ):
                                            # kiểm tra KPI được tạo theo quý hay theo tháng
                                            $getYear = kpi_get_year_by_id( $item['year_id'] );
                                            if( $getYear['kpi_time'] == 'quy' ):
                                                ?>
                                                <a href="javascript:;" data-action="load_personal_target_for_precious" data-method="get" data-id="<?= $item['id']; ?>" class="action-edit personal-action-edit" data-edit="" data-toggle="modal" data-target="#tpl-personal-target-precious-<?= $tabkey; ?>"><span><?php echo str_replace(str_split("\|"), "", $item['post_title']); ?></span></a>
                                                <?php
                                            else:
                                                ?>
                                            <a href="javascript:;" data-action="load_personal_target_for_month" data-method="get" data-id="<?= $item['id']; ?>" class="action-edit personal-action-edit" data-edit="" data-toggle="modal" data-target="#tpl-personal-target-<?= $tabkey; ?>"><span><?php echo str_replace(str_split("\|"), "", $item['post_title']); ?></span></a>
                                            <?php endif; ?>
                                        <?php else: ?>
                                        <span><?php echo str_replace(str_split("\|"), "", $item['post_title']); ?>
                                        <?php endif; ?>
	                                    <?php
	                                    $arrFormula = [
		                                    'html' => render_html_formula($item['formulas'], $item['formula_type']),
		                                    'title' => $item['formula_title'],
		                                    'note' => $item['note']
	                                    ]
	                                    ?>
                                        <a data-target="#formula-popup" data-toggle="modal" href="javascript:;" class="view-formula" data-formula="<?= esc_json_attr($arrFormula); ?>"><i class="fa fa-info-circle"></i></a>
                                    </td>
                                    <td class="column-3 kpi-company_plan"><?php echo $item['plan_for_year']; ?> <?= $unit; ?></td>
                                    <td class="column-4 kpi-department_plan"><?php echo $item['plan']; ?> <?= $unit; ?></td>
                                    <td class="column-6 kpi-receive align-center"><?php echo substr(kpi_format_date( $item['receive'] ), 0, 10); ?></td>
                                    <td class="column-8 kpi-percent align-center"><?php echo $item['percent']; ?>%</td>

                                    <td class="column-9 align-center">
                                        <span><?= $textSendApproved; ?></span>
                                    </td>

                                    <td class="column-10 align-center">
                                        <?php if( $item['required'] == 'no' && $item['status'] == KPI_STATUS_DRAFT ):
                                            ?>
                                            <a href="javascript:;" data-action="load_personal_target_register" data-id="<?= $item['id']; ?>" data-ajax="load_personal_target_register" data-toggle="modal" class="action-edit" data-target="#tpl-add-target-<?php echo $tabkey; ?>" data-method="get"><span class="glyphicon glyphicon-edit"></span></a>
                                            <a href="javascript:;" data-action="remove_target_register" data-id="<?= $item['id']; ?>" data-title="<?php _e('Bạn muốn xoá mục tiêu công việc?', TPL_DOMAIN_LANG); ?>" class="action-remove" data-target="#confirm-apply-kpi" data-method="post"><span class="glyphicon glyphicon-remove"></span></a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach;
                        endif;
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="9">
                                <a href="javascript:;" data-toggle="modal"
                                   data-target="#tpl-add-target-<?php echo $tabkey; ?>" data-show="show-row"><i
                                            class="fa fa-plus-circle"
                                            aria-hidden="true"></i> <?php _e('Thêm mục tiêu', TPL_DOMAIN_LANG); ?>
                                </a>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                    <div class="action-bot">
                        <button type="submit" class="btn-brb-default btn-save-and-public btn-action-item"
                                name="btn-save-public"><i class="fa fa-floppy-o"
                                                          aria-hidden="true"></i> <?php _e('Gửi xét duyệt', TPL_DOMAIN_LANG); ?>
                        </button>
                        <button type="button" class="btn-brb-default btn-print btn-action-item" name="btn-print"><i
                                    class="fa fa-print"
                                    aria-hidden="true"></i> <?php _e('Xuất báo cáo', TPL_DOMAIN_LANG); ?></button>
                    </div>
                </form>
                <div id="tpl-personal-target-<?php echo $tabkey; ?>" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
                    <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
                          action="<?php echo admin_url('admin-ajax.php?action=register_kpi_for_month') ?>" method="post">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"><?php echo $tabData['title']; ?>
                                    : Đăng ký KPI</h4>
                            </div>
                            <div class="modal-body clearfix">
                                <div class="col-md-12">
                                    <?php
                                        if( !empty( $listAsign ) ){
                                            $kpiStaff = $listAsign[0];
                                            if( empty($firtKpiForStaff) ){
                                                $firtKpiForStaff['id'] = $kpiStaff['id'];
                                                $firtKpiForStaff['chart_id'] = $kpiStaff['chart_id'];
                                                $firtKpiForStaff['plan_on_level'] = $kpiStaff['plan_on_level'];
                                                $firtKpiForStaff['unit'] = $kpiStaff['unit'];
                                                $firtKpiForStaff['type'] = $kpiStaff['type'];
                                                $firtKpiForStaff['year_id'] = $kpiStaff['year_id'];
                                                $firtKpiForStaff['plan'] = $kpiStaff['plan'];
                                                $firtKpiForStaff['receive'] = $kpiStaff['receive'];
                                                $firtKpiForStaff['percent'] = $kpiStaff['percent'];
                                            }
                                        }
                                    ?>
                                    <table class="table-list-detail">
                                        <tbody>
                                            <tr>
                                                <td class="column-1">
                                                    <?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?>
                                                </td>
                                                <td class="column-2">
                                                    <span class="post_title"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="column-1">
                                                    <?php _e('Kế hoạch cấp trên', TPL_DOMAIN_LANG); ?>
                                                </td>
                                                <td class="column-2">
                                                    <span class="plan_on_level"></span>
                                                    <span class="unit"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="column-1">
                                                    <?php _e('Kế hoạch cá nhân', TPL_DOMAIN_LANG); ?>
                                                </td>
                                                <td class="column-2">
                                                    <span class="plan"></span>
                                                    <span class="unit"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="column-1-2">
                                                    <div class="fieldset-month">
                                                        <fieldset>
                                                            <legend><?php _e( 'Mục tiêu tháng', TPL_DOMAIN_LANG); ?></legend>
                                                            <div class="content-fieldset-month">
                                                                <?php
                                                                $outputLeft = "";
                                                                $outputRight = "";
                                                                for( $month = 1; $month <= 12; $month++ ): ?>
                                                                    <?php
                                                                        $output = "<div class=\"item-form-group-for owner-register-for\">
                                                                    <div class=\"form-group input-group left-target-for\">
                                                                        <input name=\"plan_month[{$month}][month]\" value=\"{$month}\" type=\"hidden\" />
                                                                        <span class=\"input-group-addon\" for=\"plan\" id=\"plan-{$tabkey}-{$month}\">".__('Tháng ', TPL_DOMAIN_LANG)."{$month}</span>
                                                                        <input type=\"text\" name=\"plan_month[{$month}][plan]\" id=\"plan-{$tabkey}-{$month}\" class=\"plan-{$tabkey} form-control\" placeholder=\"". __('Kế hoạch', TPL_DOMAIN_LANG)."\" value=\"\" />
                                                                    </div>
                                                                    <div class=\"form-group input-group right-target-percent\">
                                                                        <span class=\"input-group-addon\" for=\"plan\" id=\"plan-{$tabkey}-{$month}\">".__('Trọng số ', TPL_DOMAIN_LANG)."</span>
                                                                        <input type=\"text\" name=\"plan_month[{$month}][percent]\" id=\"plan-{$tabkey}-{$month}\" class=\"plan-{$tabkey} form-control\" placeholder=\"". __('Trọng số ', TPL_DOMAIN_LANG)."\" value=\"\" />
                                                                    </div>
                                                                </div>";
                                                                        if( $month <= 6 ){
                                                                            $outputLeft .= $output;
                                                                        }else{
                                                                            $outputRight .= $output;
                                                                        }
                                                                    ?>
                                                                <?php endfor; ?>
                                                                <table class="table-field-list" border="0">
                                                                    <tr>
                                                                        <td class="column-left" valign="top"><?php echo $outputLeft; ?></td>
                                                                        <td class="column-right" valign="top"><?php echo $outputRight; ?></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="column-1">
                                                    <?php _e('Thời điểm nhận kết quả', TPL_DOMAIN_LANG); ?>
                                                </td>
                                                <td class="column-2">
                                                    <span class="receive"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="column-1">
                                                    <?php _e('Trọng số', TPL_DOMAIN_LANG); ?>
                                                </td>
                                                <td class="column-2">
                                                    <span class="percent"></span>%
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>


                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" class="_wpnonce" name="_wpnonce" value="">
                                <input type="hidden" class="parent" name="parent" value="">
                                <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <button type="submit" class="btn btn-primaryy">Gửi duyệt đăng ký tháng</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="tpl-personal-target-precious-<?php echo $tabkey; ?>" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
                    <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
                          action="<?php echo admin_url('admin-ajax.php?action=register_kpi_for_precious') ?>" method="post">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"><?php echo $tabData['title']; ?>
                                    : Đăng ký KPI</h4>
                            </div>
                            <div class="modal-body clearfix">
                                <div class="col-md-12">
						            <?php
						            if( !empty( $listAsign ) ){
							            $kpiStaff = $listAsign[0];
							            if( empty($firtKpiForStaff) ){
								            $firtKpiForStaff['id'] = $kpiStaff['id'];
								            $firtKpiForStaff['chart_id'] = $kpiStaff['chart_id'];
								            $firtKpiForStaff['plan_on_level'] = $kpiStaff['plan_on_level'];
								            $firtKpiForStaff['unit'] = $kpiStaff['unit'];
								            $firtKpiForStaff['type'] = $kpiStaff['type'];
								            $firtKpiForStaff['year_id'] = $kpiStaff['year_id'];
								            $firtKpiForStaff['plan'] = $kpiStaff['plan'];
								            $firtKpiForStaff['receive'] = $kpiStaff['receive'];
								            $firtKpiForStaff['percent'] = $kpiStaff['percent'];
							            }
						            }
						            ?>
                                    <table class="table-list-detail">
                                        <tbody>
                                        <tr>
                                            <td class="column-1">
									            <?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?>
                                            </td>
                                            <td class="column-2">
                                                <span class="post_title"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="column-1">
									            <?php _e('Kế hoạch cấp trên', TPL_DOMAIN_LANG); ?>
                                            </td>
                                            <td class="column-2">
                                                <span class="plan_on_level"></span>
                                                <span class="unit"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="column-1">
									            <?php _e('Kế hoạch cá nhân', TPL_DOMAIN_LANG); ?>
                                            </td>
                                            <td class="column-2">
                                                <span class="plan"></span>
                                                <span class="unit"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="column-1-2">
                                                <div class="fieldset-precious">
                                                    <fieldset>
                                                        <legend><?php _e( 'Mục tiêu quý', TPL_DOMAIN_LANG); ?></legend>
                                                        <div class="content-fieldset-precious">

												            <?php
                                                            $outputLeft = "";
                                                            $outputRight = "";
												            for( $precious = 1; $precious <= 4; $precious++ ): ?>
                                                                <?php
                                                                 $output = "<div class=\"item-form-group-for owner-register-for\">
                                                                    <div class=\"form-group input-group left-target-for\">
                                                                        <input name=\"plan_precious[{$precious}][precious]\" value=\"{$precious}\" type=\"hidden\" />
                                                                        <span class=\"input-group-addon\" for=\"plan\" id=\"plan-{$tabkey}-{$precious}\">".__('Quý', TPL_DOMAIN_LANG)." {$precious}</span>
                                                                        <input type=\"text\" name=\"plan_precious[{$precious}][plan]\" id=\"plan-{$tabkey}-{$precious}\" class=\"plan-{$tabkey} form-control\" placeholder=\"".__('Kế hoạch', TPL_DOMAIN_LANG)."\" value=\"\" />
                                                                    </div>
                                                                    <div class=\"form-group input-group right-target-percent\">
                                                                        <span class=\"input-group-addon\" for=\"plan\" id=\"plan-{$tabkey}-{$precious}\">".__('Trọng số ', TPL_DOMAIN_LANG)."</span>
                                                                        <input type=\"text\" name=\"plan_precious[{$precious}][percent]\" id=\"plan-{$tabkey}-{$precious}\" class=\"plan-{$tabkey} form-control\" placeholder=\"".__('Trọng số ', TPL_DOMAIN_LANG)."\" value=\"\" />
                                                                    </div>
                                                                </div>";
                                                                 if( $precious <= 2 ){
                                                                     $outputLeft .= $output;
                                                                 }else{
                                                                     $outputRight .= $output;
                                                                 }
                                                                ?>
												            <?php endfor; ?>
                                                            <table class="table-field-list" border="0">
                                                                <tr>
                                                                    <td class="column-left" valign="top"><?php echo $outputLeft; ?></td>
                                                                    <td class="column-right" valign="top"><?php echo $outputRight; ?></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="column-1">
									            <?php _e('Thời điểm nhận kết quả', TPL_DOMAIN_LANG); ?>
                                            </td>
                                            <td class="column-2">
                                                <span class="receive"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="column-1">
									            <?php _e('Trọng số', TPL_DOMAIN_LANG); ?>
                                            </td>
                                            <td class="column-2">
                                                <span class="percent"></span>%
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>


                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" class="_wpnonce" name="_wpnonce" value="">
                                <input type="hidden" class="parent" name="parent" value="">
                                <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <button type="submit" class="btn btn-primaryy">Gửi duyệt đăng ký quý</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="tpl-add-target-<?php echo $tabkey; ?>" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
                    <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
                          action="<?php echo admin_url('admin-ajax.php?action=add_personal_target') ?>" method="post">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"><?php echo $tabData['title']; ?>
                                    : Thêm KPI</h4>
                            </div>

                            <div class="modal-body clearfix">
                                <div class="col-md-12">
                                        <div class="form-group form-plans">
                                            <div class="form-group input-group input-group-select">
                                                <span class="input-group-addon" for="post_title" id="post_title-<?php echo $tabkey; ?>"><?php _e('Chọn KPI', TPL_DOMAIN_LANG); ?></span>
                                                <select data-none-results-text="<?php _e( 'Kết quả tìm kiếm {0}', TPL_DOMAIN_LANG); ?>" data-none-selected-text="<?php _e('Chọn KPI', TPL_DOMAIN_LANG); ?>" data-loading=".form-group.input-group.input-group-select" class="selectpicker cat-bank" name="cat" data-live-search="true">
                                                    <?php
                                                            echo "<option value='{$orgchartID}'>{$orgchart->name}</option>";
                                                    ?>
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group form-plans">
                                            <div class="form-group input-group input-group-select">
                                            <span class="input-group-addon" for="post_title" id="post_title-<?php echo $tabkey; ?>"><?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?></span>
                                            <select data-none-results-text="<?php _e( 'Kết quả tìm kiếm {0}', TPL_DOMAIN_LANG); ?>" data-none-selected-text="<?php _e('Chọn mục tiêu', TPL_DOMAIN_LANG); ?>" data-loading=".form-group.input-group.input-group-select" class="selectpicker post_title post_title-<?php echo $tabkey; ?>" name="bank" data-live-search="true">
                                            </select>
                                        </div>
                                        </div>
                                        <div class="content-offer-bank">
                                            <fieldset class="offer-bank">
                                                <legend>Đề xuất mục tiêu KPI</legend>
                                                <div class="form-group input-group">
                                                    <span class="input-group-addon" for="post_title_add" id="post_title_add"><?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?></span>
                                                    <input class="plan form-control" type="text" name="offer[post_title]" placeholder="<?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?>" value="" />
                                                </div>
                                                <div class="form-group form-plans">
                                                    <div class="form-group input-group input-group-select">
                                                        <span class="input-group-addon" for="choose-formula" id="choose-formula-<?php echo $tabkey; ?>"><?php _e('Chọn phương pháp đo', TPL_DOMAIN_LANG); ?></span>
                                                        <select data-none-results-text="<?php _e( 'Kết quả tìm kiếm {0}', TPL_DOMAIN_LANG); ?>" data-none-selected-text="<?php _e('Chọn phương pháp đo', TPL_DOMAIN_LANG); ?>" data-loading=".form-group.input-group.input-group-select" class="selectpicker choose-formula width657 choose-formula-<?php echo $tabkey; ?>" name="offer[formula_id]" data-live-search="true">
                                                            <?php
                                                                $listFormulas = get_list_formula();
                                                            if( !empty( $listFormulas ) && !is_wp_error( $listFormulas ) ) {
	                                                            foreach ( $listFormulas as $key => $formula ){
		                                                            $formulaTitle = $formula->title;
		                                                            $formulaID = $formula->ID;
		                                                            echo "<option value='".esc_attr($formulaID)."'>". esc_attr( $formulaTitle ) . "</option>";
	                                                            }
                                                            }

                                                            ?>
                                                            <option value="0">Đề xuất phương pháp đo khác</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group input-group offer-formula-custom">
                                                        <div class="show-formula-des">
                                                            <div class="title">
                                                                Mô tả công thức:
                                                            </div>
                                                            <div class="content"></div>
                                                        </div>
                                                        <div class="show-offer-formula">
                                                            <span class="input-group-addon" for="description-formula" id="description-formula"><?php _e('Đề xuất công thức', TPL_DOMAIN_LANG); ?></span>
                                                            <textarea class="plan form-control" type="text" name="offer[post_content]" placeholder="<?php _e('Mô tả công thức', TPL_DOMAIN_LANG); ?>" value=""></textarea>
                                                        </div>
                                                    </div>
                                            </fieldset>
                                        </div>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon" for="plan" id="plan"><?php _e('Kế hoạch cá nhân', TPL_DOMAIN_LANG); ?></span>
                                            <input class="plan form-control" type="text" name="plan" placeholder="<?php _e('Kế hoạch cá nhân', TPL_DOMAIN_LANG); ?>" value="" />
                                        </div>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon" for="unit" id="unit"><?php _e('ĐVT', TPL_DOMAIN_LANG); ?></span>
                                            <select data-none-selected-text="Đơn vị tính" id="unit-<?php echo $tabkey; ?>" class="select-unit selectpicker" name="unit">
		                                        <?php
		                                        $unitArr = getUnit();
		                                        foreach( $unitArr as $kUnit => $vUnit ) {
			                                        echo sprintf( "<option value='%s'>%s</option>", $kUnit, $vUnit );
		                                        }
		                                        ?>
                                            </select>
                                        </div>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon" for="receive" id="receive"><?php _e('Thời điểm nhận kết quả', TPL_DOMAIN_LANG); ?></span>
                                            <input type="text" name="receive" class="form-control"
                                                   placeholder="Thời điểm nhận kết quả" data-lang="vi" data-format="<?php echo KPI_FORMAT_DATE; ?>"
                                                   data-min-date="01-01-<?= TIME_YEAR_VALUE; ?>"
                                                <?php /* data-max-date="" */ ?>
                                                   data-group-date=".group-date" data-timepicker="false"
                                                   data-btn-date=".input-group-addon.date-btn"
                                                   data-ctrl-date="" aria-describedby="receive">
                                        </div>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon" for="percent" id="percent"><?php _e('Trọng số', TPL_DOMAIN_LANG); ?></span>
                                            <input type="text" name="percent" class="percent form-control" placeholder="<?php _e('Trọng số', TPL_DOMAIN_LANG); ?>" value="" />
                                        </div>
                                        <div class="form-group form-plans">
                                            <div class="form-group input-group input-group-select">
                                                <span class="input-group-addon" for="kpi-time" id="kpi-time-<?php echo $tabkey; ?>"><?php _e('KPI theo', TPL_DOMAIN_LANG); ?></span>
                                                <select data-none-results-text="<?php _e( 'Kết quả tìm kiếm {0}', TPL_DOMAIN_LANG); ?>" data-none-selected-text="<?php _e('KPI Theo', TPL_DOMAIN_LANG); ?>" data-loading=".form-group.input-group.input-group-select" class="selectpicker kpi-for kpi-for-<?php echo $tabkey; ?>" name="kpi_time" data-live-search="true">
                                                    <option value="quy">Quý</option>
                                                    <option value="thang">Tháng</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" name="_wpnonce" value="<?php echo $add_target; ?>">
                                    <input type="hidden" name="time_year" value="<?php echo $_GET['nam']; ?>">
                                    <input type="hidden" name="id" value="">
                                    <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <button type="submit" class="btn btn-primaryy">Lưu</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    </div>
</div>
