<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 20/01/2018
 * Time: 15:53
 */
require_once THEME_DIR . '/inc/lib-formulas.php';

$typeFormulas = getTypeFormulas();
$unitFormulas = getUnitFormulas();

get_template_part('contents/content', 'left');

?>
<div class="col-sm-9 kpi-bank-right page-formulas">
	<div class="content-top">
		<div class="content-title content-kpi-bank col-sm-9">
			<h1 class="entry-title"><?php echo $title; ?></h1>
		</div>
		<?php /*
		<div class="search-right col-sm-3">
			<form class="frm-search-kpi-bank" action="" method="get">
				<div class="kpi-bank-search">
					<input type="text" class="txt-default keyword" name="keyword" placeholder="Search">
					<button class="btn-kpi-search" name="kpi-search">
						<i class="fa fa-search" aria-hidden="true"></i>
					</button>
				</div>
			</form>
		</div>
        */ ?>
	</div>
	<div class="clearfix"></div>
	<div class="main-content-kpi">
		<div class="grid-view grid-view-kpi brainmark-repeater" data-idx="-1">
			<table class="table-kpi table-formulas brb-custom-table brainmark-table">
				<thead class="row-thead-title-parent">
				<tr>
					<th class="row-title title-numerical-order"><?php _e('STT', TPL_DOMAIN_LANG); ?></th>
					<th class="row-title title-code-formula"><?php _e('Mã công thức', TPL_DOMAIN_LANG); ?></th>
					<th class="row-title title-target-content"><?php _e('Tiêu đề công thức', TPL_DOMAIN_LANG); ?></th>
					<th class="row-title title-type-formula"><?php _e('Loại tính toán', TPL_DOMAIN_LANG); ?></th>
					<th class="row-title title-note-formula"><?php _e('Ghi chú', TPL_DOMAIN_LANG); ?></th>
					<th class="row-title title-action"><?php _e('', TPL_DOMAIN_LANG); ?></th>
				</tr>
				</thead>
				<tbody>
                    <?php
                        $formulars = get_list_formula();
                        if( !empty( $formulars ) ):
                            $i = 0;
                            foreach( $formulars as $key => $fm ):
                                $i++;
                        ?>
					    <tr>
						<td class="column-1"><?php echo $i; ?></td>
						<td class="column-2"><?php echo $fm->ID; ?></td>
						<td class="column-3"><?php echo $fm->title; ?></td>
						<td class="column-5">
                            <?php
                            if( !empty( $fm->type ) && array_key_exists( $fm->type, $typeFormulas ) ){
                                echo $typeFormulas[$fm->type]['name'];
                            }
                            ?>
                        </td>
						<td class="column-6"><?php echo $fm->note; ?></td>
						<td class="column-7">
							<a href="javascript:;" data-id="<?php echo $fm->ID; ?>" data-action="load_formula" data-method="get" data-toggle="modal" data-target="#popup-formula" class="edit" title="<?php _e('Chỉnh sửa', TPL_DOMAIN_LANG);?>"> <span class="glyphicon glyphicon-pencil"></span> <?php _e('Chỉnh sửa', TPL_DOMAIN_LANG);?></a>
							<a href="javascript:;" data-nonce="<?= wp_create_nonce('delete_formula'); ?>" data-id="<?php echo $fm->ID; ?>" data-action="del_formula" data-method="post" class="delete" title="<?php _e('Xóa', TPL_DOMAIN_LANG);?>"><span class="glyphicon glyphicon-trash"></span> <?php _e('Xóa', TPL_DOMAIN_LANG);?></a>
						</td>
					</tr>
                    <?php
                            endforeach;
                    endif; ?>
				</tbody>
				<tfoot class="row-tfoot-group">
				<tr>
					<th colspan="8" class="action-add-group">
						<a href="javascript:;" data-toggle="modal" data-keyboard="false" data-backdrop="static" data-target="#popup-formula" data-show="show-row"><?php _e('Thêm công thức', TPL_DOMAIN_LANG); ?><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
					</th>
				</tr>
				</tfoot>

			</table>
		</div>
	</div>
    <div id="popup-formula" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php _e('Ngân hàng KPI', TPL_DOMAIN_LANG); ?></h4>
                </div>
                <div class="modal-body">
                    <form data-form-formula="" enctype="application/x-www-form-urlencoded" action="<?= admin_url('admin-ajax.php'); ?>" method="post" class="frm-formula">
												<input type="hidden" name="formula[ID]" class="formula_ID" value="" />

												<table class="table-input-formula">
                            <tbody>
                                <!--tr class="code-formula">
                                    <td class="column-1">
                                        <label for=""><?php //_e('Mã công thức', TPL_DOMAIN_LANG); ?>:</label>
                                    </td>
                                    <td colspan="3" class="column-2">
                                        <span class="code-formula-hidden" id="formula_ID"></span>
                                    </td>
                                </tr-->
                                <tr>
                                    <td class="column-1" style="padding-bottom: 10px;"><?php _e('KH: Kế hoạch', TPL_DOMAIN_LANG); ?></td>
                                    <td colspan="3" class="column-2" style="text-align: left;padding-left: 16px;padding-bottom: 10px;">
                                        <?php _e('KQ: Kết quả thực tế', TPL_DOMAIN_LANG); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="column-1" valign="top">
                                        <label for="formula-title" style="padding-top: 5px;"><?php _e('Tiêu đề công thức', TPL_DOMAIN_LANG); ?>:</label>
                                    </td>
                                    <td colspan="3" class="column-2">
                                        <input type="text" name="formula[title]" id="formula-title" placeholder="<?php _e('Tiêu đề công thức', TPL_DOMAIN_LANG); ?>" style="margin-bottom: 10px;"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="column-1">
                                        <label><?php _e('Ghi chú', TPL_DOMAIN_LANG); ?></label>
                                    </td>
                                    <td colspan="3" class="column-2">
                                        <textarea name="formula[note]" id="formula-note" class="input-text"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="column-1">
                                        <div class="group-formula" style="margin-top: 15px;border-top: 1px dotted #ddd;padding-top: 10px;">
                                            <div class="group-formula-item">
                                                <label class="label-radio">
                                                    <?php _e('On/off', TPL_DOMAIN_LANG); ?>
                                                    <input type="radio" checked="checked" name="formula[type]" id="radio_on_off" value="on_off">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="group-formula-item">
                                                <label class="label-radio">
                                                    <?php _e('Dao động', TPL_DOMAIN_LANG); ?>
                                                    <input type="radio" name="formula[type]" id="radio_oscillate" value="oscillate">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="group-formula-item">
                                                <label class="label-radio">
                                                    <?php _e('Thêm max KQ KPI', TPL_DOMAIN_LANG); ?>
                                                    <input type="radio" name="formula[type]" id="radio_add_max_kq" value="add_max_kq">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="group-formula-item">
                                                <label class="label-radio">
                                                    <?php _e('Thêm trong khoảng', TPL_DOMAIN_LANG); ?>
                                                    <input type="radio" name="formula[type]" id="radio_add_between" value="add_between">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                            <td colspan="4" class="column-1 formula-template">
                                <table id="table_on_off" class="table-group-detail">
                                    <thead>
                                    <tr>
                                        <th class="column-1"><?php _e('Điều kiện', TPL_DOMAIN_LANG); ?></th>
                                        <th class="column-2"></th>
                                        <th class="column-3"><?php _e('Kết quả KPI (%)', TPL_DOMAIN_LANG); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody class="tbody-main">
                                    <tr data-idx="0">
                                        <td class="column-1">
                                            <select class="width_full selectpicker"  name="meta_input[on_off][0][condition]">
                                                <?php
                                                $measurementFormulas = getMeasurementFormulas();
                                                $measurementFormulas['!='] = __('KH # KQ', TPL_DOMAIN_LANG);
                                                $measurementFormulas['0'] = __('----------', TPL_DOMAIN_LANG);
                                                if( !empty($measurementFormulas) ):
                                                    foreach ( $measurementFormulas as $key => $mf ){
                                                        echo "<option value='".$key."'>".$mf."</option>";
                                                    }
                                                endif;
                                                ?>
                                            </select>
                                        </td>
                                        <td class="column-2 align-center"> = </td>
                                        <td class="column-3">
                                            <input type="text" name="meta_input[on_off][0][value]" value="" class="input-text value-result-of-evaluation width_full" />
                                            <a href="javascript:;" class="remove-repeater-mf" data-event="remove-condition-mf"><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                    <tbody class="clone-mf display-none">
                                    <tr class="" data-idx="IDX">
                                        <td class="column-1">
                                            <select class="width_full selectpicker"  name="meta_input[on_off][IDX][condition]">
                                                <?php
                                                $measurementFormulas = getMeasurementFormulas();
                                                $measurementFormulas['!='] = __('KH # KQ', TPL_DOMAIN_LANG);
                                                $measurementFormulas['0'] = __('----------', TPL_DOMAIN_LANG);
                                                if( !empty($measurementFormulas) ):
                                                    foreach ( $measurementFormulas as $key => $mf ){
                                                        echo "<option value='".$key."'>".$mf."</option>";
                                                    }
                                                endif;
                                                ?>
                                            </select>
                                        </td>
                                        <td class="column-2 align-center"> = </td>
                                        <td class="column-3">
                                            <input type="text" name="meta_input[on_off][IDX][value]" value="" class="input-text value-result-of-evaluation width_full" />
                                            <a href="javascript:;" class="remove-repeater-mf" data-event="remove-condition-mf"><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="3">
                                            <a href="javascript:;" class="" data-event="repeat-condition-mf" data-name-clone="table_on_off"><i class="fa fa-plus-circle" aria-hidden="true"></i> <?php  _e('Thêm điều kiện'); ?></a>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                                <table id="table_oscillate" class="display-none table-group-detail">
                                    <thead>
                                    <tr>
                                        <th class="column-1"><?php _e('Điều kiện', TPL_DOMAIN_LANG); ?></th>
                                        <th class="column-2"></th>
                                        <th colspan="" class="column-3"><?php _e('Dao động', TPL_DOMAIN_LANG); ?></th>
                                        <th class="column-4"></th>
                                        <th class="column-5"><?php _e('Kết quả KPI (%)', TPL_DOMAIN_LANG); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody class="tbody-main">
                                    <tr data-idx="0">
                                        <td class="column-1">
                                            <select class="width_full selectpicker"  name="meta_input[oscillate][0][condition]">
                                                <?php
                                                $measurementFormulas = getMeasurementFormulas();
                                                if( !empty($measurementFormulas) ):
                                                    foreach ( $measurementFormulas as $key => $mf ){
                                                        echo "<option value='".$key."'>".$mf."</option>";
                                                    }
                                                endif;
                                                ?>
                                            </select>
                                        </td>
                                        <td class="column-2 align-center"> + </td>
                                        <td class="column-3">
                                            <input type="text" name="meta_input[oscillate][0][oscillate]" value="" class="input-text value-result-of-evaluation width_full" />
                                        </td>
                                        <td class="column-4 align-center"> = </td>
                                        <td class="column-5">
                                            <input type="text" name="meta_input[oscillate][0][value]" value="" class="input-text value-result-of-evaluation width_full" />
                                            <a href="javascript:;" class="remove-repeater-mf" data-event="remove-condition-mf"><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                    <tbody class="clone-mf display-none">
                                    <tr class="" data-idx="IDX">
                                        <td class="column-1">
                                            <select class="width_full selectpicker" name="meta_input[oscillate][IDX][condition]">
                                                <?php
                                                $measurementFormulas = getMeasurementFormulas();
                                                if( !empty($measurementFormulas) ):
                                                    foreach ( $measurementFormulas as $key => $mf ){
                                                        echo "<option value='".$key."'>".$mf."</option>";
                                                    }
                                                endif;
                                                ?>
                                            </select>
                                        </td>
                                        <td class="column-2 align-center"> + </td>
                                        <td class="column-3">
                                            <input type="text" name="meta_input[oscillate][IDX][oscillate]" value="" class="input-text value-result-of-evaluation width_full" />
                                        </td>
                                        <td class="column-4 align-center"> = </td>
                                        <td class="column-5">
                                            <input type="text" name="meta_input[oscillate][IDX][value]" value="" class="input-text value-result-of-evaluation width_full" />
                                            <a href="javascript:;" class="remove-repeater-mf" data-event="remove-condition-mf"><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="5">
                                            <a href="javascript:;" class="" data-event="repeat-condition-mf" data-name-clone="table_oscillate"><i class="fa fa-plus-circle" aria-hidden="true"></i> <?php  _e('Thêm điều kiện'); ?></a>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                                <table id="table_add_max_kq" class="display-none table-group-detail">
                                    <thead>
                                    <tr>
                                        <th class="column-1"><?php _e('Điều kiện', TPL_DOMAIN_LANG); ?></th>
                                        <th class="column-2"></th>
                                        <th class="column-3"><?php _e('Kết quả KPI (%)', TPL_DOMAIN_LANG); ?></th>
                                        <th class="column-4"></th>
                                    </tr>
                                    </thead>
                                    <tbody class="tbody-main">
                                    <tr data-idx="0">
                                        <td class="column-1">
                                            <select class="width_full selectpicker" name="meta_input[add_max_kq][0][condition]">
                                                <?php
                                                $measurementFormulas = getMeasurementFormulas();
                                                if( !empty($measurementFormulas) ):
                                                    foreach ( $measurementFormulas as $key => $mf ){
                                                        echo "<option value='".$key."'>".$mf."</option>";
                                                    }
                                                endif;
                                                ?>
                                            </select>
                                        </td>
                                        <td class="column-2 align-center"> = </td>
                                        <td class="column-3">
                                            <input type="text" name="meta_input[add_max_kq][0][value]" value="" class="input-text value-result-of-evaluation width_full" />
                                        </td>
                                        <td class="column-4">
                                            <label>(<?php _e('Max KPI', TPL_DOMAIN_LANG); ?> = <input type="text" name="meta_input[add_max_kq][0][max]" value="" class="input-text value-result-of-evaluation width_full" /> )</label>
                                            <a href="javascript:;" class="remove-repeater-mf" data-event="remove-condition-mf"><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                    <tbody class="clone-mf display-none">
                                    <tr class="" data-idx="IDX">
                                        <td class="column-1">
                                            <select class="width_full selectpicker" name="meta_input[add_max_kq][IDX][condition]">
                                                <?php
                                                $measurementFormulas = getMeasurementFormulas();
                                                if( !empty($measurementFormulas) ):
                                                    foreach ( $measurementFormulas as $key => $mf ){
                                                        echo "<option value='".$key."'>".$mf."</option>";
                                                    }
                                                endif;
                                                ?>
                                            </select>
                                        </td>
                                        <td class="column-2 align-center"> = </td>
                                        <td class="column-3">
                                            <input type="text" name="meta_input[add_max_kq][IDX][value]" value="" class="input-text value-result-of-evaluation width_full" />
                                        </td>
                                        <td class="column-4">
                                            <label>(<?php _e('Max KPI', TPL_DOMAIN_LANG); ?> = <input type="text" name="meta_input[add_max_kq][IDX][max]" value="" class="input-text value-result-of-evaluation width_full" /> )</label>
                                            <a href="javascript:;" class="remove-repeater-mf" data-event="remove-condition-mf"><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="4">
                                            <a href="javascript:;" class="" data-event="repeat-condition-mf" data-name-clone="table_add_max_kq"><i class="fa fa-plus-circle" aria-hidden="true"></i> <?php  _e('Thêm điều kiện'); ?></a>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                                <table id="table_add_between" class="display-none table-group-detail">
                                    <thead>
                                    <tr>
                                        <th class="column-1 align-center"><?php _e('KQ Min (X)', TPL_DOMAIN_LANG); ?></th>
                                        <th class="column-2 align-center"><?php _e('Điều kiện', TPL_DOMAIN_LANG); ?></th>
                                        <th class="column-3 align-center"><?php _e('KQ Max (Y)', TPL_DOMAIN_LANG); ?></th>
                                        <th class="column-4 align-center"><?php _e('Kết quả KPI (%)', TPL_DOMAIN_LANG); ?></th>
                                    </tr>
                                    </thead>
                                    <?php
                                    $argsResultMINMAX = [
                                        '0'  => '',
                                        '<' => '<',
                                        '>' => '>',
                                        '=' => '=',
                                        '<=' => '<=',
                                        '>=' => '>=',
                                    ];
                                    ?>
                                    <tbody class="tbody-main">
                                    <tr data-idx="0">
                                        <td class="column-1 align-center">
                                            <input type="text" name="meta_input[add_between][0][kq_min]" value="" class="input-text value-result-of-evaluation width_full" />
                                        </td>
                                        <td class="column-2 align-center">
                                            <select class="width_full selectpicker" data-none-selected-text="" name="meta_input[add_between][0][condition_1]">
                                                <?php

                                                if( !empty($argsResultMINMAX) ):
                                                    foreach ( $argsResultMINMAX as $key => $mf ){
                                                        echo "<option value='".$key."'>".$mf."</option>";
                                                    }
                                                endif;
                                                ?>
                                            </select>
                                            <span class="condition-results
">KQ</span>
                                            <select class="width_full selectpicker" data-none-selected-text="" name="meta_input[add_between][0][condition_2]">
                                                <?php
                                                if( !empty($argsResultMINMAX) ):
                                                    foreach ( $argsResultMINMAX as $key => $mf ){
                                                        echo "<option value='".$key."'>".$mf."</option>";
                                                    }
                                                endif;
                                                ?>
                                            </select>
                                        </td>
                                        <td class="column-3 align-center">
                                            <input type="text" name="meta_input[add_between][0][kq_max]" value="" class="input-text value-result-of-evaluation width_full" />
                                        </td>
                                        <td class="column-4 align-center">
                                            <input type="text" name="meta_input[add_between][0][value]" value="" class="input-text value-result-of-evaluation width_full" />
                                            <a href="javascript:;" class="remove-repeater-mf" data-event="remove-condition-mf"><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                    <tbody class="clone-mf display-none">
                                    <tr class="" data-idx="IDX">
                                        <td class="column-1 align-center">
                                            <input type="text" name="meta_input[add_between][IDX][kq_min]" value="" class="input-text value-result-of-evaluation width_full" />
                                        </td>
                                        <td class="column-2 align-center">

                                            <select class="width_full selectpicker" data-none-selected-text="" name="meta_input[add_between][IDX][condition_1]">
                                                <?php
                                                if( !empty($argsResultMINMAX) ):
                                                    foreach ( $argsResultMINMAX as $key => $mf ){
                                                        echo "<option value='".$key."'>".$mf."</option>";
                                                    }
                                                endif;
                                                ?>
                                            </select>
                                            <span class="condition-results
">KQ</span>
                                            <select class="width_full selectpicker" data-none-selected-text="" name="meta_input[add_between][IDX][condition_2]">
                                                <?php
                                                if( !empty($argsResultMINMAX) ):
                                                    foreach ( $argsResultMINMAX as $key => $mf ){
                                                        echo "<option value='".$key."'>".$mf."</option>";
                                                    }
                                                endif;
                                                ?>
                                            </select>
                                        </td>
                                        <td class="column-3 align-center">
                                            <input type="text" name="meta_input[add_between][IDX][kq_max]" value="" class="input-text value-result-of-evaluation width_full" />
                                        </td>
                                        <td class="column-4 align-center">
                                            <input type="text" name="meta_input[add_between][IDX][value]" value="" class="input-text value-result-of-evaluation width_full" />
                                            <a href="javascript:;" class="remove-repeater-mf" data-event="remove-condition-mf"><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="4">
                                            <a href="javascript:;" class="" data-event="repeat-condition-mf" data-name-clone="table_add_between"><i class="fa fa-plus-circle" aria-hidden="true"></i> <?php  _e('Thêm điều kiện'); ?></a>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </td>
                        </tr>
                            </tbody>
                            <tfoot>
                        <tr>
                            <td colspan="4">
                                <div class="hidden">
                                    <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce('create_edit_formulas'); ?>" />
                                    <input type="hidden" value="create_edit_formula" name="action">
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                                <div class="action-bot">
                                    <button type="submit" class="btn-brb-default btn-save" id="save-formula" name="btn-save"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php  _e('Lưu', TPL_DOMAIN_LANG); ?></button>
                                    <button type="button" data-dismiss="modal" class="btn-brb-default btn-cancel close-popup"> <?php _e('Đóng', TPL_DOMAIN_LANG); ?></button>
                                </div>
                            </td>
                        </tr>
                        </tfoot>
                        </table>
                    </form>
                </div>
            </div>

        </div>

    </div>

    <!-- Modal -->
    <div id="dialog-formula" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><?php _e('Bạn muốn xóa công thức', TPL_DOMAIN_LANG); ?></h4>
                </div>
                <div class="modal-body">
                    <button type="button" class="btn-brb-default btn-yes btn-primary"><?php _e('Đồng ý', TPL_DOMAIN_LANG); ?></button>
                    <button type="button" data-dismiss="modal" class="btn-brb-default btn-cancel btn-no"><?php _e('Hủy', TPL_DOMAIN_LANG); ?></button>
                </div>

            </div>

        </div>
    </div>
</div>
