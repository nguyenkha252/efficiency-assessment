<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 31/12/2017
 * Time: 23:20
 */

$bgColor = getColor4Column();
$dataSet = [];
if( isset( $_GET['uid'] ) ){
	$getUser = get_user_by("ID", $_GET['uid']);
	$orgchartUser = user_load_orgchart( $getUser );
	$firstYear = year_get_year_by_chart_id( $orgchartUser->id, TIME_YEAR_VALUE );
}else {
	$firstYear = $GLOBALS['firstYear'];
}
if( !empty( $firstYear ) ) {
    extract($firstYear);
    $data = [(int)$finance, (int)$customer, (int)$operate, (int)$development];
    $dataSet = [
        'labels' => [
            __('Tài chính', TPL_DOMAIN_LANG),
            __('Khách hàng', TPL_DOMAIN_LANG),
            __('Vận hành', TPL_DOMAIN_LANG),
            __('Phát triển', TPL_DOMAIN_LANG),
        ],
        'datasets' => [[
            'data' => $data,
            'backgroundColor' => array_values($bgColor),
        ]]
    ];
}
?>
<div class="content-register-kpi-chart ">
    <div class="register-kpi-chart room-register-chart">
        <canvas class="data-report" width="300" height="300" data-type="pie" data-datasets="<?php echo esc_json_attr($dataSet); ?>"></canvas>
    </div>
</div>
