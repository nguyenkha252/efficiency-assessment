<?php

require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-formulas.php';
#require_once THEME_DIR . '/inc/lib-kpi_bank.php';
get_template_part( 'contents/content', 'left' );
if( !isset($_GET['cat']) ){
	$title = __('Vui lòng chọn loại mục tiêu ngân hàng KPI', TPL_DOMAIN_LANG);
	?>
	<div class="col-sm-9 kpi-bank-right">
		<div class="content-top">
			<div class="content-title content-kpi-bank col-md-9">
				<h1 class="entry-title"><?php echo $title; ?></h1>
			</div>
		</div>
	</div>
<?php
}else {
	$matchTieuChi = preg_match( '#(tieu-chi-nang-luc)#i', $_GET['cat'] );
	$matchThaiDo  = preg_match( '#(thai-do-hanh-vi)#i', $_GET['cat'] );
	if ( ! empty( $matchTieuChi ) ) {
		get_template_part( 'contents/bank/tieu-chi', 'nang-luc' );
	} elseif ( ! empty( $matchThaiDo ) ) {
		get_template_part( 'contents/bank/thai-do', 'hanh-vi' );
	} else {
		get_template_part( 'contents/bank/he-thong', 'chuc-danh' );
	}
}