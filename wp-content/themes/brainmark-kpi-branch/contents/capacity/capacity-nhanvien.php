<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 06/02/2018
 * Time: 00:24
 */

require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-orgchart.php';
require_once THEME_DIR . '/inc/lib-formulas.php';

global $wpdb, $capacity_nx;

$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
$tableKpiCP = "{$prefix}kpis_capacity";
if( isset( $_GET['uid'] ) && !empty( $_GET['uid'] ) ){
	$userID = (int)wp_slash($_GET['uid']);
	$user = kpi_get_user_by_id($userID);
}else{
	$user = wp_get_current_user();
}
$orgchart = user_load_orgchart($user);
$currentOrgID = $orgchart ? $orgchart->id : '';
$member_role = $orgchart ? $orgchart->role : '';
$userID = $user->ID;
$strUsers = '';
if( $member_role == 'nhanvien' ){
	$search_onlevel = user_get_phong_ban_by_user( $currentOrgID );
	if( !empty( $search_onlevel ) ){
		$users = kpi_get_user_info(['id' => 0, 'single' => false, 'exclude' => []]);
		if( !empty( $users ) && !empty( $users['items'] ) ){
			foreach ( $users['items'] as $key => $value ){
				if( $value['parent'] == $search_onlevel->id || $value['orgchart_id'] == $search_onlevel->id ) {
					$arrUsers[] = $value['id'];
				}
			}
			$strUsers = implode(", ", $arrUsers);
		}
    }
}
$orgChartParent = 0;
$firstYear = kpi_get_first_year_capacity( TIME_YEAR_VALUE );
$yearID = !empty( $firstYear ) ? $firstYear['id'] : 0;
$kpiForI = [];
$kpiForIII = [];
$tabs = ["'Finance'", "'Customer'", "'Operate'", "'Development'"];
$strTabs = implode( ", ", $tabs );

if( in_array( $member_role, ['phongban', 'bgd', 'nhanvien'] ) ):
	# lấy % kế hoạch thực hiện được của công ty
	#$groupDatas = kpi_get_group_kpi_by_year_cty( $strTabs, TIME_YEAR_VALUE, 'no' );
    #$groupDatas = kpi_get_group_kpi_by_year_orgchart_for_year_by_lower_grade_nv($strTabs, TIME_YEAR_VALUE, $user->ID);
	#$arrTabs = kpi_get_total_percent( $groupDatas, $tabs );
	#$totalPercent = array_sum( $arrTabs['total_percent'] );
	# lấy % kế hoạch thực hiện được của phòng ban
	#$groupDatasPB = kpi_get_group_kpi_by_year_orgchart_for_year( $strTabs, TIME_YEAR_VALUE, $userID, 'no' );
	#$arrTabsPB = kpi_get_total_percent($groupDatasPB, $tabs);
	#$totalPercentPB = array_sum( $arrTabsPB['total_percent'] );

	$orgchartPB = user_get_phong_ban_by_user( $currentOrgID );

	$getCPRoot = capacity_get_list_kpi_by_parent(TIME_YEAR_VALUE, APPLY_OF_EMPLOYEES, 'tieu-chi-nang-luc', KPI_STATUS_RESULT);
	$getCPYearOf = capacity_get_kpi_year_and_total_not_user( $yearID, APPLY_OF_EMPLOYEES, KPI_STATUS_RESULT );
	$getCPYearOf = array_group_by($getCPYearOf, 'type_of');
	$getCPUser = capacity_get_list_kpi_by_user($yearID, APPLY_OF_EMPLOYEES, $userID, KPI_STATUS_RESULT);
	$arrGroupCPRoot = array_group_by( $getCPRoot, 'id' );
	$arrGroupCPUser = array_group_by( $getCPUser, 'parent' );
	$arrDiff = array_diff_key( $arrGroupCPRoot, $arrGroupCPUser );
	$arrDiffDel = array_diff_key( $arrGroupCPUser, $arrGroupCPRoot );
	$dataAfterInsert = [];
	foreach ( $arrDiff as $key => $values ){
		foreach ( $values as $k => $item ) {
			$dataCapacity = $item;
			$dataCapacity['parent'] = $item['id'];
			$dataCapacity['chart_id'] = $currentOrgID;
			$dataCapacity['user_id'] = $userID;
			$dataAfterInsert[$item['id']] = $item;
			unset($dataCapacity['id']);
			unset($dataCapacity['post_title']);
			unset($dataCapacity['level_1']);
			unset($dataCapacity['level_2']);
			unset($dataCapacity['level_3']);
			$wpdb->insert( $tableKpiCP, $dataCapacity );
			$dataAfterInsert[$item['id']]['id'] = $wpdb->insert_id;
		}
	}
	if( empty($dataAfterInsert) ){
		$dataAfterInsert = $getCPUser;
	}
	#kiểm tra xem kpi đã tạo trước đó nếu cấp trên xoá mà đã triển khai thì xoá luôn kpi của cấp dưới
	foreach ( $arrDiffDel as $key => $values ){
		foreach ( $values as $k => $item ) {
			capacity_delete_by_id( $item['id'] );
		}
	}
	#echo "<pre>";
	#echo "</pre>";
	if ( ! empty( $getCPYearOf ) ) {
		$kpiForI   = $getCPYearOf['kpi_nam'];
		$kpiForIII = $getCPYearOf['kpi_kq'];
	}

	#$getCPUser = capacity_get_list_kpi_by_user($yearID, APPLY_OF_EMPLOYEES, $userID, KPI_STATUS_RESULT);

	if( !empty( $kpiForI ) ):
	?>
        <h2 class='notification description description-title'>KPI năm <?= TIME_YEAR_VALUE; ?></h2>
		<form class="frm-add-kpi-year" id="" method="post" action="<?php echo admin_url("admin-ajax.php?action=update_result_of_user"); ?>">
		<input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce( "update_result_of_user" ); ?>">
		<?php
		$totalKQI = 0;
        $totalPercentPB = 0;
        $arrTabs = [
            'total_percent' => ['Finance' => 0, 'Customer' => 0, 'Operate' => 0, 'Development' => 0],
            'name' => [
                'Finance' => __('Finance', TPL_DOMAIN_LANG),
                'Customer' => __('Customer', TPL_DOMAIN_LANG),
                'Operate' => __('Operate', TPL_DOMAIN_LANG),
                'Development' => __('Development', TPL_DOMAIN_LANG)
            ],
        ];

        $usersOfCty = orgchart_get_orgchart_by_alias('congty');
        $arrUserOfCty = [];
        if( !empty( $usersOfCty ) ){
	        foreach ( $usersOfCty as $k => $item ) {
		        $arrUserOfCty[] = $item['ID'];
            }
        }
        $strUserCty = implode(", ", $arrUserOfCty);
		$totalPercent   = 0;
		$arrTabsCty = [];
		foreach ( ['quy', 'thang'] as $for => $vfor ) {
            $getYearByKpiTime = year_get_year_by_kpi_time( $vfor );
            if( !empty( $getYearByKpiTime ) ){
	            $arrKpiTime = [$vfor => ['finance' => 0, 'customer' => 0, 'operate' => 0, 'development' => 0 ] ];
                foreach ( $getYearByKpiTime as $ktime => $itemTime ){
                    $arrKpiTime[$vfor]['finance'] += $itemTime['finance'];
                    $arrKpiTime[$vfor]['customer'] += $itemTime['customer'];
                    $arrKpiTime[$vfor]['operate'] += $itemTime['operate'];
                    $arrKpiTime[$vfor]['development'] += $itemTime['development'];
                }
            }
			$groupDatasCty     = kpi_get_group_kpi_by_year_orgchart_for_year_of_users( $strTabs, TIME_YEAR_VALUE, $strUserCty, $vfor,'all' );

			$arrTabsCty = kpi_get_total_percent( $groupDatasCty, 'no' );
			$totalPercent += array_sum( $arrTabsCty['total_percent'] );
		}

		$groupDatasI = kpi_get_group_kpi_by_year_orgchart_for_year_by_lower_grade( $strTabs, TIME_YEAR_VALUE, $strUsers, 'all' );
		#$groupDatasType = array_group_by($groupDatasI, 'type');
		$groupDatas = kpi_get_group_kpi_by_year_orgchart_for_year_by_lower_grade_nv($strTabs, TIME_YEAR_VALUE, $user->ID);
		$arrTabs = kpi_get_total_percent( $groupDatasI );
		$totalPercentPB = round( array_sum( $arrTabs['total_percent'] ), 1);
		if( !empty( $kpiForI ) ): ?>
			<div id="room-target-kpi" class="section-row">
				<div class="list-detail">
					<div class="top-list-detail">
						<h3 class="title">
							<?php _e('KPI CẢ NĂM CỦA CÔNG TY - PHÒNG BAN'); ?>
						</h3>
					</div>
					<table class="table-list-detail table-managerment-target-kpi" cellpadding="0" cellspacing="0">
						<thead>
						<tr>
							<th class="column-1 align-center"><?php _e('STT'); ?></th>
							<th class="column-2"><?php _e('Nội dung'); ?></th>
							<th class="column-3"><?php _e('Trọng số'); ?></th>
							<th class="column-4"><?php _e('Kế hoạch'); ?></th>
							<th class="column-5 align-center"><?php _e('Thực hiện'); ?></th>
							<th class="column-6 align-center"><?php _e('Hoàn thành'); ?></th>
							<th class="column-7 align-center"><?php _e('Ghi chú'); ?></th>
							<th class="column-8 align-center"><?php _e('Kết quả KPI'); ?></th>
						</tr>
						</thead>
						<tbody>
						<?php
							$stt_for_I = 0;
							?>
							<?php foreach ( $kpiForI as $key => $item ):
							$stt_for_I ++;
							$titleI = $item['type_kpis'] == 'cong-ty' ? "Mục tiêu KPI công ty" : "Mục tiêu KPI Phòng ban";
							$plan = $item['plan'];
							$percent = $item['percent'];
							$note = $item['note'];
							$HT = 0;
							$totalItem = 0;
							#if( $actual_gain != '' ){
							$actual_gain = $stt_for_I == 1 ? $totalPercent : $totalPercentPB;
							$HT = (double)$actual_gain / $plan;
							$totalItem = $HT * $percent;
							$totalKQI += round($totalItem, 2);
							#}

							?>
							<tr>
								<td class="column-1 align-center"><?= $stt_for_I; ?></td>
								<td class="column-2"><?php esc_attr_e($titleI); ?></td>
								<td class="column-3"><?php esc_attr_e( $percent ); ?>%</td>
								<td class="column-4"><?php esc_attr_e( $plan ); ?>%</td>
								<td class="column-5 align-center"><?php esc_attr_e( round( $actual_gain, 1) ); ?>%</td>
								<td class="column-6 align-center"><?php echo round( $actual_gain * $plan / 100, 1); ?>%</td>
								<td class="column-7 align-center"><?php echo strip_tags($note); ?></td>
								<td class="column-8 align-center">
									<?php echo round($totalItem, 2); ?>%
								</td>
							</tr>
						<?php endforeach; ?>
						<tr class="kpi-of-year">
							<td colspan="7"><?php _e('Tổng', TPL_DOMAIN_LANG); ?></td>
							<td class="column-8 align-center">
								<?php echo $totalKQI; ?>%
							</td>
						</tr>
						</tbody>

					</table>
				</div>
			</div>
		<?php endif; ?>

            <div class="section-row">
                <div class="list-detail clearfix">
                    <div class="top-list-detail">
                        <h3 class="title">
				            <?php _e('KPI CẢ NĂM CỦA NHÂN VIÊN'); ?>
                        </h3>
                    </div>
                    <table class="table-list-detail table-managerment-target-kpi table-result-kpi-personal" cellpadding="0" cellspacing="0">
                        <thead>
                        <tr>
                            <th rowspan="" class="column-1 kpi-id align-center"><?php _e('STT', TPL_DOMAIN_LANG); ?></th>
                            <th rowspan="" class="column-2 kpi-content"><?php _e('Mục tiêu', TPL_DOMAIN_LANG); ?></th>
                            <th rowspan="" class="column-3 kpi-company_plan"><?php _e('Trọng số (%)', TPL_DOMAIN_LANG); ?></th>
                            <th rowspan="" class="column-4 kpi-department_plan"><?php _e('Chứng minh', TPL_DOMAIN_LANG); ?> </th>
                            <th rowspan="" class="column-6 kpi-receive align-center"><?php _e('Kế hoạch', TPL_DOMAIN_LANG); ?></th>
                            <th colspan="" class="column-7 align-center"><?php _e('Thực hiện', TPL_DOMAIN_LANG); ?></th>
                            <th colspan="" class="column-8 align-center"><?php _e('Hoàn thành', TPL_DOMAIN_LANG); ?></th>
                            <th rowspan="" class="column-9 align-center"><?php _e('Kết quả KPI', TPL_DOMAIN_LANG); ?></th>
                        </tr>
                        </thead>
                        <tbody>
			            <?php
                        $totalKPIOfYearOfMem = 0;
                        $totalKQII = 0;
			            if (!empty($groupDatas)):
				            foreach ($groupDatas as &$item):
					            $sttKPI++;
					            $plan = $item['plan'];
					            $actual_gain = $item['actual_gain'];
					            $planForYear = $item['plan_for_year'];
					            $percentForMonth = 0;
					            $percentForYear = 0;
                                $total_actual_gain = kpi_sum_kpi_by_parent( $item['id'] );
                                if( !empty( $total_actual_gain ) && array_key_exists('total_actual_gain', $total_actual_gain) ){
                                    $ttactual_gain = $total_actual_gain['total_actual_gain'];
                                }else{
                                    $ttactual_gain = '';
                                }
					            ?>
                                <tr>
                                    <td class="column-1 kpi-id align-center">
							            <?php echo $sttKPI;?>
                                    </td>
                                    <td class="column-2 kpi-content"><?php echo $item['post_title']; ?></td>
                                    <td class="column-3 kpi-company_plan"><?php echo $item['percent']; ?></td>
                                    <td class="column-4 kpi-department_plan">
							            <?php
                                        $getFiles = kpi_get_file_kpi_by_parent( $item['id'] );
							            if( !empty( $getFiles ) ){
									            echo sprintf("<a href='javascript:;' data-action='%s' data-id='%s' class='view-file-upload' data-target='#tpl-personal-view-file' >%s</a>", admin_url('admin-ajax.php?action=load_file_upload_by_parent&id='.$item['id']), $item['id'], __('Xem danh sách tập tin', TPL_DOMAIN_LANG));
							            }
							            ?>
                                    </td>
                                    <td class="column-6 kpi-receive align-center"><?php echo esc_attr($plan) . " " . getUnit( $item['unit'] ); ?></td>
                                    <td class="column-7 align-center">
							            <?php
							            if( $ttactual_gain != '' ):
								            $formulas = maybe_unserialize( $item['formulas'] );
								            $percentForMonth = getPercentForMonth( $item['unit'], $item['formula_type'], $formulas, $plan, $ttactual_gain );
								            $KQKPI = $percentForMonth * (int)$item['percent'] / 100;
								            $totalKQII += $KQKPI;
                                            $totalKPIOfYearOfMem += round( $KQKPI, 1 );
							            endif;
                                        echo esc_attr( $ttactual_gain ) . " " . getUnit( $item['unit'] );
							            ?>

                                    </td>
                                    <td colspan="" class="column-8 kpi-percent align-center"><?php echo round( $percentForMonth, 1 ); ?>%</td>
						            <?php /*<td class="column-9 align-center"><?php echo $percentForYear; ?></td> */ ?>

                                    <td class="column-9 align-center">
							            <?php
							            echo round($KQKPI, 1 );
							            ?>%
                                    </td>
                                </tr>
				            <?php endforeach;
			            endif;
			            ?>
                        <tr class="kpi-of-year">
                            <td colspan="7"><?php _e('Tổng', TPL_DOMAIN_LANG); ?></td>
                            <td class="column-8 align-center">
					            <?= round($totalKQII, 1); ?>%
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
		<?php
		$totalActualGain = 0;
		$rank = "";
		if( !empty( $dataAfterInsert ) ):
		?>
			<div id="management-capacity" class="section-row">
				<div class="list-detail">
					<div class="top-list-detail">
						<h3 class="title">
							<?php _e('Tiêu chí năng lực dành cho nhân viên'); ?>
						</h3>
					</div>
				</div>
				<table class="table-list-detail table-managerment-capacity" cellpadding="0" cellspacing="0">
					<thead>
					<tr>
						<th class="column-1 align-center"><?php _e('STT'); ?></th>
						<th class="column-2"><?php _e('Chỉ tiêu mong đợi'); ?></th>
						<th class="column-3 align-center"><?php _e('Cần cải thiện'); ?></th>
						<th class="column-4 align-center"><?php _e('Hiệu quả'); ?></th>
						<th class="column-5 align-center"><?php _e('Nổi bật'); ?></th>
						<th class="column-6 align-center"><?php _e('Xu hướng'); ?></th>
						<th class="column-7 align-center"><?php _e('Ghi chú'); ?></th>
						<th class="column-8 align-center"><?php _e('Kết quả'); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php

						$stt = 0;
						foreach ($dataAfterInsert as $key => $item):
							$post_title = $item['post_title'];
							$capacityID = $item['id'];
							$actual_gain = $item['actual_gain'];
							$level_1 = $item['level_1'];
							$level_2 = $item['level_2'];
							$level_3 = $item['level_3'];
							$note = strip_tags($item['note']);
							if( $actual_gain == '' ){
								$trend = "";
							}else{
								$trend = "";
								$actual_gain = (int)$actual_gain;
								if( $actual_gain <= $level_1 || ( $level_1 < $actual_gain && $actual_gain < $level_2 ) ){
									$trend = "<i class=\"fa fa-long-arrow-up icon-trend icon-trend-level_1\"></i>";
								}elseif( $level_2 <= $actual_gain && $actual_gain < $level_3 ){
									$trend = "<i class=\"fa fa-long-arrow-up icon-trend icon-trend-level_2\"></i>";
								}elseif($level_3 <= $actual_gain){
									$trend = "<i class=\"fa fa-long-arrow-up icon-trend icon-trend-level_3\"></i>";
								}
								$totalActualGain += $actual_gain;
							}
							?>
							<tr>
								<td class="column-1 align-center"><?php $stt++; echo $stt; ?></td>
								<td class="column-2"><?= $post_title; ?></td>
								<td class="column-3 align-center"><?php esc_attr_e($level_1); ?>%</td>
								<td class="column-4 align-center"><?php esc_attr_e($level_2); ?>%</td>
								<td class="column-5 align-center"><?php esc_attr_e($level_3); ?>%</td>
								<td class="column-6 align-center">
									<?php echo $trend; ?>
								</td>
								<td class="column-7 align-center">
                                    <?php
                                    if( $userID != $item['user_id'] ):
                                    ?>
									<textarea name="capacity[<?= $capacityID; ?>][note]" class="textarea-note"><?php echo strip_tags($note); ?></textarea>
                                    <?php else: ?>
	                                    <?php echo strip_tags($note); ?>
                                    <?php endif; ?>
								</td>
								<td class="column-8 align-center">
                                    <input type="hidden" name="capacity[<?= $capacityID; ?>][ID]" value="<?= $capacityID; ?>">
									<input name="capacity[<?= $capacityID; ?>][actual_gain]" type="text" value="<?php esc_attr_e($actual_gain); ?>" class="txt-result" />
								</td>
							</tr>
							<?php
						endforeach;
					?>
					<tr class="kpi-of-year">
						<td colspan="7"><?php _e('Tổng', TPL_DOMAIN_LANG); ?></td>
						<td class="column-8 align-center">
							<?= (int)$totalActualGain; ?>%
						</td>
					</tr>
					</tbody>

				</table>

			</div>
		<?php endif; ?>
	<?php
	$stt_for_III = 0;
	$totalIII = 0;
	$textRating = '';
	if ( ! empty( $kpiForIII ) ):
	?>
		<div id="results-of" class="section-row">
			<div class="list-detail">
				<div class="top-list-detail">
					<h3 class="title">
						<?php _e('Kết quả của quá trình đánh giá'); ?>
					</h3>
				</div>
			</div>
			<table class="table-list-detail table-managerment-capacity" cellpadding="0" cellspacing="0">
				<thead>
				<tr>
					<th class="column-1 align-center"><?php _e('Nội dung'); ?></th>
					<th class="column-2"><?php _e('Tổng điểm'); ?></th>
					<th class="column-3 align-center"><?php _e('Trọng số'); ?></th>
					<th class="column-4 align-center"><?php _e('Kết quả KPI'); ?></th>
					<th class="column-5 align-center"><?php _e('Danh hiệu'); ?></th>
				</tr>
				</thead>
				<tbody>
				<?php
					$totalRT = 0;
					$it = 0;
					foreach ( $kpiForIII as $key => $item ):
						$it++;
						if( $it == 1 ){
							$total = round($totalKQI, 2);
						}elseif( $it == 2 ){
						    $total = $totalKPIOfYearOfMem;
                        }else{
							$total = (int)$totalActualGain;
						}
						$percent = (int)$item['percent'];
						$kq = $total * $percent / 100;
						$totalRT += $kq;
					endforeach;
					if( $totalRT <= 50 ){
						$textRating = 'Kém';
					}elseif( $totalRT > 50 && $totalRT <= 60 ){
						$textRating = 'Cần cải thiện';
					}elseif( $totalRT > 60 && $totalRT <= 70 ){
						$textRating = 'Đạt';
					}elseif( $totalRT > 70 && $totalRT <= 90 ){
						$textRating = 'Rất tốt';
					}else{
						$textRating = 'Xuất sắc';
					}
				endif; ?>
				<?php if ( ! empty( $kpiForIII ) ): ?>
					<?php

					foreach ( $kpiForIII as $key => $item ):
						$stt_for_III ++;
						$td = '';
						if( $stt_for_III == 1 ){
							$td = '<td class="column-4 align-center width10" rowspan="4">'.esc_attr($textRating).'</td>';
							$total = round($totalKQI, 2);
						}elseif( $stt_for_III == 2 ){
                            $total = $totalKPIOfYearOfMem;
                        }elseif( $stt_for_III == 3 ){
							$total = (int)$totalActualGain;
						}
						$percent = (int)$item['percent'];
						$kq = $total * $percent / 100;
						$totalIII += $kq;
						?>
						<tr>
							<td class="column-1">Điểm phần <?= $stt_for_III; ?></td>
							<td class="column-2 align-center width10"><?= $total; ?>%</td>
							<td class="column-3 align-center width10"><?php echo $percent; ?>%</td>
							<td class="column-4 align-center width10"><?php echo round($kq,3); ?>%</td>
							<?= $td; ?>
						</tr>
						<?php
					endforeach;
				?>
				<tr class="kpi-of-year">
					<td colspan="3"><?php _e('Tổng', TPL_DOMAIN_LANG); ?></td>
					<td class="column-8 align-center">
						<?= $totalIII; ?>%
					</td>
				</tr>
				</tbody>
			</table>
		</div>
		<?php endif; ?>
		<div id="general-comment" class="section-row">
			<div class="list-detail">
				<div class="top-list-detail">
					<h3 class="title">
						<?php _e('Nhận xét chung'); ?>
					</h3>
				</div>
			</div>
			<div class="managerment-capacity-feedback">
				<div class="managerment-capacity-feedback-content">
					<div class="capacity-trend">
						<div class="title">
							<h3><?php _e('Xu hướng năng lực'); ?></h3>
						</div>
                        <div class="show-info-input">
							<?php
							$capNX = capacity_get_nx_by_user( $userID, $yearID );
							if( empty( $capNX ) ){
								$capNX = ['trend' => 'di-xuong', 'note' => '', 'id' => ''];
							}
							?>
                            <input type="hidden" name="capacityfb[ID]" value="<?php echo $capNX['id']; ?>">
                            <select class="selectpicker" name="capacityfb[trend]">
								<?php
								foreach ($capacity_nx as $key => $nx):
									$selected = $capNX['trend'] == $key ? 'selected' : '';
									?>
                                    <option <?= $selected; ?> value="<?php esc_attr_e($key); ?>"><?php esc_attr_e($nx); ?></option>
									<?php
								endforeach;
								?>
                            </select>
                            <div class="show-image">
                            </div>
                        </div>
					</div>
					<div class="capacity-feedback-other">
						<div class="title">
							<h3><?php _e('Nhận xét khác'); ?></h3>
						</div>
						<div class="show-info-input">
                            <textarea name="capacityfb[note]" class="textarea-capacity-feedback"><?php echo strip_tags($capNX['note']); ?></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="action-bot">
				<button type="submit" class="btn-brb-default btn-primary" name="btn-save"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php  _e('Save'); ?></button>
                <button type="button" class="btn-brb-default btn-primary btn-print" name="btn-print" onclick="location.href='<?php
				echo esc_url( PAGE_EXPORTS_URL );
				?>'"><i
                            class="fa fa-print"
                            aria-hidden="true"></i> <?php _e('Xuất báo cáo', TPL_DOMAIN_LANG); ?></button>
                <?php
                    $load_params = [
                        'action' => 'export_capacity_result_of_member',
                        'year_id' => $yearID,
                    ];

                    if( user_is_manager() ){
                        $load_params['user_id'] = $userID;
                    }
                    $actionExport = add_query_arg( $load_params, admin_url("admin-ajax.php") );

				/*<button data-action="<?php echo $actionExport; ?>" data-wpnonce="<?php echo wp_create_nonce( "export_capacity_result_of_member" ); ?>" data-method="POST" type="button" class="btn-brb-default btn-primary btn-export-capacity-year"><i class="fa fa-sign-out" aria-hidden="true"></i> <?php _e('Xuất file'); ?></button>*/
				?>
			</div>
		</div>
	</form>
        <div id="tpl-personal-view-file" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
            <form class="modal-dialog modal-lg">
                <input type="hidden" name="id" value="">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><?php _e('Danh sách tập tin tải lên', TPL_DOMAIN_LANG); ?></h4>
                    </div>
                    <div class="modal-body clearfix">
                        <table class="table-list-files table-list-detail">
                            <thead>
                            <tr>
                                <th class="column-1"><?php _e('STT', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-2"><?php _e('Tên tập tin', TPL_DOMAIN_LANG); ?></th>
                            </tr>
                            </thead>
                            <tbody class="tbody-main">

                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
	<?php
	else:
		$year_list = capacity_year_list();
		echo "<div class=\"col-md-6 \"><h2 class='notification error description'>Không tìm thấy KPI cá nhân của năm ".TIME_YEAR_VALUE."</h2></div>
";?>
        <div class="col-md-6 col-year capacity-for-year">
            <label for="select-for-year-phongban"><?php _e( 'Năm', TPL_DOMAIN_LANG ); ?></label>
            <select class="selectpicker select-for-year" id="select-for-year" name="nam">
                <option value="">-- Năm --</option>
				<?php foreach ( $year_list as $item ):
					$selected = $item['year'] == TIME_YEAR_VALUE ? 'selected="selected"' : '';
					echo sprintf( '<option value="%s" %s>%s</option>', $item['year'], $selected, $item['year'] );
				endforeach;
				?>
            </select>
        </div>
<?php

	endif;
endif;