<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 2/9/18
 * Time: 11:41
 */
require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-orgchart.php';

$user = wp_get_current_user();
$firstYear = kpi_get_first_year_capacity( TIME_YEAR_VALUE );
$year_id = !empty( $firstYear ) ? $firstYear['id'] : 0;
$year = !empty( $firstYear ) ? $firstYear['year'] : 0;
$kpiForI = [];
$kpiForIII = [];
$checkPUBLISH = true;

$getCP = capacity_get_list_kpi($year_id, APPLY_OF_MANAGER, CAPACITY_TIEU_CHI, 'chitiet');
$getCPYearOf = capacity_get_kpi_year_and_total($year_id, APPLY_OF_MANAGER, $user->ID);
$getCPYearOf = array_group_by($getCPYearOf, 'type_of');

if( isset($_GET['create']) && ( !empty( $getCPYearOf ) || !empty( $getCP ) ) ){
    $redirect = preg_replace("#(\&create\=mngql)#i", "", $_SERVER['HTTP_REFERER']);
    #redirect to page edit;
	echo "<script> window.location.href = \"{$redirect}\" </script>";
}

global $kpi_contents_parts;
$applyKpiSystem = $kpi_contents_parts['apply-bsc-kpi-system'];
$applyKpiSystemUrl = $applyKpiSystem['url'];
$url = add_query_arg('type', 'kpi-nam', $applyKpiSystemUrl);
if( isset($_GET['create']) && !empty( $getCPYearOf ) ){
 echo "<h2 class='notification error description'>KPI cho năm ". TIME_YEAR_VALUE ." đã tồn tại. Vui lòng thực hiện thao tác khác.</h2>";
}else {

	if ( ! empty( $getCPYearOf ) ) {
		$kpiForI   = $getCPYearOf['kpi_nam'];
		$kpiForIII = $getCPYearOf['kpi_kq'];
	}

	$year_list = capacity_year_list();

	?>
    <div class="col-md-6 ">
		<?php
		if ( $year < TIME_YEAR_VALUE || ( empty( $getCP ) && empty( $getCPYearOf ) ) ) {
			if ( user_is_manager() ) {
				$view        = isset( $_GET['view'] ) && $_GET['view'] == 'mngql' ? 'mngql' : '';
				$mnUrlQLView = add_query_arg( 'view', $view, $url );
				$mnUrlQL     = add_query_arg( 'create', 'mngql', $mnUrlQLView );
				$mnUrlQL     = add_query_arg( 'nam', $_GET['nam'], $mnUrlQL );
				echo "<a href=\"{$mnUrlQL}\" class='mngql' title=\"Tạo KPI Năng lực\">Tạo KPI Năng lực quản lý</a>";
			}
		}else{
		    $showPublish = true;
		    if( !empty( $getCP ) ){
		        foreach ( $getCP as $key => $item){
		            if( $showPublish == false )
		                break;
		            if( $showPublish && $item['status'] != KPI_STATUS_RESULT ){
		                $showPublish = false;
                    }
                }
            }
            if( !empty( $kpiForI ) && $showPublish ){
	            foreach ( $kpiForI as $key => $item){
		            if( $showPublish == false )
			            break;
		            if( $showPublish && $item['status'] != KPI_STATUS_RESULT ){
			            $showPublish = false;
		            }
	            }
            }
            if( !empty( $kpiForIII ) && $showPublish ){
	            foreach ( $kpiForIII as $key => $item){
		            if( $showPublish == false )
			            break;
		            if( $showPublish && $item['status'] != KPI_STATUS_RESULT ){
			            $showPublish = false;
		            }
	            }
            }
            if( $showPublish ) {
	            echo "<h3 class='success notification'>
                    <i class=\"fa fa-check-circle\"></i>
                    Đã được triển khai
                </h3>";
            }else{
	            echo "<h3 class='notification warning'>
                    <i class=\"fa fa-exclamation-triangle\"></i>
                    Chưa được triển khai
                </h3>";
            }
        }
		?>
    </div>
    <div class="col-md-6 col-year capacity-for-year">
        <label for="select-for-year-phongban"><?php echo _LBL_YEAR;//_e( 'Năm', TPL_DOMAIN_LANG ); ?></label>
        <select class="selectpicker select-for-year" id="select-for-year" name="nam">
            <option value="">-- Năm --</option>
			<?php foreach ( $year_list as $item ):
				$selected = $item['year'] == TIME_YEAR_VALUE ? 'selected="selected"' : '';
				echo sprintf( '<option value="%s" %s>%s</option>', $item['year'], $selected, $item['year'] );
			endforeach;
			?>
        </select>
    </div>
    <div class="clearfix"></div>
	<?php
	if ( ! empty( $getCP ) || ! empty( $getCPYearOf ) || ( ! empty( $_GET['create'] ) && $_GET['create'] == 'mngql' ) ):
		?>
        <form class="frm-add-manager frm-add-kpi-year" id="" method="post"
              action="<?php echo admin_url( "admin-ajax.php?action=save_capacity_manager" ); ?>">
            <input type="hidden" name="year_id" value="<?php echo ! empty( $firstYear ) ? $firstYear['id'] : 0; ?>">
            <input type="hidden" name="apply_of" value="<?php echo APPLY_OF_MANAGER; ?>">
            <input type="hidden" name="_wp_http_referer"
                   value="<?php echo "?type=" . $_GET['type'] . "&view=" . $_GET['view'] . "&nam=" . $_GET['nam']; ?>">
            <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce( "save_capacity_manager" ); ?>">
            <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
            <div id="room-target-kpi" class="section-row">
                <div class="list-detail">
                    <div class="top-list-detail">
                        <h3 class="title">
							<?php _e( 'KPI CẢ NĂM CỦA CÔNG TY - PHÒNG BAN' ); ?>
                        </h3>
                    </div>
                    <table class="table-list-detail table-managerment-target-kpi" cellpadding="0" cellspacing="0">
                        <thead>
                        <tr>
                            <th class="column-1 align-center"><?php _e( 'STT' ); ?></th>
                            <th class="column-2"><?php _e( 'Nội dung' ); ?></th>
                            <th class="column-3 align-center"><?php _e( 'Trọng số' ); ?></th>
                            <th class="column-4 align-center"><?php _e( 'Kế hoạch' ); ?></th>
                            <th class="column-5 align-center"><?php _e( 'Thực hiện' ); ?></th>
                            <th class="column-6 align-center"><?php _e( 'Hoàn thành' ); ?></th>
                            <th class="column-7 align-center"><?php _e( 'Ghi chú' ); ?></th>
                        </tr>
                        </thead>
                        <tbody>
						<?php
						$stt_for_I = 0;
						if ( ! empty( $kpiForI ) ):
							foreach ( $kpiForI as $key => $item ):
								if ( $checkPUBLISH && $item['status'] != KPI_STATUS_RESULT ) {
									$checkPUBLISH = false;
								}
								$stt_for_I ++;
								$titleI = $item['type_kpis'] == 'cong-ty' ? "Mục tiêu KPI Công Ty" : "Mục tiêu KPI Phòng Ban";
								$cl     = $item['type_kpis'] == 'cong-ty' ? 'cty' : 'pb';
								?>
                                <tr>
                                    <td class="column-1 align-center"><?= $stt_for_I; ?></td>
                                    <td class="column-2"><?php echo $titleI; ?></td>
                                    <td class="column-3 width10 align-center">
                                        <input type="text" name="type_nam[<?= $cl; ?>][percent]" size="10"
                                               value="<?php esc_attr_e( $item['percent'] ); ?>"> %
                                        <input type="hidden" name="type_nam[<?= $cl; ?>][ID]" size="10"
                                               value="<?php esc_attr_e( $item['id'] ); ?>">
                                    </td>
                                    <td class="column-4 width10 align-center">
                                        <input type="text" name="type_nam[<?= $cl; ?>][plan]" size="10"
                                               value="<?php esc_attr_e( $item['plan'] ); ?>"> %
                                    </td>
                                    <td class="column-5 align-center"></td>
                                    <td class="column-6 align-center"></td>
                                    <td class="column-7 align-center">
                                        <textarea
                                                name="type_nam[<?= $cl; ?>][note]"><?php esc_attr_e( $item['note'] ); ?></textarea>
                                    </td>
                                </tr>
								<?php
							endforeach;
						else:
							?>
                            <tr>
                                <td class="column-1 align-center"><?= 1; ?></td>
                                <td class="column-2">Mục tiêu KPI công ty</td>
                                <td class="column-3 width10 align-center">
                                    <input type="text" name="type_nam[cty][percent]" size="10" value=""> %
                                    <input type="hidden" name="type_nam[cty][ID]" size="10" value="">
                                </td>
                                <td class="column-4 width10 align-center">
                                    <input type="text" name="type_nam[cty][plan]" size="10" value=""> %
                                </td>
                                <td class="column-5 align-center"></td>
                                <td class="column-6 align-center"></td>
                                <td class="column-7 align-center">
                                    <textarea name="type_nam[cty][note]"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1 align-center"><?= 1; ?></td>
                                <td class="column-2">Mục tiêu KPI Phòng ban</td>
                                <td class="column-3 align-center">
                                    <input type="text" name="type_nam[pb][percent]" size="10" value=""> %
                                    <input type="hidden" name="type_nam[pb][ID]" size="10" value="">
                                </td>
                                <td class="column-4 align-center">
                                    <input type="text" name="type_nam[pb][plan]" size="10" value=""> %
                                </td>
                                <td class="column-5 align-center"></td>
                                <td class="column-6 align-center"></td>
                                <td class="column-7 align-center">
                                    <textarea name="type_nam[pb][note]"></textarea>
                                </td>
                            </tr>
						<?php endif; ?>
                        </tbody>

                    </table>
                </div>
            </div>

            <div id="management-capacity" class="section-row">
                <div class="list-detail">
                    <div class="top-list-detail">
                        <h3 class="title">
							<?php _e( 'Tiêu chí năng lực dành cho quản lý' ); ?>
                        </h3>
                    </div>
                </div>

                <input type="hidden" name="apply_of" value="quan-ly">
                <table class="table-list-detail table-managerment-capacity" cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <th class="column-1 align-center"><?php _e( 'STT' ); ?></th>
                        <th class="column-2"><?php _e( 'Chỉ tiêu mong đợi' ); ?></th>
                        <th class="column-3 align-center"><?php _e( 'Cần cải thiện' ); ?></th>
                        <th class="column-4 align-center"><?php _e( 'Hiệu quả' ); ?></th>
                        <th class="column-5 align-center"><?php _e( 'Nổi bật' ); ?></th>
                        <th class="column-6 align-center"><?php _e( 'Ghi chú' ); ?></th>
                        <th class="column-8 align-center"><?php _e( 'Thao tác' ); ?></th>
                    </tr>
                    </thead>
                    <tbody>
					<?php
					if ( ! empty( $getCP ) ):
						$stt = 0;
						foreach ( $getCP as $key => $item ):
							if ( $checkPUBLISH && $item['status'] != KPI_STATUS_RESULT ) {
								$checkPUBLISH = false;
							}
							$stt ++;
							?>
                            <tr>
                                <td class="column-1 align-center"><?= $stt; ?></td>
                                <td class="column-2"><?= esc_attr( $item['post_title'] ); ?></td>
                                <td class="column-3 align-center"><?= esc_attr( $item['level_1'] ); ?>%</td>
                                <td class="column-4 align-center"><?= esc_attr( $item['level_2'] ); ?>%</td>
                                <td class="column-5 align-center"><?= esc_attr( $item['level_3'] ); ?>%</td>
                                <td class="column-6 align-center">
									<?= esc_attr( $item['note'] ); ?>
                                </td>
                                <td class="column-8">
                                    <input type="hidden" name="kpi_capacity[<?php echo $item['id']?>][id]"                                           value="<?= $item['id']?>"/>
                                    <a href="javascript:;" data-id="<?php echo $item['id']; ?>"
                                       data-wpnonce="<?= wp_create_nonce( "load_capacity" ); ?>"
                                       data-action="load_capacity" data-method="get" data-toggle="modal"
                                       data-target="#add-management-capacity" class="edit action-edit"
                                       title="<?php _e( 'Chỉnh sửa', TPL_DOMAIN_LANG ); ?>"> <span
                                                class="glyphicon glyphicon-pencil"></span> <?php _e( 'Chỉnh sửa', TPL_DOMAIN_LANG ); ?>
                                    </a><br>
                                    <a href="javascript:;" data-nonce="<?= wp_create_nonce( 'delete_capacity' ); ?>"
                                       data-id="<?php echo $item['id']; ?>" data-action="delete_capacity"
                                       data-title="Bạn có chắc xóa năng lực quản lý hiện tại?"
                                       data-target="#confirm-apply-kpi" data-method="post" class="delete"
                                       title="<?php _e( 'Xóa', TPL_DOMAIN_LANG ); ?>"><span
                                                class="glyphicon glyphicon-trash"></span> <?php _e( 'Xóa', TPL_DOMAIN_LANG ); ?>
                                    </a>
                                </td>
                            </tr>
						<?php endforeach; ?>
					<?php endif; ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="7">
                            <a href="javascript:;" data-title="Bạn có chắc xoá năng lực quản lý?" data-toggle="modal"
                               data-target="#add-management-capacity" class=""><i class="fa fa-plus-circle"
                                                                                  aria-hidden="true"></i> <?php _e( 'Thêm chỉ tiêu' ); ?>
                            </a>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>

            <div id="results-of" class="section-row">
                <div class="list-detail">
                    <div class="top-list-detail">
                        <h3 class="title">
							<?php _e( 'Kết quả của quá trình đánh giá' ); ?>
                        </h3>
                    </div>
                </div>
                <table class="table-list-detail table-managerment-capacity-total" cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <th class="column-1"><?php _e( 'Nội dung' ); ?></th>
                        <th class="column-2"><?php _e( 'Tổng điểm' ); ?></th>
                        <th class="column-3 align-center"><?php _e( 'Trọng số' ); ?></th>
                    </tr>
                    </thead>
                    <tbody>
					<?php if ( ! empty( $kpiForIII ) ): ?>
						<?php
						$stt_for_III = 0;
						foreach ( $kpiForIII as $key => $item ):
							if ( $checkPUBLISH && $item['status'] != KPI_STATUS_RESULT ) {
								$checkPUBLISH = false;
							}
							$stt_for_III ++;
							#$titleIII = $item['type_kpis'] == 'cong-ty' ? "Điểm phần 1" : "Điểm phần 2";
							?>
                            <tr>
                                <td class="column-1">Điểm phần <?= $stt_for_III; ?></td>
                                <td class="column-2 align-center width10"></td>
                                <td class="column-3 align-center width10">
                                    <input type="text" name="type_kq[<?= $stt_for_III; ?>][percent]" size="10"
                                           value="<?php esc_attr_e( $item['percent'] ); ?>"> %
                                    <input type="hidden" name="type_kq[<?= $stt_for_III; ?>][ID]" size="10"
                                           value="<?php esc_attr_e( $item['id'] ); ?>">

                                </td>
                            </tr>
							<?php
						endforeach;
						?>
					<?php else: ?>
                        <tr>
                            <td class="column-1">Điểm phần 1</td>
                            <td class="column-2 align-center width10">0%</td>
                            <td class="column-3 align-center width10">
                                <input type="text" name="type_kq[1][percent]" size="10" value=""> %
                            </td>
                        </tr>
                        <tr>
                            <td class="column-1">Điểm phần 2</td>
                            <td class="column-2 align-center width10">0%</td>
                            <td class="column-3 align-center width10">
                                <input type="text" name="type_kq[2][percent]" size="10" value=""> %
                            </td>
                        </tr>
					<?php endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="action-bot">
                <button type="submit" class="btn-brb-default btn-primary btn-save-change" name="btn-save"><i
                            class="fa fa-floppy-o" aria-hidden="true"></i> <?php _e( 'Save' ); ?></button>
                <button type="button" class="btn-brb-default btn-primary btn-save-assign <?php echo $checkPUBLISH ? "hidden" : ""; ?>"
                            name="btn-save-assign"  data-action="save_and_assign_capacity_manager"><?php _e( 'Triển khai' ); ?></button>
                <button type="button" class="btn-brb-default btn-cancel"><i class="fa fa-trash"
                                                                            aria-hidden="true"></i> <?php _e( 'Cancel' ); ?>
                </button>
            </div>
        </form>
        <div id="add-management-capacity" class="modal fade" role="dialog">
            <form class="modal-dialog modal-md" enctype="application/x-www-form-urlencoded"
                  action="<?php echo admin_url( 'admin-ajax.php?action=add_edit_management_capacity' ) ?>"
                  method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Thêm năng lực KPI</h4>
                    </div>
                    <div class="modal-body clearfix">
                        <div class="col-md-12">
                            <div class="form-group form-plans">
                                <div class="form-group input-group input-group-select">
                                    <span class="input-group-addon" for="post_title"
                                          id="post_title"><?php _e( 'Tiêu chí', TPL_DOMAIN_LANG ); ?></span>
                                    <select data-none-results-text="<?php _e( 'Kết quả tìm kiếm {0}', TPL_DOMAIN_LANG ); ?>"
                                            data-none-selected-text="<?php _e( 'Tiêu chí', TPL_DOMAIN_LANG ); ?>"
                                            data-loading=".form-group.input-group.input-group-select"
                                            class="selectpicker bank_id" name="bank_id" data-live-search="true">
										<?php
										$slug        = "tieu-chi-nang-luc-quan-ly";
										$cate        = getRootKPICapacity( $slug );
										$idCatParent = 0;
										if ( ! empty( $cate ) && ! is_wp_error( $cate ) ) {
											$cate    = $cate->term_id;
											$getBank = getKpisByCategory( $cate, [ 'publish' ], [ 'posts_per_page' => - 1 ] );
											if ( ! empty( $getBank ) && ! is_wp_error( $getBank ) ) {
												echo renderPostDropdown( $getBank->get_posts(), 0 );
											}
										}
										?>
                                    </select>
                                </div>
                                <div class="form-group input-group">
                                    <span class="input-group-addon" for="level_1"
                                          id="level_1"><?php _e( 'Cần cải thiện', TPL_DOMAIN_LANG ); ?></span>
                                    <input type="text" readonly name="level_1" class="level_1 form-control"
                                           placeholder="<?php _e( 'Cần cải thiện', TPL_DOMAIN_LANG ); ?>" value=""/>
                                </div>
                                <div class="form-group input-group">
                                    <span class="input-group-addon" for="level_2"
                                          id="level_2"><?php _e( 'Hiệu quả', TPL_DOMAIN_LANG ); ?></span>
                                    <input type="text" readonly name="level_2" class="level_2 form-control"
                                           placeholder="<?php _e( 'Hiệu quả', TPL_DOMAIN_LANG ); ?>" value=""/>
                                </div>
                                <div class="form-group input-group">
                                    <span class="input-group-addon" for="level_3"
                                          id="level_3"><?php _e( 'Nổi bật', TPL_DOMAIN_LANG ); ?></span>
                                    <input type="text" readonly name="level_3" class="level_3 form-control"
                                           placeholder="<?php _e( 'Nổi bật', TPL_DOMAIN_LANG ); ?>" value=""/>
                                </div>
                                <div class="form-group input-group">
                                    <span class="input-group-addon" for="note"
                                          id="note"><?php _e( 'Ghi chú', TPL_DOMAIN_LANG ); ?></span>
                                    <input type="text" name="note" class="percent form-control"
                                           placeholder="<?php _e( 'Ghi chú', TPL_DOMAIN_LANG ); ?>" value=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" value="">
                        <input type="hidden" name="_wpnonce"
                               value="<?php echo wp_create_nonce( "add_edit_management_capacitys" ); ?>">
                        <input type="hidden" name="year_id"
                               value="<?php echo ! empty( $firstYear ) ? $firstYear['id'] : 0; ?>">
                        <input type="hidden" name="apply_of" value="<?php echo "quan-ly"; ?>">
                        <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <button type="submit" class="btn btn-primaryy">Lưu</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
	<?php endif; ?>
	<?php
}
