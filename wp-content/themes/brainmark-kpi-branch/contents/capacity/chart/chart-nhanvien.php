<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 06/02/2018
 * Time: 00:37
 */
$year_list = kpi_get_year_list(0, KPI_YEAR_STATUS_PUBLISH);
$totalPercent = 1;
$data = [];
$totalColorLoop  = 30;
for( $i = 0; $i<=$totalColorLoop; $i++ ){
	$data[$i] = 10;
}
$totalPercentMonth = ($totalPercentItem / $totalPercent ) * 100;
if( $totalPercentMonth % 1 === 0 ){
	$totalPercentMonth = number_format($totalPercentMonth, 1);
}

$percentItem = $totalPercentMonth / 10;
if( !empty($percentItem) ) {
	$gradient_1 = Gradient("ff1300", "ffe100", $percentItem);
	$gradient_2 = Gradient("ffe100", "84e91c", $percentItem);
	$gradient_3 = Gradient("84e91c", "84e91c", $percentItem);
}else{
	$gradient_1 = [];
	$gradient_2 = [];
	$gradient_3 = [];
}
$dataSet = ['datasets' => [['backgroundColor' => array_merge($gradient_1, $gradient_2, $gradient_3 ), 'data' => array_map('intval', $data)]], 'total' => (double)$totalPercentMonth];

?>
<div class="col-md-12 management-top">
    <div class="header-target-kpi header-block">
        <div class="for-date">
            <div class="for-year">
                <label for="select-for-year"><?php _e('Năm', TPL_DOMAIN_LANG); ?></label>
                <select class="select-for-year selectpicker" id="select-for-year" name="">
					<?php foreach ($year_list as $item):
						$selected = $item['year'] == TIME_YEAR_VALUE ? 'selected="selected"' : '';
						echo sprintf('<option value="%s" %s>%s</option>', $item['year'], $selected, $item['year']);
					endforeach;?>
                </select>
            </div>
            <div class="for-quarter">
                <label for="select-for-quarter"><?php _e('Quý', TPL_DOMAIN_LANG); ?></label>
                <select class="select-for-quarter selectpicker" id="select-for-quarter" name="">
                    <option value="0"><?php _e('Cả năm', TPL_DOMAIN_LANG); ?></option>
					<?php
					for ( $i = 1; $i <= 4; $i++ ){
						$selected = $i == TIME_PRECIOUS_VALUE ? 'selected="selected"' : '';
						echo sprintf('<option value="%s" %s>Quý %s</option>', $i, $selected, $i);
					}
					?>
                </select>
            </div>
        </div>
        <div class="name-block">
            <h2><?php _e('KPI năng lực', TPL_DOMAIN_LANG); ?></h2>
        </div>
    </div>
</div>
<div class="col-md-4">
    <div id="block-target-kpi" class="block-shadow block-item border-radius-default block-management">
        <div class="header-dashboard-kpi header-block">
            <div class="name-block">
                <h2><?php _e('Kết quả', TPL_DOMAIN_LANG); ?></h2>
            </div>
        </div>
        <div class="content-block content-block-target-kpi">
            <div class="report-target-kpi management-chart">
                <div class="report-item">
                    <canvas data-type="derivedDoughnut" width="200" height="100" data-datasets="<?php echo str_replace('"', '&#34;', json_encode( $dataSet ) ); ?>" data-report="" id="canvas-report-kpi" class="data-report"></canvas>
                </div>
            </div>
            <div class="progress-item">
                <div class="header-progress">
                    <span class="title"><?php _e('Thái độ hành vi', TPL_DOMAIN_LANG); ?> <strong>(50)</strong></span>
                    <span class="percent">50%</span>
                </div>
                <div class="progress">
                    <div class="progress-bar progress-bar-room" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%"></div>
                </div>
            </div>
        </div>
    </div>
</div>