<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 06/02/2018
 * Time: 00:24
 */

require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-years.php';

$userID = 0;
global $wpdb;
if( array_key_exists( 'uid', $_GET ) ){
    $userID = (int)wp_slash( $_GET['uid'] );
}
$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
$tableKpiCP = "{$prefix}kpis_capacity";
$getUser = wp_get_current_user();
$orgchartUser = user_load_orgchart($getUser);


$orgChartParent = 0;
if( $userID > 0 ) {
    $getUser = kpi_get_user_by_id($userID);
    $orgchartUser = $getUser->__get('orgchart');
    #$orgChartParent = $orgchartUser->parent;

}
$orgchartID = $orgchartUser ? $orgchartUser->id : '';
$member_role = $orgchartUser ? $orgchartUser->role : '';
$orgChartParent = $orgchartUser->parent;
$aliasUser = $orgchartUser->role;
$firstYear = kpi_get_first_year_capacity( TIME_YEAR_VALUE );
$yearID = !empty( $firstYear ) ? $firstYear['id'] : 0;
$kpiForI = [];
$kpiForIII = [];
if( in_array( $aliasUser, ['phongban', 'bgd'] ) ):
    $userID = $getUser->ID;
    $getCPRoot = capacity_get_list_kpi_by_parent(TIME_YEAR_VALUE, APPLY_OF_MANAGER, 'tieu-chi-nang-luc', KPI_STATUS_RESULT);
    $getCPYearOf = capacity_get_kpi_year_and_total_not_user( $yearID, APPLY_OF_MANAGER );
    $getCPYearOf = array_group_by($getCPYearOf, 'type_of');
    $getCPUser = capacity_get_list_kpi_by_user($yearID, APPLY_OF_MANAGER, $userID, KPI_STATUS_RESULT);
    $arrGroupCPRoot = array_group_by( $getCPRoot, 'id' );
    $arrGroupCPUser = array_group_by( $getCPUser, 'parent' );
    $arrDiff = array_diff_key( $arrGroupCPRoot, $arrGroupCPUser );
    $arrDiffDel = array_diff_key( $arrGroupCPUser, $arrGroupCPRoot );

    foreach ( $arrDiff as $key => $values ){
        foreach ( $values as $k => $item ) {
            $dataCapacity = $item;
            $dataCapacity['parent'] = $item['id'];
            $dataCapacity['chart_id'] = $orgchartUser->id;
            $dataCapacity['user_id'] = $userID;
            unset($dataCapacity['id']);
            unset($dataCapacity['post_title']);
            unset($dataCapacity['level_1']);
            unset($dataCapacity['level_2']);
            unset($dataCapacity['level_3']);
            $wpdb->insert( $tableKpiCP, $dataCapacity );
        }
    }
    #kiểm tra xem kpi đã tạo trước đó nếu cấp trên xoá mà đã triển khai thì xoá luôn kpi của cấp dưới
    foreach ( $arrDiffDel as $key => $values ){
        foreach ( $values as $k => $item ) {
            capacity_delete_by_id( $item['id'] );
        }
    }
    #echo "<pre>";
    #echo "</pre>";
    if ( ! empty( $getCPYearOf ) ) {
        $kpiForI   = $getCPYearOf['kpi_nam'];
        $kpiForIII = $getCPYearOf['kpi_kq'];
    }

    #$getCPUser = capacity_get_list_kpi_by_user($yearID, APPLY_OF_MANAGER, $userID);

?>
<form class="frm-add-kpi-year" id="" method="post" action="<?php echo admin_url("admin-ajax.php?action=update_result_of_member"); ?>">
    <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce( "update_result_of_member" ); ?>">
<div id="room-target-kpi" class="section-row">
    <div class="list-detail">
        <div class="top-list-detail">
            <h3 class="title">
                <?php _e('KPI CẢ NĂM CỦA CÔNG TY - PHÒNG BAN'); ?>
            </h3>
        </div>
            <table class="table-list-detail table-managerment-target-kpi" cellpadding="0" cellspacing="0">
                <thead>
                <tr>
                    <th class="column-1 align-center"><?php _e('STT'); ?></th>
                    <th class="column-2"><?php _e('Nội dung'); ?></th>
                    <th class="column-3"><?php _e('Trọng số'); ?></th>
                    <th class="column-4"><?php _e('Kế hoạch'); ?></th>
                    <th class="column-5 align-center"><?php _e('Thực hiện'); ?></th>
                    <th class="column-6 align-center"><?php _e('Hoàn thành'); ?></th>
                    <th class="column-7 align-center"><?php _e('Ghi chú'); ?></th>
                    <th class="column-8 align-center"><?php _e('Kết quả KPI'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $totalKQI = 0;
                if( !empty( $kpiForI ) ):
                    $stt_for_I = 0;
                ?>
                    <?php foreach ( $kpiForI as $key => $item ):
                        $stt_for_I ++;
                        $titleI = $item['type_kpis'] == 'cong-ty' ? "Mục tiêu KPI công ty" : "Mục tiêu KPI Phòng ban";
                        $actual_gain = $item['actual_gain'];
                        $plan = $item['plan'];
                        $percent = $item['percent'];
                        $note = $item['note'];
                        $HT = 0;
                        $totalItem = 0;
                        if( $actual_gain != '' ){
                            $HT = (double)$actual_gain / $plan;
                            $totalItem = $HT * $percent / 100;
                            $totalKQI+= (int)$totalItem;
                        }

                        ?>
                        <tr>
                            <td class="column-1 align-center"><?= $stt_for_I; ?></td>
                            <td class="column-2"><?php esc_attr_e($titleI); ?></td>
                            <td class="column-3"><?php esc_attr_e( $percent ); ?>%</td>
                            <td class="column-4"><?php esc_attr_e( $plan ); ?>%</td>
                            <td class="column-5 align-center"><?php esc_attr_e( $actual_gain ); ?></td>
                            <td class="column-6 align-center"><?php echo (int)$HT; ?>%</td>
                            <td class="column-7 align-center"><?php esc_attr_e($note); ?></td>
                            <td class="column-8 align-center">
                                <?php echo (int)$totalItem; ?>%
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                <tr class="kpi-of-year">
                    <td colspan="7"><?php _e('Tổng', TPL_DOMAIN_LANG); ?></td>
                    <td class="column-8 align-center">
                        <?php echo $totalKQI; ?>%
                    </td>
                </tr>
                </tbody>

            </table>
    </div>
</div>

<div id="management-capacity" class="section-row">
    <div class="list-detail">
        <div class="top-list-detail">
            <h3 class="title">
                <?php _e('Tiêu chí năng lực dành cho quản lý'); ?>
            </h3>
        </div>
    </div>
        <table class="table-list-detail table-managerment-capacity" cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                <th class="column-1 align-center"><?php _e('STT'); ?></th>
                <th class="column-2"><?php _e('Chỉ tiêu mong đợi'); ?></th>
                <th class="column-3 align-center"><?php _e('Cần cải thiện'); ?></th>
                <th class="column-4 align-center"><?php _e('Hiệu quả'); ?></th>
                <th class="column-5 align-center"><?php _e('Nổi bật'); ?></th>
                <th class="column-6 align-center"><?php _e('Xu hướng'); ?></th>
                <th class="column-7 align-center"><?php _e('Ghi chú'); ?></th>
                <th class="column-8 align-center"><?php _e('Kết quả'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $totalActualGain = 0;
            $rank = "";
            if( !empty( $getCPUser ) ):
                $stt = 0;
                foreach ($getCPUser as $key => $item):
                    $post_title = $item['post_title'];
                    $capacityID = $item['id'];
                    $actual_gain = $item['actual_gain'];
                    $level_1 = $item['level_1'];
                    $level_2 = $item['level_2'];
                    $level_3 = $item['level_3'];
                    $note = strip_tags($item['note']);
                    if( $actual_gain == '' ){
                        $trend = "";
                    }else{
                        $trend = "";
                        $actual_gain = (int)$actual_gain;
                        if( $actual_gain <= $level_1 || ( $level_1 < $actual_gain && $actual_gain < $level_2 ) ){
                            $trend = "<i class=\"fa fa-long-arrow-up icon-trend icon-trend-level_1\"></i>";
                        }elseif( $level_2 <= $actual_gain && $actual_gain < $level_3 ){
                            $trend = "<i class=\"fa fa-long-arrow-up icon-trend icon-trend-level_2\"></i>";
                        }elseif($level_3 <= $actual_gain){
                            $trend = "<i class=\"fa fa-long-arrow-up icon-trend icon-trend-level_3\"></i>";
                        }
                        $totalActualGain += $actual_gain;
                    }
                    ?>
                    <tr>
                        <td class="column-1 align-center"><?php $stt++; echo $stt; ?></td>
                        <td class="column-2"><?= $post_title; ?></td>
                        <td class="column-3 align-center"><?php esc_attr_e($level_1); ?>%</td>
                        <td class="column-4 align-center"><?php esc_attr_e($level_2); ?>%</td>
                        <td class="column-5 align-center"><?php esc_attr_e($level_3); ?>%</td>
                        <td class="column-6 align-center">
                            <?php echo $trend; ?>
                        </td>
                        <td class="column-7 align-center">
                            <textarea name="capacity[<?= $capacityID; ?>][note]" class="textarea-note"><?php esc_attr_e($note); ?></textarea>
                            <input type="hidden" name="capacity[<?= $capacityID; ?>][ID]" value="<?= $capacityID; ?>">
                        </td>
                        <td class="column-8 align-center">
                            <input name="capacity[<?= $capacityID; ?>][actual_gain]" type="text" value="<?php esc_attr_e($actual_gain); ?>" class="txt-result" />
                        </td>
                    </tr>
            <?php
                endforeach;
            endif;
            ?>
            <tr class="kpi-of-year">
                <td colspan="7"><?php _e('Tổng', TPL_DOMAIN_LANG); ?></td>
                <td class="column-8 align-center">
                    <?= (int)$totalActualGain; ?>%
                </td>
            </tr>
            </tbody>

        </table>

</div>


<div id="results-of" class="section-row">
    <div class="list-detail">
        <div class="top-list-detail">
            <h3 class="title">
                <?php _e('Kết quả của quá trình đánh giá'); ?>
            </h3>
        </div>
    </div>
        <table class="table-list-detail table-managerment-capacity" cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                <th class="column-1 align-center"><?php _e('Nội dung'); ?></th>
                <th class="column-2"><?php _e('Tổng điểm'); ?></th>
                <th class="column-3 align-center"><?php _e('Trọng số'); ?></th>
                <th class="column-4 align-center"><?php _e('Kết quả KPI'); ?></th>
                <th class="column-5 align-center"><?php _e('Danh hiệu'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php if ( ! empty( $kpiForIII ) ): ?>
            <?php
                $stt_for_III = 0;
                $totalIII = 0;
                foreach ( $kpiForIII as $key => $item ):
                    $stt_for_III ++;
                    $td = '';
                    if( $stt_for_III == 1 ){
                        $td = '<td class="column-4 align-center width10" rowspan="3">Xuất sắc</td>';
                        $total = (int)$totalKQI;
                    }else{
                        $total = (int)$totalActualGain;
                    }
                    $percent = (int)$item['percent'];
                    $kq = $total * $percent / 100;
                    $totalIII += $kq;
                    ?>
                    <tr>
                        <td class="column-1">Điểm phần <?= $stt_for_III; ?></td>
                        <td class="column-2 align-center width10"><?= $total; ?>%</td>
                        <td class="column-3 align-center width10"><?php echo $percent; ?>%</td>
                        <td class="column-4 align-center width10"><?php echo $kq; ?>%</td>
                        <?= $td; ?>
                    </tr>
                <?php
                endforeach;
            endif;
            ?>
            <tr class="kpi-of-year">
                <td colspan="3"><?php _e('Tổng', TPL_DOMAIN_LANG); ?></td>
                <td class="column-8 align-center">
                    <?= $totalIII; ?>%
                </td>
            </tr>
            </tbody>
        </table>
</div>
<div id="general-comment" class="section-row">
    <div class="list-detail">
        <div class="top-list-detail">
            <h3 class="title">
                <?php _e('Nhận xét chung'); ?>
            </h3>
        </div>
    </div>
    <div class="managerment-capacity-feedback">
        <div class="managerment-capacity-feedback-content">
            <div class="capacity-trend">
                <div class="title">
                    <h3><?php _e('Xu hướng năng lực'); ?></h3>
                </div>
                <div class="show-info-input">
                    <select class="selectpicker" name="">
                        <option value="1"><?php _e('Đi lên'); ?></option>
                    </select>
                    <div class="show-image">
                    </div>
                </div>
            </div>
            <div class="capacity-feedback-other">
                <div class="title">
                    <h3><?php _e('Nhận xét khác'); ?></h3>
                </div>
                <div class="show-info-input">
                    <textarea name="" class="textarea-capacity-feedback"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="action-bot">
        <button type="submit" class="btn-brb-default btn-save" name="btn-save"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php  _e('Save'); ?></button>
        <button type="button" class="btn-brb-default btn-cancel"><i class="fa fa-trash" aria-hidden="true"></i> <?php _e('Cancel'); ?></button>
    </div>
</div>
</form>
<?php
endif;