<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 2/6/18
 * Time: 15:12
 */

require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-orgchart.php';

$userID = 0;
global $wpdb;
if( array_key_exists( 'uid', $_GET ) ){
    $userID = (int)wp_slash( $_GET['uid'] );
}
$user = wp_get_current_user();
$orgchart = user_load_orgchart($user);
$currentOrgID = $orgchart ? $orgchart->id : '';
$member_role = $orgchart ? $orgchart->role : '';

$orgChartParent = 0;
if( $userID > 0 ) {
    $getUser = kpi_get_user_by_id($userID);
    $orgchartUser = $getUser->__get('orgchart');
    $orgChartParent = $orgchartUser->parent;
	$member_role = $orgchartUser ? $orgchartUser->role : '';
}else{
	$getUser = wp_get_current_user();
	$orgchartUser = $getUser->__get('orgchart');
	$orgChartParent = $orgchartUser->parent;
	$member_role = $orgchartUser ? $orgchartUser->role : '';
	$userID = $getUser->ID;
}
echo '<div id="confirm-register-kpi" class="confirm-register-kpi">';
if (!empty($userID) && !empty($getUser) && $orgChartParent != 0 && isset($_GET['viewmyself'])):
    #$member_role = $orgchartUser->role;
    #get_template_part('contents/capacity/chart/chart', $member_role);
    #get_template_part('contents/capacity/news/news', $member_role);
    get_template_part('contents/capacity/capacity', $member_role);
elseif( !in_array( $_GET['view'], ['mngql', 'mngnv'] ) ):
    require_once THEME_DIR . '/ajax/get_users_list.php';
    require_once THEME_DIR . '/ajax/get_user_info.php';
    $orgCharts = orgchart_get_all_kpi_by_parent( $currentOrgID );
    $arrOrgChart = [];
    foreach ( $orgCharts as $key => $item ){
        $arrOrgChart[] = $item['id'];
    }
    $strOrgChart = implode(", ", $arrOrgChart);
    $listUser = user_list_for_kpi( $strOrgChart, TIME_YEAR_VALUE, 'nam', 'canhan' );
    $listUser = array_group_by( $listUser, 'room' );
    global $kpi_contents_parts;
    $applyKpiSystem = $kpi_contents_parts['apply-bsc-kpi-system'];
    $applyKpiSystemUrl = $applyKpiSystem['url'];
    $url = add_query_arg('type', 'kpi-nam', $applyKpiSystemUrl);
    if( user_is_manager() ){
        $mnUrlQL = add_query_arg('view', 'mngql', $url);
        $mnUrlNV = add_query_arg('view', 'mngnv', $url);
        $htmlViewOrCreate = "<a href=\"{$mnUrlQL}\" class='mngql' title=\"Xem KPI Năng lực\">Xem KPI Năng lực quản lý</a>";
        $htmlViewOrCreate .= "<a href=\"{$mnUrlNV}\" class='mngnv' title=\"Xem KPI Năng lực\">Xem KPI Năng lực nhân viên</a>";
    }else{
        $mnUrl = add_query_arg('viewmyself', true, $url);
        $htmlViewOrCreate = "<a href=\"{$mnUrl}\" title=\"Xem KPI Cá nhân\">Xem KPI Cá nhân</a>";
    }
    ?>
    <div class="lists confirm-register-list-user block-item <?php echo empty($orgChartParent) ? 'approved-kpi' : ''; ?>">
        <h1 class="title-list-member"><?php _e('Danh sách triển khai', TPL_DOMAIN_LANG); ?></h1>
        <label class="list-user-kpi-for-year">
            <?php echo $htmlViewOrCreate; ?>
        </label>
        <table class="user-list list-staff" cellpadding="0" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th class="id-col">STT</th>
                <th>Mã nhân viên</th>
                <th class="name"><?php _e('Name', TPL_DOMAIN_LANG); ?></th>
                <th class="position_name"><?php _e('Position Name', TPL_DOMAIN_LANG); ?></th>
                <th>Duyệt KPI</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $output = '';
            if( !empty($listUser) ):
	            $stt               = 1;
                foreach ( $listUser as $key => $item ) {
	                $tdChild = '';
	                foreach ( $item as $user ):
		                $countKPI = $user['count_kpi'];
		                $url_1           = add_query_arg( 'uid', $user['ID'], $url );
		                $url_2           = add_query_arg( 'viewmyself', true, $url_1 );
		                $showHtmlCount = "";
		                if ( $countKPI > 0 ) {
			                $showHtmlCount = "<label title='Số lượng cần duyệt' class='stick-count-kpi'>{$countKPI}</label>";
		                }
		                $tdChild .=
                        "<tr class=\"user-row\">
                            <td class=\"id-stt\">{$stt}</td>
                            <td>".$user['user_nicename']."</td>
                            <td class=\"display_name\">".$user['display_name']." ".$showHtmlCount."</td>
                            <td class=\"position_name\">".__( $user['orgchart_name'], TPL_DOMAIN_LANG )."</td>
                            <td>
                                <div class=\"viewkpiofroom approve-register\">
                                    <a href=\"{$url_2}\" title=\"Xem\">Xem</a>
                                </div>
                            </td>
                        </tr>";
		                $stt ++;
	                endforeach;
	                $output .= "
                        <tr class='parent'>
                            <td colspan='5'>{$key}</td>
                        </tr> {$tdChild}";
                }

            endif;
                echo $output;
            ?>
            </tbody>
        </table>
    </div>
<?php
else:
    $applyOf = !empty( $_GET['view'] ) && $_GET['view'] == 'mngql' ? APPLY_OF_MANAGER : APPLY_OF_EMPLOYEES;
    get_template_part("contents/capacity/template-add/add", $applyOf);
    ?>
<?php
endif;
echo '</div>';
/*

    <div id="management-capacity" class="section-row">
        <div class="list-detail">
            <div class="top-list-detail">
                <h3 class="title">
                    <?php
                    $titleCreate = !empty( $_GET['create'] ) && $_GET['create'] == 'mngql' ? "Tiêu chí năng lực dành cho quản lý" : "Tiêu chí năng lực dành cho nhân viên";
                    _e($titleCreate); ?>
                </h3>
            </div>
        </div>
        <form class="" id="frm-mng-capacity" method="post" action="<?php echo admin_url("admin-ajax.php?action=save_and_assign_capacity"); ?>">
            <input type="hidden" name="_wpnonce" value="<?= wp_create_nonce("save_and_assign_capacity"); ?>">
            <input type="hidden" name="apply_of" value="<?php echo !empty( $_GET['create'] ) && $_GET['create'] == 'mngql' ? "quan-ly" : "nhan-vien" ?>" >
            <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
            <table class="table-list-detail table-managerment-capacity" cellpadding="0" cellspacing="0">
                <thead>
                <tr>
                    <th class="column-1 align-center"><?php _e('STT'); ?></th>
                    <th class="column-2"><?php _e('Chỉ tiêu mong đợi'); ?></th>
                    <th class="column-3 align-center"><?php _e('Cần cải thiện'); ?></th>
                    <th class="column-4 align-center"><?php _e('Hiệu quả'); ?></th>
                    <th class="column-5 align-center"><?php _e('Nổi bật'); ?></th>
                    <th class="column-6 align-center"><?php _e('Ghi chú'); ?></th>
                    <th class="column-7 align-center"><?php _e('Áp dụng'); ?></th>
                    <th class="column-8 align-center"><?php _e('Thao tác'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                    if( !empty( $getCP ) ):
                        $stt = 0;
                        foreach ( $getCP as $key => $item ):
                            $stt++;
                    ?>
                            <tr>
                                <td class="column-1 align-center"><?= $stt; ?></td>
                                <td class="column-2"><?= esc_attr($item['post_title']); ?></td>
                                <td class="column-3 align-center"><?= esc_attr($item['level_1']); ?>%</td>
                                <td class="column-4 align-center"><?= esc_attr($item['level_2']); ?>%</td>
                                <td class="column-5 align-center"><?= esc_attr($item['level_3']); ?>%</td>
                                <td class="column-6 align-center">
                                    <?= esc_attr( $item['note'] ); ?>
                                </td>
                                <td class="column-7 align-center">
                                    <?php
                                        $checked = '';
                                        if( $item['status'] != KPI_STATUS_DRAFT ){
                                            $checked = "checked";
                                        }
                                    ?>
                                    <label class="switch">
                                        <input class="checkbox-status" name="kpi_capacity[<?php echo $item['id']; ?>][status]" <?= $checked; ?> id="kpi_capacity<?php echo $item['id']; ?>" data-id="<?php echo $item['id']; ?>" value="1" type="checkbox">
                                        <span class="checkbox-slider round"></span>
                                        <input type="hidden" name="kpi_capacity[<?php echo $item['id']; ?>][id]" value="<?php echo $item['id']; ?>" />
                                    </label>
                                </td>
                                <td class="column-8">
                                    <a href="javascript:;" data-id="<?php echo $item['id']; ?>" data-wpnonce="<?= wp_create_nonce("load_capacity"); ?>" data-action="load_capacity" data-method="get" data-toggle="modal" data-target="#add-management-capacity" class="edit action-edit" title="<?php _e('Chỉnh sửa', TPL_DOMAIN_LANG);?>"> <span class="glyphicon glyphicon-pencil"></span> <?php _e('Chỉnh sửa', TPL_DOMAIN_LANG);?></a><br>
                                    <a href="javascript:;" data-nonce="<?= wp_create_nonce('delete_capacity'); ?>" data-id="<?php echo $item['id']; ?>" data-action="delete_capacity" data-title="Bạn có chắc xóa năng lực quản lý hiện tại?" data-target="#confirm-apply-kpi" data-method="post" class="delete" title="<?php _e('Xóa', TPL_DOMAIN_LANG);?>"><span class="glyphicon glyphicon-trash"></span> <?php _e('Xóa', TPL_DOMAIN_LANG);?></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="7">
                        <a href="javascript:;" data-title="Bạn có chắc xoá năng lực quản lý?" data-toggle="modal" data-target="#add-management-capacity" class=""><i class="fa fa-plus-circle" aria-hidden="true"></i> <?php  _e('Thêm chỉ tiêu'); ?></a>
                    </td>
                </tr>
                </tfoot>
            </table>
        </form>
    </div>

    <div id="add-management-capacity" class="modal fade" role="dialog">
        <form class="modal-dialog modal-md" enctype="application/x-www-form-urlencoded"
              action="<?php echo admin_url('admin-ajax.php?action=add_edit_management_capacity') ?>" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Thêm năng lực KPI</h4>
                </div>
                <div class="modal-body clearfix">
                    <div class="col-md-12">
                        <div class="form-group form-plans">
                            <div class="form-group input-group input-group-select">
                                <span class="input-group-addon" for="post_title" id="post_title"><?php _e('Tiêu chí', TPL_DOMAIN_LANG); ?></span>
                                <select data-none-results-text="<?php _e( 'Kết quả tìm kiếm {0}', TPL_DOMAIN_LANG); ?>" data-none-selected-text="<?php _e('Tiêu chí', TPL_DOMAIN_LANG); ?>" data-loading=".form-group.input-group.input-group-select" class="selectpicker bank_id" name="bank_id" data-live-search="true">
                                    <?php
                                    $slug = ( !empty( $_GET['create']) && $_GET['create'] == 'mngql' ) ? 'tieu-chi-nang-luc-quan-ly' : 'tieu-chi-nang-luc-nhan-vien';
                                    $cate = getRootKPICapacity( $slug );
                                    $idCatParent = 0;
                                    if( !empty( $cate ) && !is_wp_error( $cate ) ){
                                        $cate = $cate->term_id;
                                        $getBank = getKpisByCategory($cate, ['publish'], ['posts_per_page' => -1]);
                                        if( !empty( $getBank ) && !is_wp_error( $getBank ) ){
                                            echo renderPostDropdown( $getBank->get_posts(), 0 );
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon" for="level_1" id="level_1"><?php _e('Cần cải thiện', TPL_DOMAIN_LANG); ?></span>
                                <input type="text" name="level_1" class="level_1 form-control" placeholder="<?php _e('Cần cải thiện', TPL_DOMAIN_LANG); ?>" value="" />
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon" for="level_2" id="level_2"><?php _e('Hiệu quả', TPL_DOMAIN_LANG); ?></span>
                                <input type="text" name="level_2" class="level_2 form-control" placeholder="<?php _e('Hiệu quả', TPL_DOMAIN_LANG); ?>" value="" />
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon" for="level_3" id="level_3"><?php _e('Nổi bật', TPL_DOMAIN_LANG); ?></span>
                                <input type="text" name="level_3" class="level_3 form-control" placeholder="<?php _e('Nổi bật', TPL_DOMAIN_LANG); ?>" value="" />
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon" for="note" id="note"><?php _e('Ghi chú', TPL_DOMAIN_LANG); ?></span>
                                <input type="text" name="note" class="percent form-control" placeholder="<?php _e('Ghi chú', TPL_DOMAIN_LANG); ?>" value="" />
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon" for="status" id="status"><?php _e('Áp dụng', TPL_DOMAIN_LANG); ?></span>
                                <div class="status-capacity">
                                    <label class="switch">
                                        <input class="checkbox-status" name="status" type="checkbox" value="1">
                                        <span class="checkbox-slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="year_id" value="<?php echo !empty($firstYear) ? $firstYear['id'] : 0; ?>">
                    <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce("add_edit_management_capacitys"); ?>">
                    <input type="hidden" name="apply_of" value="<?php echo !empty( $_GET['create'] ) && $_GET['create'] == 'mngql' ? "quan-ly" : "nhan-vien" ?>" >
                    <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <button type="submit" class="btn btn-primaryy">Lưu</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
*/