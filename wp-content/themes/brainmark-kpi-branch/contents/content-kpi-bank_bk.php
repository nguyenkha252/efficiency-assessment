<?php

require_once THEME_DIR . '/inc/lib-kpis.php';

get_template_part('contents/content', 'left');


$isGetCat = false;
if( array_key_exists( 'cat', $_GET ) && !empty( $_GET['cat'] ) ){

    global $wpdb;
    $cat = $wpdb->prepare( "%s", $_GET['cat'] );
    $myTerm = get_term_by( 'slug', $cat, 'category' );
    $level = getLevelCategories( $myTerm );
    $namePosition = ''; $IDPosition = 0; $slugPosition = ''; $parentPosition = 0; $rootParentCat = 0;
    $nameRoom = '';$IDRoom = 0; $childrenTerm = [];

    $rootCategory = getParentTerm( $myTerm, $level+1, 'category' );
    if( !empty($rootCategory) && !is_wp_error( $rootCategory ) ){
        $rootParentCat = $rootCategory->term_id;
    }
    if( !empty($myTerm) && !is_wp_error( $myTerm ) ){
        $namePosition = $myTerm->name;
        $slugPosition = $myTerm->slug;
        $IDPosition = $myTerm->term_id;
        $parentPosition = $myTerm->parent;
        $childrenTerm = get_term_children($IDPosition, 'category');
    }
    if( $level > 1 && $namePosition != ''){
        $parentTerm = getParentTerm( $myTerm, 1, 'category' );//get_term_by('id', $myTerm->parent, 'category');

        if( !empty($parentTerm) && !is_wp_error( $parentTerm ) ){
            $nameRoom = $parentTerm->name;
            $slugRoom = $parentTerm->slug;
            $IDRoom = $parentTerm->term_id;
        }
    }

    # get parent category
    $originCategory = getParentTerm( $myTerm, $level, 'category' );

    if( empty($childrenTerm) ){
        # check child
        $isGetCat = true;
    }
    #get group target
    $args_post = [
        'posts_per_page' => '-1',
        'category_name' => $slugPosition,
        'order' => 'ASC',
        'orderby' => 'post_date',
        'post_type' => 'post',
        'post_status' => ['draft', 'publish']
    ];

    $kpiQuery = new WP_Query($args_post);
    $postKPI = $kpiQuery->get_posts();
    $arrTarget = []; $arrGroup = [];
    if( !empty( $postKPI ) ){
        $arrTarget = array_group_by($postKPI, 'post_parent');
        if( !empty( $arrTarget ) ){
            $arrGroup = $arrTarget[0];
            unset($arrTarget[0]);
        }
    }
}
$title = $isGetCat ? $originCategory->name : __('Vui lòng chọn phân loại mục tiêu, trước khi tạo mục tiêu!', TPL_DOMAIN_LANG);
?>
<div class="col-sm-9 kpi-bank-right">
    <div class="content-top">
        <div class="content-title content-kpi-bank col-sm-9">
            <h1 class="entry-title"><?php echo $title; ?></h1>
        </div>
        <?php /*
		<div class="search-right col-sm-3">
			<form class="frm-search-kpi-bank" action="" method="get">
				<div class="kpi-bank-search">
					<input type="text" class="txt-default keyword" name="keyword" placeholder="Search">
					<button class="btn-kpi-search" name="kpi-search">
						<i class="fa fa-search" aria-hidden="true"></i>
					</button>
				</div>
			</form>
		</div>
        */ ?>
    </div>
    <div class="clearfix"></div>
    <?php if( $isGetCat ): ?>
        <div class="main-content-kpi">
            <div class="brb-position-top">
                <?php if( $level > 1 ): ?>
                    <div class="position-1">
                        <label class="lbl-title kpi-position-title"><?php _e('Phòng', TPL_DOMAIN_LANG); ?>:</label> <span class="kpi-position-name"><?php esc_attr_e($nameRoom); ?></span>
                    </div>
                <?php endif; ?>
                <?php if($parentPosition != $rootParentCat): ?>
                    <div class="position-1">
                        <label class="lbl-title kpi-position-title"><?php _e('Vị trí', TPL_DOMAIN_LANG); ?>:</label> <span class="kpi-position-name"><?php esc_attr_e($namePosition); ?></span>
                    </div>
                <?php endif; ?>
            </div>
            <div class="grid-view grid-view-kpi brainmark-repeater" data-idx="-1">
                <table class="table-kpi brb-custom-table custom-repeater brainmark-table">
                    <thead class="row-thead-title-parent">
                    <tr>
                        <th class="row-title title-numerical-order"><?php _e('STT', TPL_DOMAIN_LANG); ?></th>
                        <th class="row-title title-code-kpi"><?php _e('Mã KPI', TPL_DOMAIN_LANG); ?></th>
                        <th class="row-title title-target-content"><?php _e('Nội dung chỉ tiêu', TPL_DOMAIN_LANG); ?></th>
                        <th class="row-title title-target"><?php _e('Kế hoạch', TPL_DOMAIN_LANG); ?></th>
                        <th class="row-title title-type-kpi"><?php _e('Loại KPI', TPL_DOMAIN_LANG); ?></th>
                        <th class="row-title title-measure"><?php _e('Đo lường', TPL_DOMAIN_LANG); ?></th>
                        <th class="row-title title-synthesis"><?php _e('Cách tổng hợp', TPL_DOMAIN_LANG); ?></th>
                        <th class="row-title title-apply"><?php _e('Áp dụng', TPL_DOMAIN_LANG); ?></th>
                    </tr>
                    </thead>
                    <?php
                    $idxG = 1;
                    if( !empty( $arrGroup ) ){
                        foreach ( $arrGroup as $key => $item_group ):
                            #foreach ( $item_group as $k => $item ) {
                            $groupID = $item_group->ID;
                            $nameGroup = get_the_title($groupID);
                            $sttRoman = convert_number_2_roman($idxG);
                            $rowItem = '';
                            if( array_key_exists( $groupID, $arrTarget ) ){
                                $targetItem = $arrTarget[$groupID];
                                $idxTg = 0;
                                $rowItem = '<tbody class="row-tbody-content-item" data-group="' . $idxG . '">';
                                foreach ( $targetItem as $k => $item ){
                                    $idxTg++;
                                    $targetID = $item->ID;
                                    $titleTarget = get_the_title($targetID);
                                    $targetMeta = $item->target;
                                    $termKPI = get_the_terms($targetID, 'types_kpi');
                                    $mF = $item->measurement_formulas;
                                    $content = strip_tags( $item->post_content );
                                    $typesKPIName = ''; $typesKPIID = 0;
                                    $checked = $item->post_status === 'publish' ? "checked" : '';
                                    if( !empty( $termKPI ) && !is_wp_error( $termKPI ) && count( $termKPI ) > 0 ){
                                        $typesKPIName = $termKPI[0]->name;
                                        $typesKPIID = $termKPI[0]->term_id;
                                    }
                                    $mFContent = '';
                                    if( !empty($mF) ){
                                        foreach ( $mF as $mk => $mv ){
                                            $mfCondition = $mv['condition'];
                                            $mfValue = $mv['value'];
                                            $mfName = getMeasurementFormulas($mfCondition);
                                            if( !empty( $mfName ) ){
                                                $mFContent .= $mfName .': ' . $mfValue .'% KPI<br>';
                                            }
                                        }
                                    }

                                    $rowItem .= '<tr class="brainmark-kpi-target-item" data-idg="' . $idxTg . '" data-id="' . esc_attr($targetID) . '">
                                                    <td class="item-numerical-order">
                                                        <span class="value">' . $idxTg . '</span>
                                                        <input name="kpi_bank[post][' . $idxTg . '][menu_order]" id="kpi_bank_menu_order_' . $idxTg . '" class="input-text hidden-default">
                                                    </td>
                                                    <td class="item-code">
                                                        <span class="value">' . esc_attr($targetID) . '</span>
                                                    </td>
                                                    <td class="item-content">
                                                        <span class="value">' . esc_attr($titleTarget) . '</span>
                                                    </td>
                                                    <td class="item-target">
                                                        <span class="value">' . esc_attr($targetMeta) . '</span>
                                                    </td>
                                                    <td class="item-type-kpi">
                                                        <span class="value">' . esc_attr($typesKPIName) . '</span>
                                                    </td>
                                                    <td class="item-measure">
                                                        <span class="value">' . $mFContent . '</span>
                                                    </td>
                                                    <td class="item-synthesis">
                                                        <span class="value">' . esc_attr($content) . '</span>
                                                    </td>
                                                    <td class="item-action-on-off">
                                                        <label class="switch">
                                                            <input ' . $checked .' class="checkbox-status" name="kpi_bank[post][' . $idxTg . '][status]" id="kpi_bank_status_' . $idxTg . '" data-id="' . $targetID . '" value="1" type="checkbox">
                                                            <span class="checkbox-slider round"></span>
                                                        </label>
                                                    </td>
                                                </tr>';

                                }
                                unset( $arrTarget[$groupID] );

                                $rowItem .= '</tbody>';
                            }

                            ?>
                            <thead class="row-thead-title-group" data-group="<?php echo $idxG; ?>"
                                   data-idx-group="<?php echo $idxG; ?>">
                            <tr>
                                <th class="row-group-title group-numerical-order"><?php echo $sttRoman; ?></th>
                                <th colspan="4"
                                    class="row-group-title group-title"><?php esc_attr_e($nameGroup); ?></th>
                                <th colspan="3"
                                    class="row-group-title action-add-row-target action-add-row-group">
                                    <a href="javascript:;" data-parent="<?php echo $groupID; ?>"
                                       data-event="add-row-item"><?php _e('Thêm mục tiêu', TPL_DOMAIN_LANG); ?>
                                        <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                    <?php
                                    if( empty( $rowItem ) ):
                                    ?>
                                    <a href="javascript:;" data-action="del_group_target" data-id="<?php echo $groupID; ?>" data-event="del-row-group">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                    <?php endif; ?>
                                </th>
                            </tr>
                            </thead>
                            <?php
                            echo $rowItem;
                            $idxG++;
                            #}
                        endforeach;
                    }
                    ?>
                    <tfoot class="row-tfoot-group">
                    <tr>
                        <th colspan="8" class="action-add-group">
                            <a href="javascript:;" data-show="show-row"><?php _e('Thêm nhóm mục tiêu', TPL_DOMAIN_LANG); ?><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                        </th>
                    </tr>
                    <tr class="display-none">
                        <th colspan="8">

                            <form novalidate="novalidate" action="<?php echo admin_url('admin-ajax.php'); ?>" class="frm-add-group-target" data-add-group-event="" method="post">
                                <?php
                                if( $level > 1 ):
                                    ?>
                                    <input type="hidden" value="<?php esc_attr_e($IDRoom); ?>" name="post_category[]" />
                                <?php endif; ?>
                                <input type="hidden" value="<?php esc_attr_e($IDPosition); ?>" name="post_category[]" />
                                <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce('create_posts'); ?>" />
                                <input type="hidden" name="action" value="create_group_target" />

                                <label for="post-title-group-target"></label>
                                <input required="required" id="post-title-group-target" data-err-required="<?php _e('Vui lòng nhập tên nhóm mục tiêu', TPL_DOMAIN_LANG); ?>" type="text" value="" class="input-text" name="post_title" placeholder="<?php _e('Tên nhóm mục tiêu', TPL_DOMAIN_LANG); ?>" />


                                <button type="submit" name="add_group_target" class="btn btn-add-group-target"><i class="fa fa-plus-circle" aria-hidden="true"></i><?php _e('Tạo nhóm'); ?></button>
                            </form>
                        </th>
                    </tr>
                    </tfoot>
                    <thead class="row-thead-title-group clone-thead repeat-clone" data-clone-group="">
                    <tr>
                        <th class="row-group-title group-numerical-order"><?php _e('STT', TPL_DOMAIN_LANG); ?></th>
                        <th colspan="4" class="row-group-title group-title">
                            <span class="content"></span>
                            <input type="hidden" value="" class="" name="group_kpi[IDX][]" />
                        </th>
                        <th colspan="3" class="row-group-title action-add-row-target action-add-row-group">
                            <a href="javascript:;" data-event="add-row-item"><?php _e('Thêm mục tiêu', TPL_DOMAIN_LANG); ?> <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                            <a href="javascript:;" data-event="del-row-group"> <i class="fa fa-trash-o" aria-hidden="true"></i> </a>
                        </th>
                    </tr>
                    </thead>
                    <tr class="group-item-clone repeat-clone" data-id="brainmarkcloneitemindex">
                        <td class="item-numerical-order">1</td>
                        <td class="item-code"></td>
                        <td class="item-content"></td>
                        <td class="item-target"></td>
                        <td class="item-type-kpi"></td>
                        <td class="item-measure"></td>
                        <td class="item-synthesis"></td>
                        <td class="item-action-on-off">
                            <label class="switch">
                                <input class="checkbox-status" name="post-status" type="checkbox">
                                <span class="checkbox-slider round"></span>
                            </label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="popup-kpi-bank" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><?php _e('Ngân hàng KPI', TPL_DOMAIN_LANG); ?></h4>
                    </div>
                    <div class="modal-body">
                        <form data-add-group-event="" novalidate="novalidate" action="<?= admin_url('admin-ajax.php'); ?>" method="post" class="frm-add-target-item">
                            <table class="table-input-kpi-bank">
                                <tr class="code-kpi">
                                    <td class="column-1">
                                        <label for=""><?php _e('Mã KPI', TPL_DOMAIN_LANG); ?>:</label>
                                    </td>
                                    <td class="column-2">
                                        <span class="code-post"></span>
                                        <input type="hidden" name="post_id" class="post_id" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="column-1" valign="top">
                                        <label for=""><?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?>:</label>
                                    </td>
                                    <td class="column-2">
                                        <textarea name="post_title" placeholder="<?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?>" ></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="column-1">
                                        <label><?php _e('Chỉ tiêu', TPL_DOMAIN_LANG); ?></label>
                                    </td>
                                    <td class="column-2">
                                        <input type="text" value="" name="meta_input[target]" class="input-text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="column-1">
                                        <label><?php _e('Loại KPI', TPL_DOMAIN_LANG); ?></label>
                                    </td>
                                    <td class="column-2">
                                        <?php
                                        # @return array|int|WP_Error List of WP_Term instances and their children. Will return WP_Error, if any of $taxonomies do not exist.
                                        $typesKPI = get_terms([
                                            'taxonomy' => 'types_kpi',
                                            'hide_empty' => 0,
                                            'orderby'           => 'id',
                                            'order'             => 'ASC',
                                        ]);
                                        ?>
                                        <select class="width_full" data-live-search="true" data-container="this.parentNode.parentNode.parentNode" name="tax_input[types_kpi]" data-use-bootstrap="">
                                            <?php
                                            if( !empty( $typesKPI ) && !is_wp_error( $typesKPI )){
                                                foreach ( $typesKPI as $key => $term_kpi ){
                                                    $kpi_slug = $term_kpi->slug;
                                                    $kpi_name = $term_kpi->name;
                                                    $kpi_id = $term_kpi->term_id;
                                                    echo "<option value='".esc_attr($kpi_id)."'>".esc_attr($kpi_name)."</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="column-1">
                                        <label><?php _e('Cách tổng hợp', TPL_DOMAIN_LANG ); ?></label>
                                    </td>
                                    <td class="column-2">
                                        <textarea placeholder="<?php _e('Mô tả cách tổng hợp', TPL_DOMAIN_LANG ); ?>" name="post_content" class="input-text" ></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="column-1"><?php _e("Áp dụng", TPL_DOMAIN_LANG ); ?></td>
                                    <td class="column-2">
                                        <label class="switch">
                                            <input class="checkbox-status" name="post_status" id="children-post-status" type="checkbox" value="on">
                                            <span class="checkbox-slider round"></span>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <fieldset class="fieldset-measurement-formulas">
                                            <legend><?php _e('Công thức đo lường', TPL_DOMAIN_LANG); ?></legend>
                                            <div class="fieldset-top">
                                            <span class="fs-target">
                                                <strong><?php _e('KH', TPL_DOMAIN_LANG); ?>: </strong>
                                                <span><?php _e('Kế hoạch'); ?></span>
                                            </span>
                                                <span class="fs-result">
                                                <strong><?php _e('KQ', TPL_DOMAIN_LANG); ?>: </strong>
                                                <span><?php _e('Kết quả thực tế đạt được', TPL_DOMAIN_LANG); ?></span>
                                            </span>
                                            </div>
                                            <div class="mf-content">
                                                <table class="table-measurement-formulas scroll">
                                                    <thead>
                                                    <tr>
                                                        <th><?php _e('Điều kiện', TPL_DOMAIN_LANG); ?></th>
                                                        <th><?php _e('Kết quả đánh giá (%)', TPL_DOMAIN_LANG); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="tbody-main">
                                                    <tr data-idx="0">
                                                        <td>
                                                            <select class="width_full" data-use-bootstrap="" name="meta_input[measurement_formulas][0][condition]">
                                                                <?php
                                                                $measurementFormulas = getMeasurementFormulas();
                                                                if( !empty($measurementFormulas) ):
                                                                    foreach ( $measurementFormulas as $key => $mf ){
                                                                        echo "<option value='".$key."'>".$mf."</option>";
                                                                    }
                                                                endif;
                                                                ?>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" name="meta_input[measurement_formulas][0][value]" value="" class="input-text value-result-of-evaluation width_full" />
                                                            <a href="javascript:;" class="remove-repeater-mf" data-event="remove-condition-mf"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                    <tbody class="clone-mf display-none">
                                                    <tr class="" data-idx="IDX">
                                                        <td>
                                                            <select class="width_full" data-use-bootstrap="" name="meta_input[measurement_formulas][IDX][condition]">
                                                                <?php
                                                                $measurementFormulas = getMeasurementFormulas();
                                                                if( !empty($measurementFormulas) ):
                                                                    foreach ( $measurementFormulas as $key => $mf ){
                                                                        echo "<option value='".$key."'>".$mf."</option>";
                                                                    }
                                                                endif;
                                                                ?>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" name="meta_input[measurement_formulas][IDX][value]" value="" class="input-text value-result-of-evaluation width_full" />
                                                            <a href="javascript:;" class="remove-repeater-mf" data-event="remove-condition-mf"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <td colspan="2">
                                                            <a href="javascript:;" class="" data-event="add-condition-mf"><i class="fa fa-plus-circle" aria-hidden="true"></i> <?php  _e('Thêm điều kiện'); ?></a>
                                                        </td>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="hidden">
                                            <?php
                                            if( $level > 1 ):
                                                ?>
                                                <input type="hidden" value="<?php esc_attr_e($IDRoom); ?>" name="post_category[]" />
                                            <?php endif; ?>
                                            <input type="hidden" value="<?php esc_attr_e($IDPosition); ?>" name="post_category[]" />
                                            <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce('create_posts'); ?>" />
                                            <input type="hidden" name="post_parent" value="" />
                                            <input type="hidden" value="create_edit_target_item" name="action">

                                        </div>
                                        <div class="action-bot">
                                            <button type="submit" class="btn-brb-default btn-save" id="save-target-item" name="btn-save"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php  _e('Xác nhận'); ?></button>
                                            <button type="button" class="btn-brb-default btn-cancel close-popup"><i class="fa fa-trash" aria-hidden="true"></i> <?php _e('Huỷ'); ?></button>
                                            <button data-id="" data-action="del_target" data-nonce="<?php echo wp_create_nonce('delete_posts'); ?>" type="button" class="btn-brb-default btn-cancel" data-event="del-item">
                                                <?php  _e('Xóa'); ?>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            </table>


                        </form>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>

            </div>
        </div>

        <!-- Modal -->
        <div id="myDialogConfirm" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"><?php _e('Bạn có chắc chắn xóa mục tiêu hiện tại', TPL_DOMAIN_LANG); ?></h4>
                    </div>
                    <div class="modal-body">
                        <button type="button" class="btn-brb-default btn-yes"><?php _e('Đồng ý', TPL_DOMAIN_LANG); ?></button>
                        <button type="button" data-dismiss="modal" class="btn-brb-default btn-cancel btn-no"><?php _e('Hủy', TPL_DOMAIN_LANG); ?></button>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>

            </div>
        </div>
    <?php endif; ?>
</div>

