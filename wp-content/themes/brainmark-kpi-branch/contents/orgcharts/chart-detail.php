<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/19/18
 * Time: 09:58
 */
global $ds_chuc_danh, $cap_bac, $users, $jsonUserData;
global $kpi_contents_parts;

global $wpdb;
$charts = kpi_list_charts_tree(0);

require_once THEME_DIR . '/inc/render-charts.php';
?>
<div class="ds-orgchart">
    <a href="<?php echo $kpi_contents_parts['organizational-structure']['url']; ?>?chuc-danh=1">Tạo Chức Danh</a>
    <a href="javascript:;" class="<?php echo (!empty($charts) ? 'hidden' : ''); ?>"
       data-toggle="modal" data-action="new" data-level="1" data-target="#tpl-form-user-role"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
    <ul id="orgchart-root" class="root" data-url="<?php echo admin_url('admin-ajax.php?action=position_update&_wpnonce='.wp_create_nonce('position_update')); ?>"><?php
        kpi_render_tree_chart_node($charts, 1);
        ?>
    </ul>
    <svg class="ds-orgchart-line" width="1920" height="1920"></svg>
</div>
<div id="tpl-form-user-role" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog" data-users="<?php echo esc_json_attr($jsonUserData); ?>">
    <form class="modal-dialog modal-md" onsubmit="return false" enctype="application/x-www-form-urlencoded" method="post"
          data-update-url="<?php echo admin_url('admin-ajax.php?action=user_role_update'); ?>"
          data-add-url="<?php echo admin_url('admin-ajax.php?action=user_role_add'); ?>">
        <input type="hidden" name="level" value="">
        <input type="hidden" name="parent" value="">
        <input type="hidden" name="ID" value="">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" data-title="<?php _e('Thêm Orgchart ', TPL_DOMAIN_LANG); ?>"><?php _e('Thêm Orgchart ', TPL_DOMAIN_LANG); ?></h4>
            </div>
            <div class="modal-body clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 node-info">
                    <div class="group-ten-to-chuc">
                        <div class="input-group group-ten">
                            <span class="input-group-addon input-dragable" id="ten-to-chuc">Tên tổ chức:</span>
                            <input type="text" name="ten" class="form-control" aria-describedby="ten-to-chuc"
                                   placeholder="Tên tổ chức. Ví dụ: Công Ty, Tổng Công Ty, Khu Vực, Phòng A, Phòng B, ..." >
                        </div>
                    </div>
                    <div class="input-group group-chuc_danh-truong group-chuc_danh-item">
                        <div class="input-group cap_bac_truong">
                            <span class="input-group-addon" id="ten-cap_bac_truong">Cấp bậc trưởng:</span>
                            <select data-url="<?php echo admin_url('admin-ajax.php?action=chuc_danh_list&chuc_danh=Trưởng%|Tổng%|Giám%&cap_bac=CEO|PHONGBAN'); ?>"
                                    class="form-control ten-cap_bac" name="cap_bac_truong"
                                    data-old-value="" data-change-target="select#chuc_danh_truong"
                                    aria-describedby="ten-cap_bac_truong" id="cap_bac_truong" style="min-width: 100px"
                                    data-live-search="true" data-live-search-placeholder="CEO, Trưởng Phòng, Nhân Viên" >
                                <option value=""> -- Cấp Bậc --</option>
                                <?php $space = ''; foreach ($cap_bac as $value => $text):
                                    echo sprintf('<option value="%s">%s%s</option>', $value, $space, $text) . "\n";
                                    $space .= ' -- ';
                                    $space = str_replace('  ', ' ', $space);
                                endforeach; ?>
                            </select>
                        </div>
                        <div class="input-group chuc_danh_truong">
                            <span class="input-group-addon" id="ten-chuc_danh-truong">Tên chức vụ trưởng:</span>
                            <select data-url="<?php echo admin_url('admin-ajax.php?action=chuc_danh_list&chuc_danh=Trưởng%|Tổng%|Giám%&cap_bac=CEO|PHONGBAN'); ?>"
                                    class="form-control ten-chuc_danh" name="chuc_danh_truong" id="chuc_danh_truong"
                                    data-old-value="" data-change-target="input#ten_chuc_danh_truong_name"
                                    aria-describedby="ten-chuc_danh-truong" id="chuc_danh_truong"
                                    data-live-search="true" data-live-search-placeholder="Tên chức vụ trưởng. Ví dụ: Tổng giám đốc, Giám Đốc CTy A, Trưởng Phòng A, Trưởng Phòng B, ..." >
                                <option value=""> -- Chức Danh Trưởng --</option>
                                <?php foreach ($ds_chuc_danh as $truong):
                                    echo sprintf('<option value="%s" data-cap-bac="%s">%s</option>', $truong['ID'], $truong['cap_bac'], $truong['chuc_danh']) . "\n";
                                endforeach; ?>
                            </select>
                            <input name="chuc_danh_truong_old" value="" type="hidden">
                        </div>
                        <div class="input-group ten_chuc_danh_truong">
                            <span class="input-group-addon" id="ten_chuc_danh_truong">Người phụ trách:</span>
                            <input type="text" name="ten_chuc_danh_truong_name" class="form-control ten-nguoi-phu-trach" placeholder="Người phụ trách"
                                   aria-describedby="ten_chuc_danh_truong" readonly>
                            <a href="javascript:;" class="input-group-addon" data-toggle="modal" data-target="#tpl-user-list"
                               data-ten-chuc_danh="select.form-control.ten-chuc_danh"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                            <input type="hidden" class="nhan-vien-id" name="ten_chuc_danh_truong" value="">
                        </div>
                        <div class="input-group phan_quyen_chuc_danh_truong">
                            <span class="input-group-addon" id="phan_quyen_chuc_danh_truong">Được phân bổ KPI:</span>
                            <input type="checkbox" name="assign_task" value="1" class="form-control ten-nguoi-phu-trach" placeholder="Được phân bổ KPI"
                                   aria-describedby="phan_quyen_chuc_danh_truong" readonly>
                        </div>
                    </div>
                    <ul class="list-group group-chuc_danh-pho">
                        <li class="group-item input-label"><span>Tên các chức vụ phó:</span>
                            <a href="javascript:;" class="btn-add-chu-vu-pho"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                        </li>
                        <li class="group-item input-group group-chuc_danh-item item-template">
                            <input name="chuc_danh_pho[IDX][ID]" value="" type="hidden">
                            <div class="input-group cap_bac_pho">
                                <span class="input-group-addon" id="ten-cap_bac_pho_IDX">Cấp bậc trưởng:</span>
                                <select data-url="<?php echo admin_url('admin-ajax.php?action=chuc_danh_list&chuc_danh=Trưởng%|Tổng%|Giám%&cap_bac=CEO|PHONGBAN'); ?>"
                                        class="form-control ten-cap_bac" name="chuc_danh_pho[IDX][cap_bac]"
                                        data-old-value="" data-change-target="select#chuc_danh_pho_IDX_ten"
                                        aria-describedby="ten-cap_bac_pho_IDX" id="cap_bac_pho_IDX" style="min-width: 100px"
                                        data-live-search="true" data-live-search-placeholder="CEO, Trưởng Phòng, Nhân Viên" >
                                    <option value=""> -- Cấp Bậc --</option>
                                    <?php $space = ''; foreach ($cap_bac as $value => $text):
                                        echo sprintf('<option value="%s">%s%s</option>', $value, $space, $text) . "\n";
                                        $space .= ' -- ';
                                        $space = str_replace('  ', ' ', $space);
                                    endforeach; ?>
                                </select>
                            </div>
                            <div class="input-group chuc_danh_pho">
                                <span class="input-group-addon" id="ten-chuc_danh-pho-IDX">Chức vụ phó:</span>
                                <select data-url="<?php echo admin_url('admin-ajax.php?action=chuc_danh_list&chuc_danh=Phó%&cap_bac=CEO|PHONGBAN'); ?>"
                                        class="form-control ten-chuc_danh" name="chuc_danh_pho[IDX][ten]" id="chuc_danh_pho_IDX_ten"
                                        aria-describedby="ten-chuc_danh-pho-IDX" data-change-target="input#chuc_danh_pho_IDX_user_id_name"
                                        data-live-search="true" data-live-search-placeholder="Tên chức vụ phó. Ví dụ: Phó Tổng giám đốc, Phó Giám Đốc CTy A, Phó Phòng A, Phó Phòng B, ..." >
                                    <option value=""> -- Chức Danh Phó --</option>
                                    <?php foreach ($ds_chuc_danh as $pho):
                                        echo sprintf('<option value="%s" data-cap-bac="%s">%s</option>', $pho['ID'], $pho['cap_bac'], $pho['chuc_danh']) . "\n";
                                    endforeach; ?>
                                </select>
                                <a href="javascript:;" class="input-group-addon" data-toggle="modal" data-target="#confirm-dialog"
                                   data-title="Bạn có chắc chắn muốn xoá chức vụ phó này?"
                                ><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                <input name="chuc_danh_pho[IDX][ten_old]" value="" type="hidden">
                            </div>
                            <div class="input-group chuc_danh_pho">
                                <span class="input-group-addon" id="chuc_danh_pho_id-IDX">Người phụ trách:</span>
                                <input type="text" name="chuc_danh_pho[IDX][user]" id="chuc_danh_pho_IDX_user_id_name" class="form-control ten-nguoi-phu-trach"
                                       placeholder="Người phụ trách" aria-describedby="chuc_danh_pho_id-IDX" readonly>
                                <a href="javascript:;" class="input-group-addon" data-toggle="modal" data-target="#tpl-user-list"
                                   data-ten-chuc_danh="select.form-control.ten-chuc_danh"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                <input class="nhan-vien-id" type="hidden" name="chuc_danh_pho[IDX][user_id]" id="chuc_danh_pho_IDX_user_id" value="">
                                <input name="chuc_danh_pho[IDX][user_id_old]" value="" type="hidden">
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <button type="submit" class="btn btn-primary" data-create="Thêm" data-update="Lưu">Thêm</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </form>
</div>

<div id="tpl-user-list" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog"
     data-list-url="<?php echo admin_url('admin-ajax.php?action=list_users'); ?>">
    <form class="modal-dialog modal-md" action="" enctype="application/x-www-form-urlencoded"
          data-update-url="<?php echo admin_url('admin-ajax.php?action=user_role_update'); ?>"
          data-add-url="<?php echo admin_url('admin-ajax.php?action=user_role_add'); ?>"
          method="post" onsubmit="return false">
        <input type="hidden" name="role_id" value="">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" data-title="<?php _e('Chọn nhân viên cho chức danh ', TPL_DOMAIN_LANG); ?>"><?php _e('Chọn nhân viên cho chức danh ', TPL_DOMAIN_LANG); ?></h4>
            </div>
            <div class="modal-body clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group input-group input-group-select">
                        <label class="input-group-addon" for="year_id" id="users">Danh sách nhân viên: </label>
                        <select class="form-control users selectpicker" required aria-describedby="users" id="users_id" name="users" style="min-width: 100px"
                                data-live-search="true" data-live-search-placeholder="Chọn nhân viên ..." >
                            <option value=""> -- Chọn Nhân Viên -- </option>
                            <?php
                            foreach ($jsonUserData as $u):
                                echo sprintf('<option class="user" value="%d" data-cap-bac="%s" data-chuc-danh="%s">%s</option>', $u['user_id'], $u['cap_bac'], $u['chuc_danh'], $u['display_name']) . "\n";
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <button type="submit" class="btn btn-primaryy">Chọn</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Bỏ Qua</button>
                </div>
            </div>
        </div>
    </form>
</div>


