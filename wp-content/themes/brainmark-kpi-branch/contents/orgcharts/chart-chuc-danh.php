<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/19/18
 * Time: 09:56
 */
global $ds_chuc_danh, $cap_bac, $users, $jsonUserData;
?>
<div class="ds-chuc-danh">
    <form class="chuc-danh"
          data-crate-url="<?php echo admin_url('admin-ajax.php?action=chuc_danh_create&_wpnonce=' . wp_create_nonce('chuc_danh_create') ); ?>"
          data-update-url="<?php echo admin_url('admin-ajax.php?action=chuc_danh_update&_wpnonce=' . wp_create_nonce('chuc_danh_update')); ?>"
          enctype="application/x-www-form-urlencoded" method="post">
        <input type="hidden" name="ID" value="">
        <div class="input-group">
            <span class="input-group-addon" id="ten-chuc-danh">Tên chức danh</span>
            <input type="text" name="chuc_danh" class="form-control" placeholder="Tên chức danh" aria-describedby="ten-chuc-danh">
        </div>
        <div class="input-group">
            <span class="input-group-addon" id="cap-bac">Cấp Bậc</span>
            <select data-items="<?php esc_json_attr_e($cap_bac); ?>" class="form-control selectpicker" name="cap_bac" placeholder="Cấp Bậc" aria-describedby="cap-bac">
                <option value="">-- Cấp Bậc --</option>
                <?php foreach ($cap_bac as $value => $text): ?>
                    <option value="<?php echo $value; ?>"><?php echo $text; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="group-butotns">
            <div class="response-container"></div>
            <div class="btn-group" role="group" aria-label="...">
                <button type="submit" class="btn btn-primary" data-html-create="Tạo" data-html-save="Lưu">Lưu</button>
                <button type="reset" class="btn btn-default">Bỏ Qua</button>
                <button type="button" class="btn btn-default add-new">Thêm mới</button>
            </div>
        </div>
    </form>
    <ul class="list-group">
        <?php /* <li class="list-group-item"><a class="add-new" href="javascript:;">Thêm mới</a></li> */ ?>
        <li class="list-group-item clearfix hidden item-template" data-item="" data-id="">
            <span class="ten-chuc-danh col-lg-6 col-md-6"></span>
            <span class="ten-cap-bac col-lg-3 col-md-3"></span>
            <span class="commands col-lg-3 col-md-3">
                                    <a href="javascript:;" class="edit-chuc-danh" title="Chỉnh Sửa"><span class="fa fa-edit"></span></a>
                                    <a href="javascript:;" class="remove-chuc-danh" data-toggle="modal" data-target="#confirm-dialog"
                                       data-title="Bạn có chắc chắn muốn xoá chức danh này?"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                </span>
        </li>
        <?php
        foreach($ds_chuc_danh as $item): ?>
            <li class="list-group-item clearfix" data-item="<?php esc_json_attr_e($item); ?>" data-id="<?php echo $item['ID']; ?>">
                <span class="ten-chuc-danh col-lg-6 col-md-6"><?php echo $item['chuc_danh']; ?></span>
                <span class="ten-cap-bac col-lg-3 col-md-3"><?php
                    echo isset($cap_bac[$item['cap_bac']]) ? $cap_bac[$item['cap_bac']] : ''; ?></span>
                <span class="commands col-lg-3 col-md-3">
                                        <a href="javascript:;" class="edit-chuc-danh" title="Chỉnh Sửa"><span class="fa fa-edit"></span></a>
                                        <a href="javascript:;" class="remove-chuc-danh" data-toggle="modal" data-target="#confirm-dialog"
                                           data-title="Bạn có chắc chắn muốn xoá chức danh này?"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                    </span>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
