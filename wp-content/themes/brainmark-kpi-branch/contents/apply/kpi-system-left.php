<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 25/12/2017
 * Time: 22:41
 */

global $kpi_contents_parts;
$applyKpiSystem = $kpi_contents_parts['apply-bsc-kpi-system'];
# $applyKpiSystemPage = get_page_by_path(PAGE_APPLY_KPI_SYSTEM);
$applyKpiSystemUrl = $applyKpiSystem['url'];
$user = wp_get_current_user();
$fullName = $user->first_name .' '. $user->last_name;
$orgchart = user_load_orgchart($user);
$memberRoleName = !empty($orgchart) ? $orgchart->name : '';
$member_role = !empty($orgchart) ? $orgchart->role : '';
if( empty($member_role) ) {
    $member_role = '';
}
if( !in_array( $member_role, ['NHANVIEN'] ) ){
    require_once THEME_DIR . '/inc/lib-users.php';
    require_once THEME_DIR . '/inc/lib-orgchart.php';
    require_once THEME_DIR . '/inc/lib-kpis.php';
    require_once THEME_DIR . '/inc/lib-behavior.php';
    require_once THEME_DIR . '/ajax/get_user_info.php';
    $arrUsers = [];
    $htmlAmountPending = "";
    $htmlAmountWaiting = "";
    #$users = kpi_get_user_info(['id' => 0, 'single' => false, 'exclude' => []]);
	$chartChild = orgchart_get_all_kpi_by_parent( $orgchart->id );
	$arrCharts = [];
	if( !empty( $chartChild ) ){
		foreach ( $chartChild as $key => $item ){
			$arrCharts[] = $item['id'];
		}
	}
	$arrCharts = implode(", ", $arrCharts);
	$users = user_get_user_by_chart( $arrCharts );
    if( !empty( $users ) ) {
	    foreach ( $users as $key => $value ) {
		    $arrUsers[] = $value['ID'];
	    }
	    $arrUsers       = implode( ", ", $arrUsers );
	    $amountApproved = count_kpi_of_year_by_user_status_duyet( TIME_YEAR_VALUE, $arrUsers );
	    if ( ! empty( $amountApproved ) ) {
		    foreach ( $amountApproved as $k => $v ) {
			    if ( array_key_exists( 'status_type', $v ) ) {
				    if ( $v['status_type'] == 'pending' ) {
					    $amountPending = ! empty( $v['amount_status'] ) ? $v['amount_status'] : 0;
				    } elseif ( $v['status_type'] == 'waiting' ) {
					    $amountWaiting = ! empty( $v['amount_status'] ) ? $v['amount_status'] : 0;
				    }
			    }

		    }
	    }

	    $amountApproveBehavior     = behavior_count_of_year_by_user_status_duyet( TIME_YEAR_VALUE, $arrUsers );
	    $htmlAmountWaitingBehavior = '';
	    if ( ! empty( $amountApproveBehavior ) ) {
		    $amountWaitingBehavior     = ! empty( $amountApproveBehavior['amount_status'] ) ? $amountApproveBehavior['amount_status'] : 0;
		    $htmlAmountWaitingBehavior = ! empty( $amountWaitingBehavior ) ? "<span class='user-amount-pw amount-pending'>{$amountWaitingBehavior}</span>" : '';
	    }

	    if ( ! empty( $amountPending ) ) {
		    $htmlAmountPending = "<span class='user-amount-pw amount-pending'>{$amountPending}</span>";
	    }
	    if ( ! empty( $amountWaiting ) ) {
		    $htmlAmountWaiting = "<span class='user-amount-pw amount-pending'>{$amountWaiting}</span>";
	    }
    }
}
$type = !empty( $_GET['type'] ) ? $_GET['type'] : __('ket-qua-kpi', TPL_DOMAIN_LANG);
$nam = !empty( $_GET['nam'] ) ? $_GET['nam'] : '';
$quy = !empty( $_GET['quy'] ) ? $_GET['quy'] : '';
$thang = !empty( $_GET['thang'] ) ? $_GET['thang'] : '';
$quy = '';
$thang = '';
$_REQUEST['type'] = $_GET['type'] = $type;
$_REQUEST['nam'] = $_GET['nam'] = $nam;
$_REQUEST['quy'] = $_GET['quy'] = $quy;
$_REQUEST['thang'] = $_GET['thang'] = $thang;

$arrLeftMenu = [
    'dang-ky-kpi' => ['url' => add_query_arg(['type' => __('dang-ky-kpi', TPL_DOMAIN_LANG), 'nam' => $nam, 'quy' => $quy, 'thang' => $thang], $applyKpiSystemUrl), 'text' => __('Đăng ký KPI <br/><i style="font-size:12px;">(KPI registration)</i>', TPL_DOMAIN_LANG), 'not_allow_role' => [], 'amount' => '' ],
    'duyet-dang-ky' => ['url' => add_query_arg(['type' => __('duyet-dang-ky', TPL_DOMAIN_LANG), 'nam' => $nam, 'quy' => $quy, 'thang' => $thang], $applyKpiSystemUrl), 'text' => __('Duyệt đăng ký <br/><i style="font-size:12px;">(Approve of registration)</i> ', TPL_DOMAIN_LANG), 'not_allow_role' => ['nhanvien'], 'amount' => $htmlAmountPending ],
    'duyet-ket-qua' => ['url' => add_query_arg(['type' => __('duyet-ket-qua', TPL_DOMAIN_LANG), 'nam' => $nam, 'quy' => $quy, 'thang' => $thang], $applyKpiSystemUrl), 'text' => __('Duyệt kết quả <br/><i style="font-size:12px;">(Approve of result)</i> ', TPL_DOMAIN_LANG), 'not_allow_role' => ['nhanvien'], 'amount' => $htmlAmountWaiting ],
    'ket-qua-kpi' => ['url' => add_query_arg(['type' => __('ket-qua-kpi', TPL_DOMAIN_LANG), 'nam' => $nam, 'quy' => $quy, 'thang' => $thang], $applyKpiSystemUrl), 'text' => __('Kết quả KPI <br/><i style="font-size:12px;">(KPI result)</i>', TPL_DOMAIN_LANG), 'not_allow_role' => [], 'amount' => '' ],
    'kpi-nam' => ['url' => add_query_arg(['type' => __('kpi-nam', TPL_DOMAIN_LANG), 'nam' => $nam, 'quy' => $quy, 'thang' => $thang], $applyKpiSystemUrl), 'text' => __('KPI năm <br/><i style="font-size:12px">(KPI Year)</i>', TPL_DOMAIN_LANG), 'not_allow_role' => [], 'amount' => '' ],
];
if( user_is_manager() || $member_role == 'phongban' ){
	$arrLeftMenu['thai-do-hanh-vi'] = ['url' => add_query_arg(['type' => BEHAVIOR, 'nam' => $nam, 'quy' => $quy, 'thang' => $thang], $applyKpiSystemUrl), 'text' => __('Thái độ hành vi <br/><i style="font-size:12px">(Behaviour)</i>', TPL_DOMAIN_LANG), 'not_allow_role' => [], 'amount' => $htmlAmountWaitingBehavior ];
}
?>
<div class="col-md-2" id="dashboard-menu-wrap">
	<nav class="nav nav-left">
		<ul class="menuwrap">
            <?php
                foreach ($arrLeftMenu as $key => $item):
                    $checkManagers = ( in_array($key, ['duyet-dang-ky', 'duyet-ket-qua', 'kpi-nam']) && ( array_key_exists('user_managers',  $user->caps ) || array_key_exists('user_employers',  $user->caps ) ) ) ? true : false;
                    if( $checkManagers )
                        continue;
                    if( !in_array( $member_role, $item['not_allow_role'] ) ){
                        $active = !empty($type) && $type == $key ? "active" : '';
                        ?>
                        <li class="menu-item <?php echo "{$key}-item {$active}"; ?>">
                            <a <?php /*data-event="load-content"*/?> href="<?php echo esc_attr($item['url']); ?>"
                               class="<?php echo $key; ?>" data-type="<?php echo $key; ?>" title="">
                                <img src="<?php echo THEME_URL; ?>/assets/images/<?php echo $key; ?>.png"/>
                                <span class="title"><?php echo $item['text']; ?></span>
                            </a>
                            <?php echo $item['amount']; ?>
                        </li>
                        <?php
                    }
                endforeach;
            ?>
		</ul>
	</nav>
</div>
