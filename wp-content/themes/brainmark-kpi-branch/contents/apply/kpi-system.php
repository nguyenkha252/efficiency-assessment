<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 25/12/2017
 * Time: 23:40
 */
global $wpdb;
$type = __('ket-qua-kpi', TPL_DOMAIN_LANG);
if( !empty( $_GET['type'] ) ){
    $type = wp_slash( $_GET['type'] );
}
$member_role = $GLOBALS['member_role'];
$user = wp_get_current_user();

$tabsTitle = [
    'Finance' => __('Finance', TPL_DOMAIN_LANG),
    'Customer' => __('Customer', TPL_DOMAIN_LANG),
    'Execute' => __('Execute', TPL_DOMAIN_LANG),
    'CareerPath' => __('CareerPath', TPL_DOMAIN_LANG)
];
if( !IS_AJAX ) {
    ?>
    <div class="dashboard-content main-content apply-kpi-system <?php esc_attr_e($type); ?> <?php esc_attr_e("apply-for" . $member_role); ?>">
    <?php
}
    switch ($type):
        case __('ket-qua-kpi', TPL_DOMAIN_LANG):
            if( has_cap($user, USER_CAP_MANAGERS) || has_cap( $user, USER_CAP_EMPLOYERS ) ){
	            get_template_part( 'contents-user-managers/list', 'user' );
            }elseif( !empty($member_role) ) {
                # get_template_part('contents/content', 'result-kpi-target' . $member_role);
                # get_template_part('contents/content', 'result-kpi-dashboard-news' . $member_role);
                if( $member_role == 'phongban' ){
                    $currentUser = $GLOBALS['current_user'];
                    $room = $currentUser->orgchart->room;
                    $argsView = [
                        'type' => $_GET['type'],
                        'nam' => $_GET['nam'],
                        'quy' => TIME_PRECIOUS_VALUE,
                        'kqmyself' => true
                    ];

                    if( isset($_GET['kqmyself']) ) {
	                    get_template_part('contents/results/myself/kpi', 'target-' . $member_role);
	                    get_template_part('contents/results/myself/kpi', 'dashboard-news-' . $member_role);
	                    get_template_part('contents/results/myself/kpi', 'detail-' . $member_role);
                    }else{
	                    $urlView = add_query_arg($argsView, site_url());
	                    echo "<div class='viewkpiofroom'><a href='{$urlView}' title='KPI cá nhân'>KPI cá nhân</a></div>";
	                    get_template_part('contents/results/kpi', 'target-' . $member_role);
	                    get_template_part('contents/results/kpi', 'dashboard-news-' . $member_role);
	                    get_template_part('contents/results/kpi', 'detail-' . $member_role);
                    }
                }else{
	                get_template_part('contents/results/kpi', 'target-' . $member_role);
	                get_template_part('contents/results/kpi', 'dashboard-news-' . $member_role);
	                get_template_part('contents/results/kpi', 'detail-' . $member_role);
                }
            }


            break;
        case __('dang-ky-kpi', TPL_DOMAIN_LANG):
	        if( has_cap($user, USER_CAP_MANAGERS) || has_cap( $user, USER_CAP_EMPLOYERS ) ){
		        get_template_part( 'contents-user-managers/list', 'user' );
	        }elseif( !empty($member_role) ) {
                # echo "<pre>{$wpdb->last_query}\n{$wpdb->last_error}"; var_dump($yearInfo); echo "</pre>";
                if( isset($_GET['regmyself']) ){
	                get_template_part( 'contents/registers/myself/new-kpi', $member_role );
                }else {
	                get_template_part( 'contents/registers/new-kpi', $member_role );
                }
            }
            break;
        case __('duyet-dang-ky', TPL_DOMAIN_LANG):
            if( !empty($member_role) ) {
	            get_template_part( 'contents/confirms/registers/confirm-register', $member_role );
	            /*echo '<div id="confirm-register-kpi" class="confirm-register-kpi">';
                get_template_part( 'contents/content', 'confirm-register-kpi' . $member_role );
	            get_template_part( 'contents/content', 'register-kpi-detail' . $member_role );
	            echo '</div>';*/
                #<h2>Đang xây dựng...</h2>
            }
            break;
        case __('duyet-ket-qua', TPL_DOMAIN_LANG):
            if( !empty($member_role) ) {
                get_template_part('contents/confirms/results/confirm-result', $member_role);
            }
            break;
        case __('kpi-nam', TPL_DOMAIN_LANG):
	        if( !empty($member_role) && $member_role != 'nhanvien') {
		        get_template_part('contents/capacity/list', 'member-of');
	        }elseif( $member_role == 'nhanvien' ){
		        get_template_part('contents/capacity/capacity', $member_role);
            }
            break;
        case BEHAVIOR:
            if( user_is_manager() || $member_role == 'phongban' ){
	            get_template_part('contents/behavior/list', 'member-of');
            }
            break;
        default:
            if( !empty($member_role) ) {
                ?>
                <img src="<?php echo THEME_URL; ?>/assets/images/dashboard/dashboard-ket-qua-kpi.jpg"
                     style="width: 100%; ">
                <?php
            }
            break;
    endswitch;
    if( !IS_AJAX ):
        ?>
        <div id="confirm-apply-kpi" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
            <div class="modal-dialog modal-sm">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"><?php _e('Bạn có chắc chắn xóa mục tiêu hiện tại', TPL_DOMAIN_LANG); ?></h4>
                    </div>
                    <div class="modal-body">
                        <button type="button" class="btn-brb-default btn-primary btn-yes"><?php _e('Đồng ý', TPL_DOMAIN_LANG); ?></button>
                        <button type="button" data-dismiss="modal" class="btn-brb-default btn-cancel btn-no"><?php _e('Hủy', TPL_DOMAIN_LANG); ?></button>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>

            </div>
        </div>
        <div id="formula-popup" class="modal fade" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                        <h3 class="modal-title"><?php _e('Phương pháp đo', TPL_DOMAIN_LANG); ?></h3>
                    </div>
                    <div class="modal-body">
                        <div class="content-show-formula">
                            <div class="formula-title hidden"><label>Tiêu đề: </label><strong class="content"></strong></div>
                            <div class="formulas hidden"><label>Phương pháp đo: </label><span class="content"></span></div>
                            <div class="formula-note hidden"><label>Ghi chú phương pháp đo: </label><span class="content"></span></div>
                            <div class="formula-not-found hidden">
                                Không áp dụng công thức
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', TPL_DOMAIN_LANG); ?></button>
                    </div><!-- /.modal-content -->
                </div>
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
        <?php
    endif;