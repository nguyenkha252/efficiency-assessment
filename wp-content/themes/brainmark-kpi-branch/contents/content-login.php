<?php
$redirect_to = site_url();
?>
    <body class="login" style="background:url('<?php echo THEME_URL;?>/assets/images/bg_login.jpg') no-repeat 0 140px / 100% !important;">
        <!-- BEGIN LOGO -->
        <div class="logo">
          <h1 style="text-shadow: 1px 1px 1px #999;font-weight: 300;font-size: 30px;color: #000;">COMPETENCY SYSTEM</h1>
          <div class="copyright"> <?php if( is_multisite() ){
                  $blogID =  get_current_blog_id();
                  $blogDetailByID = get_blog_details($blogID);
                  echo $blogDetailByID->blogname;
              }else{
                  echo get_bloginfo('name');
              }
          ?>. </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="<?php echo site_url("/"); ?>" method="POST" enctype="application/x-www-form-urlencoded">
                <h3 class="form-title"><?php _e('Đăng nhập', TPL_DOMAIN_LANG); # ?></h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span><?php _e('Vui lòng nhập tài khoản hay mật khẩu của bạn.', TPL_DOMAIN_LANG); ?></span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9" for="user_login">Tài khoản hoặc email</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="<?php _e('Tài khoản hoặc email', TPL_DOMAIN_LANG); ?>" id="user_login" name="user_login" /> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9" for="user_password"><?php _e('Mật khẩu', TPL_DOMAIN_LANG); ?></label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="<?php _e('Mật khẩu', TPL_DOMAIN_LANG); ?>" id="user_password" name="user_password" /> </div>
                </div>
                <div class="form-group">
                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                        <input class="form-control-checkbox" value="forever" <?php isset($rememberme) ? checked( $rememberme ) : ''; ?> id="rememberme" name="remember" type="checkbox"> <?php _e( 'Ghi nhớ?', TPL_DOMAIN_LANG ); ?>
                        <span></span>
                    </label>
                    <button type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large"><?php _e('Đăng nhập', TPL_DOMAIN_LANG); ?></button>
                </div>



                <input type="hidden" name="redirect_to" value="<?php echo esc_attr($redirect_to); ?>" />
            </form>
            <!-- END LOGIN FORM -->
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <div class="copyright"> 2017 &copy; BrainMark System.
        </div>
        <!-- END COPYRIGHT -->
        <!--[if lt IE 9]>
        <script src="<?php echo THEME_URL; ?>/assets/global/plugins/respond.min.js"></script>
        <script src="<?php echo THEME_URL; ?>/assets/global/plugins/excanvas.min.js"></script>
        <script src="<?php echo THEME_URL; ?>/assets/global/plugins/ie8.fix.min.js"></script>
        <![endif]-->
        <?php wp_footer(); ?>

    </body>
<?php
