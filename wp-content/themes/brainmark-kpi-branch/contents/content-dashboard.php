<?php
if( !IS_AJAX ) {
	require_once THEME_DIR . '/ajax/get_post_info.php';
	$wpnonce_post_info = wp_create_nonce( 'post_info' );
# @TODO begin add tempalate
	require_once ABSPATH . '_files/dashboard/content-left.php';
}

require_once ABSPATH . '_files/dashboard/content.php';
# @TODO end add template
/*
<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
    <div class="forms hidden">
        <form onsubmit="return false;"
              action="<?php echo esc_attr( admin_url('admin-ajax.php?action=create_user&_wpnonce='.$nonce_create_user) ); ?>"
              method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_wpnonce" value="<?php echo esc_attr($wpnonce_post_info); ?>">
            <input type="hidden" name="action" value="save_post">
            <div class="title"><label for="title"><?php _e('Post Title', TPL_DOMAIN_LANG); ?></label><input name="post_title" id="title" value=""></div>
            <div class="content">
                <label for="content"><?php _e('Post Content', TPL_DOMAIN_LANG); ?></label>
                <?php
                $_wp_editor_expand = $_content_editor_dfw = false;
                wp_editor( '', 'content', array(
                    '_content_editor_dfw' => $_content_editor_dfw,
                    'drag_drop_upload' => true,
                    'tabfocus_elements' => 'content-html,save-post',
                    'editor_height' => 300,
                    'media_buttons' => false,
                    'default_editor' => 'html',
                    'tinymce' => array(
                        'resize' => false,
                        'wp_autoresize_on' => $_wp_editor_expand,
                        'add_unload_trigger' => false,
                        'wp_keep_scroll_position' => ! $is_IE,
                    ),
                ) ); ?>
            </div>
        </form>
    </div>
    <ul class="lastest-posts">
        <?php echo kpi_render_posts(); ?>
    </ul>
</div>
<!-- END PAGE HEADER-->
*/
?>