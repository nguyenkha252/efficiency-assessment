<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN THEME PANEL -->
        <!-- END THEME PANEL -->
        <h1 class="page-title"> Cơ cấu tổ chức công ty</h1>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="index.html">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Cơ cấu tổ chức công ty</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <div class="portlet sub-content ">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-gift"></i><h1>Cơ cấu tổ chức công ty</h1> </div>
            </div>
            <div class="portlet-body">
                <div id="orgChartContainer">
                    <div id="orgChart"></div>
                </div>
                <div id="consoleOutput"></div>
            </div>

        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
