<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/25/17
 * Time: 02:20
 */
?>
    <div class="lists">
        <h1><?php _e('List Members', TPL_DOMAIN_LANG); ?> <a href="<?php
            $url = esc_url( kpi_get_profile_url('new') );
            echo $url;
            ?>" class="btn btn-primary"><?php _e('Create New', TPL_DOMAIN_LANG); ?></a>
        </h1>
        
        <table class="user-list" cellpadding="0" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th class="id-col"><?php _e('ID', TPL_DOMAIN_LANG); ?></th>
                <th class="avatar-img"><?php _e('Avatar', TPL_DOMAIN_LANG); ?></th>
                <th class="name"><?php _e('Name', TPL_DOMAIN_LANG); ?></th>
                <th class="email"><?php _e('Email', TPL_DOMAIN_LANG); ?></th>
                <th class="login"><?php _e('Username', TPL_DOMAIN_LANG); ?></th>
                <th class="display_name"><?php _e('Display Name', TPL_DOMAIN_LANG); ?></th>
                <th class="position_name"><?php _e('Position Name', TPL_DOMAIN_LANG); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $jsonData = kpi_get_users_list();
            echo $jsonData['html'];
            ?>
            </tbody>

        </table>
    </div>
    <!-- /.lists -->
<?php
