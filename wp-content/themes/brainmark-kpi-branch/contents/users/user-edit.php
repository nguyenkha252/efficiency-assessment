<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/25/17
 * Time: 02:20
 */
global $wpdb;

require_once THEME_DIR . '/inc/lib-users.php';

$profile_id = $_GET['profile'];
$user = new WP_User($profile_id);
$referrer = str_replace('&', '%26', !empty($_REQUEST['_referrer']) ? $_REQUEST['_referrer'] : '' );
?>
<form class="user-edit-form user-form" action="<?php echo esc_attr( kpi_get_profile_url($profile_id) ); ?>" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="ID" value="<?php echo $user->ID; ?>">
    <input type="hidden" name="action" value="update_user">
    <input type="hidden" name="_referrer" value="<?php echo esc_attr($referrer); ?>">
    <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce('update_user'); ?>">

    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h4 class="title"><?php _e('Update Member', TPL_DOMAIN_LANG); ?></h4>
        </div>
        <?php
        do_action('edit_user_result');
        ?>
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="display_name"><?php _e('Display Name', TPL_DOMAIN_LANG); ?></label>
                <input required class="form-control placeholder-no-fix" type="text" autocomplete="off"
                       placeholder="<?php _e('Display Name', TPL_DOMAIN_LANG); ?>" id="display_name"
                       name="display_name" value="<?php echo esc_attr($user->display_name); ?>"/>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="user_email"><?php _e('Email', TPL_DOMAIN_LANG); ?></label>
                <input required class="email form-control placeholder-no-fix" type="text" autocomplete="off"
                       placeholder="<?php _e('Email', TPL_DOMAIN_LANG); ?>" id="user_email" name="user_email" value="<?php echo esc_attr($user->user_email); ?>"/>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="orgchart_id"><?php _e('Mã số nhân viên', TPL_DOMAIN_LANG); ?></label>
                <input required class="form-control placeholder-no-fix" type="text" autocomplete="off"
                       placeholder="<?php _e('Mã số nhân viên', TPL_DOMAIN_LANG); ?>" id="user_nicename"
                       name="user_nicename" value="<?php echo esc_attr($user->user_nicename); ?>"/>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="user_pass"><?php _e('Password', TPL_DOMAIN_LANG); ?></label>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off"
                       placeholder="<?php _e('Password', TPL_DOMAIN_LANG); ?>" id="user_pass" name="user_pass"/>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="re_user_pass"><?php _e('Re-Password', TPL_DOMAIN_LANG); ?></label>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off"
                       placeholder="<?php _e('Re-Password', TPL_DOMAIN_LANG); ?>" id="re_user_pass"
                       name="re_user_pass"/>
            </div>
            <p>Mật khẩu có ít nhất 4 ký tự</p>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group" data-group="orgcharttype">
                <label class="control-label visible-ie8 visible-ie9"
                       for="type_orgchart_id"><?php _e('Cấp', TPL_DOMAIN_LANG); ?></label>
                <?php
                # $all_charts = kpi_load_org_charts('query');
                $phong = kpi_get_orgchart_phongban($user->orgchart_id);
                $bgd = kpi_get_orgchart_bgd($user->orgchart_id);
                $groupTypes = [
                    CHART_ALIAS_MANAGER => CHART_ALIAS_MANAGER_TEXT,
                    CHART_ALIAS_DEPARTMENTS => CHART_ALIAS_DEPARTMENTS_TEXT
                ];
                $selected_val = (!empty($phong) ? CHART_ALIAS_DEPARTMENTS: (!empty($bgd) ? CHART_ALIAS_MANAGER : '') );
                # echo '<pre>'; var_dump(__LINE__, __FILE__, $user->orgchart_id, $selected_val, $bgd, $phong); echo '</pre>';
                ?>
                <select required data-chart-id="<?php echo $selected_val; ?>"
                        id="type_orgchart_id" class="selectpicker" data-live-search="true" data-target="#phongban_orgchart_id, #orgchart_id">
                    <?php
                    foreach ($groupTypes as $val => $text):
                        $selected = ($selected_val == $val) ? 'selected="selected"' : '';
                        echo sprintf('<option value="%s" %s>%s</option>', $val, $selected, $text);
                    endforeach;
                    ?>
                </select>
            </div>
            <div class="form-group <?php echo (!empty($bgd) ? 'hidden' : ''); ?>" data-group="phongban">
                <label class="control-label visible-ie8 visible-ie9"
                       for="phongban_orgchart_id"><?php _e('Phòng Ban', TPL_DOMAIN_LANG); ?></label>
                <select id="phongban_orgchart_id" data-chart-id="<?php echo (!empty($phong) ? $phong['id']: ''); ?>"
                        data-charts-tree="<?php esc_json_attr_e( orgchart_get_all() ); ?>"
                        class="selectpicker" data-live-search="true" data-target="#orgchart_id">
                    <option value=""> -- <?php _e('Chọn Phòng Ban', TPL_DOMAIN_LANG); ?> -- </option>
                    <?php
                    $departments_html = kpi_get_departments_dropdown(!empty($phong) ? $phong['id']: null, false );
                    echo $departments_html;
                    ?>
                </select>
            </div>
            <div class="form-group" data-group="name">
                <label class="control-label visible-ie8 visible-ie9"
                       for="orgchart_id"><?php _e('Chức danh', TPL_DOMAIN_LANG); ?></label>
                <select required name="orgchart_id" data-chart-id="<?php echo $user->orgchart_id; ?>" id="orgchart_id"
                        class="orgchart-dropdown selectpicker" data-live-search="true"
                        data-charts-tree="<?php esc_json_attr_e( orgchart_get_all() ); ?>">
                    <option value=""> -- <?php _e('Chọn Chức danh', TPL_DOMAIN_LANG); ?> -- </option>
                    <?php
                    $html = get_user_orgchart_dropdown( $user->orgchart_id, false, ['', CHART_ALIAS_GROUP, CHART_ALIAS_MANAGER, CHART_ALIAS_DEPARTMENTS, CHART_ALIAS_EMPLOYEES], 'name');
                    echo str_replace('--- ', '', $html);
                    ?>
                </select>
            </div>
            <div class="form-group">
                <div class="preview-avatar"><img
                            class="preview-avatar-image" onerror="this.src = this.getAttribute('data-src'); this.parentNode.className += ' hidden';"
                            src="<?php echo esc_attr($user->user_url); ?>"
                            data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                            style="max-width: 213px; min-height: 1px;"></div>
                <label class="control-label visible-ie8 visible-ie9"
                       for="avatar"><?php _e('Avatar', TPL_DOMAIN_LANG); ?></label>
                <!-- <input class="form-control placeholder-no-fix" type="file" autocomplete="off" placeholder="<?php _e('Avatar', TPL_DOMAIN_LANG); ?>" id="avatar" name="user_url" /> -->
                <div class="file-loading">
                    <input id="avatar" name="user_url" type="file" class="file"
                           placeholder="<?php _e('Avatar', TPL_DOMAIN_LANG); ?>" data-show-upload="false">
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="footer">
                <input type="hidden" name="bnt" value="save">
                <button type="button" onclick="location.href='<?php echo esc_attr( kpi_get_members_url() ); ?>'" class="btn btn-default"><?php _e('Back to members list', TPL_DOMAIN_LANG); ?></button>
                <button type="submit" class="btn btn-primary" onclick="this.form.bnt.value = 'save'"><?php _e('Save', TPL_DOMAIN_LANG); ?></button>
                <button type="submit" class="btn btn-primary" onclick="this.form.bnt.value = 'save_exit'"><?php _e('Save and Exit', TPL_DOMAIN_LANG); ?></button>
            </div>
        </div>
    </div>
</form><!-- /.user-edit-form -->
