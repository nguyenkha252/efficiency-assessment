<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/25/17
 * Time: 02:20
 */
global $wpdb;

require_once THEME_DIR . '/inc/lib-orgchart.php';

$url = kpi_get_profile_url('new');

$referrer = str_replace('&', '%26', !empty($_REQUEST['_referrer']) ? $_REQUEST['_referrer'] : '' );
?>
<form class="user-register-form user-form"
      action="<?php echo esc_attr( $url ); ?>"
      method="POST" enctype="multipart/form-data">
    <input type="hidden" name="ID" value="0">
    <input type="hidden" name="action" value="create_user">
    <input type="hidden" name="_referrer" value="<?php echo esc_attr($referrer); ?>">
    <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce('create_user'); ?>">


    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h4 class="title"><?php _e('Register Member', TPL_DOMAIN_LANG); ?></h4>
        </div>
        <?php
        do_action('create_user_result');
        ?>
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="display_name"><?php _e('Display Name', TPL_DOMAIN_LANG); ?></label>
                <input required class="form-control placeholder-no-fix" type="text" autocomplete="off"
                       placeholder="<?php _e('Display Name', TPL_DOMAIN_LANG); ?>" id="display_name"
                       name="display_name" value="<?php  ?>"/>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="user_email"><?php _e('Email', TPL_DOMAIN_LANG); ?></label>
                <input required class="email form-control placeholder-no-fix" type="text" autocomplete="off"
                       placeholder="<?php _e('Email', TPL_DOMAIN_LANG); ?>" id="user_email" name="user_email"/>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="orgchart_id"><?php _e('Mã số nhân viên', TPL_DOMAIN_LANG); ?></label>
                <input required class="form-control placeholder-no-fix" type="text" autocomplete="off"
                       placeholder="<?php _e('Mã số nhân viên', TPL_DOMAIN_LANG); ?>" id="user_nicename"
                       name="user_nicename" value=""/>
            </div>
            <?php
            $site_url = preg_replace('#^http(s|)://(www\.|)#', '', site_url());
            $parts = explode('.', $site_url);
            $user_pass = $parts[0] . '@123';
            ?>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="user_pass"><?php _e('Password', TPL_DOMAIN_LANG); ?></label>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" value="<?php echo $user_pass; ?>"
                       placeholder="<?php _e('Password', TPL_DOMAIN_LANG); ?>" id="user_pass" name="user_pass"/>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="re_user_pass"><?php _e('Lặp lại mật khẩu', TPL_DOMAIN_LANG); ?></label>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" value="<?php echo $user_pass; ?>"
                       placeholder="<?php _e('Lặp lại mật khẩu', TPL_DOMAIN_LANG); ?>" id="re_user_pass"
                       name="re_user_pass"/>
            </div>
            <p>Mật khẩu có ít nhất 4 ký tự</p>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group" data-group="orgcharttype">
                <label class="control-label visible-ie8 visible-ie9"
                       for="type_orgchart_id"><?php _e('Cấp', TPL_DOMAIN_LANG); ?></label>
                <select required id="type_orgchart_id" class="selectpicker" data-live-search="true" data-target="#phongban_orgchart_id, #orgchart_id">
                    <?php
                    $groupTypes = [
                        CHART_ALIAS_MANAGER => CHART_ALIAS_MANAGER_TEXT,
                        CHART_ALIAS_DEPARTMENTS => CHART_ALIAS_DEPARTMENTS_TEXT
                    ];
                    foreach ($groupTypes as $val => $text):
                        echo sprintf('<option value="%s">%s</option>', $val, $text);
                    endforeach;
                    ?>
                </select>
            </div>
            <div class="form-group" data-group="phongban">
                <label class="control-label visible-ie8 visible-ie9"
                       for="phongban_orgchart_id"><?php _e('Phòng Ban', TPL_DOMAIN_LANG); ?></label>
                <select id="phongban_orgchart_id" class="selectpicker"
                        data-charts-tree="<?php esc_json_attr_e( orgchart_get_all() ); ?>"
                        data-live-search="true" data-target="#orgchart_id">
                    <option value=""> -- <?php _e('Chọn Phòng Ban', TPL_DOMAIN_LANG); ?> -- </option>
                    <?php
                    $departments_html = kpi_get_departments_dropdown(!empty($phong) ? $phong['id']: null );
                    echo $departments_html;
                    ?>
                </select>
            </div>
            <div class="form-group" data-group="name">
                <label class="control-label visible-ie8 visible-ie9"
                       for="orgchart_id"><?php _e('Chức danh', TPL_DOMAIN_LANG); ?></label>
                <select required name="orgchart_id" id="orgchart_id" class="orgchart-dropdown selectpicker" data-live-search="true"
                        data-charts-tree="<?php esc_json_attr_e( orgchart_get_all() ); ?>">
                    <option value=""> -- <?php _e('Chọn Chức danh', TPL_DOMAIN_LANG); ?> -- </option>
                    <?php
                    $html = get_user_orgchart_dropdown( null, false, ['', CHART_ALIAS_GROUP, CHART_ALIAS_MANAGER, CHART_ALIAS_DEPARTMENTS, CHART_ALIAS_EMPLOYEES], 'name');
                    echo str_replace('--- ', '', $html);
                    ?>
                </select>
            </div>
            <div class="form-group">
                <div class="preview-avatar"><img
                            class="preview-avatar-image" onerror="this.src = this.getAttribute('data-src'); this.parentNode.className += ' hidden';"
                            src=""
                            data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                            style="max-width: 213px; min-height: 1px;"></div>
                <label class="control-label visible-ie8 visible-ie9"
                       for="avatar"><?php _e('Avatar', TPL_DOMAIN_LANG); ?></label>
                <!-- <input class="form-control placeholder-no-fix" type="file" autocomplete="off" placeholder="<?php _e('Avatar', TPL_DOMAIN_LANG); ?>" id="avatar" name="user_url" /> -->
                <div class="file-loading">
                    <input id="avatar" name="user_url" type="file" class="file"
                           placeholder="<?php _e('Avatar', TPL_DOMAIN_LANG); ?>" data-show-upload="false">
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="footer">
                <button type="button" onclick="location.href='<?php echo esc_attr( kpi_get_members_url() ); ?>'" class="btn btn-default"><?php _e('Back to members list', TPL_DOMAIN_LANG); ?></button>
                <button type="reset" class="btn btn-default"><?php _e('Reset', TPL_DOMAIN_LANG); ?></button>
                <button type="submit" class="btn btn-primary"><?php _e('Create', TPL_DOMAIN_LANG); ?></button>
            </div>
        </div>
    </div>
</form><!-- /.user-register-form -->