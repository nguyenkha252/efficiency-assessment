<?php
/**
 * Created by PhpStorm.
 * User: Heckman
 * Date: 12/25/17
 * Time: 02:20
 */

$user = wp_get_current_user();
$orgchart = user_load_orgchart($user);
$member_role = $orgchart ? $orgchart->role : '';
if( empty($member_role) ) {
    $member_role = '';
} else {
    $member_role = strtolower($member_role);
}

require_once THEME_DIR . '/inc/lib-users.php';

$tim = !empty($_REQUEST['tim']) ? strip_tags($_REQUEST['tim']) : '';
$room_id = $_REQUEST['room_id'] = $_GET['room_id'] = !empty($_GET['room_id']) ? (int)$_GET['room_id'] : '';
$orgchart_id = $_REQUEST['chart_id'] = $_GET['chart_id'] = !empty($_GET['chart_id']) ? (int)$_GET['chart_id'] : '';
$result = kpi_get_users($_REQUEST);
$users = $result['items'];
$total = $result['total'];
$trang = $result['trang'];
$limit = $result['limit'];
global $wpdb;
$profile = get_page_by_path( PAGE_PROFILE_PATH );
$members = get_page_by_path( PAGE_MANAGE_MEMBERS );
$profile_url = apply_filters( 'the_permalink', get_permalink( $profile ), $profile );
$current_url = apply_filters( 'the_permalink', get_permalink( $members ), $members );
# echo '<pre>'; var_dump(__LINE__, __FILE__, $user->orgchart_id, $result['sql'], $result['error'], $users[0]); echo '</pre>';

# echo '<pre>';
# global $wp_the_query;
# var_dump($_SERVER['REQUEST_URI'], $_GET, $wp_the_query);
# echo '</pre>';
$referrer = str_replace('&', '%26', urlencode($_SERVER['REQUEST_URI']) );
$charts = kpi_get_list_org_charts($user->orgchart_id, true);

$orgChartChildren = orgchart_get_ids_all_kpi_by_parent_not_nv($orgchart->id);
$listOrgchart = [];
if(!empty($orgChartChildren)){
    $ids = implode(",", $orgChartChildren);
    $listOrgchart = orgchart_get_room_by_ids($ids);
}
?>
    <div class="lists container-fluid">
        <h1 class="col-md-4" style="margin: 0;padding: 0;margin-bottom: 30px;margin-top: 20px; width: 100%">
          <?php //_e('List Members', TPL_DOMAIN_LANG); ?>
          Danh sách nhân viên
          <br/> <i style="font-size:13px;">(Resource management)</i>
          <a style="margin-top: -15px;float: right;"
            href="<?php
            $url = esc_url( add_query_arg('_referrer',  $referrer, kpi_get_profile_url('new') ) );
            echo $url;
            ?>" class="btn btn-primary"><?php //_e('Create New', TPL_DOMAIN_LANG); ?>
            Thêm mới <i style="font-size:13px; font-weight:100;">(Add new)</i>
          </a>

        </h1>
        <form class="form-user-filter col-md-10" style="margin: 0;padding: 0;" action="<?php
        echo add_query_arg(['trang' => $trang, 'limit1' => $limit, 'tim' => $tim], $current_url);
        ?>" method="get" enctype="application/x-www-form-urlencoded">
            <div class="col-md-3" style="margin: 0;padding: 0;">
                <div class="input-group">
                    <label class="input-group-addon" for="room_id" id="room_id-id">Phòng: <br/> <i style="font-size:13px; color:#555; font-weight:100;">(Group)</i></label>
                    <select name="room_id" id="room_id" aria-describedby="room_id-id" class="orgchart-dropdown selectpicker" data-live-search="true">
                        <option value=""> -- <?php _e('Lọc theo phòng', TPL_DOMAIN_LANG); ?> -- </option>
                        <?php
                        foreach($listOrgchart as $k => $chart){
                            if( $chart->alias != 'phongban' )
                                continue;
                            $json = esc_json_attr($chart);
                            echo sprintf('<option data-level="%d" value="%d" %s data-info="%s" >%s</option>'."\n",
                                $chart->level, $chart->id, (( ($chart->id == $room_id)) ? 'selected="selected"' : ''), $json, $chart->room);
                        }
                        #$departments_html = kpi_get_departments_dropdown($room_id);
                        #echo $departments_html;
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group">
                    <label class="input-group-addon" id="chart_id-id">Chức danh:<br/> <i style="font-size:13px; color:#555; font-weight:100;">(Role)</i></label>
                    <select name="chart_id" id="chart_id" aria-describedby="chart_id-id" class="orgchart-dropdown selectpicker" data-live-search="true">
                        <option value=""> -- <?php _e('Lọc theo chức danh', TPL_DOMAIN_LANG); ?> -- </option>
                        <?php
                        get_user_orgchart_dropdown( $orgchart_id, true);
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-5">
                <div class="input-group">
                    <input class="form-control" type="text" name="tim" placeholder="Tìm theo tên, email" value="<?php echo $tim; ?>" aria-describedby="tim-id">
                    <span class="input-group-addon" id="tim-id">
                        <button type="submit" class="btn btn-default">Lọc Nhân Viên <i style="font-size:13px; font-weight:100;">(Fillter)</i></button>
                    </span>
                </div>
            </div>
            <input type="hidden" name="trang" value="1">
            <input type="hidden" name="limit" value="<?php echo $limit; ?>">
            <input type="hidden" name="chinhxac" value="0">
        </form>
        <table class="user-list" cellpadding="0" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th class="id-stt"><?php _e('No.', TPL_DOMAIN_LANG); ?></th>
                <th class="id-col">
                  <?php //_e('Mã Số NV', TPL_DOMAIN_LANG); ?>
                  Mã Số NV <br/><i style="font-size:13px; font-weight:100;">(Peronal ID)</i>
                </th>
                <th class="avatar-img">
                  <?php //_e('Avatar', TPL_DOMAIN_LANG); ?>
                  Ảnh <br/><i style="font-size:13px; font-weight:100;">(Avatar)</i>

                </th>
                <th class="display_name">
                  <?php //_e('Display Name', TPL_DOMAIN_LANG); ?>
                  Tên hiển thị <br/><i style="font-size:13px; font-weight:100;">(Display name)</i>
                </th>
                <th class="email">
                  <?php //_e('Email', TPL_DOMAIN_LANG); ?>
                  Thư điện tử <br/><i style="font-size:13px; font-weight:100;">(Email)</i>
                </th>
                <th class="login">
                  <?php //_e('Tập Đoàn/Công Ty/Phòng Ban', TPL_DOMAIN_LANG); ?>
                  Nhóm <br/><i style="font-size:13px; font-weight:100;">(Group)</i>
                </th>
                <th class="position_name">
                  <?php //_e('Position Name', TPL_DOMAIN_LANG); ?>
                  Chức vụ <br/><i style="font-size:13px; font-weight:100;">(Role)</i>
                </th>
                <th class="position_name">
                  <?php //_e('Cấp Trên', TPL_DOMAIN_LANG); ?>
                  Cấp trên <br/><i style="font-size:13px; font-weight:100;">(Manager)</i>
                </th>
                <?php if(user_is_manager()): ?>
                    <th class="action" style="width:85px">
                      <?php //_e('Thao Tác', TPL_DOMAIN_LANG); ?>
                      Thao Tác <br/><i style="font-size:13px; font-weight:100;">(Action)</i>
                    </th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            <?php
            if( !empty($users) ):
                $default_avatar = THEME_URL . '/assets/images/no-photo.jpg';
                $stt = 1;
                $remove_url = esc_url( admin_url('admin-ajax.php?action=user-remove&uid=USERID&_wpnonce='.
                    wp_create_nonce('remove_user') . '&_referrer=' . $referrer) );
                foreach($users as $user):
                    $edit_url = esc_url( add_query_arg( ['profile' => $user['ID'], '_referrer' => $referrer], $profile_url ) );
                    ?>
                    <tr class="user-row">
                        <td class="id-stt"><?php echo $stt; $stt ++;?></td>
                        <td class="id-col"><label><input class="id" type="checkbox" name="user_id[]" value="<?php echo $user['ID']; ?>"><span><?php echo $user['user_nicename']; ?></span></label></td>
                        <td class="avatar-img"><img class="avatar"
                                                    onerror="this.src = this.getAttribute('data-src');"
                                                    src="<?php echo empty($user['user_url']) ? $default_avatar : $user['user_url']; ?>"
                                                    data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                                    style="max-width: 32px; max-height: 32px"></td>
                        <td class="display_name"><?php echo $user['display_name']; ?></td>
                        <td class="email"><?php echo $user['user_email']; ?></td>
                        <td class="login"><?php echo $user['room']; ?></td>
                        <td class="position_name"><?php echo __($user['orgchart_name'], TPL_DOMAIN_LANG); ?></td>
                        <td class="parent_name"><?php echo __($user['orgchart_parent_name'], TPL_DOMAIN_LANG); ?></td>
	                <?php if(user_is_manager()): ?>
                        <td class="action">
                            <a href="<?php echo $edit_url; ?>" title="Chỉnh Sửa"><span class="fa fa-pencil"></span></a>
                            <a href="javascript:;" data-target="#confirm-popup" data-toggle="modal"
                               data-remove-element="tr.user-row" data-loading-element="tr.user-row"
                               data-url="<?php echo str_replace('USERID', $user['ID'], $remove_url); ?>"
                               data-type="post" data-data-type="json"
                               data-message="Bạn có muốn xóa nhân viên <?php echo esc_attr("\"{$user['display_name']}\"") ?>?"
                               title="Xóa"><span class="fa fa-trash-o"></span></a>
                        </td>
                    <?php endif; ?>
                    </tr>
                <?php
                endforeach;
            endif;
            ?>
            </tbody>

        </table>
        <?php
        $nav = new Paginator($total, $limit, $trang, "{$current_url}?trang=(:num)");
        $nav->setPreviousText('Trước');
        $nav->setNextText('Sau');
        echo $nav->toHtml();
        ?>
    </div>
    <!-- /.lists -->
<?php
