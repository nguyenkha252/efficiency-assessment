<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 12/28/17
 * Time: 4:26 PM
 */

require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-formulas.php';
require_once THEME_DIR . '/inc/lib-behavior.php';
$add_target = wp_create_nonce('add_member_target');
$departments = kpi_orgchart_get_list();
$departments = process_children($departments);
$user = wp_get_current_user();
$fullName = $user->first_name .' '. $user->last_name;
$orgchart = user_load_orgchart($user);
$memberRoleName = $orgchart->name;
#$year_list = kpi_get_year_list(0,KPI_YEAR_STATUS_PUBLISH);
$year_list = year_get_list_year_ceo();
#$firstYear = kpi_get_first_year(0, KPI_YEAR_STATUS_PUBLISH);
$firstYear = year_get_year_by_chart_id( $orgchart->id, TIME_YEAR_VALUE );
if( empty($firstYear) ){
    if(!empty($getKPI = get_kpi_by_year_orgchart_not_month_not_precious(TIME_YEAR_VALUE, $user->ID))){
        $yearID = $getKPI[0]['year_id'];
        $year = kpi_get_year_by_id($yearID);
        if (!empty($year)) {
            $firstYear = year_get_year_by_chart_id($year['chart_id'], TIME_YEAR_VALUE);
        }
    }
}
$percentUser = kpi_get_total_percent_by_user( $user->ID, $orgchart->id, TIME_YEAR_VALUE, 'no' );

#behavior
$year_list_behavior = year_get_list_year_behavior();
$firstYearBehavior = year_get_first_year_behavior( TIME_YEAR_VALUE );
if( empty( $firstYearBehavior ) && !empty(  $year_list_behavior) ){
	$firstYearBehavior = $year_list_behavior[0];
}
$year_id_behavior = !empty( $firstYearBehavior ) && !empty($firstYearBehavior['id']) ? $firstYearBehavior['id'] : 0;
$year_behavior = !empty( $firstYearBehavior ) && !empty($firstYearBehavior['year']) ? $firstYearBehavior['year'] : 0;
$arrApply_for = !empty( $firstYearBehavior ) && !empty($firstYearBehavior['apply_for']) ? maybe_unserialize($firstYearBehavior['apply_for']) : [];
if( empty($arrApply_for) ) {
    $arrApply_for = [];
}
if( !in_array( $orgchart->id, $arrApply_for ) ) {
	$resultBehavior = behavior_get_list_behavior_of_user( $orgchart->id, $user->ID, $year_id_behavior, TIME_YEAR_VALUE );
	if ( empty( TIME_PRECIOUS_VALUE ) ) {
		$resultBehavior = behavior_get_total_number_for_year( TIME_YEAR_VALUE, $user->ID, $orgchart->id );
		$resultBehavior = array_group_by( $resultBehavior, 'parent_1' );
	}
	$getBehavior = [];
	if ( empty( $resultBehavior ) ) {
		$getBehavior = behavior_get_list_behavior_of_admin_by_year( TIME_YEAR_VALUE, 0, KPI_STATUS_RESULT );
	}
	$groupBehavior = [];
	if ( ! empty( $resultBehavior ) ) {
		$groupBehavior = $resultBehavior[0];
		unset( $resultBehavior[0] );
	}
	$percentBehavior = $firstYearBehavior && ! empty( $firstYearBehavior['behavior_percent'] ) ? $firstYearBehavior['behavior_percent'] : 0;
}else{
	$percentBehavior = 0;
}
$tabs = [
    'target_work' => [
        'title' => __('Mục tiêu công việc', TPL_DOMAIN_LANG),
        'type' => 'target_work',
        'percent' => 100 - $percentBehavior,
        'img' => 'tai-chinh.png',
        'time_type' => 'MONTH',
        'time_value' => TIME_MONTH_VALUE,
        'year_id' => $firstYear
    ],
    'behavior' => [
        'title' => __('Thái độ hành vi', TPL_DOMAIN_LANG),
        'type' => 'behavior',
        'percent' => $percentBehavior,
        'img' => 'thai-do-hanh-vi-personal.png',
        'time_type' => 'MONTH',
        'time_value' => TIME_MONTH_VALUE,
        'year_id' => $year_id_behavior
    ],
    /*'comments' => [
        'title' => __('Nhận xét', TPL_DOMAIN_LANG),
        'type' => 'comments',
        'percent' => $firstYear && !empty($firstYear['comments']) ? $firstYear['comments'] : '',
        'img' => 'van-hanh.png',
        'time_type' => 'MONTH',
        'time_value' => TIME_MONTH_VALUE,
        'year_id' => $firstYear
    ],*/
];


$tabsGroup = [
    'finance' => [
        'title' => _TABS_Finance,
        'type' => 'Finance',
        'percent' => $firstYear ? $firstYear['finance'] : '',
        'img' => 'tai-chinh.png',
        'time_type' => 'PRECIOUS',
        'time_value' => TIME_PRECIOUS_VALUE,
        'year_id' => $firstYear
    ],
    'customer' => [
        'title' => _TABS_Customer,
        'type' => 'Customer',
        'percent' => $firstYear ? $firstYear['customer'] : '',
        'img' => 'customer.png',
        'time_type' => 'PRECIOUS',
        'time_value' => TIME_PRECIOUS_VALUE,
        'year_id' => $firstYear
    ],
    'operate' => [
        'title' => _TABS_Operate,
        'type' => 'Operate',
        'percent' => $firstYear ? $firstYear['operate'] : '',
        'img' => 'van-hanh.png',
        'time_type' => 'PRECIOUS',
        'time_value' => TIME_PRECIOUS_VALUE,
        'year_id' => $firstYear
    ],
    'development' => [
        'title' => _TABS_Development,
        'type' => 'Development',
        'percent' => $firstYear ? $firstYear['development'] : '',
        'img' => 'phat-trien.png',
        'time_type' => 'PRECIOUS',
        'time_value' => TIME_PRECIOUS_VALUE,
        'year_id' => $firstYear
    ]
];

if( empty( TIME_PRECIOUS_VALUE ) ){
    $tabsarr = ["'Finance'", "'Customer'", "'Operate'", "'Development'", "''"];
    $strTabs = implode( ", ", $tabsarr );
    $kpiForStaff = kpi_get_group_kpi_by_year_orgchart_for_year_by_lower_grade_nv($strTabs, TIME_YEAR_VALUE, $user->ID);
}else{
	if( !empty( TIME_MONTH_VALUE ) ){
		$kpiForStaff = get_kpi_by_year_orgchart_for_month_and_status($firstYear['year'], TIME_MONTH_VALUE, $user->ID);
	}else{
		$kpiForStaff = get_kpi_by_year_orgchart_for_precious_and_status($firstYear['year'], TIME_PRECIOUS_VALUE, $user->ID);
	}
}
?>
<div class="clearfix"></div>
<div class="block-detail block-result-kpi-detail">
    <?php
    #echo "<pre>";
    #print_r( $tabs );
    #echo "</pre>";
    ?>
    <ul id="" class="list-room-target-kpi clearfix nav nav-pills">
        <?php
        foreach ($tabs as $tabkey => $tabData):
            $cls = ('target_work' == $tabkey) ? 'active' : '';
            ?>
            <li class="room-target-kpi-item <?php echo $cls; ?> room-<?php echo $tabkey; ?>">
                <a href="#room-<?php echo $tabkey; ?>" class="" title="" data-toggle="tab">
                    <img src="<?php echo THEME_URL; ?>/assets/images/<?php echo $tabData['img']; ?>"/>
                    <?php echo $tabData['title']; ?>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
    <?php
        $kpi_for = '';
    ?>
    <div class="for-date">
        <div class="for-year">
            <label for="select-for-year"><?php echo _LBL_YEAR; //_e('Năm', TPL_DOMAIN_LANG); ?></label>
            <select class="select-for-year selectpicker" id="select-for-year" name="nam">
                <?php foreach ($year_list as $item):
                    if( empty($kpi_for) && $item['year'] == TIME_YEAR_VALUE ){
                        $kpi_for = $item['kpi_time'];
                    }
                    $selected = $item['year'] == TIME_YEAR_VALUE ? 'selected="selected"' : '';
                    echo sprintf('<option value="%s" %s>%s</option>', $item['year'], $selected, $item['year']);
                endforeach;
                ?>
            </select>
        </div>
        <?php if( $kpi_for == 'quy' ): ?>
            <div class="for-quarter">
                <label for="select-for-quarter">KPI</label>
                <select class="select-for-quarter selectpicker" id="select-for-quarter" name="">
                    <option value="0"><?php _e('Cả năm', TPL_DOMAIN_LANG); ?></option>
                    <?php
                    for ( $i = 1; $i <= 4; $i++ ){
                        $selected = $i == TIME_PRECIOUS_VALUE ? 'selected="selected"' : '';
                        echo sprintf('<option value="%s" %s>Quý %s</option>', $i, $selected, $i);
                    }
                    ?>
                </select>
            </div>
        <?php elseif( $kpi_for == 'thang' ): ?>
            <div class="for-quarter">
                <label for="select-for-quarter">KPI</label>
                <select class="select-for-month select-for-month-nv selectpicker" id="select-for-month" name="">
                    <option value="0"><?php _e( 'Cả năm', TPL_DOMAIN_LANG ); ?></option>
			        <?php
			        for ( $i = 1; $i <= 12; $i ++ ) {
				        $selected = $i == TIME_MONTH_VALUE ? 'selected="selected"' : '';
				        echo sprintf( '<option value="%s" %s>Tháng %s</option>', $i, $selected, $i );
			        }
			        ?>
                </select>
            </div>
        <?php endif; ?>
    </div>
    <div class="list-detail tab-content clearfix">
        <?php foreach ($tabs as $tabkey => $tabData):
            $cls = ('target_work' == $tabkey) ? 'active' : '';
            if( $tabkey == 'target_work' ):
            ?>
                <div class="tab-pane <?php echo $cls; ?>" id="room-<?php echo $tabkey; ?>">
                <div class="list-detail">
                    <div class="top-list-detail">
                        <h3 class="title">
                            <?php echo $tabData['title']; ?>
                            <a href="javascript:;" data-year="<?php echo esc_json_attr($tabData['year_id']); ?>" data-toggle="modal"
                               data-target="#tpl-ceo-kpi-year" >
                                <span class="icon-percent" contenteditable="false" data-kpi-percent="<?php echo $tabkey; ?>"><?php echo $tabData['percent']; ?>%</span>
                                <i class="glyphicon glyphicon-pencil hidden" aria-hidden="true"></i>
                            </a>
                        </h3>
                    </div>
                </div>
                <?php if( $tabkey == 'target_work' ): ?>
                    <?php #muc tieu cong việc ?>
                    <form data-update-result="" action="<?php echo admin_url('admin-ajax.php?action=personal_update_result'); ?>" class=""
                          id="form-<?php echo $tabkey; ?>" method="post" enctype="multipart/form-data"
                          onsubmit="return false">
                        <input value="<?php echo wp_create_nonce('personal_update_result'); ?>" type="hidden" name="_wpnonce">
                        <input value="<?php echo $tabData['type']; ?>" type="hidden" name="type">
                        <table class="table-list-detail table-managerment-target-kpi table-result-kpi-personal" cellpadding="0" cellspacing="0">
                            <thead>
                            <tr>
                                <th rowspan="2" class="column-1 kpi-id align-center"><?php _e('Mã KPI', TPL_DOMAIN_LANG); ?></th>
                                <th rowspan="2" class="column-2 kpi-content"><?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?></th>
                                <th rowspan="2" class="column-3 kpi-company_plan"><?php _e('Trọng số', TPL_DOMAIN_LANG); ?></th>
                                <th rowspan="2" class="column-4 kpi-department_plan"><?php _e('Kế hoạch', TPL_DOMAIN_LANG); ?> </th>
                                <th rowspan="2" class="column-5 kpi-unit align-center"><?php _e('Thực hiện', TPL_DOMAIN_LANG); ?></th>
                                <th rowspan="2" class="column-7 align-center"><?php _e('Hoàn thành', TPL_DOMAIN_LANG); ?></th>
                                <th rowspan="2" class="column-8 align-center"><?php _e('Kết quả', TPL_DOMAIN_LANG); ?></th>
                                <th rowspan="2" class="column-10 align-center"><?php _e('Chứng minh', TPL_DOMAIN_LANG); ?></th>
                                <th rowspan="2" class="column-11 align-center"><?php _e('Tình trạng', TPL_DOMAIN_LANG); ?></th>
                            </tr>

                            </thead>
                            <tbody>
                            <?php
                            $groupDatas = [];
                            if( $tabData['type'] == 'target_work' ){
                                foreach ($tabsGroup as $ktb => $valuetb){
                                    $groupDatas = $kpiForStaff;
                                }
                            }
                            if (!empty($groupDatas)):
                                foreach ($groupDatas as &$item):
                                    $arrPercentForDateTime = [];
                                    $plan = $item['plan'];
                                    $actual_gain = $item['actual_gain'];
                                    $planForYear = $item['plan_for_year'];
                                    $unit = $item['unit'];
                                    if( !empty( $item['formulas'] ) ){
                                        $formulas = maybe_unserialize( $item['formulas'] );
                                    }else{
                                        $formulas = [];
                                    }
                                    $percentForMonth = 0;
                                    $percentForYear = 0;
                                    //For Precious
                                    if( empty( TIME_PRECIOUS_VALUE ) ):
                                        $ttactual_gain = 0;
                                        if( $unit != KPI_UNIT_THOI_GIAN ) {
                                            if( $unit == KPI_UNIT_PERCENT ){
                                                # @TODO Note
                                                # đơn vị là % thì lấy tổng kết quả của các tháng chia cho số tháng nhập KPI
                                                # Các tháng có KPI trong mục đăng ký KPI tháng
                                                # Update feature on 01/10/2018
                                                # Link request: https://trello.com/c/CPoMsHN1/24-imagepng
                                                $total_actual_gain = kpi_unit_percent_sum_kpi_by_parent($item['id']);
                                            }else {
                                                $total_actual_gain = kpi_sum_kpi_by_parent($item['id']);
                                            }
                                            #print_r( $total_actual_gain );
                                            if ( ! empty( $total_actual_gain ) && array_key_exists( 'total_actual_gain', $total_actual_gain ) ) {
                                                $ttactual_gain = $total_actual_gain['total_actual_gain'];
                                            } else {
                                                $ttactual_gain = '';
                                            }
                                        }else {
	                                        $results = kpi_get_all_kpi_by_parent( $item['id'] );
	                                        if ( ! empty( $results ) ) {
		                                        $arrPercentForTime = [];
		                                        foreach ( $results as $ks => $vs ) {
			                                        $ttplan = $vs['plan'];
			                                        #$ttpercent     = $vs['percent'];
			                                        if ( $vs['unit'] == KPI_UNIT_THOI_GIAN ) {

				                                        if ( ! empty( $vs['actual_gain'] ) ) {
					                                        $arrPercentForDateTime[] = $vs['actual_gain'];
				                                        }
				                                        $arrPercentForTime[] = getPercentForMonth( $vs['unit'], $item['formula_type'], $formulas, $ttplan, $vs['actual_gain'] );
			                                        } else {
				                                        $ttactual_gain += $vs['actual_gain'];
			                                        }
		                                        }
		                                        if ( ! empty( $arrPercentForTime ) ) {
			                                        $ttactual_gain = array_sum( $arrPercentForTime ) / count( $arrPercentForTime );
		                                        }
	                                        } else {
		                                        $ttactual_gain = '';
	                                        }
                                        }
                                        #$ttactual_gain = getPercentForMonth( $unit, $item['formula_type'], $formulas, $plan, $actual_gain );

                                        ?>
                                        <tr>
                                            <td class="column-1 kpi-id align-center">
                                                <?php echo $item['id'];
                                                $classNode = getColorStar( $item['create_by_node'] );
                                                if( $item['required'] == 'yes' ): ?>
                                                    <i class="fa fa-star <?php esc_attr_e($classNode); ?>" aria-hidden="true"></i>
                                                <?php endif; ?>
                                            </td>
                                            <td class="column-2 kpi-content">
                                                <?php echo str_replace(str_split("\|"), "", $item['post_title']); ?>
	                                            <?php
	                                            $arrFormula = [
		                                            'html' => render_html_formula($item['formulas'], $item['formula_type']),
		                                            'title' => $item['formula_title'],
		                                            'note' => $item['note']
	                                            ]
	                                            ?>
                                                <a data-target="#formula-popup" data-toggle="modal" href="javascript:;" class="view-formula" data-formula="<?= esc_json_attr($arrFormula); ?>"><i class="fa fa-info-circle"></i></a>
                                            </td>
                                            <td class="column-3 kpi-company_plan"><?php echo $item['percent']; ?>%</td>
                                            <td class="column-4 kpi-department_plan"><?php esc_attr_e( $plan ); ?> <?php echo !empty( $item['unit'] ) ? getUnit($item['unit']) : ''; ?></td>
                                            <td class="column-5 kpi-unit align-center">

                                                <?php
                                                if( $ttactual_gain != '' && $item['unit'] != KPI_UNIT_THOI_GIAN ):
                                                    $percentForMonth = getPercentForMonth( $unit, $item['formula_type'], $formulas, $plan, $ttactual_gain );
                                                elseif( $item['unit'] == KPI_UNIT_THOI_GIAN ):
                                                    $percentForMonth = $ttactual_gain;
                                                endif;
                                                ?>
                                                <?php
                                                if( !in_array($item['status'], [KPI_STATUS_DONE, KPI_STATUS_WAITING] ) ):
                                                    echo $unit != KPI_UNIT_THOI_GIAN ? $ttactual_gain: implode( "<br> ", $arrPercentForDateTime );
                                                    ?>
	                                                <?php echo !empty( $item['unit'] ) ? getUnit($item['unit']) : ''; ?>
                                                <?php else: ?>
                                                    <?php esc_attr_e($unit != KPI_UNIT_THOI_GIAN ? $ttactual_gain : implode( "<br> ", $arrPercentForDateTime ) ); ?>
	                                                <?php echo !empty( $item['unit'] ) ? getUnit($item['unit']) : ''; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td colspan="" class="column-8 kpi-percent align-center">
                                                <?php echo round( $percentForMonth, 1 ); ?>%
                                            </td>
                                            <td class="column-8 align-center"><?php echo round($percentForMonth * $item['percent'] / 100, 1); ?>%</td>
                                            <?php /*<td class="column-9 align-center"><?php echo $percentForYear; ?></td> */ ?>
                                            <td class="column-10 align-center">

                                            </td>
                                            <td class="column-11 align-center">

                                            </td>
                                        </tr>
                                    <?php
                                    else:
                                        #For Month
                                    ?>

                                    <tr>
                                        <td class="column-1 kpi-id align-center">
	                                        <?php
	                                        $disabled = '';
	                                        $name = 'name="kpis['.$item['id'].'][ID]"';
	                                        $valueInput = $item['id'];
	                                        $nameCheck = 'name="kpis['.$item['id'].'][status]"';
	                                        $textSendApproved = '';
	                                        if( in_array( $item['status'], [KPI_STATUS_DONE] ) ){
		                                        $disabled = 'disabled';
		                                        $name ='';
		                                        $valueInput = '';
		                                        $nameCheck = '';
		                                        $textSendApproved = __('Đã duyệt', TPL_DOMAIN_LANG);
	                                        }elseif( $item['status'] == KPI_STATUS_WAITING ){
		                                        $disabled = 'disabled';
		                                        $name ='';
		                                        $valueInput = '';
		                                        $nameCheck = '';
		                                        $textSendApproved = __('Đã gửi', TPL_DOMAIN_LANG);
	                                        }
	                                        ?>
                                            <label class="switch">
                                                <input class="checkbox-status hidden" type="checkbox" <?= $nameCheck; ?> <?= $disabled; ?> <?php echo in_array( $item['status'], [ KPI_STATUS_WAITING, KPI_STATUS_DONE ] ) ? 'checked="checked"' : ''; ?> value="1"/>
                                                <span class="checkbox-slider"></span>
                                            </label>
                                            <input type="hidden" <?= $name; ?> value="<?= $valueInput; ?>" />
                                            <?php echo $item['id'];
                                            $classNode = getColorStar( $item['create_by_node'] );
                                            if( $item['required'] == 'yes' ): ?>
                                                <i class="fa fa-star <?php esc_attr_e($classNode); ?>" aria-hidden="true"></i>
                                            <?php endif; ?>
                                        </td>
                                        <td class="column-2 kpi-content">
                                            <?php echo str_replace(str_split("\|"), "", $item['post_title']); ?>
	                                        <?php
	                                        $arrFormula = [
		                                        'html' => render_html_formula($item['formulas'], $item['formula_type']),
		                                        'title' => $item['formula_title'],
		                                        'note' => $item['note']
	                                        ]
	                                        ?>
                                            <a data-target="#formula-popup" data-toggle="modal" href="javascript:;" class="view-formula" data-formula="<?= esc_json_attr($arrFormula); ?>"><i class="fa fa-info-circle"></i></a>
                                        </td>
                                        <td class="column-3 kpi-company_plan"><?php echo $item['percent']; ?>%</td>
                                        <td class="column-4 kpi-department_plan"><?php esc_attr_e( $plan ); ?> <?php echo !empty( $item['unit'] ) ? getUnit($item['unit']) : ''; ?></td>
                                        <td class="column-5 kpi-unit align-center">

                                                <?php
                                                    if( $actual_gain != '' ):
                                                        #$formulas = maybe_unserialize( $item['formulas'] );
                                                        $percentForMonth = getPercentForMonth( $item['unit'], $item['formula_type'], $formulas, $plan, $actual_gain );
                                                    endif;
                                                ?>
                                            <?php
                                            $receive = strtotime($item['receive']);
                                            $nowTime = time();
                                            $effective_time = $receive - $nowTime;
                                            if( !in_array($item['status'], [KPI_STATUS_DONE, KPI_STATUS_WAITING] ) && $effective_time >= 0 ):
                                            ?>
                                            <input type="text" name="resultKPI[<?= $item['id']; ?>][actual_gain]" value="<?= esc_attr($actual_gain); ?>" />
	                                        <?php echo !empty( $item['unit'] ) ? getUnit($item['unit']) : ''; ?>
                                            <input type="hidden" name="resultKPI[<?= $item['id']; ?>][ID]" value="<?= $item['id']; ?>" />
                                            <?php else: ?>
                                                <?php echo sprintf("<div class='notification error'>%s <strong>(%s)</strong></div>", __('Đã hết hạn nhập kết quả',TPL_DOMAIN_LANG), substr(kpi_format_date($item['receive']), 0, 10)) ?>
                                                <?php  esc_attr_e( $actual_gain ); ?>
	                                            <?php echo !empty( $item['unit'] ) ? getUnit($item['unit']) : ''; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td class="column-8 kpi-percent align-center">
                                            <?php
                                            echo round( $percentForMonth, 1 );
                                            ?>%
                                        </td>
                                        <td class="column-8 align-center"><?php echo round($percentForMonth * $item['percent'] / 100, 1); ?>%</td>
                                        <?php /*<td class="column-9 align-center"><?php echo $percentForYear; ?></td> */ ?>
                                        <td class="column-10 align-center">
                                            <?php
                                                if( !empty( $item['files'] ) ){
                                                    $files = maybe_unserialize( $item['files'] );
                                                    if( !empty( $files ) && !empty( $files['files_url'] ) ){
                                                        echo sprintf("<a href='javascript:;' data-action='%s' data-id='%s' class='view-file-upload' data-target='#tpl-personal-view-file' >%s</a>", admin_url('admin-ajax.php?action=load_file_upload&id='.$item['id']), $item['id'], __('Xem danh sách tập tin', TPL_DOMAIN_LANG));
                                                    }
                                                }
                                            if( !in_array($item['status'], [KPI_STATUS_DONE, KPI_STATUS_WAITING] ) ):
                                            ?>
                                            <input type="file" name="resultKPI[<?= $item['id']; ?>][files][]" id="resultKPI[<?= $item['id']; ?>][files]" class="inputfile" accept="image/*, .xls, .xlsx, .doc, .docx, .txt, .zip, .rar" data-error-type="<?php _e('Vui lòng tải tập tin đúng định dạng (jpg, png, jpeg, doc, docx, xlsx, xls, txt)'); ?>" data-error-size="<?php _e('Vui lòng tải tập tin < 5M', TPL_DOMAIN_LANG); ?>" data-multiple-caption="{count} <?= __('được chọn', TPL_DOMAIN_LANG); ?>" multiple />
                                            <label for="resultKPI[<?= $item['id']; ?>][files]"><span><i class="fa fa-upload" aria-hidden="true"></i> <?php _e('Chọn tập tin', TPL_DOMAIN_LANG); ?></span></label>
                                                <?php endif; ?>
                                        </td>
                                        <td class="column-11 align-center">
                                            <span><?= $textSendApproved; ?></span>
                                        </td>
                                    </tr>
                                <?php
                                        endif;
                                    endforeach;
                            endif;
                            ?>
                            </tbody>
                        </table>
                        <div class="action-bot">
                            <button type="submit" class="btn-brb-default btn-save btn-action-item"
                                    name="btn-save"><i class="fa fa-floppy-o"
                                                              aria-hidden="true"></i> <?php _e('Lưu', TPL_DOMAIN_LANG); ?>
                            </button>
                            <button type="button" data-method="post" data-action="<?php echo admin_url('admin-ajax.php?action=send_results'); ?>" class="btn-brb-default btn-save btn-action-item"
                                    name="btn-save-send"><i class="fa fa-floppy-o"
                                                       aria-hidden="true"></i> <?php _e('Gửi kết quả', TPL_DOMAIN_LANG); ?>
                            </button>
                            <button type="button" class="btn-brb-default btn-action-item" name="btn-print" data-target="#pp-option-export" data-toggle="modal"><i
                                        class="fa fa-print"
                                        aria-hidden="true"></i> <?php _e('Xuất báo cáo', TPL_DOMAIN_LANG); ?></button>
                        </div>
                    </form>
                    <div id="tpl-personal-view-file" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
                        <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
                              action="<?php echo admin_url('admin-ajax.php?action=update_upload_file') ?>" method="post">
                            <input type="hidden" name="id" value="">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"><?php _e('Danh sách tập tin tải lên', TPL_DOMAIN_LANG); ?></h4>
                                </div>
                                <div class="modal-body clearfix">
                                    <table class="table-list-files table-list-detail">
                                        <thead>
                                            <tr>
                                                <th class="column-1"><?php _e('STT', TPL_DOMAIN_LANG); ?></th>
                                                <th class="column-2"><?php _e('Tên tập tin', TPL_DOMAIN_LANG); ?></th>
                                                <th class="column-3"><?php _e('Chọn', TPL_DOMAIN_LANG); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody class="tbody-main">

                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <button type="submit" class="btn btn-cancel">Xoá</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div id="tpl-department-target-<?php echo $tabkey; ?>" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
                    <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
                          action="<?php echo admin_url('admin-ajax.php?action=add_department_target') ?>" method="post">
                        <input type="hidden" name="_wpnonce" value="<?php echo $add_target; ?>">
                        <input type="hidden" name="type" value="<?php echo $tabData['type']; ?>">
                        <input type="hidden" name="year_id" value="<?php echo $firstYear ? $firstYear['id'] : 0; ?>">
                        <input type="hidden" name="time_type" value="<?php echo $tabData['time_type']; ?>">
                        <input type="hidden" name="time_value" value="<?php echo $tabData['time_value']; ?>">
                        <input type="hidden" name="chart_id" value="<?php echo $orgchart->id; ?>">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"><?php echo $tabData['title']; ?>
                                    : Thêm KPI</h4>
                            </div>
                            <div class="modal-body clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group input-group input-group-select">
                                        <label class="input-group-addon" for="year_id" id="chart_name-<?php echo $tabkey; ?>">Chỉ mục KPI: </label>
                                        <select data-loading=".form-group.input-group.input-group-select" data-target="#list-kpis-<?php echo $tabkey; ?>" aria-describedby="chart_name-<?php echo $tabkey; ?>"
                                                class="kpi-category selectpicker input-select" name="cat"><?php
                                            kpiRenderCategoryDropdown('', true, '--- ');
                                            ?></select>
                                    </div>
                                    <div class="form-group input-group-list">
                                        <div class="department-container hidden"></div>
                                        <ul id="list-kpis-<?php echo $tabkey; ?>" class="list-group"></ul>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group form-plans">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="company_plan-<?php echo $tabkey; ?>">Nhập kế hoạch</span>
                                            <input type="text" name="company_plan" class="form-control"
                                                   placeholder="Kế hoạch công ty" aria-describedby="company_plan-<?php echo $tabkey; ?>">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="unit-<?php echo $tabkey; ?>">Đơn vị tính</span>
                                            <input type="text" name="unit" class="form-control"
                                                   placeholder="Đơn vị tính" aria-describedby="unit-<?php echo $tabkey; ?>">
                                        </div>
                                        <div class="input-group group-date">
                                                <span class="input-group-addon"
                                                      id="receive-<?php echo $tabkey; ?>">Thời điểm nhận kết quả</span>
                                            <input type="text" name="receive" class="form-control"
                                                   placeholder="Thời điểm nhận kết quả" data-lang="vi" data-format="<?php echo KPI_FORMAT_DATE; ?>"
                                                   data-min-date="01-01-<?php echo $tabData['time_value']; ?>"
                                                <?php /* data-max-date="" */ ?>
                                                   data-group-date=".group-date" data-timepicker="false"
                                                   data-btn-date=".input-group-addon.date-btn"
                                                   data-ctrl-date aria-describedby="receive-<?php echo $tabkey; ?>">
                                            <span class="input-group-addon date-btn">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="percent-<?php echo $tabkey; ?>">Trọng số tối đa</span>
                                            <input type="text" name="percent" class="form-control"
                                                   placeholder="Trọng số tối đa" aria-describedby="percent-<?php echo $tabkey; ?>">
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                    <div class="form-group phongban-list">
                                        <label class="control-label">Nhân viên:</label>
                                        <ul class="nhanvien">
                                            <?php
                                            if( !empty($departments_attrs) ):
                                            foreach ($departments_attrs as $department):
                                                echo '<li class="input-group">' .
                                                    '   <label class="input-group-addon switch department" id="department_' . $department['id'] . '_id-'.$tabkey.'">' .
                                                    '       <input class="checkbox-status" name="departments[' . $department['id'] . '][id]" value="' . $department['id'] . '" type="checkbox">' .
                                                    '       <span class="checkbox-slider round"></span>' .
                                                    '       <span class="text">' . $department['name'] . '</span>' .
                                                    '   </label>' .
                                                    '<input class="form-control" placeholder="Trọng số" aria-describedby="department_' . $department['id'] . '_id-'.$tabkey.'" ' .
                                                    'name="departments[' . $department['id'] . '][plan]" size="10" type="number" min="0" value="">' .
                                                    '<span class="input-group-addon">%</span>'.
                                                    '</li>';
                                            endforeach; endif; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <button type="submit" class="btn btn-primaryy">Lưu</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                    <?php else: #thai do hanh vi ?>
                    <form data-update-result="" action="<?php echo admin_url('admin-ajax.php?action=update_result_behavior'); ?>" class=""
                          id="form-<?php echo $tabkey; ?>" method="post" enctype="multipart/form-data"
                          onsubmit="return false">
                        <input value="<?php echo wp_create_nonce('update_result_behavior'); ?>" type="hidden" name="_wpnonce">
                        <input value="<?php echo $tabData['type']; ?>" type="hidden" name="type">
                        <table class="table-list-detail table-managerment-target-kpi table-result-kpi-personal" cellpadding="0" cellspacing="0">
                            <thead>
                            <tr>
                                <th rowspan="2" class="column-1 kpi-id align-center"><?php _e('Mã KPI', TPL_DOMAIN_LANG); ?></th>
                                <th rowspan="2" class="column-2 kpi-content"><?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?></th>
                                <th rowspan="2" class="column-3 kpi-company_plan"><?php _e('Trọng số', TPL_DOMAIN_LANG); ?></th>
                                <th rowspan="2" class="column-4 kpi-department_plan"><?php _e('Kế hoạch tháng', TPL_DOMAIN_LANG); ?> </th>
                                <th rowspan="2" class="column-5 kpi-unit align-center"><?php _e('Thực hiện', TPL_DOMAIN_LANG); ?></th>
                                <th rowspan="2" class="column-7 align-center"><?php _e('Hoàn thành', TPL_DOMAIN_LANG); ?></th>
                                <th rowspan="2" class="column-8 align-center"><?php _e('Kết quả', TPL_DOMAIN_LANG); ?></th>
                                <th rowspan="2" class="column-10 align-center"><?php _e('Chứng minh', TPL_DOMAIN_LANG); ?></th>
                                <th rowspan="2" class="column-11 align-center"><?php _e('Tình trạng', TPL_DOMAIN_LANG); ?></th>
                            </tr>

                            </thead>
                            <tbody>
			                <?php
			                $groupDatas = [];
			                if( $tabData['type'] == 'target_work' ){
				                foreach ($tabsGroup as $ktb => $valuetb){
					                $groupDatas = $kpiForStaff;
				                }
			                }
			                if (!empty($groupDatas)):
				                foreach ($groupDatas as &$item):
					                $arrPercentForDateTime = [];
					                $plan = $item['plan'];
					                $actual_gain = $item['actual_gain'];
					                $planForYear = $item['plan_for_year'];
					                $unit = $item['unit'];
					                if( !empty( $item['formulas'] ) ){
						                $formulas = maybe_unserialize( $item['formulas'] );
					                }else{
						                $formulas = [];
					                }
					                $percentForMonth = 0;
					                $percentForYear = 0;
					                if( empty( TIME_PRECIOUS_VALUE ) ):
						                $ttactual_gain = 0;
						                if( $unit != KPI_UNIT_THOI_GIAN ) {
							                $total_actual_gain = kpi_sum_kpi_by_parent( $item['id'] );
							                #print_r( $total_actual_gain );
							                if ( ! empty( $total_actual_gain ) && array_key_exists( 'total_actual_gain', $total_actual_gain ) ) {
								                $ttactual_gain = $total_actual_gain['total_actual_gain'];
							                } else {
								                $ttactual_gain = '';
							                }
						                }else {
							                $results = kpi_get_all_kpi_by_parent( $item['id'] );
							                if ( ! empty( $results ) ) {
								                $arrPercentForTime = [];
								                foreach ( $results as $ks => $vs ) {
									                $ttplan = $vs['plan'];
									                #$ttpercent     = $vs['percent'];
									                if ( $vs['unit'] == KPI_UNIT_THOI_GIAN ) {

										                if ( ! empty( $vs['actual_gain'] ) ) {
											                $arrPercentForDateTime[] = $vs['actual_gain'];
										                }
										                $arrPercentForTime[] = getPercentForMonth( $vs['unit'], $item['formula_type'], $formulas, $ttplan, $vs['actual_gain'] );
									                } else {
										                $ttactual_gain += $vs['actual_gain'];
									                }
								                }
								                if ( ! empty( $arrPercentForTime ) ) {
									                $ttactual_gain = array_sum( $arrPercentForTime ) / count( $arrPercentForTime );
								                }
							                } else {
								                $ttactual_gain = '';
							                }
						                }
						                #$ttactual_gain = getPercentForMonth( $unit, $item['formula_type'], $formulas, $plan, $actual_gain );
						                ?>
                                        <tr>
                                            <td class="column-1 kpi-id align-center">
	                                            <?php
	                                            $disabled = '';
	                                            $name = 'name="kpis['.$item['id'].'][ID]"';
	                                            $valueInput = $item['id'];
	                                            $nameCheck = 'name="kpis['.$item['id'].'][status]"';
	                                            $textSendApproved = '';
	                                            if( in_array( $item['status'], [KPI_STATUS_DONE] ) ){
		                                            $disabled = 'disabled';
		                                            $name ='';
		                                            $valueInput = '';
		                                            $nameCheck = '';
		                                            $textSendApproved = __('Đã duyệt', TPL_DOMAIN_LANG);
	                                            }elseif( $item['status'] == KPI_STATUS_WAITING ){
		                                            $disabled = 'disabled';
		                                            $name ='';
		                                            $valueInput = '';
		                                            $nameCheck = '';
		                                            $textSendApproved = __('Đã gửi', TPL_DOMAIN_LANG);
	                                            }
	                                            ?>
                                                <label class="switch">
                                                    <input class="checkbox-status hidden" type="checkbox" <?= $nameCheck; ?> <?= $disabled; ?> <?php echo in_array( $item['status'], [ KPI_STATUS_WAITING, KPI_STATUS_DONE ] ) ? 'checked="checked"' : ''; ?> value="1"/>
                                                    <span class="checkbox-slider"></span>
                                                </label>
                                                <input type="hidden" <?= $name; ?> value="<?= $valueInput; ?>" />
								                <?php echo $item['id'];
								                $classNode = getColorStar( $item['create_by_node'] );
								                if( $item['required'] == 'yes' ): ?>
                                                    <i class="fa fa-star <?php esc_attr_e($classNode); ?>" aria-hidden="true"></i>
								                <?php endif; ?>
                                            </td>
                                            <td class="column-2 kpi-content">
                                                <?php echo str_replace(str_split("\|"), "", $item['post_title']); ?>
	                                            <?php
	                                            $arrFormula = [
		                                            'html' => render_html_formula($item['formulas'], $item['formula_type']),
		                                            'title' => $item['formula_title'],
		                                            'note' => $item['note']
	                                            ]
	                                            ?>
                                                <a data-target="#formula-popup" data-toggle="modal" href="javascript:;" class="view-formula" data-formula="<?= esc_json_attr($arrFormula); ?>"><i class="fa fa-info-circle"></i></a>
                                            </td>
                                            <td class="column-3 kpi-company_plan"><?php echo $item['percent']; ?></td>
                                            <td class="column-4 kpi-department_plan"><?php esc_attr_e( $plan ); ?></td>
                                            <td class="column-5 kpi-unit align-center">

								                <?php
								                if( $ttactual_gain != '' && $item['unit'] != KPI_UNIT_THOI_GIAN ):
									                $percentForMonth = getPercentForMonth( $unit, $item['formula_type'], $formulas, $plan, $ttactual_gain );
                                                elseif( $item['unit'] == KPI_UNIT_THOI_GIAN ):
									                $percentForMonth = $ttactual_gain;
								                endif;
								                ?>
								                <?php
								                if( !in_array($item['status'], [KPI_STATUS_DONE, KPI_STATUS_WAITING] ) ):
									                echo $unit != KPI_UNIT_THOI_GIAN ? round( $percentForMonth, 1 ) : implode( "<br> ", $arrPercentForDateTime );
									                ?>
								                <?php else: ?>
									                <?php esc_attr_e($unit != KPI_UNIT_THOI_GIAN ? round( $percentForMonth, 1 ) : implode( "<br> ", $arrPercentForDateTime ) ); ?>
								                <?php endif; ?>
                                            </td>
                                            <td class="column-6 kpi-receive align-center"><?php echo getUnit($item['unit']); ?></td>
                                            <td colspan="" class="column-8 kpi-percent align-center">
								                <?php echo round( $percentForMonth, 1 ); ?>%
                                            </td>
                                            <td class="column-8 align-center"><?php echo round($percentForMonth * $item['percent'] / 100, 1); ?>%</td>
							                <?php /*<td class="column-9 align-center"><?php echo $percentForYear; ?></td> */ ?>
                                            <td class="column-10 align-center">

                                            </td>
                                            <td class="column-11 align-center">

                                            </td>
                                        </tr>
						                <?php
					                else:
						                #For Month
						                ?>

                                        <tr>
                                            <td class="column-1 kpi-id align-center">
	                                            <?php
	                                            $disabled = '';
	                                            $name = 'name="kpis['.$item['id'].'][ID]"';
	                                            $valueInput = $item['id'];
	                                            $nameCheck = 'name="kpis['.$item['id'].'][status]"';
	                                            $textSendApproved = '';
	                                            if( in_array( $item['status'], [KPI_STATUS_DONE] ) ){
		                                            $disabled = 'disabled';
		                                            $name ='';
		                                            $valueInput = '';
		                                            $nameCheck = '';
		                                            $textSendApproved = __('Đã duyệt', TPL_DOMAIN_LANG);
	                                            }elseif( $item['status'] == KPI_STATUS_WAITING ){
		                                            $disabled = 'disabled';
		                                            $name ='';
		                                            $valueInput = '';
		                                            $nameCheck = '';
		                                            $textSendApproved = __('Đã gửi', TPL_DOMAIN_LANG);
	                                            }
	                                            ?>
                                                <label class="switch">
                                                    <input class="checkbox-status hidden" type="checkbox" <?= $nameCheck; ?> <?= $disabled; ?> <?php echo in_array( $item['status'], [ KPI_STATUS_WAITING, KPI_STATUS_DONE ] ) ? 'checked="checked"' : ''; ?> value="1"/>
                                                    <span class="checkbox-slider"></span>
                                                </label>
                                                <input type="hidden" <?= $name; ?> value="<?= $valueInput; ?>" />
								                <?php echo $item['id'];
								                $classNode = getColorStar( $item['create_by_node'] );
								                if( $item['required'] == 'yes' ): ?>
                                                    <i class="fa fa-star <?php esc_attr_e($classNode); ?>" aria-hidden="true"></i>
								                <?php endif; ?>
                                            </td>
                                            <td class="column-2 kpi-content">
                                                <?php echo str_replace(str_split("\|"), "", $item['post_title']); ?>
	                                            <?php
	                                            $arrFormula = [
		                                            'html' => render_html_formula($item['formulas'], $item['formula_type']),
		                                            'title' => $item['formula_title'],
		                                            'note' => $item['note']
	                                            ]
	                                            ?>
                                                <a data-target="#formula-popup" data-toggle="modal" href="javascript:;" class="view-formula" data-formula="<?= esc_json_attr($arrFormula); ?>"><i class="fa fa-info-circle"></i></a>
                                            </td>
                                            <td class="column-3 kpi-company_plan"><?php echo $item['percent']; ?>%</td>
                                            <td class="column-4 kpi-department_plan"><?php esc_attr_e( $plan ); ?> <?php echo !empty( $item['unit'] ) ? getUnit($item['unit']) : ''; ?></td>
                                            <td class="column-5 kpi-unit align-center">

								                <?php
								                if( $actual_gain != '' ):
									                #$formulas = maybe_unserialize( $item['formulas'] );
									                $percentForMonth = getPercentForMonth( $item['unit'], $item['formula_type'], $formulas, $plan, $actual_gain );
								                endif;
								                ?>
								                <?php
                                                $receive = strtotime($item['receive']);
								                $effective_time = $receive - time();
								                if( !in_array($item['status'], [KPI_STATUS_DONE, KPI_STATUS_WAITING] ) && $effective_time >= 0 ):
									                ?>
                                                    <input type="text" name="resultKPI[<?= $item['id']; ?>][actual_gain]" value="<?= esc_attr($actual_gain); ?>" />
									                <?php echo !empty( $item['unit'] ) ? getUnit($item['unit']) : ''; ?>
                                                    <input type="hidden" name="resultKPI[<?= $item['id']; ?>][ID]" value="<?= $item['id']; ?>" />
								                <?php else: ?>
									                <?php  esc_attr_e( $actual_gain ); ?>
									                <?php echo !empty( $item['unit'] ) ? getUnit($item['unit']) : ''; ?>
								                <?php endif; ?>
                                            </td>
                                            <td class="column-8 kpi-percent align-center">
								                <?php
								                echo $percentForMonth;
								                ?>%
                                            </td>
                                            <td class="column-8 align-center"><?php echo round($percentForMonth * $item['percent'] / 100, 1); ?>%</td>
							                <?php /*<td class="column-9 align-center"><?php echo $percentForYear; ?></td> */ ?>
                                            <td class="column-10 align-center">
								                <?php
								                if( !empty( $item['files'] ) ){
									                $files = maybe_unserialize( $item['files'] );
									                if( !empty( $files ) && !empty( $files['files_url'] ) ){
										                echo sprintf("<a href='javascript:;' data-action='%s' data-id='%s' class='view-file-upload' data-target='#tpl-personal-view-file' >%s</a>", admin_url('admin-ajax.php?action=load_file_upload&id='.$item['id']), $item['id'], __('Xem danh sách tập tin', TPL_DOMAIN_LANG));
									                }
								                }
								                if( !in_array($item['status'], [KPI_STATUS_DONE, KPI_STATUS_WAITING] ) ):
									                ?>
                                                    <input type="file" name="resultKPI[<?= $item['id']; ?>][files][]" id="resultKPI[<?= $item['id']; ?>][files]" class="inputfile" accept="image/*, .xls, .xlsx, .doc, .docx, .txt, .zip, .rar" data-error-type="<?php _e('Vui lòng tải tập tin đúng định dạng (jpg, png, jpeg, doc, docx, xlsx, xls, txt)'); ?>" data-error-size="<?php _e('Vui lòng tải tập tin < 5M', TPL_DOMAIN_LANG); ?>" data-multiple-caption="{count} <?= __('được chọn', TPL_DOMAIN_LANG); ?>" multiple />
                                                    <label for="resultKPI[<?= $item['id']; ?>][files]"><span><i class="fa fa-upload" aria-hidden="true"></i> <?php _e('Chọn tập tin', TPL_DOMAIN_LANG); ?></span></label>
								                <?php endif; ?>
                                            </td>
                                            <td class="column-11 align-center">
                                                <span><?= $textSendApproved; ?></span>
                                            </td>
                                        </tr>
						                <?php
					                endif;
				                endforeach;
			                endif;
			                ?>
                            </tbody>
                        </table>
                        <div class="action-bot">
                            <button type="submit" class="btn-brb-default btn-save btn-action-item"
                                    name="btn-save"><i class="fa fa-floppy-o"
                                                       aria-hidden="true"></i> <?php _e('Lưu', TPL_DOMAIN_LANG); ?>
                            </button>
                            <button type="button" data-method="post" data-action="<?php echo admin_url('admin-ajax.php?action=send_results'); ?>" class="btn-brb-default btn-save btn-action-item"
                                    name="btn-save-send"><i class="fa fa-floppy-o"
                                                            aria-hidden="true"></i> <?php _e('Gửi kết quả', TPL_DOMAIN_LANG); ?>
                            </button>
                            <button type="button" class="btn-brb-default btn-action-item" name="btn-print" data-target="#pp-option-export" data-toggle="modal"><i
                                        class="fa fa-print"
                                        aria-hidden="true"></i> <?php _e('Xuất báo cáo', TPL_DOMAIN_LANG); ?></button>
                        </div>
                    </form>
                <?php endif; ?>
            </div>
            <?php else: ?>
                <div class="tab-pane" id="room-<?php echo $tabkey; ?>">
                    <form class="frm-add-manager frm-add-kpi-year" id="" method="post"
                          action="<?php echo admin_url( "admin-ajax.php?action=update_result_behavior" ); ?>">
                        <input type="hidden" name="year_id" value="<?= $year_id_behavior; ?>">
                        <input type="hidden" name="year" value="<?= $year_behavior; ?>">
                        <input type="hidden" name="_wp_http_referer"
                               value="<?php esc_attr_e( wp_unslash( $_SERVER['REQUEST_URI'] ) ); ?>#room-<?= $tabkey; ?>">
                        <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce( "update_result_behavior" ); ?>">
                        <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>

                        <div id="management-behavior" class="section-row">
                            <div class="list-detail">
                                <div class="top-list-detail top-list-detail-behavior">
                                    <div class="col-md-6">
                                        <h3 class="title">
							                <?php _e( 'Thái độ hành vi' ); ?>
                                            <span class="icon-percent" contenteditable="false" data-kpi-percent="finance"><?php echo $percentBehavior; ?>%</span>
                                        </h3>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
	                        <?php
	                        $urlParam = '';
	                        if(empty( $resultBehavior ) && !empty($getBehavior)) {
		                        $loadParams = [
			                        'action' => 'list_behavior_for_user',
			                        'year_id' => $year_id_behavior,
			                        'precious' => TIME_PRECIOUS_VALUE,
			                        'month' => TIME_MONTH_VALUE,
			                        'year' => TIME_YEAR_VALUE,
		                        ];
		                        $urlParam = add_query_arg($loadParams, admin_url("admin-ajax.php"));
	                        }
	                        ?>
                            <table class="table-list-detail table-managerment-behavior" cellpadding="0" cellspacing="0" data-action="<?= $urlParam; ?>">
                                <thead>
                                <tr>
                                    <th class="column-1 align-center"><?php _e( 'STT' ); ?></th>
                                    <th class="column-2"><?php _e( 'Hành vi' ); ?></th>
                                    <th class="column-3 align-center"><?php _e( 'Đánh giá / 1 lần <br> vi phạm' ); ?></th>
                                    <th class="column-4 align-center"><?php _e( 'Số lần <br> vi phạm' ); ?></th>
                                    <th class="column-5 width10 align-center"><?php _e( 'Tỷ lệ <br> bị trừ' ); ?></th>
                                    <th class="column-6 width10 align-center hidden status-behavior">
                                        Trạng thái
                                    </th>
                                </tr>
                                </thead>
				                <?php
				                $checkSendResult = false;
				                $checkColumnStatus = false;
				                if ( ! empty( $groupBehavior ) ):
					                $htmlGroup = '';
					                foreach ( $groupBehavior as $key => $itemGroup ):
						                $htmlItem = '';
						                $stt = 0;
						                $getItemBehavior = [];
						                if( array_key_exists( $itemGroup['id'], $resultBehavior ) ){
							                $getItemBehavior = $resultBehavior[$itemGroup['id']];
							                unset( $resultBehavior[$itemGroup['id']] );
						                }
						                foreach ( $getItemBehavior as $k => $item ) {
							                $stt ++;
							                $number = empty( TIME_PRECIOUS_VALUE ) ? $item['total_number'] : $item['number'];
							                $violation_assessment = $item['violation_assessment'];
							                $status = $item['status'];
							                $kqvp = $number * $violation_assessment;
							                if( !empty( TIME_PRECIOUS_VALUE ) && $status == KPI_STATUS_RESULT){
							                    $inputPercent = "<input class='width60 align-center' type='number' name='behavior[".$item['id']."][number]' value='".esc_attr($item['number'])."' />
                                            <input type='hidden' name='behavior[".$item['id']."][id]' value='".$item['id']."' />";
							                    if( !$checkSendResult ) {
								                    $checkSendResult = true;
							                    }
                                            }else{
							                    $inputPercent = $number;
                                            }
                                            if( !$checkColumnStatus && $status != KPI_STATUS_RESULT ){
	                                            $checkColumnStatus = true;
                                            }
							                $htmlStatus = 'Chưa duyệt';
							                if( $status == KPI_STATUS_WAITING ){
							                    $textStatus = "Chờ duyệt";
							                    $htmlStatus = "<span class=\"error notification\">{$textStatus}</span>";
                                            }elseif( $status == KPI_STATUS_DONE ){
								                $textStatus = "Đã duyệt";
								                $htmlStatus = "<span class=\"success notification\"><i class=\"fa fa-check-circle\"></i>{$textStatus}</span>";
                                            }
							                $htmlItem .= "
                                        <tr class='item-'>
                                        <td class=\"column-1 align-center\">{$stt}</td>
                                        <td class=\"column-2\">" . esc_attr( str_replace(str_split("\|"), "", $item['post_title']) ) . "</td>
                                        <td class=\"column-3 align-center\">" . esc_attr( $violation_assessment ) . "%</td>
                                        <td class=\"column-4 align-center\">
                                            {$inputPercent}
                                        </td>
                                        <td class=\"column-5 align-center\">
                                            ".esc_attr($kqvp)."%
                                        </td>
                                        <td class=\"column-6 width10 align-center hidden status-behavior\">$htmlStatus
                                        </td>
                                    </tr>
                                        ";
						                }
						                $actionDelGroup = "";
						                if( !empty( $htmlItem ) ){
							                $htmlItem = "<tbody>{$htmlItem}</tbody>";
						                }
						                $htmlGroup .= "
                                    <thead class='group-behavior-item'>
                                        <tr>
                                            <td class='column-1 align-center'>
                                                <a href='javascript:;' class='group-item-down'><i class=\"fa fa-chevron-circle-down fa-chevron-circle-up\"></i></a>
                                            </td>
                                            <td colspan='4' class='column-2'>".esc_attr(str_replace(str_split("\|"), "", $itemGroup['post_title']))."</td>
                                        </tr>
                                    </thead>
                                    {$htmlItem}
                                    ";
						                ?>
					                <?php endforeach; ?>
					                <?php echo $htmlGroup;  ?>
				                <?php endif; ?>
                            </table>
                        </div>

                        <div class="action-bot">
                            <button type="submit" class="btn-brb-default btn-primary btn-save-change" name="btn-save"><i
                                        class="fa fa-floppy-o" aria-hidden="true"></i> <?php _e( 'Lưu lại' ); ?></button>
                            <?php if( $checkSendResult ): ?>
                                <button data-method="post" data-action="<?php echo admin_url("admin-ajax.php?action=send_result_behavior"); ?>" type="button" class="btn-brb-default btn-primary" name="btn-save-send">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                    <?php _e( 'Lưu & gửi kết quả' ); ?>
                                </button>
                            <?php endif;
                                if($checkColumnStatus):
                            ?>
                                <span class="hidden" id="checksendresult"></span>
                            <?php endif; ?>
                            <button type="button" class="btn-brb-default btn-action-item" name="btn-print" data-target="#pp-option-export" data-toggle="modal"><i
                                        class="fa fa-print"
                                        aria-hidden="true"></i> <?php _e('Xuất báo cáo', TPL_DOMAIN_LANG); ?></button>
                        </div>
                    </form>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>


    <div id="pp-option-export" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
        <form class="modal-dialog modal-sm" enctype="application/x-www-form-urlencoded"
              action="<?php echo PAGE_EXPORTS_URL; ?>" method="get">
            <?php
            if(!empty($_GET)){
                foreach ($_GET as $k => $item){
                    if( $k == 'thang' && $item == '' )
                        $item = TIME_MONTH_VALUE;
                    if( $k == 'quy' && $item == '' )
                        $item = TIME_PRECIOUS_VALUE;
                    echo sprintf("<input name='%s' value='%s' type='hidden'/>", $k, $item);
                }
            }
            ?>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php _e('Tuỳ chọn Export', TPL_DOMAIN_LANG);?></h4>
                </div>

                <div class="modal-body clearfix">
                    <div class="col-md-12">
                        <div class="left-option">
                            <label class="switch">
                                <input class="checkbox-status" name="options" type="radio" value="all">
                                <span class="checkbox-slider round round-radio"></span>
                                <span><?php _e('Export tất cả', TPL_DOMAIN_LANG);?></span>
                            </label>
                            <label class="switch">
                                <input class="checkbox-status" checked name="options" type="radio" value="not-all">
                                <span class="checkbox-slider round round-radio"></span>
                                <span><?php _e('Không export những thái độ hành vi không vi phạm', TPL_DOMAIN_LANG);?></span>
                            </label>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <button type="submit" class="btn btn-primaryy"><?php _e('Export', TPL_DOMAIN_LANG);?></button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Đóng', TPL_DOMAIN_LANG);?></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
<?php
/**
 * <ul class="management-list">
<li class="management-item">
<a href="javascript:;"  data-id="room-target-kpi" title=""><?php _e('Mục tiêu KPI phòng'); ?></a>
</li>
<li class="management-item">
<a href="javascript:;" class="active" data-id="management-capacity" title=""><?php _e('Năng lực quản lý'); ?></a>
</li>
</ul>

<div id="room-target-kpi" data-content-management="" style="display: none">
<ul id="" class="list-room-target-kpi clearfix">
<li class="room-target-kpi-item room-finance">
<a href="javascript:;" class="active" title=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tai-chinh.png" /><?php _e('Tài chính'); ?></a>
</li>
<li class="room-target-kpi-item room-customer">
<a href="javascript:;" title="">
<img src="<?php echo get_template_directory_uri(); ?>/assets/images/customer.png" />
<?php _e('Khách hàng'); ?>
</a>
</li>
<li class="room-target-kpi-item room-operate">
<a href="javascript:;" title="">
<img src="<?php echo get_template_directory_uri(); ?>/assets/images/van-hanh.png" />
<?php _e('Vận hành'); ?>
</a>
</li>
<li class="room-target-kpi-item room-development">
<a href="javascript:;" title="">
<img src="<?php echo get_template_directory_uri(); ?>/assets/images/phat-trien.png" />
<?php _e('Phát triển'); ?>
</a>
</li>
</ul>
<div class="list-detail">
<div class="top-list-detail">
<h3 class="title"><?php _e('Mục tiêu công việc'); ?></h3>
<a href="javascript:;" title="" class="check-all" data-check-all=""><i class="fa fa-check" aria-hidden="true"></i> <?php _e('Đồng ý tất cả'); ?></a>
</div>
<form action="" class="" id="" method="">
<table class="table-list-detail table-managerment-target-kpi" cellpadding="0" cellspacing="0">
<thead>
<tr>
<th class="column-1 align-center"><?php _e('Mã KPI'); ?></th>
<th class="column-2"><?php _e('Nội dung mục tiêu'); ?></th>
<th class="column-3"><?php _e('Kế hoạch phòng'); ?></th>
<th class="column-4"><?php _e('Kế hoạch cá nhân'); ?></th>
<th class="column-5 align-center"><?php _e('ĐVT'); ?></th>
<th class="column-6 align-center"><?php _e('Thời điểm nhận kết quả'); ?></th>
<th class="column-7 align-center"><?php _e('Trọng số (%)'); ?></th>
<th class="column-8 align-center"><?php _e('Đồng ý'); ?></th>
</tr>
</thead>
<tbody>
<?php
for($i = 0; $i < 8; $i++):
$checked = $i % 2 == 0 ? 'checked' : '';
?>
<tr>
<td class="column-1 align-center">0001</td>
<td class="column-2">Thi phần đạt 20% tăng 5% sv 2017</td>
<td class="column-3">800.000.000.000</td>
<td class="column-4">2.000.000.000</td>
<td class="column-5 align-center">đ</td>
<td class="column-6 align-center">31-12-2018</td>
<td class="column-7 align-center">40</td>
<td class="column-8 align-center">
<input type="checkbox" <?= $checked; ?> class="cbx-status" id="checkbox-status-<?= $i; ?>" />
<label for="checkbox-status-<?= $i; ?>"></label>
</td>
</tr>
<?php
endfor;
?>

</tbody>
</table>
<div class="action-bot">
<button type="submit" class="btn-brb-default btn-save" name="btn-save"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php  _e('Save'); ?></button>
<button type="button" class="btn-brb-default btn-cancel"><i class="fa fa-trash" aria-hidden="true"></i> <?php _e('Cancel'); ?></button>
</div>
</form>
</div>
</div>
<div id="management-capacity" data-content-management="">
<div class="list-detail">
<div class="top-list-detail">
<h3 class="title">
<?php _e('Tiêu chí năng lực quản lý'); ?>
<span class="icon-percent">100%</span>
</h3>
</div>
</div>
<form class="" id="" method="" action="">
<table class="table-list-detail table-managerment-capacity" cellpadding="0" cellspacing="0">
<thead>
<tr>
<th class="column-1 align-center"><?php _e('Stt'); ?></th>
<th class="column-2"><?php _e('Chỉ tiêu mong đợi'); ?></th>
<th class="column-3 align-center"><?php _e('Cần cải thiện'); ?></th>
<th class="column-4 align-center"><?php _e('Hiệu quả'); ?></th>
<th class="column-5 align-center"><?php _e('Nổi bật'); ?></th>
<th class="column-6 align-center"><?php _e('Xu hướng'); ?></th>
<th class="column-7 align-center"><?php _e('Kết quả'); ?></th>
<th class="column-8 align-center"><?php _e('Ghi chú'); ?></th>
</tr>
</thead>
<tbody>
<?php
for($i = 0; $i < 2; $i++):
#$checked = $i % 2 == 0 ? 'checked' : '';
?>
<tr>
<td class="column-1 align-center">1</td>
<td class="column-2">TRÁCH NHIỆM: Mức độ thể hiện sự nổ lực để bắt đầu thực hiện công việc trong phạm vi trách nhiệm nhằm hoàn thành công việc theo yêu cầu trong phạm vi quyền hạn</td>
<td class="column-3 align-center">10%</td>
<td class="column-4 align-center">10%</td>
<td class="column-5 align-center">10%</td>
<td class="column-6 align-center"><input name="" type="text" value="21" class="txt-tendency" /></td>
<td class="column-7 align-center"><input name="" type="text" value="21" class="txt-result" /></td>
<td class="column-8 align-center">
<textarea name="" class="textarea-note"></textarea>
</td>
</tr>
<?php
endfor;
?>

</tbody>
</table>
<div class="managerment-capacity-feedback">
<div class="managerment-capacity-feedback-content">
<div class="capacity-trend">
<div class="title">
<h3><?php _e('Xu hướng năng lực'); ?></h3>
</div>
<div class="show-info-input">
<select class="selectpicker" name="">
<option value="1"><?php _e('Đi lên'); ?></option>
</select>
<div class="show-image"></div>
</div>
</div>
<div class="capacity-feedback-other">
<div class="title">
<h3><?php _e('Nhận xét khác'); ?></h3>
</div>
<div class="show-info-input">
<textarea name="" class="textarea-capacity-feedback"></textarea>
</div>
</div>
</div>
</div>
<div class="action-bot">
<button type="submit" class="btn-brb-default btn-save" name="btn-save"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php  _e('Save'); ?></button>
<button type="button" class="btn-brb-default btn-cancel"><i class="fa fa-trash" aria-hidden="true"></i> <?php _e('Cancel'); ?></button>
</div>
</form>
</div>
 */
