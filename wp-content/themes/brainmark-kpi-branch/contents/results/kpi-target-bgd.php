<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 26/12/2017
 * Time: 23:13
 */
$gradient_1 = Gradient( "ff1300", "ffe100" , 7 );
$gradient_2 = Gradient( "ffe100", "84e91c" , 7 );
$gradient_3 = Gradient( "84e91c", "84e91c" , 7 );

require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-formulas.php';
#$year_list = kpi_get_year_list(0, KPI_YEAR_STATUS_PUBLISH);
$year_list = year_get_list_year_ceo();
$firstYear = kpi_get_first_year(0, KPI_YEAR_STATUS_PUBLISH);
$tabs = ["'Finance'", "'Customer'", "'Operate'", "'Development'"];
$strTabs = !empty($tabs) ? implode( ", ", $tabs ) : '';
$user = wp_get_current_user();
$uID = 0;
if( array_key_exists( 'uid', $_GET ) ){
	$uID = (int)wp_slash( $_GET['uid'] );
}
if( $uID > 0 ){
	$user = kpi_get_user_by_id($uID);
}
#$orgChildren = orgchart_get_all_kpi_by_parent( !empty($orgchart) ? $orgchart->id : 0 );
$orgCty = orgchart_get_congty();
$arrOrgs = [];
if( !empty( $orgCty ) ){
	foreach ( $orgCty as $key => $value ){
		array_push($arrOrgs, $value['id'] );
	}
	$arrOrgs = implode(", ", $arrOrgs);
}
$orgchart = user_load_orgchart($user);
$groupDatas = kpi_get_group_kpi_by_year_orgchart_for_year_by_lower_grade_ceo( $strTabs, TIME_YEAR_VALUE, $orgchart->id, 'no', 'no' );

$arrTabs = kpi_get_total_percent_congty( $groupDatas );

$yearCEO = year_get_list_year_ceo_by_year( TIME_YEAR_VALUE );
$arrChart['Finance'] = !empty( $yearCEO ) ? $yearCEO['finance'] : 0;
$arrChart['Customer'] = !empty( $yearCEO ) ? $yearCEO['customer'] : 0;
$arrChart['Operate'] = !empty( $yearCEO ) ? $yearCEO['operate'] : 0;
$arrChart['Development'] = !empty( $yearCEO ) ? $yearCEO['development'] : 0;
$arrOutput = kpi_get_total_for_congty( $arrTabs, $arrChart );

$data = [];
$totalColorLoop  = 30;
for( $i = 0; $i<=$totalColorLoop; $i++ ){
    $data[$i] = 10;
}
$totalPercent = array_sum( $arrOutput['total_percent'] );
$percentItem = $totalPercent / 10;
$backgroundColor = render_color_chart($percentItem);
$dataSet = ['datasets' => [['backgroundColor' => $backgroundColor, 'data' => array_map('intval', $data)]], 'total' => $totalPercent ];
$kpi_for = '';
?>
<div class="col-md-6">
    <div id="block-target-kpi" class="block-shadow block-item border-radius-default ">
        <form action="get" data-chart-report="">
            <div class="header-target-kpi header-block add-english">
                <div class="for-date">
                    <div class="for-year" style="float:right;">
                        <label for="select-for-year"><?php echo _LBL_YEAR;//_e('Năm', TPL_DOMAIN_LANG); ?></label>
                        <select class="select-for-year selectpicker" id="select-for-year" name="">
						    <?php foreach ($year_list as $item):
							    if( empty($kpi_for) && $item['year'] == TIME_YEAR_VALUE ){
								    $kpi_for = $item['kpi_time'];
							    }
							    $selected = $item['year'] == TIME_YEAR_VALUE ? 'selected="selected"' : '';
							    echo sprintf('<option value="%s" %s>%s</option>', $item['year'], $selected, $item['year']);
						    endforeach;?>
                        </select>
                    </div>
	                <?php if( $kpi_for == 'quy' ): ?>
                        <div class="for-quarter">
                            <label for="select-for-quarter">Quý <br/> <span style="font-style:italic;color:#888;font-size: 15px;">Quarter</span></label>
                            <select class="select-for-quarter selectpicker" id="select-for-quarter" name="">
                                <option value="0"><?php _e('Cả năm', TPL_DOMAIN_LANG); ?></option>
				                <?php
				                for ( $i = 1; $i <= 4; $i++ ){
					                $selected = $i == TIME_PRECIOUS_VALUE ? 'selected="selected"' : '';
					                echo sprintf('<option value="%s" %s>Quý %s</option>', $i, $selected, $i);
				                }
				                ?>
                            </select>
                        </div>
	                <?php elseif( $kpi_for == 'thang' ): ?>
                        <div class="for-quarter">
                            <label for="select-for-quarter">Tháng <br/> <span style="font-style:italic;color:#888;font-size: 15px;">Month</span></label>
                            <select class="select-for-month select-for-month-nv selectpicker" id="select-for-month" name="">
                                <option value="0"><?php _e( 'Cả năm', TPL_DOMAIN_LANG ); ?></option>
				                <?php
				                for ( $i = 1; $i <= 12; $i ++ ) {
					                $selected = $i == TIME_MONTH_VALUE ? 'selected="selected"' : '';
					                echo sprintf( '<option value="%s" %s>Tháng %s</option>', $i, $selected, $i );
				                }
				                ?>
                            </select>
                        </div>
	                <?php endif; ?>
                </div>
                <div class="name-block">
                    <h2><?php echo _Objective_KPI; // _e('Mục tiêu KPI'); ?></h2>
                </div>
            </div>
            <div class="content-block content-block-target-kpi">
                <div class="report-target-kpi">
                    <div class="report-item">
                        <canvas data-type="derivedDoughnut" width="200" height="100" data-datasets="<?php echo str_replace('"', '&#34;', json_encode( $dataSet ) ); ?>" data-report="" id="canvas-report-kpi" class="data-report"></canvas>
                    </div>
                </div>
			    <?php
			    foreach ( $arrTabs['name'] as $k => $value ):
				    $nameTabs = $arrTabs['name'][$k];
				    $percentTabs = $arrTabs['total_percent'][$k];
				    $class = mb_strtolower( $k );
				    $percent = array_key_exists($k, $arrChart) ? $arrChart[$k] : 0;
				    ?>
                    <div class="progress-item">
                        <div class="header-progress">
                            <span class="title"><?php esc_attr_e($nameTabs); ?> <strong>(<?= $percent; ?>)</strong></span>
                            <span class="percent"><?= round($percentTabs,1); ?>%</span>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-<?= $class; ?> color-<?= $class; ?>" role="progressbar" aria-valuenow="<?= round($percentTabs,1); ?>" aria-valuemin="0" aria-valuemax="100" ></div>
                        </div>
                    </div>
				    <?php
			    endforeach;
			    ?>
            </div>
        </form>
    </div>
</div>
