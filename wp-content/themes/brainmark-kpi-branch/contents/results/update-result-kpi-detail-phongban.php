<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 12/28/17
 * Time: 4:26 PM
 */

require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-formulas.php';
$add_target = wp_create_nonce('add_target');
$user = wp_get_current_user();
$orgchart = user_load_orgchart( $user );
$department_id = $orgchart->id;
#$firstYear = kpi_get_first_year();
$firstYear = year_get_year_by_chart_id( $orgchart->id, TIME_YEAR_VALUE );
$currentLevel = 1;
$tabs = [
	'finance' => [
		'title' => _TABS_Finance,
		'type' => 'Finance',
		'percent' => $firstYear ? $firstYear['finance'] : '',
		'img' => 'tai-chinh.png',
		'time_type' => 'PRECIOUS',
		'time_value' => TIME_PRECIOUS_VALUE,
		'year_id' => $firstYear
	],
	'customer' => [
		'title' => _TABS_Customer,
		'type' => 'Customer',
		'percent' => $firstYear ? $firstYear['customer'] : '',
		'img' => 'customer.png',
		'time_type' => 'PRECIOUS',
		'time_value' => TIME_PRECIOUS_VALUE,
		'year_id' => $firstYear
	],
	'operate' => [
		'title' => _TABS_Operate,
		'type' => 'Operate',
		'percent' => $firstYear ? $firstYear['operate'] : '',
		'img' => 'van-hanh.png',
		'time_type' => 'PRECIOUS',
		'time_value' => TIME_PRECIOUS_VALUE,
		'year_id' => $firstYear
	],
	'development' => [
		'title' => _TABS_Development,
		'type' => 'Development',
		'percent' => $firstYear ? $firstYear['development'] : '',
		'img' => 'phat-trien.png',
		'time_type' => 'PRECIOUS',
		'time_value' => TIME_PRECIOUS_VALUE,
		'year_id' => $firstYear
	]
];

?>
<div class="clearfix"></div>
<?php
?>
    <div class="block-detail block-result-kpi-detail">
        <div id="room-target-kpi" data-content-management="" style="display: block">
            <ul id="" class="list-room-target-kpi clearfix nav nav-pills">
				<?php
				foreach ($tabs as $tabkey => $tabData):
					$cls = ('finance' == $tabkey) ? 'active' : '';
					?>
                    <li class="room-target-kpi-item <?php echo $cls; ?> room-<?php echo $tabkey; ?>">
                        <a href="#room-<?php echo $tabkey; ?>" class="" title="" data-toggle="tab">
                            <img src="<?php echo THEME_URL; ?>/assets/images/<?php echo $tabData['img']; ?>"/>
							<?php echo $tabData['title']; ?>
                        </a>
                    </li>
				<?php endforeach; ?>
            </ul>
            <div class="list-detail tab-content clearfix">
				<?php foreach ($tabs as $tabkey => $tabData):
					$charts = kpi_get_list_org_charts($user->orgchart_id);
					$cls = ('finance' == $tabkey) ? 'active' : '';
					?>
                    <div class="tab-pane <?php echo $cls; ?>" id="room-<?php echo $tabkey; ?>">
                        <div class="list-detail">
                            <div class="top-list-detail">
                                <h3 class="title">
									<?php echo $tabData['title']; ?>
                                    <a href="javascript:;" data-year="<?php echo esc_json_attr($tabData['year_id']); ?>" data-toggle="modal"
                                       data-target="#tpl-ceo-kpi-year" >
                                        <span class="icon-percent" contenteditable="false" data-kpi-percent="<?php echo $tabkey; ?>"><?php echo $tabData['percent']; ?>%</span>
                                        <i class="glyphicon glyphicon-pencil hidden" aria-hidden="true"></i>
                                    </a>
                                </h3>
                            </div>
                        </div>

                        <form data-update-result="" action="<?php echo admin_url('admin-ajax.php?action=personal_update_result'); ?>" class=""
                              id="form-<?php echo $tabkey; ?>" method="post" enctype="application/x-www-form-urlencoded"
                              onsubmit="return false">
                            <input value="<?php echo $add_target; ?>" type="hidden" name="_wpnonce">
                            <input value="<?php echo $tabData['type']; ?>" type="hidden" name="type">
                            <table class="table-list-detail table-managerment-target-kpi" cellpadding="0" cellspacing="0">
                                <thead>
                                <tr>
                                    <th class="column-1 kpi-id align-center"><?php _e('Mã KPI', TPL_DOMAIN_LANG); ?></th>
                                    <th class="column-2 kpi-content"><?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?></th>
                                    <th class="column-3 kpi-company_plan"><?php _e('Trọng số', TPL_DOMAIN_LANG); ?></th>
                                    <th class="column-4 kpi-unit align-center"><?php _e('Kế hoạch', TPL_DOMAIN_LANG); ?></th>
                                    <th class="column-5 kpi-receive align-center"><?php _e('Thực hiện', TPL_DOMAIN_LANG); ?></th>
                                    <th class="column-6 align-center"><?php _e('Hoàn thành', TPL_DOMAIN_LANG); ?></th>
                                    <th class="column-7 align-center"><?php _e('Chứng minh', TPL_DOMAIN_LANG); ?></th>
                                    <th class="column-8 align-center"><?php _e('Tình trạng', TPL_DOMAIN_LANG); ?></th>
                                </tr>
                                </thead>
                                <tbody>
								<?php
								$groupDatas = get_kpi_by_year_orgchart_for_year( $tabData['type'], TIME_YEAR_VALUE, $user->ID, 'yes' );
								if (!empty($groupDatas)):
									foreach ($groupDatas as &$item):
										$plan = $item['plan'];
										$actual_gain = $item['actual_gain'];
										$planForYear = $item['plan_for_year'];
										$percentForMonth = 0;
										$percentForYear = 0;
										?>
                                        <tr>
                                            <td class="column-1 kpi-id align-center">
												<?php echo $item['id']; ?>
												<?php
												$classStar = "";
												if( $item['create_by_node'] == KPI_CREATE_BY_NODE_START ){
													$classStar = "color-node-0";
												}elseif( $item['create_by_node'] == KPI_CREATE_BY_NODE_MIDDLE ){
													$classStar = "color-node-1-2";
												}
												if( $item['required'] == 'yes' ): ?>
                                                    <span class="<?php esc_attr_e($classStar); ?> glyphicon glyphicon-star"></span>
												<?php endif; ?>
                                            </td>
                                            <td class="column-2 kpi-content"><?php echo $item['post_title']; ?></td>
                                            <td class="column-3 kpi-company_plan"><?php echo $item['percent']; ?>%</td>
                                            <td class="column-4 kpi-unit align-center"><?php esc_attr_e( $plan ); ?> <?php esc_attr_e(getUnit($item['unit'])); ?></td>
                                            <td class="column-5 kpi-receive align-center">
												<?php

												if( !empty( $actual_gain ) ):
													$formulas = maybe_unserialize( $item['formulas'] );
												    $formula_type = $item['formula_type'];
													$percentForMonth = getPercentForMonth( $item['unit'], $formula_type, $formulas, $plan, $actual_gain );
												endif;
												?>
												<?php
												if( !in_array($item['status'], [KPI_STATUS_DONE, KPI_STATUS_WAITING] ) ):
													?>
                                                    <input type="text" name="resultKPI[<?= $item['id']; ?>][actual_gain]" value="<?= esc_attr($actual_gain); ?>" /> <?php esc_attr_e(getUnit($item['unit'])); ?>
                                                    <input type="hidden" name="resultKPI[<?= $item['id']; ?>][ID]" value="<?= $item['id']; ?>" />
												<?php else: ?>
													<?php  esc_attr_e( $actual_gain ); ?> <?php esc_attr_e(getUnit($item['unit'])); ?>
												<?php endif; ?>
                                            </td>
                                            <td class="column-6 kpi-percent align-center"><?php echo round( $percentForMonth, 1); ?>%</td>
                                            <td class="column-7 align-center">
												<?php
												if( !empty( $item['files'] ) ){
													$files = maybe_unserialize( $item['files'] );
													if( !empty( $files ) && !empty( $files['files_url'] ) ){
														echo sprintf("<a href='javascript:;' data-action='%s' data-id='%s' class='view-file-upload' data-target='#tpl-personal-view-file' >%s</a>", admin_url('admin-ajax.php?action=load_file_upload&id='.$item['id']), $item['id'], __('Xem danh sách tập tin', TPL_DOMAIN_LANG));
													}
												}
												if( $item['status'] != KPI_STATUS_DONE ):
													?>
                                                    <input type="file" name="resultKPI[<?= $item['id']; ?>][files][]" id="resultKPI[<?= $item['id']; ?>][files]" class="inputfile" accept="image/*, .xls, .xlsx, .doc, .docx, .txt, .zip, .rar" data-error-type="<?php _e('Vui lòng tải tập tin đúng định dạng (jpg, png, jpeg, doc, docx, xlsx, xls, txt)'); ?>" data-error-size="<?php _e('Vui lòng tải tập tin < 5M', TPL_DOMAIN_LANG); ?>" data-multiple-caption="{count} <?= __('được chọn', TPL_DOMAIN_LANG); ?>" multiple />
                                                    <label for="resultKPI[<?= $item['id']; ?>][files]"><span><i class="fa fa-upload" aria-hidden="true"></i> <?php _e('Chọn tập tin', TPL_DOMAIN_LANG); ?></span></label>
												<?php endif; ?>
                                            </td>
                                            <td class="column-8 align-center">
	                                            <?php
	                                            $disabled = '';
	                                            $name = 'name="kpis['.$item['id'].'][ID]"';
	                                            $valueInput = $item['id'];
	                                            $nameCheck = 'name="kpis['.$item['id'].'][status]"';
	                                            $textSendApproved = '';
	                                            if( in_array( $item['status'], [KPI_STATUS_DONE] ) ){
		                                            $disabled = 'disabled';
		                                            $name ='';
		                                            $valueInput = '';
		                                            $nameCheck = '';
		                                            $textSendApproved = __('Đã duyệt', TPL_DOMAIN_LANG);
	                                            }elseif( $item['status'] == KPI_STATUS_WAITING ){
		                                            $disabled = 'disabled';
		                                            $name ='';
		                                            $valueInput = '';
		                                            $nameCheck = '';
		                                            $textSendApproved = __('Đã gửi', TPL_DOMAIN_LANG);
	                                            }
	                                            ?>
                                                <label class="switch">
                                                    <input class="checkbox-status hidden" type="checkbox" <?= $nameCheck; ?> <?= $disabled; ?> <?php echo in_array( $item['status'], [ KPI_STATUS_WAITING, KPI_STATUS_DONE ] ) ? 'checked="checked"' : ''; ?> value="1"/>
                                                    <span class="checkbox-slider"></span>
                                                </label>
                                                <input type="hidden" <?= $name; ?> value="<?= $valueInput; ?>" />
                                                <span><?= $textSendApproved; ?></span>
                                            </td>
                                        </tr>
									<?php endforeach;
								endif;
								?>
                                </tbody>
                            </table>
                            <div class="action-bot">
                                <button type="submit" class="btn-brb-default btn-save-and-public btn-action-item"
                                        name="btn-save-public"><i class="fa fa-floppy-o"
                                                                  aria-hidden="true"></i> <?php _e('Lưu', TPL_DOMAIN_LANG); ?>
                                </button>
                                <button type="button" data-method="post" data-action="<?php echo admin_url('admin-ajax.php?action=send_results'); ?>" class="btn-brb-default btn-save btn-action-item"
                                        name="btn-save-send"><i class="fa fa-floppy-o"
                                                                aria-hidden="true"></i> <?php _e('Gửi duyệt kết quả', TPL_DOMAIN_LANG); ?>
                                </button>
                                <button type="button" class="btn-brb-default btn-print btn-action-item" name="btn-print" onclick="location.href='<?php
                                echo esc_url( PAGE_EXPORTS_URL );
                                ?>'">
                                    <i class="fa fa-print" aria-hidden="true"></i>
									<?php _e('Xuất báo cáo',TPL_DOMAIN_LANG); ?>
                                </button>
                            </div>
                        </form>
                        <div id="tpl-personal-view-file" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
                            <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
                                  action="<?php echo admin_url('admin-ajax.php?action=update_upload_file') ?>" method="post">
                                <input type="hidden" name="id" value="">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title"><?php _e('Danh sách tập tin tải lên', TPL_DOMAIN_LANG); ?></h4>
                                    </div>
                                    <div class="modal-body clearfix">
                                        <table class="table-list-files table-list-detail">
                                            <thead>
                                            <tr>
                                                <th class="column-1"><?php _e('STT', TPL_DOMAIN_LANG); ?></th>
                                                <th class="column-2"><?php _e('Tên tập tin', TPL_DOMAIN_LANG); ?></th>
                                                <th class="column-3"><?php _e('Chọn', TPL_DOMAIN_LANG); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody class="tbody-main">

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <button type="submit" class="btn btn-cancel">Xoá</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
				<?php endforeach; ?>
            </div>
        </div>
    </div>
<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/8/18
 * Time: 17:44
 */

/*
require_once THEME_DIR . '/inc/lib-orgchart.php';
require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-formulas.php';
$uID = 0;
global $wpdb;
if( array_key_exists( 'uid', $_GET ) ){
	$uID = (int)wp_slash( $_GET['uid'] );
}
$user = wp_get_current_user();
$orgchart = user_load_orgchart($user);
$currentOrgID = $orgchart->id;
$year = $_GET['nam'];
$firstYear = kpi_get_first_year(0, KPI_YEAR_STATUS_PUBLISH);
# lấy thông tin người dùng để load dữ liệu KPI cần approved
if( $uID > 0 ){
	$user = kpi_get_user_by_id($uID);
	$orgchart = user_load_orgchart( $getUser );
}
if( empty( TIME_PRECIOUS_VALUE ) ){
	$tabsarr = ["'Finance'", "'Customer'", "'Operate'", "'Development'"];
	$strTabs = implode( ", ", $tabsarr );
	#$kpiForStaff = kpi_get_group_kpi_by_year_orgchart_for_year_by_lower_grade_nv($strTabs, TIME_YEAR_VALUE, $user->ID);
}else{
	$kpiForStaff = get_kpi_by_year_orgchart_for_precious_and_status($firstYear['year'], TIME_PRECIOUS_VALUE, $user->ID);
}
?>

<?php

	$tabs = [
		'finance' => [
			'title' => __('Finance', TPL_DOMAIN_LANG),
			'type' => 'Finance',
			'percent' => $firstYear ? $firstYear['finance'] : '',
			'img' => 'tai-chinh.png',
			'time_type' => 'YEAR',
			'time_value' => TIME_YEAR_VALUE,
			'year_id' => $firstYear
		],
		'customer' => [
			'title' => __('Customer', TPL_DOMAIN_LANG),
			'type' => 'Customer',
			'percent' => $firstYear ? $firstYear['customer'] : '',
			'img' => 'customer.png',
			'time_type' => 'YEAR',
			'time_value' => TIME_YEAR_VALUE,
			'year_id' => $firstYear
		],
		'operate' => [
			'title' => __('Operate', TPL_DOMAIN_LANG),
			'type' => 'Operate',
			'percent' => $firstYear ? $firstYear['operate'] : '',
			'img' => 'van-hanh.png',
			'time_type' => 'YEAR',
			'time_value' => TIME_YEAR_VALUE,
			'year_id' => $firstYear
		],
		'development' => [
			'title' => __('Development', TPL_DOMAIN_LANG),
			'type' => 'Development',
			'percent' => $firstYear ? $firstYear['development'] : '',
			'img' => 'phat-trien.png',
			'time_type' => 'YEAR',
			'time_value' => TIME_YEAR_VALUE,
			'year_id' => $firstYear
		]
	];
	?>

	<div class="clearfix"></div>
	<div class="block-detail block-result-kpi-detail">
		<div id="room-target-kpi" data-content-management="" style="display: block">
			<ul id="" class="list-room-target-kpi clearfix nav nav-pills">
				<?php
				foreach ($tabs as $tabkey => $tabData):
					$cls = ('finance' == $tabkey) ? 'active' : '';
					?>
					<li class="room-target-kpi-item <?php echo $cls; ?> room-<?php echo $tabkey; ?>">
						<a href="#room-<?php echo $tabkey; ?>" class="" title="" data-toggle="tab">
							<img src="<?php echo THEME_URL; ?>/assets/images/<?php echo $tabData['img']; ?>"/>
							<?php echo $tabData['title']; ?>
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
			<div class="list-detail tab-content clearfix">
				<?php foreach ($tabs as $tabkey => $tabData):
					$charts = kpi_get_list_org_charts($user->orgchart_id);
					$cls = ('finance' == $tabkey) ? 'active' : '';
					?>
					<div class="tab-pane <?php echo $cls; ?>" id="room-<?php echo $tabkey; ?>">
						<div class="list-detail">
							<div class="top-list-detail">
								<h3 class="title">
									<?php echo $tabData['title']; ?>
									<a href="javascript:;" data-year="<?php echo esc_json_attr($tabData['year_id']); ?>" data-toggle="modal"
									   data-target="#tpl-ceo-kpi-year" >
										<span class="icon-percent" contenteditable="false" data-kpi-percent="<?php echo $tabkey; ?>"><?php echo $tabData['percent']; ?>%</span>
										<i class="glyphicon glyphicon-pencil hidden" aria-hidden="true"></i>
									</a>
								</h3>
							</div>
						</div>

						<form data-update-result="" action="<?php echo admin_url('admin-ajax.php?action=personal_update_result'); ?>" class=""
						      id="form-<?php echo $tabkey; ?>" method="post" enctype="application/x-www-form-urlencoded"
						      onsubmit="return false">
							<input value="<?php echo $add_target; ?>" type="hidden" name="_wpnonce">
							<input value="<?php echo $tabData['type']; ?>" type="hidden" name="type">
							<table class="table-list-detail table-managerment-target-kpi" cellpadding="0" cellspacing="0">
								<thead>
								<tr>
									<th class="column-1 kpi-id align-center"><?php _e('Mã KPI', TPL_DOMAIN_LANG); ?></th>
									<th class="column-2 kpi-content"><?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?></th>
									<th class="column-3 kpi-company_plan"><?php _e('Trọng số (%)', TPL_DOMAIN_LANG); ?></th>
									<th class="column-4 kpi-unit align-center"><?php _e('Kế hoạch', TPL_DOMAIN_LANG); ?></th>
									<th class="column-5 kpi-receive align-center"><?php _e('Thực hiện', TPL_DOMAIN_LANG); ?></th>
									<th class="column-6 align-center"><?php _e('ĐVT', TPL_DOMAIN_LANG); ?></th>
									<th class="column-7 align-center"><?php _e('Hoàn thành (%)', TPL_DOMAIN_LANG); ?></th>
									<th class="column-8 align-center"><?php _e('Chứng minh', TPL_DOMAIN_LANG); ?></th>
									<th class="column-9 align-center"><?php _e('Tình trạng', TPL_DOMAIN_LANG); ?></th>
								</tr>
								</thead>
								<tbody>
								<?php
								$groupDatas = get_kpi_by_year_orgchart_for_year( $tabData['type'], TIME_YEAR_VALUE, $getUser->ID, 'yes' );
								if (!empty($groupDatas)):
									foreach ($groupDatas as &$item):
										$plan = $item['plan'];
										$actual_gain = $item['actual_gain'];
										$planForYear = $item['plan_for_year'];
										$percentForMonth = 0;
										$percentForYear = 0;
										?>
										<tr>
											<td class="column-1 kpi-id align-center">
												<?php echo $item['id']; ?>
												<?php
												$classStar = "";
												if( $item['create_by_node'] == KPI_CREATE_BY_NODE_START ){
													$classStar = "color-node-0";
												}elseif( $item['create_by_node'] == KPI_CREATE_BY_NODE_MIDDLE ){
													$classStar = "color-node-1-2";
												}
												if( $item['required'] == 'yes' ): ?>
													<span class="<?php esc_attr_e($classStar); ?> glyphicon glyphicon-star"></span>
												<?php endif; ?>
											</td>
											<td class="column-2 kpi-content"><?php echo $item['post_title']; ?></td>
											<td class="column-3 kpi-company_plan"><?php echo $item['percent']; ?></td>
											<td class="column-4 kpi-unit align-center"><?php esc_attr_e( $plan ); ?></td>
											<td class="column-5 kpi-receive align-center">
												<?php
												if( !empty( $actual_gain ) ):
													$formulas = maybe_unserialize( $item['formulas'] );
													if( $item['unit'] == KPI_UNIT_THOI_GIAN ){
														$dateUnit = explode("/", $plan);
														$dateActualAgain = explode("/", $actual_gain);
														if( count($dateUnit) > 2 ){
															#DD/MM/YYYY
															$dayUnit = $dateUnit[0];
															$monthUnit = $dateUnit[1];
															$yearUnit = $dateUnit[2];
															if ($item['formula_type'] == KPI_FORMULA_ON_OFF){
																if( is_numeric( $monthUnit ) && is_numeric( $yearUnit ) ){

																}else{
																	$planUnit = (int)$dayUnit;
																	$resultActual = (int)$dateActualAgain[0];
																	$percentForMonth = calculator_on_off_by_number( $formulas, $planUnit, $resultActual );
																}
															}elseif( $item['formula_type'] == KPI_FORMULA_OSCILLATE ){
																if( is_numeric( $monthUnit ) && is_numeric( $yearUnit ) ){

																}else{
																	$planUnit = (int)$dayUnit;
																	$resultActual = (int)$dateActualAgain[0];
																	$percentForMonth = calculator_oscillate_by_number( $formulas, $planUnit, $resultActual );
																}
															}elseif( $item['formula_type'] == KPI_FORMULA_ADD_MAX_KQ ){
																if( is_numeric( $monthUnit ) && is_numeric( $yearUnit ) ){

																}else{
																	$planUnit = (int)$dayUnit;
																	$resultActual = (int)$dateActualAgain[0];
																	$percentForMonth = calculator_add_max_kq_by_number($formulas, $planUnit, $resultActual);
																}
															}else{
																if( is_numeric( $monthUnit ) && is_numeric( $yearUnit ) ){

																}else{
																	$planUnit = (int)$dayUnit;
																	$resultActual = (int)$dateActualAgain[0];
																	$percentForMonth = calculator_add_between_by_number($formulas, $resultActual);
																}
															}
														}elseif( count($dateUnit) > 1 ){
															#DD/MM
															$dayUnit = $dateUnit[0];
															$monthUnit = $dateUnit[1];
															if ($item['formula_type'] == KPI_FORMULA_ON_OFF){
																if( is_numeric($monthUnit) ){

																}else{
																	$planUnit = (int)$dayUnit;
																	$resultActual = (int)$dateActualAgain[0];
																	$percentForMonth = calculator_on_off_by_number( $formulas, $planUnit, $resultActual );
																}
															}elseif( $item['formula_type'] == KPI_FORMULA_OSCILLATE ){
																if( is_numeric($monthUnit) ){

																}else{
																	$planUnit = (int)$dayUnit;
																	$resultActual = (int)$dateActualAgain[0];
																	$percentForMonth = calculator_oscillate_by_number( $formulas, $planUnit, $resultActual );
																}
															}elseif( $item['formula_type'] == KPI_FORMULA_ADD_MAX_KQ ){
																if( is_numeric($monthUnit) ){

																}else{
																	$planUnit = (int)$dayUnit;
																	$resultActual = (int)$dateActualAgain[0];
																	$percentForMonth = calculator_add_max_kq_by_number($formulas, $planUnit, $resultActual);
																}
															}else{
																if( is_numeric($monthUnit) ){

																}else{
																	$planUnit = (int)$dayUnit;
																	$resultActual = (int)$dateActualAgain[0];
																	$percentForMonth = calculator_add_between_by_number($formulas, $resultActual);
																}
															}
														}else{
															#DD
															$dayUnit = (int)$dateUnit[0];
															$planUnit = (int)$dayUnit;
															$resultActual = (int)$dateActualAgain[0];
															if ($item['formula_type'] == KPI_FORMULA_ON_OFF){
																$percentForMonth = calculator_on_off_by_number( $formulas, $planUnit, $resultActual );
															}elseif ($item['formula_type'] == KPI_FORMULA_OSCILLATE) {
																$percentForMonth = calculator_oscillate_by_number( $formulas, $planUnit, $resultActual );
															} elseif ($item['formula_type'] == KPI_FORMULA_ADD_MAX_KQ) {
																$percentForMonth = calculator_add_max_kq_by_number($formulas, $planUnit, $resultActual);
															} else {
																#KPI_FORMULA_ADD_BETWEEN
																$percentForMonth = calculator_add_between_by_number($formulas, $resultActual);
															}
														}


													}else {
														if ($item['formula_type'] == KPI_FORMULA_ON_OFF) {
															#KH is plan, KQ is actual_gain
															#var_dump( $formulas, $plan, $actual_gain );
															$percentForMonth = calculator_on_off_by_number( $formulas, $plan, $actual_gain );
														} elseif ($item['formula_type'] == KPI_FORMULA_OSCILLATE) {
															$percentForMonth = calculator_oscillate_by_number( $formulas, $plan, $actual_gain );
														} elseif ($item['formula_type'] == KPI_FORMULA_ADD_MAX_KQ) {
															$percentForMonth = calculator_add_max_kq_by_number($formulas, $plan, $actual_gain);
														} else {
															#KPI_FORMULA_ADD_BETWEEN
															$percentForMonth = calculator_add_between_by_number($formulas, $actual_gain);
														}
													}
												endif;
												?>
												<?php
												if( $item['status'] != KPI_STATUS_DONE ):
													?>
													<input type="text" name="resultKPI[<?= $item['id']; ?>][actual_gain]" value="<?= esc_attr($actual_gain); ?>" />
													<input type="hidden" name="resultKPI[<?= $item['id']; ?>][ID]" value="<?= $item['id']; ?>" />
												<?php else: ?>
													<?php  esc_attr_e( $actual_gain ); ?>
												<?php endif; ?>
											</td>
											<td class="column-6 kpi-departments align-center">
												<?php esc_attr_e(getUnit($item['unit'])); ?>
											</td>
											<td class="column-7 kpi-percent align-center"><?php echo $percentForMonth; ?></td>
											<td class="column-8 align-center">
												<?php
												if( !empty( $item['files'] ) ){
													$files = maybe_unserialize( $item['files'] );
													if( !empty( $files ) && !empty( $files['files_url'] ) ){
														echo sprintf("<a href='javascript:;' data-action='%s' data-id='%s' class='view-file-upload' data-target='#tpl-personal-view-file' >%s</a>", admin_url('admin-ajax.php?action=load_file_upload&id='.$item['id']), $item['id'], __('Xem danh sách tập tin', TPL_DOMAIN_LANG));
													}
												}
												if( $item['status'] != KPI_STATUS_DONE ):
													?>
													<input type="file" name="resultKPI[<?= $item['id']; ?>][files][]" id="resultKPI[<?= $item['id']; ?>][files]" class="inputfile" accept="image/*, .xls, .xlsx, .doc, .docx, .txt, .zip, .rar" data-error-type="<?php _e('Vui lòng tải tập tin đúng định dạng (jpg, png, jpeg, doc, docx, xlsx, xls, txt)'); ?>" data-error-size="<?php _e('Vui lòng tải tập tin < 5M', TPL_DOMAIN_LANG); ?>" data-multiple-caption="{count} <?= __('được chọn', TPL_DOMAIN_LANG); ?>" multiple />
													<label for="resultKPI[<?= $item['id']; ?>][files]"><span><i class="fa fa-upload" aria-hidden="true"></i> <?php _e('Chọn tập tin', TPL_DOMAIN_LANG); ?></span></label>
												<?php endif; ?>
											</td>
											<td class="column-9 align-center">
												<?php
												$disabled = '';
												$name = 'name="kpis['.$item['id'].'][ID]"';
												$valueInput = $item['id'];
												$nameCheck = 'name="kpis['.$item['id'].'][status]"';
												$textSendApproved = '';
												if( in_array( $item['status'], [KPI_STATUS_DONE] ) ){
													$disabled = 'disabled';
													$name ='';
													$valueInput = '';
													$nameCheck = '';
													$textSendApproved = __('Đã duyệt', TPL_DOMAIN_LANG);
												}elseif( $item['status'] == KPI_STATUS_WAITING ){
													$textSendApproved = __('Chờ duyệt', TPL_DOMAIN_LANG);
												}
												?>
												<label class="switch">
													<input class="checkbox-status hidden" type="checkbox" <?= $nameCheck; ?> <?= $disabled; ?> <?php echo in_array( $item['status'], [ KPI_STATUS_DONE ] ) ? 'checked="checked"' : ''; ?> value="1"/>
													<span class="checkbox-slider"></span>
												</label>
												<input type="hidden" <?= $name; ?> value="<?= $valueInput; ?>" />
												<span><?= $textSendApproved; ?></span>
											</td>
										</tr>
									<?php endforeach;
								endif;
								?>
								</tbody>
							</table>
							<div class="action-bot">
								<button type="submit" class="btn-brb-default btn-save-and-public btn-action-item"
								        name="btn-save-public"><i class="fa fa-floppy-o"
								                                  aria-hidden="true"></i> <?php _e('Lưu', TPL_DOMAIN_LANG); ?>
								</button>
								<button type="button" data-method="post" data-action="<?php echo admin_url('admin-ajax.php?action=update_approved_results'); ?>" class="btn-brb-default btn-save btn-action-item"
								        name="btn-save-send"><i class="fa fa-floppy-o"
								                                aria-hidden="true"></i> <?php _e('Duyệt kết quả', TPL_DOMAIN_LANG); ?>
								</button>
								<button type="button" class="btn-brb-default btn-print btn-action-item" name="btn-print">
									<i class="fa fa-print" aria-hidden="true"></i>
									<?php _e('Xuất báo cáo', TPL_DOMAIN_LANG); ?>
								</button>
							</div>
						</form>
						<div id="tpl-personal-view-file" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
							<form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
							      action="<?php echo admin_url('admin-ajax.php?action=update_upload_file') ?>" method="post">
								<input type="hidden" name="id" value="">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title"><?php _e('Danh sách tập tin tải lên', TPL_DOMAIN_LANG); ?></h4>
									</div>
									<div class="modal-body clearfix">
										<table class="table-list-files table-list-detail">
											<thead>
											<tr>
												<th class="column-1"><?php _e('STT', TPL_DOMAIN_LANG); ?></th>
												<th class="column-2"><?php _e('Tên tập tin', TPL_DOMAIN_LANG); ?></th>
												<th class="column-3"><?php _e('Chọn', TPL_DOMAIN_LANG); ?></th>
											</tr>
											</thead>
											<tbody class="tbody-main">

											</tbody>
										</table>
									</div>
									<div class="modal-footer">
										<div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
										<div class="col-lg-12 col-md-12 col-sm-12">
											<button type="submit" class="btn btn-cancel">Xoá</button>
											<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>

*/ ?>
