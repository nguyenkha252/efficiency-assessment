<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/25/17
 * Time: 02:20
 */

$user = wp_get_current_user();
$orgchart = user_load_orgchart($user);
$member_role = $orgchart ? $orgchart->role : '';
if( empty($member_role) ) {
    $member_role = '';
} else {
    $member_role = strtolower($member_role);
}

require_once THEME_DIR . '/inc/lib-users.php';

$tim = !empty($_REQUEST['tim']) ? strip_tags($_REQUEST['tim']) : '';
$room_id = $_REQUEST['room_id'] = $_GET['room_id'] = !empty($_GET['room_id']) ? (int)$_GET['room_id'] : '';
$orgchart_id = $_REQUEST['chart_id'] = $_GET['chart_id'] = !empty($_GET['chart_id']) ? (int)$_GET['chart_id'] : '';

$result = get_list_user_by_role_managers($_REQUEST);
/*echo "<pre>";
print_r( $result );
echo "</pre>";*/

$result->get_results();
$users = $result->get_results();
$total = $result->get_total();
$trang = $_REQUEST['trang'];
$limit = $_REQUEST['limit'];

global $wpdb;
$managers = get_page_by_path( PAGE_USER_MANAGERS );
#$members = get_page_by_path( PAGE_MANAGE_MEMBERS );
#$profile_url = apply_filters( 'the_permalink', get_permalink( $profile ), $profile );
$current_url = apply_filters( 'the_permalink', get_permalink( $managers ), $managers );
# echo '<pre>'; var_dump(__LINE__, __FILE__, $user->orgchart_id, $result['sql'], $result['error'], $users[0]); echo '</pre>';


# echo '<pre>';
# global $wp_the_query;
# var_dump($_SERVER['REQUEST_URI'], $_GET, $wp_the_query);
# echo '</pre>';
$referrer = str_replace('&', '%26', urlencode($_SERVER['REQUEST_URI']) );
$charts = kpi_get_list_org_charts($user->orgchart_id, true);

?>
    <div class="lists container-fluid page-template-page-manage-members ">
        <h1 class="col-md-4" style="margin: 0;padding: 0;margin-bottom: 30px;margin-top: 20px;"><?php _e('Danh sách quản trị', TPL_DOMAIN_LANG); ?> <a href="<?php
            $url = esc_url( add_query_arg('_referrer',  $referrer, kpi_get_profile_manager_url('new') ) );
            echo $url;
            ?>" class="btn btn-primary"><?php _e('Create New', TPL_DOMAIN_LANG); ?></a>

        </h1>
        <table class="user-list" cellpadding="0" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th class="id-stt"><?php _e('Số TT', TPL_DOMAIN_LANG); ?></th>
                <th class="id-col"><?php _e('Mã Số NV', TPL_DOMAIN_LANG); ?></th>
                <th class="avatar-img"><?php _e('Avatar', TPL_DOMAIN_LANG); ?></th>
                <th class="display_name"><?php _e('Display Name', TPL_DOMAIN_LANG); ?></th>
                <th class="email"><?php _e('Email', TPL_DOMAIN_LANG); ?></th>
                <th class="action"><?php _e('Thao Tác', TPL_DOMAIN_LANG); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php
            if( !empty($users) ):
                $default_avatar = THEME_URL . '/assets/images/no-photo.jpg';
                $stt = 1;
                $remove_url = esc_url( admin_url('admin-ajax.php?action=user-remove&uid=USERID&_wpnonce='.
                    wp_create_nonce('remove_user') . '&_referrer=' . $referrer) );
                foreach($users as $user):
                    $edit_url = esc_url( add_query_arg( ['profile' => $user->ID, '_referrer' => $referrer], $current_url ) );
                    ?>
                    <tr class="user-row">
                        <td class="id-stt"><?php echo $stt; $stt ++;?></td>
                        <td class="id-col"><label><input class="id" type="checkbox" name="user_id[]" value="<?php echo $user->ID; ?>"><span><?php echo $user->user_nicename; ?></span></label></td>
                        <td class="avatar-img"><img class="avatar"
                                                    onerror="this.src = this.getAttribute('data-src');"
                                                    src="<?php echo empty($user->avatar) ? $default_avatar : $user->avatar; ?>"
                                                    data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                                    style="max-width: 32px; max-height: 32px"></td>
                        <td class="display_name"><?php echo $user->display_name; ?></td>
                        <td class="email"><?php echo $user->user_email; ?></td>
                        <td class="action">
                            <a href="<?php echo $edit_url; ?>" title="Chỉnh Sửa"><span class="fa fa-pencil"></span></a>
                            <a href="javascript:;" data-target="#confirm-popup" data-toggle="modal"
                               data-remove-element="tr.user-row" data-loading-element="tr.user-row"
                               data-url="<?php echo str_replace('USERID', $user->ID, $remove_url); ?>"
                               data-type="post" data-data-type="json"
                               data-message="Bạn có muốn xóa nhân viên <?php echo esc_attr("\"{$user->display_name}\"") ?>?"
                               title="Xóa"><span class="fa fa-trash-o"></span></a>
                        </td>
                    </tr>
                <?php
                endforeach;
            endif;
            ?>
            </tbody>

        </table>
        <?php
        $nav = new Paginator($total, $limit, $trang, "{$current_url}?trang=(:num)");
        $nav->setPreviousText('Trước');
        $nav->setNextText('Sau');
        echo $nav->toHtml();
        ?>
    </div>
    <!-- /.lists -->
<?php
