<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/25/17
 * Time: 02:20
 */
global $wpdb;

$profile_id = $_GET['profile'];
$url = kpi_get_profile_manager_url($profile_id);
$user = new WP_User($profile_id);
$referrer = str_replace('&', '%26', !empty($_REQUEST['_referrer']) ? $_REQUEST['_referrer'] : '' );
if( !empty($user) && !is_wp_error( $user ) && ($_SERVER['REQUEST_METHOD'] == 'POST') ) {
	$params = wp_unslash( $_POST );
	require_once THEME_DIR . '/inc/lib-users.php';
	$wpnonce = !empty($params['_wpnonce']) ? $params['_wpnonce'] : '';

	$referrer = !empty($params['_referrer']) ? str_replace('%26', '&', $params['_referrer']) : '';
	if( wp_verify_nonce($wpnonce, 'update_user_mamager') ) {
		$userdata = $params;
		$userdata['user_login'] = $params['user_email'];
		$user = wp_update_user( $userdata );
	} else {
		$user = new WP_Error(USER_ERROR_INVALID_NONCE, __("Create user: nonce invalid", TPL_DOMAIN_LANG));
	}
	if( is_wp_error($user) ) {
		add_action('create_user_result', function() use ( $user ) {
			echo sprintf('<div class="col-lg-12 col-md-12 col-sm-12 error error-%s">%s</div>', $user->get_error_code(), $user->get_error_message() );
		});
	} else {
		$user = get_user_by("ID", $user);

		#user_send_mail_user( $user, $params['user_pass'] );
		if( $params['bnt'] == 'save_exit'){
			if( headers_sent() ){
				echo "<script>window.location.href = '". site_url() . $referrer ."'</script>";
			}else{
				wp_redirect( !empty($referrer) ? $referrer : site_url("/") );
				exit;
			}
            #wp_redirect( !empty($referrer) ? site_url() . $referrer : site_url("/") );
            # wp_redirect( kpi_get_profile_url( $user ) );
            #exit;
        }

	}
}
?>
<form class="user-edit-form user-form" action="<?php echo esc_attr( $url ); ?>" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="ID" value="<?php echo $user->ID; ?>">
    <input type="hidden" name="action" value="update_user_manager">
    <input type="hidden" name="_referrer" value="/<?php echo PAGE_USER_MANAGERS; ?>/">
    <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce('update_user_mamager'); ?>">

    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h4 class="title"><?php _e('Update Member', TPL_DOMAIN_LANG); ?></h4>
        </div>
        <?php
        do_action('edit_user_result');
        ?>
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="display_name"><?php _e('Display Name', TPL_DOMAIN_LANG); ?></label>
                <input required class="form-control placeholder-no-fix" type="text" autocomplete="off"
                       placeholder="<?php _e('Display Name', TPL_DOMAIN_LANG); ?>" id="display_name"
                       name="display_name" value="<?php echo esc_attr($user->display_name); ?>"/>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="user_email"><?php _e('Email', TPL_DOMAIN_LANG); ?></label>
                <input required class="email form-control placeholder-no-fix" type="text" autocomplete="off"
                       placeholder="<?php _e('Email', TPL_DOMAIN_LANG); ?>" id="user_email" name="user_email" value="<?php echo esc_attr($user->user_email); ?>"/>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="orgchart_id"><?php _e('Mã số nhân viên', TPL_DOMAIN_LANG); ?></label>
                <input required class="form-control placeholder-no-fix" type="text" autocomplete="off"
                       placeholder="<?php _e('Mã số nhân viên', TPL_DOMAIN_LANG); ?>" id="user_nicename"
                       name="user_nicename" value="<?php echo esc_attr($user->user_nicename); ?>"/>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="user_pass"><?php _e('Password', TPL_DOMAIN_LANG); ?></label>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off"
                       placeholder="<?php _e('Password', TPL_DOMAIN_LANG); ?>" id="user_pass" name="user_pass"/>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="re_user_pass"><?php _e('Re-Password', TPL_DOMAIN_LANG); ?></label>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off"
                       placeholder="<?php _e('Re-Password', TPL_DOMAIN_LANG); ?>" id="re_user_pass"
                       name="re_user_pass"/>
            </div>
            <span class="notification warning">Mật khẩu có ít nhất 4 ký tự</span>
            <div class="form-group" data-group="orgcharttype">
                <label class="control-label visible-ie8 visible-ie9"
                       for="type_orgchart_id"><?php _e('Quản trị', TPL_DOMAIN_LANG); ?></label>
                <select required id="type_manager" class="selectpicker" data-live-search="true" name="role">
			        <?php
                    $selected = array_key_exists('user_employers', $user->caps) ? 'user_employers' : 'user_managers';
			        wp_dropdown_roles($selected);
			        ?>
                </select>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="footer">
                <input type="hidden" name="bnt" value="save">
                <button type="button" onclick="location.href='<?php echo esc_attr( kpi_get_profile_manager_url() ); ?>'" class="btn btn-default"><?php _e('Trở về danh sách quản trị', TPL_DOMAIN_LANG); ?></button>
                <button type="submit" class="btn btn-primary" onclick="this.form.bnt.value = 'save'"><?php _e('Save', TPL_DOMAIN_LANG); ?></button>
                <button type="submit" class="btn btn-primary" onclick="this.form.bnt.value = 'save_exit'"><?php _e('Save and Exit', TPL_DOMAIN_LANG); ?></button>
            </div>
        </div>
    </div>
</form><!-- /.user-edit-form -->
