<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/25/17
 * Time: 02:20
 */
global $wpdb;

# require_once THEME_DIR . '/inc/lib-users.php';

$url = kpi_get_profile_manager_url('new');

$referrer = str_replace('&', '%26', !empty($_REQUEST['_referrer']) ? $_REQUEST['_referrer'] : '' );

if( !empty($_GET['profile']) && $_GET['profile'] == 'new' && ($_SERVER['REQUEST_METHOD'] == 'POST') ) {
    $params = wp_unslash( $_POST );
	require_once THEME_DIR . '/inc/lib-users.php';
	$wpnonce = !empty($params['_wpnonce']) ? $params['_wpnonce'] : '';

	$referrer = !empty($params['_referrer']) ? str_replace('%26', '&', $params['_referrer']) : '';
	if( wp_verify_nonce($wpnonce, 'create_user_mamager') ) {
	    $userdata = $params;
	    $userdata['user_login'] = $params['user_email'];
	    #print_r( $userdata );exit;
		$user = wp_insert_user( $userdata );
		if( headers_sent() ){
            echo "<script>window.location.href = '". site_url() . $referrer ."'</script>";
        }else{
			wp_redirect( !empty($referrer) ? $referrer : site_url("/") );
			exit;
        }
		#print_r( $user );echo "<br>";
		#echo __LINE__ . ' ' . __FILE__;
	} else {
		$user = new WP_Error(USER_ERROR_INVALID_NONCE, __("Create user: nonce invalid", TPL_DOMAIN_LANG));
	}
	if( is_wp_error($user) ) {
		add_action('create_user_result', function() use ( $user ) {
			echo sprintf('<div class="col-lg-12 col-md-12 col-sm-12 error error-%s">%s</div>', $user->get_error_code(), $user->get_error_message() );
		});
	} else {
		$user = get_user_by("ID", $user);
		user_send_mail_user( $user, $params['user_pass'] );
		#wp_redirect( !empty($referrer) ? $referrer : site_url("/") );
		#wp_redirect( kpi_get_profile_url( $user ) );
		#exit;
	}
}
?>
<form class="user-register-form user-form"
      action="<?php echo esc_attr( $url ); ?>"
      method="POST" enctype="multipart/form-data">
    <input type="hidden" name="ID" value="0">
    <input type="hidden" name="action" value="create_user_mamager">
    <input type="hidden" name="_referrer" value="/<?php echo PAGE_USER_MANAGERS; ?>/">
    <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce('create_user_mamager'); ?>">


    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h4 class="title"><?php _e('Register Member', TPL_DOMAIN_LANG); ?></h4>
        </div>
        <?php
        do_action('create_user_result');
        ?>
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="display_name"><?php _e('Display Name', TPL_DOMAIN_LANG); ?></label>
                <input required class="form-control placeholder-no-fix" type="text" autocomplete="off"
                       placeholder="<?php _e('Display Name', TPL_DOMAIN_LANG); ?>" id="display_name"
                       name="display_name" value="<?php  ?>"/>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="user_email"><?php _e('Email', TPL_DOMAIN_LANG); ?></label>
                <input required class="email form-control placeholder-no-fix" type="text" autocomplete="off"
                       placeholder="<?php _e('Email', TPL_DOMAIN_LANG); ?>" id="user_email" name="user_email"/>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="orgchart_id"><?php _e('Mã số nhân viên', TPL_DOMAIN_LANG); ?></label>
                <input required class="form-control placeholder-no-fix" type="text" autocomplete="off"
                       placeholder="<?php _e('Mã số nhân viên', TPL_DOMAIN_LANG); ?>" id="user_nicename"
                       name="user_nicename" value=""/>
            </div>
            
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
	        <?php
	        $site_url = preg_replace('#^http(s|)://(www\.|)#', '', site_url());
	        $parts = explode('.', $site_url);
	        $user_pass = $parts[0] . '@123';
	        ?>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="user_pass"><?php _e('Password', TPL_DOMAIN_LANG); ?></label>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" value="<?php echo $user_pass; ?>"
                       placeholder="<?php _e('Password', TPL_DOMAIN_LANG); ?>" id="user_pass" name="user_pass"/>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"
                       for="re_user_pass"><?php _e('Lặp lại mật khẩu', TPL_DOMAIN_LANG); ?></label>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" value="<?php echo $user_pass; ?>"
                       placeholder="<?php _e('Lặp lại mật khẩu', TPL_DOMAIN_LANG); ?>" id="re_user_pass"
                       name="re_user_pass"/>
            </div>
            <span class="notification warning">Mật khẩu có ít nhất 4 ký tự</span>
            <div class="form-group" data-group="orgcharttype">
                <label class="control-label visible-ie8 visible-ie9"
                       for="type_orgchart_id"><?php _e('Quản trị', TPL_DOMAIN_LANG); ?></label>

                <select required id="type_manager" class="selectpicker" data-live-search="true" name="role">
                    <?php
                    wp_dropdown_roles();
                    ?>
                </select>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="footer">
                <button type="button" onclick="location.href='<?php echo esc_attr( kpi_get_members_url() ); ?>'" class="btn btn-default"><?php _e('Back to members list', TPL_DOMAIN_LANG); ?></button>
                <button type="reset" class="btn btn-default"><?php _e('Reset', TPL_DOMAIN_LANG); ?></button>
                <button type="submit" class="btn btn-primary"><?php _e('Create', TPL_DOMAIN_LANG); ?></button>
            </div>
        </div>
    </div>
</form><!-- /.user-register-form -->