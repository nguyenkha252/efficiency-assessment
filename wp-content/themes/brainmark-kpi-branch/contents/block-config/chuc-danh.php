<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/15/18
 * Time: 09:49
 */

return [
    1 => 'Tổng Giám Đốc Điều Hành',
    2 => 'Phó TGĐ',
    5 => 'Trưởng Khu Vực',
    6 => 'Phó Trưởng Khu Vực',
    7 => 'Giám Đốc',
    8 => 'Phó Giám Đốc',
    9 => 'Trưởng Phòng',
    10 => 'Phó Trưởng Phòng',
    11 => 'Nhân Viên Trưởng nhóm',
    12 => 'Nhân Viên',
];