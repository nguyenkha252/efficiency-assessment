<?php
/**
 * Created by PhpStorm.
 * User: 
 * Date: 3/15/18
 * Time: 10:21
 */

# /wp-content/themes/brainmark-kpi-branch/contents/exports/phieu-danh-gia-kpi-quan-ly.php
global $website_settings, $logo_url;
# require_once __DIR__ . '/../../../../../wp-load.php';
$user = wp_get_current_user();
$orgchart = user_load_orgchart( $user );
$orgchart_id = $orgchart ? $orgchart->id : 0;
$member_role = !empty( $orgchart ) ? $orgchart->role : '';
if( isset($_GET['room']) ):
	get_template_part( 'contents/exports/phieu-danh-gia-kpi-'.$member_role, 'room'  );
else:
	get_template_part( 'contents/exports/phieu-danh-gia-kpi', 'nhanvien'  );
#$firstYear = kpi_get_first_year();
/*
$firstYear = year_get_year_by_chart_id( $orgchart_id, TIME_YEAR_VALUE );
$currentLevel = 1;
$tabs = [
    'finance' => [
        'title' => __('Finance', TPL_DOMAIN_LANG),
        'type' => 'Finance',
        'percent' => $firstYear ? $firstYear['finance'] : '',
        'time_value' => TIME_PRECIOUS_VALUE,
        'year_id' => $firstYear
    ],
    'customer' => [
        'title' => __('Customer', TPL_DOMAIN_LANG),
        'type' => 'Customer',
        'percent' => $firstYear ? $firstYear['customer'] : '',
        'time_value' => TIME_PRECIOUS_VALUE,
        'year_id' => $firstYear
    ],
    'operate' => [
        'title' => __('Operate', TPL_DOMAIN_LANG),
        'type' => 'Operate',
        'percent' => $firstYear ? $firstYear['operate'] : '',
        'time_value' => TIME_PRECIOUS_VALUE,
        'year_id' => $firstYear
    ],
    'development' => [
        'title' => __('Development', TPL_DOMAIN_LANG),
        'type' => 'Development',
        'percent' => $firstYear ? $firstYear['development'] : '',
        'time_value' => TIME_PRECIOUS_VALUE,
        'year_id' => $firstYear
    ]
];

$chart_type = sanitize_title($GLOBALS['chartCurrent'] ? $GLOBALS['chartCurrent']['room'] : '');
$html = "";
$logo_base64 = base64_encode( file_get_contents($logo_url ) );
$logo = "data:image/png;base64,".$logo_base64;
# $logo = "http://sadeco.kpibrainmark.com/wp-content/themes/brainmark-kpi-branch/inc/tmp/logo.png";

	$textPrecious =
		(!empty($_GET['quy']) ? ( !empty($_GET['quy']) ? ('Quý ' . $_GET['quy']) : ('Tháng' . $_GET['thang']) ) : '  ...  ') . " / " .
		($firstYear ? $firstYear['year'] : '201...') . "";
	if( !isset( $_GET['quy'] ) || empty( $_GET['quy'] ) ){
		$textPrecious = "Năm " . ($firstYear ? $firstYear['year'] : '201...');
	}
	$title = "PHIẾU ĐÁNH GIÁ KPIs CẤP ". ($GLOBALS['chartCurrent'] ? mb_strtoupper($GLOBALS['chartCurrent']['room']) : '') . " - " . $textPrecious;


	if( empty( TIME_PRECIOUS_VALUE ) ){
		$tabsarr = ["'Finance'", "'Customer'", "'Operate'", "'Development'"];
		$strTabs = implode( ", ", $tabsarr );
		$kpiForStaff = kpi_get_group_kpi_by_year_orgchart_for_year_by_lower_grade_nv($strTabs, TIME_YEAR_VALUE, $user->ID);
	}else{
		if( !empty( TIME_MONTH_VALUE ) ){
			$kpiForStaff = get_kpi_by_year_orgchart_for_month_and_status($firstYear['year'], TIME_MONTH_VALUE, $user->ID);

		}else{
			$kpiForStaff = get_kpi_by_year_orgchart_for_precious_and_status($firstYear['year'], TIME_PRECIOUS_VALUE, $user->ID);
		}
	}

global $wpdb;
$totalPercent = 0;
$percentResults = [];
foreach($tabs as $key => $tab):
    $percentResults[$key] = ['total' => 0];
	$groupDatas = $kpiForStaff; #get_kpi_by_year_orgchart_for_year( $tab['type'], TIME_YEAR_VALUE, $user->ID, 'yes' );


    $percentResults[$key]['data'] = $groupDatas;
    # echo '<pre>'; var_dump($wpdb->last_query, $wpdb->last_error, $percentResults); echo '</pre>'; exit;

    if( !empty($groupDatas) ):
        # $countItem = count($groupDatas);
        foreach ($groupDatas as &$item):
            $plan = $item['plan'];
            $actual_gain = $item['actual_gain'];
            $planForYear = $item['plan_for_year'];
            $percentForMonth = 0;
            $percentForYear = 0;
            if( !empty( $actual_gain ) ):
                $formulas = maybe_unserialize( $item['formulas'] );
                $formula_type = $item['formula_type'];
                $percentForMonth = getPercentForMonth( $item['unit'], $formula_type, $formulas, $plan, $actual_gain );
            endif;
            $percentResults[$key][$item['id']] = doubleval($percentForMonth);
            $percentResults[$key]['total'] += doubleval($percentForMonth);
        endforeach;
    endif;
    $percentResults[$key]['percent'] = doubleval($tab['percent']);
    $percentResults[$key]['percent'] = ($percentResults[$key]['percent'] * $percentResults[$key]['total']) / 100;
    $totalPercent += $percentResults[$key]['percent'];
endforeach;
# echo '<pre>'; var_dump($percentResults); echo '</pre>'; exit;

ob_start();
$arrKPI = [];
?>
    <style>
        tr.group > td {
            font-weight: bolder;
            font-size: 12px;
        }
        table th{
            font-weight: bolder;
        }
        .head th{
            vertical-align: middle;
            padding: 5px 0;
            background-color: #e7e6e6;
        }
    </style>
<div style="width: 1024px; margin: 0 auto">
    <table width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr class="logo-row-1" style="">
            <th rowspan="3" width="30%" align="left" style="padding-right: 20px;"><img width="180" src="<?php #echo $logo; ?>"></th>
            <th align="left" width="70%" style="font-weight: normal;"><?php echo $website_settings['title_website']; ?></th>
        </tr>
        <tr class="logo-row-2">
            <th align="left" style="padding-top: 5px;"><?php echo $title; ?></th>
        </tr>
        <tr class="logo-row-3">
            <th align="left">Mã số: ...</th>
        </tr>
        </tbody>
    </table>
    <br><br><br>
    <table width="100%" border="1" cellspacing="0" cellpadding="0" style="margin-top: 50px;border-color: #eee;">
        <thead>
        <tr class="head">
            <?php $styleTH = "background-color: #e7e6e6;padding: 10px 0;";?>
            <th align="center" width="5%" style="<?= $styleTH; ?>">Stt</th>
            <th align="center" width="" style="<?= $styleTH; ?>">Mục tiêu</th>
            <th align="center" width="8%" style="<?= $styleTH; ?>">Trọng số (%)</th>
            <th align="center" width="8%" style="<?= $styleTH; ?>">Thời điểm nhận kết qủa</th>
            <th align="center" width="10%" style="<?= $styleTH; ?>">Kế hoạch</th>
            <th align="center" width="10%" style="<?= $styleTH; ?>">Thực hiện</th>
            <th align="center" width="9%" style="<?= $styleTH; ?>">Hoàn thành</th>
            <th align="center" width="8%" style="background-color: #e7e6e6;">KẾT QUẢ</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $styleTD = "padding: 10px;";
        $styleTDBG = "background-color:#fef6cc;";
        foreach($tabs as $key => $tab):
	        $arrKPI[$key] = ['total' => $tab['percent'], 'total_item' => 0];
            ?>
            <tr class="group">
                <td colspan="9" align="left" style="padding: 10px;"><?php echo "{$tab['title']} ({$tab['percent']}%)"; ?></td>
            </tr>
            <?php
            $groupDatas = &$percentResults[$key]['data'];

            # get_kpi_by_year_orgchart_for_year( $tab['type'], TIME_YEAR_VALUE, $user->ID, 'yes' );
            if (!empty($groupDatas)):
                $idx = 1;
                foreach ($groupDatas as &$item):
                    $plan = $item['plan'];
                    $actual_gain = $item['actual_gain'];
                    $planForYear = $item['plan_for_year'];
                    $percentForMonth = 0;
                    $percentForYear = 0;
                    $HT = round($percentResults[$key][$item['id']], 1);
                    $KQ = round($HT * $item['percent'] / 100, 1);
	                $arrKPI[$key]['total_item'] += $KQ;
                    ?>
                    <tr class="item">
                        <td align="center" style="<?= $styleTD; ?>"><?php echo $idx ?>&nbsp;</td>
                        <td align="left" style="<?= $styleTD; ?> <?= $styleTDBG; ?>"><?php echo $item['post_title']; ?></td>
                        <td align="center" style="<?= $styleTD; ?>"><?php echo $item['percent']; ?>%</td>
                        <td align="center" style="<?= $styleTD; ?> <?= $styleTDBG; ?>"><?php echo kpi_format_date( substr($item['receive'], 0, 10) ) ?></td>
                        <td align="center" style="<?= $styleTD; ?>"><?php echo $item['plan']; ?> <?php echo !empty($item['unit']) ? getUnit($item['unit']) : ''; ?></td>
                        <td align="center" style="<?= $styleTD; ?> <?= $styleTDBG; ?>"><?php echo "{$actual_gain}"; ?> <?php echo !empty($item['unit']) ? getUnit($item['unit']) : ''; ?></td>
                        <td align="center" style="<?= $styleTD; ?>"><?php echo "{$HT}"; ?>%</td>
                        <td align="center" style="<?= $styleTDBG; ?>"><?php echo $KQ; ?>%</td>
                    </tr>
                    <?php
                    $idx++;
                endforeach;
            endif;
        endforeach;
        ?>
        </tbody>
        <tfoot>
            <tr style="background-color: #e7e6e6;">
                <td colspan="7" align="left" style="padding: 10px;">TỔNG:</td><td align="center" style="font-weight: bold;">
                    <?php
                    $total = 0;
                    if( !empty( $arrKPI ) ){
                        foreach ( $arrKPI as $key => $item ){
                            $total += $item['total_item'] * $item['total'] / 100;
                        }
                    } ?>
		            <?php echo round($total,1); ?>%</td>
            </tr>
        </tfoot>
    </table>
    <br>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td align="center" style="font-weight: bold;">Người lập</td>
            <td align="center" style="font-weight: bold;">Xem xét</td>
            <td align="center" style="font-weight: bold;">Phê duyệt</td>
        </tr>
        <tr style="min-height: 100px">
            <td align="center">Giám đốc HCNS</td>
            <td align="center">Phó Tổng Giám đốc</td>
            <td align="center">Tổng Giám đốc</td>
        </tr>
        <tr style="min-height: 100px">
            <td><br><br><br><br><br><br><br></td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
        </tr>
        <tr>
            <td align="center">Ngày ...... / ...... / ......</td>
            <td align="center">Ngày ...... / ...... / ......</td>
            <td align="center">Ngày ...... / ...... / ......</td>
        </tr>
        </tbody>
    </table>
</div>

<?php


$html = ob_get_clean();

if( !empty($_GET['html']) ) {
    echo $html;
} else {
    phptopdf([
        'action' => 'download',
        "source_type" => 'html',
        "source" => $html,
        # "save_directory" => __DIR__ . '/',
        "file_name" => "phieu-danh-gia-kpi-cap-quan-ly-{$user->user_nicename}-theo-{$chart_type}.pdf"
    ]);

   $file_name = "phieu-danh-gia-kpi-cap-quan-ly-{$user->user_nicename}-theo-{$chart_type}.pdf";
   save_pdf( $file_name, $html, $logo_base64 );
}*/
endif;
