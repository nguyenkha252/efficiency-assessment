<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 3/15/18
 * Time: 10:21
 */
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-formulas.php';
require_once THEME_DIR . '/inc/lib-orgchart.php';
require_once THEME_DIR . '/ajax/get_user_info.php';
# /wp-content/themes/brainmark-kpi-branch/contents/exports/phieu-danh-gia-kpi-quan-ly.php
global $website_settings, $logo_url;
# require_once __DIR__ . '/../../../../../wp-load.php';
$user = wp_get_current_user();
$orgchart = user_load_orgchart( $user );
$orgchart_id = $orgchart ? $orgchart->id : 0;
$member_role = $orgchart ? $orgchart->role : '';
#$firstYear = kpi_get_first_year();
$firstYear = year_get_year_by_chart_id( $orgchart_id, TIME_YEAR_VALUE );
$currentLevel = 1;
$tabs = [
    'thaidohanhvi' => [
        'title' => __('THÁI ĐỘ - HÀNH VI', TPL_DOMAIN_LANG),
        'type' => 'Finance',
        'percent' => $firstYear ? $firstYear['finance'] : '',
        'time_value' => TIME_PRECIOUS_VALUE,
        'year_id' => $firstYear
    ],
    /*'muctieucongviec' => [
        'title' => __('Customer', TPL_DOMAIN_LANG),
        'type' => 'Customer',
        'percent' => $firstYear ? $firstYear['customer'] : '',
        'time_value' => TIME_PRECIOUS_VALUE,
        'year_id' => $firstYear
    ]*/
];

$chart_type = sanitize_title($GLOBALS['chartCurrent'] ? $GLOBALS['chartCurrent']['room'] : '');
$html = "";
$logo_base64 = base64_encode( file_get_contents($logo_url ) );
$imsize = @getimagesize($logo_url);
$width = 50;
$height = '';
if( $imsize ){
	$width_temp = $imsize[0];
	$height_temp = $imsize[1];
	$height = $height_temp * $width / $width_temp;
}
$logo = "data:image/png;base64,".$logo_base64;
$images_level_1 = get_template_directory_uri() . '/assets/images/can-cai-thien.png';
$images_level_1 = base64_encode( file_get_contents( $images_level_1 ) );
$images_level_1 = "data:image/png;base64,".$images_level_1;

$images_level_2 = get_template_directory_uri() . '/assets/images/hieu-qua.png';
$images_level_2 = base64_encode( file_get_contents( $images_level_2 ) );
$images_level_2 = "data:image/png;base64,".$images_level_2;

$images_level_3 = get_template_directory_uri() . '/assets/images/noi-bat.png';
$images_level_3 = base64_encode( file_get_contents( $images_level_3 ) );
$images_level_3 = "data:image/png;base64,".$images_level_3;
# $logo = "http://sadeco.kpibrainmark.com/wp-content/themes/brainmark-kpi-branch/inc/tmp/logo.png";

$title = "PHIẾU ĐÁNH GIÁ KPIs CÁ NHÂN - " .
    (!empty($_GET['quy']) ? ( !empty($_GET['quy']) ? ('Quý ' . $_GET['quy']) : ('Tháng' . $_GET['thang']) ) : '  ...  ') . " / " .
    ($firstYear ? $firstYear['year'] : '201...') . "";
$textPrecious = "Năm " . ($firstYear ? $firstYear['year'] : '201...');
$title = "PHIẾU ĐÁNH GIÁ KPIs NĂM ". ($GLOBALS['chartCurrent'] ? mb_strtoupper($GLOBALS['chartCurrent']['room']) : '') . " - " . $textPrecious;
# 2480px

global $wpdb;
$totalPercent = 0;
$percentResults = [];
$tabsarr = ["'Finance'", "'Customer'", "'Operate'", "'Development'"];
$strTabs = implode( ", ", $tabsarr );
$orgChildren = orgchart_get_all_kpi_by_parent( !empty($orgchart) ? $orgchart->id : 0 );
$arrOrgs = [];
if( !empty( $orgChildren ) ){
	foreach ( $orgChildren as $key => $value ){
		array_push($arrOrgs, $value['id'] );
	}
	$arrOrgs = implode(", ", $arrOrgs);
}
$groupDatas = kpi_get_group_kpi_by_year_orgchart_for_year_by_lower_grade_nv($strTabs, TIME_YEAR_VALUE, $user->ID);
# echo '<pre>'; var_dump($percentResults); echo '</pre>'; exit;
#echo '<pre>'; print_r($percentResults); echo '</pre>';
ob_start();
?>
    <!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="utf-8" />
        <title><?php echo $title; ?></title>
        <style>
            tr.group > td {
                font-weight: bold;
                font-size: 20px;
            }
            .name, .value, .head th{
                padding: 5px 10px;
            }
            .before-table-title{
                margin:0;
                padding-top:30px;
                padding-bottom: 10px;
                color:red;
            }
            table th{
                font-weight: bolder;
            }
            .background-column{
                background-color:#fef6cc;
            }
        </style>
    </head>
    <body>
    <div style="width: 1024px; margin: 0 auto">
        <table width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr class="logo-row-1" style="">
                <th rowspan="3" width="30%" align="left" style="padding-right: 20px;"><img width="180" src="<?php #echo $logo; ?>"></th>
                <th align="left" width="70%" style="font-weight: normal;"><?php echo $website_settings['title_website']; ?></th>
            </tr>
            <tr class="logo-row-2">
                <th align="left" style="padding-top: 5px;"><?php echo $title; ?></th>
            </tr>
            <tr class="logo-row-3">
                <th align="left">Mã số: ...</th>
            </tr>
            </tbody>
        </table>
        <br>
        <br>
        <br><br>
	    <?php $styleTH = "background-color: #e7e6e6;";?>
        <table width="100%" border="1" cellspacing="0" cellpadding="5" style="margin-top: 50px;border-color: #eee;" class="infor-personal">
            <tbody>
            <tr>
                <th align="left" class="name" width="20%">Họ và tên:</th>
                <td align="left" class="value" width="30%"><?php echo ($user ? ($user->first_name . ' ' . $user->last_name) : ''); ?></td>
                <th align="left" class="name" width="20%">Chức danh:</th>
                <td align="left" class="value" width="30%"><?php echo ($orgchart ? $orgchart->name : ''); ?></td>
            </tr>
            <tr>
                <th align="left" class="name">Mã nhân viên:</th>
                <td align="left" class="value"><?php echo ($user ? $user->user_nicename : ''); ?></td>
                <th align="left" class="name">Cấp bậc:</th>
                <td align="left" class="value"></td>
            </tr>
            <tr>
                <th align="left" class="name">Giới tính:</th>
                <td align="left" class="value"><?php ; ?></td>
                <th align="left" class="name">Phòng/ Bộ phận:</th>
                <td align="left" class="value"><?php echo ($orgchart ? $orgchart->room : ''); ?></td>
            </tr>
            <tr>
                <th align="left" class="name">Ngày sinh:</th>
                <td align="left" class="value"></td>
                <th align="left" class="name">Khối/ Ban:</th>
                <td align="left" class="value"></td>
            </tr>
            <tr>
                <th align="left" class="name"></th>
                <td align="left" class="value"></td>
                <th align="left" class="name">Ngày vào Công ty:</th>
                <td align="left" class="value"></td>
            </tr>
            </tbody>
        </table>

        <?php
        $kpiForI = [];
        $kpiForIII = [];
        $firstYear = kpi_get_first_year_capacity( TIME_YEAR_VALUE );
        $yearID = !empty( $firstYear ) ? $firstYear['id'] : 0;
        $getCPYearOf = capacity_get_kpi_year_and_total_not_user( $yearID, APPLY_OF_EMPLOYEES, KPI_STATUS_RESULT );
        $getCPYearOf = array_group_by($getCPYearOf, 'type_of');
        if ( ! empty( $getCPYearOf ) ) {
	        $kpiForI   = $getCPYearOf['kpi_nam'];
	        $kpiForIII = $getCPYearOf['kpi_kq'];
        }
        $getCPUser = capacity_get_list_kpi_by_user($yearID, APPLY_OF_EMPLOYEES, $user->ID, KPI_STATUS_RESULT);


        $totalKQI = 0;
        $totalPercentPB = 0;
        $arrTabs = [
	        'total_percent' => ['Finance' => 0, 'Customer' => 0, 'Operate' => 0, 'Development' => 0],
	        'name' => [
		        'Finance' => __('Finance', TPL_DOMAIN_LANG),
		        'Customer' => __('Customer', TPL_DOMAIN_LANG),
		        'Operate' => __('Operate', TPL_DOMAIN_LANG),
		        'Development' => __('Development', TPL_DOMAIN_LANG)
	        ],
        ];

        $usersOfCty = orgchart_get_orgchart_by_alias('congty');
        $arrUserOfCty = [];
        if( !empty( $usersOfCty ) ){
	        foreach ( $usersOfCty as $k => $item ) {
		        $arrUserOfCty[] = $item['ID'];
	        }
        }
        $strUserCty = implode(", ", $arrUserOfCty);
        $totalPercent   = 0;
        $arrTabsCty = [];
        foreach ( ['quy', 'thang'] as $for => $vfor ) {
	        $getYearByKpiTime = year_get_year_by_kpi_time( $vfor );
	        if( !empty( $getYearByKpiTime ) ){
		        $arrKpiTime = [$vfor => ['finance' => 0, 'customer' => 0, 'operate' => 0, 'development' => 0 ] ];
		        foreach ( $getYearByKpiTime as $ktime => $itemTime ){
			        $arrKpiTime[$vfor]['finance'] += $itemTime['finance'];
			        $arrKpiTime[$vfor]['customer'] += $itemTime['customer'];
			        $arrKpiTime[$vfor]['operate'] += $itemTime['operate'];
			        $arrKpiTime[$vfor]['development'] += $itemTime['development'];
		        }
	        }
	        $groupDatasCty     = kpi_get_group_kpi_by_year_orgchart_for_year_of_users( $strTabs, TIME_YEAR_VALUE, $strUserCty, $vfor,'all' );

	        $arrTabsCty = kpi_get_total_percent( $groupDatasCty, 'no' );
	        $totalPercent += array_sum( $arrTabsCty['total_percent'] );
        }
        $strUsers = '';
        if( $member_role == 'nhanvien' ){
	        $search_onlevel = user_get_phong_ban_by_user( $orgchart_id );
	        if( !empty( $search_onlevel ) ){
		        $arrUsers = [];
		        $users = kpi_get_user_info(['id' => 0, 'single' => false, 'exclude' => []]);
		        if( !empty( $users ) && !empty( $users['items'] ) ){
			        foreach ( $users['items'] as $key => $value ){
				        if( $value['parent'] == $search_onlevel->id || $value['orgchart_id'] == $search_onlevel->id ) {
					        $arrUsers[] = $value['id'];
				        }
			        }
			        $strUsers = implode(", ", $arrUsers);
		        }
	        }
        }
        $groupDatasI = kpi_get_group_kpi_by_year_orgchart_for_year_by_lower_grade( $strTabs, TIME_YEAR_VALUE, $strUsers, 'all' );
        $groupDatasType = array_group_by($groupDatasI, 'type');
        $arrTabs = kpi_get_total_percent( $groupDatasI );
        $totalPercentPB = round( array_sum( $arrTabs['total_percent'] ), 1);
        ?>
        <br>
        <h3 class="before-table-title">I. KPI CẢ NĂM CỦA CÔNG TY - PHÒNG BAN</h3>
        <table width="100%" cellspacing="0" cellpadding="5"border="1" style="border-color: #eee;">
            <thead>
            <tr class="head">
                <th class="name" align="center" width="5%" style="<?= $styleTH; ?>">Stt</th>
                <th class="name" align="center" width="25%" style="<?= $styleTH; ?>">Nội dung</th>
                <th class="name" align="center" width="10%" style="<?= $styleTH; ?>">Trọng số</th>
                <th class="name" align="center" width="10%" style="<?= $styleTH; ?>">Kế hoạch</th>
                <th class="name" align="center" width="10%" style="<?= $styleTH; ?>">Thực hiện</th>
                <th class="name" align="center" width="10%" style="<?= $styleTH; ?>">Hoàn thành</th>
                <th class="name" align="center" width="20%" style="<?= $styleTH; ?>">Ghi chú</th>
                <th class="name" align="center" width="10%" style="<?= $styleTH; ?>">Kết quả KPI</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $stt_for_I = 0;
            ?>
            <?php foreach ( $kpiForI as $key => $item ):
	            $stt_for_I ++;
	            $titleI = $item['type_kpis'] == 'cong-ty' ? "Mục tiêu KPI công ty" : "Mục tiêu KPI Phòng ban";
	            $plan = $item['plan'];
	            $percent = $item['percent'];
	            $note = $item['note'];
	            $HT = 0;
	            $totalItem = 0;
	            #if( $actual_gain != '' ){
	            $actual_gain = $stt_for_I == 1 ? $totalPercent : $totalPercentPB;
	            $HT = (double)$actual_gain / $plan;
	            $totalItem = $HT * $percent;
	            $totalKQI += round($totalItem, 2);
	            #}

	            ?>
                <tr>
                    <td width="5%" class="column-1 value" align="center"><?= $stt_for_I; ?></td>
                    <td width="25%" class="column-2 value background-column"><?php esc_attr_e($titleI); ?></td>
                    <td width="10%" class="column-3 value background-column" align="center"><?php esc_attr_e( $percent ); ?>%</td>
                    <td width="10%" class="column-4 value background-column" align="center"><?php esc_attr_e( $plan ); ?>%</td>
                    <td width="10%" class="column-5 value background-column align-center" align="center"><?php esc_attr_e( round( $actual_gain, 1) ); ?>%</td>
                    <td width="10%" class="column-6 value background-column align-center" align="center"><?php echo round( $actual_gain * $plan / 100, 1); ?>%</td>
                    <td width="20%" class="column-7 value background-column align-center"><?php echo strip_tags($note); ?></td>
                    <td width="10%" class="column-8 value align-center" align="center">
			            <?php echo round($totalItem, 2); ?>%
                    </td>
                </tr>
            <?php endforeach; ?>
            <tr class="kpi-of-year" style="background-color: #f1f1f1;">
                <td colspan="7" class="value"><?php _e('Tổng', TPL_DOMAIN_LANG); ?></td>
                <td class="column-8 align-center" align="center">
		            <strong><?php echo $totalKQI; ?>%</strong>
                </td>
            </tr>
            </tbody>
        </table>
        <br>
        <h3 class="before-table-title">II. KPI CẢ NĂM CỦA NHÂN VIÊN</h3>
        <table width="100%" cellspacing="0" cellpadding="5" border="1" style="border-color: #eee;">
            <thead class="head">
                <tr>
                    <th rowspan="" width="5%" align="center" class="column-1 kpi-id align-center" style="<?= $styleTH; ?>"><?php _e('Stt', TPL_DOMAIN_LANG); ?></th>
                    <th rowspan="" width="45%" align="center" class="column-2 kpi-content" style="<?= $styleTH; ?>"><?php _e('Mục tiêu', TPL_DOMAIN_LANG); ?></th>
                    <th rowspan="" width="10%" align="center" class="column-3 kpi-company_plan" style="<?= $styleTH; ?>"><?php _e('Trọng số', TPL_DOMAIN_LANG); ?></th>
                    <th rowspan="" width="10%" align="center" class="column-6 kpi-receive align-center" style="<?= $styleTH; ?>"><?php _e('Kế hoạch', TPL_DOMAIN_LANG); ?></th>
                    <th colspan="" width="10%" align="center" class="column-7 align-center" style="<?= $styleTH; ?>"><?php _e('Thực hiện', TPL_DOMAIN_LANG); ?></th>
                    <th colspan="" width="10%" align="center" class="column-8 align-center" style="<?= $styleTH; ?>"><?php _e('Hoàn thành', TPL_DOMAIN_LANG); ?></th>
                    <th rowspan="" width="10%" align="center" class="column-9 align-center" style="<?= $styleTH; ?>"><?php _e('Kết quả KPI', TPL_DOMAIN_LANG); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $totalKPIOfYearOfMem = 0;
                $totalKQII = 0;
                if (!empty($groupDatas)):
                    foreach ($groupDatas as &$item):
                        if( $item['required'] == 'no' )
                            continue;
                        $sttKPI++;
                        $plan = $item['plan'];
                        $actual_gain = $item['actual_gain'];
                        $planForYear = $item['plan_for_year'];
                        $percentForMonth = 0;
                        $percentForYear = 0;
                        $total_actual_gain = kpi_sum_kpi_by_parent( $item['id'] );
                        if( !empty( $total_actual_gain ) && array_key_exists('total_actual_gain', $total_actual_gain) ){
                            $ttactual_gain = $total_actual_gain['total_actual_gain'];
                        }else{
                            $ttactual_gain = '';
                        }
                        ?>
                        <tr>
                            <td width="5%" class="column-1 kpi-id align-center" align="center">
                                <?php echo $sttKPI;?>
                            </td>
                            <td width="45%" class="column-2 kpi-content value background-column"><?php echo $item['post_title']; ?></td>
                            <td width="10%" class="column-3 kpi-company_plan background-column" align="center"><?php echo $item['percent']; ?>%</td>
                            <td width="10%" class="column-6 kpi-receive align-center background-column" align="center"><?php echo esc_attr($plan) . " " . getUnit( $item['unit'] ); ?></td>
                            <td width="10%" class="column-7 align-center background-column" align="center">
                                <?php
                                if( $ttactual_gain !== '' ):
                                    $formulas = maybe_unserialize( $item['formulas'] );
                                    $percentForMonth = getPercentForMonth( $item['unit'], $item['formula_type'], $formulas, $plan, $ttactual_gain );
                                    $KQKPI = $percentForMonth * (int)$item['percent'] / 100;
	                                $totalKQII += $KQKPI;
                                    $totalKPIOfYearOfMem += round( $KQKPI, 1 );
                                endif;
                                echo esc_attr( $ttactual_gain ) . " " . getUnit( $item['unit'] );
                                ?>

                            </td>
                            <td width="10%" colspan="" class="column-8 kpi-percent align-center background-column" align="center"><?php echo round( $percentForMonth, 1 ); ?>%</td>
                            <td width="10%" class="column-9 align-center" align="center">
                                <?php
                                echo round($KQKPI, 1 );
                                ?>%
                            </td>
                        </tr>
                    <?php endforeach;
                endif;
                ?>
                <tr class="kpi-of-year" style="background-color: #f1f1f1;">
                    <td colspan="6" class="name"><?php _e('Tổng', TPL_DOMAIN_LANG); ?></td>
                    <td class="column-8 align-center" align="center">
		                <strong><?= round($totalKQII, 1); ?>%</strong>
                    </td>
                </tr>
            </tbody>
        </table>
        <br>
        <h3 class="before-table-title">III. ĐÁNH GIÁ NĂNG LỰC NHÂN VIÊN</h3>
        <table width="100%" cellspacing="0" cellpadding="5" border="1" style="border-color: #eee;">
            <thead class="head" style="<?= $styleTH; ?>">
                <tr>
                    <th width="5%" align="center" style="<?= $styleTH; ?>" class="column-1 align-center"><?php _e('Stt'); ?></th>
                    <th width="41%" align="center" style="<?= $styleTH; ?>" class="column-2"><?php _e('Chỉ tiêu mong đợi'); ?></th>
                    <th width="8%" align="center" style="<?= $styleTH; ?>" class="column-3 align-center"><?php _e('Cần cải thiện'); ?></th>
                    <th width="8%" align="center" style="<?= $styleTH; ?>" class="column-4 align-center"><?php _e('Hiệu quả'); ?></th>
                    <th width="8%" align="center" style="<?= $styleTH; ?>" class="column-5 align-center"><?php _e('Nổi bật'); ?></th>
                    <th width="10%" align="center" style="<?= $styleTH; ?>" class="column-6 align-center"><?php _e('Xu hướng'); ?></th>
                    <th width="10%" align="center" style="<?= $styleTH; ?>" class="column-7 align-center"><?php _e('Ghi chú'); ?></th>
                    <th width="10%" align="center" style="<?= $styleTH; ?>" class="column-8 align-center"><?php _e('Kết quả'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $stt = 0;
                $total_level_1 = 0;
                $total_level_2 = 0;
                $total_level_3 = 0;
                foreach ($getCPUser as $key => $item):
                    $post_title = $item['post_title'];
                    $capacityID = $item['id'];
                    $actual_gain = $item['actual_gain'];
                    $level_1 = $item['level_1'];
                    $level_2 = $item['level_2'];
                    $level_3 = $item['level_3'];
                    $total_level_1 += $level_1;
                    $total_level_2 += $level_2;
                    $total_level_3 += $level_3;
                    $note = strip_tags($item['note']);
                    if( $actual_gain == '' ){
                        $trend = "";
                    }else{
                        $trend = "";
                        $actual_gain = (int)$actual_gain;
                        if( $actual_gain <= $level_1 || ( $level_1 < $actual_gain && $actual_gain < $level_2 ) ){
                            $trend = $images_level_1;
                        }elseif( $level_2 <= $actual_gain && $actual_gain < $level_3 ){
                            $trend = $images_level_2;
                        }elseif($level_3 <= $actual_gain){
                            $trend = $images_level_3;
                        }
                        $totalActualGain += $actual_gain;
                    }
                    ?>
                    <tr>
                        <td width="5%" class="column-1 align-center" align="center"><?php $stt++; echo $stt; ?></td>
                        <td width="41%" class="column-2 value background-column"><?= $post_title; ?></td>
                        <td width="8%" class="column-3 align-center background-column" align="center"><?php esc_attr_e($level_1); ?>%</td>
                        <td width="8%" class="column-4 align-center background-column" align="center"><?php esc_attr_e($level_2); ?>%</td>
                        <td width="8%" class="column-5 align-center background-column" align="center"><?php esc_attr_e($level_3); ?>%</td>
                        <td width="10%" class="column-6 align-center background-column" align="center">
                            <img src="<?php echo $trend; ?>" />
                        </td>
                        <td width="10%" class="column-7 align-center value background-column">
	                        <?php echo strip_tags($note); ?>
                        </td>
                        <td width="10%" class="column-8 align-center" align="center">
                            <?php esc_attr_e($actual_gain); ?>%
                        </td>
                    </tr>
                    <?php
                endforeach;
                ?>
                <tr class="kpi-of-year" style="background-color: #f1f1f1;">
                    <td colspan="7" class="name"><?php _e('Tổng', TPL_DOMAIN_LANG); ?></td>
                    <td class="column-8 align-center" align="center">
                        <strong><?= round($totalActualGain, 1); ?>%</strong>
                    </td>
                </tr>
            </tbody>
        </table>
        <br>
        <h3 class="before-table-title">IV. KẾT QUẢ CỦA QUÁ TRÌNH ĐÁNH GIÁ</h3>
        <table width="100%" cellspacing="0" cellpadding="5" border="1" style="border-color: #eee;">
            <thead class="head" style="<?= $styleTH; ?>">
                <tr>
                    <th width="20%" class="column-1 align-center name" align="center" style="<?= $styleTH; ?>"><?php _e('Nội dung'); ?></th>
                    <th width="20%" class="column-2" align="center" style="<?= $styleTH; ?>"><?php _e('Tổng điểm'); ?></th>
                    <th width="20%" class="column-3 align-center name" align="center" style="<?= $styleTH; ?>"><?php _e('Trọng số'); ?></th>
                    <th width="20%" class="column-4 align-center name" align="center" style="<?= $styleTH; ?>"><?php _e('Kết quả KPI'); ?></th>
                    <th width="20%" class="column-5 align-center name" align="center" style="<?= $styleTH; ?>"><?php _e('Danh hiệu'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $totalRT = 0;
                $it = 0;
                $totalIII = 0;
                foreach ( $kpiForIII as $key => $item ):
                    $it++;
                    if( $it == 1 ){
                        $total = round($totalKQI, 2);
                    }elseif( $it == 2 ){
                        $total = $totalKPIOfYearOfMem;
                    }else{
                        $total = round($totalActualGain,1);
                    }
                    $percent = (int)$item['percent'];
                    $kq = $total * $percent / 100;
                    $totalRT += $kq;
                endforeach;
                    if( $totalRT <= 50 ){
                        $textRating = 'Kém';
                    }elseif( $totalRT > 50 && $totalRT <= 60 ){
                        $textRating = 'Cần cải thiện';
                    }elseif( $totalRT > 60 && $totalRT <= 70 ){
                        $textRating = 'Đạt';
                    }elseif( $totalRT > 70 && $totalRT <= 90 ){
                        $textRating = 'Rất tốt';
                    }else{
                        $textRating = 'Xuất sắc';
                    }
                ?>
            <?php if ( ! empty( $kpiForIII ) ): ?>
                <?php
                foreach ( $kpiForIII as $key => $item ):
                    $stt_for_III ++;
                    $td = '';
                    if( $stt_for_III == 1 ){
                        $td = '<td width="20%" class="column-4 align-center width10" rowspan="4" align="center" valign="middle"><strong style="font-size: 25px;color:red">'.esc_attr($textRating).'</strong></td>';
                        $total = round($totalKQI, 2);
                    }elseif( $stt_for_III == 2 ){
                        $total = $totalKPIOfYearOfMem;
                    }elseif( $stt_for_III == 3 ){
                        $total = (int)$totalActualGain;
                    }
                    $percent = (int)$item['percent'];
                    $kq = $total * $percent / 100;
                    $totalIII += $kq;
                    ?>
                    <tr>
                        <td width="20%" class="column-1 name">Điểm phần <?= $stt_for_III; ?></td>
                        <td width="20%" class="column-2 align-center width10" align="center"><?= $total; ?>%</td>
                        <td width="20%" class="column-3 align-center width10" align="center"><?php echo $percent; ?>%</td>
                        <td width="20%" class="column-4 align-center width10" align="center"><?php echo round($kq,3); ?>%</td>
                        <?= $td; ?>
                    </tr>
                    <?php
                endforeach;
                endif;
                ?>
                <tr class="kpi-of-year" style="background-color: #f1f1f1;">
                    <td colspan="3" class="name"><?php _e('Tổng', TPL_DOMAIN_LANG); ?></td>
                    <td class="column-8 align-center"  align="center">
                        <strong><?= $totalIII; ?>%</strong>
                    </td>
                </tr>
            </tbody>
        </table>
        <br>
        <h3 class="before-table-title">V. NHẬN XÉT CHUNG</h3>
        <table width="100%" cellspacing="0" cellpadding="5" border="1" style="border-color: #eee;">
            <thead class="head" style="<?= $styleTH; ?>">
                <tr>
                    <th class="name" width="5%">Stt</th>
                    <th class="name" width="20%">Tên</th>
                    <th class="name" width="75%">Nội dung</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td width="5%" align="center">1</td>
                    <td width="20%" class="name">Xu hướng năng lực</td>
                    <td width="75%" class="background-column" style="padding: 10px 0;" align="center">
                        <?php
                        $capNX = capacity_get_nx_by_user( $user->ID, $yearID );
                        if( empty( $capNX ) ){
	                        $capNX = ['trend' => '', 'note' => '', 'id' => ''];
                        }
                        $image_dixuong = get_template_directory_uri() . '/assets/images/di-xuong.png';
                        $image_dixuong = base64_encode( file_get_contents( $image_dixuong ) );
                        $image_dixuong = "data:image/png;base64,".$image_dixuong;

                        $image_ondinh = get_template_directory_uri() . '/assets/images/on-dinh.png';
                        $image_ondinh = base64_encode( file_get_contents( $image_ondinh ) );
                        $image_ondinh = "data:image/png;base64,".$image_ondinh;

                        $image_dilen = get_template_directory_uri() . '/assets/images/di-len.png';
                        $image_dilen = base64_encode( file_get_contents( $image_dilen ) );
                        $image_dilen = "data:image/png;base64,".$image_dilen;
                            if( $capNX['trend'] == 'di-xuong' ){
                                #can cai thien
                                $textXH = "<strong>Đi xuống</strong><br><img width=\"100\" src=\"{$image_dixuong}\" />";
                            }elseif( $capNX['trend'] == 'on-dinh' ){
                                #hieu qua
	                            $textXH = "<strong>Ổn định</strong><br><img width=\"100\" src=\"{$image_ondinh}\" />";
                            }elseif( $capNX['trend'] == 'di-len' ){
                                #noi bat
	                            $textXH = "<strong>Đi lên</strong><br><img width=\"100\" src=\"{$image_dilen}\" />";
                            }else{
	                            $textXH = '';
                            }
                            echo $textXH;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td width="5%" align="center">2</td>
                    <td width="20%" class="name">Nhận xét khác</td>
                    <td width="75%" class="value background-column"><?php
                            echo html_entity_decode( $capNX['note'] );
                            #echo html_entity_decode($capNX['note']);exit;
                        ?>
                    </td>
                </tr>
            </tbody>
        </table>
        <br><br><br>
        <table width="100%" cellspacing="0" cellpadding="0" style="margin-top:50px;">
            <tfoot>
            <tr>

                <td align="center" style="font-weight: bold;">Người nhận tiêu chí đánh giá</td>
                <td align="center" style="font-weight: bold;">Người duyệt tiêu chí đánh giá</td>
                <td align="center" style="font-weight: bold;">Người duyệt kết quả đánh giá</td>
            </tr>
            <tr style="min-height: 100px">

                <td align="center">Chức danh</td>
                <td align="center">Chức danh</td>
                <td align="center">Chức danh</td>
            </tr>
            <tr style="min-height: 100px">
                <td><br><br><br><br><br><br><br></td>
                <td align="center">&nbsp;</td>
                <td align="center">&nbsp;</td>
            </tr>
            <tr>
                <td align="center">Ngày ...... / ...... / ......</td>
                <td align="center">Ngày ...... / ...... / ......</td>
                <td align="center">Ngày ...... / ...... / ......</td>
            </tr>
            </tfoot>
        </table>

    </div>
</body>
</html>
<?php


$html = ob_get_clean();
$fileName = "phieu-danh-gia-kpi-cap-nhan-vien-{$user->user_nicename}-theo-{$chart_type}.pdf";
if( !empty($_GET['html']) ) {
    #echo $html;
	save_pdf( $fileName, $html, $logo_base64, 16, "I", $width, $height );
} else {
    /*phptopdf([
        'action' => 'download',
        "source_type" => 'html',
        "source" => $html,
        # "save_directory" => __DIR__ . '/',
        "file_name" => "phieu-danh-gia-kpi-cap-nhan-vien-{$user->user_nicename}-theo-{$chart_type}.pdf"
    ]);*/

	save_pdf( $fileName, $html, $logo_base64, 16, "D", $width, $height );
}
