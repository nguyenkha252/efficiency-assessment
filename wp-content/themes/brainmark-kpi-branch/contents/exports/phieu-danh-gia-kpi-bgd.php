<?php

/**
 * Created by PhpStorm.
 * User: 
 * Date: 3/15/18
 * Time: 10:21
 */
# /wp-content/themes/brainmark-kpi-branch/contents/exports/phieu-danh-gia-kpi-quan-ly.php
global $website_settings, $logo_url;
# require_once __DIR__ . '/../../../../../wp-load.php';
$user = wp_get_current_user();
$orgchart = user_load_orgchart( $user );
$orgchart_id = $orgchart ? $orgchart->id : 0;
#$firstYear = kpi_get_first_year();
$firstYear = year_get_year_by_chart_id( $orgchart_id, TIME_YEAR_VALUE );
$currentLevel = 1;
$tabs = [
    'finance' => [
        'title' => __('Finance', TPL_DOMAIN_LANG),
        'type' => 'Finance',
        'percent' => $firstYear ? $firstYear['finance'] : '',
        'time_value' => TIME_PRECIOUS_VALUE,
        'year_id' => $firstYear
    ],
    'customer' => [
        'title' => __('Customer', TPL_DOMAIN_LANG),
        'type' => 'Customer',
        'percent' => $firstYear ? $firstYear['customer'] : '',
        'time_value' => TIME_PRECIOUS_VALUE,
        'year_id' => $firstYear
    ],
    'operate' => [
        'title' => __('Operate', TPL_DOMAIN_LANG),
        'type' => 'Operate',
        'percent' => $firstYear ? $firstYear['operate'] : '',
        'time_value' => TIME_PRECIOUS_VALUE,
        'year_id' => $firstYear
    ],
    'development' => [
        'title' => __('Development', TPL_DOMAIN_LANG),
        'type' => 'Development',
        'percent' => $firstYear ? $firstYear['development'] : '',
        'time_value' => TIME_PRECIOUS_VALUE,
        'year_id' => $firstYear
    ]
];

$chart_type = sanitize_title($GLOBALS['chartCurrent'] ? $GLOBALS['chartCurrent']['room'] : '');
$html = "";
#$handle = fopen($logo_url, "rb");
#$contents = stream_get_contents($handle);
$logo_base64 = base64_encode(file_get_contents( $logo_url ) );
$imsize = @getimagesize($logo_url);
$width = 50;
$height = '';
if( $imsize ){
	$width_temp = $imsize[0];
	$height_temp = $imsize[1];
	$height = $height_temp * $width / $width_temp;
}

#$logo = "data:image/png;base64,".$logo_base64;
# $logo = "http://sadeco.kpibrainmark.com/wp-content/themes/brainmark-kpi-branch/inc/tmp/logo.png";
$textPrecious =
	(!empty($_GET['quy']) ? ( !empty($_GET['quy']) ? ('Quý ' . $_GET['quy']) : ('Tháng' . $_GET['thang']) ) : '  ...  ') . " / " .
	($firstYear ? $firstYear['year'] : '201...') . "";
if( !isset( $_GET['quy'] ) || empty( $_GET['quy'] ) ){
    $textPrecious = "Năm " . ($firstYear ? $firstYear['year'] : '201...');
}
$title = "PHIẾU ĐÁNH GIÁ KPIs CẤP ". ($GLOBALS['chartCurrent'] ? mb_strtoupper($GLOBALS['chartCurrent']['room']) : '') . " - " . $textPrecious;

# 2480px

global $wpdb;
$totalPercent = 0;
$percentResults = [];
$orgCty = orgchart_get_congty();
$arrOrgs = [];
if( !empty( $orgCty ) ){
	foreach ( $orgCty as $key => $value ){
		array_push($arrOrgs, $value['id'] );
	}
	$arrOrgs = implode(", ", $arrOrgs);
}

foreach($tabs as $key => $tab):
    $percentResults[$key] = ['total' => 0];
    require_once THEME_DIR . '/ajax/get_user_info.php';
    $users        = kpi_get_user_info( [ 'id' => 0, 'single' => false, 'exclude' => [] ] );
    $arrUsers = [$user->ID];
    if ( ! empty( $users ) && ! empty( $users['items'] ) ) {
        foreach ( $users['items'] as $k => $value ) {
            if ( $value['parent'] == $orgchart->id ) {
                $arrUsers[] = $value['id'];
            }
        }
        $arrUsers = implode( ", ", $arrUsers );
    }
	$groupDatas = kpi_get_group_kpi_by_year_orgchart_for_year_by_lower_grade_ceo( $wpdb->prepare("%s", $tab['type']), TIME_YEAR_VALUE, $arrOrgs, 'all', 'no' );
    $percentResults[$key]['data'] = $groupDatas;
    # echo '<pre>'; var_dump($wpdb->last_query, $wpdb->last_error, $percentResults); echo '</pre>'; exit;
	#echo '<pre>'; print_r($percentResults); echo '</pre>';
    if( !empty($groupDatas) ):
        # $countItem = count($groupDatas);
        foreach ($groupDatas as &$item):
            $plan = $item['plan'];
            #$actual_gain = $item['actual_gain'];
            $planForYear = $item['plan_for_year'];
            $percentForMonth = 0;
            $percentForYear = 0;
	        $id              = $item['id'];
	        #$getYear = kpi_get_year_by_id( $item['year_id'] );
	        #$actual_gain = 0;
	        $percentForMonth = 0;
	        $plan = $item['plan'];
	        if( !empty( $item['formulas'] ) ){
		        $formulas = maybe_unserialize( $item['formulas'] );
	        }else{
		        $formulas = [];
	        }
	        $actual_gain = get_actual_gain( $item, 'no' );
	        if( $actual_gain !== '' && $item['unit'] != KPI_UNIT_THOI_GIAN ):
		        $percentForMonth = getPercentForMonth( $item['unit'], $item['formula_type'], $formulas, $plan, $actual_gain );
		        $percentForMonth = $percentForMonth * $item['percent'] / 100;
            elseif( $item['unit'] == KPI_UNIT_THOI_GIAN ):
		        $percentForMonth = $actual_gain * $item['percent'] / 100;
	        endif;
	        if ( $actual_gain !== '' ):
		        $formulas        = maybe_unserialize( $item['formulas'] );
		        $percentForMonth = getPercentForMonth( $item['unit'], $item['formula_type'], $formulas, $plan, $actual_gain );
	        endif;
            $percentResults[$key][$id] = doubleval($percentForMonth);
            $percentResults[$key]['total'] += doubleval($percentForMonth);
        endforeach;
    endif;
    $percentResults[$key]['percent'] = doubleval($tab['percent']);
    $percentResults[$key]['percent'] = ($percentResults[$key]['percent'] * $percentResults[$key]['total']) / 100;
    $totalPercent += $percentResults[$key]['percent'];
endforeach;
# echo '<pre>'; var_dump($percentResults); echo '</pre>'; exit;
#echo '<pre>'; print_r($percentResults); echo '</pre>';
/*<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title><?php echo $title; ?></title>
        <style>
            tr.group > td {
                font-weight: bold;
                font-size: 20px;
            }
        </style>
    </head>
    <body>*/
ob_start();
$arrKPI = [];
?>
    <style>
        tr.group > td {
            font-weight: bolder;
            font-size: 12px;
        }
        table th{
            font-weight: bolder;
        }
        .logo-row-1-1{
            padding-right: 20px;
        }
        .logo-row-1-2{
            font-weight: normal;
        }
        .logo-row-2-1{
            padding-top: 5px;
        }
        table.table-1{
            margin-top: 50px;
            border-color: #eee;

        }
        .head th{
            vertical-align: middle;
            padding: 5px 0;
            background-color: #e7e6e6;
        }
    </style>
<div style="width: 1024px; margin: 0 auto;">
    <table width="100%" border="" cellspacing="0" cellpadding="0" bgcolor="">
        <tbody>
        <tr class="logo-row-1" style="">
            <th rowspan="3"  width="200" class="logo-row-1-1" align="left" style=""><img width="180" src="<?php #echo $logo; ?>"></th>
            <th align="left" class="logo-row-1-2" style=""><?php echo $website_settings['title_website']; ?></th>
        </tr>
        <tr class="logo-row-2">
            <th align="left" width="100%" class="logo-row-2-1" style=""><?php echo $title; ?></th>
        </tr>
        <tr class="logo-row-3">
            <th align="left" width="100%">Mã số: ...</th>
        </tr>
        </tbody>
    </table>
    <br>
    <br>
    <br>
    <h3 class="before-table-title"></h3>
    <table width="100%" border="1" cellspacing="0" cellpadding="5" class="table-1">
        <thead>
        <tr class="head">
            <?php $styleTH = "";?>
            <th align="center" valign="middle" width="5%"><br>Stt</th>
            <th align="center" width="33%" style="<?= $styleTH; ?>">Mục tiêu</th>
            <th align="center" width="10%" style="<?= $styleTH; ?>">Trọng số</th>
            <th align="center" width="15%" style="<?= $styleTH; ?>">Thời điểm nhận kết quả</th>
            <th align="center" width="10%" style="<?= $styleTH; ?>">Kế hoạch</th>
            <th align="center" width="10%" style="<?= $styleTH; ?>">Thực hiện</th>
            <th align="center" width="9%" style="<?= $styleTH; ?>">Hoàn thành</th>
            <th align="center" width="8%">KẾT QUẢ</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $styleTD = "padding: 10px;";
        $styleTDBG = "background-color:#fef6cc;";
        foreach($tabs as $key => $tab):
	        $arrKPI[$key] = ['total' => $tab['percent'], 'total_item' => 0];
            ?>
            <tr class="group">
                <td colspan="9" width="100%" align="left"><?php echo mb_strtoupper($tab['title']). " ({$tab['percent']}%)"; ?></td>
            </tr>
            <?php
            $groupDatas = &$percentResults[$key]['data'];
            # get_kpi_by_year_orgchart_for_year( $tab['type'], TIME_YEAR_VALUE, $user->ID, 'yes' );
            if (!empty($groupDatas)):
                $idx = 1;
                foreach ($groupDatas as &$item):
                    $plan = $item['plan'];
                    $actual_gain = get_actual_gain( $item, 'no' );
                    $planForYear = $item['plan_for_year'];
                    $percentForMonth = 0;
                    $percentForYear = 0;
                    #$HT = round($percentResults[$key][$item['id']], 1);
	                if( !empty( $item['formulas'] ) ){
		                $formulas = maybe_unserialize( $item['formulas'] );
	                }else{
		                $formulas = [];
	                }
	                if ( $actual_gain != '' ):
		                $formulas        = maybe_unserialize( $item['formulas'] );
		                $percentForMonth = getPercentForMonth( $item['unit'], $item['formula_type'], $formulas, $plan, $actual_gain );
	                endif;
	                $HT = round($percentForMonth, 1);
                    $KQ = round($percentForMonth * $item['percent'] / 100, 1);
	                $arrKPI[$key]['total_item'] += $KQ;
                    ?>
                    <tr class="item">
                        <td width="5%" align="center" style="<?= $styleTD; ?>"><?php echo $idx ?>&nbsp;</td>
                        <td width="33%" align="left" style="<?= $styleTD; ?> <?= $styleTDBG; ?>"><?php echo str_replace(str_split("\|"), "",$item['post_title']); ?></td>
                        <td width="10%" align="center" style="<?= $styleTD; ?>"><?php echo $item['percent']; ?>%</td>
                        <td width="15%" align="center" style="<?= $styleTD; ?> <?= $styleTDBG; ?>"><?php echo kpi_format_date( substr($item['receive'], 0, 10) ) ?></td>
                        <td width="10%" align="center" style="<?= $styleTD; ?>"><?php echo $item['plan']; ?> <?php echo !empty($item['unit']) ? getUnit($item['unit']) : ''; ?></td>
                        <td width="10%" align="center" style="<?= $styleTD; ?> <?= $styleTDBG; ?>"><?php echo "{$actual_gain}"; ?> <?php echo !empty($item['unit']) ? getUnit($item['unit']) : ''; ?></td>
                        <td width="9%" align="center" style="<?= $styleTD; ?>"><?php echo "{$HT}"; ?>%</td>
                        <td width="8%" align="center" style="<?= $styleTDBG; ?>"><?php echo $KQ; ?>%</td>
                    </tr>
                    <?php
                    $idx++;
                endforeach;
            endif;
        endforeach;
        ?>
        </tbody>
        <tfoot>
            <tr style="background-color: #e7e6e6;">
                <td colspan="7" width="92%" align="left" style="padding: 10px;">TỔNG:</td>
                <td width="8%" align="center" style="font-weight: bold;">
                    <?php
                    $total = 0;
                    if( !empty( $arrKPI ) ){
                        foreach ( $arrKPI as $key => $item ){
                            $total += $item['total_item'] * $item['total'] / 100;
                        }
                    } ?>
		            <?php echo round($total,1); ?>%</td>
            </tr>
        </tfoot>
    </table>
    <br><br><br>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td align="center" style="font-weight: bold;">Người lập</td>
            <td align="center" style="font-weight: bold;">Xem xét</td>
            <td align="center" style="font-weight: bold;">Phê duyệt</td>
        </tr>
        <tr style="min-height: 100px">
            <td align="center">Giám đốc HCNS</td>
            <td align="center">Phó Tổng Giám đốc</td>
            <td align="center">Tổng Giám đốc</td>
        </tr>
        <tr style="min-height: 100px">
            <td><br><br><br><br><br><br><br></td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
        </tr>
        <tr>
            <td align="center">Ngày ...... / ...... / ......</td>
            <td align="center">Ngày ...... / ...... / ......</td>
            <td align="center">Ngày ...... / ...... / ......</td>
        </tr>
        </tbody>
    </table>
</div>

<?php
/*  </body>
    </html>*/
$html = ob_get_clean();
$_pdf_filename = "phieu-danh-gia-kpi-cap-congty-{$user->user_nicename}-theo-{$chart_type}.pdf";
if( !empty($_GET['html']) ) {
    #echo $html;
	save_pdf($_pdf_filename, $html, $logo_base64, 12, "I", $width, $height);
} else {

    /*phptopdf([
        'action' => 'download',
        "source_type" => 'html',
        "source" => $html,
        # "save_directory" => __DIR__ . '/',
        "file_name" => "phieu-danh-gia-kpi-cap-cong-ty-{$user->user_nicename}-theo-{$chart_type}.pdf"
    ]); */
	save_pdf($_pdf_filename, $html, $logo_base64);

}
;