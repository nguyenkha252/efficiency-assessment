<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 3/15/18
 * Time: 10:21
 */
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-formulas.php';
require_once THEME_DIR . '/inc/lib-behavior.php';
# /wp-content/themes/brainmark-kpi-branch/contents/exports/phieu-danh-gia-kpi-quan-ly.php
global $website_settings, $logo_url;
# require_once __DIR__ . '/../../../../../wp-load.php';
$user = wp_get_current_user();
$orgchart = user_load_orgchart( $user );
$orgchart_id = $orgchart ? $orgchart->id : 0;
#$firstYear = kpi_get_first_year();
$firstYear = year_get_year_by_chart_id( $orgchart_id, TIME_YEAR_VALUE );
$currentLevel = 1;
$tabs = [
    'thaidohanhvi' => [
        'title' => __('THÁI ĐỘ - HÀNH VI', TPL_DOMAIN_LANG),
        'type' => 'Finance',
        'percent' => $firstYear ? $firstYear['finance'] : '',
        'time_value' => TIME_PRECIOUS_VALUE,
        'year_id' => $firstYear
    ],
    /*'muctieucongviec' => [
        'title' => __('Customer', TPL_DOMAIN_LANG),
        'type' => 'Customer',
        'percent' => $firstYear ? $firstYear['customer'] : '',
        'time_value' => TIME_PRECIOUS_VALUE,
        'year_id' => $firstYear
    ]*/
];

$chart_type = sanitize_title($GLOBALS['chartCurrent'] ? $GLOBALS['chartCurrent']['room'] : '');
$html = "";
$logo_base64 = base64_encode( file_get_contents($logo_url ) );
$imsize = @getimagesize($logo_url);
$width = 50;
$height = '';
if( $imsize ){
	$width_temp = $imsize[0];
	$height_temp = $imsize[1];
	$height = $height_temp * $width / $width_temp;
}
$logo = "data:image/png;base64,".$logo_base64;
# $logo = "http://sadeco.kpibrainmark.com/wp-content/themes/brainmark-kpi-branch/inc/tmp/logo.png";

$title = "PHIẾU ĐÁNH GIÁ KPIs CÁ NHÂN - " .
    (!empty($_GET['quy']) ? ( !empty($_GET['quy']) ? ('Quý ' . $_GET['quy']) : ('Tháng' . $_GET['thang']) ) : '  ...  ') . " / " .
    ($firstYear ? $firstYear['year'] : '201...') . "";

$textPrecious =
	(!empty($_GET['quy']) ? ( !empty($_GET['quy']) ? ('Quý ' . $_GET['quy']) : ('Tháng' . $_GET['thang']) ) : '  ...  ') . " / " .
	($firstYear ? $firstYear['year'] : '201...') . "";
if( !isset( $_GET['quy'] ) || empty( $_GET['quy'] ) ){
	$textPrecious = "Năm " . ($firstYear ? $firstYear['year'] : '201...');
}
if( isset( $_GET['thang'] ) && !empty( $_GET['thang'] ) ){
	$textPrecious = "Tháng " . $_GET['thang'] . '/' . ($firstYear ? $firstYear['year'] : '201...');
}
$title = "PHIẾU ĐÁNH GIÁ KPIs CÁ NHÂN " . " - " . $textPrecious;
# 2480px

global $wpdb;
$totalPercent = 0;
$percentResults = [];

#KPI
if( empty( TIME_PRECIOUS_VALUE ) ){
    $tabsarr = ["'Finance'", "'Customer'", "'Operate'", "'Development'"];
    $strTabs = implode( ", ", $tabsarr );
    $kpiForStaff = kpi_get_group_kpi_by_year_orgchart_for_year_by_lower_grade_nv($strTabs, TIME_YEAR_VALUE, $user->ID);
}else{
    if( !empty( TIME_MONTH_VALUE ) ){
	    $kpiForStaff = get_kpi_by_year_orgchart_for_month_and_status($firstYear['year'], TIME_MONTH_VALUE, $user->ID);

    }else{
	    $kpiForStaff = get_kpi_by_year_orgchart_for_precious_and_status($firstYear['year'], TIME_PRECIOUS_VALUE, $user->ID);
    }

}

foreach($tabs as $key => $tab):
    $percentResults[$key] = ['total' => 0];
    $groupDatas = $kpiForStaff;
    $percentResults[$key]['data'] = $groupDatas;
    # echo '<pre>'; var_dump($wpdb->last_query, $wpdb->last_error, $percentResults); echo '</pre>'; exit;

    if( !empty($groupDatas) ):
        # $countItem = count($groupDatas);
        foreach ($groupDatas as &$item):
            $plan = $item['plan'];
            #$actual_gain = $item['actual_gain'];
            $planForYear = $item['plan_for_year'];
	        $unit = $item['unit'];
            $percentForMonth = 0;
            $percentForYear = 0;
	        $formulas = maybe_unserialize( $item['formulas'] );
	        $formula_type = $item['formula_type'];
	        if( empty( TIME_PRECIOUS_VALUE ) ){
	            $actual_gain = 0;
		        if( $unit != KPI_UNIT_THOI_GIAN ) {
			        $total_actual_gain = kpi_sum_kpi_by_parent( $item['id'] );
			        #print_r( $total_actual_gain );
			        if ( ! empty( $total_actual_gain ) && array_key_exists( 'total_actual_gain', $total_actual_gain ) ) {
				        $actual_gain = $total_actual_gain['total_actual_gain'];
			        } else {
				        $actual_gain = '';
			        }
		        }else {
			        $results = kpi_get_all_kpi_by_parent( $item['id'] );
			        if ( ! empty( $results ) ) {
				        $arrPercentForTime = [];
				        foreach ( $results as $ks => $vs ) {
					        $ttplan = $vs['plan'];
					        if ( $vs['unit'] == KPI_UNIT_THOI_GIAN ) {

						        if ( ! empty( $vs['actual_gain'] ) ) {
							        $arrPercentForDateTime[] = $vs['actual_gain'];
						        }
						        $arrPercentForTime[] = getPercentForMonth( $vs['unit'], $item['formula_type'], $formulas, $ttplan, $vs['actual_gain'] );
					        } else {
						        $actual_gain += $vs['actual_gain'];
					        }
				        }
				        if ( ! empty( $arrPercentForTime ) ) {
					        $actual_gain = array_sum( $arrPercentForTime ) / count( $arrPercentForTime );
				        }
			        } else {
				        $actual_gain = '';
			        }
		        }
		        if( $actual_gain !== '' && $item['unit'] != KPI_UNIT_THOI_GIAN ):
			        $percentForMonth = getPercentForMonth( $unit, $item['formula_type'], $formulas, $plan, $actual_gain );
                elseif( $item['unit'] == KPI_UNIT_THOI_GIAN ):
			        $percentForMonth = $actual_gain;
		        endif;
            }else{
	            $actual_gain = $item['actual_gain'];

		        if( $actual_gain !== '' ):
			        #$formulas = maybe_unserialize( $item['formulas'] );

			        $percentForMonth = getPercentForMonth( $item['unit'], $item['formula_type'], $formulas, $plan, $actual_gain );
		        endif;
	        }

            /*if( !empty( $actual_gain ) ):
                $formulas = maybe_unserialize( $item['formulas'] );
                $formula_type = $item['formula_type'];
                $percentForMonth = getPercentForMonth( $item['unit'], $formula_type, $formulas, $plan, $actual_gain );
            endif;*/

            $percentResults[$key][$item['id']] = doubleval($percentForMonth);
            $percentResults[$key][$item['id'].'_actual_gain'] = $actual_gain;
            $percentResults[$key]['total'] += doubleval($percentForMonth);
        endforeach;
    endif;
    $percentResults[$key]['percent'] = doubleval($tab['percent']);
    $percentResults[$key]['percent'] = ($percentResults[$key]['percent'] * $percentResults[$key]['total']) / 100;
    $totalPercent += $percentResults[$key]['percent'];
endforeach;
# echo '<pre>'; var_dump($percentResults); echo '</pre>'; exit;
#echo '<pre>'; print_r($percentResults); echo '</pre>';
ob_start();
?>
    <style>
        tr.group > td {
            font-weight: bolder;
            font-size: 12px;
        }
        table th{
            font-weight: bolder;
        }

        .head th{
            vertical-align: middle;
            padding: 5px 0;
            background-color: #e7e6e6;
        }
        .background-column{
            background-color:#fef6cc;
        }
        .name, .value, .head th{
            padding: 5px 10px;
        }
        .before-table-title{
            margin:0;
            padding-top:30px;
            padding-bottom: 10px;
            color:red;
        }
    </style>
    <div style="width: 1024px; margin: 0 auto">
        <table width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr class="logo-row-1" style="">
                <th rowspan="3" width="30%" align="left" style="padding-right: 20px;"><img width="180" src="<?php #echo $logo; ?>"></th>
                <th align="left" width="70%" style="font-weight: normal;"><?php echo $website_settings['title_website']; ?></th>
            </tr>
            <tr class="logo-row-2">
                <th align="left" style="padding-top: 5px;"><?php echo $title; ?></th>
            </tr>
            <tr class="logo-row-3">
                <th align="left">Mã số: ...</th>
            </tr>
            </tbody>
        </table>
        <br><br><br>
	    <?php $styleTH = "background-color: #e7e6e6;padding: 10px 0;";?>
        <table width="100%" border="1" cellspacing="0" cellpadding="5" style="margin-top: 50px;border-color: #eee;" class="infor-personal">
            <tbody>
            <tr>
                <th align="left" class="name" width="20%">Họ và tên:</th>
                <td align="left" class="value" width="30%"><?php echo ($user ? ($user->first_name . ' ' . $user->last_name) : ''); ?></td>
                <th align="left" class="name" width="20%">Chức danh:</th>
                <td align="left" class="value" width="30%"><?php echo ($orgchart ? $orgchart->name : ''); ?></td>
            </tr>
            <tr>
                <th align="left" class="name">Mã nhân viên:</th>
                <td align="left" class="value"><?php echo ($user ? $user->user_nicename : ''); ?></td>
                <th align="left" class="name">Cấp bậc:</th>
                <td align="left" class="value"></td>
            </tr>
            <tr>
                <th align="left" class="name">Giới tính:</th>
                <td align="left" class="value"><?php ; ?></td>
                <th align="left" class="name">Phòng/ Bộ phận:</th>
                <td align="left" class="value"><?php echo ($orgchart ? $orgchart->room : ''); ?></td>
            </tr>
            <tr>
                <th align="left" class="name">Ngày sinh:</th>
                <td align="left" class="value"></td>
                <th align="left" class="name">Khối/ Ban:</th>
                <td align="left" class="value"></td>
            </tr>
            <tr>
                <th align="left" class="name"></th>
                <td align="left" class="value"></td>
                <th align="left" class="name">Ngày vào Công ty:</th>
                <td align="left" class="value"></td>
            </tr>
            </tbody>
        </table>
        <br>
        <?php

        $firstYearBehavior = year_get_first_year_behavior( TIME_YEAR_VALUE );
        $year_id_behavior = !empty( $firstYearBehavior ) && !empty($firstYearBehavior['id']) ? $firstYearBehavior['id'] : 0;
        $year_behavior = !empty( $firstYearBehavior ) && !empty($firstYearBehavior['year']) ? $firstYearBehavior['year'] : 0;
        $arrApply_for = !empty( $firstYearBehavior ) ? maybe_unserialize($firstYearBehavior['apply_for']) : [];
        if( !in_array( $orgchart->id, $arrApply_for ) ) {
	        $resultBehavior = behavior_get_list_behavior_of_user( $orgchart->id, $user->ID, $year_id_behavior, TIME_YEAR_VALUE );
	        if ( empty( TIME_PRECIOUS_VALUE ) ) {
		        $resultBehavior = behavior_get_total_number_for_year( TIME_YEAR_VALUE, $user->ID, $orgchart->id );
		        $resultBehavior = array_group_by( $resultBehavior, 'parent_1' );
	        }
	        $getBehavior = [];
	        if ( empty( $resultBehavior ) ) {
		        $resultBehavior = behavior_get_list_behavior_of_admin_by_year( TIME_YEAR_VALUE, 0, KPI_STATUS_RESULT );
	        }
	        $groupBehavior = [];
	        if ( ! empty( $resultBehavior ) ) {
		        $groupBehavior = $resultBehavior[0];
		        unset( $resultBehavior[0] );
	        }
	        $percentBehavior = $firstYearBehavior && ! empty( $firstYearBehavior['behavior_percent'] ) ? $firstYearBehavior['behavior_percent'] : 0;
        }else{
	        $resultBehavior = [];
	        $percentBehavior = 0;
        }

        $arrPercentIII = [
            'I' => ['total' => $percentBehavior, 'total_item' => 0],
            'II' => ['total' => (100 - $percentBehavior), 'total_item' => 0]
        ];

        ?>
        <h3 class="before-table-title">I. <?php echo "THÁI ĐỘ - HÀNH VI";?> (<?= $percentBehavior; ?>%)</h3>
        <table width="100%" cellspacing="0" cellpadding="5" border="1" style="margin-top: 20px;border-color: #eee;">
            <thead>
            <tr class="head">
                <th align="center" width="5%" style="<?= $styleTH; ?>">Stt</th>
                <th align="center" width="61%" style="<?= $styleTH; ?>">Hành vi</th>
                <th align="center" width="12%" style="<?= $styleTH; ?>">Đánh giá / 1 lần <br> vi phạm</th>
                <th align="center" width="12%" style="<?= $styleTH; ?>">Số lần <br> vi phạm</th>
                <th align="center" width="10%" style="<?= $styleTH; ?>">Tỷ lệ <br> bị trừ</th>
            </tr>
            </thead>

            <?php
            # @TODO Loop get Thai Do Hanh Vi
            if ( ! empty( $groupBehavior ) ):
	            $htmlGroup = '';
	            foreach ( $groupBehavior as $key => $itemGroup ):
		            $htmlItem = '';
		            $stt = 0;
		            $getItemBehavior = [];
		            if( array_key_exists( $itemGroup['id'], $resultBehavior ) ){
			            $getItemBehavior = $resultBehavior[$itemGroup['id']];
			            unset( $resultBehavior[$itemGroup['id']] );
		            }
		            foreach ( $getItemBehavior as $k => $item ) {
                        if(empty(TIME_PRECIOUS_VALUE) && empty(TIME_MONTH_VALUE)){
                            if(!empty(behavior_check_behavior_approved_by_year($item['parent'])))
                                continue;
                        }elseif( $item['status'] != KPI_STATUS_DONE ) {
                            continue;
                        }
			            $stt ++;
			            $number = empty( TIME_PRECIOUS_VALUE ) ? $item['total_number'] : $item['number'];
			            $violation_assessment = $item['violation_assessment'];
			            $status = $item['status'];
			            $kqvp = $number * $violation_assessment;
			            $arrPercentIII['I']['total_item'] += $kqvp;
			            if( isset($_REQUEST['options']) && $_REQUEST['options'] == 'not-all' && empty($number) )
			                continue;
			            $htmlItem .= "
                                        <tr>
                                            <td width=\"5%\" align=\"center\" class=\"column-1 align-center\">{$stt}</td>
                                            <td width=\"61%\" align=\"left\" class=\"value background-column\">" . esc_attr( $item['post_title'] ) . "</td>
                                            <td width=\"12%\" align=\"center\" class=\"value background-column\">" . esc_attr( $violation_assessment ) . "%</td>
                                            <td width=\"12%\" align=\"center\" class=\"value background-column\">
                                                {$number}
                                            </td>
                                            <td width=\"10%\" align=\"center\" class=\"value\">
                                                ".esc_attr($kqvp)."%
                                            </td>
                                        </tr>
                                        ";
		            }
		            if( empty( $htmlItem ) ){
                        $htmlItem = "<tr>
                                <td width=\"100%\" colspan=\"5\" class=\"value\" align=\"left\">
                                    ".__('Không vi phạm',TPL_DOMAIN_LANG)."
                                </td>
                            </tr>";
		            }
		            $htmlGroup .= "
                                 <tbody>
                                    <tr>
                                        <td width=\"100%\" colspan=\"5\" class=\"value\" align=\"left\" style=\"font-weight:bolder;\">".mb_strtoupper($itemGroup['post_title'])."</td>
                                    </tr>
                                    {$htmlItem}
                                 </tbody>   
                                    ";
		            ?>
	            <?php endforeach; ?>
	            <?php echo $htmlGroup;  ?>
            <?php endif; ?>
        </table>
        <br>

        <h3 class="before-table-title">II. <?php echo "MỤC TIÊU CÔNG VIỆC (".(100 - $percentBehavior)."%)"; ?></h3>
        <table width="100%" cellspacing="0" cellpadding="5" border="1" style="border-color: #eee;">
            <thead>
                <tr class="head" style="<?= $styleTH; ?>">
                    <th align="center" width="5%">Stt</th>
                    <th align="center" width="48%">Mục tiêu</th>
                    <th align="center" width="10%">Trọng số</th>
                    <th align="center" width="10%">Kế hoạch</th>
                    <th align="center" width="10%">Thực hiện</th>
                    <th align="center" width="9%">Hoàn thành</th>
                    <th align="center" width="8%">KẾT QUẢ</th>
                </tr>
            </thead>
            <tbody>


            <?php
            $htmlRequireYes = '';
            $htmlRequireNo = '';
            foreach($tabs as $key => $tab):
                ?>
                <?php
                $groupDatas = &$percentResults[$key]['data'];
                if (!empty($groupDatas)):
                    $idx = 1;
                    $idxYes = 1;
                    $idxNo = 1;
                    foreach ($groupDatas as &$item):
                        if(empty(TIME_PRECIOUS_VALUE) && empty(TIME_MONTH_VALUE)){
                            if(!empty(kpi_check_kpi_approved_by_year($item['id'], $item['year_id'])))
                                continue;
                        }elseif( $item['status'] != KPI_STATUS_DONE ) {
                            continue;
                        }
	                    $plan = $item['plan'];
	                    #$actual_gain = $item['actual_gain'];
	                    $actual_gain = $percentResults[$key][$item['id'].'_actual_gain'];
	                    $planForYear = $item['plan_for_year'];
	                    $percentForMonth = 0;
	                    $percentForYear = 0;
	                    $HT = $percentResults[$key][$item['id']];
	                    $KQ = round($HT * $item['percent'] / 100, 1);
	                    $arrPercentIII['II']['total_item'] += $KQ;
	                    if( $item['required'] == 'yes' ){
                            $idx = $idxYes;
                            $idxYes++;
	                    }else{
		                    $idx = $idxNo;
		                    $idxNo++;
	                    }
	                    $unit = !empty($item['unit']) ? getUnit($item['unit']) : '';
	                    $htmlTr = "<tr class=\"item\">
                            <td width=\"5%\" align=\"center\" class=\"name\">{$idx}&nbsp;</td>
                            <td width=\"48%\" align=\"left\" class=\"name background-column\">".str_replace(str_split("\|"), "",$item['post_title']) . "</td>
                            <td width=\"10%\" align=\"center\" class=\"name background-column\">".$item['percent']."%</td>
                            <td width=\"10%\" align=\"center\" class=\"name background-column\">".$item['plan']." {$unit}</td>
                            <td width=\"10%\" align=\"center\" class=\"name background-column\">{$actual_gain} {$unit}</td>
                            <td width=\"9%\" align=\"center\" class=\"name background-column\">".round($HT, 1)."%</td>
                            <td width=\"8%\" align=\"center\" class=\"name\">".$KQ."%</td>
                        </tr>";

	                    if( $item['required'] == 'yes' ){
		                    $htmlRequireYes .= $htmlTr;
	                    }else{
		                    $htmlRequireNo .= $htmlTr;
	                    }
                        ?>

                        <?php
                        #$idx++;
                    endforeach;
                endif;
            endforeach;
            if( !empty( $htmlRequireYes ) ){
                $htmlRequireYes = "
                <tr>
                    <td align=\"left\" colspan=\"8\" class=\"name\"><strong>MỤC TIÊU BẮT BUỘC</strong>
                    </td>
            </tr>{$htmlRequireYes}";
            }
            if( !empty( $htmlRequireNo ) ){
                $htmlRequireNo = "<tr>
                <td width=\"100%\" align=\"left\" colspan=\"8\" class=\"name\"><strong>MỤC TIÊU KHÔNG BẮT BUỘC</strong></td>
            </tr>{$htmlRequireNo}";
            }
            echo $htmlRequireYes;
            echo $htmlRequireNo;
            ?>
            </tbody>

        </table>
        <br>
        <h3 class="before-table-title">III. KẾT QUẢ KPI</h3>
        <table width="100%" cellspacing="0" cellpadding="5" border="1" style="border-color: #eee;">
            <thead>
            <tr class="head" style="<?= $styleTH; ?>">
                <th align="center" width="5%">Stt</th>
                <th align="center" width="50%">Mục tiêu KPI</th>
                <th align="center" width="15%">Trọng số</th>
                <th align="center" width="15%">Tổng điểm</th>
                <th align="center" width="15%">Kết quả</th>
            </tr>
            </thead>
            <tbody>
            <?php
                $kqI = $percentBehavior + ($percentBehavior * $arrPercentIII['I']['total_item'] /100 );
                $kqII = $arrPercentIII['II']['total_item'] * (100 - $percentBehavior) / 100;
                $total = $kqI + $kqII;
            ?>
                <tr>
                    <td align="center" width="5%">1</td>
                    <td align="left" width="50%">Thái độ hành vi</td>
                    <td align="center" width="15%"><?= $percentBehavior; ?>%</td>
                    <td align="center" width="15%"><?= $arrPercentIII['I']['total_item']; ?></td>
                    <td align="center" width="15%"><?= round($kqI, 1); ?>%</td>
                </tr>
                <tr>
                    <td align="center" width="5%">2</td>
                    <td align="left" width="50%">Mục tiêu công việc</td>
                    <td align="center" width="15%"><?= 100 - $percentBehavior; ?>%</td>
                    <td align="center" width="15%"><?= $arrPercentIII['II']['total_item']; ?></td>
                    <td align="center" width="15%"><?= round($kqII, 1); ?>%</td>
                </tr>
            </tbody>
            <tfoot>
            <tr class="group">
                <td colspan="4">Tổng</td>
                <td align="center">
			        <?php echo round($total,1); ?>%</td>
            </tr>
            </tfoot>
        </table>
        <br><br><br><br>
        <table width="100%" cellspacing="0" cellpadding="0" style="margin-top:50px;">
            <tbody>
            <tr>

                <td align="center" style="font-weight: bold;">Người nhận tiêu chí đánh giá</td>
                <td align="center" style="font-weight: bold;">Người duyệt tiêu chí đánh giá</td>
                <td align="center" style="font-weight: bold;">Người duyệt kết quả đánh giá</td>
            </tr>
            <tr style="min-height: 100px">

                <td align="center">Chức danh</td>
                <td align="center">Chức danh</td>
                <td align="center">Chức danh</td>
            </tr>
            <tr style="min-height: 100px">

                <td><br><br><br><br><br><br><br></td>
                <td align="center">&nbsp;</td>
                <td align="center">&nbsp;</td>
            </tr>
            <tr>
                <td align="center">Ngày ...... / ...... / ......</td>
                <td align="center">Ngày ...... / ...... / ......</td>
                <td align="center">Ngày ...... / ...... / ......</td>
            </tr>
            </tbody>
        </table>

    </div>
<?php

exit;
$html = ob_get_clean();
if( $orgchart->role == 'phongban' ){
	$file_name = "phieu-danh-gia-kpi-cap-quan-ly-{$user->user_nicename}-theo-{$chart_type}.pdf";
}else{
	$file_name = "phieu-danh-gia-kpi-cap-nhan-vien-{$user->user_nicename}-theo-{$chart_type}.pdf";
}
if( !empty($_GET['html']) ) {
    #echo $html;
	save_pdf($file_name, $html, $logo_base64, 12, "I", $width, $height);
} else {
    /*phptopdf([
        'action' => 'download',
        "source_type" => 'html',
        "source" => $html,
        # "save_directory" => __DIR__ . '/',
        "file_name" => "phieu-danh-gia-kpi-cap-nhan-vien-{$user->user_nicename}-theo-{$chart_type}.pdf"
    ]);*/

    save_pdf($file_name, $html, $logo_base64, 12, "D", $width, $height);
}
