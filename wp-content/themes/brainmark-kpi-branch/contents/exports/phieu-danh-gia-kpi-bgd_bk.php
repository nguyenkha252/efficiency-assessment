<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 3/15/18
 * Time: 10:21
 */

# /wp-content/themes/brainmark-kpi-branch/contents/exports/phieu-danh-gia-kpi-quan-ly.php

require_once __DIR__ . '/../../../../../wp-load.php';
require_once THEME_DIR . '/inc/phpToPDF.php';

$type = '';
$html = "";
$logo = base64_encode( file_get_contents(__DIR__ . '/tmp/logo.png') );
$logo = "data:image/png;base64,".$logo;
# $logo = "http://sadeco.kpibrainmark.com/wp-content/themes/brainmark-kpi-branch/inc/tmp/logo.png";


ob_start();
?>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr class="logo-row-1" style="color: #0a6aa1">
            <th rowspan="3"><img src="<?php echo $logo; ?>"></th>
            <th colspan="7" style="color: red">Công ty Cổ phần Phát triển Kinh doanh BrainMark</th>
        </tr>
        <tr class="logo-row-2">
            <th colspan="7">PHIẾU ĐÁNH GIÁ KPIs CẤP QUẢN LÝ - QUÝ … / 201...</th>
        </tr>
        <tr class="logo-row-3">
            <th colspan="7">Mã số: …</th>
        </tr>
        </tbody>
    </table>
    <table width="100%" cellspacing="0" cellpadding="0">
        <thead>
        <tr class="head">
            <th>Stt</th>
            <th>Mục tiêu</th>
            <th>Trọng số (%)</th>
            <th>ĐVT</th>
            <th>Kế hoạch</th>
            <th>Thực hiện</th>
            <th>Hoàn thành</th>
            <th>KẾT QUẢ</th>
        </tr>
        </thead>
        <tbody>
        <tr class="group">
            <td colspan="8"></td>
        </tr>
        <tr class="item">
            <td>1</td>
            <td>Mục tiêu 1</td>
            <td>10</td>
            <td>lần</td>
            <td>3</td>
            <td>3</td>
            <td>100</td>
            <td>OK</td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="7">TỔNG:</td><td>0%</td>
        </tr>
        </tfoot>
    </table>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td align="center">Người lập</td>
            <td align="center">Xem xét</td>
            <td align="center">Phê duyệt</td>
        </tr>
        <tr>
            <td align="center">Giám đốc HCNS</td>
            <td align="center">Phó Tổng Giám đốc</td>
            <td align="center">Tổng Giám đốc</td>
        </tr>
        <tr>
            <td align="center">Ngày ....../....../......</td>
            <td align="center">Ngày ....../....../......</td>
            <td align="center">Ngày ....../....../......</td>
        </tr>
        </tbody>
    </table>
<?php


$html = ob_get_clean();
/*
phptopdf([
    'action' => 'download',
    "source_type" => 'url',
    "source" => 'http://google.com',
    "save_directory" => __DIR__ . '/',
    "file_name" => 'google.pdf'
]); */


if( !empty($_GET['html']) ) {
	echo $html;
} else {
	phptopdf( [
		'action'         => 'download',
		"source_type"    => 'html',
		"source"         => $html,
		"save_directory" => __DIR__ . '/',
		"file_name"      => "phieu-danh-gia-kpi-cap-quan-ly-theo-{$type}.pdf"
	] );
}