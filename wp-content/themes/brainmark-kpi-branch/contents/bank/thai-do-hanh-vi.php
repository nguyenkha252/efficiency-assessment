<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 06/03/2018
 * Time: 16:04
 */
$isGetCat = true;
if( array_key_exists( 'cat', $_GET ) && !empty( $_GET['cat'] ) ){
	global $wpdb;
	$cat = $wpdb->prepare( "%s", $_GET['cat'] );
	$myTerm = get_term_by( 'slug', $cat, 'category' );
	$level = getLevelCategories( $myTerm );
	$namePosition = '';
	$IDPosition = 0;
	$slugPosition = '';
	$parentPosition = 0;
	$rootParentCat = 0;
	$nameRoom = '';
	$IDRoom = 0;
	$childrenTerm = [];

	$rootCategory = getParentTerm( $myTerm, $level+1, 'category' );
	if( !empty($rootCategory) && !is_wp_error( $rootCategory ) ){
		$rootParentCat = $rootCategory->term_id;
	}
	if( !empty($myTerm) && !is_wp_error( $myTerm ) ){
		$namePosition = $myTerm->name;
		$slugPosition = $myTerm->slug;
		$IDPosition = $myTerm->term_id;
		$parentPosition = $myTerm->parent;
		# $childrenTerm = get_term_children($IDPosition, 'category');
	}
	#get group target
	$args_post = [
		'posts_per_page' => '-1',
		'category_name' => $slugPosition,
		'order' => 'ASC',
		'orderby' => 'post_date',
		'post_type' => 'post',
		'post_status' => ['draft', 'publish']
	];
	/*
	$kpiQuery = new WP_Query($args_post);
	*/
	if( !empty($myTerm) && !is_wp_error($myTerm) ) {
		$originCategory = getParentTerm( $myTerm, $level, 'category' );
		$kpiQuery = getKpisByCategory($myTerm->term_id);
		$postKPI = $kpiQuery->get_posts();
	} else {
		$postKPI = [];
	}

	$arrTarget = [];
	$arrGroup = [];
	if( !empty( $postKPI ) ){
		$arrTarget = array_group_by($postKPI, 'post_parent');
		if( !empty( $arrTarget ) ){
			$arrGroup = $arrTarget[0];
			unset($arrTarget[0]);
		}
	}

	$listFormulas = get_list_formula();
}
#print_r( $query->posts );
$title = !empty($originCategory) ? $originCategory->name : __('Vui lòng chọn phân loại mục tiêu, trước khi tạo mục tiêu!', TPL_DOMAIN_LANG);
$showColumn = true;
if( empty( $IDPosition ) ):
	echo "<h3 class='notification error'>Không tìm thấy thái độ hành vi</h3>";
else:
?>

<div class="col-sm-9 kpi-bank-right">
	<div class="content-top">
		<div class="content-title content-kpi-bank col-md-9">
			<h1 class="entry-title"><?php echo $title; ?></h1>
		</div>
		<?php if( $isGetCat && isset( $level ) ): ?>
			<div class="col-md-3 importkpi">
				<div class="action-show-popup">
					<button href="javascript:;" type="button" data-keyboard="false" data-backdrop="static" data-target="#popup-imports" class="popup-import-kpi btn-brb-default btn-primary" data-toggle="modal" ><i class="fa fa-sign-in"></i> Import</button>
				</div>
				<div id="popup-imports" class="modal fade" role="dialog">
					<?php
					$actionImport = "import_bank_kpi_behavior";
					?>
					<form method="post" enctype="multipart/form-data" class="modal-dialog frm-import-kpi-bank" action="<?= admin_url("admin-ajax.php?action={$actionImport}"); ?>">
						<input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce("import_bank"); ?>" />
						<input type="hidden" value="<?php esc_attr_e($IDPosition); ?>" name="post_category[]" />
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title"><?php _e('Nhập ngân hàng từ File dữ liệu', TPL_DOMAIN_LANG); ?></h4>
							</div>
							<div class="modal-body">
								<div class="bd-bt-poup bd-bt-poup">
									<?php
									$template2003 = "MauFileNganHangBehaviorKPI2003";
									$templateXLSX = "MauFileNganHangBehaviorKPI";
									?>
									Xử lý dữ liệu (Tải về File mẫu:
									<a title="Download" href="<?= THEME_URL; ?>/assets/template/imports/<?= $template2003; ?>.xls">Excel 2003</a>)
								</div>
								<div class="warning-content">
									<h5 class="title-alert">
										<i class="fa fa-warning"></i>
										Lưu ý
									</h5>
									<p>Hệ thống cho phép nhập tối đa 500 dòng mỗi lần từ file.</p>
								</div>
							</div>
							<div class="modal-footer">
								<div class="col-md-12 left-option">
									<label class="switch">
										<input class="checkbox-status" checked name="options" type="radio" value="appendto">
										<span class="checkbox-slider round round-radio"></span>
										<strong>Cập nhật thêm</strong>
									</label>
									<label class="switch">
										<input class="checkbox-status" name="options" type="radio" value="removeall">
										<span class="checkbox-slider round round-radio"></span>
										<span>Xoá dữ liệu cũ và tạo mới</span>
									</label>
								</div>
								<div class="right-file">
									<input type="file" name="imports" id="imports-bank-kpi" class="inputfile" accept=".xls, .xlsx" data-error-type="<?php _e('Vui lòng tải tập tin đúng định dạng (xlsx, xls)'); ?>" data-error-size="<?php _e('Vui lòng tải tập tin < 5M', TPL_DOMAIN_LANG); ?>" data-multiple-caption="{count} <?= __('được chọn', TPL_DOMAIN_LANG); ?>" />
									<label for="imports-bank-kpi"><span><i class="fa fa-upload" aria-hidden="true"></i> <?php _e('Chọn file dữ liệu', TPL_DOMAIN_LANG); ?></span></label>
								</div>
								<ul class="ul-upload-files has-files">
									<li class="file-item">
										<span class="li-filename" title=""></span>
										<strong class="upload-status">
											<button type="button" class="btn-brb-default btn-remove-file-import">
												<i class="fa fa-close"></i>
											</button>
										</strong>
									</li>
								</ul>
								<div class="action">
									<div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
									<button type="submit" class="btn-brb-default send-imports btn-primary has-files" id="send-imports" name="send-imports"><i class="fa fa-copy"></i> <?php  _e('Thực hiện'); ?></button>
									<button type="button" data-dismiss="modal" class="btn-brb-default btn-cancel close-popup">
										<?php _e('Đóng'); ?>
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<div class="clearfix"></div>
	<?php if( $isGetCat && isset( $level ) ): ?>
		<div class="main-content-kpi">
			<div class="grid-view grid-view-kpi brainmark-repeater" data-idx="-1">
				<table class="table-kpi brb-custom-table custom-repeater brainmark-table">
					<thead class="row-thead-title-parent">
					<tr>
						<th class="row-title width10"><?php _e('STT', TPL_DOMAIN_LANG); ?></th>
						<th class="row-title"><?php _e('Hành vi', TPL_DOMAIN_LANG); ?></th>
						<th class="row-title width20"><?php _e('Đánh giá / 1 lần <br>vi phạm', TPL_DOMAIN_LANG); ?></th>
						<th class="row-title title-apply width20"><?php _e('Áp dụng', TPL_DOMAIN_LANG); ?></th>
					</tr>
					</thead>
					<?php
					$idxG = 1;
					if( !empty( $arrGroup ) ){
						foreach ( $arrGroup as $key => $item_group ):
							#foreach ( $item_group as $k => $item ) {
							$groupID = $item_group->ID;
							$nameGroup = get_the_title($groupID);
							$sttRoman = convert_number_2_roman($idxG);
							$rowItem = '';
							if( array_key_exists( $groupID, $arrTarget ) ){
								$targetItem = $arrTarget[$groupID];
								$idxTg = 0;
								$rowItem = '<tbody class="row-tbody-content-item" data-group="' . $idxG . '">';
								foreach ( $targetItem as $k => $item ){
									$idxTg++;
									$targetID = $item->ID;
									$titleTarget = str_replace(str_split("\|"), "", get_the_title($targetID));
									$checked = $item->post_status === 'publish' ? "checked" : '';

									$rowItem .= '<tr class="brainmark-kpi-target-item" data-idg="' . $idxTg . '" data-id="' . esc_attr($targetID) . '">
                                                    <td class="item-numerical-order">
                                                        <span class="value">' . $idxTg . '</span>
                                                        <input name="kpi_bank[post][' . $idxTg . '][menu_order]" id="kpi_bank_menu_order_' . $idxTg . '" class="input-text hidden-default">
                                                    </td>
                                                    <td class="item-content" data-toggle="modal" data-target="#popup-kpi-bank" data-event="edit-row-item">
                                                        <span class="value">' . esc_attr($titleTarget) . '</span>
                                                    </td>
                                                    <td class="item-evaluate">
                                                        <span class="value">' . esc_attr($item->violation_assessment) . '%</span>
                                                    </td>
                                                    <td class="item-action-on-off">
                                                        <label class="switch">
                                                            <input ' . $checked .' class="checkbox-status" name="kpi_bank[post][' . $idxTg . '][status]" id="kpi_bank_status_' . $idxTg . '" data-id="' . $targetID . '" value="1" type="checkbox">
                                                            <span class="checkbox-slider round"></span>
                                                        </label>
                                                    </td>
                                                </tr>';

								}
								unset( $arrTarget[$groupID] );

								$rowItem .= '</tbody>';
							}

							?>
							<thead class="row-thead-title-group" data-group="<?php echo $idxG; ?>"
							       data-idx-group="<?php echo $idxG; ?>">
							<tr>
								<th class="row-group-title group-numerical-order"><?php echo $sttRoman; ?></th>
								<th colspan=""
								    class="row-group-title group-title"><?php esc_attr_e($nameGroup); ?></th>
								<th colspan="2"
								    class="row-group-title action-add-row-target action-add-row-group">
									<a href="javascript:;" data-toggle="modal"
									   data-target="#popup-kpi-bank" data-parent="<?php echo $groupID; ?>"
									   data-event="add-row-item"><?php _e('Thêm mục tiêu', TPL_DOMAIN_LANG); ?>
										<i class="fa fa-plus-circle" aria-hidden="true"></i></a>
									<?php
									if( empty( $rowItem ) ):
										?>
										<a href="javascript:;" data-action="del_group_target" data-id="<?php echo $groupID; ?>" data-event="del-row-group">
											<i class="fa fa-trash-o" aria-hidden="true"></i>
										</a>
									<?php endif; ?>
								</th>
							</tr>
							</thead>
							<?php
							echo $rowItem;
							$idxG++;
							#}
						endforeach;
					}
					?>
					<tfoot class="row-tfoot-group">
					<tr>
						<th colspan="8" class="action-add-group">
							<a href="javascript:;" data-show="show-row"><?php _e('Thêm nhóm mục tiêu', TPL_DOMAIN_LANG); ?><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
						</th>
					</tr>
					<tr class="display-none">
						<th colspan="8">

							<form novalidate="novalidate" action="<?php echo admin_url('admin-ajax.php'); ?>" class="frm-add-group-target" data-add-group-event="" method="post">
								<input type="hidden" value="<?php esc_attr_e($IDPosition); ?>" name="post_category[]" />
								<input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce('create_posts'); ?>" />
								<input type="hidden" name="action" value="create_group_target" />

								<label for="post-title-group-target"></label>
								<input required="required" id="post-title-group-target" data-err-required="<?php _e('Vui lòng nhập tên nhóm mục tiêu', TPL_DOMAIN_LANG); ?>" type="text" value="" class="input-text" name="post_title" placeholder="<?php _e('Tên nhóm mục tiêu', TPL_DOMAIN_LANG); ?>" />


								<button type="submit" name="add_group_target" class="btn btn-add-group-target"><i class="fa fa-plus-circle" aria-hidden="true"></i><?php _e('Tạo nhóm'); ?></button>
							</form>
						</th>
					</tr>
					</tfoot>
					<thead class="row-thead-title-group clone-thead repeat-clone" data-clone-group="">
					<tr>
						<th class="row-group-title group-numerical-order"><?php _e('STT', TPL_DOMAIN_LANG); ?></th>
						<th colspan="4" class="row-group-title group-title">
							<span class="content"></span>
							<input type="hidden" value="" class="" name="group_kpi[IDX][]" />
						</th>
						<th colspan="3" class="row-group-title action-add-row-target action-add-row-group">
							<a href="javascript:;" data-event="add-row-item"><?php _e('Thêm mục tiêu', TPL_DOMAIN_LANG); ?> <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
							<a href="javascript:;" data-event="del-row-group"> <i class="fa fa-trash-o" aria-hidden="true"></i> </a>
						</th>
					</tr>
					</thead>
					<tr class="group-item-clone repeat-clone" data-id="brainmarkcloneitemindex">
						<td class="item-numerical-order">1</td>
						<td class="item-code"></td>
						<td class="item-content"></td>
						<td class="item-target"></td>
						<td class="item-type-kpi"></td>
						<td class="item-measure"></td>
						<td class="item-synthesis"></td>
						<td class="item-action-on-off">
							<label class="switch">
								<input class="checkbox-status" name="post-status" type="checkbox">
								<span class="checkbox-slider round"></span>
							</label>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div id="popup-kpi-bank" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
			<form data-add-group-event="" novalidate="novalidate" action="<?= admin_url('admin-ajax.php'); ?>" method="post" class="modal-dialog frm-add-target-item">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><?php _e('Ngân hàng KPI', TPL_DOMAIN_LANG); ?></h4>
					</div>
					<div class="modal-body">
						<table class="table-input-kpi-bank">
							<tr>
								<td class="column-1" valign="top">
									<label for=""><?php _e('Hành vi', TPL_DOMAIN_LANG); ?>:</label>
								</td>
								<td class="column-2">
									<textarea name="post_title" placeholder="<?php _e('Hành vi', TPL_DOMAIN_LANG); ?>" ></textarea>
                                    <input type="hidden" name="post_id" class="post_id" value="" />
								</td>
							</tr>
							<tr>
								<td class="column-1" valign="top">
									<label for=""><?php _e('Đánh giá / 1 lần vi phạm', TPL_DOMAIN_LANG); ?>:</label>
								</td>
								<td class="column-2">
									<input type="text" name="violation_assessment" placeholder="<?php _e('Đánh giá / 1 lần vi phạm', TPL_DOMAIN_LANG); ?>" class="input-text input-text-target" />
                                    <input type="hidden" name="post_id" class="post_id" value="" />
								</td>
							</tr>

							<tr>
								<td class="column-1"><?php _e("Áp dụng", TPL_DOMAIN_LANG ); ?></td>
								<td class="column-2">
									<label class="switch">
										<input class="checkbox-status" name="post_status" id="children-post-status" type="checkbox" value="on">
										<span class="checkbox-slider round"></span>
									</label>
								</td>
							</tr>
						</table>
					</div>
					<div class="modal-footer">
						<input type="hidden" value="<?php esc_attr_e($IDPosition); ?>" name="post_category[]" />
						<input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce('create_posts'); ?>" />
						<input type="hidden" name="post_parent" value="" />
						<input type="hidden" value="create_edit_behavior_bank" name="action">

						<div class="action-bot">
							<button type="submit" class="btn-brb-default btn-save" id="save-target-item" name="btn-save"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php  _e('Xác nhận'); ?></button>
							<button data-id="" data-action="del_target" data-nonce="<?php echo wp_create_nonce('delete_posts'); ?>" type="button" class="btn-brb-default btn-cancel" data-event="del-item">
								<?php  _e('Xóa'); ?>
							</button>
							<button type="button" data-dismiss="modal" class="btn-brb-default btn-cancel close-popup"><i class="fa fa-trash" aria-hidden="true"></i> <?php _e('Đóng'); ?></button>
						</div>
					</div>
				</div>
			</form>
		</div>

		<!-- Modal -->
		<div id="myDialogConfirm" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title"><?php _e('Bạn có chắc chắn xóa mục tiêu hiện tại', TPL_DOMAIN_LANG); ?></h4>
					</div>
					<div class="modal-body">
						<button type="button" class="btn-brb-default btn-primary btn-yes"><?php _e('Đồng ý', TPL_DOMAIN_LANG); ?></button>
						<button type="button" data-dismiss="modal" class="btn-brb-default btn-cancel btn-no"><?php _e('Đóng', TPL_DOMAIN_LANG); ?></button>
					</div>
					<div class="modal-footer">
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>
<?php endif; ?>