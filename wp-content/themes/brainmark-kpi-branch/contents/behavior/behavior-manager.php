<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 09/03/2018
 * Time: 16:39
 */
require_once THEME_DIR . '/inc/lib-behavior.php';
require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-orgchart.php';
$currentUser = wp_get_current_user();
$orgchartCurrent = user_load_orgchart( $currentUser );
if( isset( $_GET['uid'] ) ){
    $uID = wp_slash( $_GET['uid'] );
    $user = get_user_by("ID", $uID);
}else{
	$user = wp_get_current_user();
}
$orgchart = user_load_orgchart( $user );
if( ( !user_is_manager() && $orgchartCurrent->id != $orgchart->parent ) && isset($_GET['uid']) ){
	echo "<h3 class='notification error'> Không tìm thấy Thái độ hành vi </h3>";
}else {


	$userID    = $user->ID;
	$chartID   = $orgchart->id;
	$year_list = year_get_list_year_behavior();
	$firstYear = year_get_first_year_behavior( TIME_YEAR_VALUE );
	if ( empty( $firstYear ) && ! empty( $year_list ) ) {
		$firstYear = $year_list[0];
	}
	$year_id = ! empty( $firstYear ) ? $firstYear['id'] : 0;
	$year    = ! empty( $firstYear ) ? $firstYear['year'] : 0;
	$arrApply_for = !empty( $firstYear ) ? maybe_unserialize($firstYear['apply_for']) : [];

	$checkPUBLISH = true;
    $textNotify = "";
    if( !in_array( $chartID, $arrApply_for ) ) {
	    $percentBehavior = ! empty( $firstYear ) ? $firstYear['behavior_percent'] : 0;
	    $resultBehavior  = behavior_get_list_behavior_of_user( $chartID, $userID, $year_id, $year );
	    if ( empty( TIME_PRECIOUS_VALUE ) ) {
		    $resultBehavior = behavior_get_total_number_for_year( $year, $userID, $chartID );
		    $resultBehavior = array_group_by( $resultBehavior, 'parent_1' );
	    }
	    $groupBehavior = [];
	    if ( ! empty( $resultBehavior ) ) {
		    $groupBehavior = $resultBehavior[0];
		    unset( $resultBehavior[0] );
	    }
    }else{
        $percentBehavior = 0;
	    $textNotify = "<span class='notification not-publish-user'><i class=\"fa fa-warning\"></i>Thái độ hành vi không áp dụng cho cá nhân này</span>";
    }


	?>
    <?php echo $textNotify; ?>
    <form class="frm-add-manager frm-add-kpi-year" id="" method="post"
          action="<?php echo admin_url( "admin-ajax.php?action=update_result_behavior" ); ?>">
        <input type="hidden" name="year_id" value="<?= $year_id; ?>">
        <input type="hidden" name="year" value="<?= $year; ?>">
        <input type="hidden" name="_wp_http_referer"
               value="<?php echo "?type=" . $_GET['type'] . "&view=" . $_GET['view']; ?>">
        <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce( "update_result_behavior" ); ?>">
        <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>

        <div id="management-behavior" class="section-row">
            <div class="list-detail">
                <div class="top-list-detail top-list-detail-behavior">
                    <div class="col-md-6">
                        <h3 class="title">
							<?php _e( 'Thái độ hành vi' ); ?>
                            <span class="icon-percent" contenteditable="false"
                                  data-kpi-percent="finance"><?php echo $percentBehavior; ?>
                                %</span>
                        </h3>
                    </div>
                    <div class="col-md-6 col-year behavior-for-year">
						<?php if ( ! empty( $year_list ) ): ?>
                            <div class="for-quarter">
                                <label for="select-for-year-phongban"><?php echo _LBL_YEAR; //_e( 'Năm', TPL_DOMAIN_LANG ); ?></label>
                                <select class="selectpicker select-for-year" id="select-for-year" name="nam">
                                    <option value="">-- Năm --</option>
									<?php foreach ( $year_list as $item ):
										$selected = $item['year'] == $year ? 'selected="selected"' : '';
										echo sprintf( '<option value="%s" %s>%s</option>', $item['year'], $selected, $item['year'] );
									endforeach;
									?>
                                </select>
                            </div>
						<?php endif; ?>
						<?php if ( $firstYear['kpi_time'] == 'quy' ): ?>
                            <div class="for-quarter">
                                <label for="select-for-quarter"><?php _e( 'Quý', TPL_DOMAIN_LANG ); ?></label>
                                <select class="select-for-quarter selectpicker" id="select-for-quarter" name="">
                                    <option value="0"><?php _e( 'Cả năm', TPL_DOMAIN_LANG ); ?></option>
									<?php
									for ( $i = 1; $i <= 4; $i ++ ) {
                                        $countBehaviorForPrecious = behavior_get_behavior_need_approve_or_done(TIME_YEAR_VALUE, $i, $user->ID, 'precious');
                                        $styleApprove = '';
                                        if( !empty($countBehaviorForPrecious) ){
                                            $amount_status_approve = array_shift($countBehaviorForPrecious[0]);
                                            $amount_status_complete = array_shift($countBehaviorForPrecious[1]);
                                            if( $amount_status_approve > 0 ){
                                                $styleApprove = "data-content='Quý {$i}<span class=\"need-approve\">*</span>'";
                                            }elseif($amount_status_complete > 0){
                                                $styleApprove = "data-content='Quý {$i}<span class=\"approve-complete\">*</span>'";
                                            }
                                        }
										$selected = $i == TIME_PRECIOUS_VALUE ? 'selected="selected"' : '';
										echo sprintf( '<option %s value="%s" %s>Quý %s</option>', $styleApprove, $i, $selected, $i );
									}
									?>
                                </select>
                            </div>
						<?php elseif ( $firstYear['kpi_time'] == 'thang' ): ?>
                            <div class="for-quarter">
                                <label for="select-for-quarter"><?php _e( 'Tháng', TPL_DOMAIN_LANG ); ?></label>
                                <select class="select-for-month selectpicker" id="select-for-month" name="">
                                    <option value="0"><?php _e( 'Cả năm', TPL_DOMAIN_LANG ); ?></option>
									<?php
									for ( $i = 1; $i <= 12; $i ++ ) {
									    $countBehaviorForMonth = behavior_get_behavior_need_approve_or_done(TIME_YEAR_VALUE, $i, $user->ID, 'month');
                                        $styleApprove = '';
									    if( !empty($countBehaviorForMonth) ){
                                            $amount_status_approve = array_shift($countBehaviorForMonth[0]);
                                            $amount_status_complete = array_shift($countBehaviorForMonth[1]);
                                            if( $amount_status_approve > 0 ){
                                                $styleApprove = "data-content='Tháng {$i}<span class=\"need-approve\">*</span>'";
                                            }elseif($amount_status_complete > 0){
                                                $styleApprove = "data-content='Tháng {$i}<span class=\"approve-complete\">*</span>'";
                                            }
                                        }
										$selected = $i == TIME_MONTH_VALUE ? 'selected="selected"' : '';
										echo sprintf( '<option %s value="%s" %s>Tháng %s</option>', $styleApprove, $i, $selected, $i );
									}
									?>
                                </select>
                            </div>
						<?php endif; ?>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <table class="table-list-detail table-managerment-behavior" cellpadding="0" cellspacing="0">
                <?php
                $checkSendResult = false;
                $checkSendResultApprove = false;
                $htmlGroup = '';
                $checked_all = "checked='checked'";
                if ( ! empty( $groupBehavior ) ):
                    foreach ( $groupBehavior as $key => $itemGroup ):
                        $htmlItem        = '';
                        $stt             = 0;
                        $getItemBehavior = [];
                        if ( array_key_exists( $itemGroup['id'], $resultBehavior ) ) {
                            $getItemBehavior = $resultBehavior[ $itemGroup['id'] ];
                            unset( $resultBehavior[ $itemGroup['id'] ] );
                        }
                        foreach ( $getItemBehavior as $k => $item ) {
                            $stt ++;
                            $violation_assessment = $item['violation_assessment'];
                            $number               = empty( TIME_PRECIOUS_VALUE ) ? $item['total_number'] : $item['number'];
                            $kqvp                 = $number * $violation_assessment;
                            if ( ! empty( TIME_PRECIOUS_VALUE ) && ( user_is_manager() || $orgchartCurrent->id == $orgchart->parent || $orgchart->id == $orgchartCurrent->id ) ) {
                                $inputPercent = "<input class='width60 align-center' type='number' name='behavior[" . $item['id'] . "][number]' value='" . esc_attr( $item['number'] ) . "' />
                                            <input type='hidden' name='behavior[" . $item['id'] . "][id]' value='" . $item['id'] . "' />";
                            } else {
                                $inputPercent = $number;
                            }
                            if ( ! $checkSendResult && in_array( $item['status'], [ KPI_STATUS_WAITING ] ) ) {
                                $checkSendResult = true;
                            }
                            if( !$checkSendResultApprove ) {
                                $checkSendResultApprove = true;
                            }
                            $valueInput       = $item['id'];
                            $nameCheck        = 'name="behavior[' . $item['id'] . '][status]"';
                            $textSendApproved = '';
                            $actionRollback = '';

                            $checked  = in_array( $item['status'], [ KPI_STATUS_DONE ] ) ? 'checked=\"checked\"' : '';
                            if(empty($checked) && !empty($checked_all)){
                                $checked_all = '';
                            }
                            $inputCheck = '';
                            if(!empty(TIME_MONTH_VALUE) || !empty(TIME_PRECIOUS_VALUE)){
                                if ( in_array( $item['status'], [ KPI_STATUS_DONE ] ) ) {
                                    $textSendApproved = "<span class='notification success'>" . __( 'Đã duyệt', TPL_DOMAIN_LANG ) . "</span>";
                                } elseif ( $item['status'] == KPI_STATUS_WAITING ) {
                                    $textSendApproved = "<span class='notification error'>" . __( 'Chờ duyệt', TPL_DOMAIN_LANG ) . "</span> ";
                                    $actionRollback = "<div><a href='javascript:;' class='behavior-rollback' data-title=\"".__('Bạn có muốn hoàn tác thái độ hành vi này cho '.$user->first_name .' '. $user->last_name . ' cập nhật lại?', TPL_DOMAIN_LANG)."\" data-target=\"#confirm-popup\" data-action='".admin_url("admin-ajax.php?action=rollback_result_behavior&id=".$item['id'])."'>Hoàn tác</a></div>";
                                } else {
                                    $textSendApproved = "<span class='notification'>" . __( 'Chưa duyệt', TPL_DOMAIN_LANG ) . "</span>";
                                }
                                $inputCheck = "<label class=\"switch\">
                                                <input class=\"checkbox-status hidden\" type=\"checkbox\" {$nameCheck} {$checked} value=\"1\"/>
                                                <span class=\"checkbox-slider\"></span>
                                            </label>{$actionRollback}";

                            }

                            $htmlItem .= "
                                        <tr class='item-'>
                                        <td class='column-0 align-center width05'>
                                            {$inputCheck}
                                        </td>
                                        <td class=\"column-1 align-center\">{$stt}</td>
                                        <td class=\"column-2\">" . esc_attr( $item['post_title'] ) . "</td>
                                        <td class=\"column-3 align-center\">" . esc_attr( $violation_assessment ) . "%</td>
                                        <td class=\"column-4 align-center\">
                                            {$inputPercent}
                                        </td>
                                        <td class=\"column-5 align-center\">
                                            " . esc_attr( $kqvp ) . "%
                                        </td>
                                         <td class=\"column-6 width20 align-center\">
                                            {$textSendApproved}
                                        </td>
                                    </tr>
                                        ";
                            if ( $checkPUBLISH && $item['status'] != KPI_STATUS_RESULT ) {
                                $checkPUBLISH = false;
                            }
                        }
                        $actionDelGroup = "";
                        if ( ! empty( $htmlItem ) ) {
                            $htmlItem = "<tbody>{$htmlItem}</tbody>";
                        }
                        $htmlGroup .= "
                                    <thead class='group-behavior-item'>
                                        <tr>
                                            <td class='column-0 align-center'>
                                                <a href='javascript:;' class='group-item-down'><i class=\"fa fa-chevron-circle-down fa-chevron-circle-up\"></i></a>
                                            </td>
                                            <td colspan='6' class='column-2'>{$itemGroup['post_title']}</td>
                                        </tr>
                                    </thead>
                                    {$htmlItem}
                                    ";
                        ?>
                    <?php endforeach; ?>
                <?php endif; ?>
                <thead>
                <tr>
                    <th class="column-0 align-center width05">
                        <?php if(!empty(TIME_MONTH_VALUE) || !empty(TIME_PRECIOUS_VALUE)): ?>
                            <label class="switch">
                                <input class="checkbox-status hidden checkall" <?php echo $checked_all; ?> type="checkbox" name="behavior[ID][status]" value="1"/>
                                <span class="checkbox-slider"></span>
                            </label>
                        <?php endif; ?>
                    </th>
                    <th class="column-1 align-center"><?php _e( 'STT' ); ?></th>
                    <th class="column-2"><?php _e( 'Hành vi' ); ?></th>
                    <th class="column-3 align-center"><?php _e( 'Đánh giá / 1 lần <br> vi phạm' ); ?></th>
                    <th class="column-4 align-center"><?php _e( 'Số lần <br> vi phạm' ); ?></th>
                    <th class="column-5 width10 align-center"><?php _e( 'Tỷ lệ <br> bị trừ' ); ?></th>
                    <th class="column-6 width10 align-center">
                        Trạng thái
                    </th>
                </tr>
                </thead>
                <?php echo $htmlGroup; ?>
            </table>
        </div>

        <div class="action-bot">
            <button type="submit" class="btn-brb-default btn-primary btn-save-change" name="btn-save"><i
                        class="fa fa-floppy-o" aria-hidden="true"></i> <?php _e( 'Lưu lại' ); ?></button>
			<?php if ( $checkSendResult ): ?>
                <button data-method="post" data-action="<?php echo admin_url( "admin-ajax.php?action=approve_result_behavior" ); ?>" type="button" class="btn-brb-default btn-primary" name="btn-save-send">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
					<?php _e( 'Duyệt kết quả' ); ?>
                </button>
			<?php endif; ?>
            <?php if ( $checkSendResultApprove && $orgchart->id == $orgchartCurrent->id && !user_is_manager() ): ?>
                <button data-method="post" data-action="<?php echo admin_url( "admin-ajax.php?action=send_result_behavior" ); ?>" type="button" class="btn-brb-default btn-primary" name="btn-save-send">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
					<?php _e( 'Gửi kết quả' ); ?>
                </button>
			<?php endif; ?>
        </div>
    </form>
	<?php
}
