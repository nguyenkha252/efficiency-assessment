<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 2/6/18
 * Time: 15:12
 */

require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-orgchart.php';

$userID = 0;
global $wpdb;
if( array_key_exists( 'uid', $_GET ) ){
	$userID = (int)wp_slash( $_GET['uid'] );
}
$user = wp_get_current_user();
$orgchart = user_load_orgchart($user);
$currentOrgID = $orgchart ? $orgchart->id : '';
$member_role = $orgchart ? $orgchart->role : '';

$orgChartParent = 0;
if( $userID > 0 ) {
	$getUser = kpi_get_user_by_id($userID);
	$orgchartUser = $getUser->__get('orgchart');
	$orgChartParent = $orgchartUser->parent;
	$member_role = $orgchartUser ? $orgchartUser->role : '';
}else{
	$getUser = wp_get_current_user();
	$orgchartUser = $getUser->__get('orgchart');
	$orgChartParent = $orgchartUser->parent;
	$member_role = $orgchartUser ? $orgchartUser->role : '';
	$userID = $getUser->ID;
}
echo '<div id="confirm-register-kpi" class="confirm-register-kpi">';
if (!empty($userID) && !empty($getUser) && $orgChartParent != 0 && isset($_GET['viewmyself'])):
	#$member_role = $orgchartUser->role;
	#get_template_part('contents/capacity/chart/chart', $member_role);
	#get_template_part('contents/capacity/news/news', $member_role);
	get_template_part('contents/behavior/behavior', 'manager');
elseif( !in_array( $_GET['view'], ['behavior'] ) ):
	require_once THEME_DIR . '/ajax/get_users_list.php';
	require_once THEME_DIR . '/ajax/get_user_info.php';
	$orgCharts = orgchart_get_all_kpi_by_parent( $currentOrgID );
	$arrOrgChart = [];
	foreach ( $orgCharts as $key => $item ){
		$arrOrgChart[] = $item['id'];
	}
	$strOrgChart = implode(", ", $arrOrgChart);
	$listUser = user_list_for_kpi_behavior( $strOrgChart, TIME_YEAR_VALUE, 'behavior' );
	$listUser = array_group_by( $listUser, 'room' );
	global $kpi_contents_parts;
	$applyKpiSystem = $kpi_contents_parts['apply-bsc-kpi-system'];
	$applyKpiSystemUrl = $applyKpiSystem['url'];
	$url = add_query_arg('type', BEHAVIOR, $applyKpiSystemUrl);
	if( user_is_manager() ){
		$mnUrlQL = add_query_arg('view', 'behavior', $url);
		$htmlViewOrCreate = "<a href=\"{$mnUrlQL}\" class='mngql view-list-behavior' title=\"Danh sách thái độ hành vi\">Danh sách thái độ hành vi</a>";
	}else{
		$mnUrl = add_query_arg('viewmyself', true, $url);
		$htmlViewOrCreate = "<a href=\"{$mnUrl}\" class='view-list-behavior' title=\"Thái độ hành vi cá nhân\">Thái độ hành vi cá nhân</a>";
	}
	?>
	<div class="lists confirm-register-list-user block-item <?php echo empty($orgChartParent) ? 'approved-kpi' : ''; ?>">
		<h1 class="title-list-member"><?php _e('Danh sách triển khai', TPL_DOMAIN_LANG); ?></h1>
		<label class="list-user-kpi-for-year">
			<?php echo $htmlViewOrCreate; ?>
		</label>
		<table class="user-list list-staff" cellpadding="0" cellspacing="0" width="100%">
			<thead>
			<tr>
				<th class="id-col">STT</th>
                <th>Mã nhân viên</th>
                <th class="name"><?php _e('Name', TPL_DOMAIN_LANG); ?></th>
                <th class="position_name"><?php _e('Position Name', TPL_DOMAIN_LANG); ?></th>
                <th>Duyệt thái độ hành vi</th>
			</tr>
			</thead>
			<tbody>
			<?php
			if( !empty($listUser) ):
				$output = '';
                $stt               = 1;
                foreach ( $listUser as $key => $item ) {
                    $tdChild           = '';
                    foreach ( $item as $user ):
                        $countKPI = $user['count_kpi'];
                        $url_1           = add_query_arg( 'uid', $user['ID'], $url );
                        $url_2           = add_query_arg( 'viewmyself', true, $url_1 );
                        $showHtmlCount = "";
                        if ( $countKPI > 0 ) {
                            $showHtmlCount = "<label title='Số lượng cần duyệt' class='stick-count-kpi'>{$countKPI}</label>";
                        }
	                    $tdChild .=
		                    "<tr class=\"user-row\">
                            <td class=\"id-stt\">{$stt}</td>
                            <td>".$user['user_nicename']."</td>
                            <td class=\"display_name\">".$user['display_name']." ".$showHtmlCount."</td>
                            <td class=\"position_name\">".__( $user['orgchart_name'], TPL_DOMAIN_LANG )."</td>
                            <td>
                                <div class=\"viewkpiofroom approve-register\">
                                    <a href=\"{$url_2}\" title=\"Xem\">Xem</a>
                                </div>
                            </td>
                        </tr>";
	                    $stt ++;
                    endforeach;
	                $output .= "
                        <tr class='parent'>
                            <td colspan='5'>{$key}</td>
                        </tr> {$tdChild}";
                }
			endif;
                echo $output;
			?>
			</tbody>
		</table>
	</div>
	<?php
else:
	#$applyOf = !empty( $_GET['view'] ) && $_GET['view'] == 'mngql' ? APPLY_OF_MANAGER : APPLY_OF_EMPLOYEES;
	get_template_part("contents/behavior/template-add/add", "behavior");
	?>
	<?php
endif;
echo '</div>';
