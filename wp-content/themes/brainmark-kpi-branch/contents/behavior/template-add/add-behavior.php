<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 2/9/18
 * Time: 11:41
 */
require_once THEME_DIR . '/inc/lib-behavior.php';
require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-orgchart.php';

$user = wp_get_current_user();
$orgchart = user_load_orgchart( $user );
$orgchartID = $orgchart->id;
$year_list = year_get_list_year_behavior();
$firstYear = year_get_first_year_behavior( TIME_YEAR_VALUE );
if( empty( $firstYear ) && !empty(  $year_list) ){
    $firstYear = $year_list[0];
}
$year_id = !empty( $firstYear ) ? $firstYear['id'] : 0;
$year = !empty( $firstYear ) ? $firstYear['year'] : 0;
$arrApply_for = !empty( $firstYear ) ? maybe_unserialize($firstYear['apply_for']) : [];
$checkPUBLISH = true;

#$getCP = capacity_get_list_kpi($year_id, APPLY_OF_MANAGER, CAPACITY_TIEU_CHI, 'chitiet');
#$getCPYearOf = capacity_get_kpi_year_and_total($year_id, APPLY_OF_MANAGER, $user->ID);
#$getCPYearOf = array_group_by($getCPYearOf, 'type_of');
$getBehavior = behavior_get_list_behavior_of_admin_by_year( $year,0);
$arrPublish = [];
foreach( $getBehavior as $key => $item ){
    if( $item['status'] != KPI_STATUS_RESULT && $item['parent_1'] != 0 ){
        $arrPublish[] = $item['post_title'];
    }
}
$resultBehavior = array_group_by( $getBehavior, 'parent_1' );
$groupBehavior = [];
if( !empty( $resultBehavior ) ){
    $groupBehavior = $resultBehavior[0];
    unset($resultBehavior[0]);
}
global $kpi_contents_parts;
$applyKpiSystem = $kpi_contents_parts['apply-bsc-kpi-system'];
$applyKpiSystemUrl = $applyKpiSystem['url'];
$url = add_query_arg('type', BEHAVIOR, $applyKpiSystemUrl);
/*if( !empty( $getBehavior ) ){
 echo "<h2 class='notification error description'>Thái độ hành vi cho năm ". TIME_YEAR_VALUE ." đã tồn tại. Vui lòng thực hiện thao tác khác.</h2>";
}else {*/
	?>
    <div class="col-md-6 ">
        <?php
            if( !empty( $getBehavior ) ) {
	            if ( ! empty( $arrPublish ) ) {
		            echo "<h3 class='warning notification'><i class=\"fa fa-exclamation-triangle\"></i> Một số hành vi chưa được triển khai: </h3>";
		            echo "<ul>";
		            foreach ( $arrPublish as $k => $it ) {
			            echo "<li class='error'>" . ( $k + 1 ) . ". " . $it . ".</li>";
		            }
		            echo "<ul/>";
	            } else {
		            echo "
                <h3 class='success notification'>
                    <i class=\"fa fa-check-circle\"></i>
                    Đã được triển khai
                </h3>
                ";

	            }
            }
        ?>
    </div>
    <div class="col-md-6 col-year capacity-for-year">
        <?php if( !empty( $year_list ) ): ?>
            <label for="select-for-year-phongban"><?php echo _LBL_YEAR; //_e( 'Năm', TPL_DOMAIN_LANG ); ?></label>
            <select class="selectpicker select-for-year" id="select-for-year" name="nam">
                <option value="">-- Năm --</option>
                <?php foreach ( $year_list as $item ):
                    $selected = $item['year'] == $year ? 'selected="selected"' : '';
                    echo sprintf( '<option value="%s" %s>%s</option>', $item['year'], $selected, $item['year'] );
                endforeach;
                ?>
            </select>
        <?php endif; ?>
    </div>
    <div class="clearfix"></div>
        <form class="frm-add-manager frm-add-kpi-year" id="" method="post"
              action="<?php echo admin_url( "admin-ajax.php?action=deployment_behavior" ); ?>">
            <input type="hidden" name="year_id" value="<?= $year_id; ?>">
            <input type="hidden" name="year" value="<?= $year; ?>">
            <input type="hidden" name="_wp_http_referer"
                   value="<?php echo "?type=" . $_GET['type'] . "&view=" . $_GET['view']; ?>">
            <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce( "deployment_behavior" ); ?>">
            <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>

            <div id="management-behavior" class="section-row">
                <div class="list-detail">
                    <div class="top-list-detail">
                        <h3 class="title">
							<?php _e( 'Thái độ hành vi' ); ?>
                            <?php
                                $load_params = [
                                    'action' => 'update_behavior_percent',
                                    'id' => $year_id,
                                ];
                                $urlUpdateBP = add_query_arg( $load_params, admin_url("admin-ajax.php"));

                            ?>
                            <input type="number" data-method="post" data-action="<?php echo $urlUpdateBP; ?>" id="behavior-percent" name="behavior_percent" class="width20 behavior-percent" value="<?php echo !empty( $firstYear ) ? $firstYear['behavior_percent'] : 0; ?>"> %
                        </h3>
                        <a href="javajscript:;" data-toggle="modal" data-target="#apply-for-role">Triển khai phòng ban, chức danh</a>
                    </div>
                </div>

                <input type="hidden" name="apply_of" value="quan-ly">
                <table class="table-list-detail table-managerment-behavior" cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <th class="column-1 align-center"><?php _e( 'STT' ); ?></th>
                        <th class="column-2"><?php _e( 'Hành vi' ); ?></th>
                        <th class="column-3 align-center"><?php _e( 'Đánh giá / 1 lần <br> vi phạm' ); ?></th>
                        <th class="column-4 align-center"><?php _e( 'Thao tác' ); ?></th>
                    </tr>
                    </thead>
                    <tbody>
					<?php
					if ( ! empty( $groupBehavior ) ):
						$htmlGroup = '';
						foreach ( $groupBehavior as $key => $itemGroup ):
                            $htmlItem = '';
							$stt = 0;
							$getItemBehavior = [];
							if( array_key_exists( $itemGroup['id'], $resultBehavior ) ){
							    $getItemBehavior = $resultBehavior[$itemGroup['id']];
							    unset( $resultBehavior[$itemGroup['id']] );
                            }
                            foreach ( $getItemBehavior as $k => $item ) {
	                            $stt ++;
	                            $htmlItem .= "
                                <tr class='item-'>
                                <td class=\"column-1 align-center\">{$stt}</td>
                                <td class=\"column-2\">" . esc_attr( $item['post_title'] ) . "</td>
                                <td class=\"column-3 align-center\">" . esc_attr( $item['violation_assessment'] ) . "%</td>
                                <td class=\"column-8\">
                                    <a href=\"javascript:;\" data-id=\"{$item['id']}\"
                                       data-wpnonce=\"" . wp_create_nonce( 'load_behavior' ) . "\"
                                       data-action=\"load_behavior\" data-method=\"get\" data-toggle=\"modal\"
                                       data-target=\"#add-management-behavior\" class=\"edit action-edit\"
                                       title=\"" . __( 'Chỉnh sửa', TPL_DOMAIN_LANG ) . "\"> <span
                                                class=\"glyphicon glyphicon-pencil\"></span> " . __( 'Chỉnh sửa', TPL_DOMAIN_LANG ) . "
                                    </a><br>
                                    <a href=\"javascript:;\" data-nonce=\"" . wp_create_nonce( 'delete_behavior' ) . "\"
                                       data-id=\"{$item['id']}\" data-action=\"delete_behavior\"
                                       data-title=\"Bạn có chắc xóa thái độ hành vi hiện tại?\"
                                       data-target=\"#confirm-apply-kpi\" data-method=\"post\" class=\"delete\"
                                       title=\"" . __( 'Xóa', TPL_DOMAIN_LANG ) . "\"><span
                                                class=\"glyphicon glyphicon-trash\"></span> " . __( 'Xóa', TPL_DOMAIN_LANG ) . "
                                    </a>
                                </td>
                            </tr>
                                ";
	                            if ( $checkPUBLISH && $item['status'] != KPI_STATUS_RESULT ) {
		                            $checkPUBLISH = false;
	                            }
                            }
                            $actionDelGroup = "";
							if( empty( $htmlItem ) ){
							    $actionDelGroup = "<a href=\"javascript:;\" data-nonce=\"".wp_create_nonce( 'delete_behavior' )."\"
                                       data-id=\"{$itemGroup['id']}\" data-action=\"delete_behavior\"
                                       data-title=\"Bạn có chắc xóa thái độ hành vi hiện tại?\"
                                       data-target=\"#confirm-apply-kpi\" data-method=\"post\" class=\"delete\"
                                       title=\"".__( 'Xóa', TPL_DOMAIN_LANG )."\"><span
                                                class=\"glyphicon glyphicon-trash\"></span> ".__( 'Xóa', TPL_DOMAIN_LANG )."
                                    </a>";
                            }else{
							    $htmlItem = "<tbody>{$htmlItem}</tbody>";
                            }
                            $htmlGroup .= "
                            <thead class='group-behavior-item'>
                                <tr>
                                    <td class='column-1 align-center'>
                                        <a href='javascript:;' class='group-item-down'><i class=\"fa fa-chevron-circle-down fa-chevron-circle-up\"></i></a>
                                    </td>
                                    <td colspan='2' class='column-2'>{$itemGroup['post_title']}</td>
                                    <td class='column-4'>{$actionDelGroup}</td>
                                </tr>
                            </thead>
                            {$htmlItem}
                            ";
                            ?>
						<?php endforeach; ?>
                        <?php echo $htmlGroup;  ?>
					<?php endif; ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="7">
                            <a href="javascript:;" data-title="Bạn có chắc xoá thái độ hành vi?" data-toggle="modal"
                               data-target="#add-management-behavior" class=""><i class="fa fa-plus-circle"
                                                                                  aria-hidden="true"></i> <?php _e( 'Thêm thái độ hành vi' ); ?>
                            </a>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>

            <div class="action-bot">
	            <?php if ( !$checkPUBLISH ): ?>
                <button type="submit" class="btn-brb-default btn-primary btn-save-change" name="btn-save"><i
                            class="fa fa-floppy-o" aria-hidden="true"></i> <?php _e( 'Triển khai' ); ?></button>
	            <?php endif; ?>
            </div>
        </form>
        <div id="add-management-behavior" class="modal fade" role="dialog">
            <form class="modal-dialog modal-md" enctype="application/x-www-form-urlencoded"
                  action="<?php echo admin_url( 'admin-ajax.php?action=add_management_behavior' ); ?>" data-action-add="<?php echo admin_url( 'admin-ajax.php?action=add_management_behavior' ); ?>" data-action-edit="<?php echo admin_url( 'admin-ajax.php?action=edit_management_behavior' ) ?>"
                  method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Thêm thái độ hành vi</h4>
                    </div>
                    <div class="modal-body clearfix">
                        <div class="col-md-12">
                            <div class="form-group form-plans">
                                <div class="form-group input-group input-group-select">
                                    <span class="input-group-addon" for="post_title"
                                          id="post_title"><?php _e( 'Hành vi', TPL_DOMAIN_LANG ); ?></span>
                                    <select data-none-results-text="<?php _e( 'Kết quả tìm kiếm {0}', TPL_DOMAIN_LANG ); ?>"
                                            data-none-selected-text="<?php _e( 'Tiêu chí', TPL_DOMAIN_LANG ); ?>"
                                            data-loading=".form-group.input-group.input-group-select"
                                            class="selectpicker bank_id" name="bank_id" data-live-search="true">
										<?php
										$slug        = BEHAVIOR;
										$cate        = getRootKPICapacity( $slug );
										$idCatParent = 0;
										if ( ! empty( $cate ) && ! is_wp_error( $cate ) ) {
											$cate    = $cate->term_id;
											$getBank = getKpisByCategory( $cate, [ 'publish' ], [ 'posts_per_page' => - 1 ] );
											if ( ! empty( $getBank ) && ! is_wp_error( $getBank ) ) {
												echo renderPostDropdown( $getBank->get_posts(), 0 );
											}
										}
										?>
                                    </select>
                                </div>
                                <div class="form-group input-group">
                                    <span class="input-group-addon" for="violation_assessment"
                                          id="violation_assessment"><?php _e( 'Đánh giá / 1 lần vi phạm', TPL_DOMAIN_LANG ); ?></span>
                                    <input type="text" name="violation_assessment" class="violation_assessment form-control"
                                           placeholder="<?php _e( 'Đánh giá / 1 lần vi phạm', TPL_DOMAIN_LANG ); ?>" value=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" value="">
                        <input type="hidden" name="_wpnonce"
                               value="<?php echo wp_create_nonce( "add_management_behavior" ); ?>">
                        <input type="hidden" name="year_id"
                               value="<?php echo $year_id; ?>">
                        <input type="hidden" name="year"
                               value="<?php echo $year; ?>">
                        <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <button type="submit" class="btn btn-primaryy">Lưu</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div id="apply-for-role" class="modal fade" role="dialog">
            <form class="modal-dialog modal-lg frm-add-kpi-year" enctype="application/x-www-form-urlencoded"
                  action="<?php echo admin_url( 'admin-ajax.php?action=behavior_apply_for_role' ); ?>" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Áp dụng thái độ hành vi cho phòng ban, chức danh</h4>
                    </div>
                    <div class="modal-body clearfix">
                        <div class="col-md-12">
	                        <?php
	                        $orgCharts = orgchart_get_list_orgchart();
	                        $orgChartsGroup = array_group_by( $orgCharts, 'room' );
	                        $liGroupHtml = '';
	                        if( !empty( $orgChartsGroup ) ){
		                        foreach ( $orgChartsGroup as $key => $chartItem ):
			                        $liChild = '';
		                            $ulChild = '';
		                            $checkedNumber = 0;
			                        foreach ( $chartItem as $k => $item ){
			                            if( $item->alias == 'congty' || $item->alias == 'tapdoan' )
			                                continue;
			                            if( !in_array( $item->id, $arrApply_for ) ){
			                                $checkedNumber++;
				                            $checked = "checked";
                                        }else{
			                                $checked = '';
                                        }

				                        $liChild .= "
                                                <li>
                                                    <label class=\"switch\">
                                                        <input class=\"checkbox-status hidden\" type=\"checkbox\" {$checked} name=\"charts[{$item->id}]\" value=\"{$item->id}\" />
                                                        <span class=\"checkbox-slider\"></span>
                                                        <span class='chart-name'>{$item->name}</span>
                                                    </label>
                                                </li>";
			                        }
			                        $ulChild = !empty( $liChild ) ? "<ul class='group-chart-item'>{$liChild}</ul>" : '';
                                    $checkedGroup = $checkedNumber > 0 ? "checked" : "";
			                        $liGroupHtml .= !empty($ulChild) ? "<li>
                                                        <label class=\"switch switch-group\">
                                                            <input class=\"checkbox-status hidden\" type=\"checkbox\" {$checkedGroup} value=\"1\" />
                                                            <span class=\"checkbox-slider\"></span>
                                                            <span class='chart-name'>{$key}</span>
                                                        </label>
                                                        {$ulChild}
                                                       </li>" : "";
		                        endforeach;
	                        }
	                        echo "<ul class='group-chart treeview'>{$liGroupHtml}</ul>";
	                        ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="_wpnonce"
                               value="<?php echo wp_create_nonce( "behavior_apply_for_role" ); ?>">
                        <input type="hidden" name="year_id"
                               value="<?php echo $year_id; ?>">
                        <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <button type="submit" class="btn btn-primaryy">Lưu</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
	<?php
#}
