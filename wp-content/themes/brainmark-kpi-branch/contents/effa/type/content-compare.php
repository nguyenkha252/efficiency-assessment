<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/16/18
 * Time: 5:09 PM
 */
global $wp;
$assignLV = [];
if( isset($_GET['lv']) && !empty($_GET['lv']) ){
    $lv = $_GET['lv'];
    $assignLV = effa_site_level_up_assign_get_by_id($lv);
}
if( !empty($assignLV) ){
    get_template_part("contents/effa/type/compare/admin", "compare");
}else{
    get_template_part("contents/effa/type/compare/list", "compare");
}
?>


