<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/23/18
 * Time: 11:12 AM
 */


global $wp;
$settings = get_settings_website();
#$background_color = isset($settings['background_header_table']) ? "style='background-color:{$settings['background_header_table']};'" : "#008ED5";
$current_url = site_url( $wp->request );
$current_url = add_query_arg(['type' => 'compare'], $current_url);
$year = date('Y', time());
$ajax = admin_url("admin-ajax.php");
$list_users = effa_site_user_get_all();
$list_orgcharts = effa_site_orgchart_get_all_not_root();
?>

<div class="col-sm-9 effa-right effa-compare-content">
    <div class="main-content-effa">
        <form method="post" action="<?php echo add_query_arg(['action' => 'effa_add_level_up'], $ajax) ?>">
            <input type="hidden" name="year" value="<?= $year; ?>">
            <input type="hidden" name="_wpnonce" value="<?= wp_create_nonce('effa_add_level_up'); ?>">
            <fieldset>
                <legend><?php _e("So sánh", TPL_DOMAIN_LANG); ?></legend>
                <div class="fieldset-content">
                    <div class="row">
                        <div class="col-md-6">
                            <?php
                            if( !empty($list_orgcharts) ){
                                $options = "<option value='{$current_url}'>".__('-----------', TPL_DOMAIN_LANG)."</option>";
                                $orgcharts = array_group_by($list_orgcharts, 'room');
                                foreach ( $orgcharts as $room => $room_item ){
                                    $options_item = "";
                                    if( !empty($room_item) ){
                                        foreach ($room_item as $item){
                                            $selected = $orgchartID == $item->id ? "selected" : "";
                                            $options_item .= sprintf("<option value=\"%d\" %s >%s</option>", $item->id, $selected, $item->name);
                                        }
                                    }
                                    if( !empty($options_item) ){
                                        $options .= sprintf("<optgroup label=\"%s\">%s</optgroup>", $room, $options_item);
                                    }else{
                                        $options .= sprintf("<option value=\"%d\">%s</option>", $room_item->id, $room_item->name);
                                    }
                                }
                                echo "<label class='lbl-position lbl-text'>".__('Chọn chức danh: ', TPL_DOMAIN_LANG)."</label>";
                                echo "<select name='orgchart_new' class=\"selectpicker\" data-live-search='true'>";
                                echo $options;
                                echo "</select>";
                            }
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            if( !empty($list_users) ) {
                                $options = "";
                                $users = array_group_by($list_users, 'room');
                                if (!empty($users)) {
                                    foreach ($users as $room => $room_item) {
                                        $options_item = "";
                                        if (!empty($room_item)) {
                                            foreach ($room_item as $user) {
                                                $orgchart_name = isset($user->orgchart_name) ? $user->orgchart_name : "";
                                                $options_item .= sprintf("<option value=\"%s\" >%s &lt;%s&gt;</option>", $user->ID, $user->display_name, $orgchart_name);
                                            }
                                        }
                                        if (!empty($options_item)) {
                                            $options .= sprintf("<optgroup label=\"%s\">%s</optgroup>", $room, $options_item);
                                        } else {
                                            $orgchart_name = isset($room_item->orgchart_name) ? $room_item->orgchart_name : "";
                                            $options .= sprintf("<option value=\"%s\">%s &lt;%s&gt;</option>", $room_item->ID, $room_item->display_name, $orgchart_name);
                                        }
                                    }
                                    echo "<label class='lbl-position lbl-text'>" . __('Chọn nhân viên: ', TPL_DOMAIN_LANG) . "</label>";
                                    echo "<select name='user_id[]' class=\"selectpicker\" multiple data-live-search='true'>";
                                    echo $options;
                                    echo "</select>";
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="row mt-10">
                        <div class="col">
                            <div class="response-container"></div>
                            <button type="submit" id="implement-level-up" class="btn btn-default btn-primary"><?php _e('Thực hiện', TPL_DOMAIN_LANG); ?></button>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
        <?php
        $listAssignLV = effa_site_level_up_list_all();
        if( !empty($listAssignLV) ):
            ?>
            <div id="table-compare-detail" class="table-reponsive grid-view grid-view-effa">
                <table class="table-bordered table-striped table-condensed cf table-content-build">
                    <?php
                    $labelSTT = __('STT', TPL_DOMAIN_LANG);
                    $labelFullName = __('Họ và tên', TPL_DOMAIN_LANG);
                    $labelThisPosition = __('Vị trí hiện tại', TPL_DOMAIN_LANG);
                    $labelPositionNew = __('Vị trí so sánh', TPL_DOMAIN_LANG);
                    $labelStatus = __('Tình trạng', TPL_DOMAIN_LANG);
                    $labelDetail = __('Chi tiết', TPL_DOMAIN_LANG);
                    ?>
                    <thead class="cf row-thead-title-parent">
                    <tr class="row-thead-title-parent">
                        <th ><?php echo $labelSTT; ?></th>
                        <th ><?php echo $labelFullName; ?></th>
                        <th ><?php echo $labelThisPosition; ?></th>
                        <th ><?php echo $labelPositionNew; ?></th>
                        <th ><?php echo $labelStatus; ?></th>
                        <th ></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $arrStatus = [
                        'publish' => __('Đang thực hiện', TPL_DOMAIN_LANG),
                        'failed' => __('Trượt', TPL_DOMAIN_LANG),
                        'success' => __('Hoàn thành', TPL_DOMAIN_LANG),
                        'trash' => __('Đã bỏ', TPL_DOMAIN_LANG),
                        'finish-quiz' => __('Đã hoàn thành bài kiểm tra. <br> Đang đợi đánh giá', TPL_DOMAIN_LANG),
                    ];
                    foreach ($listAssignLV as $k => $assign):
                        $status = isset($arrStatus[$assign->status]) ? $arrStatus[$assign->status] : "";
                        $linkDetail = add_query_arg(['lv' => $assign->id], $current_url);
                        ?>
                        <tr>
                            <td data-title="<?= $labelSTT; ?>"><?= $k+1; ?></td>
                            <td data-title="<?= $labelFullName; ?>"><?= $assign->display_name; ?></td>
                            <td data-title="<?= $labelThisPosition; ?>"><?= $assign->org_name_old; ?></td>
                            <td data-title="<?= $labelPositionNew; ?>"><?= $assign->org_name_new; ?></td>
                            <td data-title="<?= $labelStatus; ?>"><?= $status ?></td>
                            <td data-title=""><a href="<?= $linkDetail; ?>"><?= $labelDetail; ?></a></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>
</div>
