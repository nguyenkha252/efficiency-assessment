<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/3/18
 * Time: 11:37 PM
 */
#$settings = get_settings_website();
#$background_header_table = isset($settings['background_header_table']) ? "style='background-color:{$settings['background_header_table']};'" : "style='background-color:#008fd5'";
#$background_header = isset($settings['background_header']) && !empty($settings['background_header']) ? "style='background-color:{$settings['background_header']} !important;'" : "";
if( !user_is_manager() ){
    return;
}
$year = date('Y', time());
$current_url = site_url( $wp->request );
$current_url = add_query_arg(['type' => 'dictionary'], $current_url);
$args_effa = [
    'post_type' => ["efficiency", "questions"],
    'post_status' => ['publish'],
    'posts_per_page' => -1,
    'orderby' => 'ID',
    'order' => 'ASC'
];
$GLOBALS['where_year'] = $year;
add_filter('posts_clauses', 'site_posts_clauses_not_department', 100);
$wpQuery = new WP_Query($args_effa);

$postEffa = $wpQuery->get_posts();
$arrTarget = [];
$arrGroup = [];
$postEfficiency = [];
$postQuestions = [];
if( !empty( $postEffa ) ){
    $groupPostType = array_group_by($postEffa, 'post_type');
    if( isset($groupPostType['efficiency']) ){
        $postEfficiency = $groupPostType['efficiency'];
        $arrTarget = array_group_by($postEfficiency, 'post_parent');
    }
    if( isset($groupPostType['questions']) ){
        $postQuestions = $groupPostType['questions'];
        $postQuestions = array_group_by($postQuestions, 'post_code');
    }
    if( !empty( $arrTarget ) ){
        $arrGroup = $arrTarget[0];
        unset($arrTarget[0]);
    }
}
remove_filter('posts_clauses', 'site_posts_clauses_not_department', 100);
wp_reset_query();

?>
<div class="col-sm-9 effa-right">
    <div class="content-top">
        <div class="content-title content-effa col-md-9">
            <h1 class="entry-title"><?php #echo $title; ?></h1>
        </div>
        <div class="col-md-3 importeffa">
            <div class="action-show-popup">
                <button id="import-all" data-action="<?php echo admin_url("admin-ajax.php?action=import_effa");?>" type="button" data-keyboard="false" data-backdrop="static" data-target="#popup-imports" class="popup-import-effa btn-brb-default btn-primary" data-toggle="modal" ><i class="fa fa-sign-in"></i> Import</button>
            </div>

            <div id="popup-imports" class="modal fade" role="dialog">
                <?php
                $actionImport = "import_effa";
                ?>
                <form method="post" enctype="multipart/form-data" class="modal-dialog frm-import-effa" action="<?= admin_url("admin-ajax.php?action={$actionImport}"); ?>">
                    <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce("import_post"); ?>" />
                    <input type="hidden" name="department" value="<?php echo isset($_GET['department']) ? $_GET['department'] : ""; ?>">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">
                                <?php
                                if( isset($_GET['department']) ){
                                    _e('Nhập năng lực quản lý theo phòng', TPL_DOMAIN_LANG);
                                }else{
                                    _e('Nhập năng lực quản lý từ File dữ liệu', TPL_DOMAIN_LANG);
                                }
                                ?>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="bd-bt-poup bd-bt-poup">
                                <?php
                                $template2003 = "MauFileNLQL2003";
                                $templateXLSX = "MauFileNLQL";
                                ?>
                                Xử lý dữ liệu (Tải về File mẫu:
                                <a title="Download" href="<?= THEME_URL; ?>/assets/template/imports/<?= $template2003; ?>.xls">Excel 2003</a>)
                            </div>
                            <div class="warning-content">
                                <h5 class="title-alert">
                                    <i class="fa fa-warning"></i>
                                    Lưu ý
                                </h5>
                                <p>Hệ thống cho phép nhập tối đa 500 dòng mỗi lần từ file.</p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="col-md-12 left-option">
                                <label class="switch">
                                    <input class="checkbox-status" checked name="options" type="radio" value="appendto">
                                    <span class="checkbox-slider round round-radio"></span>
                                    <strong>Cập nhật thêm</strong>
                                </label>
                                <label class="switch">
                                    <input class="checkbox-status" name="options" type="radio" value="removeall">
                                    <span class="checkbox-slider round round-radio"></span>
                                    <span>Xoá dữ liệu cũ và tạo mới</span>
                                </label>
                            </div>
                            <div class="right-file">
                                <input type="file" name="imports" id="imports-effa" class="inputfile" accept=".xls, .xlsx" data-error-type="<?php _e('Vui lòng tải tập tin đúng định dạng (xlsx, xls)'); ?>" data-error-size="<?php _e('Vui lòng tải tập tin < 5M', TPL_DOMAIN_LANG); ?>" data-multiple-caption="{count} <?= __('được chọn', TPL_DOMAIN_LANG); ?>" />
                                <label for="imports-effa"><span><i class="fa fa-upload" aria-hidden="true"></i> <?php _e('Chọn file dữ liệu', TPL_DOMAIN_LANG); ?></span></label>
                            </div>
                            <ul class="ul-upload-files has-files">
                                <li class="file-item">
                                    <span class="li-filename" title=""></span>
                                    <strong class="upload-status">
                                        <button type="button" class="btn-brb-default btn-remove-file-import">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </strong>
                                </li>
                            </ul>
                            <div class="action">
                                <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                                <button type="submit" class="btn-brb-default send-imports btn-primary has-files" id="send-imports" name="send-imports"><i class="fa fa-copy"></i> <?php  _e('Thực hiện'); ?></button>
                                <button type="button" data-dismiss="modal" class="btn-brb-default btn-cancel close-popup">
                                    <?php _e('Đóng'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <?php


     ?>
    <div class="main-content-effa">
        <?php
        $idxG = 1;
        if( !empty( $arrGroup ) ):
        ?>
        <div id="table-reponsive" class="table-reponsive grid-view grid-view-effa" data-idx="-1">
            <table class="col-md-12 table-bordered table-striped table-condensed cf floatthead">
                <?php
                    $labelSTT = __('STT', TPL_DOMAIN_LANG);
                    $labelCode= __('Mã', TPL_DOMAIN_LANG);
                    $labelLabelEfficiency = __('Năng lực', TPL_DOMAIN_LANG);
                    $labelDefine = __('Định nghĩa', TPL_DOMAIN_LANG);#__('Definition');
                    $labelEffaLV1 = __('Tối thiểu', TPL_DOMAIN_LANG);
                    $labelEffaLV2 = __('Cơ bản', TPL_DOMAIN_LANG);
                    $labelEffaLV3 = __('Đạt yêu cầu', TPL_DOMAIN_LANG);
                    $labelEffaLV4 = __('Thành thạo', TPL_DOMAIN_LANG);
                    $labelEffaLV5 = __('Xuất sắc', TPL_DOMAIN_LANG);
                    $labelQS = __('Câu hỏi', TPL_DOMAIN_LANG);
                ?>
                <thead class="cf row-thead-title-parent">
                    <tr class="row-thead-title-parent">
                        <th><?php echo $labelSTT; ?></th>
                        <th><?php echo $labelCode; ?></th>
                        <th><?php echo $labelLabelEfficiency; ?></th>
                        <th class="width20percent"></th>
                    </tr>
                </thead>
                <?php
                    $urleditGroup = add_query_arg(['action' => 'update_group_effa'], admin_url("admin-ajax.php"));
                    foreach ( $arrGroup as $key => $item_group ):
                        #foreach ( $item_group as $k => $item ) {
                        $groupID = $item_group->ID;
                        $nameGroup = get_the_title($groupID);
                        $sttRoman = convert_number_2_roman($idxG);
                        $groupCode = isset( $item_group->post_code ) && !empty( $item_group->post_code ) ? $item_group->post_code : "";
                        $groupCode = isset( $item_group->post_code ) && !empty( $item_group->post_code ) ? $item_group->post_code : "";
                        $rowItem = '';
                        if( array_key_exists( $groupID, $arrTarget ) ){
                            $targetItem = $arrTarget[$groupID];
                            $idxTg = 0;
                            $rowItem = '<tbody class="row-tbody-content-item" data-group-item="' . $idxG . '" data-group="' . $idxG . '">';
                            foreach ( $targetItem as $k => $item ){
                                $idxTg++;
                                $sttQS = 1; $rowQS = "";
                                $targetID = $item->ID;
                                $titleTarget = str_replace(str_split("\|"), "", get_the_title($targetID));
                                $contentTarget = get_the_content($targetID);
                                $postCode = !empty( $item->post_code ) ? $item->post_code : "";
                                $effaLV1 = !empty( $item->_effa_assessment_level_1 ) ? $item->_effa_assessment_level_1 : "";
                                $effaLV2 = !empty( $item->_effa_assessment_level_2 ) ? $item->_effa_assessment_level_2 : "";
                                $effaLV3 = !empty( $item->_effa_assessment_level_3 ) ? $item->_effa_assessment_level_3 : "";
                                $effaLV4 = !empty( $item->_effa_assessment_level_4 ) ? $item->_effa_assessment_level_4 : "";
                                $effaLV5 = !empty( $item->_effa_assessment_level_5 ) ? $item->_effa_assessment_level_5 : "";

                                if( isset($postQuestions[$postCode]) ){
                                    foreach ( $postQuestions[$postCode] as $kQS => $itemQS ){
                                        $QSID = $itemQS->ID;
                                        $titleQS = str_replace(str_split("\|"), "", get_the_title($itemQS));
                                        $postCodeQS = !empty( $itemQS->post_code ) ? $itemQS->post_code : "";
                                        $QS_anwser_1 = !empty( $itemQS->_effa_anwser_1 ) ? $itemQS->_effa_anwser_1 : "";
                                        $QS_anwser_2 = !empty( $itemQS->_effa_anwser_2 ) ? $itemQS->_effa_anwser_2 : "";
                                        $QS_anwser_3 = !empty( $itemQS->_effa_anwser_3 ) ? $itemQS->_effa_anwser_3 : "";
                                        $QS_anwser_4 = !empty( $itemQS->_effa_anwser_4 ) ? $itemQS->_effa_anwser_4 : "";
                                        $QS_anwser_5 = !empty( $itemQS->_effa_anwser_5 ) ? $itemQS->_effa_anwser_5 : "";

                                        $rowQS .= "<div class='col-md-6 question-item'>
                                                    <table>
                                                        <tr>
                                                            <td>".__('Câu hỏi', TPL_DOMAIN_LANG)." {$sttQS}</td>
                                                            <td><input type='hidden' name='posts[{$sttQS}][ID]' value='{$QSID}'> <textarea class=\"width100percent\" name=\"posts[{$sttQS}][post_title]\" placeholder=\"".__('Câu hỏi', TPL_DOMAIN_LANG)." {$sttQS}\" >{$titleQS}</textarea></td>
                                                        </tr>
                                                        <tr>
                                                            <td>{$labelEffaLV1}</td>
                                                            <td data-title=\"{$labelEffaLV1}\"><textarea class=\"width100percent\" name=\"posts[{$sttQS}][_effa_anwser_1]\" placeholder=\"{$labelEffaLV1}\" >{$QS_anwser_1}</textarea></td>
                                                        </tr>
                                                        <tr>
                                                            <td>{$labelEffaLV2}</td>
                                                            <td data-title=\"{$labelEffaLV2}\"><textarea class=\"width100percent\" name=\"posts[{$sttQS}][_effa_anwser_2]\" placeholder=\"{$labelEffaLV2}\" >{$QS_anwser_2}</textarea></td>
                                                        </tr>
                                                        <tr>
                                                            <td>{$labelEffaLV3}</td>
                                                            <td data-title=\"{$labelEffaLV3}\"><textarea class=\"width100percent\" type=\"text\" name=\"posts[{$sttQS}][_effa_anwser_3]\" placeholder=\"{$labelEffaLV3}\" >{$QS_anwser_3}</textarea></td>
                                                        </tr>
                                                        <tr>
                                                            <td>{$labelEffaLV4}</td>
                                                            <td data-title=\"{$labelEffaLV4}\"><textarea class=\"width100percent\" name=\"posts[{$sttQS}][_effa_anwser_4]\" placeholder=\"{$labelEffaLV4}\" >{$QS_anwser_4}</textarea></td>
                                                        </tr>
                                                        <tr>
                                                            <td>{$labelEffaLV5}</td>
                                                            <td data-title=\"{$labelEffaLV5}\"><textarea class=\"width100percent\" name=\"posts[{$sttQS}][_effa_anwser_5]\" placeholder=\"{$labelEffaLV5}\" >{$QS_anwser_5}</textarea></td>
                                                        </tr>
                                                    </table>
                                                </div>";
                                        $sttQS++;
                                    }
                                    unset($postQuestions[$postCode]);
                                }
                                /**
                                 * Kiểm tra với mỗi năng lực có đủ 3 câu hỏi chưa
                                 * Nếu chưa thì show đủ 3 form câu hỏi để người dùng nhập liệu
                                 */
                                if( ($sttQS - 1) < 3 ){
                                    for ($sttQS; $sttQS <= 3; $sttQS++){
                                        $rowQS .= "<div class='col-md-6 question-item'>
                                                    <table>
                                                        <tr>
                                                            <td>".__('Câu hỏi', TPL_DOMAIN_LANG)." {$sttQS}</td>
                                                            <td><textarea class=\"width100percent\" name=\"posts[{$sttQS}][post_title]\" placeholder=\"".__('Câu hỏi', TPL_DOMAIN_LANG)." {$sttQS}\" ></textarea></td>
                                                        </tr>
                                                        <tr>
                                                            <td>{$labelEffaLV1}</td>
                                                            <td data-title=\"{$labelEffaLV1}\"><textarea class=\"width100percent\" name=\"posts[{$sttQS}][_effa_anwser_1]\" placeholder=\"{$labelEffaLV1}\" ></textarea></td>
                                                        </tr>
                                                        <tr>
                                                            <td>{$labelEffaLV2}</td>
                                                            <td data-title=\"{$labelEffaLV2}\"><textarea class=\"width100percent\" name=\"posts[{$sttQS}][_effa_anwser_2]\" placeholder=\"{$labelEffaLV2}\" ></textarea></td>
                                                        </tr>
                                                        <tr>
                                                            <td>{$labelEffaLV3}</td>
                                                            <td data-title=\"{$labelEffaLV3}\"><textarea class=\"width100percent\" type=\"text\" name=\"posts[{$sttQS}][_effa_anwser_3]\" placeholder=\"{$labelEffaLV3}\" ></textarea></td>
                                                        </tr>
                                                        <tr>
                                                            <td>{$labelEffaLV4}</td>
                                                            <td data-title=\"{$labelEffaLV4}\"><textarea class=\"width100percent\" name=\"posts[{$sttQS}][_effa_anwser_4]\" placeholder=\"{$labelEffaLV4}\" ></textarea></td>
                                                        </tr>
                                                        <tr>
                                                            <td>{$labelEffaLV5}</td>
                                                            <td data-title=\"{$labelEffaLV5}\"><textarea class=\"width100percent\" name=\"posts[{$sttQS}][_effa_anwser_5]\" placeholder=\"{$labelEffaLV5}\" ></textarea></td>
                                                        </tr>
                                                    </table>
                                                </div>";
                                    }
                                }
                                if( !empty($rowQS) ){
                                    $rowQS = "
                                            <tr class='show-hide-questions'>
                                                <td colspan='4'>
                                                    <form id='question-{$postCode}' class='frm-questions' novalidate=\"novalidate\" action=\"".admin_url('admin-ajax.php')."\" method=\"post\">
                                                        <input type='hidden' name='action' value='update_question_effa'>
                                                        <input type='hidden' name='post_code' value='{$postCode}'>
                                                        <input type='hidden' name=\"_wpnonce\" value=\"".wp_create_nonce('action_question_'.$postCode)."\">
                                                        {$rowQS}
                                                        <div class='clearfix'></div>
                                                        <div class=\"response-container\"></div>
                                                        <div class=\"action-bot\">
                                                            <button type=\"submit\" class=\"btn-brb-default btn-save\" id=\"save-target-item\" name=\"btn-save\"><i class=\"fa fa-floppy-o\" aria-hidden=\"true\"></i> <span class=\"text-action\">".__('Cập nhật', TPL_DOMAIN_LANG)."</span></button>
                                                            <button type=\"button\"class=\"btn-brb-default btn-cancel\">".__('Đóng', TPL_DOMAIN_LANG)."</button>
                                                        </div>
                                                        
                                                   </form> 
                                                </td>
                                            </tr>
                                                ";
                                }

                                $rowItem .= "
                                <tr class=\"effa-target-item\" data-id=\"{$targetID}\">
                                    <td data-title=\"{$labelSTT}\">{$idxTg}</td>
                                    <td data-title=\"{$labelCode}\">{$postCode}</td>
                                    <td data-toggle=\"modal\" data-target=\"#popup-action-effa\" colspan=\"1\" class=\"column-content style-italic\" data-title=\"{$labelLabelEfficiency}\">{$titleTarget}</td>
                                    <td align='right' data-title='{$labelQS}'><a href='javascript:;' class='action-show-questions' data-target='#question-{$postCode}' >{$labelQS}</a></td>
                                </tr>
                                
                                {$rowQS}
                            ";

                            }
                            unset( $arrTarget[$groupID] );

                            $rowItem .= '</tbody>';
                        }
                        $params = [
                            'ID' => $groupID,
                            '_wpnonce' => wp_create_nonce("update_group_effa_{$groupID}"),
                        ];
                        $params = esc_json_attr($params);
                        ?>
                        <tbody class="row-thead-title-group" data-group="<?php echo $idxG; ?>"
                               data-idx-group="<?php echo $idxG; ?>">
                            <tr>
                                <td align="center">
                                    <a href="javascript:;" data-group-item="<?php echo $idxG; ?>" class="group-item-down"><i class="fa fa-chevron-circle-down fa-chevron-circle-up"></i></a>
                                </td>
                                <td colspan="1" class="table-responsive-hidden row-group-title group-numerical-order"><?php echo $groupCode; ?></td>
                                <td data-title="<?php echo $groupCode; ?>"
                                    class="row-group-title group-title td-editable"><span data-editable="" data-type="text" data-params="<?php echo $params; ?>" data-pk=\"1\" data-url="<?php echo $urleditGroup; ?>" data-title="<?php echo $labelLabelEfficiency; ?> <<?php esc_attr_e($nameGroup); ?>>"><?php esc_attr_e($nameGroup); ?></span></td>
                                <td colspan="1"
                                    class="row-group-title action-add-row-target action-add-row-group">
                                    <a href="javascript:;" data-toggle="modal"
                                       data-target="#popup-action-effa" data-parent="<?php echo $groupID; ?>"
                                       data-event="add-row-item"><?php _e('Thêm mục tiêu', TPL_DOMAIN_LANG); ?>
                                        <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                    <?php
                                    if( empty( $rowItem ) ):
                                        ?>
                                        <a href="javascript:;" data-action="del_group_post_effa" data-title="<?php _e('Bạn có chắc chắn xoá mục tiêu', TPL_DOMAIN_LANG); ?> <strong><?php echo mb_strtoupper($nameGroup); ?></strong>" data-id="<?php echo $groupID; ?>" data-event="del-row-group">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </tbody>
                        <?php
                        echo $rowItem;
                        $idxG++;
                        #}
                    endforeach;

                ?>
            </table>
        </div>
        <?php endif; ?>
        <div class="clearfix"></div>
        <div class="competency-for-department">
            <h3 class="import-nlcm-title mt-20"><?php _e('Năng lực chuyên môn'); ?></h3>
            <?php
            $departments = effa_site_orgchart_get_all_not_root();
            if (!empty($departments)) {
                $arrPositions = array_group_by($departments, 'room');
                $arrPositions = array_keys($arrPositions);
            }
            if( !empty($arrPositions) ){
                echo "
                    <div class='mt-20'>
                    <label>".__('Chọn phòng xem', TPL_DOMAIN_LANG).": </label>
                    <select class='selectpicker' id='department' name='department'>
                    <option value='0' >Chọn phòng</option>
                                ";
                foreach ($arrPositions as $k => $position){
                    $position_url = str_replace('&', 'and', $position);
                    $url = add_query_arg(['department' => $position_url], $current_url);
                    #$selected = isset($_GET['department']) && $_GET['department'] == $position_url ? "selected" : "";
                    echo sprintf("<option value='%s' %s >%s</option>", $position_url, $selected, $position);
                }
                echo "</select></div>";
            }
            ?>
            <div class="action-show-popup mt-20 mb-20">
                <button id="import-department" data-action="<?php echo admin_url("admin-ajax.php?action=import_effa_department");?>" type="button" data-keyboard="false" data-backdrop="static" data-target="#popup-imports" class="popup-import-effa btn-brb-default btn-primary" data-toggle="modal" ><i class="fa fa-sign-in"></i> Import</button>
            </div>
            <div class="clearfix"></div>
            <div id="table-reponsive-2" class="notboxshadow table-reponsive grid-view grid-view-effa" data-idx="-1">

            </div>
        </div>
    </div>
    <div id="popup-action-effa" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
                <form data-add-group-event="" novalidate="novalidate" action="<?= admin_url('admin-ajax.php'); ?>" method="post" class="modal-dialog frm-add-target-item">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><?php _e('Năng lực quản lý', TPL_DOMAIN_LANG); ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="table-responsive popup-table-responsive">
                                <table class="col-md-12 table-bordered table-striped table-condensed cf">
                                    <tr class="effa-code">
                                        <td class="column-1">
                                            <label for=""><?php echo $labelCode; ?>:</label>
                                        </td>
                                        <td class="column-2">
                                            <input type="hidden" name="post_id" class="post_id" value="" />
                                            <input type="text" name="post_code" placeholder="<?php echo $labelCode; ?>" class="post_code" value="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="column-1" valign="top">
                                            <label for=""><?php echo $labelLabelEfficiency; ?>:</label>
                                        </td>
                                        <td class="column-2">
                                            <input type="text" name="post_title" placeholder="<?php echo $labelLabelEfficiency; ?>" />
                                        </td>
                                    </tr>

                                    <tr class="post-child">
                                        <td class="column-1">
                                            <label><?php echo $labelDefine; ?></label>
                                        </td>
                                        <td class="column-2">
                                            <textarea name="post_content" placeholder="<?php echo $labelDefine; ?>" ></textarea>
                                        </td>
                                    </tr>
                                    <tr class="post-child">
                                        <td class="column-1">
                                            <label><?php echo $labelEffaLV1; ?></label>
                                        </td>
                                        <td class="column-2">
                                            <input type="text" name="meta_input[_effa_assessment_level_1]" placeholder="<?php echo $labelEffaLV1; ?>" />
                                        </td>
                                    </tr>
                                    <tr class="post-child">
                                        <td class="column-1">
                                            <label><?php echo $labelEffaLV2; ?></label>
                                        </td>
                                        <td class="column-2">
                                            <input type="text" name="meta_input[_effa_assessment_level_2]" placeholder="<?php echo $labelEffaLV2; ?>" />
                                        </td>
                                    </tr>
                                    <tr class="post-child">
                                        <td class="column-1">
                                            <label><?php echo $labelEffaLV3; ?></label>
                                        </td>
                                        <td class="column-2">
                                            <input type="text" name="meta_input[_effa_assessment_level_3]" placeholder="<?php echo $labelEffaLV3; ?>" />
                                        </td>
                                    </tr>
                                    <tr class="post-child">
                                        <td class="column-1">
                                            <label><?php echo $labelEffaLV4; ?></label>
                                        </td>
                                        <td class="column-2">
                                            <input type="text" name="meta_input[_effa_assessment_level_4]" placeholder="<?php echo $labelEffaLV4; ?>" />
                                        </td>
                                    </tr>
                                    <tr class="post-child">
                                        <td class="column-1">
                                            <label><?php echo $labelEffaLV5; ?></label>
                                        </td>
                                        <td class="column-2">
                                            <input type="text" name="meta_input[_effa_assessment_level_5]" placeholder="<?php echo $labelEffaLV5; ?>" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="response-container"></div>
                            <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce('create_posts'); ?>" />
                            <input type="hidden" name="post_parent" value="" />
                            <input type="hidden" value="create_edit_post_effa" name="action">
                            <div class="action-bot">
                                <button type="submit" class="btn-brb-default btn-save" data-text-add="<?php _e('Thêm', TPL_DOMAIN_LANG); ?>" data-text-edit="<?php _e('Cập nhật', TPL_DOMAIN_LANG); ?>" id="save-target-item" name="btn-save"><i class="fa fa-floppy-o" aria-hidden="true"></i> <span class="text-action"><?php  _e('Cập nhật'); ?></span></button>
                                <button data-id="" data-action="del_post_effa" data-nonce="<?php echo wp_create_nonce('delete_posts'); ?>" type="button" class="btn-brb-default btn-cancel" data-event="del-item">
                                    <i class="fa fa-trash" aria-hidden="true"></i> <?php  _e('Xóa'); ?>
                                </button>
                                <button type="button" data-dismiss="modal" class="btn-brb-default btn-cancel close-popup"><?php _e('Đóng'); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
    </div>
    <div id="myDialogConfirm" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" data-title="<?php _e('Bạn có chắc chắn xóa mục tiêu hiện tại', TPL_DOMAIN_LANG); ?>"><?php _e('Bạn có chắc chắn xóa mục tiêu hiện tại', TPL_DOMAIN_LANG); ?></h4>
                </div>
                <div class="modal-body">
                    <button type="button" class="btn-brb-default btn-primary btn-yes"><?php _e('Đồng ý', TPL_DOMAIN_LANG); ?></button>
                    <button type="button" data-dismiss="modal" class="btn-brb-default btn-cancel btn-no"><?php _e('Đóng', TPL_DOMAIN_LANG); ?></button>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

</div>
