<?php
    if( array_key_exists( 'uid', $_GET ) ){
        $userID = (int)$wpdb->escape( $_GET['uid'] );
    }

    $year = date('Y', time());
    $getUser = [];
    $user = wp_get_current_user();

    if ($userID > 0) {
        $orgchartID = isset($user->orgchart_id) ? $user->orgchart_id : 0;
        $orgchart = effa_site_user_load_orgchart($user);

        $getUser = effa_site_get_user_by_id($userID);
        $orgchartUser = effa_site_user_load_orgchart($getUser);

        $orgChartParent = $orgchartUser->parent;
        $currentOrgChartID = $orgchartUser->id;

        if (!user_is_manager()) {
            $checkChildren = effa_site_orgchart_check_on_level($currentOrgChartID, $orgchartID);

            if (empty($checkChildren)) {
                return;
            }
        }

        $current_user = $getUser;
    }
    else {
        $current_user = $user;
    }

    $assign_orgcharts = get_result_of_user($current_user);

    if ( empty($assign_orgcharts) ) {
        $assign_orgcharts = get_assign_orgcharts($current_user);
    }

    $labels = array_column($assign_orgcharts, 'post_title');

    $standards = filter_points_by_field($assign_orgcharts, 'standard');
    $scores_of_manager = filter_points_by_field($assign_orgcharts, 'manager_lv1');
    $scores_of_counsil = filter_points_by_field($assign_orgcharts, 'manager_lv0');


    $codes = array_column($assign_orgcharts, 'post_code');
    $efficiency = get_user_meta($current_user->ID, 'efficiency_' . $year, true);
    $scores = array();

    if( !empty($codes) && !empty($efficiency) ) {
        foreach ($codes as $code) {
            if(isset($efficiency[$code])) {
                $scores[] = $efficiency[$code];
            }
        }
    }

?>
<div class="col-sm-9 effa-right">
    <div class="main-content-effa">
        <div class="content-title content-effa has-position">
            <h1 class="entry-title"><?php _e('Kết Quả Bài Đánh Giá Năng Lực', TPL_DOMAIN_LANG); ?></h1>
            <h2 class="title"><?php echo $current_user->display_name; ?></h2>
        </div>
        <div class="col-md-10" style="margin-top: 20px">
            <canvas id="canvas"></canvas>
        </div>
    </div>
</div>
<script>
    var labels = <?php echo wp_json_encode($labels); ?>;
    var standards = <?php echo wp_json_encode($standards); ?>;
    var scores_of_manager = <?php echo wp_json_encode($scores_of_manager); ?>;
    var scores_of_counsil = <?php echo wp_json_encode($scores_of_counsil); ?>;
    var scores = <?php echo wp_json_encode($scores); ?>;
</script>
<?php wp_enqueue_script( 'result-js', THEME_URL . '/assets/global/scripts/result.js', array ( 'jquery' ), 1.1, true); ?>