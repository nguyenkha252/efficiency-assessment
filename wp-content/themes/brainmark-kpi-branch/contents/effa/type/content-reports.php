<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/8/18
 * Time: 4:16 PM
 */

if( array_key_exists( 'uid', $_GET ) ){
    $userID = (int)$wpdb->escape( $_GET['uid'] );
}

$rid = 0;

if( array_key_exists( 'rid', $_GET ) ){
    $rid = (int)$wpdb->escape( $_GET['rid'] );
}

$res = 0;

if( array_key_exists( 'res', $_GET ) ){
    $res = (int)$wpdb->escape( $_GET['res'] );
}

$effa = 0;

if( array_key_exists( 'effa', $_GET ) ){
    $effa = (int)$wpdb->escape( $_GET['effa'] );
}
$com = isset($_GET['com']) ? $_GET['com'] : "";


global $wp;
$settings = get_settings_website();
$background_header_table = isset($settings['background_header_table']) ? "style='background-color:{$settings['background_header_table']};'" : "#008ED5";
$year = date('Y', time());
$current_url = site_url( $wp->request );
$current_url = add_query_arg(['type' => 'reports'], $current_url);

$columnsImportant = effa_site_efficiency_get_enum_by_column('important');
$colspanImportant = count($columnsImportant);

$columnsStandard = effa_site_efficiency_get_enum_by_column('standard');
$colspanStandard = count($columnsStandard);
$rowspanDefault = !empty($columnsImportant) || !empty($columnsStandard) ? 2 : 1;
?>
<div class="col-sm-9 effa-right effa-reports-content">
    <div class="main-content-effa">
        <div id="table-reports-detail" class="table-reponsive grid-view grid-view-effa" data-idx="-1">
            <fieldset class="fielset-report">
                <legend><?php _e('Báo cáo tổng hợp năng lực theo công ty', TPL_DOMAIN_LANG) ?></legend>
                <div class="fieldset-content">
                    <div class="row">
                        <div class="col-md-4">
                            <select class="selectpicker" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                                <option value="<?php echo $current_url; ?>"><?php _e('-----------', TPL_DOMAIN_LANG); ?></option>
                                <option <?php echo $com == 'bad' ? "selected" : ""; ?> value="<?php echo add_query_arg(['com' => 'bad'], $current_url) ?>"><?php _e('5 năng lực thiếu điểm cao nhất', TPL_DOMAIN_LANG); ?></option>
                                <option <?php echo $com == 'best' ? "selected" : ""; ?> value="<?php echo add_query_arg(['com' => 'best'], $current_url) ?>"><?php _e('5 năng lực đạt điểm cao nhất', TPL_DOMAIN_LANG); ?></option>
                            </select>
                        </div>
                    </div>

                    <?php if( !empty($com) && in_array($com, ['bad', 'best']) ):
                        $standard_benchmark = $settings['standard_benchmark'] ? $settings['standard_benchmark'] : 0;
                        if( $com == 'bad' ){
                        $reports = effa_site_reports_filter_efficiency($year, $standard_benchmark, "<");
                        }else{
                        $reports = effa_site_reports_filter_efficiency($year, $standard_benchmark, ">=");
                        }

                        if( !empty($reports) ):
                        ?>

                            <div class="table-reports-content">
                                <table class="table-bordered table-striped table-condensed cf table-content-build">
                                    <?php
                                    $labelSTT = __('STT', TPL_DOMAIN_LANG);
                                    $labelCode= __('Mã NL', TPL_DOMAIN_LANG);
                                    $labelLabelEfficiency = __('Năng lực', TPL_DOMAIN_LANG);
                                    $labelCount = __('Số lượng', TPL_DOMAIN_LANG);
                                    ?>
                                    <thead class="cf row-thead-title-parent">
                                    <tr class="row-thead-title-parent">
                                        <th rowspan="<?php echo $rowspanDefault; ?>"><?php echo $labelSTT; ?></th>
                                        <th rowspan="<?php echo $rowspanDefault; ?>"><?php echo $labelCode; ?></th>
                                        <th rowspan="<?php echo $rowspanDefault; ?>"><?php echo $labelLabelEfficiency; ?></th>
                                        <th width="10%" rowspan="<?php echo $rowspanDefault; ?>"><?php echo $labelCount; ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($reports as $k => $item){
                                        echo "
                                        <tr>
                                            <td data-title='{$labelSTT}' align='center'>" . ($k+1) . "</td>
                                            <td data-title='{$labelCode}' align='center'>{$item->post_code}</td>
                                            <td data-title='{$labelLabelEfficiency}'>{$item->post_title}</td>
                                            <td data-title='{$labelCount}' align='center'>{$item->summary_post}</td>
                                        </tr>";
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="export-reports export-staff mt-2"><a <?= $background_header_table; ?> data-method="get" data-params="<?= esc_json_attr(array_merge(['_wpnonce' => wp_create_nonce('export_effa_report_for_company'), 'action'=> 'export_effa_report_for_company'], $_GET )); ?>" href="javascript:;"><?php _e('Xuất báo cáo',TPL_DOMAIN_LANG); ?></a></div>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </fieldset>
            <fieldset class="fielset-report">
                <legend><?php _e('Báo cáo tổng hợp năng lực theo tuỳ chọn', TPL_DOMAIN_LANG) ?></legend>
                <div class="fieldset-content">
                    <div class="row">
                        <div class="col-md-4">
                            <?php
                            $url = $rid == 0 ? $current_url : add_query_arg(['rid' => $rid], $current_url);
                            $url = $effa == 0 ? $url : add_query_arg(['effa' => $effa], $url);

                            echo "<label class='lbl-position'>".__('Chọn kết quả: ', TPL_DOMAIN_LANG)."</label><select class=\"selectpicker\" onchange=\"this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);\">";
                            echo sprintf("<option value=\"%s\">%s</option>", $url, __('-----------', TPL_DOMAIN_LANG));
                            echo sprintf("<option value=\"%s\" %s >%s</option>", add_query_arg(['res' => 1], $url), $res == 1 ? 'selected' : '', __('Đạt', TPL_DOMAIN_LANG));
                            echo sprintf("<option value=\"%s\" %s >%s</option>", add_query_arg(['res' => 2], $url), $res == 2 ? 'selected' : '',__('Không đạt', TPL_DOMAIN_LANG));
                            echo "</select>";
                            ?>
                        </div>
                        <div class="col-md-4">
                            <?php
                            $effas = get_posts(array(
                                'post_type' => 'efficiency',
                                'numberposts' => -1,
                                'post_parent__not_in' => array(0),
                            ));

                            $effa_url = $rid == 0 ? $current_url : add_query_arg(['rid' => $rid], $current_url);
                            $effa_url = $res == 0 ? $effa_url : add_query_arg(['res' => $res], $effa_url);

                            echo "<label class='lbl-position'>".__('Chọn năng lực: ', TPL_DOMAIN_LANG)."</label><select class=\"selectpicker\" onchange=\"this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);\">";
                            echo sprintf("<option value=\"%s\">%s</option>", $effa_url, __('-----------', TPL_DOMAIN_LANG));

                            foreach ($effas as $eff) {
                                echo sprintf("<option value=\"%s\" %s >%s</option>", add_query_arg(['effa' => $eff->ID], $effa_url), $effa == $eff->ID ? 'selected' : '', __($eff->post_title, TPL_DOMAIN_LANG));
                            }

                            echo "</select>";
                            ?>
                        </div>
                        <div class="col-md-4">
                            <?php
                            $ori_url = $res == 0 ? $current_url : add_query_arg(['res' => $res], $current_url);
                            $ori_url = $effa == 0 ? $ori_url : add_query_arg(['effa' => $effa], $ori_url);

                            $rooms = get_all_rooms();

                            echo "<label class='lbl-position'>".__('Chọn phòng ban: ', TPL_DOMAIN_LANG)."</label><select class=\"selectpicker\" onchange=\"this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);\">";
                            echo sprintf("<option value=\"%s\">%s</option>", $ori_url, __('-----------', TPL_DOMAIN_LANG));

                            foreach ($rooms as $room) {
                                echo sprintf("<option value=\"%s\" %s >%s</option>", add_query_arg(['rid' => $room->id], $ori_url), $rid == $room->id ? 'selected' : '', __($room->room, TPL_DOMAIN_LANG));
                            }

                            echo "</select>";
                            ?>
                        </div>
                    </div>
                    <div class="table-reports-content">
                        <table class="table-bordered table-striped table-condensed cf table-content-build">
                        <?php
                        $labelSTT = __('STT', TPL_DOMAIN_LANG);
                        $labelCode= __('Mã', TPL_DOMAIN_LANG);
                        $labelFullName = __('Họ và tên', TPL_DOMAIN_LANG);
                        $labelPositionName = __('Chức danh', TPL_DOMAIN_LANG);
                        $labelRoomName = __('Phòng ban', TPL_DOMAIN_LANG);
                        $labelLink = __('Liên kết', TPL_DOMAIN_LANG);
                        $labelResult = __('Kết quả', TPL_DOMAIN_LANG);
                        ?>
                        <thead class="cf row-thead-title-parent">
                        <tr class="row-thead-title-parent" <?php echo $background_header_table; ?>>
                            <th rowspan="<?php echo $rowspanDefault; ?>"><?php echo $labelSTT; ?></th>
                            <th rowspan="<?php echo $rowspanDefault; ?>"><?php echo $labelCode; ?></th>
                            <th rowspan="<?php echo $rowspanDefault; ?>"><?php echo $labelFullName; ?></th>
                            <th rowspan="<?php echo $rowspanDefault; ?>"><?php echo $labelPositionName; ?></th>
                            <th rowspan="<?php echo $rowspanDefault; ?>"><?php echo $labelRoomName; ?></th>
                            <th rowspan="<?php echo $rowspanDefault; ?>"><?php echo $labelLink; ?></th>
                            <th rowspan="<?php echo $rowspanDefault; ?>"><?php echo $labelResult; ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $users = get_reported_users($rid, $res, $effa);
                        foreach ($users as $key => $user) {
                            ?>
                            <tr>
                                <td data-title="<?php echo $labelSTT; ?>"><?php echo $key + 1; ?></td>
                                <td data-title="<?php echo $labelCode; ?>"><?php echo $user->user_nicename; ?></td>
                                <td data-title="<?php echo $labelFullName; ?>"><?php echo $user->display_name ?></td>
                                <td data-title="<?php echo $labelPositionName; ?>"><?php echo $user->name; ?></td>
                                <td data-title="<?php echo $labelRoomName; ?>"><?php echo $user->room; ?></td>
                                <td data-title="<?php echo $labelLink; ?>"><a href="<?php echo home_url() . '/?type=assessment&uid=' . $user->ID; ?>"><?php _e('Chi tiết', TPL_DOMAIN_LANG) ?></a></td>
                                <td data-title="<?php echo $labelResult; ?>"><?php echo $user->total_percent; ?>%</td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="export-reports export-staff"><a <?= $background_header_table; ?> data-method="get" data-params="<?= esc_json_attr(array_merge(['_wpnonce' => wp_create_nonce('export_effa_report_list_staff'), 'action'=> 'export_effa_report_list_staff'], $_GET )); ?>" href="javascript:;"><?php _e('Xuất báo cáo',TPL_DOMAIN_LANG); ?></a></div>
            </div>
        </div>
    </div>
</div>
