<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/5/18
 * Time: 1:15 AM
 */

global $wp;
$userID = 0;
if( array_key_exists( 'uid', $_GET ) ){
    $userID = (int)$wpdb->escape( $_GET['uid'] );
}

$current_url = site_url( $wp->request );
$current_url = add_query_arg(['type' => 'assessment'], $current_url);

$getUser = [];
$user = wp_get_current_user();
$orgchartID = isset($user->orgchart_id) ? $user->orgchart_id : 0;
$orgchart = effa_site_user_load_orgchart($user);

if( $orgchart->role == 'nhanvien' || ( isset($_GET['mode']) && $_GET['mode'] == 'own' ) ){
    get_template_part('contents/effa/type/assessment/user', 'assessment');
}else {
    if ($userID > 0) {
        $getUser = effa_site_get_user_by_id($userID);
        $orgchartUser = effa_site_user_load_orgchart($getUser);
        $orgChartParent = $orgchartUser->parent;
        $currentOrgChartID = $orgchartUser->id;
        if (!user_is_manager()) {
            $checkChildren = effa_site_orgchart_check_on_level($currentOrgChartID, $orgchartID);
            if (empty($checkChildren)) {
                return;
            }
        }
    }
    if (!empty($getUser)) {
        if (user_is_manager()) {
            get_template_part('contents/effa/type/assessment/admin', 'assessment');
        } else {

            get_template_part('contents/effa/type/assessment/manager', 'assessment');
        }
    } else {
        get_template_part('contents/effa/list', 'users');
    }
}