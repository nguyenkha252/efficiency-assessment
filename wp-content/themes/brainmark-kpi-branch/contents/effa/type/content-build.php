<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/4/18
 * Time: 3:13 PM
 */

global $wp;
$settings = get_settings_website();
#$background_header_table = isset($settings['background_header_table']) ? "background-color:{$settings['background_header_table']};" : "#008ED5";
$current_url = site_url( $wp->request );
$current_url = add_query_arg(['type' => 'build'], $current_url);
$year = date('Y', time());
$ajax = admin_url("admin-ajax.php");
$thisPosition = [];
$args_effa = [
    'post_type' => "efficiency",
    'post_status' => ['publish'],
    'posts_per_page' => -1,
    'orderby' => 'ID',
    'order' => 'ASC'
];
if( !isset($_GET['department']) ) {
    $GLOBALS['where_year'] = $year;
    add_filter('posts_clauses', 'site_posts_clauses_not_department', 100);
}else{
    $name = trim($_GET['department']);
    $name = str_replace('and', '&', $name);
    $GLOBALS['where_year'] = $year;
    $GLOBALS['department'] = $name;
    add_filter('posts_clauses', 'site_posts_clauses_department', 100);
}
$wpQuery = new WP_Query($args_effa);
$postEffa = $wpQuery->get_posts();

if(!user_is_manager()){
    $user = wp_get_current_user();
    $orgChartID = isset($user->orgchart_id) ? $user->orgchart_id : 0;
    $thisPosition = effa_site_orgchart_get_chart_by_id($orgChartID);
}
$positions = [];
if( !isset($_GET['department']) ) {
    $departments = effa_site_orgchart_get_all_not_root();
    if (!empty($departments)) {
        $arrPositions = array_group_by($departments, 'room');
        $arrPositions = array_keys($arrPositions);
    }
}else {
    $positions = effa_site_orgchart_get_department($name);
    $colspan = count($positions);
}

$arrGroup = [];
if (!empty($postEffa)) {
    $arrTarget = array_group_by($postEffa, 'post_parent');
    if (!empty($arrTarget)) {
        $arrGroup = $arrTarget[0];
        unset($arrTarget[0]);
    }
}
?>
<div class="col-sm-9 effa-right">
    <?php
    if( !empty($arrGroup) ):
    ?>
    <div class="main-content-effa">
        <?php
            if( !empty($thisPosition) ):
                $htmlPercent = "";
        ?>
                <div class="content-title content-effa has-position">
                    <h1 class="entry-title"><?php _e('Xây dựng khung năng lực', TPL_DOMAIN_LANG); ?></h1>
                    <h2 class="title"><?php echo $thisPosition->name; ?></h2>
                </div>
        <?php endif; ?>
        <div id="table-reponsive" class="table-reponsive grid-view grid-view-effa" data-idx="-1">
            <table class="table-bordered table-striped table-condensed cf table-content-build floatthead">
                <?php
                $labelSTT = __('STT', TPL_DOMAIN_LANG);
                $labelCode= __('Mã', TPL_DOMAIN_LANG);
                $labelLabelEfficiency = __('Năng lực', TPL_DOMAIN_LANG);
                $labelDefine = __('Định nghĩa', TPL_DOMAIN_LANG);#__('Definition');
                $labelEffaLV1 = __('Tối thiểu', TPL_DOMAIN_LANG);
                $labelEffaLV2 = __('Đạt yêu cầu', TPL_DOMAIN_LANG);
                $labelEffaLV3 = __('Thành thạo', TPL_DOMAIN_LANG);
                $labelEffaLV4 = __('Rất tốt', TPL_DOMAIN_LANG);
                $labelEffaLV5 = __('Xuất sắc', TPL_DOMAIN_LANG);
                ?>
                <thead class="cf row-thead-title-parent">
                <tr class="row-thead-title-parent">
                    <th class="width5"><?php echo $labelSTT; ?></th>
                    <th class="width7"><?php echo $labelCode; ?></th>
                    <th class="width30"><?php echo $labelLabelEfficiency; ?></th>
                    <?php
                        if( !isset($_GET['department']) && !empty($arrPositions) ){
                            $pcColumn = ceil(60/count($arrPositions));
                            foreach ($arrPositions as $k => $position){
                                #$position_url = str_replace(' ', '-', $position);
                                $position_url = str_replace('&', 'and', $position);
                                $url = add_query_arg(['department' => $position_url], $current_url);
                                echo sprintf("<th><a href=\"%s\">%s</a></th>",$url, $position);
                                #$htmlPercent .= sprintf("<td><span contenteditable=\"true\"></span></td>", $url, $position->name);
                            }
                        }elseif( !empty($positions) ){
                            $pcColumn = ceil(60/count($positions));
                            foreach ($positions as $k => $position){
                                $url = add_query_arg(['type' => 'standard', 'pos' => $position->id], $wp->request);
                                echo sprintf("<th><a href=\"%s\">%s</a></th>", $url, $position->name);
                                #$htmlPercent .= sprintf("<td><span contenteditable=\"true\"></span></td>", $url, $position->name);
                            }
                        }
                    ?>
                </tr>
                </thead>
                <?php
                $idxG = 1;
                $htmlGroup = "";
                if( !empty( $arrGroup ) ){
                    foreach ( $arrGroup as $key => $item_group ):
                        $groupID = $item_group->ID;
                        $nameGroup = get_the_title($groupID);
                        $sttRoman = convert_number_2_roman($idxG);
                        $groupCode = isset( $item_group->post_code ) && !empty( $item_group->post_code ) ? $item_group->post_code : "";
                        $rowItem = '';
                        if( array_key_exists( $groupID, $arrTarget ) ){
                            $targetItem = $arrTarget[$groupID];
                            $idxTg = 0;
                            #$rowItem = '<tbody class="row-tbody-content-item" data-group="' . $idxG . '">';
                            $rowItem = "";
                            foreach ( $targetItem as $k => $item ){
                                $idxTg++;
                                $targetID = $item->ID;
                                $titleTarget = str_replace(str_split("\|"), "", get_the_title($targetID));
                                $targetMeta = !empty( $item->post_code ) ? $item->post_code : "";
                                $rowsPosition = "";
                                if( !empty($positions) ){
                                    foreach ($positions as $k => $position){
                                        $assign = effa_site_assign_get_by_postid_chartid_year($targetID, $position->id, $year);
                                        $checked = !empty($assign) ? "checked" : "";
                                        $confirm = isset($assign->user_id) && $assign->user_id != '' ? true : false;
                                        $classes = strlen($position->name) > 20 ? "table-content-build-td" : "";
                                        $rowsPosition .= "<td class=\"{$classes} item-action-on-off\" data-title=\"{$position->name}\" align=\"center\">
                                        <label class=\"switch\">
                                            <input class=\"form-control-checkbox checkbox-status\" value=\"\" id=\"assign-chart-{$position->id}-{$targetID}\" name=\"assign_chart\" {$checked} data-chart-id=\"{$position->id}\" data-post-id=\"{$targetID}\" type=\"checkbox\" data-confirm='{$confirm}'>
                                            <span class=\"checkbox-slider round fa fas\"></span>
                                        </label></td>";
                                    }
                                }
                                $rowItem .= "
                                <tr class=\"effa-target-item row-tbody-content-item\" data-group-item=\"{$idxG}\"  data-id=\"{$targetID}\">
                                    <td data-title=\"{$labelSTT}\">{$idxTg}</td>
                                    <td data-title=\"{$labelCode}\">{$targetMeta}</td>
                                    <td colspan=\"\" class=\"column-content style-italic\" data-title=\"{$labelLabelEfficiency}\">{$titleTarget}</td>
                                    {$rowsPosition}
                                </tr>
                            ";
                            }
                            unset( $arrTarget[$groupID] );

                            #$rowItem .= '</tbody>';
                        }
                        $columnsPercent = "";
                        if( !empty($positions) ) {
                            foreach ($positions as $k => $position) {
                                $assignGroup = effa_site_assign_get_by_postid_chartid_year($groupID, $position->id, $year, 'group');
                                if( !empty($assignGroup->percent) ){
                                    $percent = $assignGroup->percent . "%";
                                    $emptyText = "";
                                }else{
                                    $percent = "";
                                    $emptyText = "0%";
                                }
                                #$percent = !empty($assignGroup) ? $assignGroup->percent . '%' : '';
                                $classes = strlen($position->name) > 20 ? " table-content-build-td" : "";
                                $url = add_query_arg(['action' => 'update_percent_effa'], $ajax);
                                $params = esc_json_attr(['chart_id' => $position->id, 'post_id' => $groupID, 'year' => $year]);
                                $columnsPercent .= "
                                    <td data-title=\"{$position->name}\" class=\"td-editable{$classes}\">
                                        <span data-editable=\"\" data-emptytext=\"{$emptyText}\" data-type=\"text\" data-params=\"{$params}\" data-pk=\"1\" data-url=\"{$url}\" data-title=\"{$position->name}\">{$percent}</span>
                                    </td>";
                            }
                        }
                        $htmlGroup .="
                        <tr class=\"row-thead-title-group\" data-group=\"{$idxG}\"
                               data-idx-group=\"{$idxG}\">
                           <td align=\"center\">
                                <a href=\"javascript:;\" data-group-item=\"{$idxG}\" class=\"group-item-down\"><i class=\"fa fa-chevron-circle-down fa-chevron-circle-up\"></i></a>
                            </td>
                            <td class=\"table-responsive-hidden row-group-title group-numerical-order\">{$groupCode}</td>
                            <td colspan=\"1\" data-title=\"{$groupCode}\"
                                class=\"row-group-title group-title\">".esc_attr($nameGroup)."</td>
                                {$columnsPercent}
                        </tr>
                        {$rowItem}";
                        $idxG++;
                    endforeach;
                }
                echo "<tbody>{$htmlGroup}</tbody>";
                ?>
            </table>
        </div>
    </div>
        <div id="myDialogConfirm" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
            <div class="modal-dialog width300">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" data-title="<?php _e('Năng lực đã được triển khai. Bạn có chắc chắn muốn ngừng triển khai?', TPL_DOMAIN_LANG); ?>"><?php _e('Năng lực đã được triển khai. Bạn có chắc chắn muốn ngừng triển khai?', TPL_DOMAIN_LANG); ?></h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn-brb-default btn-primary btn-yes"><?php _e('Đồng ý', TPL_DOMAIN_LANG); ?></button>
                        <button type="button" data-dismiss="modal" class="btn-brb-default btn-cancel btn-no"><?php _e('Đóng', TPL_DOMAIN_LANG); ?></button>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
