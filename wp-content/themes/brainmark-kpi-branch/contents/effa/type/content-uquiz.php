<div class="col-sm-9 effa-right">
    <div class="main-content-effa">
        <?php
        $year = date('Y', time());
        $current_user = wp_get_current_user();
        $efficiency = get_user_meta($current_user->ID, "up_efficiency_{$year}", true);
        if ( empty($efficiency)) {
            $questions = render_uquiz();
            echo $questions;
            ?>
            <div id="questionConfirm" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
                <div class="modal-dialog width300">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" data-title="<?php _e('Xác nhận gửi bài kiểm tra', TPL_DOMAIN_LANG); ?>"><?php _e('Xác nhận gửi bài kiểm tra', TPL_DOMAIN_LANG); ?></h4>
                        </div>
                        <div class="modal-body">
                            <h2 class="notification error"><?php _e('Còn một số câu hỏi bạn chưa trả lời.', TPL_DOMAIN_LANG); ?></h2>
                            <h4 class="notification error"><?php _e('Bạn có muốn hoàn thành bài kiểm tra của mình', TPL_DOMAIN_LANG); ?></h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn-brb-default btn-primary btn-yes"><?php _e('Đồng ý', TPL_DOMAIN_LANG); ?></button>
                            <button type="button" data-dismiss="modal" class="btn-brb-default btn-cancel btn-no"><?php _e('Đóng', TPL_DOMAIN_LANG); ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        else {
            echo '<div class="alert alert-primary" role="alert">' . __('Bạn đã hoàn thành bài kiểm tra.', TPL_DOMAIN_LANG) . '</div>';
        }
        ?>

    </div>
</div>