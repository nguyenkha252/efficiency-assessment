<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/4/18
 * Time: 3:13 PM
 */

global $wp;
$current_url = site_url( $wp->request );
$current_url = add_query_arg(['type' => 'build'], $current_url);
$year = date('Y', time());
$thisPosition = [];
if( isset($_GET['pos']) &&
    !empty($_GET['pos']) && effa_site_orgchart_get_chart_by_id($_GET['pos']) && user_is_manager() ){
    $thisPosition = effa_site_orgchart_get_chart_by_id($_GET['pos']);
    $positions = effa_site_orgchart_get_all_childrent_by_parent($_GET['pos']);
    $colspan = count($positions);
    $postEffa = effa_site_get_efficiency_by_chart($_GET['pos'], $year);

}elseif(!user_is_manager()){
    $user = wp_get_current_user();
    $orgChartID = isset($user->orgchart_id) ? $user->orgchart_id : 0;
    $thisPosition = effa_site_orgchart_get_chart_by_id($orgChartID);
    $positions = effa_site_orgchart_get_all_childrent_by_parent($orgChartID);
    $colspan = count($positions);
}else {
    $positions = effa_site_get_orgchart();
    $colspan = count($positions);
}
$arrPositions = array_group_by($positions, 'id');
$orgchart_ids = array_keys($arrPositions);
$postEffa = effa_site_assign_percent_by_orgcharts($orgchart_ids, $year);

$arrTarget = [];
$arrGroup = [];
if (!empty($postEffa)) {
    $arrTarget = array_group_by($postEffa, 'post_parent');
    if (!empty($arrTarget)) {
        $arrGroup = $arrTarget[0];
        unset($arrTarget[0]);
    }
}
?>
<div class="col-sm-9 effa-right">
    <?php
    if( !empty($arrGroup) ):
    ?>
    <div class="main-content-effa">
        <?php
            if( !empty($thisPosition) ):
                $htmlPercent = "";
        ?>
                <div class="content-title content-effa has-position">
                    <h1 class="entry-title"><?php _e('Tiêu chuẩn năng lực', TPL_DOMAIN_LANG); ?></h1>
                    <h2 class="title"><?php echo $thisPosition->name; ?></h2>
                </div>
        <?php endif; ?>
        <div id="table-reponsive" class="table-reponsive grid-view grid-view-effa" data-idx="-1">
            <table class="table-bordered table-striped table-condensed cf table-content-build">
                <?php
                $labelSTT = __('STT', TPL_DOMAIN_LANG);
                $labelCode= __('Mã', TPL_DOMAIN_LANG);
                $labelLabelEfficiency = __('Năng lực', TPL_DOMAIN_LANG);
                $labelDefine = __('Định nghĩa', TPL_DOMAIN_LANG);#__('Definition');
                $labelEffaLV1 = __('Tối thiểu', TPL_DOMAIN_LANG);
                $labelEffaLV2 = __('Đạt yêu cầu', TPL_DOMAIN_LANG);
                $labelEffaLV3 = __('Thành thạo', TPL_DOMAIN_LANG);
                $labelEffaLV4 = __('Rất tốt', TPL_DOMAIN_LANG);
                $labelEffaLV5 = __('Xuất sắc', TPL_DOMAIN_LANG);
                ?>
                <thead class="cf row-thead-title-parent">
                <tr class="row-thead-title-parent">
                    <th><?php echo $labelSTT; ?></th>
                    <th><?php echo $labelCode; ?></th>
                    <th><?php echo $labelLabelEfficiency; ?></th>
                    <?php
                        if( !empty($positions) ){
                            foreach ($positions as $k => $position){
                                $url = add_query_arg(['pos' => $position->id], $current_url);
                                echo sprintf("<th><a href=\"%s\">%s</a></th>", $url, $position->name);
                            }
                        }
                    ?>
                </tr>
                </thead>
                <?php
                $idxG = 1;
                $htmlGroup = "";
                if( !empty( $arrGroup ) ){
                    foreach ( $arrGroup as $key => $item_group ):
                        $columnsPercent = "";
                        $groupID = $item_group->ID;
                        $nameGroup = get_the_title($groupID);
                        $sttRoman = convert_number_2_roman($idxG);
                        $groupCode = isset( $item_group->post_code ) && !empty( $item_group->post_code ) ? $item_group->post_code : "";
                        $rowItem = '';
                        if( array_key_exists( $groupID, $arrTarget ) ){
                            $targetItem = $arrTarget[$groupID];
                            $idxTg = 0;
                            $rowItem = '<tbody class="row-tbody-content-item" data-group="' . $idxG . '">';
                            foreach ( $targetItem as $k => $item ){
                                $idxTg++;
                                $targetID = $item->ID;
                                $orgchart_id = $item->orgchart_id;
                                $titleTarget = str_replace(str_split("\|"), "", get_the_title($targetID));
                                $targetMeta = !empty( $item->post_code ) ? $item->post_code : "";
                                $rowsPosition = "";
                                if( !empty($positions) ){
                                    foreach ($positions as $k => $position){
                                        $checked = $orgchart_id == $position->id ? "checked" : "";
                                        $classes = strlen($position->name) > 23 ? "table-content-build-td" : "";
                                        $rowsPosition .= "<td class=\"{$classes} item-action-on-off\" data-title=\"{$position->name}\" align=\"center\">
                                        <label class=\"switch\">
                                            <input class=\"form-control-checkbox checkbox-status\" value=\"\" id=\"assign-chart-{$position->id}-{$targetID}\" name=\"assign_chart\" {$checked} data-chart-id=\"{$position->id}\" data-post-id=\"{$targetID}\" type=\"checkbox\">
                                            <span class=\"checkbox-slider round fa fas\"></span>
                                        </label></td>";
                                    }
                                }
                                $rowItem .= "
                                <tr class=\"effa-target-item\" data-id=\"{$targetID}\">
                                    <td data-title=\"{$labelSTT}\">{$idxTg}</td>
                                    <td data-title=\"{$labelCode}\">{$targetMeta}</td>
                                    <td colspan=\"0\" class=\"column-content style-italic\" data-title=\"{$labelLabelEfficiency}\">{$titleTarget}</td>
                                    {$rowsPosition}
                                </tr>
                            ";
                            }
                            unset( $arrTarget[$groupID] );

                            $rowItem .= '</tbody>';
                        }
                        if( !empty($positions) ) {
                            foreach ($positions as $k => $position) {
                                $columnsPercent .= "
                                    <td class=\"td-editable\">
                                        <span data-editable=\"\" data-type=\"text\" data-pk=\"1\" data-url=\"/post\" data-title=\"Enter username\"></span>
                                    </td>";
                            }
                        }
                        $htmlGroup .="
                        <tbody class=\"row-thead-title-group\" data-group=\"{$idxG}\"
                               data-idx-group=\"{$idxG}\">
                        <tr>
                            <td colspan=\"2\" class=\"table-responsive-hidden row-group-title group-numerical-order\">{$groupCode}</td>
                            <td colspan=\"1\" data-title=\"{$groupCode}\"
                                class=\"row-group-title group-title\">".esc_attr($nameGroup)."</td>
                            {$columnsPercent}
                        </tr>
                        </tbody>
                        {$rowItem}";
                        $idxG++;
                        #}
                    endforeach;
                }
                echo $htmlGroup;
                ?>
            </table>
        </div>
    </div>
    <?php endif; ?>
</div>
