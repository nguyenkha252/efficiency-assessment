<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/8/18
 * Time: 4:16 PM
 */

if( array_key_exists( 'uid', $_GET ) ){
    $userID = (int)$wpdb->escape( $_GET['uid'] );
}
$orgchartID = 0;
if( array_key_exists( 'pos', $_GET ) ){
    $orgchartID = (int)$wpdb->escape( $_GET['pos'] );
}
global $wp;
$current_url = site_url( $wp->request );
$current_url = add_query_arg(['type' => 'standard'], $current_url);
$year = date('Y', time());
$thisPosition = [];
$colspan = 0;

if( user_is_manager() ){
    $thisPosition = effa_site_orgchart_get_chart_by_id($orgchartID);
    $list_orgcharts = effa_site_orgchart_get_all_not_root();

}elseif( !user_is_manager() ) {
    $user = wp_get_current_user();
    $userID = $user->ID;
    $thisOrgchartID = isset($user->orgchart_id) ? $user->orgchart_id : 0;
    $list_orgcharts = effa_site_orgchart_get_all_childrent_by_parent($thisOrgchartID);
    if( $orgchartID > 0 ) {
        $check_children = array_filter($list_orgcharts, function ($item) use ($orgchartID) {
            return $item->id == $orgchartID;
        });
        if (empty($check_children)) {
            return;
        }
        $thisPosition = effa_site_orgchart_get_chart_by_id($orgchartID);
    }

    #$positions = effa_site_orgchart_get_all_childrent_by_parent($orgChartID);
    #$colspan = count($positions);
    #$postEffa = effa_site_get_efficiency_by_chart($orgChartID, $year);
}

$orgchartID = $thisPosition->id;

$columnsImportant = effa_site_efficiency_get_enum_by_column('important');
$colspanImportant = count($columnsImportant);

$columnsStandard = effa_site_efficiency_get_enum_by_column('standard');
$colspanStandard = count($columnsStandard);
$rowspanDefault = !empty($columnsImportant) || !empty($columnsStandard) ? 2 : 1;

$postEffa = effa_site_efficiency_get_by_orgchart($orgchartID, $year);
$arrTarget = [];
$arrGroup = [];
if (!empty($postEffa)) {
    $arrTarget = array_group_by($postEffa, 'post_parent');
    if (!empty($arrTarget)) {
        $arrGroup = $arrTarget[0];
        unset($arrTarget[0]);
    }
}

?>
<div class="col-sm-9 effa-right effa-standard-content">

    <?php
    if( !empty($list_orgcharts) ){
        $options = "<option value='{$current_url}'>".__('-----------', TPL_DOMAIN_LANG)."</option>";
        $orgcharts = array_group_by($list_orgcharts, 'room');
        foreach ( $orgcharts as $room => $room_item ){
            $options_item = "";
            if( !empty($room_item) ){
                foreach ($room_item as $item){
                    $selected = $orgchartID == $item->id ? "selected" : "";
                    $url = add_query_arg(['pos' => $item->id], $current_url);
                    $options_item .= sprintf("<option value=\"%s\" %s >%s</option>", $url, $selected, $item->name);
                }
            }
            if( !empty($options) ){
                $options .= sprintf("<optgroup label=\"%s\">%s</optgroup>", $room, $options_item);
            }else{
                $url = add_query_arg(['pos' => $room_item->id]);
                $options .= sprintf("<option value=\"%s\">%s</option>", $url, $room_item->name);
            }
        }
        echo "<label class='lbl-position'>".__('Chọn chức danh: ', TPL_DOMAIN_LANG)."</label><select class=\"selectpicker\" onchange=\"this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);\">";
        echo $options;
        echo "</select>";
    }
    if( !empty($arrGroup) ):
        ?>
        <div class="main-content-effa">
            <?php
            if( !empty($thisPosition) ):
                ?>
                <div class="content-title content-effa has-position">
                    <h1 class="entry-title"><?php _e('Tiêu chuẩn năng lực', TPL_DOMAIN_LANG); ?></h1>
                    <h2 class="title"><?php echo $thisPosition->name; ?></h2>
                </div>
            <?php endif; ?>
            <div id="table-assessment-detail" class="table-reponsive grid-view grid-view-effa" data-idx="-1">
                <table class="table-bordered table-striped table-condensed cf table-content-build floatthead">
                    <?php
                    $labelSTT = __('STT', TPL_DOMAIN_LANG);
                    $labelCode= __('Mã', TPL_DOMAIN_LANG);
                    $labelLabelEfficiency = __('Năng lực', TPL_DOMAIN_LANG);
                    $labelDefine = __('Định nghĩa', TPL_DOMAIN_LANG);#__('Definition');
                    $labelEffaLV1 = __('Tối thiểu', TPL_DOMAIN_LANG);
                    $labelEffaLV2 = __('Đạt yêu cầu', TPL_DOMAIN_LANG);
                    $labelEffaLV3 = __('Thành thạo', TPL_DOMAIN_LANG);
                    $labelEffaLV4 = __('Rất tốt', TPL_DOMAIN_LANG);
                    $labelEffaLV5 = __('Xuất sắc', TPL_DOMAIN_LANG);
                    $labelPercent = __('Trọng số', TPL_DOMAIN_LANG);
                    $labelImportant = __('Mức độ quan trọng', TPL_DOMAIN_LANG);
                    $labelStandard = __('Tiêu chuẩn', TPL_DOMAIN_LANG);
                    $labelBenchmark = __('Điểm chuẩn', TPL_DOMAIN_LANG);
                    $labelTotal = __('Tổng cộng', TPL_DOMAIN_LANG);
                    ?>
                    <thead class="cf row-thead-title-parent">
                    <tr class="row-thead-title-parent">
                        <th rowspan="<?php echo $rowspanDefault; ?>"><?php echo $labelSTT; ?></th>
                        <th rowspan="<?php echo $rowspanDefault; ?>"><?php echo $labelCode; ?></th>
                        <th rowspan="<?php echo $rowspanDefault; ?>"><?php echo $labelLabelEfficiency; ?></th>
                        <th rowspan="<?php echo $rowspanDefault; ?>"><?php echo $labelPercent; ?></th>
                        <th colspan="<?php echo $colspanImportant ?>"><?php echo $labelImportant; ?></th>

                        <th colspan="<?php echo $colspanStandard ?>"><?php echo $labelStandard; ?></th>
                        <th rowspan="<?php echo $rowspanDefault; ?>"><?php echo $labelBenchmark; ?></th>
                    </tr>
                    <tr class="row-thead-title-parent">
                        <?php
                        if( !empty($columnsImportant) ){
                            foreach ($columnsImportant as $iname){
                                echo "<th>{$iname}</th>";
                            }
                        }
                        if( !empty($columnsStandard) ){
                            foreach ($columnsStandard as $sname){
                                echo "<th>{$sname}</th>";
                            }
                        }
                        ?>

                    </tr>
                    </thead>
                    <?php
                    $idxG = 1;
                    $totalPercent = 0;
                    if( !empty( $arrGroup ) ){
                        foreach ( $arrGroup as $key => $item_group ):
                            $groupID = $item_group->pID;
                            $nameGroup = get_the_title($groupID);
                            $sttRoman = convert_number_2_roman($idxG);
                            $groupCode = isset( $item_group->post_code ) && !empty( $item_group->post_code ) ? $item_group->post_code : "";
                            $percentGroup = !empty($item_group->percent) ? $item_group->percent : 0;
                            $totalPercent += $percentGroup;
                            $rowItem = '';
                            if( array_key_exists( $groupID, $arrTarget ) ){
                                $targetItem = $arrTarget[$groupID];
                                $idxTg = 0;
                                $rowItem = '<tbody class="row-tbody-content-item" data-group="' . $idxG . '">';
                                foreach ( $targetItem as $k => $item ){
                                    $idxTg++;
                                    $targetID = $item->pID;
                                    $important = $item->important;
                                    $standard = $item->standard;
                                    $titleTarget = str_replace(str_split("\|"), "", get_the_title($targetID));
                                    $targetMeta = !empty( $item->post_code ) ? $item->post_code : "";
                                    $htmlRowImportant = "";
                                    $htmlRowStandard = "";
                                    $params = [
                                        'action' => 'standard_effa',
                                        'orgchart_id' => $orgchartID,
                                        'post_id' => $targetID,
                                        'year' => $year
                                    ];
                                    $benchmark = $important * $standard;
                                    $benchmark = $benchmark > 0 ? $benchmark : '';
                                    if( !empty($columnsImportant) ){
                                        foreach ($columnsImportant as $iname){
                                            $checked = $important == $iname ? "checked" : "";
                                            $dataImportant = ['important' => $iname];
                                            $dataImportant = array_merge($dataImportant, $params);
                                            $dataImportant = esc_json_attr($dataImportant);
                                            $htmlRowImportant .= "
                                                <td class=\"mobile-height assign-effa tooltip-notify\" align=\"center\" data-title=\"{$labelImportant} {$iname}\">
                                                    <label class=\"switch\">
                                                        <input class=\"form-control-checkbox checkbox-status\" value=\"\" id=\"important-{$iname}-{$targetID}\" name=\"important_{$targetID}\" {$checked} data-params=\"{$dataImportant}\" data-chart-id=\"{$orgchartID}\" data-year=\"{$year}\" data-post-id=\"{$targetID}\" data-value=\"".(int)$iname."\" type=\"radio\">
                                                        <span class=\"checkbox-slider round fa fas\"></span>
                                                    </label>
                                                </td>";
                                        }
                                    }
                                    if( !empty($columnsStandard) ){
                                        foreach ($columnsStandard as $sname){
                                            $checked = $standard == $sname ? "checked" : "";
                                            $dataStandard = ['standard' => $sname];
                                            $dataStandard = array_merge($dataStandard, $params);
                                            $dataStandard = esc_json_attr($dataStandard);
                                            $htmlRowStandard .= "
                                                <td class=\"assign-effa tooltip-notify\" align=\"center\" data-title=\"{$labelStandard} {$sname}\">
                                                    <label class=\"switch\">
                                                        <input class=\"form-control-checkbox checkbox-status\" value=\"\" id=\"standard-{$sname}-{$targetID}\" name=\"standard_{$targetID}\" {$checked} data-params=\"{$dataStandard}\" data-chart-id=\"{$orgchartID}\" data-year=\"{$year}\" data-post-id=\"{$targetID}\" data-value=\"".(int)$sname."\" type=\"radio\">
                                                        <span class=\"checkbox-slider round fa fas\"></span>
                                                    </label>
                                                </td>";
                                        }
                                    }
                                    $rowItem .= "
                                <tr class=\"effa-target-item row-tbody-content-item\" data-group-item=\"{$idxG}\" data-id=\"{$targetID}\">
                                    <td data-title=\"{$labelSTT}\">{$idxTg}</td>
                                    <td data-title=\"{$labelCode}\">{$targetMeta}</td>
                                    <td colspan=\"0\" class=\"column-content style-italic\" data-title=\"{$labelLabelEfficiency}\">{$titleTarget}</td>
                                    <td></td>
                                    {$htmlRowImportant}
                                    {$htmlRowStandard}
                                    <td class=\"benchmark\" align='center'>
                                        {$benchmark}
                                    </td>
                                </tr>
                            ";
                                }
                                unset( $arrTarget[$groupID] );

                                $rowItem .= '</tbody>';
                            }

                            ?>
                            <tbody class="row-thead-title-group" data-group="<?php echo $idxG; ?>"
                                   data-idx-group="<?php echo $idxG; ?>">
                            <tr>
                                <td align="center">
                                    <a href="javascript:;" data-group-item="<?php echo $idxG; ?>" class="group-item-down"><i class="fa fa-chevron-circle-down fa-chevron-circle-up"></i></a>
                                </td>
                                <td colspan="1" class="table-responsive-hidden row-group-title group-numerical-order"><?php echo $groupCode; ?></td>
                                <td colspan="<?php echo ($colspan + 1); ?>" data-title="<?php echo $groupCode; ?>"
                                    class="row-group-title group-title"><?php esc_attr_e($nameGroup); ?></td>
                                <td data-title="<?php echo $labelPercent; ?>"><?php echo $percentGroup; ?>%</td>
                                <td colspan="3"></td>
                                <td colspan="5"></td>
                                <td></td>
                            </tr>
                            </tbody>
                            <?php
                            echo $rowItem;
                            $idxG++;

                        endforeach;
                    }
                    ?>
                    <tbody>
                        <tr>
                            <td colspan="3"><?php echo $labelTotal; ?></td>
                            <td colspan="<?php echo $colspanImportant + $colspanStandard + 2 ?>"><?php echo $totalPercent ?>%</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endif; ?>
</div>
