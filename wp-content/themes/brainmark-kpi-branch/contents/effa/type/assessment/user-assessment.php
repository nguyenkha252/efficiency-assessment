<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/8/18
 * Time: 4:16 PM
 */

global $wp;
$settings = get_settings_website();
$background_header_table = isset($settings['background_header_table']) ? "style='background-color:{$settings['background_header_table']};'" : "#008ED5";
$current_url = site_url( $wp->request );
$current_url = add_query_arg(['type' => 'assessment'], $current_url);
$year = date('Y', time());
$colspan = 0;
$showColumnManager = true;
$getUser = wp_get_current_user();
$userID = $getUser->ID;
$orgchartID = isset($getUser->orgchart_id) ? $getUser->orgchart_id : 0;
$orgchart = effa_site_user_load_orgchart($getUser);
if( $orgchart->role != 'nhanvien' ){
    $showColumnManager = false;
}
$columnsImportant = effa_site_efficiency_get_enum_by_column('important');
$colspanImportant = count($columnsImportant);

$columnsStandard = effa_site_efficiency_get_enum_by_column('standard');
$colspanStandard = count($columnsStandard);
$rowspanDefault = !empty($columnsImportant) || !empty($columnsStandard) ? 2 : 1;

$postEffa = effa_site_efficiency_get_by_orgchart_user($orgchartID, $userID, $year);
$arrTarget = [];
$arrGroup = [];
if (!empty($postEffa)) {
    $arrTarget = array_group_by($postEffa, 'post_parent');
    if (!empty($arrTarget)) {
        $arrGroup = $arrTarget[0];
        unset($arrTarget[0]);
    }
}
$efficiency = get_user_meta($userID, 'efficiency', true);

?>
<div class="col-sm-9 effa-right effa-assessment-content">
    <?php
    if( !empty($list_orgcharts) ){
        $options = "<option value='{$current_url}'>".__('-----------', TPL_DOMAIN_LANG)."</option>";
        $orgcharts = array_group_by($list_orgcharts, 'room');
        foreach ( $orgcharts as $room => $room_item ){
            $options_item = "";
            if( !empty($room_item) ){
                foreach ($room_item as $item){
                    $selected = $orgchartID == $item->id ? "selected" : "";
                    $url = add_query_arg(['pos' => $item->id], $current_url);
                    $options_item .= sprintf("<option value=\"%s\" %s >%s</option>", $url, $selected, $item->name);
                }
            }
            if( !empty($options) ){
                $options .= sprintf("<optgroup label=\"%s\">%s</optgroup>", $room, $options_item);
            }else{
                $url = add_query_arg(['pos' => $room_item->id]);
                $options .= sprintf("<option value=\"%s\">%s</option>", $url, $room_item->name);
            }
        }
        echo "<label class='lbl-position'>".__('Chọn chức danh: ', TPL_DOMAIN_LANG)."</label><select class=\"selectpicker\" onchange=\"this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);\">";
        echo $options;
        echo "</select>";
    }
    if( !empty($arrGroup) ):
        ?>
        <div class="main-content-effa">
            <?php
            if( !empty($thisPosition) ):
                ?>
                <div class="content-title content-effa has-position">
                    <h1 class="entry-title"><?php _e('Tiêu chuẩn năng lực', TPL_DOMAIN_LANG); ?></h1>
                    <h2 class="title"><?php echo $thisPosition->name; ?></h2>
                </div>
            <?php endif; ?>
            <div id="table-assessment-detail" class="table-reponsive grid-view grid-view-effa" data-idx="-1">
                <table class="table-bordered table-striped table-condensed cf table-content-build floatthead">
                    <?php
                    $labelSTT = __('STT', TPL_DOMAIN_LANG);
                    $labelCode= __('Mã', TPL_DOMAIN_LANG);
                    $labelLabelEfficiency = __('Năng lực', TPL_DOMAIN_LANG);
                    $labelDefine = __('Định nghĩa', TPL_DOMAIN_LANG);#__('Definition');
                    $labelEffaLV1 = __('Tối thiểu', TPL_DOMAIN_LANG);
                    $labelEffaLV2 = __('Đạt yêu cầu', TPL_DOMAIN_LANG);
                    $labelEffaLV3 = __('Thành thạo', TPL_DOMAIN_LANG);
                    $labelEffaLV4 = __('Rất tốt', TPL_DOMAIN_LANG);
                    $labelEffaLV5 = __('Xuất sắc', TPL_DOMAIN_LANG);
                    $labelPercent = __('Trọng số', TPL_DOMAIN_LANG);
                    $labelImportant = __('Mức độ', TPL_DOMAIN_LANG);
                    $labelStandard = __('Tiêu chuẩn', TPL_DOMAIN_LANG);
                    $labelManager = __('Quản lý đánh giá', TPL_DOMAIN_LANG);
                    $labelAdmin = __('Hội đồng đánh giá', TPL_DOMAIN_LANG);
                    $labelPointsAchieved = __('Điểm đạt', TPL_DOMAIN_LANG);
                    $labelResult = __('Kết quả', TPL_DOMAIN_LANG);
                    $labelBenchmark = __('Điểm chuẩn', TPL_DOMAIN_LANG);
                    #$labelTestmark = __('Tự đánh giá', TPL_DOMAIN_LANG);
                    $labelTotal = __('Tổng cộng', TPL_DOMAIN_LANG);
                    ?>
                    <thead class="cf row-thead-title-parent">
                    <tr class="row-thead-title-parent">
                        <th class="column-stt"><?php echo $labelSTT; ?></th>
                        <th class="column-code"><?php echo $labelCode; ?></th>
                        <th class="column-competency"><?php echo $labelLabelEfficiency; ?></th>
                        <th class="column-priority"><?php echo $labelPercent; ?></th>
                        <th class="column-important"><?php echo $labelImportant; ?></th>
                        <th class="column-standard"><?php echo $labelStandard; ?></th>
                        <th class="column-benchmark"><?php echo $labelBenchmark; ?></th>
                        <?php /*
                        <th class="column-testmark"><?php echo $labelTestmark; ?></th>
                        */ ?>
                        <?php if($showColumnManager): ?>
                            <th class="column-manager"><?php echo $labelManager; ?></th>
                        <?php endif; ?>
                        <th class="column-admin"><?php echo $labelAdmin; ?></th>
                        <th class="column-point"><?php echo $labelPointsAchieved; ?></th>
                        <th class="column-result"><?php echo $labelResult; ?></th>
                    </tr>
                    </thead>
                    <?php
                    $idxG = 1;
                    $totalPercent = 0;
                    $totalResultPercent = 0;
                    if( !empty( $arrGroup ) ){
                        foreach ( $arrGroup as $key => $item_group ):
                            $groupID = $item_group->pID;
                            $nameGroup = get_the_title($groupID);
                            $sttRoman = convert_number_2_roman($idxG);
                            $groupCode = isset( $item_group->post_code ) && !empty( $item_group->post_code ) ? $item_group->post_code : "";
                            $percentGroup = !empty($item_group->percent) ? $item_group->percent : 0;
                            $totalPercent += $percentGroup;
                            $rowItem = '';
                            $sumBenchmark = 0;

                            $group_score = 0;

                            if( array_key_exists( $groupID, $arrTarget ) ){
                                $targetItem = $arrTarget[$groupID];
                                $idxTg = 0;
                                $rowItem = '<tbody class="row-tbody-content-item" data-group-item="' . $idxG . '" data-group="' . $idxG . '">';

                                foreach ( $targetItem as $k => $item ){
                                    $idxTg++;
                                    $targetID = $item->pID;
                                    $important = $item->important;
                                    $standard = $item->standard;
                                    $manager_lv1 = $item->manager_lv1 != NULL ? $item->manager_lv1 : 0;
                                    $manager_lv0 = $item->manager_lv0 != NULL ? $item->manager_lv0 : 0;

                                    $score = $important * $manager_lv0;
                                    $group_score += $score;

                                    $titleTarget = str_replace(str_split("\|"), "", get_the_title($targetID));
                                    $targetMeta = !empty( $item->post_code ) ? $item->post_code : "";
                                    $benchmark = $important * $standard;
                                    $sumBenchmark += $benchmark;
                                    $benchmark = $benchmark > 0 ? $benchmark : '';
                                    $tdManager = $showColumnManager ? "<td data-title='{$labelManager}' class='column-manager font-bold' align='center'>{$manager_lv1}</td>" : "";
                                    /*
                                     * <td data-title='{$labelTestmark}' class=\"column-testmark benchmark\" align='center'>"
                                        . ( isset($efficiency[$item->post_code] ) ? $efficiency[$item->post_code] : 0 ).
                                    "</td>*/
                                    $rowItem .= "
                                <tr class=\"effa-target-item\" data-id=\"{$targetID}\">
                                    <td class=\"column-stt\" data-title=\"{$labelSTT}\">{$idxTg}</td>
                                    <td class=\"column-code\" data-title=\"{$labelCode}\">{$targetMeta}</td>
                                    <td colspan=\"0\" class=\"column-competency column-content style-italic\" data-title=\"{$labelLabelEfficiency}\">{$titleTarget}</td>
                                    <td class=\"hide-mobile\"></td>
                                    <td data-title='{$labelImportant}' class='column-important font-bold' align='center'>{$important}</td>
                                    <td data-title='{$labelStandard}' class='column-standard font-bold' align='center'>{$standard}</td>
                                    <td data-title='{$labelBenchmark}' class=\"column-benchmark benchmark\" align='center'>
                                        {$benchmark}
                                    </td>
                                     
                                    {$tdManager}
                                    <td data-title='{$labelAdmin}' class='column-admin mobile-height td-editable td-assessment'>{$manager_lv0}</td>
                                    <td class=\"column-point\" data-title='{$labelPointsAchieved}' id='score_{$targetID}'>{$score}</td>
                                    <td class=\"column-result\" data-title='{$labelResult}' id='result_{$targetID}'>" . ($benchmark == 0 ? 0 : round($score/$benchmark * 100, 0)) . "%</td>
                                </tr>
                            ";
                                }
                                unset( $arrTarget[$groupID] );

                                $rowItem .= '</tbody>';
                            }

                            ?>
                            <tbody class="row-thead-title-group" data-group="<?php echo $idxG; ?>"
                                   data-idx-group="<?php echo $idxG; ?>">
                            <tr>
                                <td align="center">
                                    <a href="javascript:;" data-group-item="<?php echo $idxG; ?>" class="group-item-down"><i class="fa fa-chevron-circle-down fa-chevron-circle-up"></i></a>
                                </td>
                                <td colspan="1" class="column-code table-responsive-hidden row-group-title group-numerical-order"><?php echo $groupCode; ?></td>
                                <td colspan="<?php echo ($colspan + 1); ?>" data-title="<?php echo $groupCode; ?>"
                                    class="column-competency row-group-title group-title"><?php esc_attr_e($nameGroup); ?></td>
                                <td align="center" class="column-priority font-bold" data-title="<?php echo $labelPercent; ?>"><?php echo $percentGroup; ?>%</td>
                                <td class="column-important hide-mobile"></td>
                                <td class="column-standard hide-mobile"></td>
                                <td class="column-benchmark benchmark" align="center" data-title="<?= $labelTotal; ?>"><?php echo $sumBenchmark; ?></td>
                                <?php
                                /*
                                 * <td class="column-testmark hide-mobile"></td>
                                 */?>
                                <?php if($showColumnManager): ?>
                                    <td class="column-manager"></td>
                                <?php endif ?>
                                <td class="column-admin hide-mobile"></td>
                                <td class="column-point" data-title="<?php echo $labelPointsAchieved; ?>" id="group_score_<?php echo $groupID; ?>"><?php echo $group_score; ?></td>
                                <td class="column-result" data-title="<?php echo $labelResult; ?>" id="group_result_<?php echo $groupID; ?>"><?php
                                    $groupPercent = $sumBenchmark == 0 ? 0 : round($group_score/$sumBenchmark * $percentGroup, 0);
                                    $totalResultPercent += $groupPercent;
                                    echo $groupPercent;
                                    ?>%</td>
                            </tr>
                            </tbody>
                            <?php
                            echo $rowItem;
                            $idxG++;

                        endforeach;
                    }
                    ?>
                    <tbody>
                    <tr>
                        <td colspan="3"><?php echo $labelTotal; ?></td>
                        <td data-title="<?php _e('Tổng trọng số',TPL_DOMAIN_LANG); ?>" class="font-bold" align="center" colspan="1"><?php echo $totalPercent ?>%</td>
                        <td colspan="<?php echo $colspanImportant + $colspanStandard - ( ($showColumnManager) ? 1 : 3 ); ?>" class="hide-mobile"></td>
                        <td data-title="<?php _e('Tổng kết quả',TPL_DOMAIN_LANG); ?>" id="total_result" class="font-bold" colspan="1"><?php echo $totalResultPercent; ?>%</td>
                    </tr>
                    <tr>
                        <?php
                            $settings_website = get_option('settings_website');
                            $standard_benchmark = $settings_website['standard_benchmark'] ? $settings_website['standard_benchmark'] : 0;
                        ?>
                        <td data-title="<?php _e('Xếp loại', TPL_DOMAIN_LANG); ?>" class="font-bold" colspan="100%" align="right"><?php echo $totalResultPercent >= $standard_benchmark ? __('Đạt  ', TPL_DOMAIN_LANG) :  __('Không đạt', TPL_DOMAIN_LANG); ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <?php
            $comments = effa_site_comments_by_user($userID, $year, 'publish', 0);
            $comment_content = "";
            if( !empty($comments) ){
                $comment_content = $comments->comment_content;
            }
            if( !empty($comment_content) ):
            ?>
                <div class="row">
                    <div class="col-md-12 effa-comment-content">
                        <fieldset>
                            <legend><?php _e('Nhận xét', TPL_DOMAIN_LANG); ?></legend>
                            <div class="legend-content">
                                <?php echo $comment_content; ?>
                            </div>
                        </fieldset>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
</div>
