<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/8/18
 * Time: 4:16 PM
 */
$userID = 0;
if( array_key_exists( 'uid', $_GET ) ){
    $userID = (int)$wpdb->escape( $_GET['uid'] );
}
global $wp;
$settings = get_settings_website();
$background_header_table = isset($settings['background_header_table']) ? "style='background-color:{$settings['background_header_table']};'" : "#008ED5";
$current_url = site_url( $wp->request );
$current_url = add_query_arg(['type' => 'assessment'], $current_url);
$year = date('Y', time());
$ajax = admin_url("admin-ajax.php");
$thisPosition = [];
$colspan = 0;

$getUser = [];
$managerUser = wp_get_current_user();
$managerOrgchartID = isset($managerUser->orgchart_id) ? $managerUser->orgchart_id : 0;

if( $userID > 0 ) {
    $getUser = effa_site_get_user_by_id($userID);
    $thisPosition = effa_site_user_load_orgchart($getUser);
    $orgChartParent = $thisPosition->parent;
    $currentOrgChartID = $thisPosition->id;
    if( !user_is_manager() ) {
        $checkChildren = effa_site_orgchart_check_on_level($currentOrgChartID, $managerOrgchartID);
        if( empty($checkChildren) ){
            return;
        }
    }
}

$orgchartID = $thisPosition->id;

$columnsImportant = effa_site_efficiency_get_enum_by_column('important');
$colspanImportant = count($columnsImportant);

$columnsStandard = effa_site_efficiency_get_enum_by_column('standard');
$colspanStandard = count($columnsStandard);
$rowspanDefault = !empty($columnsImportant) || !empty($columnsStandard) ? 2 : 1;

$postEffa = effa_site_efficiency_get_by_orgchart_user($orgchartID, $userID, $year);
$arrTarget = [];
$arrGroup = [];
if (!empty($postEffa)) {
    $arrTarget = array_group_by($postEffa, 'post_parent');
    if (!empty($arrTarget)) {
        $arrGroup = $arrTarget[0];
        unset($arrTarget[0]);
    }
}
$efficiency = get_user_meta($userID, 'efficiency_' . $year, true);
?>
<div class="col-sm-9 effa-right effa-assessment-content">
    <?php
    if( !empty($list_orgcharts) ){
        $options = "<option value='{$current_url}'>".__('-----------', TPL_DOMAIN_LANG)."</option>";
        $orgcharts = array_group_by($list_orgcharts, 'room');
        foreach ( $orgcharts as $room => $room_item ){
            $options_item = "";
            if( !empty($room_item) ){
                foreach ($room_item as $item){
                    $selected = $orgchartID == $item->id ? "selected" : "";
                    $url = add_query_arg(['pos' => $item->id], $current_url);
                    $options_item .= sprintf("<option value=\"%s\" %s >%s</option>", $url, $selected, $item->name);
                }
            }
            if( !empty($options) ){
                $options .= sprintf("<optgroup label=\"%s\">%s</optgroup>", $room, $options_item);
            }else{
                $url = add_query_arg(['pos' => $room_item->id]);
                $options .= sprintf("<option value=\"%s\">%s</option>", $url, $room_item->name);
            }
        }
        echo "<label class='lbl-position'>".__('Chọn chức danh: ', TPL_DOMAIN_LANG)."</label><select class=\"selectpicker\" onchange=\"this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);\">";
        echo $options;
        echo "</select>";
    }
    if( !empty($arrGroup) ):
        ?>
        <div class="main-content-effa">
            <?php
            if( !empty($thisPosition) ):
                ?>
                <div class="content-title content-effa has-position">
                    <h1 class="entry-title"><?php _e('Tiêu chuẩn năng lực', TPL_DOMAIN_LANG); ?></h1>
                    <h2 class="title"><label><strong><?php _e('Họ và tên:'); ?></strong></label> <?php echo $getUser->display_name; ?></h2>
                    <h2 class="title"><label><strong><?php _e('Chức danh:'); ?></strong></label> <?php echo $thisPosition->name; ?></h2>
                </div>
            <?php endif; ?>
            <div id="table-assessment-detail" class="table-reponsive grid-view grid-view-effa" data-idx="-1">
                <table class="table-bordered table-striped table-condensed cf table-content-build floatthead">
                    <?php
                    $labelSTT = __('STT', TPL_DOMAIN_LANG);
                    $labelCode= __('Mã', TPL_DOMAIN_LANG);
                    $labelLabelEfficiency = __('Năng lực', TPL_DOMAIN_LANG);
                    $labelDefine = __('Định nghĩa', TPL_DOMAIN_LANG);#__('Definition');
                    $labelEffaLV1 = __('Tối thiểu', TPL_DOMAIN_LANG);
                    $labelEffaLV2 = __('Đạt yêu cầu', TPL_DOMAIN_LANG);
                    $labelEffaLV3 = __('Thành thạo', TPL_DOMAIN_LANG);
                    $labelEffaLV4 = __('Rất tốt', TPL_DOMAIN_LANG);
                    $labelEffaLV5 = __('Xuất sắc', TPL_DOMAIN_LANG);
                    $labelPercent = __('Trọng số', TPL_DOMAIN_LANG);
                    $labelImportant = __('Mức độ', TPL_DOMAIN_LANG);
                    $labelStandard = __('Tiêu chuẩn', TPL_DOMAIN_LANG);
                    $labelManager = __('Quản lý đánh giá', TPL_DOMAIN_LANG);
                    $labelBenchmark = __('Điểm chuẩn', TPL_DOMAIN_LANG);
                    $labelTestmark = __('Tự đánh giá', TPL_DOMAIN_LANG);
                    $labelTotal = __('Tổng cộng', TPL_DOMAIN_LANG);
                    ?>
                    <thead class="cf row-thead-title-parent">
                    <tr class="row-thead-title-parent">
                        <th class="column-stt"><?php echo $labelSTT; ?></th>
                        <th class="column-code"><?php echo $labelCode; ?></th>
                        <th class="column-competency"><?php echo $labelLabelEfficiency; ?></th>
                        <th class="column-priority"><?php echo $labelPercent; ?></th>
                        <th class="column-important"><?php echo $labelImportant; ?></th>
                        <th class="column-standard"><?php echo $labelStandard; ?></th>
                        <th class="column-benchmark"><?php echo $labelBenchmark; ?></th>
                        <th class="column-testmark"><?php echo $labelTestmark; ?></th>
                        <th class="column-manager"><?php echo $labelManager; ?></th>
                    </tr>
                    </thead>
                    <?php
                    $idxG = 1;
                    $totalPercent = 0;
                    if( !empty( $arrGroup ) ){
                        foreach ( $arrGroup as $key => $item_group ):
                            $groupID = $item_group->pID;
                            $nameGroup = get_the_title($groupID);
                            $sttRoman = convert_number_2_roman($idxG);
                            $groupCode = isset( $item_group->post_code ) && !empty( $item_group->post_code ) ? $item_group->post_code : "";
                            $percentGroup = !empty($item_group->percent) ? $item_group->percent : 0;
                            $totalPercent += $percentGroup;
                            $rowItem = '';
                            $sumBenchmark = 0;
                            if( array_key_exists( $groupID, $arrTarget ) ){
                                $targetItem = $arrTarget[$groupID];
                                $idxTg = 0;
                                $rowItem = '<tbody class="row-tbody-content-item" data-group-item="' . $idxG . '" data-group="' . $idxG . '">';
                                foreach ( $targetItem as $k => $item ){
                                    $idxTg++;
                                    $targetID = $item->pID;
                                    $important = $item->important;
                                    $standard = $item->standard;
                                    $manager_lv1 = $item->manager_lv1 != NULL ? $item->manager_lv1 : 0;
                                    if( !empty($manager_lv1) ){
                                        $text_manager_lv1 = $manager_lv1;
                                        $emptyText = "";
                                    }else{
                                        $text_manager_lv1 = "0";
                                        $emptyText = "0";
                                    }
                                    $titleTarget = str_replace(str_split("\|"), "", get_the_title($targetID));
                                    $targetMeta = !empty( $item->post_code ) ? $item->post_code : "";
                                    $benchmark = $important * $standard;
                                    $sumBenchmark += $benchmark;
                                    $benchmark = $benchmark > 0 ? $benchmark : '';
                                    $url = add_query_arg(['action' => 'assessment_manager_effa'], $ajax);
                                    $params = [
                                        'user_id' => $userID,
                                        'id' => $item->id,
                                        'post_id' => $targetID,
                                        'year' => $year,
                                    ];
                                    $params = esc_json_attr($params);
                                    $rowItem .= "
                                <tr class=\"effa-target-item\" data-id=\"{$targetID}\">
                                    <td class=\"column-stt\" data-title=\"{$labelSTT}\">{$idxTg}</td>
                                    <td class=\"column-code\" data-title=\"{$labelCode}\">{$targetMeta}</td>
                                    <td colspan=\"0\" class=\"column-competency column-content style-italic columnCompetencyEvent\" data-title=\"{$labelLabelEfficiency}\">{$titleTarget}</td>
                                    <td class=\"hide-mobile\"></td>
                                    <td class=\"column-important\" data-title='{$labelImportant}' class='font-bold' align='center'>{$important}</td>
                                    <td class=\"column-standard\" data-title='{$labelStandard}' class='font-bold' align='center'>{$standard}</td>
                                    <td class=\"column-benchmark benchmark\" align='center'>
                                        {$benchmark}
                                    </td>
                                    <td data-title='{$labelTestmark}' class=\"columm-testmark benchmark\" align='center'>"
                                        . ( isset($efficiency[$item->post_code] ) ? $efficiency[$item->post_code] : 0 ).
                                    "</td>
                                    <td class='column-manager td-editable td-assessment'><span class=\"regard-score\" data-emptytext=\"{$emptyText}\" data-type=\"text\" data-params=\"{$params}\" data-pk=\"1\" data-url=\"{$url}\" data-title=\"{$titleTarget}\">{$text_manager_lv1}</span><span class=\"popSeclectScore\"></span></td>
                                </tr>
                            ";
                                }
                                unset( $arrTarget[$groupID] );

                                $rowItem .= '</tbody>';
                            }

                            ?>
                            <tbody class="row-thead-title-group" data-group="<?php echo $idxG; ?>"
                                   data-idx-group="<?php echo $idxG; ?>">
                            <tr>
                                <td align="center">
                                    <a href="javascript:;" data-group-item="<?php echo $idxG; ?>" class="group-item-down"><i class="fa fa-chevron-circle-down fa-chevron-circle-up"></i></a>
                                </td>
                                <td colspan="1" class="column-code table-responsive-hidden row-group-title group-numerical-order"><?php echo $groupCode; ?></td>
                                <td colspan="<?php echo ($colspan + 1); ?>" data-title="<?php echo $groupCode; ?>"
                                    class="column-competency row-group-title group-title"><?php esc_attr_e($nameGroup); ?></td>
                                <td align="center" class="column-priority font-bold" data-title="<?php echo $labelPercent; ?>"><?php echo $percentGroup; ?>%</td>
                                <td class="column-important hide-mobile"></td>
                                <td class="column-standard hide-mobile"></td>
                                <td class="column-benchmark benchmark" align="center" data-title="<?= $labelTotal; ?>"><?php echo $sumBenchmark; ?></td>
                                <td class="columm-testmark hide-mobile"></td>
                                <td class="column-manager hide-mobile"></td>
                            </tr>
                            </tbody>
                            <?php
                            echo $rowItem;
                            $idxG++;

                        endforeach;
                    }
                    ?>
                    <tbody>
                    <tr>
                        <td colspan="3"><?php echo $labelTotal; ?></td>
                        <td data-title="<?php _e('Tổng trọng số',TPL_DOMAIN_LANG); ?>" class="font-bold" align="center" colspan="1"><?php echo $totalPercent ?>%</td>
                        <td colspan="<?php echo $colspanImportant + $colspanStandard + 1 ?>"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="view-chart-js"><a <?= $background_header_table; ?> href=" <?php echo home_url() . "?type=result&uid=" . $userID ?>"><?php _e('Xem biểu đồ',TPL_DOMAIN_LANG); ?></a></div>
            </div>
        </div>
    <?php endif; ?>
</div>
