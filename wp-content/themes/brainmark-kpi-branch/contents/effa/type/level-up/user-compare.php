<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/5/18
 * Time: 1:15 AM
 */

global $wp;
$getUser = [];
$thisPosition = [];
$assignLV = [];
if( isset($_GET['lv']) && !empty($_GET['lv']) ){
    $lv = $_GET['lv'];
    $assignLV = effa_site_level_up_assign_get_by_id($lv);
    if( !empty($assignLV) ) {
        $userID = $assignLV->user_id;
        $getUser = effa_site_get_user_by_id($userID);
        $thisPosition = effa_site_user_load_orgchart($getUser);
    }
}else{
    return;
}
$settings = get_settings_website();
$background_header_table = isset($settings['background_header_table']) ? "style='background-color:{$settings['background_header_table']};'" : "#008ED5";

$current_url = site_url( $wp->request );
$current_url = add_query_arg(['type' => 'compare', 'lv' => $assignLV->id], $current_url);

$year = date('Y', time());
$ajax = admin_url("admin-ajax.php");
$colspan = 0;

$orgchartID = $thisPosition->id;

$columnsImportant = effa_site_level_up_get_enum_by_column('important');
$colspanImportant = count($columnsImportant);

$columnsStandard = effa_site_level_up_get_enum_by_column('standard');
$colspanStandard = count($columnsStandard);
$rowspanDefault = !empty($columnsImportant) || !empty($columnsStandard) ? 2 : 1;

$postEffa = effa_site_level_up_get_by_assign_lv_id($assignLV->id, $year);
$arrTarget = [];
$arrGroup = [];
if (!empty($postEffa)) {
    $arrTarget = array_group_by($postEffa, 'post_parent');
    if (!empty($arrTarget)) {
        $arrGroup = $arrTarget[0];
        unset($arrTarget[0]);
    }
}
$efficiency = get_user_meta($userID, 'up_efficiency_' . $year, true);

?>
<div class="col-sm-9 effa-right effa-assessment-content">
    <?php
    if( !empty($arrGroup) ):
        ?>
        <div class="main-content-effa">
            <?php
            if( !empty($thisPosition) ):
                ?>
                <div class="content-title content-effa has-position">
                    <h1 class="entry-title"><?php _e('So sánh năng lực', TPL_DOMAIN_LANG); ?></h1>
                    <h2 class="title"><label><strong><?php _e('Chức danh hiện tại:'); ?></strong></label> <?php echo $assignLV->org_name_old; ?></h2>
                    <h2 class="title"><label><strong><?php _e('Chức danh mới:'); ?></strong></label> <?php echo $assignLV->org_name_new; ?></h2>
                </div>
            <?php endif; ?>
            <div id="table-assessment-detail" class="table-reponsive grid-view grid-view-effa" data-idx="-1">
                <table class="table-bordered table-striped table-condensed cf table-content-build">
                    <?php
                    $labelSTT = __('STT', TPL_DOMAIN_LANG);
                    $labelCode= __('Mã', TPL_DOMAIN_LANG);
                    $labelLabelEfficiency = __('Năng lực', TPL_DOMAIN_LANG);
                    $labelDefine = __('Định nghĩa', TPL_DOMAIN_LANG);#__('Definition');
                    $labelEffaLV1 = __('Tối thiểu', TPL_DOMAIN_LANG);
                    $labelEffaLV2 = __('Đạt yêu cầu', TPL_DOMAIN_LANG);
                    $labelEffaLV3 = __('Thành thạo', TPL_DOMAIN_LANG);
                    $labelEffaLV4 = __('Rất tốt', TPL_DOMAIN_LANG);
                    $labelEffaLV5 = __('Xuất sắc', TPL_DOMAIN_LANG);
                    $labelPercent = __('Trọng số', TPL_DOMAIN_LANG);
                    $labelImportant = __('Mức độ', TPL_DOMAIN_LANG);
                    $labelStandard = __('Tiêu chuẩn', TPL_DOMAIN_LANG);
                    $labelManager = __('Quản lý đánh giá', TPL_DOMAIN_LANG);
                    $labelAdmin = __('Hội đồng đánh giá', TPL_DOMAIN_LANG);
                    $labelPointsAchieved = __('Điểm đạt', TPL_DOMAIN_LANG);
                    $labelResult = __('Kết quả', TPL_DOMAIN_LANG);
                    $labelBenchmark = __('Điểm chuẩn', TPL_DOMAIN_LANG);
                    $labelTestmark = __('Tự đánh giá', TPL_DOMAIN_LANG);
                    $labelTotal = __('Tổng cộng', TPL_DOMAIN_LANG);
                    ?>
                    <thead class="cf row-thead-title-parent">
                    <tr class="row-thead-title-parent" <?php echo $background_header_table; ?>>
                        <th><?php echo $labelSTT; ?></th>
                        <th><?php echo $labelCode; ?></th>
                        <th><?php echo $labelLabelEfficiency; ?></th>
                        <th><?php echo $labelPercent; ?></th>
                        <th><?php echo $labelImportant; ?></th>
                        <th><?php echo $labelStandard; ?></th>
                        <th><?php echo $labelBenchmark; ?></th>
                        <th><?php echo $labelTestmark; ?></th>
                        <th><?php echo $labelAdmin; ?></th>
                        <th><?php echo $labelPointsAchieved; ?></th>
                        <th><?php echo $labelResult; ?></th>
                    </tr>
                    </thead>
                    <?php
                    $idxG = 1;
                    $totalPercent = 0;
                    $totalResultPercent = 0;
                    if( !empty( $arrGroup ) ){
                        foreach ( $arrGroup as $key => $item_group ):
                            $groupID = $item_group->pID;
                            $nameGroup = get_the_title($groupID);
                            $sttRoman = convert_number_2_roman($idxG);
                            $groupCode = isset( $item_group->post_code ) && !empty( $item_group->post_code ) ? $item_group->post_code : "";
                            $percentGroup = !empty($item_group->percent) ? $item_group->percent : 0;
                            $totalPercent += $percentGroup;
                            $rowItem = '';
                            $sumBenchmark = 0;

                            $group_score = 0;

                            if( array_key_exists( $groupID, $arrTarget ) ){
                                $targetItem = $arrTarget[$groupID];
                                $idxTg = 0;
                                $rowItem = '<tbody class="row-tbody-content-item" data-group="' . $idxG . '">';

                                foreach ( $targetItem as $k => $item ){
                                    $idxTg++;
                                    $targetID = $item->pID;
                                    $important = $item->important;
                                    $standard = $item->standard;
                                    $manager_lv1 = $item->manager_lv1 != NULL ? $item->manager_lv1 : 0;
                                    $manager_lv0 = $item->manager_lv0 != NULL ? $item->manager_lv0 : 0;

                                    $score = $important * $manager_lv0;
                                    $group_score += $score;

                                    $titleTarget = str_replace(str_split("\|"), "", get_the_title($targetID));
                                    $targetMeta = !empty( $item->post_code ) ? $item->post_code : "";
                                    $benchmark = $important * $standard;
                                    $sumBenchmark += $benchmark;
                                    $benchmark = $benchmark > 0 ? $benchmark : '';
                                    $rowItem .= "
                                <tr class=\"effa-target-item\" data-id=\"{$targetID}\">
                                    <td data-title=\"{$labelSTT}\">{$idxTg}</td>
                                    <td data-title=\"{$labelCode}\">{$targetMeta}</td>
                                    <td colspan=\"0\" class=\"column-content style-italic\" data-title=\"{$labelLabelEfficiency}\">{$titleTarget}</td>
                                    <td class=\"hide-mobile\"></td>
                                    <td data-title='{$labelImportant}' class='font-bold' align='center'>{$important}</td>
                                    <td data-title='{$labelStandard}' class='font-bold' align='center'>{$standard}</td>
                                    <td data-title='{$labelBenchmark}' class=\"benchmark\" align='center'>
                                        {$benchmark}
                                    </td>
                                     <td data-title='{$labelTestmark}' class=\"benchmark\" align='center'>"
                                        . ( isset($efficiency[$item->post_code] ) ? $efficiency[$item->post_code] : 0 ).
                                        "</td>
                                    <td data-title='{$labelAdmin}' class='mobile-height td-assessment'>{$manager_lv0}</td>
                                    <td data-title='{$labelPointsAchieved}' id='score_{$targetID}'>{$score}</td>
                                    <td data-title='{$labelResult}' id='result_{$targetID}'>" . ($benchmark == 0 ? 0 : round($score/$benchmark * 100, 0)) . "%</td>
                                </tr>
                            ";
                                }
                                unset( $arrTarget[$groupID] );

                                $rowItem .= '</tbody>';
                            }

                            ?>
                            <tbody class="row-thead-title-group" data-group="<?php echo $idxG; ?>"
                                   data-idx-group="<?php echo $idxG; ?>">
                            <tr>
                                <td colspan="2" class="table-responsive-hidden row-group-title group-numerical-order"><?php echo $groupCode; ?></td>
                                <td colspan="<?php echo ($colspan + 1); ?>" data-title="<?php echo $groupCode; ?>"
                                    class="row-group-title group-title"><?php esc_attr_e($nameGroup); ?></td>
                                <td align="center" class="font-bold" data-title="<?php echo $labelPercent; ?>"><?php echo $percentGroup; ?>%</td>
                                <td class="hide-mobile"></td>
                                <td class="hide-mobile"></td>
                                <td class="benchmark" align="center" data-title="<?= $labelTotal; ?>"><?php echo $sumBenchmark; ?></td>
                                <td class="hide-mobile"></td>
                                <td class="hide-mobile"></td>
                                <td data-title="<?php echo $labelPointsAchieved; ?>" id="group_score_<?php echo $groupID; ?>"><?php echo $group_score; ?></td>
                                <td data-title="<?php echo $labelResult; ?>" id="group_result_<?php echo $groupID; ?>"><?php
                                    $groupPercent = $sumBenchmark == 0 ? 0 : round($group_score/$sumBenchmark * $percentGroup, 0);
                                    $totalResultPercent += $groupPercent;
                                    echo $groupPercent;
                                    ?>%</td>
                            </tr>
                            </tbody>
                            <?php
                            echo $rowItem;
                            $idxG++;

                        endforeach;
                    }
                    ?>
                    <tbody>
                    <tr>
                        <td colspan="3"><?php echo $labelTotal; ?></td>
                        <td data-title="<?php _e('Tổng trọng số',TPL_DOMAIN_LANG); ?>" class="font-bold" align="center" colspan="1"><?php echo $totalPercent ?>%</td>
                        <td colspan="<?php echo $colspanImportant + $colspanStandard - 2; ?>" class="hide-mobile"></td>
                        <td data-title="<?php _e('Tổng kết quả',TPL_DOMAIN_LANG); ?>" id="total_result" class="font-bold" colspan="1"><?php echo $totalResultPercent; ?>%</td>
                    </tr>
                    <tr>
                        <td colspan="8"><?php _e('Đánh giá', TPL_DOMAIN_LANG); ?></td>
                        <td colspan="3" data-title="<?php _e('Đánh giá', TPL_DOMAIN_LANG); ?>" class="font-bold" align="left">
                            <?php

                            $arrStatus = [
                                'publish' => __('Đang thực hiện', TPL_DOMAIN_LANG),
                                'success' => __('Hoàn thành', TPL_DOMAIN_LANG),
                                'failed' => __('Trượt', TPL_DOMAIN_LANG),
                                'trash' => __('Bỏ cuộc', TPL_DOMAIN_LANG),
                            ];
                            $default_select = isset($assignLV->status) ? $assignLV->status : "publish";
                            ?>
                           <?= $arrStatus[$default_select]; ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <?php
                $comments = effa_site_comments_by_user($userID, $year, 'publish', 0, 'assign_level_up');
                $emptytext = __('Chưa có lời nhận xét', TPL_DOMAIN_LANG);
                $comment_content = "";
                if( !empty($comments) ){
                    $comment_content = strip_tags($comments->comment_content);
                }
                ?>
                <div class="col-md-12 effa-comment-content">
                    <fieldset>
                        <legend><?php _e('Nhận xét', TPL_DOMAIN_LANG); ?></legend>
                        <div class="legend-content">
                           <?php echo $comment_content; ?>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
