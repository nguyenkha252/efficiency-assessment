<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/23/18
 * Time: 3:39 PM
 */

$settings = get_settings_website();
$background_color = isset($settings['background_header_table']) ? "style='background-color:{$settings['background_header_table']};'" : "#008ED5";
$current_url = site_url( $wp->request );
$current_url = add_query_arg(['type' => 'level-up'], $current_url);
$year = date('Y', time());
$user = wp_get_current_user();

?>

<div class="col-sm-9 effa-right effa-compare-content">
    <div class="main-content-effa">
        <?php
        $listAssignLV = effa_site_level_up_list_by_user($user->ID);
        if( !empty($listAssignLV) ):
            ?>
            <div id="table-compare-detail" class="table-reponsive grid-view grid-view-effa">
                <table class="table-bordered table-striped table-condensed cf table-content-build">
                    <?php
                    $labelSTT = __('STT', TPL_DOMAIN_LANG);
                    $labelThisPosition = __('Vị trí hiện tại', TPL_DOMAIN_LANG);
                    $labelPositionNew = __('Vị trí so sánh', TPL_DOMAIN_LANG);
                    $labelStatus = __('Tình trạng', TPL_DOMAIN_LANG);
                    $labelDetail = __('Chi tiết', TPL_DOMAIN_LANG);
                    ?>
                    <thead class="cf row-thead-title-parent">
                    <tr class="row-thead-title-parent" <?php echo $background_color; ?>>
                        <th ><?php echo $labelSTT; ?></th>
                        <th ><?php echo $labelThisPosition; ?></th>
                        <th ><?php echo $labelPositionNew; ?></th>
                        <th ><?php echo $labelStatus; ?></th>
                        <th ></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $arrStatus = [
                        'publish' => __('Đang thực hiện', TPL_DOMAIN_LANG),
                        'failed' => __('Trượt', TPL_DOMAIN_LANG),
                        'success' => __('Hoàn thành', TPL_DOMAIN_LANG),
                        'trash' => __('Đã bỏ', TPL_DOMAIN_LANG),
                        'finish-quiz' => __('Đã hoàn thành bài kiểm tra. <br> Đang đợi đánh giá', TPL_DOMAIN_LANG),
                    ];
                    foreach ($listAssignLV as $k => $assign):
                        $status = isset($arrStatus[$assign->status]) ? $arrStatus[$assign->status] : "";
                        $linkDetail = add_query_arg(['mode' => 'view', 'lv' => $assign->id], $current_url);
                        ?>
                        <tr>
                            <td data-title="<?= $labelSTT; ?>"><?= $k+1; ?></td>
                            <td data-title="<?= $labelThisPosition; ?>"><?= $assign->org_name_old; ?></td>
                            <td data-title="<?= $labelPositionNew; ?>"><?= $assign->org_name_new; ?></td>
                            <td data-title="<?= $labelStatus; ?>"><?= $status ?></td>
                            <td data-title=""><a href="<?= $linkDetail; ?>"><?= $labelDetail; ?></a></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>
</div>
