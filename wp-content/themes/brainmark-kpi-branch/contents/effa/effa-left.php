<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/3/18
 * Time: 5:02 PM
 */

global $post;
$current_url = get_permalink($post);
$user = wp_get_current_user();
$orgchart = effa_site_user_load_orgchart($user);
$effa_menu_left = effa_site_get_define_menu_left();
?>
<div id="menu-left" class="col-sm-3">
    <?php if( !empty($effa_menu_left) ): ?>
        <ul class="effa-menu-left navbar-left">
        <?php
            $type = isset($_GET['type']) ? $_GET['type'] : '';
            $type = $type == 'standard' ? 'build' : $type;
            foreach ( $effa_menu_left as $k => $item ){
                if( user_is_manager() || ( isset($item['has_role']) && in_array($orgchart->role, $item['has_role']) ) || !isset($item['has_role']) ) {
                    $classes = isset($item['classes']) ? $item['classes'] : "";
                    if( $k == 'level-up' ){
                        $assignLV = effa_site_level_up_assign_get_by_user_id($user->ID);
                        if( !empty($assignLV) ){
                            $count_publish = 0;
                            $count_publish_html = "";
                            foreach ($assignLV as $assign){
                                if( $assign->status == 'publish' ){
                                    $count_publish++;
                                }
                            }
                            if( $count_publish > 0 ){
                                $count_publish_html = "<sup>{$count_publish}</sup>";
                            }
                            $url = add_query_arg(['type' => 'level-up'], $current_url);
                            $active = $type == 'level-up' ? "active" : "";
                            echo sprintf("<li class='nav-item %s %s'><a href='%s'>%s %s</a></li>", $active, $classes, $url, __('NĂNG LỰC THĂNG CẤP', TPL_DOMAIN_LANG), $count_publish_html);
                        }
                    }else {
                        $active = $type == $k ? "active" : "";
                        $url = add_query_arg(['type' => $k], $current_url);
                        echo sprintf("<li class='nav-item %s %s'><a href='%s'>%s</a></li>", $active, $classes, $url, $item['name']);
                    }
                }
            }
        ?>
        </ul>
    <?php endif; ?>
</div>
