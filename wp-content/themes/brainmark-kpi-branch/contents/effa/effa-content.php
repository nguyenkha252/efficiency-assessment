<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/3/18
 * Time: 5:03 PM
 */

if( isset($_GET['type'])){
    $type = $_GET['type'];
    $type_temp = $type == 'standard' ? 'build' : $type;
    if( file_exists(THEME_DIR .'/contents/effa/type/content-'.$type.'.php') ) {
        $user = wp_get_current_user();
        $orgchart = effa_site_user_load_orgchart($user);
        $effa_menu_left = effa_site_get_define_menu_left();
        $thisPage = isset($effa_menu_left[$type_temp]) ? $effa_menu_left[$type_temp] : [];
        if( !empty( $thisPage ) ){
            if(user_is_manager() || ( isset($thisPage['has_role']) && in_array($orgchart->role, $thisPage['has_role']) ) || !isset($thisPage['has_role'])){
                get_template_part('contents/effa/type/content', $type);
            }
        }
    }
}