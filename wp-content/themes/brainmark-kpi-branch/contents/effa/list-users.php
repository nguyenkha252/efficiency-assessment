<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/5/18
 * Time: 11:45 AM
 */
$settings = get_settings_website();
$background_header_table = isset($settings['background_header_table']) ? "style='background-color:{$settings['background_header_table']};'" : "#008ED5";
global $wp;
$current_url = site_url( $wp->request );
$current_url = add_query_arg(['type' => 'assessment'], $current_url);
$user = wp_get_current_user();
$orgchart_id = isset($user->orgchart_id) ? $user->orgchart_id : 0;
$orgCharts = effa_site_orgchart_get_all_childrent_by_parent($orgchart_id);
$arrCharts = [];
if( !empty( $orgCharts ) ){
    foreach ( $orgCharts as $key => $item ){
        $arrCharts[] = $item->id;
    }
}
$arrCharts = implode(", ", $arrCharts);
$users = effa_site_user_get_user_by_chart( $arrCharts );
if( !empty( $users ) ){
    $groupUsers = array_group_by($users, 'room');
}else{
    $groupUsers = [];
}

$output = effa_site_render_users_for_tree($groupUsers, $current_url);
?>
<div class="col-sm-9 effa-right">
    <div class="main-content-effa">
        <div class="content-title content-effa">
            <h1><?php _e('Danh sách nhân viên', TPL_DOMAIN_LANG); ?></h1>
            <?php if( !user_is_manager() ): ?>
            <div class="effa-owner">
                <a href="<?php echo add_query_arg(['mode' => 'own'], $current_url); ?>" class="btn-effa-owner">Cá Nhân</a>
            </div>
            <?php endif; ?>
        </div>
        <div id="table-reponsive" class="table-reponsive grid-view grid-view-effa" data-idx="-1">
            <table class="table-bordered table-striped table-condensed cf table-content-build user-list list-staff" cellpadding="0" cellspacing="0" width="100%">
                <thead>
                <tr class="row-thead-title-parent" <?php echo $background_header_table; ?>>
                    <th class="id-col">STT</th>
                    <th>Mã nhân viên</th>
                    <th class="name"><?php _e('Name', TPL_DOMAIN_LANG); ?></th>
                    <th class="position_name"><?php _e('Position Name', TPL_DOMAIN_LANG); ?></th>
                    <th>Phòng ban / Công ty</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                echo $output;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>