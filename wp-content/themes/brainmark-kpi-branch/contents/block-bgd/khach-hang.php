<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 31/12/2017
 * Time: 23:18
 */
?>
<div class="content-list-register-customer content-list-register">
	<table class="table-block-list-register table-list-customer" cellspacing="0" cellpadding="0">
		<thead>
		<tr>
			<th class="column-1-2" colspan="2"><?php _e('Khách hàng', TPL_DOMAIN_LANG); ?></th>
			<th class="percent column-3">
				<span class="icon-percent" contenteditable="true">100<span class="kt-percent">%</span></span>
				<input type="text" class="txt-percent-kpi" value="30" />
			</th>
		</tr>
		</thead>
		<tbody>
		<?php
		for( $i = 0; $i < 3; $i++ ){
			$percent = $i==2 ? 20 : 40;
			?>
			<tr>
				<td class="column-1">
					<?= ($i+1); ?>.
				</td>
				<td class="column-2">
					Thị phần đạt 20%, tăng 5% sv 2017
				</td>
				<td class="column-3">
					<?= $percent; ?>%
				</td>
			</tr>
			<?php

		}
		?>
		</tbody>
	</table>
</div>


