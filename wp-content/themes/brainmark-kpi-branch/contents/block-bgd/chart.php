<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 31/12/2017
 * Time: 23:20
 */

$bgColor = getColor4Column();

$firstYear = $GLOBALS['firstYear'];
$dataSet = [];
if( is_array($firstYear) ) {
    extract( $firstYear );
    $data = [ (int)$finance, (int)$customer, (int)$operate, (int)$development ];
    $dataSet = [
        'labels' => [
            __('Tài chính', TPL_DOMAIN_LANG),
            __('Khách hàng', TPL_DOMAIN_LANG),
            __('Vận hành', TPL_DOMAIN_LANG),
            __('Phát triển', TPL_DOMAIN_LANG),
        ],
        'datasets' => [[
            'data' => $data,
            'backgroundColor' => array_values( $bgColor ),
        ]]
    ];
}


?>
<div class="content-register-kpi-chart ">
	<div class="register-kpi-chart bgd-register-chart">
        <canvas class="data-report" width="300" height="300" data-type="pie" data-datasets="<?php echo $dataSet ? esc_json_attr($dataSet) : '{}'; ?>"></canvas>
    </div>
</div>
