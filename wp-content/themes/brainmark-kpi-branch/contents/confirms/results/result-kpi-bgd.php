<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/9/18
 * Time: 16:30
 */

require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-users.php';
$uID = 0;
global $wpdb;
if( array_key_exists( 'uid', $_GET ) ){
	$uID = (int)wp_slash( $_GET['uid'] );
}
$getUser = kpi_get_user_by_id($uID);
$user = wp_get_current_user();
$orgchart = user_load_orgchart($user);
$currentOrgID = $orgchart->id;
$year_list = kpi_get_year_list(0, KPI_YEAR_STATUS_PUBLISH);

if( !empty($getUser) && $getUser instanceof \WP_User ):
    $orgchartUser = $getUser->__get('orgchart');
    $memberRoleName = $orgchartUser->name;
    $fullName = $getUser->first_name . ' ' . $getUser->last_name;
    $fullName = !empty( trim( $fullName ) ) ? $fullName : $getUser->display_name;
    $orgChartParent = $orgchartUser->parent;
    $default_avatar = THEME_URL . '/assets/images/no-photo.jpg';
    $avatar = !empty( $getUser->user_url ) ? $getUser->user_url : $default_avatar;
	$year = wp_slash( $_GET['nam'] );

	$totalPercent = 0;
	$totalPercentItem = 0;
	$data = [];
	$member_role = $orgchartUser->role;
	if( empty($member_role) ) {
		$member_role = '';
	} else {
		$member_role = '-' . strtolower($member_role);
	}

    get_template_part('contents/results/kpi', 'target' . '-nhanvien');
    get_template_part('contents/results/kpi', 'dashboard-news' . $member_role);
endif;
    ?>