<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/9/18
 * Time: 16:30
 */
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-formulas.php';
$uID = 0;
global $wpdb;
if( array_key_exists( 'uid', $_GET ) ){
    $uID = (int)wp_slash( $_GET['uid'] );
}
#$year_list = kpi_get_year_list(0, KPI_YEAR_STATUS_PUBLISH);
$year_list = year_get_list_year_ceo();
$getUser = kpi_get_user_by_id($uID);
if( !empty($getUser) && $getUser instanceof \WP_User ):
    $chartName = $getUser->orgchart->name;
    $fullName = $getUser->first_name . ' ' . $getUser->last_name;
    $fullName = !empty( trim( $fullName ) ) ? $fullName : $getUser->display_name;
    $userID = $getUser->ID;
    $orgchartUser = $getUser->__get('orgchart');
    $orgChartParent = $orgchartUser->parent;
    $memberRoleName = $orgchartUser->name;
    $default_avatar = THEME_URL . '/assets/images/no-photo.jpg';
    $avatar = !empty( $getUser->user_url ) ? $getUser->user_url : $default_avatar;
    $member_role = $orgchartUser->role;
    if( empty($member_role) ) {
        $member_role = '';
    } else {
        $member_role = '-' . strtolower($member_role);
    }
    get_template_part('contents/results/kpi', 'target' . '-nhanvien');
    get_template_part('contents/results/kpi', 'dashboard-news' . $member_role);
endif;
    ?>