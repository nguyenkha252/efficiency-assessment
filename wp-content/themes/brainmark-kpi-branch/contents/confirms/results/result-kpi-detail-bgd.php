<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/8/18
 * Time: 17:44
 */

require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-formulas.php';
$add_target = wp_create_nonce('add_target');

$departments = kpi_orgchart_get_list();
$departments = process_children($departments);
$ceo = each($departments);
$ceo = $ceo['value'];
$departments_attrs = [];

#$firstYear = kpi_get_first_year();
$year = $_GET['nam'];

$userID = 0;
global $wpdb;
if( array_key_exists( 'uid', $_GET ) ){
    $userID = (int)$wpdb->escape( $_GET['uid'] );
}

$orgchartUser = [];
$getUser = [];
if( $userID > 0 ) {
    $getUser = kpi_get_user_by_id($userID);
    $orgchartUser = user_load_orgchart($getUser);
    $orgChartParent = $orgchartUser->parent;
    $currentOrgChartID = $orgchartUser->id;
}
if( !empty( $getUser ) && !is_wp_error( $getUser ) ) {
	$firstYearCty = $GLOBALS['firstYear'];

#$listKPI = list_kpi_need_approved_results( $year, $getOrgChart->id, $getUser->ID, TIME_PRECIOUS_VALUE );

#kiểm tra tài khoản này có phải là node end không?
	$nodeEnd            = check_user_is_node_end( $getUser->ID );
	$year_list_behavior = year_get_list_year_behavior();
	$firstYearBehavior  = year_get_first_year_behavior( TIME_YEAR_VALUE );
	if ( empty( $firstYearBehavior ) && ! empty( $year_list_behavior ) ) {
		$firstYearBehavior = $year_list_behavior[0];
	}
	$year_id_behavior = ! empty( $firstYearBehavior ) && ! empty( $firstYearBehavior['id'] ) ? $firstYearBehavior['id'] : 0;
	$year_behavior    = ! empty( $firstYearBehavior ) && ! empty( $firstYearBehavior['year'] ) ? $firstYearBehavior['year'] : 0;
	$arrApply_for     = ! empty( $firstYearBehavior ) && ! empty( $firstYearBehavior['apply_for'] ) ? maybe_unserialize( $firstYearBehavior['apply_for'] ) : [];
	if ( empty( $arrApply_for ) ) {
		$arrApply_for = [];
	}
	if ( ! in_array( $orgchartUser->id, $arrApply_for ) ) {
		$percentBehavior = $firstYearBehavior && ! empty( $firstYearBehavior['behavior_percent'] ) ? $firstYearBehavior['behavior_percent'] : 0;
	} else {
		$percentBehavior = 0;
	}
#if( $nodeEnd ):
	$listKPI   = list_kpi_need_approved_results( $year, $orgchartUser->id, $getUser->ID, TIME_PRECIOUS_VALUE, TIME_MONTH_VALUE );
	/*echo "<pre>";
	print_r( $listKPI );
	echo "</pre>";*/

	$tabs      = [
		'target_work' => [
			'title'      => __( 'Mục tiêu công việc', TPL_DOMAIN_LANG ),
			'type'       => 'target_work',
			'percent'    => 100 - $percentBehavior,
			'img'        => 'tai-chinh.png',
			'time_type'  => 'MONTH',
			'time_value' => TIME_MONTH_VALUE,
		],
		'behavior'    => [
			'title'      => __( 'Thái độ hành vi <i style="font-size:12px">(Behaviour)</i>', TPL_DOMAIN_LANG ),
			'type'       => 'behavior',
			'percent'    => $percentBehavior,
			'img'        => 'customer.png',
			'time_type'  => 'MONTH',
			'time_value' => TIME_MONTH_VALUE,
		],
		/*'comments' => [
			'title' => __('Nhận xét', TPL_DOMAIN_LANG),
			'type' => 'comments',
			'percent' => $firstYear && !empty($firstYear['comments']) ? $firstYear['comments'] : '',
			'img' => 'van-hanh.png',
			'time_type' => 'MONTH',
			'time_value' => TIME_MONTH_VALUE,
			'year_id' => $firstYear
		],*/
	];
	$tabsGroup = [
		'finance'     => [
			'title'      => __( 'Finance', TPL_DOMAIN_LANG ),
			'type'       => 'Finance',
			'percent'    => ! empty( $firstYear ) ? $firstYear['finance'] : '',
			'img'        => 'tai-chinh.png',
			'time_type'  => 'PRECIOUS',
			'time_value' => TIME_PRECIOUS_VALUE,
			'year_id'    => ! empty( $firstYear ) ? $firstYear['id'] : '',
		],
		'customer'    => [
			'title'      => __( 'Customer', TPL_DOMAIN_LANG ),
			'type'       => 'Customer',
			'percent'    => ! empty( $firstYear ) ? $firstYear['customer'] : '',
			'img'        => 'customer.png',
			'time_type'  => 'PRECIOUS',
			'time_value' => TIME_PRECIOUS_VALUE,
			'year_id'    => ! empty( $firstYear ) ? $firstYear['id'] : '',
		],
		'operate'     => [
			'title'      => __( 'Operate', TPL_DOMAIN_LANG ),
			'type'       => 'Operate',
			'percent'    => ! empty( $firstYear ) ? $firstYear['operate'] : '',
			'img'        => 'van-hanh.png',
			'time_type'  => 'PRECIOUS',
			'time_value' => TIME_PRECIOUS_VALUE,
			'year_id'    => ! empty( $firstYear ) ? $firstYear['id'] : '',
		],
		'development' => [
			'title'      => __( 'Development', TPL_DOMAIN_LANG ),
			'type'       => 'Development',
			'percent'    => ! empty( $firstYear ) ? $firstYear['development'] : '',
			'img'        => 'phat-trien.png',
			'time_type'  => 'PRECIOUS',
			'time_value' => TIME_PRECIOUS_VALUE,
			'year_id'    => ! empty( $firstYear ) ? $firstYear['id'] : '',
		]
	];
	$year_list = year_get_list_year_ceo();
	?>

    <div class="clearfix"></div>
    <div class="block-detail block-result-kpi-detail">
        <div id="room-target-kpi" data-content-management="" style="display: block">
            <ul id="" class="list-room-target-kpi clearfix nav nav-pills">
				<?php
				foreach ( $tabs as $tabkey => $tabData ):
					$cls = ( 'target_work' == $tabkey ) ? 'active' : '';
					?>
                    <li class="room-target-kpi-item <?php echo $cls; ?> room-<?php echo $tabkey; ?>">
                        <a href="#room-<?php echo $tabkey; ?>" class="" title="" data-toggle="tab">
                            <img src="<?php echo THEME_URL; ?>/assets/images/<?php echo $tabData['img']; ?>"/>
							<?php echo $tabData['title']; ?>
                        </a>
                    </li>
				<?php endforeach; ?>
            </ul>
			<?php $kpi_for = ''; ?>
            <div class="for-date" style="float:right;">
                <div class="for-year">
                    <label for="select-for-year"><?php echo _LBL_YEAR;// _e( 'Năm', TPL_DOMAIN_LANG ); ?></label>
                    <select class="select-for-year selectpicker" id="select-for-year" name="nam">
						<?php foreach ( $year_list as $item ):
							if ( empty( $kpi_for ) && $item['year'] == TIME_YEAR_VALUE ) {
								$kpi_for = $item['kpi_time'];
							}
							$selected = $item['year'] == TIME_YEAR_VALUE ? 'selected="selected"' : '';
							echo sprintf( '<option value="%s" %s>%s</option>', $item['year'], $selected, $item['year'] );
						endforeach;
						?>
                    </select>
                </div>
				<?php if ( $kpi_for == 'quy' ): ?>
                    <div class="for-quarter">
                        <label for="select-for-quarter">KPI</label>
                        <select class="select-for-quarter selectpicker" id="select-for-quarter" name="">
                            <option value="0"><?php _e( 'Cả năm', TPL_DOMAIN_LANG ); ?></option>
							<?php
							for ( $i = 1; $i <= 4; $i ++ ) {
								$selected = $i == TIME_PRECIOUS_VALUE ? 'selected="selected"' : '';
								echo sprintf( '<option value="%s" %s>Quý %s</option>', $i, $selected, $i );
							}
							?>
                        </select>
                    </div>
				<?php elseif ( $kpi_for == 'thang' ): ?>
                    <div class="for-quarter">
                        <label for="select-for-quarter">KPI</label>
                        <select class="select-for-month select-for-month-nv selectpicker" id="select-for-month" name="">
                            <option value="0"><?php _e( 'Cả năm', TPL_DOMAIN_LANG ); ?></option>
							<?php
							for ( $i = 1; $i <= 12; $i ++ ) {
								$selected = $i == TIME_MONTH_VALUE ? 'selected="selected"' : '';
								echo sprintf( '<option value="%s" %s>Tháng %s</option>', $i, $selected, $i );
							}
							?>
                        </select>
                    </div>
				<?php endif; ?>
            </div>
            <div class="list-detail tab-content clearfix">
				<?php foreach ( $tabs as $tabkey => $tabData ):
					$cls = ( 'target_work' == $tabkey ) ? 'active' : '';

					?>
					<?php if ( $tabkey == 'target_work' ): ?>
                    <input value="<?php echo $add_target; ?>" type="hidden" name="_wpnonce">
                    <input value="<?php echo $tabData['type']; ?>" type="hidden" name="type">
                    <div class="tab-pane <?php echo $cls; ?>" id="room-<?php echo $tabkey; ?>">
                    <div class="list-detail">
                        <div class="top-list-detail">
                            <h3 class="title">
								<?php echo $tabData['title']; ?>
                                <span class="icon-percent"><?php echo $tabData['percent']; ?>%</span>
                            </h3>
                        </div>
                    </div>

                    <form data-update-result=""
                          action="<?php echo admin_url( 'admin-ajax.php?action=personal_update_result' ); ?>" class=""
                          id="form-<?php echo $tabkey; ?>" method="post" enctype="multipart/form-data"
                          onsubmit="return false">
                        <input value="<?php echo wp_create_nonce( 'personal_update_result' ); ?>" type="hidden"
                               name="_wpnonce">
                        <input value="<?php echo $tabData['type']; ?>" type="hidden" name="type">
                        <table class="table-list-detail table-managerment-target-kpi table-result-kpi-personal"
                               cellpadding="0" cellspacing="0">
                            <thead>
                            <tr>
                                <th rowspan="2"
                                    class="column-1 kpi-id align-center"><?php _e( 'Mã KPI', TPL_DOMAIN_LANG ); ?></th>
                                <th rowspan="2"
                                    class="column-2 kpi-content"><?php _e( 'Nội dung mục tiêu', TPL_DOMAIN_LANG ); ?></th>
                                <th rowspan="2"
                                    class="column-3 kpi-company_plan align-center"><?php _e( 'Trọng số', TPL_DOMAIN_LANG ); ?></th>
                                <th rowspan="2"
                                    class="column-4 kpi-department_plan"><?php _e( 'Kế hoạch', TPL_DOMAIN_LANG ); ?> </th>
                                <th rowspan="2"
                                    class="column-5 kpi-unit align-center"><?php _e( 'Thực hiện', TPL_DOMAIN_LANG ); ?></th>
                                <th colspan="2"
                                    class="column-7 align-center"><?php _e( 'Hoàn thành <br> (%)', TPL_DOMAIN_LANG ); ?></th>
                                <th rowspan="2"
                                    class="column-10 align-center"><?php _e( 'Chứng minh', TPL_DOMAIN_LANG ); ?></th>
                                <th rowspan="2"
                                    class="column-11 align-center"><?php _e( 'Tình trạng', TPL_DOMAIN_LANG ); ?></th>
                            </tr>
                            </thead>
                            <tbody>
							<?php
							$groupDatas = [];
							if ( $tabData['type'] == 'target_work' ) {
								foreach ( $tabsGroup as $ktb => $valuetb ) {
									$groupDatas = $listKPI;
								}
							}
							$GroupBy                 = array_group_by( $groupDatas, 'parent' );
							if ( ! empty( $groupDatas ) ):
								$i = 0;
								$titleTempmd5        = '';
								foreach ( $groupDatas as &$item ):

									$plan = $item['plan'];
									$unit = $item['unit'];
									$actual_gain     = $item['actual_gain'];
									$planForYear     = $item['plan_for_year'];
									$percentForMonth = 0;
									$percentForYear  = 0;
									$titlemd5        = md5( $item['post_title'] );

									if( !empty( $item['formulas'] ) ){
										$formulas = maybe_unserialize( $item['formulas'] );
									}else{
										$formulas = [];
									}
									$ttactual_gain = 0;
									if( $unit != KPI_UNIT_THOI_GIAN ) {
									    if( $unit == KPI_UNIT_PERCENT ){
                                            # @TODO Note
                                            # đơn vị là % thì lấy tổng kết quả của các tháng chia cho số tháng nhập KPI
                                            # Các tháng có KPI trong mục đăng ký KPI tháng
                                            # Update feature on 01/10/2018
                                            # Link request: https://trello.com/c/CPoMsHN1/24-imagepng
                                            $total_actual_gain = kpi_unit_percent_sum_kpi_by_parent($item['id']);
                                        }else {
                                            $total_actual_gain = kpi_sum_kpi_by_parent($item['id']);
                                        }
										#print_r( $total_actual_gain );
										if ( ! empty( $total_actual_gain ) && array_key_exists( 'total_actual_gain', $total_actual_gain ) ) {
											$ttactual_gain = $total_actual_gain['total_actual_gain'];
										} else {
											$ttactual_gain = '';
										}
									}else {
										$results = kpi_get_all_kpi_by_parent( $item['id'] );
										if ( ! empty( $results ) ) {
											$arrPercentForTime = [];
											foreach ( $results as $ks => $vs ) {
												$ttplan = $vs['plan'];
												#$ttpercent     = $vs['percent'];
												if ( $vs['unit'] == KPI_UNIT_THOI_GIAN ) {

													if ( ! empty( $vs['actual_gain'] ) ) {
														$arrPercentForDateTime[] = $vs['actual_gain'];
													}
													$arrPercentForTime[] = getPercentForMonth( $vs['unit'], $item['formula_type'], $formulas, $ttplan, $vs['actual_gain'] );
												} else {
													$ttactual_gain += $vs['actual_gain'];
												}
											}
											if ( ! empty( $arrPercentForTime ) ) {
												$ttactual_gain = array_sum( $arrPercentForTime ) / count( $arrPercentForTime );
											}
										} else {
											$ttactual_gain = '';
										}
									}


									if(!empty(TIME_PRECIOUS_VALUE) && !empty( TIME_MONTH_VALUE )) {
										if ( array_key_exists( $item['parent'], $GroupBy ) ) {
											$rowspan = count( $GroupBy[ $item['parent'] ] );
										} else {
											$rowspan = 3;
										}
									}
									else{
									    $rowspan = 1;
									}


									?>
									<?php
									$disabled         = '';
									$name             = 'name="kpis[' . $item['id'] . '][ID]"';
									$valueInput       = $item['id'];
									$nameCheck        = 'name="kpis[' . $item['id'] . '][status]"';
									$textSendApproved = '';
									$showCheckDuyet   = false;
									if ( in_array( $item['status'], [ KPI_STATUS_DONE ] ) ) {
										#$disabled = 'disabled';
										#$name ='';
										#$valueInput = '';
										#$nameCheck = '';
										$textSendApproved = __( 'Đã duyệt', TPL_DOMAIN_LANG );
										$showCheckDuyet = true;
									} elseif ( $item['status'] == KPI_STATUS_WAITING ) {
										$textSendApproved = __( 'Chưa duyệt', TPL_DOMAIN_LANG );
										$showCheckDuyet = true;
									}
									?>
                                    <tr class="group-confirm-results-item">
										<?php #if ( $titleTempmd5 !== $titlemd5 ): ?>
                                            <td rowspan="<?= $rowspan; ?>" class="column-1 kpi-id align-center">
												<?php echo $item['id'];
												$classNode = getColorStar( $item['create_by_node'] );
												if ( $item['required'] == 'yes' ): ?>
                                                    <i class="fa fa-star <?php esc_attr_e( $classNode ); ?>"
                                                       aria-hidden="true"></i>
												<?php endif; ?>
                                            </td>
                                            <td rowspan="<?= $rowspan; ?>"
                                                class="column-2 kpi-content"><?php echo str_replace(str_split("\|"), "", $item['post_title']); ?></td>
                                            <td rowspan="<?= $rowspan; ?>"
                                                class="column-3 kpi-company_plan align-center"><?php echo $item['percent']; ?>
                                                %
                                            </td>
											<?php
											$titleTempmd5 = $titlemd5;
										#endif; ?>
                                        <td class="column-4 kpi-department_plan">
											<?php /*<strong><?php esc_attr_e( $item['month'] ); ?></strong>*/
											?>
											<?php esc_attr_e( $plan ); ?> <?php echo ! empty( $item['unit'] ) ? getUnit( $item['unit'] ) : ''; ?>
                                        </td>
                                        <td class="column-5 kpi-unit align-center">
                                            <p>
												<?php
                                                if(empty(  TIME_PRECIOUS_VALUE)){
                                                    $actual_gain = $ttactual_gain;
                                                }
												if ( ! empty( $actual_gain ) || $actual_gain != ''):
													$formulas        = maybe_unserialize( $item['formulas'] );
													$formula_type    = $item['formula_type'];
													$percentForMonth = getPercentForMonth( $item['unit'], $formula_type, $formulas, $plan, $actual_gain );
												endif;
												?>
                                            </p>
                                            <?php if( !empty( TIME_PRECIOUS_VALUE ) ): ?>
                                            <input type="text" name="resultKPI[<?= $item['id']; ?>][actual_gain]"
                                                   value="<?= esc_attr( $actual_gain ); ?>"/>
											<?php echo ! empty( $item['unit'] ) ? getUnit( $item['unit'] ) : ''; ?>
                                            <input type="hidden" name="resultKPI[<?= $item['id']; ?>][ID]"
                                                   value="<?= $item['id']; ?>"/>
                                            <?php else: ?>
	                                            <?= esc_attr( $actual_gain ); ?>
	                                            <?php echo ! empty( $item['unit'] ) ? getUnit( $item['unit'] ) : ''; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td colspan="2"
                                            class="column-8 kpi-percent align-center"><?php echo round( $percentForMonth, 1 ); ?>
                                            %
                                        </td>
										<?php /*<td class="column-9 align-center"><?php echo $percentForYear; ?></td> */ ?>
                                        <td class="column-10 align-center">
											<?php
											if ( ! empty( $item['files'] ) ) {
												$files = maybe_unserialize( $item['files'] );
												if ( ! empty( $files ) && ! empty( $files['files_url'] ) ) {
													echo sprintf( "<a href='javascript:;' data-action='%s' data-id='%s' class='view-file-upload' data-target='#tpl-personal-view-file' >%s</a>", admin_url( 'admin-ajax.php?action=load_file_upload&id=' . $item['id'] ), $item['id'], __( 'Xem danh sách tập tin', TPL_DOMAIN_LANG ) );
												}
											}
											if( !empty( TIME_PRECIOUS_VALUE ) ):
											?>
                                            <input type="file" name="resultKPI[<?= $item['id']; ?>][files][]"
                                                   id="resultKPI[<?= $item['id']; ?>][files]" class="inputfile"
                                                   accept="image/*, .xls, .xlsx, .doc, .docx, .txt, .zip, .rar"
                                                   data-error-type="<?php _e( 'Vui lòng tải tập tin đúng định dạng (jpg, png, jpeg, doc, docx, xlsx, xls, txt)' ); ?>"
                                                   data-error-size="<?php _e( 'Vui lòng tải tập tin < 5M', TPL_DOMAIN_LANG ); ?>"
                                                   data-multiple-caption="{count} <?= __( 'được chọn', TPL_DOMAIN_LANG ); ?>"
                                                   multiple/>
                                            <label for="resultKPI[<?= $item['id']; ?>][files]"><span><i
                                                            class="fa fa-upload"
                                                            aria-hidden="true"></i> <?php _e( 'Chọn tập tin', TPL_DOMAIN_LANG ); ?></span></label>
                                                <?php endif; ?>
                                        </td>
                                        <td class="column-11 align-center">
									        <?php if( !empty( TIME_PRECIOUS_VALUE ) ): ?>
                                                <?php if ( $showCheckDuyet ): ?>
                                                    <label class="switch">
                                                        <input class="checkbox-status hidden"
                                                               type="checkbox" <?= $nameCheck; ?> <?= $disabled; ?> <?php echo in_array( $item['status'], [ KPI_STATUS_DONE ] ) ? 'checked="checked"' : ''; ?>
                                                               value="1"/>
                                                        <span class="checkbox-slider"></span>
                                                    </label>
                                                    <input type="hidden" <?= $name; ?> value="<?= $valueInput; ?>"/>
                                                <?php endif; ?>
											<?php endif; ?>
                                            <span><?= $textSendApproved; ?></span>
                                        </td>
                                    </tr>
								<?php endforeach;
							endif;
							?>
                            </tbody>
                        </table>
                        <div class="action-bot">
                            <button type="submit" class="btn-brb-default btn-save btn-action-item"
                                    name="btn-save"><i class="fa fa-floppy-o"
                                                       aria-hidden="true"></i> <?php _e( 'Lưu', TPL_DOMAIN_LANG ); ?>
                            </button>
                            <button type="button" data-method="post"
                                    data-action="<?php echo admin_url( 'admin-ajax.php?action=update_approved_results' ); ?>"
                                    class="btn-brb-default btn-save btn-action-item"
                                    name="btn-save-send"><i class="fa fa-floppy-o"
                                                            aria-hidden="true"></i> <?php _e( 'Duyệt kết quả', TPL_DOMAIN_LANG ); ?>
                            </button>
                        </div>
                    </form>
                    <div id="tpl-personal-view-file" class="modal fade" data-keyboard="false" data-backdrop="static"
                         role="dialog">
                        <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
                              action="<?php echo admin_url( 'admin-ajax.php?action=update_upload_file' ) ?>"
                              method="post">
                            <input type="hidden" name="id" value="">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"><?php _e( 'Danh sách tập tin tải lên', TPL_DOMAIN_LANG ); ?></h4>
                                </div>
                                <div class="modal-body clearfix">
                                    <table class="table-list-files table-list-detail">
                                        <thead>
                                        <tr>
                                            <th class="column-1"><?php _e( 'STT', TPL_DOMAIN_LANG ); ?></th>
                                            <th class="column-2"><?php _e( 'Tên tập tin', TPL_DOMAIN_LANG ); ?></th>
                                            <th class="column-3"><?php _e( 'Chọn', TPL_DOMAIN_LANG ); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody class="tbody-main">

                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <button type="submit" class="btn btn-cancel">Xoá</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
				<?php else: ?>
					<?php
					#behavior
					$year_list_behavior = year_get_list_year_behavior();
					$firstYearBehavior  = year_get_first_year_behavior( TIME_YEAR_VALUE );
					if ( empty( $firstYearBehavior ) && ! empty( $year_list_behavior ) ) {
						$firstYearBehavior = $year_list_behavior[0];
					}
					$year_id_behavior = ! empty( $firstYearBehavior ) && ! empty( $firstYearBehavior['id'] ) ? $firstYearBehavior['id'] : 0;
					$year_behavior    = ! empty( $firstYearBehavior ) && ! empty( $firstYearBehavior['year'] ) ? $firstYearBehavior['year'] : 0;
					$arrApply_for     = ! empty( $firstYearBehavior ) && ! empty( $firstYearBehavior['apply_for'] ) ? maybe_unserialize( $firstYearBehavior['apply_for'] ) : [];
					if ( empty( $arrApply_for ) ) {
						$arrApply_for = [];
					}
					$resultBehavior = [];
					if ( ! in_array( $orgchartUser->id, $arrApply_for ) ) {
						$resultBehavior = behavior_get_list_behavior_of_user( $orgchartUser->id, $getUser->ID, $year_id_behavior, TIME_YEAR_VALUE );
						if ( empty( TIME_PRECIOUS_VALUE ) ) {
							$resultBehavior = behavior_get_total_number_for_year( TIME_YEAR_VALUE, $getUser->ID, $orgchartUser->id );
							$resultBehavior = array_group_by( $resultBehavior, 'parent_1' );
						}
						$getBehavior = [];
						if ( empty( $resultBehavior ) ) {
							$getBehavior = behavior_get_list_behavior_of_admin_by_year( TIME_YEAR_VALUE, 0, KPI_STATUS_RESULT );
						}
						$groupBehavior = [];
						if ( ! empty( $resultBehavior ) ) {
							$groupBehavior = $resultBehavior[0];
							unset( $resultBehavior[0] );
						}
						$percentBehavior = $firstYearBehavior && ! empty( $firstYearBehavior['behavior_percent'] ) ? $firstYearBehavior['behavior_percent'] : 0;
					} else {
						$percentBehavior = 0;
					}
					?>
                    <div class="tab-pane" id="room-<?php echo $tabkey; ?>">
                        <form class="frm-add-manager frm-add-kpi-year" id="" method="post"
                              action="<?php echo admin_url( "admin-ajax.php?action=update_result_behavior" ); ?>">
                            <input type="hidden" name="year_id" value="<?= $year_id_behavior; ?>">
                            <input type="hidden" name="year" value="<?= $year_behavior; ?>">
                            <input type="hidden" name="_wp_http_referer"
                                   value="<?php esc_attr_e( wp_unslash( $_SERVER['REQUEST_URI'] ) ); ?>#room-<?= $tabkey; ?>">
                            <input type="hidden" name="_wpnonce"
                                   value="<?php echo wp_create_nonce( "update_result_behavior" ); ?>">
                            <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>

                            <div id="management-behavior" class="section-row">
                                <div class="list-detail">
                                    <div class="top-list-detail top-list-detail-behavior">
                                        <div class="col-md-6">
                                            <h3 class="title">
												<?php _e( 'Thái độ hành vi' ); ?>
                                                <span class="icon-percent" contenteditable="false"
                                                      data-kpi-percent="finance"><?php echo $percentBehavior; ?>%</span>
                                            </h3>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
								<?php
								$urlParam = '';
								if ( empty( $resultBehavior ) && ! empty( $getBehavior ) ) {
									$loadParams = [
										'action'   => 'list_behavior_for_user',
										'year_id'  => $year_id_behavior,
										'precious' => TIME_PRECIOUS_VALUE,
										'month'    => TIME_MONTH_VALUE,
										'year'     => TIME_YEAR_VALUE,
									];
									$urlParam   = add_query_arg( $loadParams, admin_url( "admin-ajax.php" ) );
								}
								?>
                                <table class="table-list-detail table-managerment-behavior" cellpadding="0"
                                       cellspacing="0" data-action="<?= $urlParam; ?>">
                                    <thead>
                                    <tr>
                                        <th class="column-1 align-center"><?php _e( 'STT' ); ?></th>
                                        <th class="column-2"><?php _e( 'Hành vi' ); ?></th>
                                        <th class="column-3 align-center"><?php _e( 'Đánh giá / 1 lần <br> vi phạm' ); ?></th>
                                        <th class="column-4 align-center"><?php _e( 'Số lần <br> vi phạm' ); ?></th>
                                        <th class="column-5 width10 align-center"><?php _e( 'Tỷ lệ <br> bị trừ' ); ?></th>
                                        <th class="column-6 width10 align-center hidden status-behavior">
                                            Trạng thái
                                        </th>
                                    </tr>
                                    </thead>
									<?php
									$checkSendResult   = false;
									$checkColumnStatus = false;
									if ( ! empty( $groupBehavior ) ):
										$htmlGroup = '';
										foreach ( $groupBehavior as $key => $itemGroup ):
											$htmlItem        = '';
											$stt             = 0;
											$getItemBehavior = [];
											if ( array_key_exists( $itemGroup['id'], $resultBehavior ) ) {
												$getItemBehavior = $resultBehavior[ $itemGroup['id'] ];
												unset( $resultBehavior[ $itemGroup['id'] ] );
											}
											foreach ( $getItemBehavior as $k => $item ) {
												$stt ++;
												$number               = empty( TIME_PRECIOUS_VALUE ) ? $item['total_number'] : $item['number'];
												$violation_assessment = $item['violation_assessment'];
												$status               = $item['status'];
												$kqvp                 = $number * $violation_assessment;
												/*if( !empty( TIME_PRECIOUS_VALUE ) && $status == KPI_STATUS_RESULT){
													$inputPercent = "<input class='width60 align-center' type='number' name='behavior[".$item['id']."][number]' value='".esc_attr($item['number'])."' />
										<input type='hidden' name='behavior[".$item['id']."][id]' value='".$item['id']."' />";
													if( !$checkSendResult ) {
														$checkSendResult = true;
													}
												}else{
													$inputPercent = $number;
												}*/
												$inputPercent = $number;
												if ( ! $checkColumnStatus && $status != KPI_STATUS_RESULT ) {
													$checkColumnStatus = true;
												}
												$htmlStatus = 'Chưa duyệt';
												if ( $status == KPI_STATUS_WAITING ) {
													$textStatus = "Chờ duyệt";
													$htmlStatus = "<span class=\"error notification\">{$textStatus}</span>";
												} elseif ( $status == KPI_STATUS_DONE ) {
													$textStatus = "Đã duyệt";
													$htmlStatus = "<span class=\"success notification\"><i class=\"fa fa-check-circle\"></i>{$textStatus}</span>";
												}
												$htmlItem .= "
                                        <tr class='item-'>
                                        <td class=\"column-1 align-center\">{$stt}</td>
                                        <td class=\"column-2\">" . esc_attr( str_replace(str_split("\|"), "", $item['post_title']) ) . "</td>
                                        <td class=\"column-3 align-center\">" . esc_attr( $violation_assessment ) . "%</td>
                                        <td class=\"column-4 align-center\">
                                            {$inputPercent}
                                        </td>
                                        <td class=\"column-5 align-center\">
                                            " . esc_attr( $kqvp ) . "%
                                        </td>
                                        <td class=\"column-6 width10 align-center hidden status-behavior\">$htmlStatus
                                        </td>
                                    </tr>
                                        ";
											}
											$actionDelGroup = "";
											if ( ! empty( $htmlItem ) ) {
												$htmlItem = "<tbody>{$htmlItem}</tbody>";
											}
											$htmlGroup .= "
                                    <thead class='group-behavior-item'>
                                        <tr>
                                            <td class='column-1 align-center'>
                                                <a href='javascript:;' class='group-item-down'><i class=\"fa fa-chevron-circle-down fa-chevron-circle-up\"></i></a>
                                            </td>
                                            <td colspan='4' class='column-2'>".esc_attr(str_replace(str_split("\|"), "", $itemGroup['post_title']))."</td>
                                        </tr>
                                    </thead>
                                    {$htmlItem}
                                    ";
											?>
										<?php endforeach; ?>
										<?php echo $htmlGroup; ?>
									<?php endif; ?>
                                </table>
                            </div>
                        </form>
                    </div>
				<?php endif; ?>
                    </div>
				<?php endforeach; ?>
            </div>
        </div>
    </div>
	<?php #else:
}
    #endif;
?>
<?php
/*
# Không phải node end
#$parentID = !empty($firstYearCty) ? $firstYearCty['id'] : 0;
#$firstYear = kpi_get_first_year( $parentID, KPI_YEAR_STATUS_PUBLISH, $orgchartUser->id, TIME_YEAR_VALUE );
$firstYear = year_get_year_by_chart_id( $orgchartUser->id, TIME_YEAR_VALUE );
$tabs = [
	'finance' => [
		'title' => __('Finance', TPL_DOMAIN_LANG),
		'type' => 'Finance',
		'percent' => $firstYear ? $firstYear['finance'] : '',
		'img' => 'tai-chinh.png',
		'time_type' => 'YEAR',
		'time_value' => TIME_YEAR_VALUE,
		'year_id' => $firstYear
	],
	'customer' => [
		'title' => __('Customer', TPL_DOMAIN_LANG),
		'type' => 'Customer',
		'percent' => $firstYear ? $firstYear['customer'] : '',
		'img' => 'customer.png',
		'time_type' => 'YEAR',
		'time_value' => TIME_YEAR_VALUE,
		'year_id' => $firstYear
	],
	'operate' => [
		'title' => __('Operate', TPL_DOMAIN_LANG),
		'type' => 'Operate',
		'percent' => $firstYear ? $firstYear['operate'] : '',
		'img' => 'van-hanh.png',
		'time_type' => 'YEAR',
		'time_value' => TIME_YEAR_VALUE,
		'year_id' => $firstYear
	],
	'development' => [
		'title' => __('Development', TPL_DOMAIN_LANG),
		'type' => 'Development',
		'percent' => $firstYear ? $firstYear['development'] : '',
		'img' => 'phat-trien.png',
		'time_type' => 'YEAR',
		'time_value' => TIME_YEAR_VALUE,
		'year_id' => $firstYear
	]
];
?>
    <div class="clearfix"></div>
    <div class="block-detail block-result-kpi-detail">
        <div id="room-target-kpi" data-content-management="" style="display: block">
            <ul id="" class="list-room-target-kpi clearfix nav nav-pills">
				<?php
				foreach ($tabs as $tabkey => $tabData):
					$cls = ('finance' == $tabkey) ? 'active' : '';
					?>
                    <li class="room-target-kpi-item <?php echo $cls; ?> room-<?php echo $tabkey; ?>">
                        <a href="#room-<?php echo $tabkey; ?>" class="" title="" data-toggle="tab">
                            <img src="<?php echo THEME_URL; ?>/assets/images/<?php echo $tabData['img']; ?>"/>
							<?php echo $tabData['title']; ?>
                        </a>
                    </li>
				<?php endforeach; ?>
            </ul>
            <div class="list-detail tab-content clearfix">
				<?php foreach ($tabs as $tabkey => $tabData):
					$charts = kpi_get_list_org_charts($orgchartUser->id);
					$cls = ('finance' == $tabkey) ? 'active' : '';
					?>
                    <div class="tab-pane <?php echo $cls; ?>" id="room-<?php echo $tabkey; ?>">
                        <div class="list-detail">
                            <div class="top-list-detail">
                                <h3 class="title">
									<?php echo $tabData['title']; ?>
                                    <a href="javascript:;" data-year="<?php echo esc_json_attr($tabData['year_id']); ?>" data-toggle="modal"
                                       data-target="#tpl-ceo-kpi-year" >
                                        <span class="icon-percent" contenteditable="false" data-kpi-percent="<?php echo $tabkey; ?>"><?php echo $tabData['percent']; ?>%</span>
                                        <i class="glyphicon glyphicon-pencil hidden" aria-hidden="true"></i>
                                    </a>
                                </h3>
                            </div>
                        </div>
                        <form data-update-result="" action="<?php echo admin_url('admin-ajax.php?action=personal_update_result'); ?>" class=""
                              id="form-<?php echo $tabkey; ?>" method="post" enctype="application/x-www-form-urlencoded"
                              onsubmit="return false">
                            <input value="<?php echo $add_target; ?>" type="hidden" name="_wpnonce">
                            <input value="<?php echo $tabData['type']; ?>" type="hidden" name="type">
                            <table class="table-list-detail table-managerment-target-kpi" cellpadding="0" cellspacing="0">
                                <thead>
                                <tr>
                                    <th class="column-1 kpi-id align-center"><?php _e('Mã KPI', TPL_DOMAIN_LANG); ?></th>
                                    <th class="column-2 kpi-content"><?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?></th>
                                    <th class="column-3 kpi-company_plan"><?php _e('Trọng số', TPL_DOMAIN_LANG); ?></th>
                                    <th class="column-4 kpi-unit align-center"><?php _e('Kế hoạch', TPL_DOMAIN_LANG); ?></th>
                                    <th class="column-5 kpi-receive align-center"><?php _e('Thực hiện', TPL_DOMAIN_LANG); ?></th>
                                    <th class="column-6 align-center"><?php _e('Hoàn thành', TPL_DOMAIN_LANG); ?></th>
                                    <th class="column-7 align-center"><?php _e('Chứng minh', TPL_DOMAIN_LANG); ?></th>
                                    <th class="column-8 align-center"><?php _e('Tình trạng', TPL_DOMAIN_LANG); ?></th>
                                </tr>
                                </thead>
                                <tbody>
								<?php
								$groupDatas = get_kpi_by_year_orgchart_for_year( $tabData['type'], TIME_YEAR_VALUE, $userID, 'yes' );
								if (!empty($groupDatas)):
									foreach ($groupDatas as &$item):
										$plan = $item['plan'];
										$actual_gain = $item['actual_gain'];
										$planForYear = $item['plan_for_year'];
										$percentForMonth = 0;
										$percentForYear = 0;
										?>
                                        <tr>
                                            <td class="column-1 kpi-id align-center">
												<?php
												$disabled = '';
												$name = 'name="kpis['.$item['id'].'][ID]"';
												$valueInput = $item['id'];
												$nameCheck = 'name="kpis['.$item['id'].'][status]"';
												$textSendApproved = '';
												if( in_array( $item['status'], [KPI_STATUS_DONE] ) ){
													#$disabled = 'disabled';
													#$name ='';
													#$valueInput = '';
													#$nameCheck = '';
													$textSendApproved = __('Đã duyệt', TPL_DOMAIN_LANG);
												}elseif( $item['status'] == KPI_STATUS_WAITING ){
													$textSendApproved = __('Chờ duyệt', TPL_DOMAIN_LANG);
												}
												?>
                                                <label class="switch">
                                                    <input class="checkbox-status hidden" type="checkbox" <?= $nameCheck; ?> <?= $disabled; ?> <?php echo in_array( $item['status'], [ KPI_STATUS_DONE ] ) ? 'checked="checked"' : ''; ?> value="1"/>
                                                    <span class="checkbox-slider"></span>
                                                </label>
                                                <input type="hidden" <?= $name; ?> value="<?= $valueInput; ?>" />
												<?php echo $item['id']; ?>
												<?php if( $firstYear['id'] == $item['year_id'] ): ?>
                                                    <span class="bgd glyphicon glyphicon-star"></span>
												<?php endif; ?>
                                            </td>
                                            <td class="column-2 kpi-content"><?php echo $item['post_title']; ?></td>
                                            <td class="column-3 kpi-company_plan"><?php echo $item['percent']; ?>%</td>
                                            <td class="column-4 kpi-unit align-center"><?php esc_attr_e( $plan ); ?> <?php esc_attr_e(getUnit($item['unit'])); ?></td>
                                            <td class="column-5 kpi-receive align-center">
												<?php
												if( !empty( $actual_gain ) ):
													$formulas = maybe_unserialize( $item['formulas'] );
													$formula_type = $item['formula_type'];
													$percentForMonth = getPercentForMonth( $item['unit'], $formula_type, $formulas, $plan, $actual_gain );
												endif;
												?>
												<?php
												#if( $item['status'] != KPI_STATUS_DONE ):
												?>
                                                <input type="text" name="resultKPI[<?= $item['id']; ?>][actual_gain]" value="<?= esc_attr($actual_gain); ?>" /> <?php esc_attr_e(getUnit($item['unit'])); ?>
                                                <input type="hidden" name="resultKPI[<?= $item['id']; ?>][ID]" value="<?= $item['id']; ?>" />

                                            </td>
                                            <td class="column-6 kpi-percent align-center"><?php echo round($percentForMonth, 1); ?>%</td>
                                            <td class="column-7 align-center">
												<?php
												if( !empty( $item['files'] ) ){
													$files = maybe_unserialize( $item['files'] );
													if( !empty( $files ) && !empty( $files['files_url'] ) ){
														echo sprintf("<a href='javascript:;' data-action='%s' data-id='%s' class='view-file-upload' data-target='#tpl-personal-view-file-%s' >%s</a>", admin_url('admin-ajax.php?action=load_file_upload&id='.$item['id']), $item['id'], $tabkey, __('Xem danh sách tập tin', TPL_DOMAIN_LANG));
													}
												}
												if( $item['status'] != KPI_STATUS_DONE ):
													?>
                                                    <input type="file" name="resultKPI[<?= $item['id']; ?>][files][]" id="resultKPI[<?= $item['id']; ?>][files]" class="inputfile" accept="image/*, .xls, .xlsx, .doc, .docx, .txt, .zip, .rar" data-error-type="<?php _e('Vui lòng tải tập tin đúng định dạng (jpg, png, jpeg, doc, docx, xlsx, xls, txt)'); ?>" data-error-size="<?php _e('Vui lòng tải tập tin < 5M', TPL_DOMAIN_LANG); ?>" data-multiple-caption="{count} <?= __('được chọn', TPL_DOMAIN_LANG); ?>" multiple />
                                                    <label for="resultKPI[<?= $item['id']; ?>][files]"><span><i class="fa fa-upload" aria-hidden="true"></i> <?php _e('Chọn tập tin', TPL_DOMAIN_LANG); ?></span></label>
												<?php endif; ?>
                                            </td>
                                            <td class="column-8 align-center">

                                                <span><?= $textSendApproved; ?></span>
                                            </td>
                                        </tr>
									<?php endforeach;
								endif;
								?>
                                </tbody>
                            </table>
                            <div class="action-bot">
                                <button type="submit" class="btn-brb-default btn-save-and-public btn-action-item"
                                        name="btn-save-public"><i class="fa fa-floppy-o"
                                                                  aria-hidden="true"></i> <?php _e('Lưu', TPL_DOMAIN_LANG); ?>
                                </button>
                            </div>
                        </form>
                        <div id="tpl-personal-view-file-<?php esc_attr_e($tabkey); ?>" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
                            <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
                                  action="<?php echo admin_url('admin-ajax.php?action=update_upload_file') ?>" method="post">
                                <input type="hidden" name="id" value="">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title"><?php _e('Danh sách tập tin tải lên', TPL_DOMAIN_LANG); ?></h4>
                                    </div>
                                    <div class="modal-body clearfix">
                                        <table class="table-list-files table-list-detail">
                                            <thead>
                                            <tr>
                                                <th class="column-1"><?php _e('STT', TPL_DOMAIN_LANG); ?></th>
                                                <th class="column-2"><?php _e('Tên tập tin', TPL_DOMAIN_LANG); ?></th>
                                                <th class="column-3"><?php _e('Chọn', TPL_DOMAIN_LANG); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody class="tbody-main">

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <button type="submit" class="btn btn-cancel">Xoá</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
				<?php endforeach; ?>
            </div>
        </div>
    </div>
<?php
