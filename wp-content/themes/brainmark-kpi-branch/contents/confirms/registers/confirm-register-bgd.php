<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/01/2018
 * Time: 01:14
 */

require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-orgchart.php';

$userID = 0;
    global $wpdb;
    if( array_key_exists( 'uid', $_GET ) ){
        $userID = (int)$wpdb->escape( $_GET['uid'] );
    }

$user = wp_get_current_user();
$orgchart = user_load_orgchart($user);
$currentOrgID = $orgchart ? $orgchart->id : '';
$member_role = $orgchart ? $orgchart->role : '';


$orgChartParent = 0;
$member_role_user = '';
if( $userID > 0 ) {
    $getUser = kpi_get_user_by_id($userID);
    $orgchartUser = $getUser->__get('orgchart');
    $orgChartParent = $orgchartUser->parent;
    $member_role_user = $orgchartUser->role;
}

echo '<div id="confirm-register-kpi" class="confirm-register-kpi">';
if( !empty($userID) && !empty( $getUser ) && $orgChartParent != 0 ):
    if( ( $member_role_user == 'phongban' && isset($_GET['approvereg']) ) || $member_role_user == 'bgd' ){
	    get_template_part( 'contents/confirms/registers/render-4-column',  $member_role );
    }else {
	    get_template_part( 'contents/confirms/registers/register', "kpi-" . $member_role );
    }
    get_template_part( 'contents/confirms/registers/register-kpi', 'detail-' . $member_role );
else:
    require_once THEME_DIR . '/ajax/get_users_list.php';
    require_once THEME_DIR . '/ajax/get_user_info.php';
    #$users = kpi_get_user_info(['id' => 0, 'single' => false, 'exclude' => []]);
    $chartChild = orgchart_get_all_kpi_by_parent( $currentOrgID );

	$arrCharts = [];
	if( !empty( $chartChild ) ){
		foreach ( $chartChild as $key => $item ){
			$arrCharts[] = $item['id'];
		}
	}
    $arrCharts = implode(", ", $arrCharts);
    $users = user_get_user_by_chart( $arrCharts );
	if( !empty( $users ) ){
		$groupUsers = array_group_by($users, 'room');
	}else{
		$groupUsers = [];
	}

    $output = render_users_by_lv_confirm($groupUsers, 'duyet-dang-ky', true, true);
    ?>
    <div class="lists confirm-register-list-user block-item <?php echo empty($orgChartParent) ? 'approved-kpi' : ''; ?>">
        <h1><?php _e('Danh sách nhân viên', TPL_DOMAIN_LANG); ?></h1>
        <table class="user-list list-staff" cellpadding="0" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="id-col">STT</th>
                    <th>Mã nhân viên</th>
                    <th class="name"><?php _e('Name', TPL_DOMAIN_LANG); ?></th>
                    <th class="position_name"><?php _e('Position Name', TPL_DOMAIN_LANG); ?></th>
                    <th>Phòng ban / Công ty</th>
                    <th>Duyệt đăng ký</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    echo $output;
                ?>
            </tbody>
        </table>
    </div>
<?php
endif;
echo '</div>';