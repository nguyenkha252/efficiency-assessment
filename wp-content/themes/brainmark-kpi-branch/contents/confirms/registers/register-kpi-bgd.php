<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/9/18
 * Time: 16:30
 */

$approvedUserID = 0;
global $wpdb;
if( array_key_exists( 'uid', $_GET ) ){
    $approvedUserID = (int)wp_slash( $_GET['uid'] );
}
#$year_list = kpi_get_year_list(0,KPI_YEAR_STATUS_PUBLISH);
$year_list = year_get_list_year_ceo();
$getUser = kpi_get_user_by_id($approvedUserID);
$user = wp_get_current_user();
$orgchart = user_load_orgchart($user);
$currentOrgID = $orgchart->id;
if( !empty($getUser) && $getUser instanceof \WP_User ):
    $orgchartUser = $getUser->__get('orgchart');
    $userID = $getUser->ID;
    $chartName = $getUser->orgchart->name;
    $fullName = $getUser->first_name . ' ' . $getUser->last_name;
    $fullName = !empty( trim($fullName) ) ? $fullName : $getUser->display_name;
    $orgChartParent = $orgchartUser->parent;
    $memberRoleName = $orgchartUser->name;
    $default_avatar = THEME_URL . '/assets/images/no-photo.jpg';
    $avatar = !empty( $getUser->user_url ) ? $getUser->user_url : $default_avatar;
	$year = wp_slash( $_GET['nam'] );
	$kpiForStaff = get_kpi_by_year_orgchart_not_month_not_precious($year, $userID);
	$data = createDataChartNode( $kpiForStaff );

?>
    <div id="block-register-kpi" class="">
        <div class="header-register-kpi header-block tranform-uppercase">
	        <?php $kpi_for = ''; ?>
            <div class="for-date">
                <div class="for-year" style="float:right;">
                    <label for="select-for-year"><?php _e('Năm', TPL_DOMAIN_LANG); ?></label>
                    <select class="select-for-year selectpicker" id="select-for-year" name="nam">
				        <?php foreach ($year_list as $item):
					        if( empty($kpi_for) && $item['year'] == TIME_YEAR_VALUE ){
						        $kpi_for = $item['kpi_time'];
					        }
					        $selected = $item['year'] == TIME_YEAR_VALUE ? 'selected="selected"' : '';
					        echo sprintf('<option value="%s" %s>%s</option>', $item['year'], $selected, $item['year']);
				        endforeach;
				        ?>
                    </select>
                </div>
		        <?php /*
 <?php if( $kpi_for == 'quy' ): ?>
                    <div class="for-quarter">
                        <label for="select-for-quarter">KPI</label>
                        <select class="select-for-quarter selectpicker" id="select-for-quarter" name="">
                            <option value="0"><?php _e('Cả năm', TPL_DOMAIN_LANG); ?></option>
					        <?php
					        for ( $i = 1; $i <= 4; $i++ ){
						        $selected = $i == TIME_PRECIOUS_VALUE ? 'selected="selected"' : '';
						        echo sprintf('<option value="%s" %s>Quý %s</option>', $i, $selected, $i);
					        }
					        ?>
                        </select>
                    </div>
		        <?php elseif( $kpi_for == 'thang' ): ?>
                    <div class="for-quarter">
                        <label for="select-for-quarter">KPI</label>
                        <select class="select-for-month select-for-month-nv selectpicker" id="select-for-month" name="">
                            <option value="0"><?php _e( 'Cả năm', TPL_DOMAIN_LANG ); ?></option>
					        <?php
					        for ( $i = 1; $i <= 12; $i ++ ) {
						        $selected = $i == TIME_MONTH_VALUE ? 'selected="selected"' : '';
						        echo sprintf( '<option value="%s" %s>Tháng %s</option>', $i, $selected, $i );
					        }
					        ?>
                        </select>
                    </div>
		        <?php endif; ?>
 */ ?>
            </div>
            <div class="name-block">
                <h2>
                    <?php if( TIME_PRECIOUS_VALUE == 0 ): ?>
                    <?php _e("Duyệt đăng ký mục tiêu KPI năm ".TIME_YEAR_VALUE."", TPL_DOMAIN_LANG); ?></h2>
                <?php else: ?>
                    <?php _e("Duyệt đăng ký mục tiêu KPI quý ".TIME_PRECIOUS_VALUE." - ".TIME_YEAR_VALUE."", TPL_DOMAIN_LANG); ?></h2>
                <?php endif; ?>
                </h2>
            </div>
        </div>
        <div class="top-register-kpi">
            <div class="head-member top-title-member">
                <div class="title-usermember">
                    <?php
                    echo sprintf( __('Nhân viên: %s - Chức danh: %s', TPL_DOMAIN_LANG), $fullName, $memberRoleName );
                    ?>
                </div>
            </div>
            <div class="col-md-3 register-kpi-1 level-middle">
                <div class="canvas-item">
                    <canvas width="200" height="200" class="data-report" data-type="pie" data-datasets="<?php echo esc_json_attr($data); ?>"></canvas>
                </div>
            </div>
            <div class="col-md-9 register-kpi-2">
                <table class="table-annotate-list" cellspacing="0" cellpadding="0">
                    <tbody>
                        <?php
                        if( !empty( $kpiForStaff ) ):
                            foreach ( $kpiForStaff as $keyKPI => $itemKPI ):
                                $classNode = getColorStar( $itemKPI['create_by_node'] );
                                $star = !empty( $classNode ) ? "<i class=\"fa fa-star {$classNode}\" aria-hidden=\"true\"></i>" : "";
                                ?>
                                <tr>
                                    <td class="column-1 column-text-center">
                                        <span class="color-annotate " style="background-color: <?php echo getColorColumnNode( $keyKPI ); ?>;"></span>
                                    </td>
                                    <td class="column-2 column-text-center">
                                        <?php echo $star; ?>
                                    </td>
                                    <td class="column-3">
                                        <?php esc_attr_e( str_replace(str_split("\|"), "", $itemKPI['post_title']) ); ?>
                                    </td>
                                    <td class="column-4"><?php esc_attr_e($itemKPI['plan']);?> <?php esc_attr_e( !empty( $itemKPI['unit'] ) ? getUnit( $itemKPI['unit'] ) : '' );  ?></td>
                                    <td class="column-5 column-text-center"><?php esc_attr_e( $itemKPI['percent'] ); ?>%</td>
                                </tr>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php
endif;
    ?>