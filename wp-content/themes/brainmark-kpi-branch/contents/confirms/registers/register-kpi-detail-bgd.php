<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/8/18
 * Time: 17:44
 */

require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/render-kpi-item-row.php';
$add_target = wp_create_nonce('add_target');

$departments = kpi_orgchart_get_list();
$departments = process_children($departments);
$ceo = each($departments);
$ceo = $ceo['value'];
$departments_attrs = [];

#$firstYear = kpi_get_first_year();

$userID = 0;
global $wpdb;
if( array_key_exists( 'uid', $_GET ) ){
    $userID = (int)wp_slash( $_GET['uid'] );
}
$getUser = [];
$orgChartParent = 0;
$orgchartUser = 0;
$currentOrgChartID = 0;
if( $userID > 0 ) {
    $getUser = kpi_get_user_by_id($userID);
    $orgchartUser = user_load_orgchart($getUser);
    $orgChartParent = $orgchartUser->parent;
    $currentOrgChartID = $getUser->orgchart_id;
}
#$firstYearCty = $GLOBALS['firstYear'];
$tabs = [
    'finance' => [
        'title' => __('Finance', TPL_DOMAIN_LANG),
        'type' => 'Finance',
        'percent' => !empty($firstYear) ? $firstYear['finance'] : '',
        'img' => 'tai-chinh.png',
        'time_type' => 'YEAR',
        'time_value' => TIME_YEAR_VALUE,
        'year_id' => !empty($firstYear) ? $firstYear['id'] : '',
    ],
    'customer' => [
        'title' => __('Customer', TPL_DOMAIN_LANG),
        'type' => 'Customer',
        'percent' => !empty($firstYear) ? $firstYear['customer'] : '',
        'img' => 'customer.png',
        'time_type' => 'YEAR',
        'time_value' => TIME_YEAR_VALUE,
        'year_id' => !empty($firstYear) ? $firstYear['id'] : '',
    ],
    'operate' => [
        'title' => __('Operate', TPL_DOMAIN_LANG),
        'type' => 'Operate',
        'percent' => !empty($firstYear) ? $firstYear['operate'] : '',
        'img' => 'van-hanh.png',
        'time_type' => 'YEAR',
        'time_value' => TIME_YEAR_VALUE,
        'year_id' => !empty($firstYear) ? $firstYear['id'] : '',
    ],
    'development' => [
        'title' => __('Development', TPL_DOMAIN_LANG),
        'type' => 'Development',
        'percent' => !empty($firstYear) ? $firstYear['development'] : '',
        'img' => 'phat-trien.png',
        'time_type' => 'YEAR',
        'time_value' => TIME_YEAR_VALUE,
        'year_id' => !empty($firstYear) ? $firstYear['id'] : '',
    ]
];
# echo '<pre>'; var_dump(TIME_TYPE, TIME_VALUE); echo '</pre>';
#kiểm tra tài khoản này có phải là node end không?
$nodeEnd = check_user_is_node_end( $getUser->ID );
if( !isset($_GET['approvereg']) && $orgchartUser->alias == 'phongban' ){
    $nodeEnd = true;
}
?>
<?php
if( $nodeEnd ):
    ?>

    <div class="clearfix"></div>
    <div class="block-detail block-result-kpi-detail">
        <div id="room-target-kpi" data-content-management="">
            <div class="list-detail tab-content clearfix">
                <div class="tab-pane active" id="target-work">
                    <div class="list-detail">
                        <div class="top-list-detail">
                            <h3 class="title">
                                <?php _e('Mục tiêu công việc', TPL_DOMAIN_LANG); ?>
                            </h3>
                            <?php /*<a href="javascript:;" title="" class="check-all" data-check-all=""><i class="fa fa-check" aria-hidden="true"></i> <?php _e('Đồng ý tất cả', TPL_DOMAIN_LANG); ?></a>*/?>
                        </div>
                    </div>
                    <form data-approved="" enctype="application/x-www-form-urlencoded"
                          action="<?php echo admin_url('admin-ajax.php?action=onlevel_approved_personal_target') ?>" method="post" >
                        <table class="table-list-detail table-managerment-target-kpi" cellpadding="0" cellspacing="0">
                            <thead>
                            <tr>
                                <th class="column-1 align-center"><?php _e('Mã KPI', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-2"><?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-3 align-center"><?php _e('Trọng số', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-4"><?php _e('Kế hoạch cá nhân', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-6 align-center"><?php _e('Thời điểm nhận kết quả', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-8 align-center"><?php _e('Triển khai <br> tháng/quý', TPL_DOMAIN_LANG); ?></th>
                                <th class="column-9 align-center"><?php _e('Thao tác', TPL_DOMAIN_LANG); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $tabkey = '-work';
                            $listKPI = kpi_get_list_kpi_by_user(TIME_YEAR_VALUE, $getUser->ID );
                            if( !empty( $listKPI ) ):
                                foreach ( $listKPI as $key => $item ):
	                                $resultCheckApprove = kpi_check_approve_all_by_parent( $item['id'] );
                                    /**
                                     * Describe: check kpi for month or for precious hadn't been approved
                                     * $checkApprove = -1: not exists kpi for month or for precious
                                     * $checkApprove = 0: don't approve all kpi for month or for preicous
                                     * $checkApprove = 1: has been approved all kpi for month or for precious
                                    */
                                    $checkApprove = -1;
                                    if( !empty( $resultCheckApprove ) ){
	                                    $checkApprove = 1;
                                        foreach ( $resultCheckApprove as $approve ){
                                            if( $approve['status'] != KPI_STATUS_RESULT ){
                                                $checkApprove = 0;
                                                break;
                                            }
                                        }
                                    }
                                    $classApprove = '';
                                    if( $checkApprove == 1 ){
                                        $classApprove = 'da-duyet';
                                    }elseif( $checkApprove == 0 ){
	                                    $classApprove = 'chua-duyet-het';
                                    }
                                    ?>
                                    <tr class="<?= $classApprove; ?>">
                                        <td class="column-1 align-center">
                                            <label class="switch">
                                                <input <?= !in_array($item['status'], [KPI_STATUS_DRAFT, KPI_STATUS_PENDING]) ? 'checked="checked"' : ''; ?> class="checkbox-status" name="apply[<?= $item['id']; ?>][status]" id="kpi_bank_status_<?= $item['id']; ?>'" data-id="<?= $item['id']; ?>" value="1" type="checkbox">
                                                <span class="checkbox-slider round"></span>
                                                <input type="hidden" name="apply[<?=$item['id']?>][ID]" value="<?= $item['id']; ?>" />
                                            </label>
                                            <?php echo $item['id']; ?>
                                            <?php
                                            $classNode = getColorStar( $item['create_by_node'] );
                                            if( $item['required'] == 'yes' ): ?>
                                                <i class="fa fa-star <?php esc_attr_e($classNode); ?>" aria-hidden="true"></i>
                                            <?php endif; ?>
                                        </td>
                                        <td class="column-2">
                                            <?php echo str_replace(str_split("\|"), "", $item['post_title']); ?>
                                        </td>
                                        <td class="column-3 align-center">
		                                    <?php echo $item['percent']; ?>%
                                        </td>
                                        <td class="column-4">
                                            <?php echo $item['plan']; ?> <?php echo !empty($item['unit']) ? getUnit($item['unit']) : '' ; ?>
                                        </td>
                                        <td class="column-6 align-center">
                                            <?php echo substr( kpi_format_date( $item['receive'] ), 0, 10) ; ?>
                                        </td>
                                        <td class="column-8 align-center">
	                                        <?php if( in_array( $item['status'], [KPI_STATUS_RESULT] ) ):
		                                        # kiểm tra KPI được tạo theo quý hay theo tháng
		                                        $getYear = kpi_get_year_by_id( $item['year_id'] );
		                                        $checkRegister = kpi_get_year_by_parent_kpi( $item['id'], $item['chart_id'] );
		                                        if( $checkRegister ):
			                                        if( $getYear['kpi_time'] == 'quy' ):
				                                        ?>
                                                        <a href="javascript:;" data-action="load_personal_target_for_precious" data-method="get" data-id="<?= $item['id']; ?>" class="action-edit personal-action-edit" data-edit="" data-toggle="modal" data-target="#tpl-personal-target-precious<?= $tabkey; ?>"><span><?php esc_attr_e('Theo quý'); ?></span></a>
				                                        <?php
			                                        else:
				                                        ?>
                                                        <a href="javascript:;" data-action="load_personal_target_for_month" data-method="get" data-id="<?= $item['id']; ?>" class="action-edit personal-action-edit" data-edit="" data-toggle="modal" data-target="#tpl-personal-target<?= $tabkey; ?>"><span><?php esc_attr_e('Theo tháng'); ?></span></a>
			                                        <?php endif; ?>
	                                            <?php endif; ?>
	                                        <?php endif; ?>
                                        </td>
                                        <td class="column-9 align-center">
                                            <a href="javascript:;" data-action="load_personal_target_register" data-id="<?= $item['id']; ?>" data-ajax="load_personal_target_register" data-toggle="modal" class="action-edit" data-target="#tpl-add-target-<?php echo $tabkey; ?>" data-method="get"><span class="glyphicon glyphicon-edit"></span></a>
                                            <a href="javascript:;" data-action="remove_target_register" data-id="<?= $item['id']; ?>" data-title="<?php _e('Bạn muốn xoá mục tiêu công việc?', TPL_DOMAIN_LANG); ?>" class="action-remove" data-target="#confirm-apply-kpi" data-method="post"><span class="glyphicon glyphicon-remove"></span></a>
                                        </td>
                                    </tr>
                                <?php
                                endforeach;
                            endif;
                            ?>

                            </tbody>
                        </table>
                        <div class="action-bot">
                            <button type="submit" class="btn-brb-default btn-save-and-public btn-action-item" name="btn-save-public" onsubmit="location.href='<?php
                            echo esc_url( PAGE_EXPORTS_URL );
                            ?>'"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php  _e('Lưu & triển khai', TPL_DOMAIN_LANG); ?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="tpl-add-target-work" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
            <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
                  action="<?php echo admin_url('admin-ajax.php?action=add_personal_target') ?>" method="post">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Mục tiêu công việc
                            : Thêm KPI</h4>
                    </div>

                    <div class="modal-body clearfix">
                        <div class="col-md-12">
                            <div class="form-group form-plans">
                                <div class="form-group input-group input-group-select">
                                    <span class="input-group-addon" for="post_title" id="post_title-<?php echo $tabkey; ?>"><?php _e('Chọn KPI', TPL_DOMAIN_LANG); ?></span>
                                    <select data-none-results-text="<?php _e( 'Kết quả tìm kiếm {0}', TPL_DOMAIN_LANG); ?>" data-none-selected-text="<?php _e('Chọn KPI', TPL_DOMAIN_LANG); ?>" data-loading=".form-group.input-group.input-group-select" class="selectpicker cat-bank width657" name="cat" data-live-search="true">
                                        <option value="<?= $orgchartUser->id; ?>"><?=  $orgchartUser->name; ?></option>
                                    </select>
                                </div>
                                <div class="form-group input-group input-group-select">
                                    <span class="input-group-addon" for="post_title" id="post_title-<?php echo $tabkey; ?>"><?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?></span>
                                    <select data-none-results-text="<?php _e( 'Kết quả tìm kiếm {0}', TPL_DOMAIN_LANG); ?>" data-none-selected-text="<?php _e('Chọn mục tiêu', TPL_DOMAIN_LANG); ?>" data-loading=".form-group.input-group.input-group-select" class="selectpicker width657 post_title post_title-<?php echo $tabkey; ?>" name="bank" data-live-search="true">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon" for="plan" id="plan"><?php _e('Kế hoạch cá nhân', TPL_DOMAIN_LANG); ?></span>
                                <input class="plan form-control" type="text" name="plan" placeholder="<?php _e('Kế hoạch cá nhân', TPL_DOMAIN_LANG); ?>" value="" />
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon" for="unit" id="unit"><?php _e('ĐVT', TPL_DOMAIN_LANG); ?></span>
                                <input type="text" name="unit" class="unit form-control" placeholder="<?php _e('ĐVT', TPL_DOMAIN_LANG); ?>" value="" />
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon" for="receive" id="receive"><?php _e('Thời điểm nhận kết quả', TPL_DOMAIN_LANG); ?></span>
                                <input type="text" name="receive" class="form-control"
                                       placeholder="Thời điểm nhận kết quả" data-lang="vi" data-format="<?php echo KPI_FORMAT_DATE; ?>"
                                       data-min-date="01-01-<?= TIME_YEAR_VALUE; ?>"
                                    <?php /* data-max-date="" */ ?>
                                       data-group-date=".group-date" data-timepicker="false"
                                       data-btn-date=".input-group-addon.date-btn"
                                       data-ctrl-date="" aria-describedby="receive">
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon" for="percent" id="percent"><?php _e('Trọng số', TPL_DOMAIN_LANG); ?></span>
                                <input type="text" name="percent" class="percent form-control" placeholder="<?php _e('Trọng số', TPL_DOMAIN_LANG); ?>" value="" />
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="_wpnonce" value="<?php echo $add_target; ?>">
                        <input type="hidden" name="year_id" value="<?php echo !empty($firtKpiForStaff) ? $firtKpiForStaff['year_id'] : 0; ?>">
                        <input type="hidden" name="time_year" value="<?php echo $_GET['nam']; ?>">
                        <input type="hidden" name="id" value="">
                        <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <button type="submit" class="btn btn-primaryy">Lưu</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div id="tpl-personal-target-work" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
        <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
              action="<?php echo admin_url('admin-ajax.php?action=onlevel_register_kpi_for_month') ?>" method="post">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php echo $tabData['title']; ?>
                        Duyệt đăng ký KPI theo tháng của: <?= $fullName; ?></h4>
                </div>
                <div class="modal-body clearfix">
                    <div class="col-md-12">
                        <table class="table-list-detail">
                            <tbody>
                            <tr>
                                <td class="column-1">
									<?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="post_title"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
									<?php _e('Kế hoạch cấp trên', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="plan_on_level"></span>
                                    <span class="unit"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
									<?php _e('Kế hoạch cá nhân', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="plan"></span>
                                    <span class="unit"></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="column-1-2">
                                    <div class="fieldset-month">
                                        <fieldset>
                                            <legend><?php _e( 'Mục tiêu tháng', TPL_DOMAIN_LANG); ?></legend>
                                            <div class="content-fieldset-month">
												<?php for( $month = 1; $month <= 12; $month++ ): ?>
                                                    <div class="form-group input-group">
                                                        <input name="plan_month[<?= $month; ?>][month]" value="<?= $month; ?>" type="hidden" />
                                                        <span class="input-group-addon" for="plan" id="plan-<?php echo $tabkey; ?>-<?= $month; ?>"><?php _e('Tháng ', TPL_DOMAIN_LANG); ?><?= $month; ?></span>
                                                        <input type="text" name="plan_month[<?= $month; ?>][plan]" id="plan-<?php echo $tabkey; ?>-<?= $month; ?>" class="plan-<?php echo $tabkey; ?> form-control confirm-month-form-control" placeholder="<?php _e('Tháng ', TPL_DOMAIN_LANG); ?><?= $month; ?>" value="" />
                                                    </div>
												<?php endfor; ?>
                                            </div>
                                        </fieldset>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
									<?php _e('Thời điểm nhận kết quả', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="receive"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
									<?php _e('Trọng số', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="percent"></span>%
                                </td>
                            </tr>

                            </tbody>
                        </table>


                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" class="_wpnonce" name="_wpnonce" value="">
                    <input type="hidden" class="parent" name="parent" value="">
                    <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <button type="submit" class="btn btn-primaryy">Lưu</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div id="tpl-personal-target-precious-work" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
        <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
              action="<?php echo admin_url('admin-ajax.php?action=onlevel_register_kpi_for_precious') ?>" method="post">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php echo $tabData['title']; ?>
                        Duyệt đăng ký KPI theo quý của: <?= $fullName; ?></h4>
                </div>
                <div class="modal-body clearfix">
                    <div class="col-md-12">
                        <table class="table-list-detail">
                            <tbody>
                            <tr>
                                <td class="column-1">
									<?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="post_title"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
									<?php _e('Kế hoạch cấp trên', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="plan_on_level"></span>
                                    <span class="unit"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
									<?php _e('Kế hoạch cá nhân', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="plan"></span>
                                    <span class="unit"></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="column-1-2">
                                    <div class="fieldset-precious">
                                        <fieldset>
                                            <legend><?php _e( 'Mục tiêu quý', TPL_DOMAIN_LANG); ?></legend>
                                            <div class="content-fieldset-precious">
												<?php
												for( $precious = 1; $precious <= 4; $precious++ ): ?>
                                                    <div class="form-group input-group">
                                                        <input name="plan_precious[<?= $precious; ?>][precious]" value="<?= $precious; ?>" type="hidden" />
                                                        <span class="input-group-addon" for="plan" id="plan-<?php echo $tabkey; ?>-<?= $precious; ?>"><?php _e('Quý ', TPL_DOMAIN_LANG); ?><?= $precious; ?></span>
                                                        <input type="text" name="plan_precious[<?= $precious; ?>][plan]" id="plan-<?php echo $tabkey; ?>-<?= $precious; ?>" class="plan-<?php echo $tabkey; ?> form-control" placeholder="<?php _e('Quý ', TPL_DOMAIN_LANG); ?><?= $precious; ?>" value="" />
                                                    </div>
												<?php endfor; ?>
                                            </div>
                                        </fieldset>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
									<?php _e('Thời điểm nhận kết quả', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="receive"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
									<?php _e('Trọng số', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="percent"></span>%
                                </td>
                            </tr>

                            </tbody>
                        </table>


                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" class="_wpnonce" name="_wpnonce" value="">
                    <input type="hidden" class="parent" name="parent" value="">
                    <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <button type="submit" class="btn btn-primaryy">Lưu</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
<?php else:
# node này không phải là node end
    $firstYearCty = year_get_firt_year_by_congty( TIME_YEAR_VALUE );
    $year_parent = !empty($firstYearCty) ? $firstYearCty['id'] : 0;
	$firstYear = year_get_year_by_chart_id( $orgchartUser->id, TIME_YEAR_VALUE, $year_parent );
	$yearID = !empty( $firstYear ) ? $firstYear['id'] : 0;
    $tabs = [
        'finance' => [
            'title' => __('Finance', TPL_DOMAIN_LANG),
            'type' => 'Finance',
            'percent' => !empty($firstYear) ? $firstYear['finance'] : '',
            'img' => 'tai-chinh.png',
            'time_type' => 'YEAR',
            'time_value' => TIME_YEAR_VALUE,
            'year_id' => !empty($firstYear) ? $firstYear['id'] : '',
        ],
        'customer' => [
            'title' => __('Customer', TPL_DOMAIN_LANG),
            'type' => 'Customer',
            'percent' => !empty($firstYear) ? $firstYear['customer'] : '',
            'img' => 'customer.png',
            'time_type' => 'YEAR',
            'time_value' => TIME_YEAR_VALUE,
            'year_id' => !empty($firstYear) ? $firstYear['id'] : '',
        ],
        'operate' => [
            'title' => __('Operate', TPL_DOMAIN_LANG),
            'type' => 'Operate',
            'percent' => !empty($firstYear) ? $firstYear['operate'] : '',
            'img' => 'van-hanh.png',
            'time_type' => 'YEAR',
            'time_value' => TIME_YEAR_VALUE,
            'year_id' => !empty($firstYear) ? $firstYear['id'] : '',
        ],
        'development' => [
            'title' => __('Development', TPL_DOMAIN_LANG),
            'type' => 'Development',
            'percent' => !empty($firstYear) ? $firstYear['development'] : '',
            'img' => 'phat-trien.png',
            'time_type' => 'YEAR',
            'time_value' => TIME_YEAR_VALUE,
            'year_id' => !empty($firstYear) ? $firstYear['id'] : '',
        ]
    ];
	$members = kpi_get_users(['room_id' => $orgchartUser->id, 'chart_id' => 0, 'trang' => 1, 'limit' => 100, 'tim' => '']);
	$members = $members['items'];
	$actionColumn = $orgchartUser->parent ? 6 : 7;

	if( !is_null($members) ) {
		$actionColumn ++;
	}
    ?>

    <div class="clearfix"></div>
    <div class="block-detail block-result-kpi-detail">
        <div id="room-target-kpi" data-content-management="" style="display: block">
            <ul id="" class="list-room-target-kpi clearfix nav nav-pills">
                <?php
                foreach ($tabs as $tabkey => $tabData):
                    $cls = ('finance' == $tabkey) ? 'active' : '';
                    ?>
                    <li class="room-target-kpi-item <?php echo $cls; ?> room-<?php echo $tabkey; ?>">
                        <a href="#room-<?php echo $tabkey; ?>" class="" title="" data-toggle="tab">
                            <img src="<?php echo THEME_URL; ?>/assets/images/<?php echo $tabData['img']; ?>"/>
                            <?php echo $tabData['title']; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>

            <div class="list-detail tab-content clearfix">
                <?php foreach ($tabs as $tabkey => $tabData):
                    $charts = kpi_get_list_org_charts($orgchartUser->id);
                    $cls = ('finance' == $tabkey) ? 'active' : '';
                    ?>
                    <div class="tab-pane <?php echo $cls; ?>" id="room-<?php echo $tabkey; ?>">
                        <div class="list-detail">
                            <div class="top-list-detail">
                                <h3 class="title">
                                    <?php echo $tabData['title']; ?>
                                    <a href="javascript:;" data-year="<?php echo esc_json_attr($tabData['year_id']); ?>" data-toggle="modal"
                                       data-target="#tpl-ceo-kpi-year" >
                                        <span class="icon-percent" contenteditable="false" data-kpi-percent="<?php echo $tabkey; ?>"><?php echo $tabData['percent']; ?>%</span>
                                        <i class="glyphicon glyphicon-pencil hidden" aria-hidden="true"></i>
                                    </a>
                                </h3>
                            </div>
                        </div>
                        <form data-approved="" action="<?php echo admin_url('admin-ajax.php?action=ceo_approved_kpi'); ?>" class=""
                              id="form-<?php echo $tabkey; ?>" method="post" enctype="application/x-www-form-urlencoded"
                              onsubmit="return false">
                            <input value="<?php echo wp_create_nonce('ceo_approved_kpi'); ?>" type="hidden" name="_wpnonce">
                            <input value="<?php echo $tabData['type']; ?>" type="hidden" name="type">
                            <input type="hidden" name="_wp_http_referer" value="<?php esc_attr_e( wp_unslash( $_SERVER['REQUEST_URI'] ) ); ?>#room-<?= $tabkey; ?>">
                            <table class="table-list-detail table-managerment-target-kpi" cellpadding="0" cellspacing="0">
                                <thead>
                                <tr>
                                    <th class="column-1 kpi-id align-center"><?php _e('Mã KPI', TPL_DOMAIN_LANG); ?></th>
                                    <th class="column-2 kpi-content"><?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?></th>
                                    <th class="column-7 align-center"><?php _e('Trọng số', TPL_DOMAIN_LANG); ?></th>
                                    <th class="column-3 kpi-company_plan"><?php _e('Kế hoạch', TPL_DOMAIN_LANG); ?></th>
                                    <th class="column-5 kpi-receive align-center"><?php _e('Thời điểm nhận kết quả', TPL_DOMAIN_LANG); ?></th>
                                    <th class="column-6 align-center"><?php _e('Trạng thái', TPL_DOMAIN_LANG); ?></th>
                                    <th class="column-8 align-center"><?php _e('Thao Tác', TPL_DOMAIN_LANG); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $totalPercent = 0.0;
                                global $wpdb;
                                #$groupDatas = kpi_get_list($tabData['type'], $tabData['time_type'], $tabData['time_value'], $user->ID, $user->orgchart_id);
                                    if( !isset( $_GET['approvereg'] ) ) {
	                                $groupDatas = kpi_get_list_register_room( $tabData['type'], $yearID, $currentOrgChartID );
                                }else{
	                                $groupDatas = kpi_get_list_register( $tabData['type'], $yearID, $currentOrgChartID );
                                }
                                if (!empty($groupDatas)):
                                    foreach ($groupDatas as &$item):
                                        $totalPercent += doubleval($item['percent']);
                                        $subcharts = kpi_get_list_of_charts($tabData['type'], $tabData['time_type'], $tabData['time_value'], 0, $orgchartUser->id, $item['bank_id']);
	                                    $html = kpi_render_ceo_duyet_row_item($item, $tabData['type'], $orgchartUser, $orgchartUser->parent, $members);
	                                    echo $html;
                                        ?>
                                    <?php endforeach;
                                endif;
                                $addData = [
	                                'id' => '',
	                                'kpi_id' => '',
	                                'year_id' => !empty($firstYear) ? $firstYear['id'] : 0,
	                                // 'bank_id' => '',
	                                'unit' => '',
	                                'actual_gain' => '',
	                                'status_result' => 'draft',
	                                'name' => $orgchartUser ? $orgchartUser->name : '',
	                                'receive' => date('31-12-Y', time()),
	                                'percent' => 0,
	                                'parent' => 0,
	                                'chart_id' => $_GET['cid'],
	                                'user_id' => $getUser->ID,
	                                'aproved' => 0,
	                                'plan' => '',
	                                'post_title' => '',
	                                'type' => $tabData['type'],
	                                'status' => KPI_STATUS_DRAFT,
	                                'required' => 'no',
	                                'owner' => 'no',
	                                'create_by_node' => 'node_middle',
	                                // 'bank_parent' => 0
                                ];
                                $load_params = [
	                                'action' => 'load-parent-bank-list',
	                                'kpi_type' => $tabData['type'],
	                                'cat_slug' => $tabkey,
	                                'year_id' => !empty($firstYear) ? $firstYear['id'] : 0,
	                                'cid' => isset($_GET['cid']) ? $_GET['cid'] : $orgchartUser->id,
	                                'year' => !empty($firstYear) ? $firstYear['year'] : 0,
	                                '_wpnonce' => wp_create_nonce('load-parent-bank-list'),
                                ];
                                ?>
                                <tr class="editer-item hidden" data-tabkey="<?php echo $tabkey; ?>" data-add-info="<?php echo esc_json_attr($addData); ?>">
                                <td class="column-1 kpi-id align-center">
                                    <input type="hidden" name="postdata[id]" value="">
                                    <span class="item-id"></span>
                                </td>
                                <td class="column-2 kpi-content">
                                    <div class="input-group input-group-select">
                                        <label class="input-group-addon" for="cat_name-<?php echo $tabkey; ?>" id="cat_name-<?php echo $tabkey; ?>_lb">Nhóm mục tiêu: </label>
                                        <select data-loading="td.kpi-content"
                                                aria-describedby="cat_name-<?php echo $tabkey; ?>_lb" id="cat_name-<?php echo $tabkey; ?>"
                                                data-live-search="true" data-live-search-placeholder="Lọc Nhóm Mục Tiêu"
                                                data-none-selected-text="Chưa chọn"
                                                data-none-results-text="Không có kết quả cho {0}"
                                                data-select-all-text="Chọn tất cả"
                                                data-deselect-all-text="Bỏ chọn"
                                                class="kpi-category selectpicker input-select" name="cat"><?php

			                                ?>
                                            <option value="<?= $orgchartUser->id; ?>"><?= $orgchartUser->name; ?></option>
                                        </select>
                                    </div>
                                    <div class="input-group input-group-select">
                                        <label class="input-group-addon" for="list-kpis-<?php echo $tabkey; ?>" id="kpi_name-<?php echo $tabkey; ?>">Mục tiêu: </label>
                                        <select aria-describedby="kpi_name-<?php echo $tabkey; ?>" id="list-kpis-<?php echo $tabkey; ?>"
                                                data-live-search="true" data-live-search-placeholder="Chọn Mục Tiêu"
                                                data-none-selected-text="Chưa chọn"
                                                data-none-results-text="Không có kết quả cho {0}"
                                                data-select-all-text="Chọn tất cả"
                                                data-deselect-all-text="Bỏ chọn"
                                                class="kpi-bank-id selectpicker input-select" name="postdata[bank_id]"></select>
                                    </div>
                                    <div class="response-status hidden"></div>
                                </td>
                                <td class="column-3 kpi-percent align-center">
                                    <input class="form-control" placeholder="Tỷ trọng (%)"
                                           name="postdata[percent]" value=""></td>
                                <td class="column-4 kpi-plan">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="postdata[plan]" placeholder="Kế hoạch" value="">
                                        <select name="postdata[unit]" class="selectpicker" data-none-selected-text="Chưa chọn đơn vị">
                                            <option value=""> - Đơn Vị Tính - </option>
			                                <?php
			                                $units = getUnit();
			                                foreach($units as $val => $text):
				                                echo sprintf('<option value="%s">%s</option>', $val, $text);
			                                endforeach;
			                                ?>
                                        </select>
                                    </div>
                                </td>

                                <td class="column-5 kpi-receive">
                                    <div class="input-group">
                                        <input class="form-control" name="postdata[receive]" placeholder="Ngày hết hạn" value=""
                                               data-lang="vi" btn-date=".input-group-addon.date-btn" data-group-date=".input-group"
                                               data-format="<?php echo KPI_FORMAT_DATE; ?>"
                                               data-min-date="01-01-<?php echo $tabData['time_value'][0]; ?>"
                                               data-group-date=".group-date"
                                               data-timepicker="false"
                                               data-btn-date=".input-group-addon.date-btn"
                                               data-ctrl-date>
                                        <span class="input-group-addon date-btn">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </td>
                                <td class="column-6 kpi-aproved align-center">
	                                <?php
	                                $load_params = [
		                                'action' => 'save-department-target',
		                                'kpi_type' => $tabData['type'],
		                                'cat_slug' => $tabkey,
		                                'year_id' => $firstYear ? $firstYear['id'] : 0,
		                                'cid' => $_GET['cid'],
		                                'year' => $firstYear ? $firstYear['year'] : 0,
		                                '_wpnonce' => wp_create_nonce('save-department-target'),
	                                ];
	                                ?>
                                    <a href="javascript:;" data-type="post"
                                       data-url="<?php echo esc_attr( add_query_arg($load_params, admin_url('admin-ajax.php') ) ); ?>"
                                       class="action-save btn btn-sm" title="<?php _e('Lưu', TPL_DOMAIN_LANG);
	                                ?>" data-kpi-type="<?php echo $tabData['type']; ?>">Lưu nháp</a>
                                </td>
                                <td class="column-<?php echo ($actionColumn-1); ?> kpi-members align-center"></td>
                                <td class="column-<?php echo $actionColumn; ?> kpi-action align-center">
                                    <a href="javascript:;" class="action-cancel btn btn-sm">Bỏ qua</a>
                                </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="action-bot">
                                <button type="submit" class="btn-brb-default btn-save-and-public btn-action-item"
                                        name="btn-save-public"><i class="fa fa-floppy-o"
                                                                  aria-hidden="true"></i> <?php _e('Duyệt', TPL_DOMAIN_LANG); ?>
                                </button>
                            </div>
                        </form>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
	<?php if( isset( $_GET['approvereg'] ) ): ?>
    <div id="tpl-personal-target-work" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
        <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
              action="<?php echo admin_url('admin-ajax.php?action=onlevel_register_kpi_for_month') ?>" method="post">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        Duyệt đăng ký KPI theo tháng</h4>
                </div>
                <div class="modal-body clearfix">
                    <div class="col-md-12">
                        <table class="table-list-detail">
                            <tbody>
                            <tr>
                                <td class="column-1">
									<?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="post_title"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
									<?php _e('Kế hoạch cấp trên', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="plan_on_level"></span>
                                    <span class="unit"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
									<?php _e('Kế hoạch cá nhân', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="plan"></span>
                                    <span class="unit"></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="column-1-2">
                                    <div class="fieldset-month">
                                        <fieldset>
                                            <legend><?php _e( 'Mục tiêu tháng', TPL_DOMAIN_LANG); ?></legend>
                                            <div class="content-fieldset-month">
												<?php for( $month = 1; $month <= 12; $month++ ): ?>
                                                    <div class="form-group input-group">
                                                        <input name="plan_month[<?= $month; ?>][month]" value="<?= $month; ?>" type="hidden" />
                                                        <span class="input-group-addon" for="plan" id="plan-<?php echo 'work'; ?>-<?= $month; ?>"><?php _e('Tháng ', TPL_DOMAIN_LANG); ?><?= $month; ?></span>
                                                        <input type="text" name="plan_month[<?= $month; ?>][plan]" id="plan-<?php echo 'work'; ?>-<?= $month; ?>" class="plan-<?php echo 'work'; ?> form-control confirm-month-form-control" placeholder="<?php _e('Tháng ', TPL_DOMAIN_LANG); ?><?= $month; ?>" value="" />
                                                    </div>
												<?php endfor; ?>
                                            </div>
                                        </fieldset>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
									<?php _e('Thời điểm nhận kết quả', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="receive"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
									<?php _e('Trọng số', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="percent"></span>%
                                </td>
                            </tr>

                            </tbody>
                        </table>


                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" class="_wpnonce" name="_wpnonce" value="">
                    <input type="hidden" class="parent" name="parent" value="">
                    <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <button type="submit" class="btn btn-primaryy">Lưu</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div id="tpl-personal-target-precious-work" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
        <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
              action="<?php echo admin_url('admin-ajax.php?action=onlevel_register_kpi_for_precious') ?>" method="post">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        Duyệt đăng ký KPI theo quý</h4>
                </div>
                <div class="modal-body clearfix">
                    <div class="col-md-12">
                        <table class="table-list-detail">
                            <tbody>
                            <tr>
                                <td class="column-1">
									<?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="post_title"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
									<?php _e('Kế hoạch cấp trên', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="plan_on_level"></span>
                                    <span class="unit"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
									<?php _e('Kế hoạch cá nhân', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="plan"></span>
                                    <span class="unit"></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="column-1-2">
                                    <div class="fieldset-precious">
                                        <fieldset>
                                            <legend><?php _e( 'Mục tiêu quý', TPL_DOMAIN_LANG); ?></legend>
                                            <div class="content-fieldset-precious">
												<?php
												for( $precious = 1; $precious <= 4; $precious++ ): ?>
                                                    <div class="form-group input-group">
                                                        <input name="plan_precious[<?= $precious; ?>][precious]" value="<?= $precious; ?>" type="hidden" />
                                                        <span class="input-group-addon" for="plan" id="plan-work-<?= $precious; ?>"><?php _e('Quý ', TPL_DOMAIN_LANG); ?><?= $precious; ?></span>
                                                        <input type="text" name="plan_precious[<?= $precious; ?>][plan]" id="plan-work-<?= $precious; ?>" class="plan-<?php echo 'work'; ?> form-control" placeholder="<?php _e('Quý ', TPL_DOMAIN_LANG); ?><?= $precious; ?>" value="" />
                                                    </div>
												<?php endfor; ?>
                                            </div>
                                        </fieldset>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
									<?php _e('Thời điểm nhận kết quả', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="receive"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
									<?php _e('Trọng số', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="percent"></span>%
                                </td>
                            </tr>

                            </tbody>
                        </table>


                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" class="_wpnonce" name="_wpnonce" value="">
                    <input type="hidden" class="parent" name="parent" value="">
                    <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <button type="submit" class="btn btn-primaryy">Lưu</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <?php endif; ?>
        <?php if( $totalPercent < 100.0 ): ?>
        <div id="tpl-ceo-target-work" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
            <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
                  action="<?php echo admin_url('admin-ajax.php?action=add_ceo_target') ?>" method="post">
                <input type="hidden" name="_wpnonce" value="<?php echo $add_target; ?>">
                <input type="hidden" name="type" value="<?php echo $tabData['type']; ?>">
                <input type="hidden" name="year_id" value="<?php echo $firstYear ? $firstYear['id'] : 0; ?>">
                <input type="hidden" name="time_type" value="<?php echo $tabData['time_type']; ?>">
                <input type="hidden" name="time_value" value="<?php echo $tabData['time_value']; ?>">
                <input type="hidden" name="chart_id" value="<?php echo $orgchartUser->id; ?>">
                <input type="hidden" name="kpiID" value="">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><?php echo $tabData['title']; ?> : <span data-title-add="Thêm KPI" data-title-edit="Chỉnh sửa KPI">Thêm KPI</span></h4>
                    </div>
                    <div class="modal-body clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group input-group input-group-select">
                                <label class="input-group-addon" for="year_id" id="chart_name-<?php echo $tabkey; ?>">Chỉ mục KPI: </label>
                                <select data-loading=".form-group.input-group.input-group-select" data-target="#list-kpis-<?php echo $tabkey; ?>" aria-describedby="chart_name-<?php echo $tabkey; ?>"
                                        class="kpi-category selectpicker input-select" name="cat"><?php
                                    kpiRenderCategoryDropdown('', true, '--- ');
                                    ?></select>
                            </div>
                            <div class="form-group input-group-list">
                                <div class="department-container hidden"></div>
                                <ul id="list-kpis-<?php echo $tabkey; ?>" class="list-group"></ul>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group form-plans">
                                <div class="input-group">
                                    <span class="input-group-addon" id="plan-<?php echo $tabkey; ?>">Nhập kế hoạch</span>
                                    <input type="text" name="plan" class="form-control"
                                           placeholder="Kế hoạch công ty" aria-describedby="plan-<?php echo $tabkey; ?>">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon" id="unit-<?php echo $tabkey; ?>">Đơn vị tính</span>
                                    <input type="text" name="unit" class="form-control"
                                           placeholder="Đơn vị tính" aria-describedby="unit-<?php echo $tabkey; ?>">
                                </div>
                                <div class="input-group group-date">
                                                    <span class="input-group-addon"
                                                          id="receive-<?php echo $tabkey; ?>">Thời điểm nhận kết quả</span>
                                    <input type="text" name="receive" class="form-control"
                                           placeholder="Thời điểm nhận kết quả" data-lang="vi" data-format="<?php echo KPI_FORMAT_DATE; ?>"
                                           data-min-date="01-01-<?php echo $tabData['time_value']; ?>"
                                        <?php /* data-max-date="" */ ?>
                                           data-group-date=".group-date" data-timepicker="false"
                                           data-btn-date=".input-group-addon.date-btn"
                                           data-ctrl-date aria-describedby="receive-<?php echo $tabkey; ?>">
                                    <span class="input-group-addon date-btn">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon" id="percent-<?php echo $tabkey; ?>">Trọng số tối đa</span>
                                    <input type="text" name="percent" class="form-control"
                                           placeholder="Trọng số tối đa" aria-describedby="percent-<?php echo $tabkey; ?>">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                            <div class="form-group phongban-list">
                                <table class="table phongban">
                                    <thead>
                                    <tr>
                                        <th class="chuc-danh" style="width: 50%">Chức Danh</th>
                                        <th class="ke-hoach" style="width: 25%">Kế Hoạch</th>
                                        <th class="trong-so" style="width: 25%">Trọng Số (%)</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($charts as $chart_id => $chart): ?>
                                        <tr>
                                            <td class="chuc-danh chuc-danh-<?php echo $chart['id']; ?>">
                                                <label class="form-control switch chuc_danh" id="chart_<?php echo "{$chart['id']}_id-{$tabkey}"; ?>">
                                                    <input class="checkbox-status" name="chuc_danh<?php echo "[{$chart['id']}][id]"; ?>" value="<?php echo $chart['id']; ?>" type="checkbox">
                                                    <span class="checkbox-slider round"></span>
                                                    <span class="text"><?php echo $chart['name']; ?></span>
                                                </label>
                                            </td>
                                            <td class="ke-hoach ke-hoach-<?php echo $chart['id']; ?>">
                                                <input class="form-control plan" name="chuc_danh<?php echo "[{$chart['id']}][plan]"; ?>" size="10" type="text" value="">
                                            </td>
                                            <td class="trong-so trong-so-<?php echo $chart['id']; ?>">
                                                <input class="form-control percent" name="chuc_danh<?php echo "[{$chart['id']}][percent]"; ?>" size="10" type="text" value="">
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <button type="submit" class="btn btn-primaryy" data-title-add="Thêm" data-title-edit="Lưu">Lưu</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    <?php endif; ?>
<?php endif; ?>