<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 19/03/2018
 * Time: 22:42
 */
global $tabsTitlem, $wpdb, $orgchart, $member_role, $user, $chartRoot, $chartCurrent, $chartParent, $chartParentName, $chartCurrentName;
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';

global $wpdb;
$userID = 0;
if( array_key_exists( 'uid', $_GET ) ){
	$userID = (int)wp_slash( $_GET['uid'] );
}
if( $userID > 0 ) {
	$getUser = get_user_by("ID", $userID);
}else{
	$getUser = wp_get_current_user();
}
$orgchartUser = user_load_orgchart( $getUser );
$orgChartParent = $orgchartUser->parent;
$user_id = 0;
$orgchart_id = $orgchartUser->id;
$member_role = $orgchartUser ? $orgchartUser->role : '';
$roleName = $orgchartUser->name;
if( empty($member_role) ) {
	$member_role = '';
} else {
	$member_role = strtolower($member_role);
}
$time_values = TIME_YEAR_VALUE;
$yearInfo = year_get_year_by_chart_id( $orgchart_id, TIME_YEAR_VALUE );
$year_list = year_get_list_year_ceo();
$firstYearCty = year_get_firt_year_by_congty( TIME_YEAR_VALUE );
$year_parent = !empty($firstYearCty) ? $firstYearCty['id'] : 0;
$firstYear = year_get_year_by_chart_id( $orgchartUser->id, TIME_YEAR_VALUE, $year_parent );
$yearID = !empty( $firstYear ) ? $firstYear['id'] : 0;
$groups = [
	'finance' => [
		'key' => 'finance',
		'type' => 'Finance',
		'title' => __('Finance', TPL_DOMAIN_LANG),
		'percent' => $yearInfo ? $yearInfo['finance'] : '',
		'time' => TIME_YEAR_VALUE,
		'year_id' => $yearInfo,
		'items' => isset( $_GET['room'] ) ? kpi_get_list_if_not_and_get_from_parent('Finance', 'YEAR', $time_values, $user_id, $orgchart_id) : kpi_get_list_if_not_and_get_from_parent_not_room( 'Finance', $yearID, $orgchart_id, $userID ),
	],
	'operate' => [
		'key' => 'operate',
		'type' => 'Operate',
		'title' => __('Operate', TPL_DOMAIN_LANG),
		'percent' => $yearInfo ? $yearInfo['operate'] : '',
		'time' => TIME_YEAR_VALUE,
		'year_id' => $yearInfo,
		'items' => isset( $_GET['room'] ) ?  kpi_get_list_if_not_and_get_from_parent('Operate', 'YEAR', $time_values, $user_id, $orgchart_id) : kpi_get_list_if_not_and_get_from_parent_not_room( 'Operate', $yearID, $orgchart_id, $userID )
	],
	'customer' => [
		'key' => 'customer',
		'type' => 'Customer',
		'title' => __('Customer', TPL_DOMAIN_LANG),
		'percent' => $yearInfo ? $yearInfo['customer'] : '',
		'time' => TIME_YEAR_VALUE,
		'year_id' => $yearInfo,
		'items' => isset( $_GET['room'] ) ? kpi_get_list_if_not_and_get_from_parent('Customer', 'YEAR', $time_values, $user_id, $orgchart_id) : kpi_get_list_if_not_and_get_from_parent_not_room( 'Customer', $yearID, $orgchart_id, $userID )
	],
	'development' => [
		'key' => 'development',
		'type' => 'Development',
		'title' => __('Development', TPL_DOMAIN_LANG),
		'percent' => $yearInfo ? $yearInfo['development'] : '',
		'time' => TIME_YEAR_VALUE,
		'year_id' => $yearInfo,
		'items' => isset( $_GET['room'] ) ? kpi_get_list_if_not_and_get_from_parent('Development', 'YEAR', $time_values, $user_id, $orgchart_id) : kpi_get_list_if_not_and_get_from_parent_not_room( 'Development', $yearID, $orgchart_id, $userID )
	],
];

function render_group_list($item, $yearInfo) {
	?>
	<div class="content-list-register-<?php echo $item['key']; ?> content-list-register">
		<table class="table-block-list-register table-list-<?php echo $item['key']; ?>" cellspacing="0" cellpadding="0">
			<thead>
			<tr>
				<th class="column-1-2" colspan="2"><?php echo $item['title']; ?></th>
				<th class="percent column-3">
					<a href="javascript:;"<?php if(USER_IS_ADMIN): ?> data-action="update" data-year="<?php echo esc_json_attr($item['year_id']); ?>" data-toggle="modal"
						data-target="#tpl-kpi-year"<?php endif; ?>>
						<span class="icon-percent" data-kpi-percent="<?php echo $item['key']; ?>"><?php echo $item['percent']; ?>%</span>
						<i class="glyphicon glyphicon-pencil hidden" aria-hidden="true"></i>
					</a>
					<input type="text" class="txt-percent-kpi hidden" value="<?php echo $item['percent']; ?>" />
				</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$items = $item['items'];
			# for( $i = 0, $n = count($items); $i < $n; $i++ ) {
			$i = 0;
			foreach( $items as $id => $kpi ) {
				$background = !empty( $kpi ) && $kpi['status'] == KPI_STATUS_PENDING ? 'status-pending' :'';
				?>
				<tr data-id="<?php echo $kpi['id']; ?>" class="<?= $background; ?>">
					<td class="column-1"><?= ($i+1); ?>.</td>
					<td class="column-2"><?php echo (str_replace(str_split("\|"), "", $kpi['post_title'])); ?></td>
					<td class="column-3"><?= $kpi['percent']; ?>%</td>
				</tr>
				<?php
				$i++;
			}
			?>
			</tbody>
		</table>
	</div>
	<?php
}

?>
<div id="block-register-kpi" class="">
	<div class="header-register-kpi header-block tranform-uppercase">
		<div class="for-date">
			<div class="for-year" style="float:right;">
				<label for="select-for-year"><?php echo _LBL_YEAR;// _e('Năm', TPL_DOMAIN_LANG); ?></label>
				<select class="select-for-year selectpicker" id="select-for-year" name="nam">
					<?php foreach ($year_list as $item):
						$selected = $item['year'] == TIME_YEAR_VALUE ? 'selected="selected"' : '';
						echo sprintf('<option value="%s" %s>%s</option>', $item['year'], $selected, $item['year']);
					endforeach;
					?>
				</select>
			</div>
		</div>
		<div class="name-block custom-name-block">
			<h2>
				<?php _e("Duyệt đăng ký mục tiêu của <span class='role-name'>{$roleName}</span> KPI năm ".TIME_YEAR_VALUE."", TPL_DOMAIN_LANG); ?>
			</h2>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="top-register-kpi">
	<div class="col-md-4 register-kpi-1">
		<?php get_template_part('contents/block-' . $member_role . '/chart'); ?>
	</div>
	<div class="col-md-8">
		<div class="col-md-6 register-kpi-2"><?php
			render_group_list($groups['finance'], $yearInfo);
			render_group_list($groups['operate'], $yearInfo);
			?></div>
		<div class="col-md-6 register-kpi-3"><?php
			render_group_list($groups['customer'], $yearInfo);
			render_group_list($groups['development'], $yearInfo);
			?></div>

	</div>
</div>
