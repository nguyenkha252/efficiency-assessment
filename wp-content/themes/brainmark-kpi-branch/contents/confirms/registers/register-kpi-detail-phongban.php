<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/8/18
 * Time: 17:44
 */
require_once THEME_DIR . '/inc/lib-orgchart.php';
require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-kpis.php';
$uID = 0;
global $wpdb;
if( array_key_exists( 'uid', $_GET ) ){
    $uID = (int)wp_slash( $_GET['uid'] );
}
$user = wp_get_current_user();
$orgchart = user_load_orgchart($user);
$currentOrgID = $orgchart->id;
$year = $_GET['nam'];
# lấy thông tin người dùng để load dữ liệu KPI cần approved
$getUser = kpi_get_user_by_id($uID);
$userID = $getUser->ID;
$fullName = $getUser->first_name . ' ' . $getUser->last_name;
$fullName = !empty( trim($fullName) ) ? $fullName : $getUser->display_name;
$getOrgChart = user_load_orgchart( $getUser );
$firstYear = kpi_get_first_year(0 ,KPI_YEAR_STATUS_PUBLISH);
$listKPI = list_kpi_need_approved( $year, $getOrgChart->id, $getUser->ID );

#kiểm tra tài khoản này có phải là node end không?
$nodeEnd = check_user_is_node_end( $getUser->ID );
if( !isset($_GET['approvereg']) && $getOrgChart->alias == 'phongban' ){
    $nodeEnd = true;
}
    if( $nodeEnd ):
        $tabkey = "target_work";

?>

<div class="clearfix"></div>
<div class="block-detail block-result-kpi-detail">
    <div id="room-target-kpi" data-content-management="">
        <div class="list-detail tab-content clearfix">
            <div class="tab-pane active" id="target-work">
                <div class="list-detail">
                    <div class="top-list-detail">
                        <h3 class="title">
                            <?php _e('Mục tiêu công việc', TPL_DOMAIN_LANG); ?>
                        </h3>
                       <?php /*
                        <a href="javascript:;" title="" class="check-all" data-check-all=""><i class="fa fa-check" aria-hidden="true"></i> <?php _e('Đồng ý tất cả', TPL_DOMAIN_LANG); ?></a>*/?>
                    </div>
                </div>
                <form data-approved="" enctype="application/x-www-form-urlencoded"
                      action="<?php echo admin_url('admin-ajax.php?action=onlevel_approved_personal_target') ?>" method="post" >
                    <table class="table-list-detail table-managerment-target-kpi" cellpadding="0" cellspacing="0">
                        <thead>
                        <tr>
                            <th class="column-1 align-center"><?php _e('Mã KPI', TPL_DOMAIN_LANG); ?></th>
                            <th class="column-2"><?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?></th>
                            <th class="column-4"><?php _e('Kế hoạch cá nhân', TPL_DOMAIN_LANG); ?></th>
                            <th class="column-6 align-center"><?php _e('Thời điểm nhận kết quả', TPL_DOMAIN_LANG); ?></th>
                            <th class="column-7 align-center"><?php _e('Trọng số', TPL_DOMAIN_LANG); ?></th>
                            <th class="column-8 align-center"><?php _e('Triển khai <br> tháng/quý', TPL_DOMAIN_LANG); ?></th>
                            <th class="column-9 align-center"><?php _e('Thao tác', TPL_DOMAIN_LANG); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if( !empty( $listKPI ) ):
                            foreach ( $listKPI as $key => $item ):
	                            $unit = !empty($item['unit']) ? getUnit($item['unit']) : '';
	                            $post = false;
                                $textSendApproved = '';
                                $showCheckDuyet = true;
	                            if( $item['required'] == 'no' ) {
		                            if ( in_array( $item['status'], [
			                            KPI_STATUS_RESULT,
			                            KPI_STATUS_WAITING,
			                            KPI_STATUS_DONE
		                            ] ) ) {
			                            $textSendApproved = __( 'Đã duyệt', TPL_DOMAIN_LANG );
		                            } elseif ( $item['status'] == KPI_STATUS_PENDING ) {
			                            $textSendApproved = "<span class='notification error'>" . __( 'Chờ duyệt', TPL_DOMAIN_LANG ) . "</span>";
		                            }
		                            if ( $item['status'] == KPI_STATUS_PENDING ) {
			                            $post = get_post( $item['bank_id'] );
			                            if ( ! empty( $post ) && $post->post_status == 'draft' ) {
				                            #$textSendApproved = "<span class='notification warning'>Chờ duyệt mục tiêu <br> trong ngân hàng KPI</span>";
				                            $showCheckDuyet = false;
			                            }
		                            }
	                            }else {
		                            $showCheckDuyet = false;
	                            }
		                            ?>

                                <tr>
                                    <td class="column-1 align-center">
                                        <?php if( $showCheckDuyet ){
	                                        ?>
                                            <label class="switch">
                                                <input <?= !in_array($item['status'], [KPI_STATUS_DRAFT, KPI_STATUS_PENDING]) ? 'checked="checked"' : ''; ?> class="checkbox-status" name="apply[<?= $item['id']; ?>][status]" id="kpi_bank_status_<?= $item['id']; ?>'" data-id="<?= $item['id']; ?>" value="1" type="checkbox">
                                                <span class="checkbox-slider round"></span>
                                                <input type="hidden" name="apply[<?=$item['id']?>][ID]" value="<?= $item['id']; ?>" />
                                            </label>
                                        <?php } ?>
                                        <?php echo $item['id'];
                                        $classNode = getColorStar( $item['create_by_node'] );
                                        if( $item['required'] == 'yes' ): ?>
                                            <i class="fa fa-star <?php esc_attr_e($classNode); ?>" aria-hidden="true"></i>
                                        <?php
                                        else:
	                                        if ( ! empty( $post ) && $post->post_status == 'draft' ) {
                                            ?>
                                                <i class="fa fa-star color-black" aria-hidden="true"></i>
                                                <?php
                                            }
                                        ?>

                                        <?php endif; ?>

                                    </td>
                                    <td class="column-2">
                                        <?php
                                        /*$amountApproved = count_kpi_by_year_and_user( $item['year_id'], $item['id'], $userID, KPI_STATUS_PENDING );
                                        if( !empty( $amountApproved ) ){
                                            $amountPending = $amountApproved['amount_status'];
                                        }
                                        $htmlAmountPending = "";
                                        if( !empty( $amountPending ) ){
                                            $htmlAmountPending = "<span class='amount-pending'>{$amountPending}</span>";
                                        }*/
                                        ?>

                                        <span><?php esc_attr_e($item['post_title']); ?></span>
                                    </td>
                                    <td class="column-4">
                                        <?php echo $item['plan']; ?> <?= $unit; ?>
                                    </td>
                                    <td class="column-6 align-center">
                                        <?php echo substr( kpi_format_date( $item['receive'] ), 0, 10) ; ?>
                                    </td>
                                    <td class="column-7 align-center">
                                        <?php echo $item['percent']; ?>%
                                    </td>
                                    <td class="column-8 align-center">
	                            <?php if( in_array( $item['status'], [KPI_STATUS_RESULT] ) ):
	                            # kiểm tra KPI được tạo theo quý hay theo tháng
                                    $getYear = kpi_get_year_by_id( $item['year_id'] );
                                    $checkRegister = kpi_get_year_by_parent_kpi( $item['id'], $item['chart_id'] );
                                    if( !empty( $checkRegister ) ):
                                        if( $getYear['kpi_time'] == 'quy' ):
                                            ?>
                                        <a href="javascript:;" data-action="load_personal_target_for_precious" data-method="get" data-id="<?= $item['id']; ?>" class="action-edit personal-action-edit" data-edit="" data-toggle="modal" data-target="#tpl-personal-target-precious-<?= $tabkey; ?>"><span><?php esc_attr_e('Theo quý'); ?></span></a>
                                            <?php
                                        else:
                                            ?>
                                        <a href="javascript:;" data-action="load_personal_target_for_month" data-method="get" data-id="<?= $item['id']; ?>" class="action-edit personal-action-edit" data-edit="" data-toggle="modal" data-target="#tpl-personal-target-<?= $tabkey; ?>"><span><?php esc_attr_e('Theo tháng'); ?></span></a>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                                    </td>
                                    <td class="column-9 align-center">
                                        <?php if( $item['required'] == 'no' ): ?>
                                            <a href="javascript:;" data-action="load_personal_target_register" data-id="<?= $item['id']; ?>" data-ajax="load_personal_target_register" data-toggle="modal" class="action-edit" data-target="#tpl-add-target-<?php echo $tabkey; ?>" data-method="get"><span class="glyphicon glyphicon-edit"></span></a>
                                            <a href="javascript:;" data-action="remove_target_register" data-id="<?= $item['id']; ?>" data-title="<?php _e('Bạn muốn xoá mục tiêu công việc?', TPL_DOMAIN_LANG); ?>" class="action-remove" data-target="#confirm-apply-kpi" data-method="post"><span class="glyphicon glyphicon-remove"></span></a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php
                            endforeach;
                        endif;
                        ?>

                        </tbody>
                    </table>
                    <div class="action-bot">
                        <button type="submit" class="btn-brb-default btn-save-and-public btn-action-item" name="btn-save-public" onsubmit="location.href='<?php
                        echo esc_url( PAGE_EXPORTS_URL );
                        ?>'"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php  _e('Lưu & triển khai', TPL_DOMAIN_LANG); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="tpl-personal-target-<?php echo $tabkey; ?>" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
        <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
              action="<?php echo admin_url('admin-ajax.php?action=onlevel_register_kpi_for_month') ?>" method="post">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php echo $tabData['title']; ?>
                        Duyệt đăng ký KPI theo tháng của: <?= $fullName; ?></h4>
                </div>
                <div class="modal-body clearfix">
                    <div class="col-md-12">
                        <table class="table-list-detail">
                            <tbody>
                            <tr>
                                <td class="column-1">
                                    <?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="post_title"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
                                    <?php _e('Kế hoạch cấp trên', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="plan_on_level"></span>
                                    <span class="unit"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
                                    <?php _e('Kế hoạch cá nhân', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="plan"></span>
                                    <span class="unit"></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="column-1-2">
                                    <div class="fieldset-month">
                                        <fieldset>
                                            <legend><?php _e( 'Mục tiêu tháng', TPL_DOMAIN_LANG); ?></legend>
                                            <div class="content-fieldset-month">
	                                            <?php
	                                            $outputLeft = "";
	                                            $outputRight = "";
	                                            for( $month = 1; $month <= 12; $month++ ): ?>
		                                            <?php
		                                            $output = "<div class=\"item-form-group-for owner-register-for\">
                                                                    <div class=\"form-group input-group left-target-for\">
                                                                        <input name=\"plan_month[{$month}][month]\" value=\"{$month}\" type=\"hidden\" />
                                                                        <span class=\"input-group-addon\" for=\"plan\" id=\"plan-{$tabkey}-{$month}\">".__('Tháng ', TPL_DOMAIN_LANG)."{$month}</span>
                                                                        <input type=\"text\" name=\"plan_month[{$month}][plan]\" id=\"plan-{$tabkey}-{$month}\" class=\"plan-{$tabkey} form-control\" placeholder=\"". __('Kế hoạch', TPL_DOMAIN_LANG)."\" value=\"\" />
                                                                    </div>
                                                                    <div class=\"form-group input-group right-target-percent\">
                                                                        <span class=\"input-group-addon\" for=\"plan\" id=\"plan-{$tabkey}-{$month}\">".__('Trọng số ', TPL_DOMAIN_LANG)."</span>
                                                                        <input type=\"text\" name=\"plan_month[{$month}][percent]\" id=\"plan-{$tabkey}-{$month}\" class=\"plan-{$tabkey} form-control\" placeholder=\"". __('Trọng số ', TPL_DOMAIN_LANG)."\" value=\"\" />
                                                                    </div>
                                                                </div>";
		                                            if( $month <= 6 ){
			                                            $outputLeft .= $output;
		                                            }else{
			                                            $outputRight .= $output;
		                                            }
		                                            ?>
	                                            <?php endfor; ?>
                                                <table class="table-field-list" border="0">
                                                    <tr>
                                                        <td class="column-left" valign="top"><?php echo $outputLeft; ?></td>
                                                        <td class="column-right" valign="top"><?php echo $outputRight; ?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </fieldset>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
                                    <?php _e('Thời điểm nhận kết quả', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="receive"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
                                    <?php _e('Trọng số', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="percent"></span>%
                                </td>
                            </tr>

                            </tbody>
                        </table>


                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" class="_wpnonce" name="_wpnonce" value="">
                    <input type="hidden" class="parent" name="parent" value="">
                    <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <button type="submit" class="btn btn-primaryy">Lưu</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div id="tpl-personal-target-precious-<?php echo $tabkey; ?>" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
        <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
              action="<?php echo admin_url('admin-ajax.php?action=onlevel_register_kpi_for_precious') ?>" method="post">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php echo $tabData['title']; ?>
                        Duyệt đăng ký KPI theo quý của: <?= $fullName; ?></h4>
                </div>
                <div class="modal-body clearfix">
                    <div class="col-md-12">
                        <table class="table-list-detail">
                            <tbody>
                            <tr>
                                <td class="column-1">
                                    <?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="post_title"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
                                    <?php _e('Kế hoạch cấp trên', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="plan_on_level"></span>
                                    <span class="unit"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
                                    <?php _e('Kế hoạch cá nhân', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="plan"></span>
                                    <span class="unit"></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="column-1-2">
                                    <div class="fieldset-precious">
                                        <fieldset>
                                            <legend><?php _e( 'Mục tiêu quý', TPL_DOMAIN_LANG); ?></legend>
                                            <div class="content-fieldset-precious">
	                                            <?php
	                                            $outputLeft = "";
	                                            $outputRight = "";
	                                            for( $precious = 1; $precious <= 4; $precious++ ): ?>
		                                            <?php
		                                            $output = "<div class=\"item-form-group-for owner-register-for\">
                                                                    <div class=\"form-group input-group left-target-for\">
                                                                        <input name=\"plan_precious[<?= $precious; ?>][precious]\" value=\"{$precious}\" type=\"hidden\" />
                                                                        <span class=\"input-group-addon\" for=\"plan\" id=\"plan-{$tabkey}-{$precious}\">".__('Kế hoạch', TPL_DOMAIN_LANG)."</span>
                                                                        <input type=\"text\" name=\"plan_precious[{$precious}][plan]\" id=\"plan-{$tabkey}-{$precious}\" class=\"plan-{$tabkey} form-control\" placeholder=\"".__('Kế hoạch', TPL_DOMAIN_LANG)."\" value=\"\" />
                                                                    </div>
                                                                    <div class=\"form-group input-group right-target-percent\">
                                                                        <span class=\"input-group-addon\" for=\"plan\" id=\"plan-{$tabkey}-{$precious}\">".__('Trọng số ', TPL_DOMAIN_LANG)."</span>
                                                                        <input type=\"text\" name=\"plan_precious[{$precious}][percent]\" id=\"plan-{$tabkey}-{$precious}\" class=\"plan-{$tabkey} form-control\" placeholder=\"".__('Trọng số ', TPL_DOMAIN_LANG)."\" value=\"\" />
                                                                    </div>
                                                                </div>";
		                                            if( $precious <= 2 ){
			                                            $outputLeft .= $output;
		                                            }else{
			                                            $outputRight .= $output;
		                                            }
		                                            ?>
	                                            <?php endfor; ?>
                                                <table class="table-field-list" border="0">
                                                    <tr>
                                                        <td class="column-left" valign="top"><?php echo $outputLeft; ?></td>
                                                        <td class="column-right" valign="top"><?php echo $outputRight; ?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </fieldset>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
                                    <?php _e('Thời điểm nhận kết quả', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="receive"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="column-1">
                                    <?php _e('Trọng số', TPL_DOMAIN_LANG); ?>
                                </td>
                                <td class="column-2">
                                    <span class="percent"></span>%
                                </td>
                            </tr>

                            </tbody>
                        </table>


                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" class="_wpnonce" name="_wpnonce" value="">
                    <input type="hidden" class="parent" name="parent" value="">
                    <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <button type="submit" class="btn btn-primaryy">Lưu</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div id="tpl-add-target-<?php echo $tabkey; ?>" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
        <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
              action="<?php echo admin_url('admin-ajax.php?action=add_personal_target') ?>" method="post">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php echo $tabData['title']; ?>Chỉnh sửa KPI</h4>
                </div>

                <div class="modal-body clearfix">
                    <div class="col-md-12">
                        <div class="form-group form-plans">
                            <div class="form-group input-group input-group-select">
                                <span class="input-group-addon" for="post_title" id="post_title-<?php echo $tabkey; ?>"><?php _e('Chọn KPI', TPL_DOMAIN_LANG); ?></span>
                                <select data-none-results-text="<?php _e( 'Kết quả tìm kiếm {0}', TPL_DOMAIN_LANG); ?>" data-none-selected-text="<?php _e('Chọn KPI', TPL_DOMAIN_LANG); ?>" data-loading=".form-group.input-group.input-group-select" class="selectpicker cat-bank width657" name="cat" data-live-search="true">
                                    <option value="<?= $getOrgChart->id; ?>"><?= $getOrgChart->name; ?></option>
                                </select>
                            </div>
                            <div class="form-group input-group input-group-select">
                                <span class="input-group-addon" for="post_title" id="post_title-<?php echo $tabkey; ?>"><?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?></span>
                                <select data-none-results-text="<?php _e( 'Kết quả tìm kiếm {0}', TPL_DOMAIN_LANG); ?>" data-none-selected-text="<?php _e('Chọn mục tiêu', TPL_DOMAIN_LANG); ?>" data-loading=".form-group.input-group.input-group-select" class="selectpicker width657 post_title post_title-<?php echo $tabkey; ?>" name="bank" data-live-search="true">
                                </select>
                            </div>
                        </div>
                        <div class="form-group input-group">
                            <span class="input-group-addon" for="plan" id="plan"><?php _e('Kế hoạch cá nhân', TPL_DOMAIN_LANG); ?></span>
                            <input class="plan form-control" type="text" name="plan" placeholder="<?php _e('Kế hoạch cá nhân', TPL_DOMAIN_LANG); ?>" value="" />
                        </div>
                        <div class="form-group input-group">
                            <span class="input-group-addon" for="unit" id="unit"><?php _e('ĐVT', TPL_DOMAIN_LANG); ?></span>
                            <select data-none-selected-text="Đơn vị tính" id="unit-<?php echo $tabkey; ?>" class="select-unit selectpicker width657" name="unit">
			                    <?php
			                    $unitArr = getUnit();
			                    foreach( $unitArr as $kUnit => $vUnit ) {
				                    echo sprintf( "<option value='%s'>%s</option>", $kUnit, $vUnit );
			                    }
			                    ?>
                            </select>
                        </div>
                        <div class="form-group input-group">
                            <span class="input-group-addon" for="receive" id="receive"><?php _e('Thời điểm nhận kết quả', TPL_DOMAIN_LANG); ?></span>
                            <input type="text" name="receive" class="form-control"
                                   placeholder="Thời điểm nhận kết quả" data-lang="vi" data-format="<?php echo KPI_FORMAT_DATE; ?>"
                                   data-min-date="01-01-<?= TIME_YEAR_VALUE; ?>"
                                   data-group-date=".group-date" data-timepicker="false"
                                   data-btn-date=".input-group-addon.date-btn"
                                   data-ctrl-date="" aria-describedby="receive">
                        </div>
                        <div class="form-group input-group">
                            <span class="input-group-addon" for="percent" id="percent"><?php _e('Trọng số', TPL_DOMAIN_LANG); ?></span>
                            <input type="text" name="percent" class="percent form-control" placeholder="<?php _e('Trọng số', TPL_DOMAIN_LANG); ?>" value="" />
                        </div>
                        <div class="form-group form-plans">
                            <div class="form-group input-group input-group-select">
                                <span class="input-group-addon" for="kpi-time" id="kpi-time-<?php echo $tabkey; ?>"><?php _e('KPI theo', TPL_DOMAIN_LANG); ?></span>
                                <select data-none-results-text="<?php _e( 'Kết quả tìm kiếm {0}', TPL_DOMAIN_LANG); ?>" data-none-selected-text="<?php _e('KPI Theo', TPL_DOMAIN_LANG); ?>" data-loading=".form-group.input-group.input-group-select" class="selectpicker kpi-for kpi-for-<?php echo $tabkey; ?>" name="kpi_time" data-live-search="true">
                                    <option value="quy">Quý</option>
                                    <option value="thang">Tháng</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" name="_wpnonce" value="<?php echo $add_target; ?>">
                    <input type="hidden" name="year_id" value="<?php echo $firtKpiForStaff['year_id'] ? $firtKpiForStaff['year_id'] : 0; ?>">
                    <input type="hidden" name="time_year" value="<?php echo $_GET['nam']; ?>">
                    <input type="hidden" name="id" value="">
                    <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <button type="submit" class="btn btn-primaryy">Lưu</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<?php else:
# node này không phải là node end
        $tabs = [
            'finance' => [
                'title' => __('Finance', TPL_DOMAIN_LANG),
                'type' => 'Finance',
                'percent' => $firstYear ? $firstYear['finance'] : '',
                'img' => 'tai-chinh.png',
                'time_type' => 'YEAR',
                'time_value' => TIME_YEAR_VALUE,
                'year_id' => $firstYear
            ],
            'customer' => [
                'title' => __('Customer', TPL_DOMAIN_LANG),
                'type' => 'Customer',
                'percent' => $firstYear ? $firstYear['customer'] : '',
                'img' => 'customer.png',
                'time_type' => 'YEAR',
                'time_value' => TIME_YEAR_VALUE,
                'year_id' => $firstYear
            ],
            'operate' => [
                'title' => __('Operate', TPL_DOMAIN_LANG),
                'type' => 'Operate',
                'percent' => $firstYear ? $firstYear['operate'] : '',
                'img' => 'van-hanh.png',
                'time_type' => 'YEAR',
                'time_value' => TIME_YEAR_VALUE,
                'year_id' => $firstYear
            ],
            'development' => [
                'title' => __('Development', TPL_DOMAIN_LANG),
                'type' => 'Development',
                'percent' => $firstYear ? $firstYear['development'] : '',
                'img' => 'phat-trien.png',
                'time_type' => 'YEAR',
                'time_value' => TIME_YEAR_VALUE,
                'year_id' => $firstYear
            ]
        ];
        ?>

        <div class="clearfix"></div>
        <div class="block-detail block-result-kpi-detail">
            <div id="room-target-kpi" data-content-management="" style="display: block">
                <ul id="" class="list-room-target-kpi clearfix nav nav-pills">
                    <?php
                    foreach ($tabs as $tabkey => $tabData):
                        $cls = ('finance' == $tabkey) ? 'active' : '';
                        ?>
                        <li class="room-target-kpi-item <?php echo $cls; ?> room-<?php echo $tabkey; ?>">
                            <a href="#room-<?php echo $tabkey; ?>" class="" title="" data-toggle="tab">
                                <img src="<?php echo THEME_URL; ?>/assets/images/<?php echo $tabData['img']; ?>"/>
                                <?php echo $tabData['title']; ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <div class="list-detail tab-content clearfix">
                    <?php foreach ($tabs as $tabkey => $tabData):
                        $charts = kpi_get_list_org_charts($user->orgchart_id);
                        $cls = ('finance' == $tabkey) ? 'active' : '';
                        ?>
                        <div class="tab-pane <?php echo $cls; ?>" id="room-<?php echo $tabkey; ?>">
                            <div class="list-detail">
                                <div class="top-list-detail">
                                    <h3 class="title">
                                        <?php echo $tabData['title']; ?>
                                        <a href="javascript:;" data-year="<?php echo esc_json_attr($tabData['year_id']); ?>" data-toggle="modal"
                                           data-target="#tpl-ceo-kpi-year" >
                                            <span class="icon-percent" contenteditable="false" data-kpi-percent="<?php echo $tabkey; ?>"><?php echo $tabData['percent']; ?>%</span>
                                            <i class="glyphicon glyphicon-pencil hidden" aria-hidden="true"></i>
                                        </a>
                                    </h3>
                                </div>
                            </div>
                            <form data-approved="" action="<?php echo admin_url('admin-ajax.php?action=department_approved_kpi'); ?>" class=""
                                  id="form-<?php echo $tabkey; ?>" method="post" enctype="application/x-www-form-urlencoded"
                                  onsubmit="return false">
                                <input value="<?php echo wp_create_nonce('department_approved_kpi'); ?>" type="hidden" name="_wpnonce">
                                <input value="<?php echo $tabData['type']; ?>" type="hidden" name="type">
                                <input type="hidden" name="_wp_http_referer" value="<?php esc_attr_e( wp_unslash( $_SERVER['REQUEST_URI'] ) ); ?>#room-<?= $tabkey; ?>">
                                <table class="table-list-detail table-managerment-target-kpi" cellpadding="0" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th class="column-1 kpi-id align-center"><?php _e('Mã KPI', TPL_DOMAIN_LANG); ?></th>
                                        <th class="column-2 kpi-content"><?php _e('Nội dung mục tiêu', TPL_DOMAIN_LANG); ?></th>
                                        <th class="column-3 kpi-company_plan"><?php _e('Kế hoạch <br>'. $orgchartUser->name, TPL_DOMAIN_LANG); ?></th>
                                        <th class="column-4 kpi-unit align-center"><?php _e('ĐVT', TPL_DOMAIN_LANG); ?></th>
                                        <th class="column-5 kpi-receive align-center"><?php _e('Thời điểm nhận kết quả', TPL_DOMAIN_LANG); ?></th>
                                        <th class="column-7 align-center"><?php _e('Trọng số (%)', TPL_DOMAIN_LANG); ?></th>
                                        <th class="column-6 align-center"><?php _e('Trạng thái', TPL_DOMAIN_LANG); ?></th>
                                        <th class="column-8 align-center"><?php _e('Thao Tác', TPL_DOMAIN_LANG); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $totalPercent = 0.0;
                                    global $wpdb;
                                    #$groupDatas = kpi_get_list($tabData['type'], $tabData['time_type'], $tabData['time_value'], $user->ID, $user->orgchart_id);
                                    $groupDatas = kpi_get_list_approved_register($tabData['type'], $tabData['time_value'], $userID);
                                    if (!empty($groupDatas)):
                                        foreach ($groupDatas as &$item):
                                            $totalPercent += doubleval($item['percent']);
                                            $subcharts = kpi_get_list_of_charts($tabData['type'], $tabData['time_type'], $tabData['time_value'], 0, $user->orgchart_id, $item['bank_id']);
                                            ?>
                                            <tr>
                                                <td class="column-1 kpi-id align-center">
                                                    <?php echo $item['id']; ?>
                                                </td>
                                                <td class="column-2 kpi-content">
                                                    <?php echo $item['post_title']; ?>
                                                </td>
                                                <td class="column-3 kpi-plan"><?php echo $item['plan']; ?></td>
                                                <td class="column-4 kpi-unit align-center"><?php echo !empty($item['unit']) ? getUnit($item['unit']) : ''; ?></td>
                                                <td class="column-5 kpi-receive align-center"><?php echo substr(kpi_format_date($item['receive']), 0, 10); ?></td>

                                                <td class="column-7 kpi-percent align-center"><?php echo $item['percent']; ?></td>
                                                <td class="column-6 kpi-departments align-center"><?php
                                                    $items = [];
                                                    $disabled = '';
                                                    $name = 'name="register['.$item['id'].'][status]"';
                                                    $valueInput = $item['id'];
                                                    $textSendApproved = '';
                                                    if( $item['status'] == KPI_STATUS_RESULT ) {
                                                        $disabled = 'disabled';
                                                        $name ='';
                                                        $valueInput = '';
                                                        $textSendApproved = __('Đã duyệt', TPL_DOMAIN_LANG);
                                                    } elseif( $item['status'] == KPI_STATUS_PENDING ) {
                                                        $textSendApproved = __('Chờ duyệt', TPL_DOMAIN_LANG);
                                                    }
                                                    ?>
                                                    <label class="switch">
                                                        <input <?= !in_array($item['status'], [KPI_STATUS_DRAFT, KPI_STATUS_PENDING]) ? 'checked="checked"' : ''; ?> class="checkbox-status" name="register[<?= $item['id']; ?>][status]" value="1" type="checkbox">
                                                        <span class="checkbox-slider round"></span>
                                                        <input type="hidden" name="register[<?=$item['id']?>][ID]" value="<?= $item['id']; ?>" />
                                                    </label>
                                                    <span><?= $textSendApproved; ?></span>
                                                </td>
                                                <td class="column-8 align-center">
                                                    <?php if( $firstYear ):
                                                        $remove_params = [
                                                            'action' => 'remove_target_department',
                                                            'year_id' => $firstYear['id'],
                                                            'id' => $item['id'],
                                                            'kpi_type' => $tabData['type'],
                                                            '_wpnonce' => wp_create_nonce('remove-target'),
                                                        ];
                                                        $load_params = [
                                                            'action' => 'load-kpi-target',
                                                            'id' => $firstYear['id'],
                                                            'kpi_id' => $item['id'],
                                                            'kpi_type' => $tabData['type'],
                                                            'chart_id' => $item['chart_id'],
                                                            'bank_id' => $item['bank_id'],
                                                            'year' => $tabData['time_value'],
                                                            '_wpnonce' => wp_create_nonce('load-kpi-target'),
                                                        ];
                                                        ?>
                                                        <a href="javascript:;" data-type="get" data-url="<?php
                                                        echo esc_attr( add_query_arg($load_params, admin_url('admin-ajax.php') ) );
                                                        ?>" data-id="<?= $item['id'];
                                                        ?>" class="action-edit" title="<?php _e('Chỉnh sửa', TPL_DOMAIN_LANG);
                                                        ?>" data-action="edit" data-toggle="modal" data-target="#tpl-department-add-target-<?= $tabkey; ?>">
                                                            <span class="glyphicon glyphicon-pencil"></span>
                                                        </a>
                                                        <a href="<?php echo esc_attr( add_query_arg($remove_params, admin_url('admin-ajax.php') ) ); ?>" title="Xóa" data-ajax="remove-department-target" data-method="post">
                                                            <span class="glyphicon glyphicon-trash"></span>
                                                        </a>
                                                    <?php endif; ?>
                                                </td>

                                            </tr>
                                        <?php endforeach;
                                    endif;
                                    ?>
                                    </tbody>
                                </table>
                                <div class="action-bot">
                                    <button type="submit" class="btn-brb-default btn-save-and-public btn-action-item"
                                            name="btn-save-public"><i class="fa fa-floppy-o"
                                                                      aria-hidden="true"></i> <?php _e('Lưu & triển khai', TPL_DOMAIN_LANG); ?>
                                    </button>
                                </div>
                            </form>
                            <?php if( $totalPercent < 100.0 ): ?>
                                <div id="tpl-department-add-target-<?php echo $tabkey; ?>" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
                                    <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded"
                                          action="<?php echo admin_url('admin-ajax.php?action=add_department_target') ?>" method="post">
                                        <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce('add_department_target'); ?>">
                                        <input type="hidden" name="type" value="<?php echo $tabData['type']; ?>">
                                        <input type="hidden" name="year_id" value="<?php echo $firstYear ? $firstYear['id'] : 0; ?>">
                                        <input type="hidden" name="time_type" value="<?php echo $tabData['time_type']; ?>">
                                        <input type="hidden" name="time_value" value="<?php echo $tabData['time_value']; ?>">
                                        <input type="hidden" name="chart_id" value="<?php echo $user->orgchart_id; ?>">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title"><?php echo $tabData['title']; ?>
                                                    : <span data-title-add="Thêm KPI" data-title-edit="Chỉnh sửa KPI"></span></h4>
                                            </div>
                                            <div class="modal-body clearfix">
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="form-group input-group input-group-select">
                                                        <label class="input-group-addon" for="year_id" id="chart_name-<?php echo $tabkey; ?>">Chỉ mục KPI: </label>
                                                        <select data-loading=".form-group.input-group.input-group-select"
                                                                data-live-search="true" data-target="#list-kpis-<?php echo $tabkey; ?>"
                                                                aria-describedby="chart_name-<?php echo $tabkey; ?>"
                                                                class="kpi-category selectpicker input-select" name="cat"><?php
                                                            kpiRenderCategoryDropdown('', true, '--- ');
                                                            ?></select>
                                                    </div>
                                                    <div class="form-group input-group-list">
                                                        <div class="department-container hidden"></div>
                                                        <ul id="list-kpis-<?php echo $tabkey; ?>" class="list-group"></ul>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="form-group form-plans">
                                                        <div class="input-group">
                                                            <span class="input-group-addon" id="plan-<?php echo $tabkey; ?>">Nhập kế hoạch</span>
                                                            <input required type="text" name="plan" class="form-control"
                                                                   placeholder="Kế hoạch" aria-describedby="plan-<?php echo $tabkey; ?>">
                                                        </div>
                                                        <div class="input-group">
                                                            <span class="input-group-addon" id="unit-<?php echo $tabkey; ?>">Đơn vị tính</span>
                                                            <select data-none-selected-text="Đơn vị tính" id="unit-<?php echo $tabkey; ?>" class="select-unit selectpicker" name="unit">
                                                                <?php
                                                                $unitArr = getUnit();
                                                                foreach( $unitArr as $kUnit => $vUnit ) {
                                                                    echo sprintf( "<option value='%s'>%s</option>", $kUnit, $vUnit );
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="input-group group-date">
                                                <span class="input-group-addon"
                                                      id="receive-<?php echo $tabkey; ?>">Thời điểm nhận kết quả</span>
                                                            <input required type="text" name="receive" class="form-control"
                                                                   placeholder="Thời điểm nhận kết quả" data-lang="vi" data-format="<?php echo KPI_FORMAT_DATE; ?>"
                                                                   data-min-date="01-01-<?php echo $tabData['time_value']; ?>"
                                                                <?php /* data-max-date="" */ ?>
                                                                   data-group-date=".group-date" data-timepicker="false"
                                                                   data-btn-date=".input-group-addon.date-btn"
                                                                   data-ctrl-date aria-describedby="receive-<?php echo $tabkey; ?>">
                                                            <span class="input-group-addon date-btn">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                                        </div>
                                                        <div class="input-group">
                                                            <span class="input-group-addon" id="percent-<?php echo $tabkey; ?>">Trọng số tối đa</span>
                                                            <input required type="number" min="0" name="percent" class="form-control"
                                                                   placeholder="Trọng số tối đa" aria-describedby="percent-<?php echo $tabkey; ?>">
                                                            <span class="input-group-addon">%</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <button type="submit" class="btn btn-primary" data-text-add="Thêm KPI" data-text-edit="Lưu">Lưu</button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php
        ?>
<?php endif; ?>