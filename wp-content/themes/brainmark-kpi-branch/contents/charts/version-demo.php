<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/19/18
 * Time: 18:34
 */

$charts = kpi_load_org_charts();
if( !empty($charts) ) {
    $jsonData = sprintf(' data-chart-data="%s"', esc_json_attr($charts));
} else {
    $jsonData = [
            "id" => 0,
            "name" => "Công Ty",
            "title" => "",
            "alias" => "congty",
            "role" => "bgd",
            "alias_text" => "Công Ty",
            "room" => "Công Ty",
            "has_kpi" => 1,
            "parentId" => 0,
            "level" => 0,
        ];
    if( !empty($jsonData) ) {
        $jsonData = sprintf(' data-chart-data="%s"', esc_json_attr($jsonData));
    }
}
require_once THEME_DIR . '/inc/lib-users.php';
$nonce = wp_create_nonce('kpi_chart');
$user = wp_get_current_user();
$editable = false;
if( kpi_is_user_ceo() || $user->has_cap('level_9') ) {
    $editable = true;
}
?>
<div id="chart-container" class="container-fluid dragscroll" <?php echo $jsonData; ?> data-editable="<?php echo $editable ? '1': '0'; ?>"
     data-edit-url="<?php echo admin_url('admin-ajax.php?action=kpi_chart&nonce='.$nonce); ?>"
     data-add-url="<?php echo admin_url('admin-ajax.php?action=kpi_chart&nonce='.$nonce); ?>"
     data-remove-url="<?php echo admin_url('admin-ajax.php?action=kpi_chart_remove&nonce='.wp_create_nonce('kpi_chart_remove')); ?>"></div>

<div id="orgchart-popup-item" class=" modal fade" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
    <form class="modal-dialog" role="document" onsubmit="return false">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" data-title-add="Thêm mới" data-title-edit="Chỉnh sửa" >Chỉnh sửa</h4>
        </div>
        <div class="modal-body">
            <div class="response-message hidden"></div>
            <div class="container-fluid">
                <div class="col-md-12">
                    <div class="input-group">
                        <label for="node_name" id="label_node_name" class="input-group-addon">Chức danh:</label>
                        <input type="text" id="node_name" name="node_name" class="form-control" placeholder="Chức danh" aria-describedby="label_node_name" aria-label="Chức danh">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group">
                        <label for="node_alias" id="label_node_alias" class="input-group-addon">Cấp:</label>
                        <select data-target="#node_room_group" class="selectpicker input-select form-control" id="node_alias" name="node_alias" aria-describedby="label_node_alias" aria-label="Cấp:">
                            <option value=""> -- Chọn -- </option>
                            <?php
                            foreach ($GLOBALS['charts_alias'] as $val => $text):
                                echo sprintf('<option value="%s">%s</option>', $val, $text);
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group">
                        <label for="node_has_kpi" id="label_node_has_kpi" class="input-group-addon">Có đánh giá KPI:</label>
                        <?php /* <input type="text" id="node_has_kpi" name="node_has_kpi" class="form-control" placeholder="Có đánh giá KPI" aria-describedby="label_node_has_kpi" aria-label="Có đánh giá KPI"> */ ?>
                        <label class="switch" aria-describedby="label_node_has_kpi" aria-label="Có đánh giá KPI"><input class="checkbox-status hidden" name="node_has_kpi" type="checkbox" value="1" /><span class="checkbox-slider"></span></label>
                    </div>
                </div>
                <div class="col-md-12" id="node_room_group">
                    <div class="input-group">
                        <label for="node_room" id="label_node_room" class="input-group-addon">Tập Đoàn/Công Ty/Tên phòng/...:</label>
                        <input type="text" id="node_room" name="node_room" class="form-control" placeholder="Tập Đoàn/Công Ty/Tên phòng/..." aria-describedby="label_node_room" aria-label="Tập Đoàn/Công Ty/Tên phòng/...">
                    </div>
                </div>
                <?php
                $list_orgchart = orgchart_get_room_and_company();
                if( !empty( $list_orgchart ) ):
                ?>
                    <div class="col-md-12" id="node_room_view_onlevel_item" >
                        <div class="input-group">
                            <label for="node_level" id="label_node_view_onlevel" class="input-group-addon">Cấp quản lý trực tiếp:</label>
                            <select class="selectpicker label_node_view_onlevel" name="node_onlevel" >
                               <?php
                                foreach ( $list_orgchart as $k => $org ){
                                    echo sprintf("<option value='%s'>%s</option>", $org['id'], $org['name']);
                                }
                               ?>
                            </select>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="col-md-12" id="node_room_view">
                    <div class="input-group">
                        <label for="node_level" id="label_node_view" class="input-group-addon">Cấp hiển thị:</label>
                        <input type="number" id="node_level" name="node_level" class="form-control" placeholder="Cấp hiển thị" aria-describedby="label_node_level" aria-label="Cấp hiển thị">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-text-edit="Lưu" data-text-add="Thêm" >Lưu</button>
            <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', TPL_DOMAIN_LANG); ?></button>
        </div><!-- /.modal-content -->
    </form><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="confirm-chart-popup" class="confirm-modal modal fade" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php _e('Xác nhận', TPL_DOMAIN_LANG); ?></h4>
        </div>
        <div class="modal-body">
            <h3 class="message" data-message=""></h3>
            <div class="response-message hidden"></div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', TPL_DOMAIN_LANG); ?></button>
            <button type="button" class="btn btn-primary"><?php _e('OK', TPL_DOMAIN_LANG); ?></button>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->