<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/19/18
 * Time: 18:33
 */
?>
<div class="portlet sub-content ">
        <div class="portlet-title">
            <div class="caption"><h2><i class="fa fa-gift"></i>Cơ cấu tổ chức công ty</h2></div>
        </div>
        <div class="portlet-body">
            <div id="orgChartContainer">
                <?php

                $jsonDataDefault = [
                    'id' => 0,
                    'name' => __("Name"),
                    'parent' => 0,
                    'role' => __('Role'),
                    'description' => __('Description'),
                    'avatar' => THEME_URL . '/assets/images/no-photo.jpg',
                    'cls' => '',
                    'childCls' => 'clearfix float'
                ];
                global $wpdb;

                $user = wp_get_current_user();
                $can_modify = false;
                if (!empty($user->allcaps['level_9']) || $user->has_cap('level_9')) {
                    $can_modify = true;
                }
                $remove_nonce = wp_create_nonce('remove_orgchart');
                ?>
<div class="container">
    <?php $roots = kpi_orgchart_get_list(-1);
    $roots = process_children($roots); ?>
    <pre style="text-align: left"><?php # echo json_encode($roots, JSON_PRETTY_PRINT). "\n\n\n"; # var_dump( $roots ); ?></pre>
    <table id="org-chart-table" data-remove-url="<?php
    echo esc_attr(admin_url('admin-ajax.php?action=remove_orgchart&_wpnonce=' . $remove_nonce));
    ?>" data-url="<?php
    echo esc_attr(admin_url('admin-ajax.php'));
    ?>" data-nonce="<?php echo $remove_nonce; ?>"
           class="org-chart-table table" cellpadding="0" cellspacing="0" width="100%">
        <colgroup>
            <col style="width: 30%">
            <col style="width: 30%">
            <col style="width: 30%">
        </colgroup>
        <thead>
        <tr>
            <th class="level1 director"><span class="name">BGD</span>
                <span class="control">
                                    <?php if ($can_modify): ?>
                                        <a href="javascript:;" class="add" data-event="add-director"
                                           title="Add"><em class="glyphicon glyphicon-plus"></em></a>
                                    <?php endif; ?>
                                </span>
            </th>
            <th class="level2 department"><span class="name">Phong Ban</span></th>
            <th class="level3 employee"><span class="name">Nhan Vien</span></th>
        </tr>
        </thead>
        <tbody class="list">
        <tr class="row-template hidden">
            <td class="level1 director" rowspan="1">
                <img class="thumb"
                     src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                     onerror="this.src=this.getAttribute('data-src');"
                     data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
                <span class="name"></span>
                <span class="control">
                                                <?php if ($can_modify): ?>
                                                    <a href="javascript:;" class="add" data-event="add-director"
                                                       title="Add"><em class="glyphicon glyphicon-plus"></em></a>
                                                    <a href="javascript:;" class="edit hidden"
                                                       data-event="edit-director" title="Edit"><em
                                                            class="glyphicon glyphicon-pencil"></em></a>
                                                    <a href="javascript:;" class="remove hidden"
                                                       data-event="remove-director" title="Remove"><em
                                                            class="glyphicon glyphicon-remove"></em></a>
                                                <?php endif; ?>
                                            </span>
            </td>
            <td class="level2 department" rowspan="1">
                <img class="thumb"
                     src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                     onerror="this.src=this.getAttribute('data-src');"
                     data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
                <span class="name"></span>
                <span class="control">
                                                <?php if ($can_modify): ?>
                                                    <a href="javascript:;" class="add hidden"
                                                       data-event="add-department" title="Add"><em
                                                            class="glyphicon glyphicon-plus"></em></a>
                                                    <a href="javascript:;" class="edit hidden"
                                                       data-event="edit-department" title="Edit"><em
                                                            class="glyphicon glyphicon-pencil"></em></a>
                                                    <a href="javascript:;" class="remove hidden"
                                                       data-event="remove-department" title="Remove"><em
                                                            class="glyphicon glyphicon-remove"></em></a>
                                                <?php endif; ?>
                                            </span>
            </td>
            <td class="level3 employee" rowspan="1">
                <img class="thumb"
                     src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                     onerror="this.src=this.getAttribute('data-src');"
                     data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
                <span class="name"></span>
                <span class="control">
                                                <?php if ($can_modify): ?>
                                                    <a href="javascript:;" class="add hidden" data-event="add-employee"
                                                       title="Add"><em class="glyphicon glyphicon-plus"></em></a>
                                                    <a href="javascript:;" class="edit hidden"
                                                       data-event="edit-employee" title="Edit"><em
                                                            class="glyphicon glyphicon-pencil"></em></a>
                                                    <a href="javascript:;" class="remove hidden"
                                                       data-event="remove-employee" title="Remove"><em
                                                            class="glyphicon glyphicon-remove"></em></a>
                                                <?php endif; ?>
                                            </span>
            </td>
        </tr>
        <?php
        if (!empty($roots)):
            foreach ($roots as &$child1):
                if (count($child1['children']) > 0):
                    # echo count($child1['children'])."<br>";
                    $dep = $child1['children'][0];
                    # var_dump($dep);
                    $emp = (count($dep['children']) > 0) ? $dep['children'][0] : null;
                    # var_dump($emp);
                    ?>
                    <tr>
                        <td class="level1 director" rowspan="<?php echo $child1['rowspan'];
                        ?>" data-id="<?php echo($child1 ? $child1['id'] : '0'); ?>"
                            data-node="<?php esc_json_attr_e($child1); ?>">
                            <img class="thumb" src="<?php
                            echo ($child1 && $child1['icon']) ? $child1['icon'] : 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
                            ?>" onerror="this.src=this.getAttribute('data-src');"
                                 data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
                            <span class="name"><?php echo $child1['name']; ?></span>
                            <span class="control">
                                                        <?php if ($can_modify): ?>
                                                            <a href="javascript:;" class="add" data-event="add-director"
                                                               title="Add"><em
                                                                    class="glyphicon glyphicon-plus"></em></a>
                                                            <a href="javascript:;" class="edit"
                                                               data-event="edit-director" title="Edit"><em
                                                                    class="glyphicon glyphicon-pencil"></em></a>
                                                            <a href="javascript:;" class="remove"
                                                               data-event="remove-director" title="Remove"><em
                                                                    class="glyphicon glyphicon-remove"></em></a>
                                                        <?php endif; ?>
                                                        </span>
                        </td>
                        <td class="level2 department" rowspan="<?php echo($dep['rowspan']);
                        ?>" data-id="<?php echo $dep['id']; ?>"
                            data-node="<?php esc_json_attr_e($dep); ?>">
                            <img class="thumb" src="<?php
                            echo ($dep && $dep['icon']) ? $dep['icon'] : 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
                            ?>" onerror="this.src=this.getAttribute('data-src');"
                                 data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
                            <span class="name"><?php echo $dep['name']; ?></span>
                            <span class="control">
                                                        <?php if ($can_modify): ?>
                                                            <a href="javascript:;" class="add"
                                                               data-event="add-department" title="Add"><em
                                                                    class="glyphicon glyphicon-plus"></em></a>
                                                            <a href="javascript:;"
                                                               class="edit<?php echo(!$dep ? ' hidden' : ''); ?>"
                                                               data-event="edit-department" title="Edit"><em
                                                                    class="glyphicon glyphicon-pencil"></em></a>
                                                            <a href="javascript:;"
                                                               class="remove<?php echo(!$dep ? ' hidden' : ''); ?>"
                                                               data-event="remove-department" title="Remove"><em
                                                                    class="glyphicon glyphicon-remove"></em></a>
                                                        <?php endif; ?>
                                                        </span>
                        </td>
                        <td class="level3 employee" rowspan="1" data-id="<?php
                        echo($emp ? $emp['id'] : '0') ?>"
                            data-node="<?php echo($emp ? esc_json_attr($emp) : '{}'); ?>">
                            <img class="thumb" src="<?php
                            echo ($emp && $emp['icon']) ? $emp['icon'] : 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
                            ?>" onerror="this.src=this.getAttribute('data-src');"
                                 data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
                            <span class="name"><?php echo($emp ? $emp['name'] : ''); ?></span>
                            <span class="control">
                                                            <?php if ($can_modify): ?>
                                                                <a href="javascript:;" class="add"
                                                                   data-event="add-employee" title="Add"><em
                                                                        class="glyphicon glyphicon-plus"></em></a>
                                                                <a href="javascript:;"
                                                                   class="edit<?php echo(!$emp ? ' hidden' : ''); ?>"
                                                                   data-event="edit-employee" title="Edit"><em
                                                                        class="glyphicon glyphicon-pencil"></em></a>
                                                                <a href="javascript:;"
                                                                   class="remove<?php echo(!$emp ? ' hidden' : ''); ?>"
                                                                   data-event="remove-employee" title="Remove"><em
                                                                        class="glyphicon glyphicon-remove"></em></a>
                                                            <?php endif; ?>
                                                        </span>
                        </td>
                    </tr>
                    <?php
                    for ($j = 1, $nj = count($dep['children']); $j < $nj; $j++):
                        $emp = $dep['children'][$j];
                        ?>
                        <tr>
                            <td class="level3 employee" rowspan="1"
                                data-id="<?php echo($emp ? $emp['id'] : '0') ?>"
                                data-node="<?php echo($emp ? esc_json_attr($emp) : '{}'); ?>">
                                <img class="thumb" src="<?php
                                echo ($emp && $emp['icon']) ? $emp['icon'] : 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
                                ?>" onerror="this.src=this.getAttribute('data-src');"
                                     data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
                                <span class="name"><?php echo($emp['name']); ?></span>
                                <span class="control">
                                                                <?php if ($can_modify): ?>
                                                                    <a href="javascript:;" class="add"
                                                                       data-event="add-employee" title="Add"><em
                                                                            class="glyphicon glyphicon-plus"></em></a>
                                                                    <a href="javascript:;" class="edit<?php echo(!$emp ? ' hidden' : ''); ?>"
                                                                       data-event="edit-employee" title="Edit"><em
                                                                            class="glyphicon glyphicon-pencil"></em></a>
                                                                    <a href="javascript:;" class="remove<?php echo(!$emp ? ' hidden' : ''); ?>"
                                                                       data-event="remove-employee" title="Remove"><em
                                                                            class="glyphicon glyphicon-remove"></em></a>
                                                                <?php endif; ?>
                                                            </span>
                            </td>
                        </tr>
                        <?php
                    endfor;

                    for ($i = 1, $n = count($child1['children']); $i < $n; $i++):
                        $dep = $child1['children'][$i];
                        $emp = (count($dep['children']) > 0) ? $dep['children'][0] : null;
                        ?>
                        <tr>
                            <td class="level2 department"
                                rowspan="<?php echo($dep ? $dep['rowspan'] : '1');
                                ?>" data-id="<?php echo($dep ? $dep['id'] : '0'); ?>"
                                data-node="<?php echo($dep ? esc_json_attr($dep) : '{}'); ?>">
                                <img class="thumb" src="<?php
                                echo ($child1 && $child1['icon']) ? $child1['icon'] : 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
                                ?>" onerror="this.src=this.getAttribute('data-src');"
                                     data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
                                <span class="name"><?php echo($dep ? ($dep['name']) : ''); ?></span>
                                <span class="control">
                                                                <?php if ($can_modify): ?>
                                                                    <a href="javascript:;" class="add"
                                                                       data-event="add-department" title="Add"><em
                                                                            class="glyphicon glyphicon-plus"></em></a>
                                                                    <a href="javascript:;"
                                                                       class="edit<?php echo(!$dep ? ' hidden' : ''); ?>"
                                                                       data-event="edit-department" title="Edit"><em
                                                                            class="glyphicon glyphicon-pencil"></em></a>
                                                                    <a href="javascript:;"
                                                                       class="remove<?php echo(!$dep ? ' hidden' : ''); ?>"
                                                                       data-event="remove-department" title="Remove"><em
                                                                            class="glyphicon glyphicon-remove"></em></a>
                                                                <?php endif; ?>
                                                            </span>
                            </td>
                            <td class="level3 employee" rowspan="1" data-id="<?php
                            echo($emp ? $emp['id'] : '0') ?>"
                                data-node="<?php echo($emp ? esc_json_attr($emp) : '{}'); ?>">
                                <img class="thumb" src="<?php
                                echo ($emp && $emp['icon']) ? $emp['icon'] : 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
                                ?>" onerror="this.src=this.getAttribute('data-src');"
                                     data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
                                <span class="name"><?php echo($emp ? ($emp['name']) : ''); ?></span>
                                <span class="control">
                                                                <?php if ($can_modify): ?>
                                                                    <a href="javascript:;"
                                                                       class="add<?php echo($dep ? '' : ' hidden'); ?>"
                                                                       data-event="add-employee" title="Add"><em
                                                                            class="glyphicon glyphicon-plus"></em></a>
                                                                    <a href="javascript:;"
                                                                       class="edit<?php echo(!$emp ? ' hidden' : ''); ?>"
                                                                       data-event="edit-employee" title="Edit"><em
                                                                            class="glyphicon glyphicon-pencil"></em></a>
                                                                    <a href="javascript:;"
                                                                       class="remove<?php echo(!$emp ? ' hidden' : ''); ?>"
                                                                       data-event="remove-employee" title="Remove"><em
                                                                            class="glyphicon glyphicon-remove"></em></a>
                                                                <?php endif; ?>
                                                            </span>
                            </td>
                        </tr>
                        <?php
                        for ($j = 1, $nj = count($dep['children']); $j < $nj; $j++):
                            $emp = $dep['children'][$j];
                            ?>
                            <tr>
                                <td class="level3 employee" rowspan="1"
                                    data-id="<?php echo($emp ? $emp['id'] : '0') ?>"
                                    data-node="<?php echo($emp ? esc_json_attr($emp) : '{}'); ?>">
                                    <img class="thumb" src="<?php
                                    echo ($emp && $emp['icon']) ? $emp['icon'] : 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
                                    ?>" onerror="this.src=this.getAttribute('data-src');"
                                         data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
                                    <span class="name"><?php echo $emp ? ($emp['name']) : ''; ?></span>
                                    <span class="control">
                                                                    <?php if ($can_modify): ?>
                                                                        <a href="javascript:;"
                                                                           class="add<?php echo(!$emp ? ' hidden' : ''); ?>"
                                                                           data-event="add-employee" title="Add"><em
                                                                                class="glyphicon glyphicon-plus"></em></a>
                                                                        <a href="javascript:;"
                                                                           class="edit<?php echo(!$emp ? ' hidden' : ''); ?>"
                                                                           data-event="edit-employee" title="Edit"><em
                                                                                class="glyphicon glyphicon-pencil"></em></a>
                                                                        <a href="javascript:;"
                                                                           class="remove<?php echo(!$emp ? ' hidden' : ''); ?>"
                                                                           data-event="remove-employee"
                                                                           title="Remove"><em
                                                                                class="glyphicon glyphicon-remove"></em></a>
                                                                    <?php endif; ?>
                                                                </span>
                                </td>
                            </tr>
                            <?php
                        endfor;
                    endfor;
                else:
                    ?>
                    <tr>
                        <td class="level1 director" rowspan="<?php echo $child1['rowspan'];
                        ?>" data-id="<?php echo($child1 ? $child1['id'] : '0'); ?>"
                            data-node="<?php esc_json_attr_e($child1); ?>">
                            <img class="thumb" src="<?php
                            echo ($child1 && $child1['icon']) ? $child1['icon'] : 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
                            ?>" onerror="this.src=this.getAttribute('data-src');"
                                 data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
                            <span class="name"><?php echo $child1['name']; ?></span>
                            <span class="control">
                                                            <?php if ($can_modify): ?>
                                                                <a href="javascript:;" class="add"
                                                                   data-event="add-director" title="Add"><em
                                                                        class="glyphicon glyphicon-plus"></em></a>
                                                                <a href="javascript:;" class="edit"
                                                                   data-event="edit-director" title="Edit"><em
                                                                        class="glyphicon glyphicon-pencil"></em></a>
                                                                <a href="javascript:;" class="remove"
                                                                   data-event="remove-director" title="Remove"><em
                                                                        class="glyphicon glyphicon-remove"></em></a>
                                                            <?php endif; ?>
                                                        </span>
                        </td>
                        <td class="level2 department" rowspan="1" data-id="0" data-node="{}">
                            <img class="thumb"
                                 src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                 onerror="this.src=this.getAttribute('data-src');"
                                 data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
                            <span class="name"></span>
                            <span class="control">
                                                            <?php if ($can_modify): ?>
                                                                <a href="javascript:;" class="add"
                                                                   data-event="add-department" title="Add"><em
                                                                        class="glyphicon glyphicon-plus"></em></a>
                                                                <a href="javascript:;" class="edit hidden"
                                                                   data-event="edit-department" title="Edit"><em
                                                                        class="glyphicon glyphicon-pencil"></em></a>
                                                                <a href="javascript:;" class="remove hidden"
                                                                   data-event="remove-department" title="Remove"><em
                                                                        class="glyphicon glyphicon-remove"></em></a>
                                                            <?php endif; ?>
                                                        </span>
                        </td>
                        <td class="level3 employee" rowspan="1" data-id="0" data-node="{}">
                            <img class="thumb"
                                 src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                 onerror="this.src=this.getAttribute('data-src');"
                                 data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
                            <span class="name"></span>
                            <span class="control">
                                                            <?php if ($can_modify): ?>
                                                                <a href="javascript:;" class="add hidden"
                                                                   data-event="add-employee" title="Add"><em
                                                                        class="glyphicon glyphicon-plus"></em></a>
                                                                <a href="javascript:;" class="edit hidden"
                                                                   data-event="edit-employee" title="Edit"><em
                                                                        class="glyphicon glyphicon-pencil"></em></a>
                                                                <a href="javascript:;" class="remove hidden"
                                                                   data-event="remove-employee" title="Remove"><em
                                                                        class="glyphicon glyphicon-remove"></em></a>
                                                            <?php endif; ?>
                                                        </span>
                        </td>
                    </tr>
                    <?php
                endif;
            endforeach;
        endif;
        ?>
        <?php /* ?>
                                    <tr class="group-hanh-chinh">
                                        <td class="level1 director" rowspan="5">
                                            <span class="name">Giam Doc HR</span>
                                            <span class="control">
                                                <a href="javascript:;" class="add" data-event="add-director" title="Add"><em class="glyphicon glyphicon-plus"></em></a>
                                                <a href="javascript:;" class="edit" data-event="edit-director" title="Edit"><em class="glyphicon glyphicon-pencil"></em></a>
                                                <a href="javascript:;" class="remove" data-event="remove-director" title="Remove"><em class="glyphicon glyphicon-remove"></em></a>
                                            </span></td>
                                        <td class="level2 department" rowspan="3">
                                            <span class="name">HR</span>
                                            <span class="control">
                                                <a href="javascript:;" class="add" data-event="add-department" title="Add"><em class="glyphicon glyphicon-plus"></em></a>
                                                <a href="javascript:;" class="edit" data-event="edit-department" title="Edit"><em class="glyphicon glyphicon-pencil"></em></a>
                                                <a href="javascript:;" class="remove" data-event="remove-department" title="Remove"><em class="glyphicon glyphicon-remove"></em></a>
                                            </span></td>
                                        <td class="level3 employee" rowspan="1">
                                            <span class="name">HR PM</span>
                                            <span class="control">
                                                <a href="javascript:;" class="add" data-event="add-employee" title="Add"><em class="glyphicon glyphicon-plus"></em></a>
                                                <a href="javascript:;" class="edit" data-event="edit-employee" title="Edit"><em class="glyphicon glyphicon-pencil"></em></a>
                                                <a href="javascript:;" class="remove" data-event="remove-employee" title="Remove"><em class="glyphicon glyphicon-remove"></em></a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="group-hanh-chinh">
                                        <td class="level3 employee" rowspan="1">
                                        <span class="name">HR Members</span>
                                        <span class="control">
                                                <a href="javascript:;" class="add" data-event="add-employee" title="Add"><em class="glyphicon glyphicon-plus"></em></a>
                                                <a href="javascript:;" class="edit" data-event="edit-employee" title="Edit"><em class="glyphicon glyphicon-pencil"></em></a>
                                                <a href="javascript:;" class="remove" data-event="remove-employee" title="Remove"><em class="glyphicon glyphicon-remove"></em></a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="group-hanh-chinh">
                                        <td class="level3 employee" rowspan="1">
                                            <span class="name">HR Relax</span>
                                            <span class="control">
                                                <a href="javascript:;" class="add" data-event="add-employee" title="Add"><em class="glyphicon glyphicon-plus"></em></a>
                                                <a href="javascript:;" class="edit" data-event="edit-employee" title="Edit"><em class="glyphicon glyphicon-pencil"></em></a>
                                                <a href="javascript:;" class="remove" data-event="remove-employee" title="Remove"><em class="glyphicon glyphicon-remove"></em></a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="group-hanh-chinh">
                                        <td class="level2 department" rowspan="2">
                                            <span class="name">Admin</span>
                                            <span class="control">
                                                <a href="javascript:;" class="add" data-event="add-department" title="Add"><em class="glyphicon glyphicon-plus"></em></a>
                                                <a href="javascript:;" class="edit" data-event="edit-department" title="Edit"><em class="glyphicon glyphicon-pencil"></em></a>
                                                <a href="javascript:;" class="remove" data-event="remove-department" title="Remove"><em class="glyphicon glyphicon-remove"></em></a>
                                            </span></td>
                                        <td class="level3 employee" rowspan="1">
                                            <span class="name">A</span>
                                            <span class="control">
                                                <a href="javascript:;" class="add" data-event="add-employee" title="Add"><em class="glyphicon glyphicon-plus"></em></a>
                                                <a href="javascript:;" class="edit" data-event="edit-employee" title="Edit"><em class="glyphicon glyphicon-pencil"></em></a>
                                                <a href="javascript:;" class="remove" data-event="remove-employee" title="Remove"><em class="glyphicon glyphicon-remove"></em></a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="group-hanh-chinh">
                                        <td class="level3 employee" rowspan="1">
                                            <span class="name">B</span>
                                            <span class="control">
                                                <a href="javascript:;" class="add" data-event="add-employee" title="Add"><em class="glyphicon glyphicon-plus"></em></a>
                                                <a href="javascript:;" class="edit" data-event="edit-employee" title="Edit"><em class="glyphicon glyphicon-pencil"></em></a>
                                                <a href="javascript:;" class="remove" data-event="remove-employee" title="Remove"><em class="glyphicon glyphicon-remove"></em></a>
                                            </span>
                                        </td>
                                    </tr>
                                    <!-- /Kinh Doanh -->
                                    <tr class="group-kinh-doanh">
                                        <td class="level1 director" rowspan="4">
                                            <span class="name">Giam Doc Kinh Doanh</span>
                                            <span class="control">
                                                <a href="javascript:;" class="add" data-event="add-director" title="Add"><em class="glyphicon glyphicon-plus"></em></a>
                                                <a href="javascript:;" class="edit" data-event="edit-director" title="Edit"><em class="glyphicon glyphicon-pencil"></em></a>
                                                <a href="javascript:;" class="remove" data-event="remove-director" title="Remove"><em class="glyphicon glyphicon-remove"></em></a>
                                            </span>
                                        </td>
                                        <td class="level2 department" rowspan="2">
                                            <span class="name">Kinh Doanh</span>
                                            <span class="control">
                                                <a href="javascript:;" class="add" data-event="add-department" title="Add"><em class="glyphicon glyphicon-plus"></em></a>
                                                <a href="javascript:;" class="edit" data-event="edit-department" title="Edit"><em class="glyphicon glyphicon-pencil"></em></a>
                                                <a href="javascript:;" class="remove" data-event="remove-department" title="Remove"><em class="glyphicon glyphicon-remove"></em></a>
                                            </span>
                                        </td>
                                        <td class="level3 employee" rowspan="1">
                                            <span class="name">Sale Lead</span>
                                            <span class="control">
                                                <a href="javascript:;" class="add" data-event="add-employee" title="Add"><em class="glyphicon glyphicon-plus"></em></a>
                                                <a href="javascript:;" class="edit" data-event="edit-employee" title="Edit"><em class="glyphicon glyphicon-pencil"></em></a>
                                                <a href="javascript:;" class="remove" data-event="remove-employee" title="Remove"><em class="glyphicon glyphicon-remove"></em></a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="group-kinh-doanh">
                                        <td class="level3 employee" rowspan="1">
                                            <span class="name">Sale Member</span>
                                            <span class="control">
                                                <a href="javascript:;" class="add" data-event="add-employee" title="Add"><em class="glyphicon glyphicon-plus"></em></a>
                                                <a href="javascript:;" class="edit" data-event="edit-employee" title="Edit"><em class="glyphicon glyphicon-pencil"></em></a>
                                                <a href="javascript:;" class="remove" data-event="remove-employee" title="Remove"><em class="glyphicon glyphicon-remove"></em></a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="group-kinh-doanh">
                                        <td class="level2 department" rowspan="2">
                                            <span class="name">Marketing</span>
                                            <span class="control">
                                                <a href="javascript:;" class="add" data-event="add-department" title="Add"><em class="glyphicon glyphicon-plus"></em></a>
                                                <a href="javascript:;" class="edit" data-event="edit-department" title="Edit"><em class="glyphicon glyphicon-pencil"></em></a>
                                                <a href="javascript:;" class="remove" data-event="remove-department" title="Remove"><em class="glyphicon glyphicon-remove"></em></a>
                                            </span>
                                        </td>
                                        <td class="level3 employee" rowspan="1">
                                            <span class="name">Mar A</span>
                                            <span class="control">
                                                <a href="javascript:;" class="add" data-event="add-employee" title="Add"><em class="glyphicon glyphicon-plus"></em></a>
                                                <a href="javascript:;" class="edit" data-event="edit-employee" title="Edit"><em class="glyphicon glyphicon-pencil"></em></a>
                                                <a href="javascript:;" class="remove" data-event="remove-employee" title="Remove"><em class="glyphicon glyphicon-remove"></em></a>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="group-kinh-doanh">
                                        <td class="level3 employee" rowspan="1">
                                            <span class="name">Mar B</span>
                                            <span class="control">
                                                <a href="javascript:;" class="add" data-event="add-employee" title="Add"><em class="glyphicon glyphicon-plus"></em></a>
                                                <a href="javascript:;" class="edit" data-event="edit-employee" title="Edit"><em class="glyphicon glyphicon-pencil"></em></a>
                                                <a href="javascript:;" class="remove" data-event="remove-employee" title="Remove"><em class="glyphicon glyphicon-remove"></em></a>
                                            </span>
                                        </td>
                                    </tr>
                                    <?php */ ?>
        </tbody>
    </table>
</div>
<?php /*
                        <ul id="orgChart" class="list-org-chart"
                            data-can-add="<?php echo ($create_users ? 'true' : 'false'); ?>"
                            data-can-edit="<?php echo ($edit_users ? 'true' : 'false'); ?>"
                            data-get-item-url="<?php echo admin_url( 'admin-ajax.php?action=user_info&_wpnonce='.$nonce_user_info.'&id=#ID#' ); ?>"
                            data-update-url="<?php echo admin_url( 'admin-ajax.php?action=update_tree&_wpnonce='.wp_create_nonce('update_tree') ); ?>"
                            data-create-item-url="<?php echo admin_url( 'admin-ajax.php?action=create_user' ); # _wpnonce='.wp_create_nonce('create_user') ?>"
                            data-default-item="<?php esc_json_attr_e($jsonDataDefault); ?>" data-items="<?php esc_json_attr_e($jsonData); ?>"></ul>
                        */ ?>
</div>
</div>

<script type="text/html" id="orgchart-register-form">
    <div class="orgchart-form-popup modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <form class="modal-content orgchart-register-form" onsubmit="return false"
                  data-update-action="<?php echo $nonce_update_orgchart; ?>"
                  data-create-action="<?php echo $nonce_create_orgchart; ?>" action="<?php
            echo esc_attr(admin_url('admin-ajax.php?action=create_orgchart&_wpnonce='));
            ?>" method="POST" enctype="multipart/form-data">
                <input type="hidden" id="orgchart_id" name="orgchart[id]" value="0">
                <input type="hidden" id="orgchart_parent" name="orgchart[parent]" value="0">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"
                        data-edit-title="<?php _e('Edit', TPL_DOMAIN_LANG); ?>"><?php _e('Register', TPL_DOMAIN_LANG); ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group form-error col-lg-12 col-md-12 col-sm-12 hidden"></div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 orgchart_parent">
                            <label class="control-label visible-ie8 visible-ie9"
                                   for="_orgchart_parent"><?php _e('Orgchart Parent', TPL_DOMAIN_LANG); ?></label>
                            <input class="form-control placeholder-no-fix" type="text" autocomplete="off"
                                   placeholder="<?php _e('Orgchart Parent', TPL_DOMAIN_LANG);
                                   ?>" id="_orgchart_parent" value=""/>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 ">
                            <label class="control-label visible-ie8 visible-ie9"
                                   for="orgchart_name"><?php _e('Orgchart Name', TPL_DOMAIN_LANG); ?></label>
                            <input required class="form-control placeholder-no-fix" type="text"
                                   autocomplete="off" placeholder="<?php _e('Orgchart Name', TPL_DOMAIN_LANG);
                            ?>" id="orgchart_name" name="orgchart[name]"/>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 hidden">
                            <label class="control-label visible-ie8 visible-ie9"
                                   for="orgchart_role"><?php _e('Role', TPL_DOMAIN_LANG); ?></label>
                            <select required class="email form-control" id="orgchart_role"
                                    name="orgchart[role]">
                                <option value=""> -- <?php _e('Role', TPL_DOMAIN_LANG); ?> --</option>
                                <option value="BGD"><?php _e('BGD', TPL_DOMAIN_LANG); ?></option>
                                <option value="PHONGBAN"><?php _e('PHONGBAN', TPL_DOMAIN_LANG); ?></option>
                                <option value="NHANVIEN"><?php _e('NHANVIEN', TPL_DOMAIN_LANG); ?></option>
                            </select>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 ">
                            <label class="control-label visible-ie8 visible-ie9"
                                   for="orgchart_description"><?php _e('Description', TPL_DOMAIN_LANG); ?></label>
                            <input required class="form-control placeholder-no-fix" type="text"
                                   autocomplete="off" placeholder="<?php _e('Description', TPL_DOMAIN_LANG);
                            ?>" id="orgchart_description" name="orgchart[description]"/>
                        </div>
                        <div class="form-group orgchart-icon col-lg-12 col-md-12 col-sm-12 ">
                            <div class="preview-avatar col-lg-6 col-md-6 col-sm-12"><img
                                    class="preview-avatar-image"
                                    onerror="this.src = this.getAttribute('data-src');"
                                    src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                    data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                    style="max-width: 213px; height: 1px"></div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="control-label visible-ie8 visible-ie9"
                                       for="orgchart_icon"><?php _e('Icon', TPL_DOMAIN_LANG); ?></label>
                                <div class="file-loading">
                                    <input id="orgchart_icon" name="orgchart[icon]" type="file" class="file"
                                           placeholder="<?php _e('Icon', TPL_DOMAIN_LANG); ?>"
                                           data-show-upload="false">
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 upload-progress">
                            <div class="progress"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal"><?php _e('Close', TPL_DOMAIN_LANG); ?></button>
                    <button type="submit" data-edit-title="<?php esc_attr_e('Update', TPL_DOMAIN_LANG); ?>"
                            class="btn btn-primary"><?php _e('Create', TPL_DOMAIN_LANG); ?></button>
                </div>
            </form><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</script>


<script type="text/html" id="orgchart-edit-form">
    <div class="orgchart-form-popup modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <form class="modal-content orgchart-register-form" onsubmit="return false"
                  action="<?php echo esc_attr(admin_url('admin-ajax.php?action=update_orgchart&_wpnonce=' . $nonce_update_orgchart)); ?>"
                  method="POST" enctype="multipart/form-data">
                <input type="hidden" id="orgchart_id" name="orgchart[id]" value="0">
                <input type="hidden" id="orgchart_parent" name="orgchart[parent]" value="0">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php _e('Edit', TPL_DOMAIN_LANG); ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">

                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                        <label class="control-label visible-ie8 visible-ie9"
                               for="orgchart_name"><?php _e('Name', TPL_DOMAIN_LANG); ?></label>
                        <input required class="form-control placeholder-no-fix" type="text" autocomplete="off"
                               placeholder="<?php _e('Name', TPL_DOMAIN_LANG); ?>" id="orgchart_name"
                               name="orgchart[name]"/>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                        <label class="control-label visible-ie8 visible-ie9"
                               for="orgchart_role"><?php _e('Role', TPL_DOMAIN_LANG); ?></label>
                        <select required class="email form-control" id="orgchart_role" name="orgchart[role]">
                            <option value=""> -- <?php _e('Role', TPL_DOMAIN_LANG); ?> --</option>
                            <option value="BGD"><?php _e('BGD', TPL_DOMAIN_LANG); ?></option>
                            <option value="PHONGBAN"><?php _e('PHONGBAN', TPL_DOMAIN_LANG); ?></option>
                            <option value="NHANVIEN"><?php _e('NHANVIEN', TPL_DOMAIN_LANG); ?></option>
                        </select>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                        <label class="control-label visible-ie8 visible-ie9"
                               for="orgchart_description"><?php _e('Description', TPL_DOMAIN_LANG); ?></label>
                        <input required class="form-control placeholder-no-fix" type="text" autocomplete="off"
                               placeholder="<?php _e('Description', TPL_DOMAIN_LANG); ?>"
                               id="orgchart_description" name="orgchart[description]"/>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                        <div class="preview-avatar"><img
                                class="preview-avatar-image"
                                onerror="this.src = this.getAttribute('data-src');"
                                src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                style="max-width: 213px"></div>
                        <label class="control-label visible-ie8 visible-ie9"
                               for="orgchart_icon"><?php _e('Icon', TPL_DOMAIN_LANG); ?></label>
                        <!-- <input class="form-control placeholder-no-fix" type="file" autocomplete="off" placeholder="<?php _e('Avatar', TPL_DOMAIN_LANG); ?>" id="avatar" name="user_url" /> -->
                        <div class="file-loading">
                            <input id="orgchart_icon" name="orgchart[icon]" type="file" class="file"
                                   placeholder="<?php _e('Icon', TPL_DOMAIN_LANG); ?>" data-show-upload="false">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal"><?php _e('Close', TPL_DOMAIN_LANG); ?></button>
                    <button type="submit"
                            class="btn btn-primary"><?php _e('Create', TPL_DOMAIN_LANG); ?></button>
                </div>
            </form><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</script>


<script type="text/html" id="orgchart-selection-form">
    <div class="orgchart-selection-popup modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php _e('Select Action', TPL_DOMAIN_LANG); ?></h4>
            </div>
            <div class="modal-body">
                <div class="message hidden"><?php _e('Please select "Create New Member" or "Select Member From List"', TPL_DOMAIN_LANG); ?></div>
                <div class="action-type"><label
                        for="type-add"><?php _e('Create New Member', TPL_DOMAIN_LANG); ?></label><input
                        id="type-add" name="actionType" type="radio" value="add"></div>
                <div class="action-type"><label
                        for="type-select"><?php _e('Select Member From List', TPL_DOMAIN_LANG); ?></label><input
                        id="type-select" <input name="actionType" type="radio" value="select"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal"><?php _e('Close', TPL_DOMAIN_LANG); ?></button>
                <button type="button" class="btn btn-primary"><?php _e('OK', TPL_DOMAIN_LANG); ?></button>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</script>


<script type="text/html" id="orgchart-list-form">
    <div class="orgchart-list-popup modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php _e('List Members', TPL_DOMAIN_LANG); ?></h4>
            </div>
            <div class="modal-body">
                <div class="message hidden"><?php _e('Please select a member in list"', TPL_DOMAIN_LANG); ?></div>
                <table cellpadding="2" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="role"><?php _e('Role', TPL_DOMAIN_LANG); ?></th>
                        <th class="id-col"><?php _e('ID', TPL_DOMAIN_LANG); ?></th>
                        <th class="avatar-img"><?php _e('Avatar', TPL_DOMAIN_LANG); ?></th>
                        <th class="name"><?php _e('Name', TPL_DOMAIN_LANG); ?></th>
                        <th class="description"><?php _e('Description', TPL_DOMAIN_LANG); ?></th>
                        <th class="parent"><?php _e('Parent', TPL_DOMAIN_LANG); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="repeater-item">
                        <td class="id-col"><label><input class="id" type="radio" name="node"
                                                         value=""><span></span></label></td>
                        <td class="avatar-img"><img class="avatar"
                                                    onerror="this.src = this.getAttribute('data-src');"
                                                    src="<?php echo THEME_URL . '/assets/images/no-photo.jpg'; ?>"
                                                    data-src="<?php echo THEME_URL . '/assets/images/no-photo.jpg'; ?>"
                                                    style="max-width: 16px; max-height: 16px"></td>
                        <td class="name"></td>
                        <td class="role"></td>
                        <td class="description"></td>
                        <td class="parent"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal"><?php _e('Close', TPL_DOMAIN_LANG); ?></button>
                <button type="submit" class="btn btn-primary"><?php _e('Save', TPL_DOMAIN_LANG); ?></button>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</script>
</div>