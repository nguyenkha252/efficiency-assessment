(function($) {


    function TableOrgChart($element, opts) {
        var self = this;
        self.$element = $element;
        self.$list = $element.children('tbody.list');

        self.options = opts;
        self.url = opts.url;
        self.removeUrl = opts.removeUrl;
        self.xhr = null;
        self.queues = [];
        self.init();
        self.idx = 0;
        return self;
    }

    TableOrgChart.prototype = {
        init: function() {
            var self = this;
            self.$template = self.$list.children('tr.row-template');
            self.$template.remove();
            self.$template.removeClass('hidden');
            self.$orgchartRegisterForm = $($('#orgchart-register-form').html()).filter('.orgchart-form-popup');
            self.$orgchartEditForm = $($('#orgchart-edit-form').html()).filter('.orgchart-form-popup');
            self.indexing();
            self.$list.children('tr').each(function(idx, tr) {
                self.initNode($(tr));
            });
            self.$popup = null;
            self.bindEvents();

            return self;
        },
        indexing: function() {
            var self = this;
            self.$list.children('tr').each(function(idx) {
                $(this).addClass('idx-' + idx);
            });
            self.$list.find('tr > td.director').each(function(idx, dir) {
                var i, $dep, $emp, $dir = $(dir), $tr = $dir.closest('tr');
                var j, empRowspan, isChild, rowspan = Math.max(parseInt($dir.attr('rowspan') || '1', 10), 1);
                $dir.attr('rowspan', rowspan);
                dir._departments = [];
                dir.$tr = $tr;
                for(i=0; i<rowspan; i++) {
                    $dep = $tr.children('td.department');
                    if( $dep.length ) {
                        $dep[0].$tr = $tr;
                        $dep[0].$dir = $dir;
                        if( i > 0 ) {
                            $dep[0].$rowDir = null;
                        } else {
                            $dep[0].$rowDir = $dir;
                        }
                        $dep[0]._employees = [];
                        empRowspan = Math.max(parseInt($dep.attr('rowspan') || '1', 10), 1);
                        $dep.attr('rowspan', empRowspan);
                        dir._departments.push( $dep );
                        isChild = true;
                    } else {
                        $dep = dir._departments[dir._departments.length - 1];
                        empRowspan = Math.max(parseInt($dep.attr('rowspan') || '1', 10), 1);
                        isChild = false;
                    }

                    for(j=0; j<empRowspan; j++) {
                        $emp = $tr.children('td.employee');
                        if( !$emp.length ) {
                            empRowspan --;
                            rowspan --;
                            $dep.attr('rowspan', empRowspan);
                            $dir.attr('rowspan', rowspan);
                            continue;
                        }
                        $emp[0].$rowDir = isChild ? $dep[0].$rowDir : null;
                        isChild = false;
                        if( j > 0 ) {
                            $emp[0].$rowDep = null;
                        } else {
                            $emp[0].$rowDep = $dep;
                        }
                        $emp[0].$tr = $tr;
                        $emp[0].$dep = $dep;
                        $emp[0].$dir = $dir;
                        $dep[0]._employees.push( $emp );
                        i++;
                        $tr = $tr.next('tr');
                    }
                    i--;
                    // $tr = $tr.next('tr');
                }
            });
            return self;
        },
        bindEvents: function() {
            var self = this;
            self.$element.off('click.item', '.control > a[data-event]')
                .on('click.item', '.control > a[data-event]', function(ev) {
                    ev.preventDefault();
                    var eventCall = $(this).data('event');
                    eventCall = eventCall.replace(/[\-]+/g, '-').replace(/\-([a-z])/g, function(m0, m1) {
                        return m1.toUpperCase();
                    });
                    var $newNode = null;
                    if( eventCall.match(/^add/) ) {
                        $newNode = self.$template.clone();
                        self.idx ++;
                        $newNode = self.initNode($newNode);
                        //$newNode.find('.director > .name').html("Director " + self.idx);
                        //$newNode.find('.department > .name').html("Department " + self.idx);
                        //$newNode.find('.employee > .name').html("Employee " + self.idx);
                    }
                    self[eventCall](ev, this, $newNode);
                    self.$list.children('tr').removeClass('first');
                    self.$list.children('tr').removeClass('last');
                    self.$list.children('tr:first').addClass('first');
                    self.$list.children('tr:last').addClass('last');
                    self.indexing();
                    return;
                });

            self.$list.children('tr').removeClass('last');
            self.$list.children('tr:first').addClass('first');
            self.$list.children('tr:last').addClass('last');
            return self;
        },
        offEvents: function() {
            var self = this;
            self.$element.off('click.add', '.control > a[data-event]');
            return self;
        },
        initNode: function($node) {
            $node[0].$director = $node.find('.director > .name');
            
            $node[0].$director_add = $node.find('.director > .control > .add');
            $node[0].$director_edit = $node.find('.director > .control > .edit');
            $node[0].$director_remove = $node.find('.director > .control > .remove');

            $node[0].$department = $node.find('.department > .name');
            $node[0].$department_add = $node.find('.department > .control > .add');
            $node[0].$department_edit = $node.find('.department > .control > .edit');
            $node[0].$department_remove = $node.find('.department > .control > .remove');

            $node[0].$employee = $node.find('.employee > .name');
            $node[0].$employee_add = $node.find('.employee > .control > .add');
            $node[0].$employee_edit = $node.find('.employee > .control > .edit');
            $node[0].$employee_remove = $node.find('.employee > .control > .remove');

            return $node;
        },
        addDirector: function(ev, anchor, $newNode) {
            var self = this, $isTop = $(anchor).closest('thead'), $next = null, $tr = $(anchor).closest('tr');

            $newNode[0].$department.html('');
            $newNode[0].$employee.html('');

            self.popup(self.$orgchartRegisterForm,
                function onShown($popup, $form) {
                    self._registerForm($form, null, 'BGD');
                    return;
                },
                function onError(xhr, status, error, $form) {
                    return;
                },
                function onSuccess(response, status, xhr, $form) {
                    var $icon = $newNode.find('.director img.thumb');
                    if( $icon.length ) {
                        $icon[0].src = response.icon || '';
                    }
                    $newNode.find('.director').data('node', response).attr('data-id', response.id);
                    $newNode[0].$director.html( response.name );
                    // $newNode[0].$director.html( response.name + '<em>('+response.description+')</em>' );

                    $newNode[0].$director_edit.removeClass('hidden');
                    $newNode[0].$director_remove.removeClass('hidden');
                    $newNode[0].$department_add.removeClass('hidden');

                    if( $isTop.length ) {
                        // Append New
                        self.$list.append( $newNode );
                    } else {
                        var $dir = $tr.children('td.director'), $emp = null;
                        if( $dir[0]._departments.length ) {

                            var $dep = $dir[0]._departments.last();
                            if($dep[0]._employees.length) {
                                $emp = $dep[0]._employees.last();
                                $emp[0].$tr.after($newNode);
                            }
                        } else {
                            self.$list.append( $newNode );
                        }
                    }
                    if( ev ) {
                        self.indexing();
                        self.reload();
                    }
                    self.hidePopup();
                    return;
                },
                function onComplete(xhr, status, $form) {
                    return;
                }
            );
            return self;
        },
        editDirector: function(ev, anchor, $newNode) {
            var self = this, $tr = $(anchor).closest('tr'), $dir = $tr.children('td.director');
            var node = $dir.data('node');
            self.popup(self.$orgchartRegisterForm,
                function onShown($popup, $form) {
                    self._editForm($form, node, null, 'BGD');
                    return;
                },
                function onError(xhr, status, error, $form) {
                    return;
                },
                function onSuccess(response, status, xhr, $form) {
                    $dir.data('node', response).attr('data-id', response.id);
                    var $icon = $dir.find('img.thumb');
                    if( $icon.length ) {
                        $icon[0].src = response.icon || '';
                    }
                    $tr[0].$director.html( response.name );
                    // $tr[0].$director.html( response.name + '<em>('+response.description+')</em>' );

                    if( ev ) {
                        self.indexing();
                        self.reload();
                    }
                    self.hidePopup();
                    return;
                },
                function onComplete(xhr, status, $form) {
                    return;
                }
            );
            return self;
        },
        removeDirector: function(ev, anchor, $newNode) {
            var self = this, $tr = $(anchor).closest('tr'), $dir = $tr.children('td.director'), $dep = $tr.children('td.department');
            var $name = $dir.children('.name');
            if( $name.html() == '' ) {
                return self;
            } else {
                var html = $name.html(), node = $dir.data('node'), src = '';
                $name.html('');
                var $icon = $dir.find('img.thumb');
                if( $icon.length ) {
                    src = $icon[0].src;
                    $icon[0].src = '';
                }
                var $dep, i, departments = $dir[0]._departments, nonce = self.$element.attr('data-nonce');

                while(departments.length) {
                    $dep = departments.pop();
                    var $remove = $dep.find('.control > a.remove:not(.hidden)');
                    if( $remove.length ) {
                        self.removeDepartment(null, $remove, null);
                    }
                }

                $dir.find('.control > a.edit, .control > a.remove').addClass('hidden');
                self.queue(
                    {
                        url: self.removeUrl,
                        id: node.id
                    },
                    function _err() {
                        $name.html(html);
                        if( $icon.length ) {
                            $icon[0].src = src;
                        }
                        return;
                    },
                    function _success() {
                        $tr.remove();

                        if( ev ) {
                            self.indexing();
                            self.reload();
                        }
                        return;
                    },
                    function _complete() {
                        return;
                    }
                );
            }
            return self;
        },
        addDepartment: function(ev, anchor, $newNode) {
            var self = this, $tr = $(anchor).closest('tr'), $emp = $tr.children('td.employee'),
                $dep = $tr.children('td.department'), $dir = $tr.children('td.director'),
                $name = $dep.children('.name');

            $newNode.children('td.director').remove();
            $newNode[0].$employee.html('');

            self.popup(self.$orgchartRegisterForm,
                function onShown($popup, $form) {
                    var node = $dep[0].$dir.data('node');
                    self._registerForm($form, node, 'PHONGBAN');
                    return;
                },
                function onError(xhr, status, error, $form) {},
                function onSuccess(response, status, xhr, $form) {
                    if( $name.html() == '' ) {
                    } else {
                        var rowspan = Math.max(parseInt($dep[0].$dir.attr('rowspan') || '1', 10), 1);
                        $dep[0].$dir.attr('rowspan', rowspan + 1);
                        if( $dep[0]._employees.length ) {
                            $emp = $dep[0]._employees[$dep[0]._employees.length-1];
                        }
                        $name = $newNode[0].$department;
                        $emp[0].$tr.after($newNode);
                    }
                    var $icon = $name.prev('img.thumb');
                    if( $icon.length ) {
                        $icon[0].src = response.icon || '';
                    }
                    $name.html( response.name );
                    // $name.html( response.name + '<em>('+response.description+')</em>' );
                    $name.closest('td.department').data('node', response).attr('data-id', response.id);

                    var tr = $name.closest('tr').get(0);
                    tr.$department_edit.removeClass('hidden');
                    tr.$department_remove.removeClass('hidden');
                    tr.$employee_add.removeClass('hidden');
                    if( ev ) {
                        self.indexing();
                        self.reload();
                    }
                    self.hidePopup();
                    return;
                },
                function onComplete(xhr, status, $form) {
                    return;
                }
            );

            return self;
        },
        editDepartment: function(ev, anchor, $newNode) {
            var self = this, $tr = $(anchor).closest('tr'), $dep = $tr.children('td.department');
            var node = $dep.data('node');
            self.popup(self.$orgchartRegisterForm,
                function onShown($popup, $form) {
                    var parent = $dep[0].$dir.data('node');
                    self._editForm($form, node, parent, 'PHONGBAN');
                    return;
                },
                function onError(xhr, status, error, $form) {
                    return;
                },
                function onSuccess(response, status, xhr, $form) {
                    var $name = $tr[0].$department, rowspan = 0, tr = $tr[0], $icon = $name.prev('img.thumb');
                    if( $icon.length ) {
                        $icon[0].src = response.icon || '';
                    }
                    $name.html( response.name );
                    // $name.html( response.name + '<em>('+response.description+')</em>' );
                    $dep.data('node', response).attr('data-id', response.id);

                    if( ev ) {
                        self.indexing();
                        self.reload();
                    }
                    self.hidePopup();
                    return;
                },
                function onComplete(xhr, status, $form) {
                    return;
                }
            );
            return self;
        },
        removeDepartment: function(ev, anchor, $newNode) {
            var self = this, $tr = $(anchor).closest('tr'), $dep = $tr.children('td.department'), $emp = $tr.children('td.employee');
            var $name = $dep.children('.name');
            if( $name.html() == '' ) {
                if( $emp.children('.name').html() == '' ) {
                    if( $dep[0].$rowDir == null ) {
                        var rowspan = Math.max(parseInt($dep[0].$dir.attr('rowspan') || '1', 10), 1);
                        $tr.remove();
                        $emp[0].$dep.attr('rowspan', rowspan - 1);
                    }
                }
                return self;
            } else {
                var html = $name.html(), node = $dep.data('node'), $icon = $name.prev('img.thumb'), src = '';
                var $_emp, employees = $dep[0]._employees, spanNumber = employees.length;
                while( employees.length ) {
                    $_emp = employees.pop();
                    var $remove = $_emp.find('.control > a.remove:not(.hidden)');
                    if( $remove.length ) {
                        self.removeEmployee(null, $remove, null);
                    }
                }
                $name.html('');
                if( $icon.length ) {
                    src = $icon[0].src;
                    $icon[0].src = '';
                }
                $tr[0].$department_edit ? $tr[0].$department_edit.addClass('hidden') : null;
                $tr[0].$department_remove ? $tr[0].$department_remove.addClass('hidden') : null;

                if( $dep[0].$rowDir == null ) {
                    var rowspan = Math.max(parseInt($dep.attr('rowspan') || '1', 10), 1);
                    if( rowspan > 1 ) {
                        $dep.attr('rowspan', Math.max(rowspan - spanNumber, 1));

                        rowspan = Math.max(parseInt($dep[0].$dir.attr('rowspan') || '1', 10), 1);
                        $dep[0].$dir.attr('rowspan', Math.max(rowspan - spanNumber, 1));
                    } else {
                        self.queue(
                            {
                                url: self.removeUrl,
                                id: node.id
                            },
                            function _err() {
                                $name.html(html);
                                if( $icon.length ) {
                                    $icon[0].src = src;
                                }
                                $tr[0].$department_edit.removeClass('hidden');
                                $tr[0].$department_remove.removeClass('hidden');
                                return;
                            },
                            function _success() {
                                $tr.remove();

                                rowspan = Math.max(parseInt($dep[0].$dir.attr('rowspan') || '1', 10), 1);
                                $dep[0].$dir.attr('rowspan', Math.max(rowspan - spanNumber, 1));

                                if( ev ) {
                                    self.indexing();
                                    self.reload();
                                }
                                return;
                            },
                            function _complete() {
                                return;
                            }
                        );
                    }
                } else if( node && node.id ) {
                    self.queue(
                        {
                            url: self.removeUrl,
                            id: node.id
                        },
                        function _err() {
                            $name.html(html);
                            if( $icon.length ) {
                                $icon[0].src = src;
                            }
                            $tr[0].$department_edit.removeClass('hidden');
                            $tr[0].$department_remove.removeClass('hidden');
                            return;
                        },
                        function _success() {
                            $tr.remove();

                            rowspan = Math.max(parseInt($dep[0].$dir.attr('rowspan') || '1', 10), 1);
                            $dep[0].$dir.attr('rowspan', Math.max(rowspan - spanNumber, 1));

                            if( ev ) {
                                self.indexing();
                                self.reload();
                            }
                            return;
                        },
                        function _complete() {
                            return;
                        }
                    );
                }
            }
            return self;
        },
        addEmployee: function(ev, anchor, $newNode) {
            var rowspan, self = this, $tr = $(anchor).closest('tr'), $emp = $tr.children('td.employee'),
                $dep = null, $dir = null, $prev = null, $name = $emp.children('.name');

            $newNode.children('td.director').remove();
            $newNode.children('td.department').remove();

            self.popup(self.$orgchartRegisterForm,
                function onShown($popup, $form) {
                    var node = $emp[0].$dep.data('node');
                    self._registerForm($form, node, 'NHANVIEN');
                    return;
                },
                function onError(xhr, status, error, $form) {
                    return;
                },
                function onSuccess(response, status, xhr, $form) {
                    if( $name.html() == '' ) {
                        // Call Popup add Employee Node
                    } else {
                        $emp[0].$tr.after( $newNode );
                        $name = $newNode[0].$employee;
                        rowspan = Math.max(parseInt($emp[0].$dep.attr('rowspan') || '1', 10), 1);
                        $emp[0].$dep.attr('rowspan', rowspan + 1);

                        rowspan = Math.max(parseInt($emp[0].$dir.attr('rowspan') || '1', 10), 1);
                        $emp[0].$dir.attr('rowspan', rowspan + 1);
                    }
                    var $icon = $name.prev('img.thumb');
                    if( $icon.length ) {
                        $icon[0].src = response.icon || '';
                    }
                    $name.html( response.name );
                    // $name.html( response.name + '<em>('+response.description+')</em>' );
                    $name.closest('td.employee').data('node', response).attr('data-id', response.id);

                    var tr = $name.closest('tr').get(0);
                    tr.$employee_add.removeClass('hidden');
                    tr.$employee_edit.removeClass('hidden');
                    tr.$employee_remove.removeClass('hidden');

                    if( ev ) {
                        self.indexing();
                        self.reload();
                    }
                    self.hidePopup();
                    return;
                },
                function onComplete(xhr, status, $form) {
                    return;
                }
            );
            return self;
        },
        editEmployee: function(ev, anchor, $newNode) {
            var self = this, $tr = $(anchor).closest('tr'), $emp = $(anchor).closest('td.employee'), $dep = null;
            var node = $emp.data('node');
            self.popup(self.$orgchartRegisterForm,
                function onShown($popup, $form) {
                    var parent = $emp[0].$dep.data('node');
                    self._editForm($form, node, parent, 'NHANVIEN');
                    return;
                },
                function onError(xhr, status, error, $form) {
                    return;
                },
                function onSuccess(response, status, xhr, $form) {
                    var $name = $tr[0].$employee, $icon = $name.prev('img.thumb');
                    if( $icon.length ) {
                        $icon[0].src = response.icon || '';
                    }
                    $name.html( response.name );
                    // $name.html( response.name + '<em>('+response.description+')</em>' );
                    $emp.data('node', response).attr('data-id', response.id);

                    if( ev ) {
                        self.indexing();
                        self.reload();
                    }
                    self.hidePopup();
                    return;
                },
                function onComplete(xhr, status, $form) {
                    return;
                }
            );
            return self;
        },
        removeEmployee: function(ev, anchor, $newNode) {
            var self = this, $tr = $(anchor).closest('tr'), $emp = $(anchor).closest('td.employee'),
                $dep = null, src = '', $icon = $emp.find('img.thumb');
            if( !$emp.length ) {
                return self;
            }
            var rowspan, $name = $emp.children('.name');
            if( $name.html() == '' ) {
                return self;
            } else {
                // Call Ajax Remove Employee
                var html = $name.html(), node = $emp.data('node');
                $name.html('');
                if( $icon.length ) {
                    src = $icon[0].src;
                    $icon[0].src = '';
                }
                self.queue(
                    {
                        url: self.removeUrl,
                        id: node.id
                    },
                    function _err() {
                        $name.html(html);
                        if( $icon.length ) {
                            $icon[0].src = src;
                        }
                        return;
                    },
                    function _success() {
                        if( !$emp[0].$rowDep && !$emp[0].$rowDir ) {
                            rowspan = Math.max(parseInt($emp[0].$dep.attr('rowspan') || '1', 10), 1);
                            $emp[0].$dep.attr('rowspan', rowspan - 1);

                            rowspan = Math.max(parseInt($emp[0].$dir.attr('rowspan') || '1', 10), 1);
                            $emp[0].$dir.attr('rowspan', rowspan - 1);

                            $tr.remove();
                        } else {
                            $name.html('');
                            $name.removeData('node');
                            $tr[0].$employee_edit.addClass('hidden');
                            $tr[0].$employee_remove.addClass('hidden');
                        }
                        if( ev ) {
                            self.indexing();
                            self.reload();
                        }
                        return;
                    },
                    function _complete() {
                        return;
                    }
                );
            }
            return self;
        },
        _registerForm: function($form, node, roleName) {
            var self = this, create = $form.attr('data-create-action');
            $form.attr('action', $form.attr('action') + create);
            $form.find('#orgchart_parent').val(node ? node.id : 0);
            $form.find('#_orgchart_parent').val(node ? node.name : '');
            var $role = $form.find('#orgchart_role');
            $role.find('option').each(function(i, op){
                if( op.value != roleName ) {
                    $(op).remove();
                }
            });
            if( roleName == 'BGD' ) {
                $form.find('.form-group.orgchart_parent').addClass('hidden');
            }
            return this;
        },
        _editForm: function($form, node, parent, roleName) {
            var self = this, update = $form.attr('data-update-action');
            var action = $form.attr('action');
            action = action.replace(/\=create_orgchart/, '=update_orgchart') + update;
            $form.attr('action', action);
            var $title = self.$popup.find('.modal-title');
            $title.html( $title.attr('data-edit-title') );
            var $id = $form.find('#orgchart_id');
            var $name = $form.find('#orgchart_name');
            var $description = $form.find('#orgchart_description');
            var $icon = $form.find('.preview-avatar > img');
            var $btn = $form.find('.modal-footer [type="submit"]');
            if( roleName == 'BGD' ) {
                $form.find('.form-group.orgchart_parent').addClass('hidden');
            } else {
                $form.find('#orgchart_parent').val(parent.id);
                $form.find('#_orgchart_parent').val(parent.name);
            }
            $id.val(node.id);
            $name.val(node.name);
            $description.val(node.description);
            if( $icon.length ) {
                $icon[0].src = node.icon || '';
            }
            $btn.html( $btn.attr('data-edit-title') );

            var $role = $form.find('#orgchart_role');
            $role.find('option').each(function(i, op){
                if( op.value != roleName ) {
                    $(op).remove();
                }
            });
            return self;
        },
        reload: function() {
            // location.reload();
            return this;
        },
        destroy: function() {
            var self = this;
            self.offEvents();
            return self;
        },
        queue: function(data, error, success, complete) {
            if( arguments.length > 0 ) {
                this.queues.push({data: data, error: error, success: success, complete: complete});
            }
            if( this.queues.length && this.xhr === null ) {
                var item = this.queues.shift();
                this.request(item.data, item.error, item.success, item.complete);
            }
            return this;
        },
        request: function( data, error, success, complete ) {
            var self = this;
            if( self.xhr ) return this;

            error = error || $.noop;
            success = success || $.noop;
            complete = complete || $.noop;
            var url = self.url;
            if( 'url' in data ) {
                url = data.url;
                delete data.url;
            }
            self.xhr = $.ajax({
                url: url,
                data: data,
                dataType: 'json',
                type: 'post',
                beforeSend: function(xhr, settings) {
                    return;
                },
                error: function(xhr, status, error) {
                    try{
                        error.apply(this, [xhr, status, error]);
                    } catch(e){};
                    return;
                },
                success: function(data, status, xhr) {
                    try{
                        success.apply(this, [data, status, xhr]);
                    } catch(e){};
                    return;
                },
                complete: function(xhr, status) {
                    try{
                        complete.apply(this, [xhr, status]);
                    } catch(e) {};

                    self.xhr = null;
                    setTimeout(function() {
                        self.queue();
                    }, 10);
                    return;
                }
            });
            return self;
        },
        popup: function($element, onShown, error, success, complete) {
            var self = this;
            self.$popup = $element.clone();
            self.$popup.appendTo('body');
            self.$popup.one('hidden.bs.modal', function() {
                var dialog = self.$popup.data('bs.modal');
                dialog ? dialog.removeBackdrop() : null;
                self.$popup.remove();
                self.$popup = null;
                return;
            });
            self.$popup.one('shown.bs.modal', function (eshown) {
                var $form = self.$popup.find('form');
                $form[0].$popup = self.$popup;
                if( $form.length ) {
                    $form.find('.file-loading input[type="file"]').fileinput();
                    self.formSubmit($form, error, success, complete);
                }
                try{
                    onShown(self.$popup, $form);
                } catch(e){};
                return;
            });
            self.$popup.modal('show');
            return self;
        },
        hidePopup: function() {
            if( this.$popup ) {
                this.$popup.modal('hide');
            }
            return this;
        },
        formSubmit: function(form, error, success, complete) {
            var self = this;
            error = error || $.noop;
            success = success || $.noop;
            complete = complete || $.noop;
            $(form).off('submit.form').on('submit.form', function(ev) {
                ev.preventDefault();
                var $progress = $('.upload-progress > .progress', this), $modal = $(this).closest('.modal-dialog');
                $(this).ajaxSubmit({
                    dataType: 'json',
                    // extraData: {},
                    resetForm: false,
                    uploadProgress: function(event, position, total, percent) {
                        $progress.width( '' + percent + '%' );
                        return;
                    },
                    beforeSend: function(xhr, o) {
                        $progress.width( 0 );
                        $modal.addClass('loading');
                        $modal.find('.form-error').addClass('hidden').html('');
                        var dialog = self.$popup.data('bs.modal');
                        dialog ? dialog.handleUpdate() : null;
                        return;
                    },
                    error: function(xhr, status, error, $form) {
                        try{
                            var $err = self.$popup.find('.form-group.form-error');
                            $err.removeClass('hidden');
                            if( ('responseJSON' in xhr) && xhr.responseJSON ) {
                                $err.html('<p class="error-' + xhr.responseJSON.code + '">' + xhr.responseJSON.message + '</p>');
                            } else {
                                $err.html(xhr.status + ' ' + error);
                            }
                            error.apply(this, [xhr, status, error, $form]);
                        } catch(e){};
                        return;
                    },
                    success: function(data, status, xhr, $form) {
                        try{
                            success.apply(this, [data, status, xhr, $form]);
                        } catch(e){};
                        return;
                    },
                    complete: function(xhr, status, $form) {
                        try{
                            $modal.removeClass('loading');
                            complete.apply(this, [xhr, status, $form]);
                        } catch(e){};
                        $form.removeData('jqxhr');
                        return;
                    }
                });
                return;
            });
            return self;
        }
    }

    $.fn.tableOrgChart = function(options) {
        $(this).each(function() {
            var tableOrgChart = $(this).data('tableOrgChart');
            if( !tableOrgChart ) {
                var opts = $.extend({}, $.fn.tableOrgChart.defaults, $(this).data() || {}, options);
                tableOrgChart = new TableOrgChart($(this), opts);
            }
            return tableOrgChart;
        });
        return;
    }

    $.fn.tableOrgChart.defaults = {

    };

    $.fn.tableOrgChart.constructor = TableOrgChart;
    // org-chart-list
})(jQuery);

Array.prototype.last = function() {
    if( this.length ) {
        var item = this.pop();
        this.push(item);
        return item;
    }
    return null;
}