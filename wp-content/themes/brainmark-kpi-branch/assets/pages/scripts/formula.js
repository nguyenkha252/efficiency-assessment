(function($){
    $.fn.formulaKPI = function(options){
        this.each(function(){
            var formula = $(this).data('dataFormula');
            if( !formula ){
                formula = new Formula( this, options );
                $(this).data('dataFormula', formula);
            }
        });
        return this;
    }
    function Formula( element, options ){
        var data = $(this).data() || {};
        this.options = $.extend(data, options || {} ,{});
        this.$element = $(element);
        this.element = element;
        this.init();
    }
    Formula.prototype = {
        init: function(){
            this.cloneMF();
            this.initEvent();
            this.popupNewFormula();
        },
        createConditionMF: function(nameClone, mF){
            var self = this;
            var $str = self[nameClone].clone();
            self[nameClone].idxMF++;
            if( $.isPlainObject(mF) && !$.isEmptyObject(mF) ){
                var type = nameClone.replace(/(table_|)/, '');
                Object.keys(mF).forEach(function(item){
                    var $selector = $str.find('[name="meta_input[' + type + '][IDX][' + item + ']"]');

                        if( $selector.is('select') ){
                            $str.find('[name="meta_input[' + type + '][IDX][' + item + ']"] [value="' + mF[item] + '"]').attr('selected', true);
                        }else{
                            $selector.attr('value', mF[item]);
                        }
                });
            }
            var $names = $str.find('[name*="[IDX]"]');
            $names.each(function(idx, item){
                var name = $(this).attr('name') || '', dataIdx = $(this).attr('data-idx') || '';
                var $tr = $(this).closest('tr[data-idx]');
                name = name.replace( /\[IDX\]/g, '['+self[nameClone].idxMF+']' );
                dataIdx = $tr.attr('data-idx');
                dataIdx = dataIdx.replace( /IDX/g, self[nameClone].idxMF );
                name ? $(this).attr('name', name) : '';
                dataIdx ? $tr.attr('data-idx', dataIdx) : '';
                return;
            });
            $('#'+nameClone).find('tbody.tbody-main').append( $str.html() );
        },
        appendConditionMF: function(nameClone, mF){
            var self = this;
            if( $.isArray( mF ) && mF.length > 0 ){
                $.each(mF, function(idx){
                    self.createConditionMF(nameClone, mF[idx]);
                });
            }else if( $.isPlainObject(mF) && !$.isEmptyObject(mF) ){
                Object.keys(mF).forEach(function(idx){
                    self.createConditionMF(nameClone, mF[idx]);
                });
            }else{
                self.createConditionMF(nameClone);
            }
            $('.selectpicker').selectpicker('refresh');
            return self;
        },
        resetIdx: function(){
            var self = this;
            var $type = $('.group-formula .group-formula-item:first-child', self.$element).find('[name="formula[type]"]');
            $type.prop('checked', true);
            $type.trigger('change.type_formula');
            $.each($('.table-group-detail', self.$element), function(idx, item){
                $(item).find('tbody.tbody-main').html('');
                self[item.id].idxMF = 1;
                self.appendConditionMF( item.id );
            });
            return this;
        },
        bindEventDialog: function(data){
            var self = this;
            var $dialogConfirm = $('#dialog-formula');
            $dialogConfirm.off('hidden.bs.modal').on('hidden.bs.modal', function(ev){
                $dialogConfirm.off('click.dialog.agree', '#dialog-formula .btn-yes');
            });
            $dialogConfirm.off('show.bs.modal').on('show.bs.modal', function(ev){
                $dialogConfirm.find('.notification').remove();
                $dialogConfirm.off('click.dialog.agree', '.btn-yes').on('click.dialog.agree', '.btn-yes', function(event){
                    event.preventDefault();
                    if( !$.isPlainObject(data) && data === undefined ){
                        alert('Vui lòng nhấn nút xóa trước khi hiển thị form này.');
                    }else{
                        $.ajax({
                            url: AJAX_URL,
                            data: {action: data.action, _wpnonce: data.nonce, ID: data.id},
                            type: 'post',
                            dataType: 'json',
                            beforeSend: function () {
                                $dialogConfirm.find('.notification').remove();
                                $('body').block(IMAGE_LOADING);
                            },
                            success: function(response, status, xhr){
                                ///console.log( 'SUCCESS',response, status, xhr );
                                if( $.isPlainObject(response) || 'message' in response ){
                                    window.location.reload();
                                }
                                return;
                            },
                            error: function(xhr, status, errThrow){
                                //console.log( 'ERROR',xhr, status, errThrow );
                                var textError = '';
                                if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                                    textError = xhr.responseJSON.error;
                                }else if( errThrow != '' ){
                                    textError = xhr.getResponseHeader('xhr-message') || errThrow;
                                    try{
                                        textError = JSON.parse(textError);
                                    } catch(e){}
                                }
                                var $err = $('<label class="notification error"> ' + textError + ' </label>');
                                $dialogConfirm.find('.modal-body').prepend( $err );
                                return;
                            },
                            complete: function(xhr, status){
                                //console.log( 'Complete', xhr, status  );
                                //self.xhr_kpi = null;
                                $('body').unblock(IMAGE_LOADING);
                                return;
                            }
                        });
                    }
                    return this;
                });
            });

            $dialogConfirm.modal();
        },
        popupNewFormula: function() {
            var self = this;
            $('.table-formulas a.edit', self.$element).off('click.editFormula').on('click.editFormula', function(ev){
                ev.preventDefault();
                var data = $(this).data();
                var $form = $('#popup-formula', self.$element).find('form');
                var $response = $( '.esponse-container', $form );
                $.ajax({
                    url: AJAX_URL,
                    type: data.method,
                    dataType: 'json',
                    data: {action: data.action, ID: data.id},
                    beforeSend: function(xhr, settings) {
                        $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                        $form.block(IMAGE_LOADING);
                        return;
                    },
                    error: function(xhr, status, error) {
                        if( ('responseJSON' in xhr) && ('message' in xhr.responseJSON) ) {
                            $response.removeClass('hidden').addClass('error').html(xhr.responseJSON.message);
                        } else {
                            error = xhr.getResponseHeader('xhr-message') || error;
                            try{
                                error = JSON.parse(error);
                            } catch(e){}
                            $response.removeClass('hidden').addClass('error').html(error);
                        }
                        return;
                    },
                    success: function(response, status, xhr) {
                        if( $.isPlainObject( response ) && 'data' in response && response.data != '' ) {
                            self.responseFM = response.data;
                            var responseFM = response.data;
                            $('.code-formula', $form).show();
                            Object.keys( responseFM ).forEach(function(item){
                                var $nameFormula = $form.find('[name="formula[' + item + ']"]');
                                if( item == 'unit' ){
                                    $nameFormula.val( responseFM[item] );
                                    $nameFormula.selectpicker('refresh');
                                }else if( item == 'ID' ){
                                    $nameFormula.val( responseFM[item] );
                                    $('#formula_ID').text(responseFM[item]);
                                }else if(item == 'type'){
                                    var $type_formula = $form.find('#radio_' + responseFM.type);
                                    $type_formula.prop('checked', true);
                                    $type_formula.trigger('change.type_formula');
                                }else if( item == 'formulas' ){
                                    var formulas = responseFM[item];
                                    $('#table_' + responseFM.type + ' .tbody-main').html('');
                                    self.appendConditionMF( 'table_' + responseFM.type, formulas );
                                }else{
                                    $nameFormula.val( responseFM[item] );
                                }
                            });
                        }
                        return;
                    },
                    complete: function(xhr, status) {
                        $form.unblock(IMAGE_LOADING);
                        return;
                    }
                });
            });
            $('.table-formulas a.delete', self.$element).off('click.deleteFormula').on('click.deleteFormula', function(ev){
                ev.preventDefault();
                var data = $(this).data();
                self.bindEventDialog( data );
            });
            $('#popup-formula', self.$element).on('show.bs.modal', function(ev) {
                var $form = $('form', $(this)), $btn = $(ev.relatedTarget);
                ('resetForm' in $form) ? $form.resetForm() : null;
                if( $btn.hasClass('edit') ) {
                } else {
                    $('[name="formula[ID]"]', $form).val('');
                }
                if( self.responseFM == '' ){
                    $('#formula_ID', $form).text('');
                    $('.code-formula', $form).hide();
                    self.resetIdx();
                }
                $form[0].chartAjax = null;
                var $response = $('.response-container', $form);
                $('#save-formula', $form).off('click.save.formula').on('click.save.formula', function(ev) {
                    ev.preventDefault();
                    $form.ajaxSubmit({
                        beforeSend: function(xhr, settings) {
                            $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                            $form.block(IMAGE_LOADING);
                            return;
                        },
                        beforeSubmit: function(a, $frm, options) {
                            var valid = true, counter = 0, form = $frm[0], messages = [];
                            $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                            /*if( form.formula[title].value.trim() == '' ) {
                                valid = false;
                                messages.push("Vui lòng nhập vào Tiêu đề công thức");
                            }
                            if( form.formula[plan].value.trim() == '' ) {
                                valid = false;
                                messages.push("Vui lòng nhập vào Kế hoạch");
                            }*/
                            // console.debug(a, $frm);
                            return valid;
                        },
                        error: function(xhr, status, error) {
                            if( ('responseJSON' in xhr) && ('code' in xhr.responseJSON) ) {
                                $response.removeClass('hidden').addClass('error').html(xhr.responseJSON.message);
                            } else {
                                error = xhr.getResponseHeader('xhr-message') || error;
                                try{
                                    error = JSON.parse(error);
                                } catch(e){}
                                $response.removeClass('hidden').addClass('error').html(error);
                            }
                            return;
                        },
                        success: function(response, status, xhr) {
                            $response.removeClass('hidden').addClass('success').html("Lưu thành công");
                            //console.info(response);
                            location.reload();
                            return;
                        },
                        complete: function(xhr, status) {
                            $form.unblock(IMAGE_LOADING);
                            return;
                        }
                    });
                    return;
                });
                return;
            });
            $('#popup-formula', self.$element).on('hidden.bs.modal', function(){
                self.responseFM = '';
            });
            return self;
        },
        cloneMF: function(){
            var self= this;
            var $tableTemplate = $('td.formula-template table.table-group-detail', self.$element);
            $.each( $tableTemplate, function(idx, item){
                self[item.id] = $(item).find('tbody.clone-mf');
                self[item.id].removeClass('display-none');
                self[item.id].remove();
                var $tr = $(item).find('tbody.tbody-main tr');
                self[item.id].idxMF = $tr.length || 0;
            });
        },
        initEvent: function(){
            var self = this;
            self.$element.off('change.type_formula', '.group-formula-item [name="formula[type]"]').on('change.type_formula', '.group-formula-item [name="formula[type]"]', function(ev){
                $('td.formula-template', self.$element).find('.table-group-detail').hide();
                $('td.formula-template', self.$element).find('#table_'+this.value).css('display', 'inline-block');
            });
            self.$element.off('click.repeat.condition.mf', '[data-event="repeat-condition-mf"]').on('click.repeat.condition.mf', '[data-event="repeat-condition-mf"]', function(ev){
                ev.preventDefault();
                var data = $(this).data();
                var nameClone = data.nameClone;
                self.appendConditionMF(nameClone);
            });
        },
    };
    $('.page-formulas').formulaKPI();
}(jQuery));