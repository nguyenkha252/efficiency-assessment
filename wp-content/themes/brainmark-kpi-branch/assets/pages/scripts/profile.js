(function($){
    var $role_id = $('#role_id'), $capbac_id = $('select#capbac_id'), $role_options = $role_id.children('option');
    window.$role_options = $role_options;
    $role_id.children('option').remove();
    $role_id.append( $role_options.filter('[value=""]') );
    $role_id.selectpicker('refresh');
    function updateRoleList(val) {
        $role_id.children('option').remove();
        if( val > '' ) {
            $role_id.append( $role_options.filter('[value=""]') );
            $role_id.append( $role_options.filter('[data-cap-bac="'+val+'"]') );
            $role_id.val('');
        }
        $role_id.selectpicker('refresh');
        var $selected = $role_id.children('option[selected]');
        if( $selected.length ) {
            $role_id.selectpicker('val', $selected.val() );
        }
        return;
    }
    $capbac_id.bind('changed.bs.select', function(ev, clickedIndex, isSelected, state) {
        var val = $capbac_id.val();
        // console.debug(val, clickedIndex, isSelected, state);
        updateRoleList(val);
        return;
    });
    if( location.href.match(/(\?|&)profile=\d+/) ) {
        var counter = 0;
        $(function() {
            var selectpicker = $role_id.data('selectpicker');
            if( !selectpicker ) {
                counter++;
                console.debug("counter " + counter);
                setTimeout(arguments.callee, 100);
                return;
            }
            var val = $capbac_id.val();
            console.debug("Val " + val);
            updateRoleList(val);
            return;
        });
    }
    return;
})(jQuery);