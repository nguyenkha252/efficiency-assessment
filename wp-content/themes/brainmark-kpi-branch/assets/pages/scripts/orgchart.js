(function($){
    var $form = $('.ds-chuc-danh form.chuc-danh');
    if( !$form.length ) return;
    var $btnSubmit = $form.find('[type="submit"]');
    var $list = $('.ds-chuc-danh ul.list-group'),  $btnAdd = $form.find('button.btn.btn-default.add-new'), // $list.find('li.group-item > .add-new'),
        $cap_bac = $($form[0].cap_bac), $chuc_danh = $($form[0].chuc_danh),
        $item = $list.children('.group-item.hidden.item-template'), cap_bac_items = $cap_bac.data('items');
    $item.remove();
    $item.removeClass('hidden');
    $item.removeClass('item-template');
    var $response = $form.find('.response-container');
    $form.bind('submit.save', function (ev) {
        ev.preventDefault();
        $form.ajaxSubmit({
            beforeSubmit: function() {
                return;
            },
            beforeSend: function(xhr, settings) {
                $form.block(IMAGE_LOADING);
                $response.addClass('hidden');
                $response.removeClass('error');
                $response.removeClass('success');
                $response.html('');
                return;
            },
            error: function(xhr, status, error) {
                if( ('responseJSON' in xhr) && xhr.responseJSON && ('message' in xhr.responseJSON) ) {
                    $response.html( xhr.responseJSON.message );
                } else {
                    error = xhr.getResponseHeader('xhr-message') || error;
                    try{
                        error = JSON.parse(error);
                    } catch(e){}
                    $response.html( error );
                }
                $response.removeClass('hidden');
                $response.addClass('error');
                return;
            },
            success: function(response, status, xhr) {
                $form.unblock(IMAGE_LOADING);
                if( xhr.status == 201 || xhr.status == 202 ) {
                    var $li = (xhr.status == 201) ? $item.clone() : $form.data('$activeLi');
                    $response.removeClass('hidden');
                    $response.addClass('success');
                    var node = response;

                    // node.capBacText = $($form[0].cap_bac.options[$form[0].cap_bac.selectedIndex]).text();
                    $li.children('.ten-chuc-danh').html( node.chuc_danh );
                    $li.children('.ten-cap-bac').html( cap_bac_items[response.cap_bac] );
                    if(xhr.status == 201) {
                        $list.append($li);
                    }
                    $li.data('item', node);
                    $btnAdd.trigger('click.add.chuc-danh');
                }
                return;
            },
            complete: function(xhr, settings) {
                $form.unblock(IMAGE_LOADING);
                return;
            },
        });
        return;
    });
    $btnAdd.bind('click.add.chuc-danh', function (ev) {
        ev.preventDefault();
        $form[0].ID.value = '';
        $form[0].chuc_danh.value = '';
        $form[0].cap_bac.value = '';
        $cap_bac.selectpicker( 'refresh' );
        $form.attr('action', $form.data('crateUrl'));
        $form.data('$activeLi', null);
        $btnSubmit.html( $btnSubmit.data('htmlCreate') );
        return;
    });
    $(function() {
        $btnAdd.trigger('click.add.chuc-danh');
    });
    $list.on('click.edit.chuc-danh', 'a.edit-chuc-danh', function(ev) {
        ev.preventDefault();
        var $li = $(this).closest('li.group-item'), node = $li.data('item');
        $form[0].ID.value = node.ID;
        $li.attr('data-id', node.ID);
        $form.data('$activeLi', $li);
        $form[0].chuc_danh.value = node.chuc_danh;
        $form[0].cap_bac.value = node.cap_bac;
        $btnSubmit.html( $btnSubmit.data('htmlSave') );
        $form.attr('action', $form.data('updateUrl'));
        $cap_bac.selectpicker( $cap_bac.data() );
        $cap_bac.selectpicker( 'refresh' );
        return;
    });
    return;
})(jQuery);

// Code for list Popup Users
(function ($) {
    var $userListPopup = $('#tpl-user-list'),
        $users = $userListPopup.find('select[name="users"]'),
        $title = $userListPopup.find('.modal-title'),
        $loading = $userListPopup.find('.modal-dialog'),
        list_users_url = $userListPopup.data('listUrl'),
        add_user_url = $userListPopup.data('addUrl'),
        update_user_url = $userListPopup.data('updateUrl');
    $users.addClass('selectpicker');

    $userListPopup.on('hide.bs.modal.users', function(ev) {
        return;
    });

    $userListPopup.on('show.bs.modal.users', function(evRelatedTarget) {
        var $btn = $(evRelatedTarget.relatedTarget), $node = $btn.closest('.group-chuc_danh-item');

        var $inputTen = $btn.closest('.input-group').find('input[type="text"].form-control'),
            $userID = $btn.closest('.input-group').find('input[type="hidden"].nhan-vien-id'),
            tenChucVu = $btn.data('tenChucVu'), $name = $( tenChucVu, $node ),
            name = $name.length ? $($name[0].options[$name[0].selectedIndex]).text() : '',
            $chuc_danh = $('select.form-control.ten-chuc_danh', $node),
            $cap_bac = $('select.form-control.ten-cap_bac', $node);

        if( $name.val() == '' ) {
            $userListPopup.modal('hide');
            return;
        }
        $title.html( $title.data('title') + name );

        var $options = $users.data('$options'), chuc_danh_id = $chuc_danh.val(),
            chuc_danh = $($chuc_danh[0].options[$chuc_danh[0].selectedIndex]).text(),
            cap_bac = $cap_bac.val();

        $users.children('option').remove();
        $users.append( $options.filter('[value=""]') );

        if( chuc_danh_id > '' ) {
            $options.each(function () {
                var cd = $(this).attr('data-chuc-danh'), cb = $(this).attr('data-cap-bac');
                if( (cb == cap_bac) || (!cd || !cb) ) {
                    $users.append( this );
                    if( cd == chuc_danh ) {
                        $users.val( this );
                    }
                }
                return;
            });
        }
        $users.selectpicker( $users.data() );
        $users.selectpicker('refresh');
        $users.triggerNative('change');

        $userListPopup.off('click.ok', '.btn-primaryy')
            .on('click.ok', '.btn-primaryy', function(ev) {
                var uid = $users.val(), old_id = $userID.val();
                $userListPopup.modal('hide');
                if( uid == '' ) {
                    url = update_user_url;
                    $inputTen.val( '' );
                    $userID.val( '' );
                } else {
                    $inputTen.val( $($users[0].options[$users[0].selectedIndex]).text() );
                    $userID.val( uid );
                }
                console.debug({uid: uid, $userID: $userID, $userID: $userID, $inputTen: $inputTen});
                return;
            });
        return;
    });
})(jQuery);

// Code for add new node Chart
(function ($) {
    var $userRolePopup = $('#tpl-form-user-role');
    if( !$userRolePopup.length ) return;
    var $form = $userRolePopup.find('form'), formRole = $form[0],
        $users = $userRolePopup.find('select[name="users"]'),
        $title = $userRolePopup.find('.modal-title'),
        $loading = $userRolePopup.find('.modal-dialog'),
        list_users_url = $form.data('listUrl'),
        add_charts_url = $form.data('addUrl'),
        update_charts_url = $form.data('updateUrl'),
        $popupUsers = $('#tpl-user-list select#users_id.form-control');

    $userRolePopup.on('changed.bs.select.cap_bac', 'select.ten-cap_bac.form-control', function() {
        var $options, $capbac = $(this), $chuc_danh = $userRolePopup.find( $capbac.data('changeTarget') ), cap_bac = $capbac.val();
        $options = $chuc_danh.data('$options');
        // console.debug({'chuc_danh': $chuc_danh});
        $chuc_danh.children('option').remove();
        if( cap_bac > '' ) {
            $chuc_danh.append( $options.filter('[value=""]') );
            $options.each(function() {
                var cb = $(this).attr('data-cap-bac');
                if( !cb || (cb == cap_bac) ) {
                    $chuc_danh.append( this );
                }
                return;
            });
        }
        var $selected = $chuc_danh.children('option[selected]');
        if( $selected.length ) {
            $chuc_danh.selectpicker('val', $selected.val() );
            $chuc_danh.selectpicker('refresh');
        } else {
            $chuc_danh.selectpicker('refresh');
        }

        $chuc_danh.triggerNative('change');
        return;
    });

    $userRolePopup.on('changed.bs.select.chuc_danh', 'select.ten-chuc_danh.form-control', function() {
        var $chuc_danh = $(this), targetName = $chuc_danh.data('changeTarget'), targetId = targetName.replace(/_name$/, ''),
            $uname = $userRolePopup.find(targetName), $uid = $userRolePopup.find(targetId), chuc_danh = $chuc_danh.val(),
            $node = $chuc_danh.closest('.group-chuc_danh-item'),
            $cap_bac = $('select.form-control.ten-cap_bac', $node);
        $uname.val( '' );
        $uid.val( '' );
        var $node = $chuc_danh.closest('.group-chuc_danh-item'), $toggle = $node.find('a[data-toggle="modal"]'),
            nodeInfo = $node.is('li') ? $node.data('node') : null;
        $toggle.data();
        var $options = $popupUsers.data('$options'), chuc_danh_id = $chuc_danh.val(),
            chuc_danh = $($chuc_danh[0].options[$chuc_danh[0].selectedIndex]).text(),
            cap_bac = $cap_bac.val();
        console.debug({cap_bac: cap_bac, chuc_danh: chuc_danh, nodeInfo: nodeInfo});
        if( nodeInfo ) {
            $chuc_danh.selectpicker('val', nodeInfo.ID);
            $uname.val( nodeInfo.display_name );
            $uid.val( nodeInfo.user_id );
        } else {
            $options.each(function () {
                var cd = $(this).attr('data-chuc-danh'), cb = $(this).attr('data-cap-bac');
                if (cb == cap_bac && cd == chuc_danh) {
                    $uname.val($(this).text());
                    $uid.val($(this).val());
                }
                return;
            });
        }
        return;
    });

    var $groupChucVuPho = $userRolePopup.find('.group-chuc_danh-pho');

    var $chucVuPho = $groupChucVuPho.children('li.item-template'), $div = $('<div></div>');
    $chucVuPho.removeClass('item-template');
    $chucVuPho.remove();
    $div.append($chucVuPho);
    var chucVuPho = $div.html();

    $('#tpl-form-user-role select.ten-cap_bac.form-control, '+
        '#tpl-form-user-role select.ten-chuc_danh.form-control, ' +
        '#tpl-user-list select#users_id.form-control'
    ).each(function() {
        this.$options = $(this).children('option');
        $(this).data('$options', this.$options);
        // console.debug({className: this.className, options: this.$options, select: this});
        return;
    });

    $userRolePopup.on('hide.bs.modal.role', function(ev) {
        return;
    });

    $userRolePopup.on('cancel.bs.modal.confirm', 'a[data-target="#confirm-dialog"]', function(ev, $confirm) {
        ev.preventDefault();
        return;
    });

    $userRolePopup.on('ok.bs.modal.confirm', 'a[data-target="#confirm-dialog"]', function(ev, $confirm) {
        ev.preventDefault();
        $(this).closest('li.group-item').remove();
        return;
    });

    var CHUC_VU_PHO = 0;
    var $themChucVuPho = $('a.btn-add-chu-vu-pho', $userRolePopup);
    $themChucVuPho.unbind('click.role.chucvupho')
        .bind('click.role.chucvupho', function (ev) {
            var node = $themChucVuPho.data('node');
            themChucVuPho(node);
            return;
        });

    function themChucVuPho(node) {
        CHUC_VU_PHO ++;
        var $html = $(chucVuPho.replace(/IDX/g, ('' + CHUC_VU_PHO) ));
        $groupChucVuPho.append( $html );
        var $select = $html.find('select[data-url].ten-chuc_danh');
        $select.data('IDX', CHUC_VU_PHO);
        var $cap_bac = $html.find('select.form-control.ten-cap_bac');

        var $li = $select.closest('li.group-item'), idx = $select.data('IDX');
        $li.data('node', node);
        if( node ) {
            $li.find('[name="chuc_danh_pho\['+idx+'\]\[ID\]"]').val(node.ID);
            $li.find('[name="chuc_danh_pho\['+idx+'\]\[ten_old\]"]').val(node.chuc_danh);
            $li.find('[name="chuc_danh_pho\['+idx+'\]\[user\]"]').val(node.display_name);
            $li.find('[name="chuc_danh_pho\['+idx+'\]\[user_id\]"]').val(node.user_id);
            $li.find('[name="chuc_danh_pho\['+idx+'\]\[user_id_old\]"]').val(node.user_id);
            $cap_bac.val( node.cap_bac );
            $select.val( node.role_id );
        }

        $cap_bac[0].$options = $cap_bac.children('option');
        $cap_bac.data('$options', $cap_bac[0].$options);

        $select[0].$options = $select.children('option');
        $select.data('$options', $select[0].$options);

        $cap_bac.addClass('selectpicker');
        $cap_bac.selectpicker( $cap_bac.data() );

        $select.addClass('selectpicker');
        $select.selectpicker( $select.data() );

        $cap_bac.triggerNative('change');
        return $select;
    }

    function line_to($from, $to) {
        var fromInfo = $.extend({}, $from.position(), {
            width: $from.width(),
            height: $from.width()
        }), toInfo = $.extend({}, $to.position(), {
            width: $to.width(),
            height: $to.width()
        });
        var $lineFrom = $from.data('$line');//, $lineTo = $to.data('$line');

        $lineFrom
            .attr('x1', Math.floor(fromInfo.left + fromInfo.width/2) ).attr('y1', Math.floor(fromInfo.top + fromInfo.height))
            .attr('x2', Math.floor(toInfo.left + toInfo.width/2) ).attr('y2', Math.floor(toInfo.top));
        return;
    }
    function update_dragable() {
        var $root = $('#orgchart-root'), url = $root.data('url'),
            $nodes = $root.find('li.node > div.node-info'),
            $svg = $('.ds-orgchart-line'), line = '<line stroke="red" x1="0" y1="0" x2="0" y2="0"></line>';

        $nodes.each(function(idx, div) {
            var $div = $(this), $list = $div.closest('ul.node-children'),
            $li = $(this).closest('li.node'), nodeInfo = $li.data('node');

            div.$li = $li;
            div.nodeInfo = nodeInfo;
            div.$line = $(line);
            $svg.append(div.$line);
            $div.data('$line', div.$line);
            if( $list.length ) {
                var $parentNode = $list.closest('li.node').children('div.node-info');
                div.$parentNode = $parentNode;
                $div.data('$parentNode', $parentNode);
                line_to($div, $parentNode);
            } else {
                div.$parentNode = null;
                $div.data('$parentNode', null);
            }
            console.debug(div);
            return;
        });
        $nodes.draggable({
            handle: '.group-ten-to-chuc > .name',
            start: function(event, ui) {
                var div = this;
                return;
            },
            drag: function(event, ui) {
                line_to($(this), this.$parentNode);
                console.debug({line_element: this, $parentNode: this.$parentNode});
                return;
            },
            stop: function(event, ui) {
                var div = this; //, $li = $(this).closest('li.node'), nodeInfo = $li.data('node');
                console.debug({element: this, $li: div.$li, event: event, ui: ui, nodeInfo: div.nodeInfo});
                if( ('xhr' in div) && div.xhr ) {
                    div.xhr.abort("Cancel update");
                }
                div.xhr = $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'json',
                    data: {
                        ID: div.nodeInfo.ID,
                        x: ui.position.left,
                        y: ui.position.top
                    },
                    error: function(xhr, status, error) {},
                    success: function(response, status, xhr) {
                        div.nodeInfo.y = ui.position.top;
                        div.nodeInfo.x = ui.position.left;
                        return;
                    },
                    complete: function(xhr, status) {
                        div.xhr = null;
                    }
                });
                return;
            }
        });
    }
    update_dragable();
    $userRolePopup.on('shown.bs.modal.role', function(evRelatedTarget) {
        var $btn = $(evRelatedTarget.relatedTarget), $node = $btn.closest('li.node[data-node]'),
            $inputTen = $btn.closest('.input-group').find('input[type="text"].form-control'),
            $userID = $btn.closest('.input-group').find('input[type="hidden"].nhan-vien-id'),
            $info = $btn.closest('.node-info'), tenChucVu = $btn.data('tenChucVu'),
            $groupName = $btn.parent().parent(), nodeInfo = $node.data('node'), // '.group-chuc_danh-truong'
            $name = $( tenChucVu, $groupName ), name = $name.val() || '',
            nodeId = $node.data('nodeId'), oldId = $userRolePopup.data('nodeId');

        if( $btn.data('action') == 'edit' ) {
            if( oldId === nodeId ) {
                return;
            }
            $userRolePopup.data('nodeId', nodeId);
        } else {
            $userRolePopup.data('nodeId', null);
        }

        $groupChucVuPho.children('.input-group.group-item').remove();

        CHUC_VU_PHO = $groupChucVuPho.children().length + 1;

        var $btnOk = $userRolePopup.find('.btn-primary'),
            $cap_bac_truong = $('#cap_bac_truong', $form),
            $chuc_danh_truong = $('#chuc_danh_truong', $form);

        $title.html( $title.data('title') + name );
        $chuc_danh_truong.append( $chuc_danh_truong.data('$options') );
        if( $btn.data('action') == 'new' ) {
            $form.attr('action', add_charts_url);
            $btnOk.html( $btnOk.attr('data-create') );
            $themChucVuPho.data('node', null);
            formRole.ID.value = '';
            formRole.parent.value = $node.length ? nodeInfo.ID : 0;
            formRole.level.value = $node.length ? (parseInt($node.data('level')) + 1) : 1;
            formRole.ten.value = '';
            formRole.chuc_danh_truong_old.value = '';
            formRole.ten_chuc_danh_truong_name.value = '';
            formRole.ten_chuc_danh_truong.value = '';
            formRole.assign_task.value = 0;
            $(formRole.assign_task).prop('checked', false);
            $chuc_danh_truong.attr('data-old-value', '');
            $chuc_danh_truong.val('');
            $cap_bac_truong.val('');
        } else {
            $form.attr('action', update_charts_url);
            $btnOk.html( $btnOk.attr('data-update') );
            $themChucVuPho.data('node', nodeInfo);
            console.log( nodeInfo );
            formRole.ID.value = nodeId;

            formRole.parent.value = nodeInfo.parent;
            formRole.level.value = $node.data('level');
            formRole.ten.value = nodeInfo.name;
            formRole.chuc_danh_truong_old.value = nodeInfo.role_id;
            formRole.ten_chuc_danh_truong_name.value = nodeInfo.display_name;
            formRole.ten_chuc_danh_truong.value = nodeInfo.user_id;
            formRole.assign_task.value = nodeInfo.assign_task;
            $(formRole.assign_task).prop('checked', nodeInfo.assign_task ? true : false);
            $chuc_danh_truong.attr('data-old-value', nodeInfo.role_id);
            $chuc_danh_truong.val(nodeInfo.role_id);
            $cap_bac_truong.val(nodeInfo.cap_bac);

            if( ('siblings' in nodeInfo) && $.isPlainObject(nodeInfo.siblings) ) {
                Object.keys(nodeInfo.siblings).forEach(function(id) {
                    themChucVuPho(nodeInfo.siblings[id]);
                });
            }
        }
        $cap_bac_truong.addClass('selectpicker');
        $cap_bac_truong.selectpicker( $cap_bac_truong.data() );

        $chuc_danh_truong.addClass('selectpicker');
        $chuc_danh_truong.selectpicker( $chuc_danh_truong.data() );

        $cap_bac_truong.triggerNative('change');
        $chuc_danh_truong.triggerNative('change');

        return;
    });

    $form.on('submit.save.role', function(ev){
        ev.preventDefault();
        $form.ajaxSubmit({
            beforeSend: function(xhr, settings) {
                $form.block(IMAGE_LOADING);
                return;
            },
            error: function(xhr, status, error) {
                return;
            },
            success: function(response, status, xhr) {
                $form.unblock(IMAGE_LOADING);
                if( xhr.status == 201 ) {
                    // Render form
                    $userRolePopup.modal('hide');
                    location.reload();
                }
                return;
            },
            complete: function(xhr, status) {
                $form.unblock(IMAGE_LOADING);
                return;
            }
        });
        return;
    });
})(jQuery);

(function ($) {

    $('#confirm-dialog').bind('shown.bs.modal.confirm', function(ev) {
        var $related = $(ev.relatedTarget), $this = $(this);
        $related.trigger('show.bs.modal.confirm', $this);
        var modal = $this.data('bs.modal');
        $this.addClass('confirm-top')
        modal.$backdrop ? modal.$backdrop.addClass('backdrop-confirm') : null;
        console.debug(modal);

        $this.find('.modal-body .msg-content').html( $related.data('title') || 'Bạn có chắc chắn?' );
        $this.find('.btn.btn-primary').unbind('click.ok.confirm').bind('click.ok.confirm', function(ev) {
            ev.preventDefault();
            $this.modal('hide');
            $related.trigger('ok.bs.modal.confirm', $this);
        });
        $this.find('.btn.btn-default[data-dismiss="modal"]').unbind('click.cancel.confirm').bind('click.cancel.confirm', function(ev) {
            ev.preventDefault();
            $related.trigger('cancel.bs.modal.confirm', $this);
        });
        return;
    });
})(jQuery);
