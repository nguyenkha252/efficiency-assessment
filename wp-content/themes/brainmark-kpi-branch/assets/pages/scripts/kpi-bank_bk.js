jQuery(function($){

    function BrainmarkRepeater(element, options){
        var dataSet = $(this).data()|| {};
        this.options = $.extend({}, dataSet, options || {});
        this.$element = $(element);
        this.element = element;
        this.init();
    }

    BrainmarkRepeater.prototype = {
        init: function(){
            var self = this, options = this.options;

            self.$formAddGroupTarget = $('form[data-add-group-event]');
            self.$popup = $('#popup-kpi-bank');
            self.idxGroup = self.$element.find('thead[data-idx-group]').length || 0;
            self.idxGroupItem = 0;
            this.inited = true;
            self.$listHead = self.$element.find(' > .brainmark-table thead');
            self.$listFoot = self.$element.find(' > .brainmark-table tfoot');
            self.$listBody = self.$element.find(' > .brainmark-table tbody');
            self.$cloneableGroupItem = self.$element.find('tr[data-id="brainmarkcloneitemindex"]');
            self.cloneRowGroup();
            /*self.$listHead.on('click.repeater.item', 'a[data-event]', function(ev){
                ev.preventDefault();
            });*/
            self.$listFoot.on('click.show.repeater.item', 'a[data-show]', function(ev){
                // Add group
                ev.preventDefault();
                var $next = $(this).closest('tr').next();
                $next.toggle();
                //var $tfoot = $(this).closest('tfoot.row-tfoot-group');
                //var $str = self.cloneRowGroup();
                //$tfoot.append( $str );
                return this;
            });
            self.initForm();
            self.eventAddRowGroup();
            self.eventRowItem();
            self.cloneButtonDelItem();
            self.cloneMF();
            self.eventRepeaterMF();
            self.selectorBootstrap();
        },
        cloneButtonDelItem: function(){
            var self = this;
            self.$btnDelItem = self.$popup.find('button[data-event="del-item"]');
            self.removeButtonDelItem();
        },
        removeButtonDelItem: function(){
            this.$btnDelItem.remove();
        },
        appendButtonDelItem: function(){
            var self = this;
            var $closePopup = $('.btn-brb-default.btn-cancel.close-popup');
            self.$btnDelItem.insertAfter( $closePopup );
        },
        /*createTemplateGroup: function( $str ){
            var self = this;
            var text = $str.toString();
            var $thead = $('<thead class="row-thead-title-group"></thead>');
            var $tr = $('<tr></tr>');
            var $th_1 = $('<th class="row-group-title group-numerical-order"> ' + self.idxGroup + '</th>');
            var $th_2 = $('<th colspan="4" class="row-group-title group-title">' + text + '</th>');
            var $th_3 = $('<th colspan="3" class="row-group-title action-add-row-target action-add-row-group">' +
                '<a href="javascript:;" data-event="add-row">Add target <i class="fa fa-plus-circle" aria-hidden="true"></i></a></th>');
        },*/
        submitFormAjax: function($form, validator, ev){
            var self = this;
            ev.preventDefault();
            /*if( ('xhr_kpi' in self) && self.xhr_kpi !== null ) {
                self.xhr_kpi.abort("Cancel and Rerquest again");
            }*/
            self.xhr_kpi = {
                dataType: 'json',
                beforeSubmit: function(serialize, form, option) {
                    $form.find('.notification').remove();
                    return true;
                },
                beforeSend: function () {
                    $form.block(IMAGE_LOADING);
                },
                success: function(response, status, xhr){
                    console.log( 'SUCCESS',response, status, xhr );
                    if( $.isPlainObject(response) || 'success' in response ){
                        window.location.reload();
                    }
                    return;
                },
                error: function(xhr, status, errThrow){
                    console.log( 'ERROR',xhr, status, errThrow );
                    //window.location.reload();
                    var textError = '';
                    if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                        textError = xhr.responseJSON.error;
                    }else if( errThrow != '' ){
                        textError = errThrow;
                    }
                    var $err = $('<label class="notification error"> ' + textError + ' </label>');
                    $form.find('.notification').remove();
                    $form.prepend( $err );
                    return;
                },
                complete: function(xhr, status){
                    //console.log( 'Complete', xhr, status  );
                    self.xhr_kpi = null;
                    $form.unblock(IMAGE_LOADING);
                    return;
                }
            };
            $form.ajaxSubmit(self.xhr_kpi);
            return false;
        },
        formValidate: function(frm){
            var self = this;
            var $frm = $(frm);
            var $required = $([].slice.call(frm.elements)).filter('[required]');
            var validationRules = {
                // Specify validation rules
                //onsubmit: callajax ? false : true,
                focusCleanup: true,
                errorClass: 'error',
                validClass: 'valid',
                rules: {},
                // Specify validation error messages
                messages: {},
                errorElement: "em",
                errorPlacement: function ( error, $element ) {
                    // Add the `help-block` class to the error element
                    error.addClass( "help-block" );
                    var data = $element.data();
                    var errText = data.errRequired || 'Please input value for ' + $element.attr('placeholder');
                    error.text( errText );
                    if('$label' in $element[0]) {
                        error.insertAfter( $element[0].$label );
                    } else if ( $element.prop( "type" ) === "checkbox" ) {
                        error.insertAfter( $element.parent( "label" ) );
                    } else {
                        error.insertAfter( $element );
                    }
                    return false;
                },
                invalidHandler: function(ev) {
                    var validator = $frm.data('validator');
                    var elements = validator.invalidElements();
                    if( validator.errorList.length == 0 ) {
                        validator.errorList = [];
                        self.initFormValidate();
                        $frm.trigger('submit.validate');
                        return;
                    }
                    return;
                },
                submitHandler: function(form, ev) {
                    var $form = $(form);
                    var validator = this;
                    return self.submitFormAjax($form, validator, ev);
                }
            };
            $required.each(function(idx, elem) {
                validationRules.rules[elem.id] = 'required';
                try {
                    var $label = $frm.find('label[for="'+elem.id+'"]');
                    elem.$label = $label;
                    $label = $label.clone();
                    $label.find('span').remove();
                    var field = $label.text().trim();
                    var data = $(elem).data() || {};
                    validationRules.messages[elem.id] = {
                        required: data.errRequired || 'Please input value for '+ field
                    }
                }catch(e) {
                }
                return;
            });
            var validator = $frm.data('validator');
            if( validator ) {
                $frm
                    .off( ".validate" )
                    .removeData( "validator" )
                    .find( ".validate-equalTo-blur" )
                    .off( ".validate-equalTo" )
                    .removeClass( "validate-equalTo-blur" );
            }
            $frm.validate(validationRules);
        },
        initFormValidate: function(){
            var self = this;
            self.$formAddGroupTarget.each(function(index, form){
                self.formValidate( form );
            });

            return;
        },
        initForm: function(){
            var self = this;
            self.initFormValidate();
            return self;
        },
        eventAddRowGroup: function(){
            var self = this;
            self.$listFoot.on('click.add_repeater.item', 'a[data-event="add-group-row"]', function(ev){
                // Add group
                ev.preventDefault();
                var $textInput = $(this).prev();
                var text = $textInput.val();
                //console.log( self.$cloneableGroup );
                self.addRowGroup.call( self, text );
                return this;
            });

            $(document).off('click.submitGroup', '.btn-add-group-target, #save-target-item').on('click.submitGroup', '.btn-add-group-target, #save-target-item', function(ev){
                ev.preventDefault();
                var $form = $(this).closest('form');
                $form.trigger('submit.validate');
                return false;
            });

        },
        eventRowItem: function(){
            var self = this;
            $(document).off('click.add_row_item_in_group', 'a[data-event="add-row-item"]').on('click.add_row_item_in_group', 'a[data-event="add-row-item"]', function(ev){
                ev.preventDefault();
                self.removeButtonDelItem();
                self.resetIdx();
                var data = $(this).data();
                var parent = 0;
                if( 'parent' in data ){
                    parent = data.parent;
                }
                $('.code-kpi', self.$popup).hide();
                $('input[name="post_parent"]', self.$popup).val( parent );
                self.$popup.modal();
            });
            $(document).off('click.edit_row_item', '.brainmark-kpi-target-item td:not(.item-action-on-off)').on('click.edit_row_item','.brainmark-kpi-target-item td:not(.item-action-on-off)', function(ev){
                var $tr = $(this).closest('tr');
                var data = $tr.data();
                self.appendButtonDelItem();
                self.resetIdx();
                if( ('xhr_kpi' in self) && self.xhr_kpi !== null ) {
                    self.xhr_kpi.abort("Cancel and Rerquest again");
                }
                self.xhr_kpi = $.ajax({
                    url: AJAX_URL,
                    data: {action: 'load_target_item', id: data.id},
                    dataType: 'json',
                    type: 'get',
                    beforeSend: function () {
                        self.$popup.find('.notification').remove();
                        self.$element.block(IMAGE_LOADING);
                    },
                    success: function(response, status, xhr){
                        ///console.log( 'SUCCESS',response, status, xhr );
                        if( $.isPlainObject(response) || 'data' in response ){
                            var resData = response.data;
                            Object.keys(resData).forEach( function(item){
                                var key = item.replace(/(\[\])/g, '\$1' );
                                var resValue = resData[item];
                                if( item == 'tax_input[types_kpi]' ){
                                    self.$popup.find('[name="' + key + '"]').val( resValue );
                                    self.$popup.find('[name="' + key + '"]').selectpicker('refresh')
                                }else if(item == 'measurement_formulas'){
                                    self.appendConditionMF( resValue );
                                }else if( item == 'post_id' ){
                                    self.$popup.find('[data-event="del-item"]').attr('data-id', resValue);
                                    self.$popup.find('[name="' + key + '"]').val( resValue );
                                    self.$popup.find('.code-post').text( resValue );
                                    self.$popup.find('.code-kpi').show();
                                }else if( item == 'post_status' ){
                                    var checked = ( resValue === 'checked' ) ? true : false;
                                    self.$popup.find('[name="' + key + '"]').attr('checked', checked);
                                }else{
                                    self.$popup.find('[name="' + key + '"]').val( resValue );
                                }

                            });
                            self.$popup.modal();
                        }
                        return;
                    },
                    error: function(xhr, status, errThrow){
                        console.log( 'ERROR',xhr, status, errThrow );
                        //window.location.reload();
                        var textError = '';
                        if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                            textError = xhr.responseJSON.error;
                        }else if( errThrow != '' ){
                            textError = errThrow;
                        }
                        var $err = $('<label class="notification error"> ' + textError + ' </label>');
                        self.$popup.find('form').prepend( $err );
                        return;
                    },
                    complete: function(xhr, status){
                        //console.log( 'Complete', xhr, status  );
                        self.xhr_kpi = null;
                        self.$element.unblock(IMAGE_LOADING);
                        return;
                    }
                });
            });

            $(document).off('click.close_popup', 'button.close-popup').on('click.close_popup', 'button.close-popup', function(ev){
                ev.preventDefault();
                $('[data-dismiss="modal"]').trigger('click');
            });

            // remove group
            $(document).off('click.del_row_item_in_group', 'a[data-event="del-row-group"]').on('click.del_row_item_in_group', 'a[data-event="del-row-group"]', function(ev){
                ev.preventDefault();
                var $theadParent = $(this).closest('thead');
                var datagroup = $theadParent.attr('data-group');
                var data = $(this).data();
                self.bindEventDialog( data );
                /*self.$element.find('[data-group="'+datagroup+'"]').remove();
                self.idxGroup --;
                $.each( self.$element.find('thead[data-idx-group]'), function(idx, item){
                    $(item).find('.row-group-title.group-numerical-order').text(idx + 1);
                });*/
            });
            $(document).off('click.remove.row_item', '[data-event="del-item"]').on('click.remove.row_item', '[data-event="del-item"]', function(ev){
                ev.preventDefault();
                var data = $(this).data();
                self.bindEventDialog(data);
            });
            //change status
            $(document).off('change.change.status', '.item-action-on-off .checkbox-status').on('change.change.status', '.item-action-on-off .checkbox-status', function(ev){
                ev.preventDefault();
                var data = $(this).data();
                var $parentTD = $(this).closest('td');
                var $parentTR = $(this).closest('tr');
                var status = $(this).prop('checked');
                status = status ? $(this).val() : '';
                $.ajax({
                    url: AJAX_URL,
                    data: {action: 'change_status', post_id: data.id, post_status: status},
                    dataType: 'json',
                    type: 'post',
                    beforeSend: function () {
                        $parentTR.block(IMAGE_LOADING);
                        $parentTD.find('.notification').remove();
                    },
                    success: function(response, status, xhr){
                        ///console.log( 'SUCCESS',response, status, xhr );
                        if( $.isPlainObject(response) || 'success' in response ){
                            //window.location.reload();
                            var $notification = $('<span class="notification success">' + response.success + '</span>');
                            $parentTD.append( $notification );
                            var notifiTimeout;
                            if( notifiTimeout !== undefined ){
                                clearTimeout( notifiTimeout );
                            }
                            notifiTimeout = setTimeout(function(){
                                $parentTD.find('.notification').remove();
                            }, 3000);
                        }
                        return;
                    },
                    error: function(xhr, status, errThrow){
                        //console.log( 'ERROR',xhr, status, errThrow );
                        var textError = '';
                        if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                            textError = xhr.responseJSON.error;
                        }else if( errThrow != '' ){
                            textError = errThrow;
                        }
                        var $err = $('<span class="notification error"> ' + textError + ' </span>');
                        $parentTD.append( $err );
                        return;
                    },
                    complete: function(xhr, status){
                        //console.log( 'Complete', xhr, status  );
                        $parentTR.unblock(IMAGE_LOADING);
                        return;
                    }
                });
                return this;
            });

        },
        bindEventDialog: function(data){
            var $dialogConfirm = $('#myDialogConfirm');
            $dialogConfirm.off('hidden.bs.modal').on('hidden.bs.modal', function(ev){
                $dialogConfirm.off('click.dialog.agree', '#myDialogConfirm .btn-yes');
            });
            $dialogConfirm.off('show.bs.modal').on('show.bs.modal', function(ev){
                $dialogConfirm.find('.notification').remove();
                $dialogConfirm.off('click.dialog.agree', '.btn-yes').on('click.dialog.agree', '.btn-yes', function(event){
                    event.preventDefault();
                    if( !$.isPlainObject(data) && data === undefined ){
                        alert('Vui lòng nhấn nút xóa trước khi hiển thị form này.');
                    }else{
                        $.ajax({
                            url: AJAX_URL,
                            data: {action: data.action, _wpnonce: data.nonce, ID: data.id},
                            type: 'post',
                            dataType: 'json',
                            beforeSend: function () {
                                $dialogConfirm.find('.notification').remove();
                                $('body').block(IMAGE_LOADING);
                            },
                            success: function(response, status, xhr){
                                ///console.log( 'SUCCESS',response, status, xhr );
                                if( $.isPlainObject(response) || 'success' in response ){
                                    window.location.reload();
                                }
                                return;
                            },
                            error: function(xhr, status, errThrow){
                                //console.log( 'ERROR',xhr, status, errThrow );
                                var textError = '';
                                if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                                    textError = xhr.responseJSON.error;
                                }else if( errThrow != '' ){
                                    textError = errThrow;
                                }
                                var $err = $('<label class="notification error"> ' + textError + ' </label>');
                                $dialogConfirm.find('.modal-body').prepend( $err );
                                return;
                            },
                            complete: function(xhr, status){
                                //console.log( 'Complete', xhr, status  );
                                self.xhr_kpi = null;
                                $('body').unblock(IMAGE_LOADING);
                                return;
                            }
                        });
                    }
                    return this;
                });
            });

            $dialogConfirm.modal();
        },
        cloneRowGroup: function(){
            var self = this;
            self.$cloneableGroup = self.$element.find('thead[data-clone-group]');
            var $cloneable = $('.repeat-clone', self.$element);
            $cloneable.remove();
            $cloneable.removeClass('repeat-clone');
            $cloneable.removeClass('clone-thead');
            $cloneable.removeAttr('data-clone-group');
            $cloneable.attr("data-idx-group", "");
            $cloneable.removeAttr('data-id');
        },
        addRowGroup: function(text){
            var self = this;
            self.idxGroup++;
            var $str = $("<div></div>");
            $str.html(self.$cloneableGroup);
            $str.find('span.content').text( text );
            $str.find('.row-group-title.group-numerical-order').text( self.idxGroup );
            $str.find('thead').attr( 'data-idx-group', self.idxGroup );
            $str.find('thead').attr( 'data-group', self.idxGroup );

            $( $str.html() ).insertBefore( self.$listFoot );

            return $str;
        },
        resetIdx: function(){
            var $frmAddTargetItem = $('form.frm-add-target-item');
            $frmAddTargetItem[0].reset();
            this.$tableMF.find('tbody.tbody-main').html('');
            this.idxMF = 1;
            return this;
        },
        createConditionMF: function(mF){
            var self = this;
            var $str = self.$cloneMF.clone();
            self.idxMF++;
            if( $.isPlainObject(mF) && !$.isEmptyObject(mF) ){
                var mfCondition = mF.condition;
                var mfValue = mF.value;
                //$str.find('select [value="' + mfCondition + '"]').attr('selected', true);
                $str.find('[name="meta_input[measurement_formulas][IDX][condition]"] [value="' + mfCondition + '"]').attr('selected', true);
                //$str.find('input.value-result-of-evaluation').attr('value', mfValue);
                $str.find('[name="meta_input[measurement_formulas][IDX][value]"]').attr('value', mfValue);
            }
            var $names = $str.find('[name*="[IDX]"]');
            $names.each(function(idx, item){
                var name = $(this).attr('name') || '', dataIdx = $(this).attr('data-idx') || '';
                var $tr = $(this).closest('tr[data-idx]');
                name = name.replace( /\[IDX\]/g, '['+self.idxMF+']' );
                dataIdx = $tr.attr('data-idx');
                dataIdx = dataIdx.replace( /IDX/g, self.idxMF );
                name ? $(this).attr('name', name) : '';
                dataIdx ? $tr.attr('data-idx', dataIdx) : '';
                return;
            });
            self.$tableMF.find('tbody.tbody-main').append( $str.html() );
        },
        appendConditionMF: function(mF){
            var self = this;
            if( $.isArray( mF ) && mF.length > 0 ){
                  $.each(mF, function(idx){
                      self.createConditionMF(mF[idx]);
                  });
            }else if( $.isPlainObject(mF) && !$.isEmptyObject(mF) ){
                Object.keys(mF).forEach(function(idx){
                    self.createConditionMF(mF[idx]);
                });
            }else{
                self.createConditionMF();
            }
            self.selectorBootstrap();
            return self;
        },
        eventRepeaterMF: function(){
            var self = this;
            $(document).off('click.repeaterMF', 'a[data-event="add-condition-mf"]').on('click.repeaterMF', 'a[data-event="add-condition-mf"]', function(ev){
                ev.preventDefault();
                self.appendConditionMF();
                return;
            });
            $(document).off('click.remove.repeaterMF', 'a[data-event="remove-condition-mf"]').on('click.remove.repeaterMF', 'a[data-event="remove-condition-mf"]', function(ev){
                ev.preventDefault();
                $(this).closest('tr').remove();
                return;
            });
            return this;
        },
        cloneMF: function(){
            var self = this;
            self.$tableMF = $('.table-measurement-formulas');
            self.$cloneMF = self.$tableMF.find('tbody.clone-mf');
            self.$cloneMF.removeClass('display-none');
            self.$cloneMF.remove();
            var $tr = self.$tableMF.find('tbody.tbody-main tr');
            self.idxMF = $tr.length || 0;
        },
        selectorBootstrap: function () {
            var $selectBootstrap = $('select[data-use-bootstrap]');
            $selectBootstrap.selectpicker();
            return this;
        },

    };
    $.fn.brainmarkRepeater = function( options ){
        this.each(function(){
            var bmRepeater = $(this).data('brainmarkRepeater');
            if( !bmRepeater ){
                bmRepeater = new BrainmarkRepeater( this, options );
                $(this).data('brainmarkRepeater', bmRepeater);
            }
        });
        return this;
    }

    function KPIBank(element, options){
        var dataSet = $(this).data()|| {};
        this.options = $.extend({}, dataSet, options || {});
        this.$parent_element = $(element);
        this.parent_element = element;
        this.init();
    }

    KPIBank.prototype = {

        initRepeater: function(){
            $('.brainmark-repeater').brainmarkRepeater();
        },

        init: function(){
            var self =this;
            self.$menuLeft = $('.parent-categories.navbar-left');
            self.initRepeater();
            self.eventDropdown();
        },
        eventDropdown: function(){
            var self = this;
            $(document).off('click.dropdown', '.parent-categories.navbar-left a.event-dropdown').on('click.dropdown', '.parent-categories.navbar-left a.event-dropdown', function(ev){
                ev.preventDefault();
                var $iconDropdown = $(this).find('i');
                $iconDropdown.toggleClass('fa-caret-up');
                $(this).next('ul').slideToggle();
            });
        }
    };

    $.fn.kpiBank = function( options ){
        this.each(function(){
            var kpi_bank = $(this).data('kpiBank');
            if( !kpi_bank ){
                kpi_bank = new KPIBank( this, options );
                $(this).data('kpiBank', kpi_bank);
            }
        });
        return this;
    }
    $(function(){
        $('.page-template-page-kpi-bank').kpiBank();
    });
});