jQuery(function($){

    function BrainmarkRepeater(element, options){
        var dataSet = $(this).data()|| {};
        this.options = $.extend({}, dataSet, options || {});
        this.$element = $(element);
        this.element = element;
        this.init();
    }

    BrainmarkRepeater.prototype = {
        init: function(){
            var self = this, options = this.options;

            self.$formAddGroupTarget = $('form[data-add-group-event]');
            self.$popup = $('#popup-kpi-bank');
            self.idxGroup = self.$element.find('thead[data-idx-group]').length || 0;
            self.idxGroupItem = 0;
            this.inited = true;
            self.$listHead = self.$element.find('.brainmark-table thead');
            self.$listFoot = self.$element.find('.brainmark-table tfoot');
            self.$listBody = self.$element.find('.brainmark-table tbody');
            self.$cloneableGroupItem = self.$element.find('tr[data-id="brainmarkcloneitemindex"]');
            self.cloneRowGroup();
            /*self.$listHead.on('click.repeater.item', 'a[data-event]', function(ev){
                ev.preventDefault();
            });*/
            self.$listFoot.on('click.show.repeater.item', 'a[data-show]', function(ev){
                // Add group
                ev.preventDefault();
                var $next = $(this).closest('tr').next();
                $next.toggle();
                //var $tfoot = $(this).closest('tfoot.row-tfoot-group');
                //var $str = self.cloneRowGroup();
                //$tfoot.append( $str );
                $next.find('.frm-add-group-target #post-title-group-target').val('');
                return this;
            });
            self.initForm();
            self.eventAddRowGroup();
            self.eventRowItem();
            self.cloneButtonDelItem();
            self.cloneMF();
            self.eventRepeaterMF();
            self.selectorBootstrap();
            self.importAction();
            self.initFormOrgchart();
        },
        initFormOrgchart: function(){
            var self = this;
            $('.frm-orgchart-term .btn-orgchart-term', self.$element).off('click.update.chart').on('click.update.chart', function(ev){
                ev.preventDefault();
                var $form = $(this).closest('form');
                var options = {
                    dataType: 'json',
                    beforeSubmit: function (serialize, form, option) {
                        $form.find('.notification').remove();
                        return true;
                    },
                    beforeSend: function () {
                        $form.block(IMAGE_LOADING);
                    },
                    success: function (response, status, xhr) {
                        //console.log('SUCCESS', response, status, xhr);
                        if (xhr.status == 201) {
                            window.location.reload();
                        }
                        return;
                    },
                    error: function (xhr, status, errThrow) {
                        // console.log( 'ERROR', xhr, status, errThrow );
                        //window.location.reload();
                        var textError = '';
                        if (('responseJSON' in xhr) && ( 'message' in xhr.responseJSON ) && (xhr.responseJSON.message != '')) {
                            textError = xhr.responseJSON.message;
                        } else if (errThrow != '') {
                            textError = xhr.getResponseHeader('xhr-message') || errThrow;
                            try {
                                textError = JSON.parse(textError);
                            } catch (e) {
                            }
                        }
                        var $err = $('<label class="notification error"> ' + textError + ' </label>');
                        $form.find('.notification').remove();
                        $form.prepend($err);
                        return;
                    },
                    complete: function (xhr, status) {
                        //console.log( 'Complete', xhr, status  );
                        self.xhr_kpi = null;
                        $form.unblock(IMAGE_LOADING);
                        return;
                    }
                };
                $form.ajaxSubmit(options);
                return false;
            });
        },
        cloneButtonDelItem: function(){
            var self = this;
            self.$btnDelItem = self.$popup.find('button[data-event="del-item"]');
            self.removeButtonDelItem();
        },
        removeButtonDelItem: function(){
            this.$btnDelItem.remove();
        },
        appendButtonDelItem: function(){
            var self = this;
            var $closePopup = $('.btn-brb-default.btn-cancel.close-popup');
            self.$btnDelItem.insertAfter( $closePopup );
        },
        /*createTemplateGroup: function( $str ){
            var self = this;
            var text = $str.toString();
            var $thead = $('<thead class="row-thead-title-group"></thead>');
            var $tr = $('<tr></tr>');
            var $th_1 = $('<th class="row-group-title group-numerical-order"> ' + self.idxGroup + '</th>');
            var $th_2 = $('<th colspan="4" class="row-group-title group-title">' + text + '</th>');
            var $th_3 = $('<th colspan="3" class="row-group-title action-add-row-target action-add-row-group">' +
                '<a href="javascript:;" data-event="add-row">Add target <i class="fa fa-plus-circle" aria-hidden="true"></i></a></th>');
        },*/
        submitFormAjax: function($form, validator, ev){
            var self = this;
            ev.preventDefault();
            /*if( ('xhr_kpi' in self) && self.xhr_kpi !== null ) {
                self.xhr_kpi.abort("Cancel and Rerquest again");
            }*/
            self.xhr_kpi = {
                dataType: 'json',
                beforeSubmit: function(serialize, form, option) {
                    $form.find('.notification').remove();
                    return true;
                },
                beforeSend: function () {
                    $form.block(IMAGE_LOADING);
                },
                success: function(response, status, xhr){
                    console.log( 'SUCCESS',response, status, xhr );
                    if( $.isPlainObject(response) || 'success' in response ){
                        $form.closest('.modal[role="dialog"]').modal('hide');
                        window.location.reload();
                    }
                    return;
                },
                error: function(xhr, status, errThrow){
                    // console.log( 'ERROR', xhr, status, errThrow );
                    //window.location.reload();
                    var textError = '';
                    if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                        textError = xhr.responseJSON.error;
                    }else if( errThrow != '' ){
                        textError = xhr.getResponseHeader('xhr-message') || errThrow;
                        try{
                            textError = JSON.parse(textError);
                        } catch(e){}
                    }
                    var $err = $('<label class="notification error"> ' + textError + ' </label>');
                    $form.find('.notification').remove();
                    $form.prepend( $err );
                    return;
                },
                complete: function(xhr, status){
                    //console.log( 'Complete', xhr, status  );
                    self.xhr_kpi = null;
                    $form.unblock(IMAGE_LOADING);
                    return;
                }
            };
            $form.ajaxSubmit(self.xhr_kpi);
            return false;
        },
        formValidate: function(frm){
            var self = this;
            var $frm = $(frm);
            var $required = $([].slice.call(frm.elements)).filter('[required]');
            var validationRules = {
                // Specify validation rules
                //onsubmit: callajax ? false : true,
                focusCleanup: true,
                errorClass: 'error',
                validClass: 'valid',
                rules: {},
                // Specify validation error messages
                messages: {},
                errorElement: "em",
                errorPlacement: function ( error, $element ) {
                    // Add the `help-block` class to the error element
                    error.addClass( "help-block" );
                    var data = $element.data();
                    var errText = data.errRequired || 'Please input value for ' + $element.attr('placeholder');
                    error.text( errText );
                    if('$label' in $element[0]) {
                        error.insertAfter( $element[0].$label );
                    } else if ( $element.prop( "type" ) === "checkbox" ) {
                        error.insertAfter( $element.parent( "label" ) );
                    } else {
                        error.insertAfter( $element );
                    }
                    return false;
                },
                invalidHandler: function(ev) {
                    var validator = $frm.data('validator');
                    var elements = validator.invalidElements();
                    if( validator.errorList.length == 0 ) {
                        validator.errorList = [];
                        self.initFormValidate();
                        $frm.trigger('submit.validate');
                        return;
                    }
                    return;
                },
                submitHandler: function(form, ev) {
                    var $form = $(form);
                    var validator = this;
                    return self.submitFormAjax($form, validator, ev);
                }
            };
            $required.each(function(idx, elem) {
                validationRules.rules[elem.id] = 'required';
                try {
                    var $label = $frm.find('label[for="'+elem.id+'"]');
                    elem.$label = $label;
                    $label = $label.clone();
                    $label.find('span').remove();
                    var field = $label.text().trim();
                    var data = $(elem).data() || {};
                    validationRules.messages[elem.id] = {
                        required: data.errRequired || 'Please input value for '+ field
                    }
                }catch(e) {
                }
                return;
            });
            var validator = $frm.data('validator');
            if( validator ) {
                $frm
                    .off( ".validate" )
                    .removeData( "validator" )
                    .find( ".validate-equalTo-blur" )
                    .off( ".validate-equalTo" )
                    .removeClass( "validate-equalTo-blur" );
            }
            $frm.validate(validationRules);
        },
        initFormValidate: function(){
            var self = this;
            self.$formAddGroupTarget.each(function(index, form){
                self.formValidate( form );
            });

            return;
        },
        initForm: function(){
            var self = this;
            self.initFormValidate();
            return self;
        },
        eventAddRowGroup: function(){
            var self = this;
            self.$listFoot.on('click.add_repeater.item', 'a[data-event="add-group-row"]', function(ev){
                // Add group
                ev.preventDefault();
                var $textInput = $(this).prev();
                var text = $textInput.val();
                //console.log( self.$cloneableGroup );
                self.addRowGroup.call( self, text );
                return this;
            });

            $(document).off('click.submitGroup', '.btn-add-group-target, #save-target-item').on('click.submitGroup', '.btn-add-group-target, #save-target-item', function(ev){
                ev.preventDefault();
                var $form = $(this).closest('form');
                $form.trigger('submit.validate');
                return false;
            });

        },
        eventRowItem: function(){
            var self = this;
            self.$popup.bind('show.bs.modal.row', function(ev) {
                var $btn = $(ev.relatedTarget), isAdd = false, data = $btn.data();;

                var parent = 0;
                if( $btn.data('event') == 'add-row-item' ) {
                    isAdd = true;
                    $('.code-kpi', self.$popup).hide();
                    self.removeButtonDelItem();
                } else {
                    // Edit
                    var $tr = $btn.closest('tr');
                    data = $tr.data();
                    self.appendButtonDelItem();
                }
                self.resetIdx();

                if( 'parent' in data ){
                    parent = data.parent;
                }
                var $formula_id = $('select[name="formula_id"]', self.$popup);
                $formula_id.val( '' );
                $formula_id.selectpicker('refresh');
                $('input[name="post_parent"]', self.$popup).val( parent );
                if( !isAdd ) {
                    if( ('xhr_kpi' in self) && self.xhr_kpi !== null ) {
                        self.xhr_kpi.abort("Cancel and Rerquest again");
                    }
                    self.xhr_kpi = $.ajax({
                        url: AJAX_URL,
                        data: {action: 'load_target_item', id: data.id},
                        dataType: 'json',
                        type: 'get',
                        beforeSend: function () {
                            self.$popup.find('.notification').remove();
                            self.$element.block(IMAGE_LOADING);
                        },
                        success: function(response, status, xhr){
                            ///console.log( 'SUCCESS',response, status, xhr );
                            if( $.isPlainObject(response) || 'data' in response ){
                                var resData = response.data;
                                Object.keys(resData).forEach( function(item){
                                    var key = item.replace(/(\[\])/g, '\$1' );
                                    var resValue = resData[item];
                                    if( item == 'formula_id' ){
                                        self.$popup.find('[name="' + key + '"]').val( resValue );
                                        self.$popup.find('[name="' + key + '"]').selectpicker('refresh')
                                    }else if(item == 'measurement_formulas'){
                                        //self.appendConditionMF( resValue );
                                    }else if( item == 'post_id' ){
                                        self.$popup.find('[data-event="del-item"]').attr('data-id', resValue);
                                        self.$popup.find('[name="' + key + '"]').val( resValue );
                                        self.$popup.find('.code-post').text( resValue );
                                        self.$popup.find('.code-kpi').show();
                                    }else if( item == 'post_status' ){
                                        var checked = ( resValue === 'checked' ) ? true : false;
                                        self.$popup.find('[name="' + key + '"]').attr('checked', checked);
                                    }else{
                                        self.$popup.find('[name="' + key + '"]').val( resValue );
                                    }
                                    return;
                                });
                            }
                            return;
                        },
                        error: function(xhr, status, errThrow){
                            console.log( 'ERROR',xhr, status, errThrow );
                            //window.location.reload();
                            var textError = '';
                            if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                                textError = xhr.responseJSON.error;
                            }else if( errThrow != '' ){
                                textError = xhr.getResponseHeader('xhr-message') || errThrow;
                                try{
                                    textError = JSON.parse(textError);
                                } catch(e){}
                            }
                            var $err = $('<label class="notification error"> ' + textError + ' </label>');
                            self.$popup.find('form').prepend( $err );
                            return;
                        },
                        complete: function(xhr, status){
                            //console.log( 'Complete', xhr, status  );
                            self.xhr_kpi = null;
                            self.$element.unblock(IMAGE_LOADING);
                            return;
                        }
                    });
                }
                return;
            });
            /*
            self.$element.off('click.add_row_item_in_group', 'a[data-event="add-row-item"]')
                .on('click.add_row_item_in_group', 'a[data-event="add-row-item"]', function(ev){
                ev.preventDefault();
                self.removeButtonDelItem();
                self.resetIdx();
                var data = $(this).data();
                var parent = 0;
                if( 'parent' in data ){
                    parent = data.parent;
                }
                $('.code-kpi', self.$popup).hide();
                $('input[name="post_parent"]', self.$popup).val( parent );
                self.$popup.modal();
            });
            self.$element.off('click.edit_row_item', '.brainmark-kpi-target-item td:not(.item-action-on-off)').on('click.edit_row_item','.brainmark-kpi-target-item td:not(.item-action-on-off)', function(ev){
                var $tr = $(this).closest('tr');
                var data = $tr.data();
                self.appendButtonDelItem();
                self.resetIdx();
                if( ('xhr_kpi' in self) && self.xhr_kpi !== null ) {
                    self.xhr_kpi.abort("Cancel and Rerquest again");
                }
                self.xhr_kpi = $.ajax({
                    url: AJAX_URL,
                    data: {action: 'load_target_item', id: data.id},
                    dataType: 'json',
                    type: 'get',
                    beforeSend: function () {
                        self.$popup.find('.notification').remove();
                        self.$element.block(IMAGE_LOADING);
                    },
                    success: function(response, status, xhr){
                        ///console.log( 'SUCCESS',response, status, xhr );
                        if( $.isPlainObject(response) || 'data' in response ){
                            var resData = response.data;
                            Object.keys(resData).forEach( function(item){
                                var key = item.replace(/(\[\])/g, '\$1' );
                                var resValue = resData[item];
                                if( item == 'formula_id' ){
                                    self.$popup.find('[name="' + key + '"]').val( resValue );
                                    self.$popup.find('[name="' + key + '"]').selectpicker('refresh')
                                }else if(item == 'measurement_formulas'){
                                    //self.appendConditionMF( resValue );
                                }else if( item == 'post_id' ){
                                    self.$popup.find('[data-event="del-item"]').attr('data-id', resValue);
                                    self.$popup.find('[name="' + key + '"]').val( resValue );
                                    self.$popup.find('.code-post').text( resValue );
                                    self.$popup.find('.code-kpi').show();
                                }else if( item == 'post_status' ){
                                    var checked = ( resValue === 'checked' ) ? true : false;
                                    self.$popup.find('[name="' + key + '"]').attr('checked', checked);
                                }else{
                                    self.$popup.find('[name="' + key + '"]').val( resValue );
                                }

                            });
                            self.$popup.modal();
                        }
                        return;
                    },
                    error: function(xhr, status, errThrow){
                        console.log( 'ERROR',xhr, status, errThrow );
                        //window.location.reload();
                        var textError = '';
                        if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                            textError = xhr.responseJSON.error;
                        }else if( errThrow != '' ){
                            textError = errThrow;
                        }
                        var $err = $('<label class="notification error"> ' + textError + ' </label>');
                        self.$popup.find('form').prepend( $err );
                        return;
                    },
                    complete: function(xhr, status){
                        //console.log( 'Complete', xhr, status  );
                        self.xhr_kpi = null;
                        self.$element.unblock(IMAGE_LOADING);
                        return;
                    }
                });
            });

            self.$element.off('click.close_popup', 'button.close-popup').on('click.close_popup', 'button.close-popup', function(ev){
                ev.preventDefault();
                $('[data-dismiss="modal"]').trigger('click');
            });
            */

            // remove group
            self.$element.off('click.del_row_item_in_group', 'a[data-event="del-row-group"]').on('click.del_row_item_in_group', 'a[data-event="del-row-group"]', function(ev){
                ev.preventDefault();
                var $theadParent = $(this).closest('thead');
                var datagroup = $theadParent.attr('data-group');
                var data = $(this).data();
                self.bindEventDialog( data );
                /*self.$element.find('[data-group="'+datagroup+'"]').remove();
                self.idxGroup --;
                $.each( self.$element.find('thead[data-idx-group]'), function(idx, item){
                    $(item).find('.row-group-title.group-numerical-order').text(idx + 1);
                });*/
            });
            self.$element.off('click.remove.row_item', '[data-event="del-item"]').on('click.remove.row_item', '[data-event="del-item"]', function(ev){
                ev.preventDefault();
                var data = $(this).data();
                self.bindEventDialog(data);
            });
            //change status
            self.$element.off('change.change.status', '.item-action-on-off .checkbox-status').on('change.change.status', '.item-action-on-off .checkbox-status', function(ev){
                ev.preventDefault();
                var data = $(this).data();
                var $parentTD = $(this).closest('td');
                var $parentTR = $(this).closest('tr');
                var status = $(this).prop('checked');
                status = status ? $(this).val() : '';
                $.ajax({
                    url: AJAX_URL,
                    data: {action: 'change_status', post_id: data.id, post_status: status},
                    dataType: 'json',
                    type: 'post',
                    beforeSend: function () {
                        $parentTR.block(IMAGE_LOADING);
                        $parentTD.find('.notification').remove();
                    },
                    success: function(response, status, xhr){
                        ///console.log( 'SUCCESS',response, status, xhr );
                        if( $.isPlainObject(response) || 'success' in response ){
                            //window.location.reload();
                            var $notification = $('<span class="notification success">' + response.success + '</span>');
                            $parentTD.append( $notification );
                            var notifiTimeout;
                            if( notifiTimeout !== undefined ){
                                clearTimeout( notifiTimeout );
                            }
                            notifiTimeout = setTimeout(function(){
                                $parentTD.find('.notification').remove();
                            }, 3000);
                        }
                        return;
                    },
                    error: function(xhr, status, errThrow){
                        //console.log( 'ERROR',xhr, status, errThrow );
                        var textError = '';
                        if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                            textError = xhr.responseJSON.error;
                        }else if( errThrow != '' ){
                            textError = xhr.getResponseHeader('xhr-message') || errThrow;
                            try{
                                textError = JSON.parse(textError);
                            } catch(e){}
                        }
                        var $err = $('<span class="notification error"> ' + textError + ' </span>');
                        $parentTD.append( $err );
                        return;
                    },
                    complete: function(xhr, status){
                        //console.log( 'Complete', xhr, status  );
                        $parentTR.unblock(IMAGE_LOADING);
                        return;
                    }
                });
                return this;
            });

        },
        bindEventDialog: function(data){
            var $dialogConfirm = $('#myDialogConfirm');
            $dialogConfirm.off('hidden.bs.modal.confirm').on('hidden.bs.modal.confirm', function(ev){
                $dialogConfirm.off('click.dialog.agree', '#myDialogConfirm .btn-yes');
            });
            $dialogConfirm.off('show.bs.modal.confirm').on('show.bs.modal.confirm', function(ev){
                $dialogConfirm.find('.notification').remove();
                $dialogConfirm.off('click.dialog.agree', '.btn-yes').on('click.dialog.agree', '.btn-yes', function(event){
                    event.preventDefault();
                    if( !$.isPlainObject(data) && data === undefined ){
                        alert('Vui lòng nhấn nút xóa trước khi hiển thị form này.');
                    }else{
                        $.ajax({
                            url: AJAX_URL,
                            data: {action: data.action, _wpnonce: data.nonce, ID: data.id},
                            type: 'post',
                            dataType: 'json',
                            beforeSend: function () {
                                $dialogConfirm.find('.notification').remove();
                                $('body').block(IMAGE_LOADING);
                            },
                            success: function(response, status, xhr){
                                ///console.log( 'SUCCESS',response, status, xhr );
                                if( $.isPlainObject(response) || 'success' in response ){
                                    window.location.reload();
                                }
                                return;
                            },
                            error: function(xhr, status, errThrow){
                                //console.log( 'ERROR',xhr, status, errThrow );
                                var textError = '';
                                if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                                    textError = xhr.responseJSON.error;
                                }else if( errThrow != '' ){
                                    textError = xhr.getResponseHeader('xhr-message') || errThrow;
                                    try{
                                        textError = JSON.parse(textError);
                                    } catch(e){}
                                }
                                var $err = $('<label class="notification error"> ' + textError + ' </label>');
                                $dialogConfirm.find('.modal-body').prepend( $err );
                                return;
                            },
                            complete: function(xhr, status){
                                //console.log( 'Complete', xhr, status  );
                                self.xhr_kpi = null;
                                $('body').unblock(IMAGE_LOADING);
                                return;
                            }
                        });
                    }
                    return this;
                });
            });

            $dialogConfirm.modal();
        },
        cloneRowGroup: function(){
            var self = this;
            self.$cloneableGroup = self.$element.find('thead[data-clone-group]');
            var $cloneable = $('.repeat-clone', self.$element);
            $cloneable.remove();
            $cloneable.removeClass('repeat-clone');
            $cloneable.removeClass('clone-thead');
            $cloneable.removeAttr('data-clone-group');
            $cloneable.attr("data-idx-group", "");
            $cloneable.removeAttr('data-id');
        },
        addRowGroup: function(text){
            var self = this;
            self.idxGroup++;
            var $str = $("<div></div>");
            $str.html(self.$cloneableGroup);
            $str.find('span.content').text( text );
            $str.find('.row-group-title.group-numerical-order').text( self.idxGroup );
            $str.find('thead').attr( 'data-idx-group', self.idxGroup );
            $str.find('thead').attr( 'data-group', self.idxGroup );

            $( $str.html() ).insertBefore( self.$listFoot );

            return $str;
        },
        resetIdx: function(){
            var $frmAddTargetItem = $('form.frm-add-target-item');
            $frmAddTargetItem[0].reset();
            this.$tableMF.find('tbody.tbody-main').html('');
            this.idxMF = 1;
            return this;
        },
        createConditionMF: function(mF){
            var self = this;
            var $str = self.$cloneMF.clone();
            self.idxMF++;
            if( $.isPlainObject(mF) && !$.isEmptyObject(mF) ){
                var mfCondition = mF.condition;
                var mfValue = mF.value;
                //$str.find('select [value="' + mfCondition + '"]').attr('selected', true);
                $str.find('[name="meta_input[measurement_formulas][IDX][condition]"] [value="' + mfCondition + '"]').attr('selected', true);
                //$str.find('input.value-result-of-evaluation').attr('value', mfValue);
                $str.find('[name="meta_input[measurement_formulas][IDX][value]"]').attr('value', mfValue);
            }
            var $names = $str.find('[name*="[IDX]"]');
            $names.each(function(idx, item){
                var name = $(this).attr('name') || '', dataIdx = $(this).attr('data-idx') || '';
                var $tr = $(this).closest('tr[data-idx]');
                name = name.replace( /\[IDX\]/g, '['+self.idxMF+']' );
                dataIdx = $tr.attr('data-idx');
                dataIdx = dataIdx.replace( /IDX/g, self.idxMF );
                name ? $(this).attr('name', name) : '';
                dataIdx ? $tr.attr('data-idx', dataIdx) : '';
                return;
            });
            self.$tableMF.find('tbody.tbody-main').append( $str.html() );
        },
        appendConditionMF: function(mF){
            var self = this;
            if( $.isArray( mF ) && mF.length > 0 ){
                  $.each(mF, function(idx){
                      self.createConditionMF(mF[idx]);
                  });
            }else if( $.isPlainObject(mF) && !$.isEmptyObject(mF) ){
                Object.keys(mF).forEach(function(idx){
                    self.createConditionMF(mF[idx]);
                });
            }else{
                self.createConditionMF();
            }
            self.selectorBootstrap();
            return self;
        },
        eventRepeaterMF: function(){
            var self = this;
            /*self.$element.off('click.repeaterMF', 'a[data-event="add-condition-mf"]').on('click.repeaterMF', 'a[data-event="add-condition-mf"]', function(ev){
                ev.preventDefault();
                self.appendConditionMF();
                return;
            });*/
            $(self.$element).off('click.remove.repeaterMF', 'a[data-event="remove-condition-mf"]').on('click.remove.repeaterMF', 'a[data-event="remove-condition-mf"]', function(ev){
                ev.preventDefault();
                $(this).closest('tr').remove();
                return;
            });
            return this;
        },
        cloneMF: function(){
            var self = this;
            self.$tableMF = $('.table-measurement-formulas');
            self.$cloneMF = self.$tableMF.find('tbody.clone-mf');
            self.$cloneMF.removeClass('display-none');
            self.$cloneMF.remove();
            var $tr = self.$tableMF.find('tbody.tbody-main tr');
            self.idxMF = $tr.length || 0;
        },
        selectorBootstrap: function () {
            var $selectBootstrap = $('select[data-use-bootstrap]');
            $selectBootstrap.selectpicker();
            return this;
        },
        importAction: function(){
          var self = this;
            self.$listFiles = $('.ul-upload-files');
            self.$fileItem = self.$listFiles.find('li').clone();
            self.$listFiles.find('li').remove();
            var $hasFiles = $('.modal-footer .has-files');
            $('.popup-import-kpi', self.$element).off('click.show.popup.import').on('click.show.popup.import', function(ev){
               ev.preventDefault();
               var data = $(this).data();
               var $popup = $(data.target);
               $popup.off('show.bs.modal').on('show.bs.modal', function(){
                   var $form = $('form', $(this));
                   $form.find(".has-files").hide();
                   $form.resetForm();
                   var $response = $form.find('.response-container');
                   $form.off('submit.import.files').on('submit.import.files', function(){
                       $form.ajaxSubmit({
                           beforeSend: function(xhr, settings) {
                               $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                               $form.block(IMAGE_LOADING);
                               return;
                           },
                           error: function(xhr, status, error) {
                               if( ('responseJSON' in xhr) && ('code' in xhr.responseJSON) ) {
                                   $response.removeClass('hidden').addClass('error').html(xhr.responseJSON.message);
                               } else {
                                   error = xhr.getResponseHeader('xhr-message') || error;
                                   try{
                                       error = JSON.parse(error);
                                   } catch(e){}
                                   if( xhr.status >= 500 ){
                                       error = "Vui lòng tải file mẫu ở trên";
                                   }
                                   $response.removeClass('hidden').addClass('error').html(error);
                               }
                               return;
                           },
                           success: function(response, status, xhr) {
                               if( xhr.status == 201 ) {
                                   $response.removeClass('hidden').addClass('success').html("Lưu thành công");
                                   location.reload();
                               }
                               return;
                           },
                           complete: function(xhr, status) {
                               $form.unblock(IMAGE_LOADING);
                               return;
                           }
                       });
                       return false;
                   });
               });
            });
            $('#imports-bank-kpi', self.$element).off('change.import').on('change.import', function(ev){
                var $label = $(this).next();
                var $parent = $(this).parent();
                var $this = $(this);
                if( this.files && this.files.length > 0 ){
                    var data = $(this).data();
                    var files = this.files;
                    Object.keys(files).forEach(function (idx) {
                        var file = files[idx];
                        // 5M
                        if (file.size > 5242880) {
                            $parent.find('.response-container').text( data.errorSize );
                        }else{
                            var $eleFile = self.$fileItem;
                            $eleFile.find('.li-filename').text( file.name );
                            $('.btn-remove-file-import', $eleFile).off('click.remove.file.import').on('click.remove.file.import', function(ev){
                                ev.preventDefault();
                                var $li = $(this).closest('li');
                                $li.remove();
                                $this.val('');
                                $hasFiles.hide();
                            });
                            self.$listFiles.append( $eleFile );
                            $hasFiles.show();
                        }
                    });
                }else{

                }
            });

        },
    };
    $.fn.brainmarkRepeater = function( options ){
        this.each(function(){
            var bmRepeater = $(this).data('brainmarkRepeater');
            if( !bmRepeater ){
                bmRepeater = new BrainmarkRepeater( this, options );
                $(this).data('brainmarkRepeater', bmRepeater);
            }
        });
        return this;
    }

    function KPIBank(element, options){
        var dataSet = $(this).data()|| {};
        this.options = $.extend({}, dataSet, options || {});
        this.$parent_element = $(element);
        this.parent_element = element;
        this.init();
    }

    KPIBank.prototype = {

        initRepeater: function(){
            this.$parent_element.brainmarkRepeater();
        },

        init: function(){
            var self =this;
            self.$menuLeft = $('.parent-categories.navbar-left');
            self.initRepeater();
            self.eventDropdown();
        },
        eventDropdown: function(){
            var self = this;
            self.$parent_element.off('click.dropdown', '.parent-categories.navbar-left a.event-dropdown i')
                .on('click.dropdown', '.parent-categories.navbar-left a.event-dropdown i', function(ev){
                ev.preventDefault();
                var $iconDropdown = $(this);
                $iconDropdown.toggleClass('fa-caret-up');
                $(this).closest('a').next('ul').slideToggle();
            });
        }
    };

    $.fn.kpiBank = function( options ){
        this.each(function(){
            var kpi_bank = $(this).data('kpiBank');
            if( !kpi_bank ){
                kpi_bank = new KPIBank( this, options );
                $(this).data('kpiBank', kpi_bank);
            }
        });
        return this;
    }
    $(function(){
        $('.page-template-page-kpi-bank').kpiBank();
    });
});