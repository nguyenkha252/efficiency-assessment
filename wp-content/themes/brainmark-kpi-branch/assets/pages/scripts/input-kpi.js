(function($) {
    function InputKPI(element, options) {
        this.$element = $(element);
        this.options = options;
        this.initEvents();
        return this;
    }
    InputKPI.prototype = {
        ajax: function(options) {
            var self = this;
            options.beforeSend = options.beforeSend || $.noop;
            options.error = options.error || $.noop;
            options.success = options.success || $.noop;
            options.complete = options.complete || $.noop;
            var params = $.extend({
                url: AJAX_URL,
                type: 'post',
                dataType: 'json',
                beforeSend: function(xhr, settings) {
                    try{
                        if( '$target' in options ) {
                            $(options.$target).addClass('loading');
                        }
                        options.beforeSend.apply(this, arguments);
                    }catch(e){
                        console.error(e);
                    }
                    return;
                },
                error: function(xhr, status, error) {
                    try{
                        arguments[2] = error = xhr.getResponseHeader('xhr-message') || error;
                        try{
                            arguments[2] = JSON.parse(error);
                        } catch(e){}
                        options.error.apply(this, arguments);
                    }catch(e){
                        console.error(e);
                    }
                    return;
                },
                success: function(response, status, xhr) {
                    try{
                        if( ('$html' in options) && ('html' in  response) ) {
                            $(options.$html).html( response.html );
                        }
                        options.success.apply(this, arguments);
                    }catch(e){
                        console.error(e);
                    }
                    return;
                },
                complete: function(xhr, status) {
                    try{
                        if( '$target' in options ) {
                            $(options.$target).removeClass('loading');
                        }
                        options.complete.apply(this, arguments);
                    }catch(e){
                        console.error(e);
                    }
                    options.xhr = null;
                    return;
                }
            }, options);
            options.xhr = $.ajax(params);
            return self;
        },
        popupKpiYear: function() {
            var self = this;
            var $popup = $('div[id="tpl-kpi-year"]'), $editForm = $popup.children('form.modal-dialog'),
                $addForm = $editForm.clone(), addFr = $addForm[0];
            if( !addFr ) return;
            $addForm.attr('action', $addForm.data('addUrl') );
            $editForm.attr('action', $editForm.data('updateUrl') );
            $editForm.before($addForm);
            ('resetForm' in $addForm) ? $addForm.resetForm() : null;
            $addForm.find('input[name="parent"][type="hidden"]').val('0');
            $addForm.find('input[name="kpi_type"][type="hidden"]').val('congty');
            $addForm.find('input.form-control[type="number"]').each(function(){
                $(this).val('');
                return;
            });
            $(addFr.nam).selectpicker('val', '');
            $(addFr.nam).selectpicker('refresh');
            addFr.year_id.value = '';
            $(addFr.kpi_time).selectpicker('val', '');
            $(addFr.kpi_time).selectpicker('refresh');

            var $title = $editForm.find('.modal-header > .modal-title');
            $title.html( $title.data('updateTitle') );

            $addForm.addClass('hidden');
            $editForm.addClass('hidden');

            $popup.on('hide.bs.modal.year', function(showEv) {
                $popup.find('form').addClass('hidden');
            });
            $popup.on('show.bs.modal.year', function(showEv) {
                var $fromButton = $(showEv.relatedTarget);
                // kpi-percent
                var $form, yearInfo = $fromButton.data('year') ||
                    {id: '', year: '', precious: '', month: '', finance: '', customer: '', customer: '', operate: '',
                        development: '', chart_id: '', kpi_time: 'quy', kpi_type: 'congty'};
                if( $fromButton.data('yearId') === 'new' ) {
                    $form = $addForm;
                } else {
                    $form = $editForm;
                }
                $form.removeClass('hidden');

                var frm = $form[0], tabInfo = $form.data('tabInfo');

                $form.off('submit.kpiyear').on('submit.kpiyear', function(ev) {
                    ev.preventDefault();
                    var $response = $('.response-container', $form);
                    $form.ajaxSubmit({
                        beforeSend: function(xhr, settings) {
                            $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                            $form.block(IMAGE_LOADING);
                        },
                        beforeSubmit: function(a, $frm, options) {
                            var valid = true, total = 0.0, messages = [];
                            $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                            if( frm.nam.value == '' ) {
                                valid = false;
                                messages.push("Vui lòng chọn vào Năm");
                            }
                            $form.find('.chuc-danh-hien-tai input.form-control[type="number"]').each(function(){
                                var num = parseInt(this.value.trim() || '0');
                                if( isNaN(num) || !isFinite(num) ) {
                                    num = 0;
                                }
                                total += num;
                            });
                            if( total != 100 ) {
                                valid = false;
                                messages.push("Tổng trọng số phải bằng 100%");
                            }
                            if( !valid ) {
                                $response.removeClass('hidden').addClass('error').html(messages.join('<br>'));
                            }
                            return valid;
                        },
                        error: function(xhr, status, error) {
                            if( ('responseJSON' in xhr) && ('message' in xhr.responseJSON) ) {
                                $response.removeClass('hidden').addClass('error').html(xhr.responseJSON.message);
                            }
                            return;
                        },
                        success: function(response, status, xhr) {
                            $response.removeClass('hidden').addClass('success').html("Lưu thành công");
                            if( $.isPlainObject(response) && ('code' in response) ) {
                                $popup.modal('hide');
                                if( 'year_value' in response ){
                                   var url = location.href.replace(/(\&|\?)(nam)(=\d+|)/, '$1$2=' + response.year_value);
                                   location.href = url;
                                }else {
                                    location.reload();
                                }
                            }
                            return;
                        },
                        complete: function(xhr, status) {
                            $form.unblock(IMAGE_LOADING);
                            return;
                        }
                    });
                    return;
                });
                return;
            });
            return self;
        },
        initDate: function(scope) {
            var self = this;
            var $items = $('input[data-ctrl-date]', scope);
            $items.each(function () {
                var $this = $(this), options = $this.data(), $btn = null, $group = $this.parent();
                if( !('xdsoft_datetimepicker' in options) ) {
                    this.datetimepicker = $this.datetimepicker(options);
                    if( ('btnDate' in options) && options.btnDate ) {
                        if( ('groupDate' in options) && options.groupDate ) {
                            $group = $this.closest(options.groupDate);
                        }
                        $btn = $(options.btnDate, $group);
                        if( $btn.length ) {
                            $btn.unbind('click.date').bind('click.date', function(ev) {
                                $this.trigger('focusin.xdsoft');
                                return;
                            });
                        }
                    }
                }
                return;
            });
            return self;
        },
        popupNewPersonalKpi: function(){
            var self = this;
            $('[role="dialog"][id^="tpl-add-target-"]', self.$element).off('show.bs.modal.add-target').on('show.bs.modal.add-target', function(showEv) {
                var $form = $('form', this), $btn = $(showEv.relatedTarget);
                var $bankSelector = $('select.post_title', $form);
                var $catBank = $form.find('select.cat-bank');
                ('resetForm' in $form) ? $form.resetForm() : null;
                self.initDate($form);
                $form.find('select.post_title-target_work').each(function(){
                    var loading = $(this).data('loading'),
                        $loading = $(loading, $form);
                    $loading.unblock(IMAGE_LOADING);
                    return;
                });

                $form[0].chartAjax = null;
                function renderOption (obj, parent){
                    var cat = [], html ='';
                    Object.keys(obj).forEach(function(id){
                        if( obj[id].parent == parent ){
                            cat.push(obj[id]);
                            delete obj[id];
                        }
                    });
                    if( cat ){
                        Object.keys(cat).forEach(function(id) {
                            var post = cat[id];
                            var outputChild = '';
                            if( obj ){
                                outputChild = renderOption(obj, post.ID);
                                if( outputChild !== '' ){
                                    html += "<optgroup label=\"" + post.title + "\"> " + outputChild + " </optgroup>";
                                }
                            }
                            if( outputChild === '' ) {
                                html += "<option value='" + post.ID + "'>" + post.title + "</option>";
                            }

                        });

                    }
                    return html;
                }
                $('select.post_title-target_work', $form).off('change.mtkpi')
                    .on('change.mtkpi', function(ev) {
                        if(this.value == -1){
                            $('.content-offer-bank').slideDown();
                            $('select.choose-formula-target_work', $form).trigger('change.formula.nv');
                        }else{
                            $('.content-offer-bank').slideUp();
                        }
                });
                var $showOfferFormula = $('.offer-formula-custom .show-offer-formula');
                var $showFormulaDes = $('.offer-formula-custom .show-formula-des');
                $('select.choose-formula-target_work', $form).off('change.formula.nv')
                    .on('change.formula.nv', function(ev) {
                        if(this.value == 0){
                            $showOfferFormula.css('display','table');
                            $showFormulaDes.css('display', 'none');
                        }else{
                            $.ajax({
                                url: AJAX_URL,
                                type: 'get',
                                dataType: 'json',
                                data: {action: 'formula_by_id', ID: this.value },
                                beforeSend: function(xhr, settings) {
                                    $showFormulaDes.block(IMAGE_LOADING);
                                    $('.content', $showFormulaDes).html('');
                                    return;
                                },
                                error: function(xhr, status, err) {
                                    if( ('responseJSON' in xhr) && xhr.responseJSON && ('message' in xhr.responseJSON) ) {
                                        $('.content', $showFormulaDes).html(xhr.responseJSON.message);
                                    } else {
                                        $('.content', $showFormulaDes).html("Có lỗi xảy ra");
                                    }
                                    return;
                                },
                                success: function(response, status, xhr) {
                                    if( $.isPlainObject(response) && !$.isEmptyObject( response ) ){
                                        var dataDes = response;
                                        if( 'data' in response ){
                                            dataDes = response.data;
                                        }
                                        $('.content', $showFormulaDes).html( dataDes );
                                    }
                                    return;
                                },
                                complete: function(xhr, status) {
                                    $showFormulaDes.unblock(IMAGE_LOADING);
                                    return;
                                }
                            });
                            $showFormulaDes.css('display', 'table');
                            $showOfferFormula.css('display', 'none');
                        }
                    });
                $catBank.off('change.bank_kpi').on('change.bank_kpi', function(ev) {
                        var target = $(this).data('target'), loading = $(this).data('loading');
                        var $formPopup = $(this).closest('form');
                        var $target = $(target, $form), $loading = $($formPopup, $form);
                        if( $form[0].chartAjax && $form[0].chartAjax.abort ) {
                            $form[0].chartAjax.abort('Cancel Request Chart Name');
                        }
                        //var year = $(this).find(':selected').data('year');
                        var $response = $('.department-container', $formPopup);
                        $form[0].chartAjax = $.ajax({
                            url: AJAX_URL,
                            type: 'get',
                            dataType: 'json',
                            data: {action: 'kpi_list', cat: this.value },
                            beforeSend: function(xhr, settings) {
                                $response.addClass('hidden').removeClass('error').removeClass('success');
                                $bankSelector.block(IMAGE_LOADING);
                                $bankSelector.html('');
                                $bankSelector.selectpicker('refresh');
                                return;
                            },
                            error: function(xhr, status, err) {
                                $response.removeClass('hidden').addClass('error').removeClass('success');
                                if( ('responseJSON' in xhr) && xhr.responseJSON && ('message' in xhr.responseJSON) ) {
                                    $response.html(xhr.responseJSON.message);
                                } else {
                                    $response.html("Có lỗi xảy ra");
                                }
                                return;
                            },
                            success: function(response, status, xhr) {
                                if( $.isPlainObject(response) && !$.isEmptyObject( response ) ){
                                    var dataObj = response;
                                    if( 'items' in response ){
                                        dataObj = response.items;
                                    }
                                    var option = renderOption(dataObj, 0);
                                    option += "<optgroup label=\"Khác (Đề xuất mục tiêu KPI)\"> " +
                                        "<option value='-1'>Khác (Đề xuất mục tiêu KPI)</option>" +
                                        " </optgroup>";
                                    $bankSelector.html(option);
                                    if( 'bank_idPersonal' in self && self.bank_idPersonal > 0  ){
                                        $bankSelector.val( self.bank_idPersonal );
                                    }
                                    $bankSelector.selectpicker('refresh');
                                }
                                return;
                            },
                            complete: function(xhr, status) {
                                $bankSelector.unblock(IMAGE_LOADING);
                                $form[0].chartAjax = null;
                                return;
                            }
                        });
                        return;
                    });
                if( $btn.length > 0 && $btn.hasClass( 'action-edit' ) ){
                    self.initLoadKpiRegisterPersonal( $(this), $btn, $form );
                }else{
                    $catBank.trigger('change.bank_kpi');
                    $catBank.selectpicker('refresh');
                }
                $form.off('submit.target.kpi').on('submit.target.kpi', function(ev) {
                    ev.preventDefault();
                    var $response = $('.response-container', $form);
                    $form.ajaxSubmit({
                        beforeSend: function(xhr, settings) {
                            $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                            $form.block(IMAGE_LOADING);
                            return;
                        },
                        beforeSubmit: function(a, $frm, options) {
                            var valid = true, counter = 0, form = $frm[0], messages = [];
                            $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                            if( form.bank.value.trim() == '' ){
                                valid = false;
                                messages.push("Vui lòng chọn mục tiêu");
                            }
                            if( !valid ) {
                                $response.removeClass('hidden').addClass('error').html(messages.join('<br>'));
                            }
                            return valid;
                        },
                        error: function(xhr, status, error) {
                            $form.unblock(IMAGE_LOADING);
                            if( ('responseJSON' in xhr) && ('code' in xhr.responseJSON) ) {
                                $response.removeClass('hidden').addClass('error').html(xhr.responseJSON.message);
                            } else {
                                error = xhr.getResponseHeader('xhr-message') || error;
                                try{
                                    error = JSON.parse(error);
                                } catch(e){}
                                $response.removeClass('hidden').addClass('error').html(error);
                            }
                            return;
                        },
                        success: function(response, status, xhr) {
                            console.info(response);
                            if( xhr.status == 201 ) {
                                $response.removeClass('hidden').addClass('success').html("Lưu thành công");
                                location.reload();
                            }
                            return;
                        },
                        complete: function(xhr, status) {
                            $form.unblock(IMAGE_LOADING);
                            return;
                        }
                    });
                    return;
                });
            });
            $('[role="dialog"][id^="tpl-personal-target-"]', self.$element).off('show.bs.modal.personal').on('show.bs.modal.personal', function(showEv) {
                var $form = $('form', this), $btn = $(showEv.relatedTarget);
                ('resetForm' in $form) ? $form.resetForm() : null;
                $form.find('select.post_title-target_work').each(function(){
                    var loading = $(this).data('loading'),
                        $loading = $(loading, $form);
                    $loading.unblock(IMAGE_LOADING);
                    return;
                });
                if( $btn.length > 0 && $btn.hasClass( 'action-edit' ) ){
                    self.templateMonth = $('.content-fieldset-month', $form).clone();
                    self.initLoadKpiDataPersonal( $(this), $btn, $form );
                }else{

                }
                $form.off('submit.kpi').on('submit.kpi', function(ev) {
                    ev.preventDefault();
                    var $response = $('.response-container', $form);
                    $form.ajaxSubmit({
                        beforeSend: function(xhr, settings) {
                            $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                            $form.block(IMAGE_LOADING);
                            return;
                        },
                        beforeSubmit: function(a, $frm, options) {
                            var valid = true, counter = 0, form = $frm[0], messages = [];
                            $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                            /*if( form.plan.value.trim() == '' ) {
                                valid = false;
                                messages.push("Vui lòng nhập vào giá trị của mục tiêu cá nhân");
                            }*/
                            if( !valid ) {
                                $response.removeClass('hidden').addClass('error').html(messages.join('<br>'));
                            }
                            return valid;
                        },
                        error: function(xhr, status, error) {
                            $form.unblock(IMAGE_LOADING);
                            if( ('responseJSON' in xhr) && ('code' in xhr.responseJSON) ) {
                                $response.removeClass('hidden').addClass('error').html(xhr.responseJSON.message);
                            } else {
                                error = xhr.getResponseHeader('xhr-message') || error;
                                $response.removeClass('hidden').addClass('error').html(error);
                            }
                            return;
                        },
                        success: function(response, status, xhr) {
                            console.info(response);
                            if( xhr.status == 201 ) {
                                $response.removeClass('hidden').addClass('success').html("Lưu thành công");
                                location.reload();
                            }
                            return;
                        },
                        complete: function(xhr, status) {
                            $form.unblock(IMAGE_LOADING);
                            return;
                        }
                    });
                    return;
                });
                return;
            });

            $('[role="dialog"][id^="tpl-personal-target-precious-"]', self.$element).off('show.bs.modal.personal.precious').on('show.bs.modal.personal.precious', function(showEv) {
                var $form = $('form', this), $btn = $(showEv.relatedTarget);
                ('resetForm' in $form) ? $form.resetForm() : null;
                $form.find('select.post_title-target_work').each(function(){
                    var loading = $(this).data('loading'),
                        $loading = $(loading, $form);
                    $loading.unblock(IMAGE_LOADING);
                    return;
                });
                if( $btn.length > 0 && $btn.hasClass( 'action-edit' ) ){
                    self.templatePrecious = $('.content-fieldset-precious', $form).clone();
                    self.initLoadKpiDataPersonalPrecious( $(this), $btn, $form );
                }else{

                }
                $form.off('submit.kpi').on('submit.kpi', function(ev) {
                    ev.preventDefault();
                    var $response = $('.response-container', $form);
                    $form.ajaxSubmit({
                        beforeSend: function(xhr, settings) {
                            $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                            $form.block(IMAGE_LOADING);
                            return;
                        },
                        beforeSubmit: function(a, $frm, options) {
                            var valid = true, counter = 0, form = $frm[0], messages = [];
                            $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                            /*if( form.plan.value.trim() == '' ) {
                                valid = false;
                                messages.push("Vui lòng nhập vào giá trị của mục tiêu cá nhân");
                            }*/
                            if( !valid ) {
                                $response.removeClass('hidden').addClass('error').html(messages.join('<br>'));
                            }
                            return valid;
                        },
                        error: function(xhr, status, error) {
                            $form.unblock(IMAGE_LOADING);
                            if( ('responseJSON' in xhr) && ('code' in xhr.responseJSON) ) {
                                $response.removeClass('hidden').addClass('error').html(xhr.responseJSON.message);
                            } else {
                                error = xhr.getResponseHeader('xhr-message') || error;
                                try{
                                    error = JSON.parse(error);
                                } catch(e){}
                                $response.removeClass('hidden').addClass('error').html(error);
                            }
                            return;
                        },
                        success: function(response, status, xhr) {
                            console.info(response);
                            if( xhr.status == 201 ) {
                                $response.removeClass('hidden').addClass('success').html("Lưu thành công");
                                location.reload();
                            }
                            return;
                        },
                        complete: function(xhr, status) {
                            $form.unblock(IMAGE_LOADING);
                            return;
                        }
                    });
                    return;
                });
                return;
            });

            $('.action-remove', self.$element).off('click.remove_personal_target').on('click.remove_personal_target', function(ev){
                ev.preventDefault();
                var data = $(this).data();
                var $dialogConfirm = $(data.target);
                $dialogConfirm.find('.modal-title').text(data.title);
                var options = {
                    url: AJAX_URL,
                    data: {action: data.action, id: data.id},
                    type: data.method,
                    dataType: "json",
                    beforeSend: function () {
                        $dialogConfirm.find('.notification').remove();
                        $('body').block(IMAGE_LOADING);
                    },
                    success: function(response, status, xhr){
                        ///console.log( 'SUCCESS',response, status, xhr );
                        if( $.isPlainObject(response) || 'success' in response ){
                            window.location.reload();
                        }
                        return;
                    },
                    error: function(xhr, status, error){
                        //console.log( 'ERROR',xhr, status, errThrow );
                        var textError = '';
                        if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                            textError = xhr.responseJSON.error;
                        }else if( error != '' ){
                            textError = xhr.getResponseHeader('xhr-message') || error;
                            try{
                                textError = JSON.parse(textError);
                            } catch(e){}
                        }
                        var $err = $('<label class="notification error"> ' + textError + ' </label>');
                        $dialogConfirm.find('.modal-body').prepend( $err );
                        return;
                    },
                    complete: function(xhr, status){
                        //console.log( 'Complete', xhr, status  );
                        self.xhr_kpi = null;
                        $('body').unblock(IMAGE_LOADING);
                        return;
                    }
                }
                self.bindEventDialogConfirm( options, data.target );
            });
        },
        popupCapacity: function(){
            var self = this;
            var removeCapacity = function(data, $element){
                var $dialogConfirm = $(data.target);
                $dialogConfirm.find('.modal-title').text(data.title);
                var options = {
                    url: AJAX_URL,
                    data: {action: data.action, id: data.id, _wpnonce: data.wp_nonce},
                    type: data.method,
                    dataType: "json",
                    beforeSend: function () {
                        $dialogConfirm.find('.notification').remove();
                        $('body').block(IMAGE_LOADING);
                    },
                    success: function(response, status, xhr){
                        ///console.log( 'SUCCESS',response, status, xhr );
                        if( $.isPlainObject(response) || 'success' in response ){
                            //window.location.reload();
                            $dialogConfirm.modal('hide');
                            $element.remove();
                            $('.table-list-detail.table-managerment-capacity > tbody > tr').each(function(idx, tr){
                                $(tr).find('td.column-1').html(idx + 1);
                            });
                        }
                        return;
                    },
                    error: function(xhr, status, errThrow){
                        //console.log( 'ERROR',xhr, status, errThrow );
                        var textError = '';
                        if( ('responseJSON' in xhr) && ( 'message' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                            textError = xhr.responseJSON.message;
                        }else if( errThrow != '' ){
                            textError = xhr.getResponseHeader('xhr-message') || errThrow;
                            try{
                                textError = JSON.parse(textError);
                            } catch(e){}
                        }
                        var $err = $('<label class="notification error"> ' + textError + ' </label>');
                        $dialogConfirm.find('.modal-body').prepend( $err );
                        return;
                    },
                    complete: function(xhr, status){
                        //console.log( 'Complete', xhr, status  );
                        self.xhr_kpi = null;
                        $('body').unblock(IMAGE_LOADING);
                        return;
                    }
                }
                self.bindEventDialogConfirm( options, data.target );
            };

            $('[role="dialog"][id^="add-management-capacity"]', self.$element).off('hide.bs.modal.capacity_manager').on('hide.bs.modal.capacity_manager', function(ev){
                var $form = $('form', this);
                var $response = $('.response-container', $form);
                $response.addClass('hidden').removeClass('error').removeClass('success').html('');
            });
            $('[role="dialog"][id^="add-management-capacity"]', self.$element).off('show.bs.modal.add-management-capacity').on('show.bs.modal.add-management-capacity', function(showEv) {
                var $popup = $(this);
                var $form = $('form', this), $btn = $(showEv.relatedTarget);
                var $response = $('.response-container', $form);
                var $bankSelector = $('select.bank_id', $form);
                ('resetForm' in $form) ? $form.resetForm() : null;
                self.eventChangeBankKpi($form);
                var $this_tr = '';
                if( $btn.length > 0 && $btn.hasClass( 'action-edit' ) ){
                    $this_tr = $btn.closest("tr");
                    self.initLoadCapacity( $(this), $btn, $form );
                }else{
                    $form.find("input[name='id']").val('');
                    $form.find('input[name="status"]').prop("checked", false);
                    $bankSelector.trigger('change.capacity.bank.kpi');
                }
                $form.off('submit.add-management-capacity').on('submit.add-management-capacity', function(ev) {
                    ev.preventDefault();
                    $form.ajaxSubmit({
                        beforeSend: function(xhr, settings) {
                            $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                            $form.block(IMAGE_LOADING);
                            return;
                        },
                        beforeSubmit: function(a, $frm, options) {
                            var valid = true, counter = 0, form = $frm[0], messages = [];
                            $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                            /*if( form.plan.value.trim() == '' ) {
                                valid = false;
                                messages.push("Vui lòng nhập vào giá trị của mục tiêu cá nhân");
                            }*/
                            if( !valid ) {
                                $response.removeClass('hidden').addClass('error').html(messages.join('<br>'));
                            }
                            return valid;
                        },
                        error: function(xhr, status, error) {
                            $form.unblock(IMAGE_LOADING);
                            if( ('responseJSON' in xhr) && ('code' in xhr.responseJSON) ) {
                                $response.removeClass('hidden').addClass('error').html(xhr.responseJSON.message);
                            } else {
                                error = xhr.getResponseHeader('xhr-message') || error;
                                try{
                                    error = JSON.parse(error);
                                } catch(e){}
                                $response.removeClass('hidden').addClass('error').html(error);
                            }
                            return;
                        },
                        success: function(response, status, xhr) {
                            //console.info(response);
                            if( xhr.status == 201 ) {
                                $response.removeClass('hidden').addClass('success').html("Lưu thành công");
                                //location.reload();
                                if( $.isPlainObject(response) && 'data' in response && !$.isEmptyObject(response.data.html) ){
                                    if( response.data.action == 'add' ){
                                        var row_number = $('.table-list-detail.table-managerment-capacity tbody > tr').length;
                                        var html = response.data.html;
                                        var html = html.replace(/(IDX)/i, row_number + 1);
                                        $('.table-list-detail.table-managerment-capacity').append( html );
                                        //$('.frm-add-kpi-year').find('.btn-save-assign').removeClass('hidden');
                                    }else{
                                        var html = response.data.html;
                                        html = html.replace(/(IDX)/i, $this_tr.index() + 1);
                                        $this_tr.html(html);

                                    }
                                    $('.table-managerment-capacity .delete', self.$element).off('click.remove_management_capacity').on('click.remove_management_capacity', function(ev){
                                        ev.preventDefault();
                                        var data = $(this).data();
                                        removeCapacity(data, $(this).closest("tr"));
                                    });
                                }
                                $popup.modal('hide');
                            }
                            return;
                        },
                        complete: function(xhr, status) {
                            $form.unblock(IMAGE_LOADING);
                            return;
                        }
                    });
                    return;
                });

                return;
            });
            $('.table-managerment-capacity .delete', self.$element).off('click.remove_management_capacity').on('click.remove_management_capacity', function(ev){
                ev.preventDefault();
                var data = $(this).data();
                removeCapacity(data, $(this).closest("tr"));
            });
            self.$element.off('change.change.status', '.table-managerment-capacity . .checkbox-status').on('change.change.status', '.table-managerment-capacity .checkbox-status', function(ev){
                ev.preventDefault();
                var data = $(this).data();
                var $parentTD = $(this).closest('td');
                var $parentTR = $(this).closest('tr');
                var status = $(this).prop('checked');
                status = status ? $(this).val() : '';
                $.ajax({
                    url: AJAX_URL,
                    data: {action: 'change_status_capacity', id: data.id, status: status},
                    dataType: 'json',
                    type: 'post',
                    beforeSend: function () {
                        $parentTR.block(IMAGE_LOADING);
                        $parentTD.find('.notification').remove();
                    },
                    success: function(response, status, xhr){
                        ///console.log( 'SUCCESS',response, status, xhr );
                        if( $.isPlainObject(response) || 'success' in response ){
                            //window.location.reload();
                            var $notification = $('<span class="notification success">' + response.success + '</span>');
                            $parentTD.append( $notification );
                            var notifiTimeout;
                            if( notifiTimeout !== undefined ){
                                clearTimeout( notifiTimeout );
                            }
                            notifiTimeout = setTimeout(function(){
                                $parentTD.find('.notification').remove();
                            }, 3000);
                        }
                        return;
                    },
                    error: function(xhr, status, errThrow){
                        //console.log( 'ERROR',xhr, status, errThrow );
                        var textError = '';
                        if( ('responseJSON' in xhr) ){
                            if( 'error' in xhr.responseJSON ) {
                                textError = xhr.responseJSON.error;
                            } else if( 'message' in xhr.responseJSON ) {
                                textError = xhr.responseJSON.message;
                            }
                        }else if( errThrow != '' ){
                            textError = xhr.getResponseHeader('xhr-message') || errThrow;
                            try{
                                textError = JSON.parse(textError);
                            } catch(e){}
                        }
                        var $err = $('<span class="notification error"> ' + textError + ' </span>');
                        $parentTD.append( $err );
                        return;
                    },
                    complete: function(xhr, status){
                        //console.log( 'Complete', xhr, status  );
                        $parentTR.unblock(IMAGE_LOADING);
                        return;
                    }
                });
                return this;
            });
            $(".frm-add-kpi-year", self.$element).off('submit.save.capacity').on('submit.save.capacity', function(ev){
                ev.preventDefault();
                var $form = $(this);
                var $response = $form.find('.response-container');
                var options = {
                    beforeSend: function(xhr, settings) {
                        $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                        $form.block(IMAGE_LOADING);
                        return;
                    },
                    error: function(xhr, status, error) {
                        if( ('responseJSON' in xhr) && ('message' in xhr.responseJSON) ) {
                            $response.removeClass('hidden').addClass('error').html(xhr.responseJSON.message);
                        } else {
                            error = xhr.getResponseHeader('xhr-message') || error;
                            try{
                                error = JSON.parse(error);
                            } catch(e){}
                            $response.removeClass('hidden').addClass('error').html(error);
                        }
                        return;
                    },
                    success: function(response, status, xhr) {
                        if( xhr.status == 201 ) {
                            $response.removeClass('hidden').addClass('success').html("Lưu thành công");
                            if( $.isPlainObject(response) && 'location' in response ){
                                location.href = response.location;
                                location.reload();
                            }else {
                                location.reload();
                            }
                        }
                        return;
                    },
                    complete: function(xhr, status) {
                        $form.unblock(IMAGE_LOADING);
                        return;
                    }
                };
                $form.ajaxSubmit(options);
                return false;
            });
            $(".frm-add-kpi-year button.btn-save-assign", self.$element).off('click.assign.capacity').on('click.assign.capacity', function(ev){
                ev.preventDefault();
                var $form = $(this).closest('form');
                var $response = $form.find('.response-container');
                var data = $(this).data();
                var action = AJAX_URL + "?action="+data.action;
                var bk_action = $form.attr('action');
                $form.attr('action', action);
                var options = {
                    beforeSend: function(xhr, settings) {
                        $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                        $form.block(IMAGE_LOADING);
                        return;
                    },
                    error: function(xhr, status, error) {
                        if( ('responseJSON' in xhr) && ('message' in xhr.responseJSON) ) {
                            $response.removeClass('hidden').addClass('error').html(xhr.responseJSON.message);
                        } else {
                            error = xhr.getResponseHeader('xhr-message') || error;
                            try{
                                error = JSON.parse(error);
                            } catch(e){}
                            $response.removeClass('hidden').addClass('error').html(error);
                        }
                        return;
                    },
                    success: function(response, status, xhr) {
                        if( xhr.status == 201 ) {
                            $response.removeClass('hidden').addClass('success').html("Lưu thành công");
                            location.reload();
                        }
                        return;
                    },
                    complete: function(xhr, status) {
                        $form.unblock(IMAGE_LOADING);
                        $form.attr('action', bk_action);
                        return;
                    }
                };
                $form.ajaxSubmit(options);
                return false;
            });
            $(".frm-add-kpi-year button.btn-export-capacity-year", self.$element).off('click.export.capacity').on('click.export.capacity', function(ev){
                ev.preventDefault();
                var $form = $(this).closest('form');
                var $response = $form.find('.response-container');
                var data = $(this).data();
                var options = {
                    url: data.action,
                    data: {_wpnonce: data.wpnonce},
                    type: data.method,
                    dataType: 'json',
                    beforeSend: function(xhr, settings) {
                        $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                        $form.block(IMAGE_LOADING);
                        return;
                    },
                    error: function(xhr, status, error) {
                        if( ('responseJSON' in xhr) && ('message' in xhr.responseJSON) ) {
                            $response.removeClass('hidden').addClass('error').html(xhr.responseJSON.message);
                        } else {
                            error = xhr.getResponseHeader('xhr-message') || error;
                            try{
                                error = JSON.parse(error);
                            } catch(e){}
                            $response.removeClass('hidden').addClass('error').html(error);
                        }
                        return;
                    },
                    success: function(response, status, xhr) {
                        if( xhr.status == 201 ) {
                            $response.removeClass('hidden').addClass('success').html("Lưu thành công");
                            //location.reload();
                        }
                        return;
                    },
                    complete: function(xhr, status) {
                        $form.unblock(IMAGE_LOADING);
                        return;
                    }
                };
                $.ajax(options);
                return false;
            });
        },
        initBehavior: function(){
            var self = this;
            var $behavior_percent = $("#behavior-percent");
            var bpvalue = 0;
            var $behaviorList = $('.table-managerment-behavior', self.$element);
            if( $behaviorList.length > 0 ) {
                var $formList = $behaviorList.closest('form');
                var behaviorData = $behaviorList.data();
                if ($formList.find('#checksendresult').length > 0) {
                    $behaviorList.find('.status-behavior').removeClass('hidden');
                    $behaviorList.find('thead.group-behavior-item > tr > td:last-child').attr('colspan', 5);
                }
                if (behaviorData && ('action' in behaviorData) && (behaviorData.action != '')) {
                    $.ajax({
                        url: behaviorData.action,
                        type: 'GET',
                        dataType: 'json',
                        beforeSend: function (xhr, settings) {
                            $behaviorList.block(IMAGE_LOADING);
                            return;
                        },
                        error: function (xhr, status, err) {
                            $response.removeClass('hidden').addClass('error').removeClass('success');
                            if (('responseJSON' in xhr) && xhr.responseJSON && ('message' in xhr.responseJSON)) {
                                //$response.html(xhr.responseJSON.message);
                                console.log(xhr.responseJSON.message);
                            }
                            return;
                        },
                        success: function (response, status, xhr) {
                            if (xhr.status == 201 && $.isPlainObject(response) && 'html' in response) {
                                $behaviorList.append(response.html);
                            }

                            return;
                        },
                        complete: function (xhr, status) {
                            $behaviorList.unblock(IMAGE_LOADING);
                            return;
                        }
                    });
                }
            }

            $('.behavior-rollback', self.$element).off('click.rollback.result').on('click.rollback.result', function(ev){
               ev.preventDefault();
                var $rollback = $(this);
                var $form = $(this).closest("form");
                var data = $(this).data();
                var $dialogConfirm = $(data.target);
                $dialogConfirm.find('.modal-title').text(data.title);
                var options = {
                    url: data.action,
                    type: 'post',
                    dataType: 'json',
                    beforeSend: function (xhr, settings) {
                        $form.block(IMAGE_LOADING);
                        return;
                    },
                    error: function (xhr, status, err) {
                        var textError = '';
                        if( ('responseJSON' in xhr) && ( 'message' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                            textError = xhr.responseJSON.message;
                        }else if( errThrow != '' ){
                            textError = xhr.getResponseHeader('xhr-message') || errThrow;
                            try{
                                textError = JSON.parse(textError);
                            } catch(e){}
                        }
                        var $err = $('<label class="notification error"> ' + textError + ' </label>');
                        $dialogConfirm.find('.modal-body').prepend( $err );
                        return;
                    },
                    success: function (response, status, xhr) {
                        if (xhr.status == 201 && $.isPlainObject(response) && 'message' in response) {
                            var $tr = $rollback.closest('tr');
                            var $notification = $tr.find('.notification')
                            $notification.removeClass('error');
                            $notification.text('Chưa duyệt');
                            $rollback.remove();
                        }

                        return;
                    },
                    complete: function (xhr, status) {
                        $form.unblock(IMAGE_LOADING);
                        return;
                    }
                };
                self.bindEventDialogConfirm(options, data.target);
            });
            $('.checkall', $('.table-managerment-behavior')).off('change.checkall').on('change.checkall', function (ev) {
                 ev.preventDefault();
                 var checked = $(this).prop('checked');
                 var $table = $(this).closest('table');
                 $table.find('td.column-0 .switch input[type="checkbox"]').prop('checked', checked);

            });
            $behavior_percent.off('focusout.behavior.percent').on('focusout.behavior.percent', function(ev){
                var percent = this.value;
                if( bpvalue != percent ) {
                    var data = $(this).data();
                    var options = {
                        url: data.action,
                        type: data.method,
                        data: {behavior_percent: percent},
                        dataType: 'json',
                        beforeSend: function (xhr, settings) {
                            //$form.block(IMAGE_LOADING);
                            return;
                        },
                        error: function (xhr, status, err) {
                            $response.removeClass('hidden').addClass('error').removeClass('success');
                            if (('responseJSON' in xhr) && xhr.responseJSON && ('message' in xhr.responseJSON)) {
                                //$response.html(xhr.responseJSON.message);
                                console.log(xhr.responseJSON.message);
                            }
                            return;
                        },
                        success: function (response, status, xhr) {
                            if (xhr.status == 201 && $.isPlainObject(response) && 'message' in response) {
                                bpvalue = percent;
                                console.log(response.message);
                            }

                            return;
                        },
                        complete: function (xhr, status) {
                            return;
                        }
                    };
                    $.ajax(options);
                }
            });
            $('[role="dialog"][id^="add-management-behavior"]', self.$element).off('show.bs.modal.add-management-behavior').on('show.bs.modal.add-management-behavior', function(showEv) {
                var $form = $('form', this), $btn = $(showEv.relatedTarget);
                var $bankSelector = $('select.bank_id', $form);
                ('resetForm' in $form) ? $form.resetForm() : null;
                self.eventBehaviorChangeBankKpi($form);
                var dataForm = $form.data();
                var action_add = dataForm.actionAdd;
                var action_edit = dataForm.actionEdit;
                if( $btn.length > 0 && $btn.hasClass( 'action-edit' ) ){
                    $form.attr('action', action_edit);
                    self.initLoadBehavior( $(this), $btn, $form );
                }else{
                    $form.attr('action', action_add);
                    $form.find("input[name='id']").val('');
                    $bankSelector.trigger('change.behavior.bank.kpi');
                }

                $form.off('submit.add-management-behavior').on('submit.add-management-behavior', function(ev) {
                    ev.preventDefault();
                    var $response = $('.response-container', $form);
                    $form.ajaxSubmit({
                        beforeSend: function(xhr, settings) {
                            $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                            $form.block(IMAGE_LOADING);
                            return;
                        },
                        beforeSubmit: function(a, $frm, options) {
                            var valid = true, counter = 0, form = $frm[0], messages = [];
                            $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                            /*if( form.plan.value.trim() == '' ) {
                                valid = false;
                                messages.push("Vui lòng nhập vào giá trị của mục tiêu cá nhân");
                            }*/
                            if( !valid ) {
                                $response.removeClass('hidden').addClass('error').html(messages.join('<br>'));
                            }
                            return valid;
                        },
                        error: function(xhr, status, error) {
                            $form.unblock(IMAGE_LOADING);
                            if( ('responseJSON' in xhr) && ('code' in xhr.responseJSON) ) {
                                $response.removeClass('hidden').addClass('error').html(xhr.responseJSON.message);
                            } else {
                                error = xhr.getResponseHeader('xhr-message') || error;
                                try{
                                    error = JSON.parse(error);
                                } catch(e){}
                                $response.removeClass('hidden').addClass('error').html(error);
                            }
                            return;
                        },
                        success: function(response, status, xhr) {
                            console.info(response);
                            if( xhr.status == 201 ) {
                                $response.removeClass('hidden').addClass('success').html("Lưu thành công");
                                location.reload();
                            }
                            return;
                        },
                        complete: function(xhr, status) {
                            $form.unblock(IMAGE_LOADING);
                            return;
                        }
                    });
                    return;
                });

                return;
            });
            $('.table-managerment-behavior .delete', self.$element).off('click.remove_management_behavior').on('click.remove_management_behavior', function(ev){
                ev.preventDefault();
                var data = $(this).data();
                var $dialogConfirm = $(data.target);
                $dialogConfirm.find('.modal-title').text(data.title);
                var options = {
                    url: AJAX_URL,
                    data: {action: data.action, id: data.id, _wpnonce: data.wp_nonce},
                    type: data.method,
                    dataType: "json",
                    beforeSend: function () {
                        $dialogConfirm.find('.notification').remove();
                        $('body').block(IMAGE_LOADING);
                    },
                    success: function(response, status, xhr){
                        ///console.log( 'SUCCESS',response, status, xhr );
                        if( $.isPlainObject(response) || 'success' in response ){
                            window.location.reload();
                        }
                        return;
                    },
                    error: function(xhr, status, errThrow){
                        //console.log( 'ERROR',xhr, status, errThrow );
                        var textError = '';
                        if( ('responseJSON' in xhr) && ( 'message' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                            textError = xhr.responseJSON.message;
                        }else if( errThrow != '' ){
                            textError = xhr.getResponseHeader('xhr-message') || errThrow;
                            try{
                                textError = JSON.parse(textError);
                            } catch(e){}
                        }
                        var $err = $('<label class="notification error"> ' + textError + ' </label>');
                        $dialogConfirm.find('.modal-body').prepend( $err );
                        return;
                    },
                    complete: function(xhr, status){
                        //console.log( 'Complete', xhr, status  );
                        self.xhr_kpi = null;
                        $('body').unblock(IMAGE_LOADING);
                        return;
                    }
                }
                self.bindEventDialogConfirm( options, data.target );
            });
            $(".group-item-down", self.$element).off("click.dropdown.item").on("click.dropdown.item", function(ev){
                ev.preventDefault();
                var $icon = $(this).find('i');
                var $tbody_next = $(this).closest('thead').next();
                if( $tbody_next[0].nodeName == 'TBODY' ){
                    $tbody_next.slideToggle();
                }
                $icon.toggleClass('fa-chevron-circle-down');

            });
            function checkSiblings ($el, checked) {
                var parent = $el.closest('li'),
                    all = true,
                    indeterminate = false;

                $el.siblings().each(function() {
                    return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
                });

                if (all && checked) {
                    parent.children('input[type="checkbox"]')
                        .prop({
                            checked: checked
                        })
                        .siblings('label');

                    checkSiblings(parent, checked);
                }
                else if (all && !checked) {
                    indeterminate = parent.find('input[type="checkbox"]:checked').length > 0;

                    parent.children('input[type="checkbox"]')
                        .prop("checked", checked)
                        .siblings('label');

                    checkSiblings(parent, checked);
                }
                else {
                    $el.parents("li").children('input[type="checkbox"]')
                        .prop({
                            checked: false
                        })
                        .siblings('label');
                }
            }
            $('.treeview input[type="checkbox"]', self.$element).off('change.check.chart').on('change.check.chart', function(ev){
                var $this = $(this),
                    $container = $this.closest('li'),
                    checked = $this.prop('checked'),
                    $siblings = $container.siblings();
                $container.find('input[type="checkbox"]')
                    .prop({
                        checked: checked
                    });
                var $parentsLi = $container.parents('li');
                var $parentUl = $container.closest('ul');
                if( $parentsLi.length > 0 ){
                    if($parentUl.find('input[type="checkbox"]:checked').length > 0 ){
                        var $children = $parentsLi.children();
                        if( $children.length > 0 ){
                            $($children[0]).find('input[type="checkbox"]').prop({
                                checked: true
                            });
                        }
                    }else{
                        $parentsLi.find('input[type="checkbox"]')
                            .prop({
                                checked: checked
                            });
                    }
                }
                //checkSiblings($container, checked);
            });
        },
        eventChangeBankKpi: function($form) {
            var self = this, $responseMsg = $('.response-container', $form);
            ('resetForm' in $form) ? $form.resetForm() : null;
            // $form[0].cat.value = '';
            var $cates = $form.find('select.bank_id');
            // $cates.trigger('change');
            $cates.each(function(){
                this.chartAjax = null;
            });
            $cates.unbind('change.capacity.bank.kpi').bind('change.capacity.bank.kpi', function(ev) {
                var selectCat = this, $cat = $(this), target = $(this).data('target'), loading = $(this).data('loading');
                if( selectCat.chartAjax && selectCat.chartAjax.abort ) {
                    selectCat.chartAjax.abort('Cancel Request Chart Name');
                }
                var $response = $('.response-container', $form);

                selectCat.chartAjax = $.ajax({
                    url: AJAX_URL,
                    type: 'get',
                    dataType: 'json',
                    data: {action: 'capacity_get_bank', ID: this.value},
                    beforeSend: function(xhr, settings) {
                        $form.block(IMAGE_LOADING);
                        return;
                    },
                    error: function(xhr, status, err) {
                        $response.removeClass('hidden').addClass('error').removeClass('success');
                        if( ('responseJSON' in xhr) && xhr.responseJSON && ('message' in xhr.responseJSON) ) {
                            $response.html(xhr.responseJSON.message);
                        }
                        return;
                    },
                    success: function(response, status, xhr) {
                        if( xhr.status == 201 && $.isPlainObject( response ) && 'data' in response ) {
                            var dataRes = response.data;
                            Object.keys(dataRes).forEach( function(item){
                                $form.find('input[name="'+ item +'"]').val(dataRes[item]);
                            });
                        }

                        return;
                    },
                    complete: function(xhr, status) {
                        $form.unblock(IMAGE_LOADING);
                        selectCat.chartAjax = null;
                        return;
                    }
                });
                return;
            });
            return self;
        },
        eventBehaviorChangeBankKpi: function($form) {
            var self = this, $responseMsg = $('.response-container', $form);
            ('resetForm' in $form) ? $form.resetForm() : null;
            // $form[0].cat.value = '';
            var $cates = $form.find('select.bank_id');
            // $cates.trigger('change');
            $cates.each(function(){
                this.chartAjax = null;
            });
            $cates.unbind('change.behavior.bank.kpi').bind('change.behavior.bank.kpi', function(ev) {
                var selectCat = this, $cat = $(this), target = $(this).data('target'), loading = $(this).data('loading');
                if( selectCat.chartAjax && selectCat.chartAjax.abort ) {
                    selectCat.chartAjax.abort('Cancel Request Chart Name');
                }
                var $response = $('.response-container', $form);

                selectCat.chartAjax = $.ajax({
                    url: AJAX_URL,
                    type: 'get',
                    dataType: 'json',
                    data: {action: 'behavior_get_bank', ID: this.value},
                    beforeSend: function(xhr, settings) {
                        $form.block(IMAGE_LOADING);
                        return;
                    },
                    error: function(xhr, status, err) {
                        $response.removeClass('hidden').addClass('error').removeClass('success');
                        if( ('responseJSON' in xhr) && xhr.responseJSON && ('message' in xhr.responseJSON) ) {
                            $response.html(xhr.responseJSON.message);
                        }
                        return;
                    },
                    success: function(response, status, xhr) {
                        if( xhr.status == 201 && $.isPlainObject( response ) && 'data' in response ) {
                            var dataRes = response.data;
                            Object.keys(dataRes).forEach( function(item){
                                $form.find('input[name="'+ item +'"]').val(dataRes[item]);
                            });
                        }

                        return;
                    },
                    complete: function(xhr, status) {
                        $form.unblock(IMAGE_LOADING);
                        selectCat.chartAjax = null;
                        return;
                    }
                });
                return;
            });
            return self;
        },
        initFormChangeKpi: function($form, type, kpiId) {
            var self = this, $responseMsg = $('.response-container', $form);
            ('resetForm' in $form) ? $form.resetForm() : null;
            // $form[0].cat.value = '';
            self.initDate($form);
            if( !kpiId ) {
                $form.find('input[name="kpiID"]').remove();
            }
            var $cates = $form.find('select.kpi-category');
            $cates.removeData('bank_id');
            $cates.val('');
            $cates.selectpicker('val', '');
            // $cates.trigger('change');
            $cates.each(function(){
                this.chartAjax = null;
            });
            $cates.unbind('change.kpi').bind('change.kpi', function(ev) {
                var selectCat = this, $cat = $(this), target = $(this).data('target'), loading = $(this).data('loading');
                var $target = $(target, $form), $loading = $(this).closest(loading);
                if( selectCat.chartAjax && selectCat.chartAjax.abort ) {
                    selectCat.chartAjax.abort('Cancel Request Chart Name');
                }

                var $response = $('.department-container', $form);
                selectCat.chartAjax = $.ajax({
                    url: AJAX_URL,
                    type: 'get',
                    dataType: 'json',
                    data: {action: 'kpi_list', cat: this.value},
                    beforeSend: function(xhr, settings) {
                        $response.addClass('hidden').removeClass('error').removeClass('success');
                        $loading.length ? $loading.block(IMAGE_LOADING) : null;
                        return;
                    },
                    error: function(xhr, status, err) {
                        $response.removeClass('hidden').addClass('error').removeClass('success');
                        if( ('responseJSON' in xhr) && xhr.responseJSON && ('message' in xhr.responseJSON) ) {
                            $response.html(xhr.responseJSON.message);
                        } else {
                            $response.html("Có lỗi xảy ra");
                        }
                        return;
                    },
                    success: function(responseJson, status, xhr) {
                        if( xhr.status != 202 ) {
                            return;
                        }
                        var $option = $target.find('option:selected'), response = responseJson;
                        $option.remove();
                        $target.empty();
                        if( 'items' in responseJson ) {
                            response = responseJson.items;
                        }
                        var keys = Object.keys(response);
                        if( keys.length == 0 ) {
                            $response.removeClass('hidden').removeClass('error').addClass('success');
                            $response.html("Không tìm thấy mục tiêu tương ứng chức danh này");
                        } else {
                            var html = self.renderItemKpi( response, 0, 0 );
                            $target.append( html );
                            var $selected = $target.find('option[value="'+$option.val()+'"]');
                            if( !$selected.length ) {
                                $target.prepend($option);
                            }
                            var bank_id = $cat.data('bank_id');
                            if( bank_id ) {
                                $target.find('option[value="' + bank_id + '"]').prop('selected', true);
                            }
                            $target.selectpicker( 'refresh' );
                        }
                        return;
                    },
                    complete: function(xhr, status) {
                        $loading.length ? $loading.unblock(IMAGE_LOADING) : null;
                        selectCat.chartAjax = null;
                        return;
                    }
                });
                return;
            });
            return self;
        },
        inputFile: function(){
            var self = this;
            var inputs = document.querySelectorAll( '.inputfile' );
            Array.prototype.forEach.call( inputs, function( input )
            {
                var label	 = input.nextElementSibling,
                    labelVal = label.innerHTML;

                input.addEventListener( 'change', function( e )
                {
                    var fileName = '';
                    var $this = $(this);
                    $this.closest('td').find('.notification').remove();
                    if( this.files && this.files.length > 1 ) {
                        var files = this.files;
                        var data = $(this).data();
                        Object.keys(files).forEach(function (idx) {
                            var file = files[idx];
                            // 5M
                            if (file.size > 5242880) {
                                $this.closest('td').prepend('<label class="notification error"> ' + data.errorSize + ' </label>');
                            }
                        });
                        fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                    }else {
                        fileName = e.target.value.split('\\').pop();
                    }
                    if( fileName )
                        label.querySelector( 'span' ).innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });
            });
            $('.view-file-upload', self.$element).off('click.view.upload').on('click.view.upload', function(ev){
                ev.preventDefault();
                var data = $(this).data();
                var $popup = $(data.target);

                $popup.off('show.bs.modal').on('show.bs.modal', function(){
                    var $form = $('form', $(this));
                    var $response = $('.response-container', $form);
                    var textChooseBK = $form.find('th.column-3').text();
                    $form.find('th.column-3').text('');
                    $form.find( 'button.btn-cancel' ).hide();
                    var options = {
                        url: data.action,
                        dataType: 'json',
                        type: 'get',
                        beforeSend: function(xhr, settings) {
                            $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                            $form.block(IMAGE_LOADING);
                            return;
                        },
                        error: function(xhr, status, error) {
                            //$form.unblock(IMAGE_LOADING);
                            if( ('responseJSON' in xhr) && ('code' in xhr.responseJSON) ) {
                                $response.removeClass('hidden').addClass('error').html(xhr.responseJSON.message);
                            } else {
                                error = xhr.getResponseHeader('xhr-message') || error;
                                try{
                                    error = JSON.parse(error);
                                } catch(e){}
                                $response.removeClass('hidden').addClass('error').html(error);
                            }
                            return;
                        },
                        success: function(response, status, xhr) {
                            if( $.isPlainObject( response ) && 'data' in response ) {
                                $form.find('.tbody-main').html( response.data );
                            }
                            if( $.isPlainObject( response ) && 'complete' in response ){
                                if( response.complete == '' || response.complete == undefined ){
                                    $form.find('th.column-3').text( textChooseBK );
                                    $form.find( 'button.btn-cancel' ).show();
                                }
                            }
                            return;
                        },
                        complete: function(xhr, status) {
                            $form.unblock(IMAGE_LOADING);
                            return;
                        }
                    };
                    $.ajax(options);
                    $form.find('[name="id"]').val( data.id );
                    $form.off('submit.delete.files').on('submit.delete.files', function(){
                        $form.ajaxSubmit({
                            beforeSend: function(xhr, settings) {
                                $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                                $form.block(IMAGE_LOADING);
                                return;
                            },
                            error: function(xhr, status, error) {
                                if( ('responseJSON' in xhr) && ('code' in xhr.responseJSON) ) {
                                    $response.removeClass('hidden').addClass('error').html(xhr.responseJSON.message);
                                } else {
                                    error = xhr.getResponseHeader('xhr-message') || error;
                                    try{
                                        error = JSON.parse(error);
                                    } catch(e){}
                                    $response.removeClass('hidden').addClass('error').html(error);
                                }
                                return;
                            },
                            success: function(response, status, xhr) {
                                if( xhr.status == 201 ) {
                                    $response.removeClass('hidden').addClass('success').html("Lưu thành công");
                                    location.reload();
                                }
                                return;
                            },
                            complete: function(xhr, status) {
                                $form.unblock(IMAGE_LOADING);
                                return;
                            }
                        });
                        return false;
                    });
                });
                $popup.modal('show');

            });
        },
        "create-kpi": function($target, ev) {
            var self = this, options = $this.data();
            options.$target = $this;
            self.ajax(options);
            return self;
        },
        "add-target": function($target, ev) {
            var self = this, options = $this.data();

            return self;
        },
        "remove-department-target": function($target, ev) {
            return this["remove-ceo-target"]($target, ev);
        },
        "remove-ceo-target": function($target, ev) {
            var self = this, options = $target.data();
            if( !('url' in options) && ('href' in $target[0]) ) {
                options.url = $target[0].href;
            }
            options.$target = $target;

            options.success = function(response, status, xhr) {
                if( xhr.status == 202 ) {
                    location.reload();
                }
                return;
            }
            self.bindEventDialogConfirm(options, '#confirm-apply-kpi');
            return self;
        },
        popupSettingsForMonth: function(){
            var self = this;
            let $inputDate = $('#tpl-lock-for-month').find('.datetimepicker');
            $inputDate.each(function(idx, elem) {
                let data = $(elem).data(),
                startDate = $(elem).val().length === 0 ? data.start : $(elem).val();
                $('#tpl-lock-for-month input[data-id='+data.id+']').datetimepicker({
                    format: 'd-m-Y',
                    formatDate: 'd-m-Y',
                    timepicker: false,
                    startDate: startDate,
                    scrollMonth: false,
                });
            })
            function formSettingsValidate($form){
                var $input = $form.find('.form-control.datetimepicker');
                var validationRules = {
                    rules:{},
                    messages: {},
                    errorPlacement: function ( error, $element ) {
                        // Add the `help-block` class to the error element
                        error.addClass( "help-block" );
                        if('$label' in $element[0]) {
                            error.insertBefore( $element[0].$label );
                        } else if ( $element.prop( "type" ) === "checkbox" ) {
                            error.insertBefore( $element.parent( "label" ) );
                        } else {
                            error.insertBefore( $element );
                        }
                        return;
                    }
                };

                $input.each(function(idx, elem) {
                    var name = $(elem).context.id;
                    validationRules.rules[name] = {
                        required: true,
                        customDateValidator: true,
                    };
                    validationRules.messages[name] = {
                        required: "Vui lòng nhập ngày tháng năm",
                        customDateValidator: 'Vui lòng nhập ngày tháng đúng định dạng',
                    }
                });
                $form.validate(validationRules);
                return;
            }
            $('#tpl-lock-for-month',self.$element).off('show.bs.modal.settings.for.month').on('show.bs.modal.settings.for.month', function(ev){
                let $popup = $(this);
                let $form = $('form', $popup);
                formSettingsValidate($form);
                $form.off('submit.settings.for.month').on('submit.settings.for.month', function(ev) {
                    ev.preventDefault();
                    var $form = $(this).closest('form');
                    var $response = $('.response-container', $form);
                    $form.ajaxSubmit({
                        beforeSend: function(xhr, settings) {
                            $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                            $form.block(IMAGE_LOADING);
                        },
                        beforeSubmit: function(a, $frm, options) {
                            var valid = true, total = 0.0, messages = [];
                            var validator = $form.data('validator');
                            valid = validator.form();
                            $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                            return valid;
                        },
                        error: function(xhr, status, error) {
                            if( ('responseJSON' in xhr) && ('message' in xhr.responseJSON) ) {
                                $response.removeClass('hidden').addClass('error').html(xhr.responseJSON.message);
                            }
                            return;
                        },
                        success: function(response, status, xhr) {
                            $response.removeClass('hidden').addClass('success').html("Lưu thành công");
                            location.reload();
                            return;
                        },
                        complete: function(xhr, status) {
                            $form.unblock(IMAGE_LOADING);
                            return;
                        }
                    });
                });
            });

        },
        bindEventDialogConfirm: function(options, element){
            var $dialogConfirm = $(element);
            $dialogConfirm.off('hidden.bs.modal.confirm').on('hidden.bs.modal.confirm', function(ev){
                $dialogConfirm.off('click.dialog.agree', element + ' .btn-yes');
            });
            $dialogConfirm.off('show.bs.modal.confirm').on('show.bs.modal.confirm', function(ev){
                $dialogConfirm.off('click.dialog.agree', '.btn-yes').on('click.dialog.agree', '.btn-yes', function(event){
                    event.preventDefault();
                    if( !$.isPlainObject(options) && options === undefined ){
                        alert('Vui lòng nhấn nút xóa trước khi hiển thị form này.');
                    }else{
                        //$dialogConfirm.modal('hide');
                        $.ajax(options);
                    }
                    return this;
                });
            });

            $dialogConfirm.modal();
            return this;
        },
        initPopupMembers: function() {
            var self = this, $popup = $('#tpl-department-kpi-members'), $form = $popup.find('form'),
                $table = $popup.find('.modal-body table.table.list-members'), form = $form.get(0),
                $list =  $table.find('tbody'), $template = $list.find('> tr.template'),
                $checkAll = $table.find('thead > tr > .select input:checkbox'),
                $checkInfluence = $table.find('thead > tr > .influence input:checkbox');

            $template.removeClass('template').addClass('members').remove();
            var $div = $('<div></div>'), isCheckAll = false, $response = $form.find('.response-container');
            $div.append($template);
            var $danhsach = $popup.find('.title-nhanvien-phong .danhsach');
            $popup.off('change.checkbox', 'td.select input:checkbox').on('change.checkbox', 'td.select input:checkbox', function(ev) {
                var $this = $(this), $tr = $this.closest('tr.members');
                var $inputs = $tr.find('input.input-personal_plan, input.input-percent');
                if( this.checked ) {
                    $tr.removeClass('unchecked');
                    $inputs.prop('disabled', false);
                } else {
                    $tr.addClass('unchecked');
                    $inputs.prop('disabled', true);
                    $checkAll.prop('checked', false);
                }
                if( !isCheckAll ) {
                    var $checkbox = $list.find('td.select input:checkbox');
                    var $checked = $checkbox.filter(':checked');
                    if( $checkbox.length == $checked.length ) {
                        $checkAll.prop('checked', true);
                    } else {
                        $checkAll.prop('checked', false);
                    }
                }
                var $checkbox = $list.find('td.select input:checkbox');
                var $checked = $checkbox.filter(':checked');
                $danhsach.html('' + $checked.length + '/' + $checkbox.length + ' nv');
                return;
            });
            $checkAll.unbind('change.checkall').bind('change.checkall', function() {
                var $checkbox = $list.find('td.select input:checkbox');
                var $checked = $checkbox.filter(':checked');
                isCheckAll = true;
                $checkbox.prop('checked', this.checked);
                $checkbox.trigger('change.checkbox');
                isCheckAll = false;
                return;
            });
            $form.unbind('submit.kpi').bind('submit.kpi', function(ev) {
                ev.preventDefault();
                $form.ajaxSubmit({
                    beforeSubmit: function(serialize, form) {
                        var $checkbox = $list.find('td.select input:checkbox');
                        var $checked = $checkbox.filter(':checked');
                        return;
                    },
                    beforeSend: function(xhr, settings) {
                        $response.addClass('hidden').removeClass('error').removeClass('success');
                        $form.block(IMAGE_LOADING);
                        return;
                    },
                    error: function(xhr, status, error) {
                        if( ('responseJSON' in xhr) && xhr.responseJSON && ('message' in xhr.responseJSON) ) {
                            $response.removeClass('hidden').removeClass('success').addClass('error').html(xhr.responseJSON.message);
                        } else {
                            error = xhr.getResponseHeader('xhr-message') || error;
                            try{
                                error = JSON.parse(error);
                            } catch(e){}
                            $response.removeClass('hidden').removeClass('success').addClass('error').html(error);
                        }
                        return;
                    },
                    success: function(response, status, xhr) {
                        //console.debug({response: response, status: status, xhr: xhr});
                        if( xhr.status == 201 ) {
                            $response.removeClass('hidden').removeClass('error').addClass('success').html("Lưu thành công");
                            if( $.isPlainObject(response) && 'location' in response ){
                                if( response.location != '' ){
                                    location.href = response.location;
                                }
                            }
                            location.reload();

                        }
                        return;
                    },
                    complete: function(xhr, status) {
                        $form.unblock(IMAGE_LOADING);
                        return;
                    }
                });
                return;
            });

            var $departmentName = $popup.find('.modal-title .phongban-name');
            var $loaiMuctieuKpi = $popup.find('.title-muctieu-phong .loai-muctieu-kpi');
            var $muctieuKpi = $popup.find('.title-muctieu-phong .muctieu-kpi');
            var $departmentPlan = $popup.find('.title-kehoach-phong .department-plan-unit');
            var $planUnit = $table.find('th.personal_plan .plan-unit');
            // var $danhsach = $popup.find('.title-nhanvien-phong .danhsach');

            $popup.off('show.bs.modal.kpi').on('show.bs.modal.kpi', function(showEv) {
                var $fromButton = $(showEv.relatedTarget);
                $response.addClass('hidden').removeClass('error').removeClass('success');
                if( $fromButton.length ) {
                    var $form_aprrove = $fromButton.closest('form');
                    $form.find('[name="_wp_http_referer"]').val($form_aprrove.find('[name="_wp_http_referer"]').val());
                    var options = $fromButton.data() || {};
                    options.kpiInfo = $fromButton.closest('tr.data-item').data('info') || {kpiInfo: {}};
                    $departmentName.html( options.department.room || '');
                    $muctieuKpi.html(options.bankTitle);
                    var unit = options.kpiInfo.unit || '';
                    unit = (unit in UNIT_TEXTS) ? UNIT_TEXTS[unit] : '';

                    $departmentPlan.html( (options.kpiInfo.plan || '') + ' ' + unit );
                    $checkAll.prop('checked', false);
                    $checkInfluence.prop("checked", false);
                    $planUnit.html( unit );
                    form.kpi_id.value = options.kpiId || '';
                    form.kpi_type.value = options.kpiType || '';
                    var keys = Object.keys(options.kpiInfo);
                    keys.forEach(function(k){
                        try {
                            if( k in form ) {
                                form[k].value = options.kpiInfo[k];
                            } else {
                                if( ['percent'].indexOf(k) == -1 ) {
                                    $form.find('.' + k).html(options.kpiInfo[k]);
                                }
                            }
                        } catch(e) {
                            console.error(k, e.message, e.stack);
                        }
                    });

                    Object.keys(options).forEach(function(field){
                        try {
                            if( typeof(options[field]) == 'string' ) {
                                if( field in form ) {
                                    form[field].value = options[field];
                                }
                            }
                        } catch(e) {
                            console.error(field, e.message, e.stack);
                        }
                    });
                    $list.empty();
                    if( $.isArray(options.members) ) {
                        options.members.forEach(function(member, idx){
                            try {
                                var $tr = $($div.html().replace(/IDX/g, member.ID));
                                $tr.children('.idx').html(idx + 1);
                                $tr.children('.fullname').html(member.display_name);
                                $tr.children('.id').html(member.user_nicename);
                                $tr.children('.cname').html(member.orgchart_name);
                                $tr.find('input.input-plan').val(member.kpi ? member.kpi.plan : '');
                                $tr.find('input.input-percent').val(member.kpi ? member.kpi.percent : '');
                                $tr.find('input.input-chart_id').val(member.orgchart_id);
                                $tr.find('input.input-kpi_item_id').val(member.kpi ? member.kpi.id : '');
                                $tr.find('input.input-id.checkbox-status').val(member.ID);
                                $list.append($tr);

                                if( member.kpi ) {
                                    var $check = $tr.find('td.select input[type="checkbox"]');
                                    $check.prop('checked', true);
                                    $check.trigger('change.checkbox');
                                    var $checkInfluenceItem = $tr.find('td.influence input[type="checkbox"]');
                                    if( member.kpi['influence'] == 'yes' ){
                                        $checkInfluenceItem.prop("checked", true);
                                    }else{
                                        $checkInfluenceItem.prop("checked", false);
                                    }
                                }
                            } catch(e) {
                                console.error(member, e.message, e.stack);
                            }
                        });
                    }
                    var $members = $fromButton.closest('.kpi-members').find('.number-members');
                } else {
                    $popup.modal('hide');
                }
                return;
            });
            return self;
        },
        renderItemKpi: function(obj, parent, level){
            var self = this, cats = [], html = '';
            Object.keys(obj).forEach(function(id){
                if( obj[id].parent == parent ){
                    cats.push(obj[id]);
                    delete obj[id];
                }
            });
            if( cats.length ){
                var groupOpt = '';
                cats.forEach(function(post) {
                    var outputChild = '', space = '';
                    for(var _i=0; _i<level; _i++) {
                        space += '&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                    if (obj) {
                        if( post.parent == 0 ) {
                            outputChild += '<option class="bank-level-' + level + '" disabled="disabled" data-parent="' + post.parent + '" value="' + post.ID + '">' + space + post.title + '</option>';
                        }
                        // outputChild = '<optgroup label="' + post.title + '" data-parent="' + post.parent + '" data-value="' + post.ID + '">';
                        outputChild += self.renderItemKpi(obj, post.ID, level + 1);
                        // outputChild += '</optgroup>';
                    }
                    var classChild = (outputChild === '' ? '' : 'no-child') + (' bank-level-' + level);
                    if (post.parent > 0) {
                        html += '<option class="' + classChild + '" data-parent="' + post.parent + '" value="' + post.ID + '">' + space + post.title + '</option>';
                    }
                    html += outputChild;
                });
            }
            return html;
        },
        initLoadKpiRegisterPersonal: function( $popup, $element, $form ){
            var self = this;
            var data = $element.data();
            var $ele = $popup;
            var $form = $ele.find('form');
            self[data.target] = {
                year_id: $ele.find('[name="year_id"]').val(),
                time_year: $ele.find('[name="time_year"]').val(),
                id: $ele.find('[name="id"]').val(),
            };
            var options = {
                url: AJAX_URL,
                type: data.method,
                dataType: 'json',
                data: {action: data.action, id: data.id},
                beforeSend: function(xhr, settings){
                    $form.block(IMAGE_LOADING);
                    return;
                },
                success: function( response, status, xhr ){
                    if( $.isPlainObject( response ) && ( 'data' in response ) ){
                        var dataResponse = response.data || {};
                        Object.keys(dataResponse).forEach(function(item){
                            if( item == 'bank' ){
                                self.bank_idPersonal = dataResponse.bank;
                                if( 'cat' in dataResponse ){
                                    var $cat_bank = $ele.find('.selectpicker.cat-bank');
                                    $cat_bank.val( dataResponse.cat );
                                    $cat_bank.trigger('change.bank_kpi');
                                    $cat_bank.selectpicker('refresh');
                                }
                            } else{
                                $ele.find('[name="'+ item +'"]').val( dataResponse[item] );
                                if( item == 'unit' || item == 'kpi_time' ){
                                    $ele.find('[name="'+ item +'"]').selectpicker("refresh");
                                }
                            }
                            return;
                        });

                    }
                    return;
                },
                error: function(xhr, status, errThrown){
                    return;
                },
                complete: function(xhr, status){
                    $ele.off("hidden.bs.modal").on("hidden.bs.modal", function () {
                        // put your default event here
                        self.bank_idPersonal = null;
                        var $selectBank = $('select.post_title-target_work', $ele);
                        $selectBank.html('');
                        $selectBank.selectpicker('refresh');
                        Object.keys(self[data.target]).forEach(function(item){
                            $ele.find('input[name="' + item + '"]').val(self[data.target][item]);
                        });
                    });
                    $form.unblock(IMAGE_LOADING);
                    return;
                },
            };
            $.ajax( options );
        },
        initLoadKpiDataPersonal: function( $popup, $element, $form ){
            var self = this;
            var data = $element.data();
            var uid = 0;
            if( 'uid' in data ){
                uid = data.uid;
            }
            var $ele = $popup;
            var $form = $ele.find('form');
            self[data.target] = {
                type: $ele.find('[name="type"]').val(),
                year_id: $ele.find('[name="year_id"]').val(),
                time_type: $ele.find('[name="time_type"]').val(),
                time_value: $ele.find('[name="time_value"]').val(),
                chart_id: $ele.find('[name="chart_id"]').val(),
            };
            var options = {
                url: AJAX_URL,
                type: data.method,
                dataType: 'json',
                data: {action: data.action, id: data.id, uid: uid},
                beforeSend: function(xhr, settings){
                    $ele.modal('show');
                    $form.block(IMAGE_LOADING);
                    //$ele.find('[name="kpiID"]').remove();
                    return;
                },
                success: function( response, status, xhr ){
                    if( $.isPlainObject( response ) && ( 'data' in response ) ){
                        var dataResponse = response.data || {};
                        if( 'departments' in dataResponse ){
                            var departments = dataResponse.departments;
                            delete dataResponse[departments];
                            Object.keys(departments).forEach(function(ditem){
                                var item = departments[ditem];
                                $ele.find('[name="departments[' + item.chart_id + '][plan]"]').val( item.department_plan );
                                $ele.find('[name="departments[' + item.chart_id + '][id]"]').prop('checked', true);

                                return;
                            });
                        }
                        Object.keys(dataResponse).forEach(function(item){
                            if( item == 'bank_id' ) {
                                if( 'bank_cat' in dataResponse ) {
                                    var $cat = $ele.find('.kpi-category');
                                    $cat.val( dataResponse.bank_cat );
                                    $cat.selectpicker('val', dataResponse.bank_cat);
                                    $cat.trigger('change');
                                }
                            } else if(item == 'receive') {
                                var receive = dataResponse[item].replace(/^(\d{2,4})(\-|\/)(\d{1,2})(\-|\/)(\d{1,2})(.*)/, "$5$4$3$2$1");
                                $ele.find('[name="'+ item +'"]').val( receive );
                            } else if( item == 'id' ) {
                                var $inputID = $('<input value="' + dataResponse[item] + '" type="hidden" name="kpiID" />');
                                var $wpNonce = $ele.find('[name="_wpnonce"]');
                                $inputID.insertBefore($wpNonce);
                            } else {
                                $ele.find('[name="'+ item +'"]').val( dataResponse[item] );
                            }
                            return;
                        });

                    } else if ( $.isPlainObject( response ) && ( 'data_personal' in response ) ){
                        var dataResponse = response.data_personal || {};
                        Object.keys(dataResponse).forEach( function( item ){
                            if( $ele.find( '.'+item ).is('input') ){
                                $ele.find('.'+item).val( dataResponse[item] );
                            }else{
                                $ele.find( '.'+item ).text(dataResponse[item]);
                            }
                            if( dataResponse.plan_on_level == '' || dataResponse.plan_on_level == undefined ){
                                $popup.find('.plan_on_level').closest('tr').hide();
                            }else{
                                $popup.find('.plan_on_level').closest('tr').show();
                            }
                        });
                        if( 'kpi_month' in response ){
                            var dataMonth = response.kpi_month;
                            if( dataMonth != '' ){
                                $('.content-fieldset-month', $ele).html(dataMonth);
                            }
                        }
                    }
                    return;
                },
                error: function(xhr, status, errThrown){
                    return;
                },
                complete: function(xhr, status){
                    $ele.off("hidden.bs.modal").on("hidden.bs.modal", function () {
                        // put your default event here
                        self.bank_id = null;
                        $('.content-fieldset-month', $form).html( self.templateMonth.html() );
                        Object.keys(self[data.target]).forEach(function(item){
                            $ele.find('input[name="' + item + '"]').val(self[data.target][item]);
                        });
                        //$(this).find('[name="kpiID"]').remove();
                    });
                    $form.unblock(IMAGE_LOADING);
                    return;
                },
            };
            $.ajax( options );
        },
        initLoadKpiDataPersonalPrecious: function( $popup, $element, $form ){
            var self = this;
            var data = $element.data();
            var uid = 0;
            if( 'uid' in data ){
                uid = data.uid;
            }
            var $ele = $popup;
            var $form = $ele.find('form');
            self[data.target] = {
                type: $ele.find('[name="type"]').val(),
                year_id: $ele.find('[name="year_id"]').val(),
                time_type: $ele.find('[name="time_type"]').val(),
                time_value: $ele.find('[name="time_value"]').val(),
                chart_id: $ele.find('[name="chart_id"]').val(),
            };
            var options = {
                url: AJAX_URL,
                type: data.method,
                dataType: 'json',
                data: {action: data.action, id: data.id, uid: uid},
                beforeSend: function(xhr, settings){
                    $ele.modal('show');
                    $form.block(IMAGE_LOADING);
                    //$ele.find('[name="kpiID"]').remove();
                    return;
                },
                success: function( response, status, xhr ){
                    if( $.isPlainObject( response ) && ( 'data' in response ) ){
                        var dataResponse = response.data || {};
                        if( 'departments' in dataResponse ){
                            var departments = dataResponse.departments;
                            delete dataResponse[departments];
                            Object.keys(departments).forEach(function(ditem){
                                var item = departments[ditem];
                                $ele.find('[name="departments[' + item.chart_id + '][plan]"]').val( item.department_plan );
                                $ele.find('[name="departments[' + item.chart_id + '][id]"]').prop('checked', true);

                                return;
                            });
                        }
                        Object.keys(dataResponse).forEach(function(item){
                            if( item == 'bank_id' ) {
                                if( 'bank_cat' in dataResponse ) {
                                    var $cat = $ele.find('.kpi-category');
                                    $cat.val( dataResponse.bank_cat );
                                    $cat.selectpicker('val', dataResponse.bank_cat);
                                    $cat.trigger('change');
                                }
                            } else if(item == 'receive') {
                                var receive = dataResponse[item].replace(/^(\d{2,4})(\-|\/)(\d{1,2})(\-|\/)(\d{1,2})(.*)/, "$5$4$3$2$1");
                                $ele.find('[name="'+ item +'"]').val( receive );
                            } else if( item == 'id' ) {
                                var $inputID = $('<input value="' + dataResponse[item] + '" type="hidden" name="kpiID" />');
                                var $wpNonce = $ele.find('[name="_wpnonce"]');
                                $inputID.insertBefore($wpNonce);
                            } else {
                                $ele.find('[name="'+ item +'"]').val( dataResponse[item] );
                            }
                            return;
                        });

                    } else if ( $.isPlainObject( response ) && ( 'data_personal' in response ) ){
                        var dataResponse = response.data_personal || {};
                        Object.keys(dataResponse).forEach( function( item ){
                            if( $ele.find( '.'+item ).is('input') ){
                                $ele.find('.'+item).val( dataResponse[item] );
                            }else{
                                $ele.find( '.'+item ).text(dataResponse[item]);
                            }
                            if( dataResponse.plan_on_level == '' || dataResponse.plan_on_level == undefined ){
                                $popup.find('.plan_on_level').closest('tr').hide();
                            }else{
                                $popup.find('.plan_on_level').closest('tr').show();
                            }
                        });
                        if( 'kpi_precious' in response ){
                            var dataPrecious = response.kpi_precious;
                            if( dataPrecious != '' ){
                                $('.content-fieldset-precious', $ele).html(dataPrecious);
                            }
                        }
                    }
                    return;
                },
                error: function(xhr, status, errThrown){
                    return;
                },
                complete: function(xhr, status){
                    $ele.off("hidden.bs.modal").on("hidden.bs.modal", function () {
                        // put your default event here
                        self.bank_id = null;
                        $('.content-fieldset-precious', $form).html( self.templatePrecious.html() );
                        Object.keys(self[data.target]).forEach(function(item){
                            $ele.find('input[name="' + item + '"]').val(self[data.target][item]);
                        });
                        //$(this).find('[name="kpiID"]').remove();
                    });
                    $form.unblock(IMAGE_LOADING);
                    return;
                },
            };
            $.ajax( options );
        },
        initLoadCapacity: function( $popup, $element, $form ){
            var self = this;
            var data = $element.data();
            var $ele = $popup;
            var $form = $ele.find('form');
            var options = {
                url: AJAX_URL,
                type: data.method,
                dataType: 'json',
                data: {action: data.action, id: data.id, _wpnonce: data.wpnonce},
                beforeSend: function(xhr, settings){
                    $ele.modal('show');
                    $form.block(IMAGE_LOADING);
                    return;
                },
                success: function( response, status, xhr ){
                    if( $.isPlainObject( response ) && ( 'data' in response ) ){
                        var dataRes = response.data;
                        Object.keys(dataRes).forEach(function(item){
                            $ele.find('input[name="' + item + '"]').val( dataRes[item] );
                            if( item == 'bank_id' ){
                                var $bankSelector = $ele.find('select[name="' + item + '"]');
                                $bankSelector.val( dataRes[item] );
                                $bankSelector.selectpicker('refresh');
                                $bankSelector.trigger('change.capacity.bank.kpi');
                            }else if( item == 'status' ) {
                                $ele.find('input[name="' + item + '"]').prop("checked", dataRes[item]);
                            }
                        });
                    }
                    return;
                },
                error: function(xhr, status, errThrown){
                    return;
                },
                complete: function(xhr, status){
                    $form.unblock(IMAGE_LOADING);
                    return;
                },
            };
            $.ajax( options );
        },
        initLoadBehavior: function( $popup, $element, $form ){
            var self = this;
            var data = $element.data();
            var $ele = $popup;
            var $form = $ele.find('form');
            var options = {
                url: AJAX_URL,
                type: data.method,
                dataType: 'json',
                data: {action: data.action, id: data.id, _wpnonce: data.wpnonce},
                beforeSend: function(xhr, settings){
                    $form.block(IMAGE_LOADING);
                    return;
                },
                success: function( response, status, xhr ){
                    if( $.isPlainObject( response ) && ( 'data' in response ) ){
                        var dataRes = response.data;
                        Object.keys(dataRes).forEach(function(item){
                            $ele.find('input[name="' + item + '"]').val( dataRes[item] );
                            if( item == 'bank_id' ){
                                var $bankSelector = $ele.find('select[name="' + item + '"]');
                                $bankSelector.val( dataRes[item] );
                                $bankSelector.selectpicker('refresh');
                                $bankSelector.trigger('change.behavior.bank.kpi');
                            }else if( item == 'status' ) {
                                $ele.find('input[name="' + item + '"]').prop("checked", dataRes[item]);
                            }
                        });
                    }
                    return;
                },
                error: function(xhr, status, errThrown){
                    return;
                },
                complete: function(xhr, status){
                    $form.unblock(IMAGE_LOADING);
                    return;
                },
            };
            $.ajax( options );
            return self;
        },
        buttonEvents: function() {
            var self = this;
            $('select#select-for-year', self.$element).bind('change.year', function() {
                var urlParams =(new URL(location)).searchParams;
                var url = location.href;
                if( 'type' in GETS && urlParams.has('type') ){
                    //url = location.href.replace(/(\&|\?)(type)(=\s+|)/, '$1$2=' + GETS.type);
                }else{
                    url += "?type=" + GETS.type;
                }
                if( urlParams.has('nam') ){
                    url = location.href.replace(/(\&|\?)(nam)(=\d+|)/, '$1$2=' + this.value);
                }else{
                    url += "&nam=" + this.value;
                }
                location.href = url;
                return;
            });
            $('select#select-for-quarter', self.$element).bind('change.quarter', function() {
                var urlParams = (new URL(location)).searchParams;
                var url = location.href;
                var $year = $('select#select-for-year', self.$element);
                if( 'type' in GETS && urlParams.has('type') ){
                    //url = location.href.replace(/(\&|\?)(type)(=\s+|)/, '$1$2=' + GETS.type);
                }else{
                    url += "?type=" + GETS.type;
                }
                if( urlParams.has('nam') ){
                    url = url.replace(/(\&|\?)(nam)(=\d+|)/, '$1$2=' + $year.val());
                }else{
                    url += "&nam=" + $year.val();
                }
                if( urlParams.has('quy') ){
                    url = url.replace(/(\&|\?)(quy)(=\d+|)/, '$1$2=' + this.value);
                }else{
                    url += "&quy=" + this.value;
                }
                location.href = url;
                return;
            });
            $('select#select-for-month', self.$element).bind('change.month', function() {
                //var url = location.href.replace(/(\&|\?)(nam)(=\d+|)/, '$1$2=' + this.value);
                var urlParams = (new URL(location)).searchParams;
                var url = location.href;
                var $year = $('select#select-for-year', self.$element);
                if( 'type' in GETS && urlParams.has('type') ){
                    //url = location.href.replace(/(\&|\?)(type)(=\s+|)/, '$1$2=' + GETS.type);
                }else{
                    url += "?type=" + GETS.type;
                }
                if( urlParams.has('nam') ){
                    url = url.replace(/(\&|\?)(nam)(=\d+|)/, '$1$2=' + $year.val());
                }else{
                    url += "&nam=" + $year.val();
                }
                var month = $(this).val();
                if( urlParams.has('thang') ){
                    url = url.replace(/(\&|\?)(thang)(=\d+|)/, '$1$2=' + month);
                }else{
                    url += "&thang=" + month;
                }
                var precious = Math.ceil( month / 3);
                if( urlParams.has('quy') ){
                    url = url.replace(/(\&|\?)(quy)(=\d+|)/, '$1$2=' + precious);
                }else{
                    url += "&quy=" + precious;
                }
                location.href = url;
                return;
            });

            $('button[name="btn-save-send"]', self.$element).off('click.send.results').on('click.send.results', function(ev){
                ev.preventDefault();
                var $form = $(this).closest('form');
                var data = $(this).data();
                var backupAction = $form.attr('action');
                $form.attr('action', data.action);
                var options = {
                    dataType: 'json',
                    beforeSend: function(xhr, settings){
                        $form.find('.notification').remove();
                        $form.block(IMAGE_LOADING);
                        return;
                    },
                    error: function(xhr, status, error) {
                        var textError = '';
                        if( ('responseJSON' in xhr) && xhr.responseJSON && ('message' in xhr.responseJSON) ) {
                            textError = xhr.responseJSON.message;
                        } else {
                            textError = xhr.getResponseHeader('xhr-message') || error;
                            try{
                                textError = JSON.parse(textError);
                            } catch(e){}
                        }
                        var $err = $('<label class="notification error"> ' + textError + ' </label>');
                        $form.prepend( $err );
                        return;
                    },
                    success: function(response, status, xhr) {
                        if( xhr.status == 201 ) {
                            var $success = $('<label class="notification success"> Lưu thành công </label>');
                            if( $.isPlainObject(response) && 'location' in response ){
                                if( response.location != '' ){
                                    location.href = response.location;
                                }
                            }
                            $form.prepend( $success );
                            location.reload();
                        }
                        return;
                    },
                    complete: function(xhr, status){
                        $form.attr('action', backupAction);
                        $form.unblock(IMAGE_LOADING);
                        return;
                    }
                }
                $form.ajaxSubmit(options);
                return;
            });
            return self;
        },
        initChangeAlias: function() {
            var self = this;
            $(self.$element).on('change.alias', 'select.selectpicker[data-change-alias]', function(ev) {
                var url = $(this).data('redirectUrl');
                if( url.match(/(\?|&)(cid=)(\d+|)/) ) {
                    url = url.replace(/(\?|&)(cid=)(\d+|)/, '$1$2' + this.value);
                } else {
                    url = url + (location.search.indexOf('?') > -1 ? '&cid=' : '?cid=') + this.value;
                }
                location.href = url;
                return;
            });

            $(function(){
                var match = location.href.match(/(\?|&)(cid=)(\d+|)/);
                match ? $('select.selectpicker[data-change-alias]').selectpicker('val', match[3]) : null;
            });
            self.$element.find('.tab-pane .table-list-detail').each(function(){
                var $table = $(this), $editRow = $table.find('tr.editer-item');
                $editRow.remove();
                $table.data('$editRow', $editRow);
            });
            var thisPercent = 0;
            self.$element.on('click.kpi.edit', '.tab-pane .table-list-detail tr > td.kpi-action > a.action-edit, .tab-pane .table-list-detail tr > td > a.action-add', function(ev) {
                ev.preventDefault();
                var $this = $(this), ac = $this.data('ac'), isEdit = (ac == 'edit'),
                        $tr = $this.closest('tr[data-info]'),
                    $table = $this.closest('table.table-list-detail'),
                    $form = $table.closest('form'),
                    $editRow = $table.data('$editRow'), $list = $table.find('tbody'),
                    dataInfo = isEdit ? $tr.data('info') : $editRow.data('addInfo'),
                    $btnSave = $editRow.find('a.action-save'),
                    $btnCancel = $editRow.find('a.action-cancel'),
                    $form = $(this).closest('form'),
                    //$response = $('tfoot tr > td > .container-message', $table);
                    $response = $('.container-message', $form),
                    thisPercent = $('td.kpi-percent', $tr).text();
                if( $editRow.hasClass('saving') ) {
                    return;
                }
                function hideEditRow() {
                    $editRow.find('select').each(function(){
                        $(this).selectpicker('destroy');
                    });
                    $editRow.addClass('hidden');
                    $editRow.removeClass(ac);
                    $editRow.removeAttr('style');
                    $editRow.remove();
                    $form.find('button.btn-save-and-public').removeAttr('disabled');
                }
                hideEditRow();

                $form.find('button.btn-save-and-public').attr('disabled', 'disabled');

                if( $editRow.hasClass('hidden') ) {
                    $list.append($editRow);
                }
                $editRow.removeClass('hidden').removeClass('add').removeClass('edit').removeAttr('style');
                $editRow.addClass(ac);

                var $ID = $editRow.find('td.kpi-id input[name="postdata\[id\]"]'),
                    $textId = $editRow.find('td.kpi-id .item-id'),
                    $content = $tr.children('td.kpi-content'),
                    $post_title = $editRow.find('td.kpi-content input[name="postdata\[post_title\]"]'),
                    $postParent = $editRow.find('td.kpi-content input[name="postdata\[parent\]"]'),
                    $influence = $editRow.find('td.kpi-content input[name="postdata\[influence\]"]'),
                    $bankParent = $editRow.find('td.kpi-content a.bank-parent'),
                    $plan = $editRow.find('td.kpi-plan input[name="postdata\[plan\]"]'),
                    $unit = $editRow.find('td.kpi-plan select[name="postdata\[unit\]"]'),
                    $receive = $editRow.find('td.kpi-receive input[name="postdata\[receive\]"]'),
                    $percent = $editRow.find('td.kpi-percent input[name="postdata\[percent\]"]'),
                    $radio = $editRow.find('.switch.bank-parent input[type="radio"].checkbox-status:checked'),
                    $congthuc = $editRow.find('td.kpi-content select[name="formula_id"]');

                $radio.prop('checked', false);
                $influence.prop('checked', false);
                if( isEdit ) {
                    $editRow.css( $tr.position() );
                    $editRow.children('td.kpi-content').width( $content.outerWidth() - 5 );
                    var $radio = $editRow.find('.switch.bank-parent input[type="radio"].checkbox-status[value="'+dataInfo.parent+'"]');
                    $radio = $radio.length ? $radio : $editRow.find('.switch.bank-parent input[type="radio"].checkbox-status[value="'+dataInfo.bank_id+'"]');
                    $radio.prop('checked', true);
                    if( dataInfo.influence == 'yes' ){
                        $influence.prop('checked', true);
                    }
                }
                $ID.val( dataInfo.id );
                $textId.html( dataInfo.id );
                $post_title.val( dataInfo.post_title );
                $plan.val( dataInfo.plan );
                $percent.val( dataInfo.percent );
                $receive.val( dataInfo.receive );
                $unit.val( dataInfo.unit );

                $congthuc.val( ('formula_id' in dataInfo) ? dataInfo.formula_id : 0 );
                $congthuc.off('change.kpibank').on('change.kpibank', function(){
                    return;
                });
                $editRow.find('select').each(function(){
                    $(this).selectpicker('destroy');
                    $(this).addClass('selectpicker');
                    $(this).selectpicker('refresh');
                         $(this).data('changed', false);
                    $(this).unbind('change.elements').bind('change.elements', function() {
                        $(this).data('changed', true);
                        return;
                    });
                    return;
                });

                self.initDate($editRow);
                var $elements = $editRow.find(
                    'td.kpi-content input[name="postdata[post_title]"], '+
                    'td.kpi-plan input[name="postdata[plan]"], '+
                    'td.kpi-receive input[name="postdata[receive]"], '+
                    'td.kpi-percent input[name="postdata[percent]"]');
                if( ac == 'add' ) {
                    $elements.data('keypressed', false);
                    $elements.unbind('keypress.elements change.elements').bind('keypress.elements change.elements', function() {
                        $(this).data('keypressed', true);
                        return;
                    });
                } else {
                    $elements.unbind('keypress.elements');
                }
                var $parentRadio = $editRow.find('td.kpi-content ul.list-group > .list-group-item input[type="radio"].checkbox-status');
                $parentRadio.off('change.bankparent').on('change.bankparent', function(ev) {
                    if( this.checked && (ac == 'add') ) {
                        var kpiParent = $(this).closest('.list-group-item').data('kpiParent');
                        if( kpiParent ) {
                            if( !$percent.data('keypressed') ) {
                                $percent.val( kpiParent.percent );
                            }
                            if( !$post_title.data('keypressed') ) {
                                $post_title.val( kpiParent.post_title );
                            }
                            if( !$plan.data('keypressed') ) {
                                $plan.val( kpiParent.plan );
                            }
                            if( !$receive.data('keypressed') ) { // || datetimepicker.data('changed', true)
                                $receive.val( kpiParent.receive );
                            }
                            if( !$congthuc.data('changed') ) {
                                $congthuc.val( kpiParent.formula_id );
                                $congthuc.selectpicker('val', kpiParent.formula_id);
                            }
                            if( !$unit.data('changed') ) {
                                $unit.val( kpiParent.unit );
                                $unit.selectpicker('val', kpiParent.unit);
                            }
                        }
                    }
                    return;
                });

                var $body = $(document.body);
                if( $body.hasClass('role-phongban') || ( $body.hasClass('role-bgd') && GETS.type == 'duyet-dang-ky')  ) {
                    self.initPhongbanKpi($editRow, dataInfo.bank_id);
                }
                $btnSave.unbind('click.kpi.save').bind('click.kpi.save', function(e1) {
                    e1.preventDefault();
                    var $this = $(this), totalPercent = 0; saveData = $.extend({action: ac}, dataInfo, {
                        'plan': $plan.val(),
                        'unit': $unit.val(),
                        'receive': $receive.val(),
                        'percent': $percent.val()
                    });
                    if( isNaN(parseInt(thisPercent)) ){
                        thisPercent = 0;
                    }
                    if( thisPercent == 0 ) {
                        //totalPercent = parseInt(saveData.percent || '0', 10);
                    }
                    if( isNaN(totalPercent) || !isFinite(totalPercent) ) {
                        totalPercent = 0;
                    }
                    $table.find('tr.data-item > td.kpi-percent').each(function(){
                        var num = parseInt($(this).text().trim() || '0', 10);
                        if( isNaN(num) || !isFinite(num) ) {
                            num = 0;
                        }
                        totalPercent += num;
                    });
                    totalPercent = totalPercent - parseInt(thisPercent) + parseInt($percent.val());
                    if( totalPercent > 100 ) {
                        $response.addClass('error').removeClass('hidden').removeClass('success').html('Tổng trọng số của các mục tiêu không được vượt qua 100%');
                        return;
                    }
                    if( $body.hasClass('role-phongban') ) {
                        saveData.bank_id = $editRow.find('select.kpi-bank-id.selectpicker').val();
                    } else {
                        var $radio = $postParent.filter(':checked'), $text = $radio.parent().children('.text'),
                            val = ($post_title.val() || '').trim() || $text.text();
                        $post_title.val( val );
                        saveData.post_title = val;
                        saveData.parent = $postParent.filter(':checked').val();
                        if( saveData.bank_id == saveData.parent ) {
                            if( ($text.html() || '').trim() != val ) {
                                saveData.bank_id = '';
                            }
                        }

                        if( $body.hasClass('role-bgd') ) {
                            saveData.formula_id = $editRow.find('select[name="formula_id"]').val();
                            saveData.influence = $influence.filter(':checked').val();
                        } else if( $body.hasClass('role-phongban') ) {
                            // saveData.formula_id = 0;
                        }
                    }
                    var options = $this.data();
                    options = $.extend({}, options, {
                        data: {postdata: saveData},
                        dataType: 'json',
                        beforeSend: function(settings, xhr) {
                            $editRow.block(IMAGE_LOADING);
                            $editRow.addClass('saving');
                            $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                            return;
                        },
                        error: function(xhr, status, error) {
                            var msg = error;
                            if( ('responseJSON' in xhr) && xhr.responseJSON && ('message' in xhr.responseJSON) ) {
                                msg = xhr.responseJSON.message;
                            }
                            $response.addClass('error').removeClass('hidden').removeClass('success').html(msg);
                            return;
                        },
                        success: function(response, status, xhr) {
                            if( xhr.status == 201 ) {
                                $response.addClass('success', response.message);
                                if( $('body').hasClass('role-managers') ){
                                    location.reload();
                                }else{
                                    if( response.html ) {
                                        var $newNode = $(response.html);
                                        if( ac == 'add' ) {
                                            $editRow.before($newNode);
                                            hideEditRow();
                                        } else {
                                            $tr.before($newNode);
                                            $tr.remove();
                                            hideEditRow();
                                        }
                                    }
                                    if( response.item_html ) {
                                        var $tbody = $('.content-list-register-' + $editRow.data('tabkey') + ' .table-block-list-register > tbody');
                                        var $itemNode = $(response.item_html);
                                        if( ac == 'add' ) {
                                            $tbody.append($itemNode);
                                            $itemNode.children('.column-1').html( '' + $tbody.children().length + '.' );
                                        } else {
                                            $tr = $tbody.children('tr[data-id="'+response.id+'"]');
                                            $tr.before($itemNode);
                                            $itemNode.children('.column-1').html( $tr.children('.column-1').html() );
                                            $tr.remove();
                                        }
                                    }
                                }
                            }
                            return;
                        },
                        complete: function(xhr, status) {
                            $editRow.unblock(IMAGE_LOADING);
                            $editRow.removeClass('saving');
                            $this[0].ajax = null;
                            return;
                        }
                    });
                    $this[0].ajax = $.ajax(options);
                    return;
                });

                $btnCancel.unbind('click.kpi.cancel').bind('click.kpi.cancel', function(e2) {
                    thisPercent = 0;
                    e2.preventDefault();
                    hideEditRow();
                    return;
                });

                return;
            });
            return self;
        },
        initPhongbanKpi: function($editRow, bankID) {
            var self = this;
            $editRow.find('select.selectpicker').selectpicker();
            var catPicker = $editRow.find('select.kpi-category.selectpicker').data('selectpicker'),
                bankPicker = $editRow.find('select.kpi-bank-id.selectpicker').data('selectpicker'),
                $response = $editRow.find('.response-status'),
                $loading = $editRow.find('td.kpi-content');
            catPicker.$element.unbind('change.catkpi').bind('change.catkpi', function(ev) {
                var selectCat = this;
                if( ('chartAjax' in selectCat) && selectCat.chartAjax && selectCat.chartAjax.abort ) {
                    selectCat.chartAjax.abort('Cancel Request Cat Name');
                }
                selectCat.chartAjax = $.ajax({
                    url: AJAX_URL,
                    type: 'get',
                    dataType: 'json',
                    data: {action: 'kpi_list', cat: this.value},
                    beforeSend: function(xhr, settings) {
                        $response.addClass('hidden').removeClass('error').removeClass('success');
                        $loading.length ? $loading.block(IMAGE_LOADING) : null;
                        return;
                    },
                    error: function(xhr, status, err) {
                        $response.removeClass('hidden').addClass('error').removeClass('success');
                        if( ('responseJSON' in xhr) && xhr.responseJSON && ('message' in xhr.responseJSON) ) {
                            $response.html(xhr.responseJSON.message);
                        } else {
                            $response.html("Có lỗi xảy ra");
                        }
                        return;
                    },
                    success: function(responseJson, status, xhr) {
                        if( xhr.status != 202 ) {
                            return;
                        }
                        var $option = bankPicker.$element.find('option:selected'), response = responseJson;
                        $option.remove();
                        bankPicker.$element.empty();
                        if( 'items' in responseJson ) {
                            response = responseJson.items;
                        }
                        var keys = Object.keys(response);
                        if( keys.length == 0 ) {
                            $response.removeClass('hidden').removeClass('error').addClass('success');
                            $response.html("Không tìm thấy mục tiêu tương ứng chức danh này");
                        } else {
                            var html = '<option value=""> -- Chọn Mục Tiêu -- </option>' + self.renderItemKpi( response, 0, 0 );
                            bankPicker.$element.append( html );
                            var $selected = bankPicker.$element.find('option[value="'+$option.val()+'"]');
                            if( !$selected.length ) {
                                bankPicker.$element.prepend($option);
                            }
                            var bank_id = catPicker.$element.data('bank_id');
                            if( bank_id == undefined ){
                                bank_id = bankID;
                            }
                            if( bank_id ) {
                                bankPicker.$element.find('option[value="' + bank_id + '"]').prop('selected', true);
                            }
                            bankPicker.$element.selectpicker( 'refresh' );
                        }
                        return;
                    },
                    complete: function(xhr, status) {
                        $loading.length ? $loading.unblock(IMAGE_LOADING) : null;
                        selectCat.chartAjax = null;
                        return;
                    }
                });
                return;
            });
            catPicker.$element.trigger('change');
            // bankPicker.$element;
            return self;
        },
        initAssignKPI: function() {
            var self = this;
            $(self.$element).off('hidden.bs.modal.assign', '.modal[id^="tab-pane-popup-"]').on('hidden.bs.modal.assign', '.modal[id^="tab-pane-popup-"]', function(ev){
                var $dialog = $(this);
                $dialog.off('click.dialog.agree', 'button.btn-yes');
            });

            $(self.$element).off('show.bs.modal.assign', '.modal[id^="tab-pane-popup-"]').on('show.bs.modal.assign', '.modal[id^="tab-pane-popup-"]', function(ev){
                var $dialog = $(this), $btn = $(ev.relatedTarget), options = $btn.data() || {bankTitle: '', kpiId: 0};
                $dialog.find('.modal-title').html(options.bankTitle);
                $dialog.off('click.dialog.agree', '.btn-yes').on('click.dialog.agree', '.btn-yes', function(event){
                    event.preventDefault();
                    if( !$.isPlainObject(options) && options === undefined ){
                        alert('Vui lòng nhấn nút xóa trước khi hiển thị form này.');
                    }else{
                        $.ajax(options);
                    }
                    return this;
                });
            });
            return self;
        },
        initFormulaPopup: function(){
            //formula-popup
            var self = this;
            $('#formula-popup').off('show.bs.modal.show-formula').on('show.bs.modal.show-formula', function(showEv) {
                var $btn = $(showEv.relatedTarget);
                var data = $btn.data();
                var $title = $('.formula-title', $(this));
                var $formulas = $('.formulas', $(this));
                var $note = $('.formula-note', $(this));
                var $notFound = $('.formula-not-found', $(this));
                $title.addClass('hidden');
                $formulas.addClass('hidden');
                $note.addClass('hidden');
                $notFound.addClass('hidden');
                if( 'formula' in data ){
                    var data_formula = data.formula;
                    if(  'title' in data_formula && data_formula.title != null ){
                        $('.content', $title).html( data_formula.title );
                        $title.removeClass('hidden');
                    }
                    if( 'html' in data_formula && data_formula.html != '' ){
                        $('.content', $formulas).html( data_formula.html );
                        $formulas.removeClass('hidden');
                    }
                    if( 'note' in data_formula && data_formula.note != null ){
                        $('.content', $note).html( data_formula.note );
                        $note.removeClass('hidden');
                    }
                    if( data_formula.title == null && data_formula.note == null && data_formula.html == '' ){
                        $notFound.removeClass('hidden');
                    }
                }

             });
        },
        initEvents: function() {
            var self = this;
            $.datetimepicker.setLocale('vi');
            self.buttonEvents();
            self.popupKpiYear();
            self.popupSettingsForMonth();
            self.initPopupMembers();
            self.popupNewPersonalKpi();
            self.popupCapacity();
            self.initBehavior();
            self.inputFile();
            self.initChangeAlias();
            self.initAssignKPI();
            self.initFormulaPopup();

            function get_store_item(key, _def) {
                if( 'sessionStorage' in window ) {
                    var val, result;
                    try {
                        val = window.sessionStorage.getItem(key);
                        result = JSON.parse(val);
                        return result;
                    } catch (e) {
                        return val || _def || null;
                    }
                }
                return null;
            }
            function set_store_item(key, val) {
                if( 'sessionStorage' in window ) {
                    try {
                        window.sessionStorage.setItem(key, JSON.stringify(val));
                    } catch (e) {
                    }
                }
            }
            function get_tab_state(type) {
                return get_store_item('tab_' + type, null);
            }
            function set_tab_state(type, value) {
                return set_store_item('tab_' + type, value);
            }
            var $tabTargetKpi = $('#room-target-kpi');
            if( $tabTargetKpi.length ) {
                var $tabs = $('a[data-toggle="tab"][data-tab-type]');
                $tabs.bind('show.bs.tab.room', function(ev) {
                    var $this = $(this), $previous = $(ev.relatedTarget),
                        tabType = $this.data('tabType'), target = $this.attr('href');
                    if( tabType ) {
                        set_tab_state(tabType, target);
                    }
                    return;
                });
                $(function() {
                    var $active, target, type = $tabTargetKpi.data('type');
                    if( type ) {
                        target = get_tab_state(type);
                        if( target ) {
                            $active = $tabs.filter('[href="'+target+'"]');
                            $active.trigger('click');
                        }
                    }
                    return;
                });
            }

            return self;
        }
    }
    $.fn.inputKPI = function (options) {
        $(this).each(function(){
            var ops = $.extend({}, $(this).data() || {}, options);
            var inputKPI = $(this).data('inputKPI');
            if( !inputKPI ) {
                inputKPI = new InputKPI(this, ops);
                $(this).data('inputKPI', inputKPI);
            }
            return inputKPI;
        });
    }
    $.validator.addMethod("customDateValidator", function(value, element){
        return this.optional(element) || /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/.test(value);
    });
    $('.apply-kpi-system').inputKPI();
    return;
})(jQuery);

