jQuery(function($){
    $.fn.settingsSite = function (options) {
        this.each(function(){
            var settings = $(this).data('settingsSite');
            if( !settings ){
                settings = new SettingsSite(this, options);
                $(this).data('settingsSite', settings);
            }
        });
        return this;
    }
    function SettingsSite(element, options){
        var dataSet = $(element).data() || {};
        this.options = $.extend({
        }, dataSet, options || {}, {});
        this.$element = $(element);
        this.$form = $(element).find('form');
        this.init();
        return this;
    }
    SettingsSite.defaultOptions = {

    }
    SettingsSite.prototype = {
        submitFormAjax: function($form, validator, ev){
            var self = this;
            ev.preventDefault();
            /*if( ('xhr_kpi' in self) && self.xhr_kpi !== null ) {
                self.xhr_kpi.abort("Cancel and Rerquest again");
            }*/
            self.xhr_kpi = {
                dataType: 'json',
                beforeSubmit: function(serialize, form, option) {
                    $form.find('.notification').remove();
                    return true;
                },
                beforeSend: function () {
                    $form.block(IMAGE_LOADING);
                },
                success: function(response, status, xhr){
                    console.log( 'SUCCESS',response, status, xhr );
                    if( $.isPlainObject(response) || 'success' in response ){
                        window.location.reload();
                    }
                    return;
                },
                error: function(xhr, status, errThrow){
                    console.log( 'ERROR',xhr, status, errThrow );
                    //window.location.reload();
                    var textError = '';
                    if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                        textError = xhr.responseJSON.error;
                    }else if( errThrow != '' ){
                        textError = xhr.getResponseHeader('xhr-message') || errThrow;
                        try{
                            textError = JSON.parse(textError);
                        } catch(e){}
                    }
                    var $err = $('<label class="notification error"> ' + textError + ' </label>');
                    $form.find('.notification').remove();
                    $form.prepend( $err );
                    return;
                },
                complete: function(xhr, status){
                    //console.log( 'Complete', xhr, status  );
                    self.xhr_kpi = null;
                    $form.unblock(IMAGE_LOADING);
                    return;
                }
            };
            $form.ajaxSubmit(self.xhr_kpi);
            return false;
        },
        formValidate: function(frm){
            var self = this;
            var $frm = $(frm);
            var $required = $([].slice.call(frm.elements)).filter('[required]');
            var validationRules = {
                // Specify validation rules
                //onsubmit: callajax ? false : true,
                focusCleanup: true,
                errorClass: 'error',
                validClass: 'valid',
                rules: {},
                // Specify validation error messages
                messages: {},
                errorElement: "em",
                errorPlacement: function ( error, $element ) {
                    // Add the `help-block` class to the error element
                    error.addClass( "help-block" );
                    var data = $element.data();
                    var errText = data.errRequired || 'Please input value for ' + $element.attr('placeholder');
                    error.text( errText );
                    if('$label' in $element[0]) {
                        error.insertAfter( $element[0].$label );
                    } else if ( $element.prop( "type" ) === "checkbox" ) {
                        error.insertAfter( $element.parent( "label" ) );
                    } else {
                        error.insertAfter( $element );
                    }
                    return false;
                },
                invalidHandler: function(ev) {
                    var validator = $frm.data('validator');
                    var elements = validator.invalidElements();
                    if( validator.errorList.length == 0 ) {
                        validator.errorList = [];
                        self.initFormValidate();
                        $frm.trigger('submit.validate');
                        return;
                    }
                    return;
                },
                submitHandler: function(form, ev) {
                    var $form = $(form);
                    var validator = this;
                    return self.submitFormAjax($form, validator, ev);
                }
            };
            $required.each(function(idx, elem) {
                validationRules.rules[elem.id] = 'required';
                try {
                    var $label = $frm.find('label[for="'+elem.id+'"]');
                    elem.$label = $label;
                    $label = $label.clone();
                    $label.find('span').remove();
                    var field = $label.text().trim();
                    var data = $(elem).data() || {};
                    validationRules.messages[elem.id] = {
                        required: data.errRequired || 'Please input value for '+ field
                    }
                }catch(e) {
                }
                return;
            });
            var validator = $frm.data('validator');
            if( validator ) {
                $frm
                    .off( ".validate" )
                    .removeData( "validator" )
                    .find( ".validate-equalTo-blur" )
                    .off( ".validate-equalTo" )
                    .removeClass( "validate-equalTo-blur" );
            }
            $frm.validate(validationRules);
        },
        initFormValidate: function(){
            var self = this;
            self.$form.each(function(index, form){
                self.formValidate( form );
            });

            return;
        },
        initForm: function(){
            var self = this;
            self.initFormValidate();
            return self;
        },
        initEvent: function(){
            var self = this;
            $('.remove-logo', self.$element).off('click.remove.logo').on('click.remove.logo', function (ev) {
                ev.preventDefault();
                var options = {
                    url: AJAX_URL,
                    type: 'post',
                    data: {action: 'remove_logo'},
                    dataType: 'json',
                    beforeSend: function (xhr, settings) {
                        self.$element.block(IMAGE_LOADING);
                        return;
                    },
                    error: function (xhr, status, err) {
                        //$response.removeClass('hidden').addClass('error').removeClass('success');
                        if (('responseJSON' in xhr) && xhr.responseJSON && ('message' in xhr.responseJSON)) {
                            //$response.html(xhr.responseJSON.message);
                            console.log(xhr.responseJSON.message);
                        }
                        return;
                    },
                    success: function (response, status, xhr) {
                        if (xhr.status == 201 && $.isPlainObject(response) && 'message' in response) {
                            location.reload();
                        }

                        return;
                    },
                    complete: function (xhr, status) {
                        self.$element.unblock(IMAGE_LOADING);
                        return;
                    }
                };
                $.ajax(options)
            });
        },
        bootstrapColorpicker: function(){
            var self = this;
            $('[data-colorpicker]').colorpicker();
        },
        init: function(){
            var self = this;
            self.bootstrapColorpicker();
            self.initForm();
            self.initEvent();

        }
    };

    $(function(){
        $('#settings').settingsSite();
    });
});