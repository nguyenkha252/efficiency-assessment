(function($){
    $(function() {

        var $chartContainer = $('#chart-container');
        var datascource = {
            "id": 1,
            "name": "Root",
            "title": "",
            "level": 0
        };

        var allNodeData = {}, __idx = 0, getId = function() {
            __idx ++;
            return __idx;//(new Date().getTime()) * 1000 + Math.floor(Math.random() * 1001);
        };
        function addNodeContent(datascource) {
            if( !('className' in datascource) ) {
                datascource.className = 'node-level-' + datascource.level;
                if( datascource.level == 0 ) {
                    datascource.className += ' node-level-root';
                }
            }
            datascource.eid = 'node-id-' + datascource.id;
            __idx = Math.max(datascource.id, __idx);
            if( ('children' in datascource) ) {
                datascource.children.forEach(function(item, idx) {
                    datascource.children[idx] = addNodeContent(item);
                });
            }
            allNodeData[datascource.id] = datascource;
            return datascource;
        }
        datascource = $chartContainer.data('chartData') || datascource;
        var get_max_level = function(datascource) {
            var level = 0;
            var loopDepth = function(node, depth) {
                if( ('children' in node) && node.children.length ) {
                    level = Math.max(level, depth);
                    for(var i=0; i<node.children.length; i++) {
                        level = loopDepth(node.children[i], depth+1);
                    }
                }
                return level;
            }
            return loopDepth(datascource, 0);
        }
        var editable = parseInt( $chartContainer.data('editable') || '0' ), counter = 0;
        var loopVerticalDepth = function(node, depth, level) {
            var i = 0, child = null, hasVertical = true;
            if( (node.alias == 'nhanvien') || ( (node.alias == 'phongban') && node.name.match(/^(Phó|Pho|Ph\.|ph\.)\s/) ) ) {

            } else {
                for(i=0; i < node.children.length; i++ ) {
                    child = node.children[i];
                    if( ['congty', 'tapdoan', 'phongban'].indexOf(child.alias) > -1 ) {
                        hasVertical = false;
                    }
                }

                if( hasVertical ) {
                    node.verticalDepth = Math.min(level + 2, depth);
                } else {
                    node.verticalDepth = depth;
                }
            }

            for(i=0; i < node.children.length; i++ ) {
                child = node.children[i];
                child.verticalDepth = node.verticalDepth;
                node.children[i] = loopVerticalDepth(child, node.verticalDepth, level + 1);
            }
            return node;
        }
        datascource.verticalDepth = 4;
        datascource = loopVerticalDepth(datascource, datascource.verticalDepth, 0);
        var loop_has_level = function(node) {
            node.hasLevel = true;
            node.children.forEach(function(item){
                loop_has_level(item);
            });
            return node;
        }
        var chartOptions = {
            data: datascource,
            nodeId: 'eid',
            nodeTitle: 'name',
            nodeContent: 'content',
            verticalDepth: datascource.verticalDepth,
            depth: 40,
            nodeTemplate: function(nodeData) {
                if( !('className' in nodeData) ) {
                    nodeData.className = 'node-level-' + nodeData.deep;
                    if( nodeData.level == 0 ) {
                        nodeData.className += ' node-level-root';
                    }
                }
                nodeData.eid = 'node-id-' + nodeData.id;
                allNodeData[nodeData.id] = nodeData;
                __idx = Math.max(nodeData.id, __idx);

                return  '<div class="title">' + nodeData.name + '</div>' +
                    '<div class="content">'+
                    '   <div class="description">' + ( (nodeData.alias == 'phongban') ? (nodeData.room || '') : (nodeData.alias_text || '') ) + '</div>'+
                    ( editable ? (
                    '   <div class="controls">'+
                    '       <a href="javascript:;" class="node-add" data-event-name="add" data-toggle="modal" data-target="#orgchart-popup-item" '+
                    'title="Thêm"><i class="fa fa-plus"></i></a>'+ //
                    '       <a href="javascript:;" class="node-edit" data-event-name="edit" data-toggle="modal" data-target="#orgchart-popup-item" '+
                    'title="Chỉnh sửa"><i class="fa fa-pencil"></i></a>' +
                        ( (nodeData.level == 0) ? '' :
                    ('       <a href="javascript:;" class="node-remove" data-message="Bạn có thật sự muốn xóa &#34;' + nodeData.name +
                    '&#34;?" data-toggle="modal" data-target="#confirm-chart-popup" title="Xóa"><i class="fa fa-trash-o"></i></a>'))+ // fa fa-minus
                    // '       <span class="has_kpi">KPI cá nhân?<input class="checkbox-status hidden" type="checkbox" ' + (nodeData.has_kpi ? 'checked="checked"' : '') +
                    // '           readonly="readonly"/><span class="checkbox-slider"></span></span>'+
                    '   </div>'
                        ) : '' ) +
                    '</div>';
            },
            createNode: function($nodeDiv, nodeData) {
                $nodeDiv.addClass(nodeData.className);
                $nodeDiv.data('orgChart', orgChart);
                $nodeDiv.data('nodeData', nodeData);
                $nodeDiv.attr('title', nodeData.name);
                $nodeDiv.attr('data-id', nodeData.id);
                $nodeDiv.attr('data-vertical-depth', nodeData.verticalDepth);

                $nodeDiv.on('click.toggle', '.toggleBtn.fa-plus-square', function(ev) {
                    actionHistory.push(nodeData);
                });

                /*
                $nodeDiv.off('createNode.done').on('createNode.done', function(ev, nodeData, level, opts) {
                    setTimeout(function() {
                        if( [22, 28, 33, '22', '28', '33'].indexOf(nodeData.id) > -1 ) {
                            var height = $nodeDiv.outerHeight() + $chartContainer.find('tr.lines').outerHeight();
                            var $table = $nodeDiv.closest('table');
                            $table.addClass('upder-level');
                            $table.css('margin-top', height);
                        }
                    }, 2000);
                    return;
                }); */
                return;
            },
            beforeBuildHierarchy: function(nodeData, parentNodeData, level, opts) {
                return nodeData;
            },
            buildVerticalDepth: function(verticalDepth, nodeData, parentNodeData, level, opts) {
                if( $.isPlainObject(parentNodeData) && ('children' in parentNodeData) ) {
                    var i, node, hasVertical = true, count = 0;
                    if( level < 3 ) {
                        for(i=0; i < parentNodeData.children.length; i++ ) {
                            node = parentNodeData.children[i];
                            if( node.alias == 'congty' ) {
                                hasVertical = false;
                            } else if( node.alias == 'phongban' && node.alias_text.match(/^(Trưởng|Truong|Tr\.|tr\.|trưởng|truong.)/) ) {
                                hasVertical = false;
                            } else {
                                // verticalDepth = level + 1;
                            }
                        }
                        if( opts.verticalDepth < verticalDepth ) {
                            opts.verticalDepth = verticalDepth;
                        }
                    }
                }
                return verticalDepth;
            },
            createNodeDone: function($nodeDiv, nodeData, $nodeWrapper) {
                var that = this;
                if( !('hasLevel' in nodeData) && (nodeData.deep != nodeData.level) ) {
                    loop_has_level(nodeData);
                    var delta = nodeData.deep - nodeData.level;
                    // style="width: 100%; height: ' + (delta * 116) + 'px;"
                    $nodeDiv.before('<div class="level-space level-space-' + delta + '">&nbsp;</div>');
                    $nodeDiv.parent().addClass('has-level-space');
                    // $nodeWrapper.find('tr.lines');
                }
                $nodeDiv.on('transitionend.animation', function(ev) {
                    var $table = $('.node-level-root', that.$chart).closest('table');
                    var w = $table.outerWidth() + 60, h = $table.outerHeight() + 60;
                    that.$chart.width( w );
                    that.$chart.height( h );
                    $chartContainer.width( w );
                    $chartContainer.height( h );
                    return;
                });
                return this;
            },
            // All chart Node rendered
            createAllNodeDone: function () {
                // console.debug(datascource);
                var that = this, $root = $('.node-level-root', that.$chart), $table = $('.node-level-root', that.$chart).closest('table');
                var rootData = $root.data('nodeData');
                var w = $table.outerWidth() + 60, h = $table.outerHeight() + 60;
                that.$chart.width( w );
                that.$chart.height( h );
                $chartContainer.width( w );
                $chartContainer.height( h );

                var loopChart = function(node) {
                    if( node.alias == 'phongban' ) {
                        var $nodeDiv = $('[data-id="'+node.id+'"]', that.$chart);
                        var childrenState = that.getNodeState($nodeDiv, 'children');
                        if (childrenState.exist) {
                            // hide the descendant nodes of the specified node
                            if (childrenState.visible) {
                                // that.hideChildren($nodeDiv);
                                $nodeDiv.find('.edge.verticalEdge.bottomEdge.fa').trigger('click');
                            }
                        }
                    } else {
                        for(var i=0; i < node.children.length; i++ ) {
                            loopChart(node.children[i]);
                        }
                    }
                }
                setTimeout(function(){
                    // loopChart.call(that, rootData);
                }, 1000);
                return this;
            }
        };
        function produce_options(chartOptions, datasource) {
            if( arguments.length > 1 ) {
                chartOptions.data = datasource;
            }
            var max_level = get_max_level(chartOptions.data);
            // chartOptions.verticalDepth = Math.max(Math.min(max_level, 7), 3);
            chartOptions.max_level = max_level;
            return chartOptions;
        }
        chartOptions = produce_options(chartOptions);
        console.debug(chartOptions);
        var orgChart = $chartContainer.orgchart(chartOptions);
        var actionHistory = [];
        window.ah = actionHistory;

        orgChart.getTree = function() {
            var that = this, loopChart = function($chart) {
                var $tr = $chart.find('tr:first'), $node = $tr.find('.node'), nodeData = $node.data('nodeData');
                var subObj = { $node: $node, children: [] };
                $tr.siblings(':last').children().each(function() {
                    subObj.children.push( loopChart($(this)) );
                });
                return subObj;
            }
            return loopChart(that.$chart);
        }

        orgChart.getRoot = function(isData) {
            var $tr = this.$chart.find('tr:first'), $node = $tr.find('.node'), nodeData = $node.data('nodeData');
            return isData ? nodeData : $node;
        }

        orgChart.removeNode = function($node) {
            var self = this, $rootNode = this.getRoot(), rootData = $rootNode.data('nodeData'), nodeData = $node.data('nodeData');
            var loopRemoveItem = function(items) {
                var itemRemove = items.removeOfChart(nodeData);
                if( itemRemove === false ) {
                    for(var i=0; i<items.length; i++) {
                        items[i].children = loopRemoveItem(items[i].children, nodeData);
                    }
                } else {
                    var removeds = [{id: itemRemove.id}];
                    if( itemRemove.id in allNodeData ) {
                        delete allNodeData[itemRemove.id];
                    }
                    var loopRemove = function(item) {
                        if( 'children' in item ) {
                            while(item.children.length) {
                                var it = item.children.shift();
                                removeds.push({id: it.id});
                                loopRemove(it);
                                if( it.id in allNodeData ) {
                                    delete allNodeData[it.id];
                                }
                                actionHistory.removeOfChart(it);
                            }
                        }
                    }
                    if( actionHistory.indexOfChart(itemRemove) > -1 ) {
                        actionHistory.removeOfChart(itemRemove);
                    }
                    loopRemove(itemRemove);
                }
                return items;
            }

            if( $node.hasClass('level-root') || (nodeData && nodeData.level == 0) ) {
                return;
            } else {
                console.debug({removeBefore: $.extend({}, rootData)});
                if( nodeData.id in allNodeData ) {
                    if( nodeData.parentId in allNodeData ) {
                        allNodeData[nodeData.parentId].children = loopRemoveItem(allNodeData[nodeData.parentId].children, nodeData);
                    }
                } else {
                    rootData.children = loopRemoveItem(rootData.children, nodeData);
                }
                console.debug({removeAfter: rootData});
                chartOptions = produce_options(chartOptions, rootData);
                console.debug(chartOptions);
                self.init({data: rootData, verticalDepth: chartOptions.verticalDepth});
                $rootNode.data('nodeData', rootData);
                actionHistory.forEach(function(ac){
                    var $toggleBtn = $chartContainer.find('#node-id-' + ac.id + ' > .toggleBtn.fa.fa-plus-square');
                    if( $toggleBtn.length ) {
                        $toggleBtn.click();
                    }
                    return;
                });
            }
            return ;
        }
        orgChart.addNode = function($node, name) {
            var self = this, $rootNode = this.getRoot(), rootData = $rootNode.data('nodeData'), nodeData = $node.data('nodeData');
            if( !('children' in nodeData) ) {
                nodeData.children = [];
            }
            var $parent = $node.closest('');
            $('.toggleBtn.fa.fa-plus-square');
            nodeData.children.push(addNodeContent({id: getId(), name: name, title: '', level: nodeData.level + 1}));
            chartOptions = produce_options(chartOptions, rootData);
            console.debug(chartOptions);
            self.init({data: rootData, verticalDepth: chartOptions.verticalDepth});
            actionHistory.forEach(function(ac){
                var $toggleBtn = $chartContainer.find('#node-id-' + ac.id + ' > .toggleBtn.fa.fa-plus-square');
                if( $toggleBtn.length ) {
                    $toggleBtn.click();
                }
                return;
            });
            return this;
        }

        orgChart.toJson = function() {
            var $tr = this.$chart.find('tr:first'), $node = $tr.find('.node'), nodeData = $node.data('nodeData');
            var result = $.extend({}, nodeData), loopItem = function(item) {
                if( 'children' in item ) {
                    if('content' in item) {
                        delete item.content;
                    }
                    if('eid' in item) {
                        delete item.eid;
                    }
                    item.children.forEach(function(item, idx) {
                        item.children[idx] = loopItem(item);
                    });
                }
                return item;
            }
            return loopItem(result);
        }
        orgChart.xhr = null;
        orgChart.save = function() {
            var self = this;
            if( this.xhr && ('abort' in this.xhr) ) {
                this.xhr.abort('Cancel');
            }
            this.xhr = $.ajax({
                url: self.$chartContainer.data('saveUrl'),
                type: 'post',
                dataType: 'json',
                data: {node: this.toJson()},
                beforeSend: function(xhr, settings) {
                    return;
                },
                error: function(xhr, status, error) {
                    self.xhr = null;
                    return;
                },
                success: function(response, status, xhr) {
                    if( xhr.status == 201 ) {
                        // var datasource = addNodeContent(response);
                        chartOptions = produce_options(chartOptions, response.charts);
                        console.debug(chartOptions);
                        self.init({data: chartOptions.data, verticalDepth: chartOptions.verticalDepth});
                    }
                    self.xhr = null;
                    return;
                },
                complete: function(xhr, status) {
                    self.xhr = null;
                    return;
                }
            });
            return this;
        }

        orgChart.findRoomName = function(nodeData) {
            var parentData = $('.node[data-id="' + nodeData.parentId + '"]', orgChart.$chart).data('nodeData');
            if( $.isPlainObject(parentData) && ('room' in parentData) ) {
                if(parentData.room == 'phongban') {
                    return parentData.room;
                } else {
                    return orgChart.findRoomName(parentData);
                }
            }
            return '';
        }

        var $popupForm = $('#orgchart-popup-item'), $form = $popupForm.find('form'),
            $pTitle = $popupForm.find('.modal-title'),
            $pBtn = $popupForm.find('button.btn-primary'),
            $responseMsg = $popupForm.find('.response-message');
        $popupForm.on('show.bs.modal.form', function(ev) {
            var $btn = $(ev.relatedTarget), $node = $btn.closest('.node'),
                nodeData = $node.data('nodeData');
            var $node_room_view_onlevel_item = $('#node_room_view_onlevel_item');
            var eventName = $btn.data('eventName'),
                form = $form[0], ajax_url = '', target = $(form.node_alias).data('target'),
                $groupRoom= $( target, form ),
                titleText = $pTitle.attr('data-title-'+eventName),
                btnText = $pBtn.attr('data-text-'+eventName);
            $pTitle.html( titleText );
            $pBtn.html( btnText );
            $(form.node_alias).selectpicker('val', form.node_alias.value);
            $(form.node_alias).unbind('change.alias').bind('change.alias', function(ev) {
                /*if( this.value != 'bgd' ) {
                    $groupRoom.removeClass('hidden');
                } else {
                    $groupRoom.addClass('hidden');
                }*/
                return;
            });
            $('#node_name').off('focusout.check.name').on('focusout.check.name', function(ev){
                let node_alias_value = $('#node_alias').val(),
                    node_level_value = $('#node_level').val(),
                    node_view_onlevel_value = $('#node_room_view_onlevel_item .selectpicker.label_node_view_onlevel').val(),
                    node_name_value = $(this).val(),
                    node_room_value = $('#node_room').val();
                let data_ajax = {
                    action: 'check_node_name',
                    node_id: 0,
                    node_name: node_name_value,
                    node_alias: node_alias_value,
                    node_level: parseInt(node_level_value) + 1,
                    node_view_onlevel: node_view_onlevel_value,
                    node_room: node_room_value
                }
                if(eventName != 'add'){
                    data_ajax.node_id = nodeData.id;
                }
                let options = {
                    url: AJAX_URL,
                    type: 'get',
                    dataType: 'json',
                    data: data_ajax,
                    beforeSend: function(xhr, settings) {
                        $popupForm.block(IMAGE_LOADING);
                        $responseMsg.addClass('hidden').removeClass('error').removeClass('success');
                        $responseMsg.html('');
                        return;
                    },
                    error: function(xhr, status, error) {
                        $responseMsg.removeClass('hidden').addClass('error');
                        try {
                            if( ('responseJSON' in xhr) && xhr.responseJSON ) {
                                $responseMsg.html( xhr.responseJSON.message );
                            } else {
                                error = xhr.getResponseHeader('xhr-message') || error;
                                error = JSON.parse(error);
                                $responseMsg.html( error );
                            }
                        } catch(e){
                            $responseMsg.html( error );
                        }
                        return;
                    },
                    success: function(response, status, xhr) {
                        if( xhr.status == 201 ) {
                            $responseMsg.removeClass('hidden').addClass('success');
                            $responseMsg.html( response.message );
                            //$popupForm.modal('hide');
                        }
                        return;
                    },
                    complete: function(xhr, status) {
                        $popupForm.unblock(IMAGE_LOADING);
                        return;
                    }
                };
                $.ajax(options);
            });

            $('#node_alias, #node_room_view_onlevel_item .selectpicker.label_node_view_onlevel').off('change.check.name').on('change.check.name',function(ev){
                $('#node_name').trigger('focusout');
            });
            $('#node_level, #node_room').off('focusout.check.name').on('focusout.check.name', function(ev){
                $('#node_name').trigger('focusout');
            });
            if( eventName == 'add' ) {
                $node_room_view_onlevel_item.hide();
                ajax_url = $chartContainer.data('addUrl');
                form.node_name.value = '';
                form.node_alias.value = ['phongban', 'nhanvien'].indexOf(nodeData.alias) > -1 ? 'nhanvien' : '';
                form.node_room.value = orgChart.findRoomName(nodeData) || nodeData.room || '';
                form.node_has_kpi.checked = true;
                form.node_level.value = (nodeData.level + 1);
            } else {
                $form.find(".label_node_view_onlevel").val($node.data('parent'));
                $form.find(".label_node_view_onlevel").selectpicker("refresh");
                if( nodeData.parent != 0 && nodeData.parent != undefined ) {
                    $node_room_view_onlevel_item.show();
                }else{
                    $node_room_view_onlevel_item.hide();
                }
                ajax_url = $chartContainer.data('editUrl');
                form.node_name.value = nodeData.name;
                form.node_alias.value = nodeData.alias;
                form.node_level.value = nodeData.level;
                form.node_room.value = orgChart.findRoomName(nodeData) || nodeData.room || '';
                form.node_has_kpi.checked = nodeData.has_kpi;
            }
            $(form.node_alias).selectpicker('val', form.node_alias.value);
            $(form.node_alias).trigger('change.alias');
            $responseMsg.addClass('hidden').removeClass('error').removeClass('success');
            $(form.node_name).focus();
            $pBtn.unbind('click.save').bind('click.save', function() {
                var dataNode = {};
                if( eventName == 'add' ) {
                    dataNode = {
                        name: form.node_name.value,
                        parent: nodeData.id,
                        level: nodeData.level + 1,
                        room: form.node_room.value,
                        alias: form.node_alias.value,
                        has_kpi: form.node_has_kpi.checked ? 1 : 0
                    };
                    // orgChart.addNode($node, form.node_name.value);
                } else {
                    nodeData.name = form.node_name.value;
                    nodeData.room = form.node_room.value;
                    nodeData.alias = form.node_alias.value;
                    nodeData.has_kpi = form.node_has_kpi.checked ? 1 : 0;
                    nodeData.onlevel = form.node_onlevel.value;
                    $node.children('.title').html(form.node_name.value);
                    $node.children('.description').html( $(form.node_alias.options[form.node_alias.selectedIndex]).text() );
                    $node.find('.has_kpi input.checkbox-status').prop('checked', form.node_has_kpi.checked);
                    $node.attr('title', nodeData.name);
                    dataNode = {
                        name: nodeData.name, parent: nodeData.parentId,
                        // level: nodeData.level,
                        level: (form.node_level.value || '').trim() || nodeData.level,
                        room: nodeData.room,
                        id: nodeData.id, alias: nodeData.alias,
                        has_kpi: nodeData.has_kpi,
                        onlevel: nodeData.onlevel,
                    };
                }
                $.ajax({
                    url: ajax_url,
                    type: 'post',
                    dataType: 'json',
                    data: {
                        type: eventName,
                        node: dataNode
                    },
                    beforeSend: function(xhr, settings) {
                        $popupForm.block(IMAGE_LOADING);
                        $responseMsg.addClass('hidden').removeClass('error').removeClass('success');
                        $responseMsg.html('');
                        return;
                    },
                    error: function(xhr, status, error) {
                        $responseMsg.removeClass('hidden').addClass('error');
                        try {
                            if( ('responseJSON' in xhr) && xhr.responseJSON ) {
                                $responseMsg.html( xhr.responseJSON.message );
                            } else {
                                error = xhr.getResponseHeader('xhr-message') || error;
                                error = JSON.parse(error);
                                $responseMsg.html( error );
                            }
                        } catch(e){
                            $responseMsg.html( error );
                        }
                        return;
                    },
                    success: function(response, status, xhr) {
                        if( xhr.status == 201 ) {
                            $responseMsg.removeClass('hidden').addClass('success');
                            $responseMsg.html( response.message );
                            chartOptions = produce_options(chartOptions, response.charts);
                            console.debug(chartOptions);
                            orgChart.init({data: chartOptions.data, verticalDepth: chartOptions.verticalDepth});
                            $popupForm.modal('hide');
                        }
                        return;
                    },
                    complete: function(xhr, status) {
                        $popupForm.unblock(IMAGE_LOADING);
                        return;
                    }
                });
                return;
            });
            return;
        });

        var $confirmDialog = $('#confirm-chart-popup'), $confirmMsg = $confirmDialog.find('.modal-body > .message'),
            $confirmResponseMsg = $confirmDialog.find('.response-message');

        $confirmDialog.off('show.bs.modal.confirm').on('show.bs.modal.confirm', function(ev) {
            var $btn = $(ev.relatedTarget), $node = $btn.closest('.node'), nodeData = $node.data('nodeData');
            $confirmMsg.html( $btn.data('message') );
            var ajax_url = $chartContainer.data('removeUrl');
            $confirmResponseMsg.addClass('hidden').removeClass('success').removeClass('error').html('');
            $confirmDialog.off('click.ok', '.btn.btn-primary').on('click.ok', '.btn.btn-primary', function() {
                $.ajax({
                    url: ajax_url,
                    type: 'post',
                    dataType: 'json',
                    data: {id: nodeData.id},
                    beforeSend: function(xhr, settings) {
                        $confirmResponseMsg.addClass('hidden').removeClass('success').removeClass('error').html('');
                        $confirmDialog.block(IMAGE_LOADING);
                        return;
                    },
                    error: function(xhr, status, error) {
                        $confirmResponseMsg.removeClass('hidden').addClass('error');
                        try {
                            if( ('responseJSON' in xhr) && xhr.responseJSON && ('message' in xhr.responseJSON) ) {
                                $confirmResponseMsg.html( xhr.responseJSON.message );
                            } else {
                                error = xhr.getResponseHeader('xhr-message') || error;
                                error = JSON.parse(error);
                                $confirmResponseMsg.html( error );
                            }
                        } catch(e){
                            $confirmResponseMsg.html( error );
                        }
                        return;
                    },
                    success: function(response, status, xhr) {
                        // orgChart.removeNode($node);
                        if( xhr.status == 202 ) {
                            $confirmResponseMsg.removeClass('hidden').addClass('success');
                            $confirmResponseMsg.html( response.message );
                            /* chartOptions = produce_options(chartOptions, response.charts);
                            console.debug(chartOptions);
                            orgChart.init({data: chartOptions.data, verticalDepth: chartOptions.verticalDepth});
                            $confirmDialog.modal('hide'); */
                            window.top.location.reload();
                        }
                        return;
                    },
                    complete: function(xhr, status) {
                        $confirmDialog.unblock(IMAGE_LOADING);
                        return;
                    }
                });
                return;
            });
        });
        window.orgChart = orgChart;
        return;
    });
    Array.prototype.removeOfChart = function(item) {
        for(var idx = 0; idx < this.length; idx++) {
            if( this[idx].id == item.id ) {
                for(; idx < this.length-1; idx++) {
                    this[idx] = this[idx+1];
                }
                var itemRemove = this.pop();
                return itemRemove;
            }
        }
        return false;
    }
    Array.prototype.indexOfChart = function(item) {
        for(var idx = 0; idx < this.length; idx++) {
            if( this[idx].id == item.id ) {
                return idx;
            }
        }
        return -1;
    }
})(jQuery);

var max_level = 20;
function find_node_level($node, level) {
    var $ = jQuery, $parent = $node.closest('tr.nodes');
    // console.debug({$table: $parent});
    $parent = $parent.length ? $parent.parent() : $();
    // console.debug({$table: $parent});
    $parent = $parent.length ? $parent.find(' > tr:first > td > .node') : $();
    // console.debug({$node: $node, level: level});
    if( level > max_level ) {
        return false;
    }
    if( $parent.length ) {
        if( $parent.hasClass('level-root') ) {
            // console.debug({$node: $parent, level: level + 1});
            return level + 1;
        }
        return find_node_level($parent, level + 1);
    }
    return level;
}
/*
$node = jQuery('.node.node-test-level');
level = find_node_level($node, 1);
*/