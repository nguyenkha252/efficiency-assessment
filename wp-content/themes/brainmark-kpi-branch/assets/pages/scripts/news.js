(function($) {
    $(function() {
        var $form = $('form#news-post-page');
        if ($form.length) {
            $form.bind('submit', function (ev) {
                ev.preventDefault();
                $form.ajaxSubmit({
                    dataType: 'json',
                    type: 'post',
                    beforeSend: function (xhr, settings) {
                        $form.find('[type="submit"]').prop('disabled', true);
                        $form.find('.message.hidden').removeClass('hidden');
                        var tiny = tinyMCE.get('post_content');
                        if( tiny ) {
                            tiny.save();
                        }
                        return;
                    },
                    error: function (xhr, err, msg, $fr) {
                        if( 'responseJSON' in xhr && xhr.responseJSON.code ) {
                            $form.find('.message').html('<p class="error-' + xhr.responseJSON.code + '">' + xhr.responseJSON.message + '</p>');
                        }
                        return;
                    },
                    success: function (response, status, xhr, $fr) {
                        try {
                            $form.resetForm();
                        } catch(e){}
                        location.reload();
                        return;
                    },
                    complete: function (xhr, status, $fr) {
                        $form.find('[type="submit"]').prop('disabled', false);
                        return;
                    }
                });
                return;
            });
        }
        return;
    });
})(jQuery);