(function($) {
    $('form.changepassword').validate({
        rules:{
            user_pass: {
                required: true,
                minlength: 4,
            },
            user_new_pass: {
                required: true,
                minlength: 4,
            },
            re_user_pass: {
                equalTo: "#user_new_pass"
            },
        },
        messages: {
            user_pass: {
                required: "Mật khẩu không được bỏ trống",
                minlength: "Mật khẩu ít nhất 4 ký tự",
            },
            user_new_pass: {
                required: "Mật khẩu mới không được bỏ trống",
                minlength: "Mật khẩu ít nhất 4 ký tự",
            },
            re_user_pass: {
                equalTo: "Mật khẩu mới và Nhập lại mật khẩu không trùng khớp"
            },
        }
    });
    var $form = $('form.user-form'), isEdit = $form.hasClass('user-edit-form'),
        $orgchartType = $form.find('#type_orgchart_id'),
        $orgchartRoom = $form.find('#phongban_orgchart_id'),
        $orgchartRoomContainer = $orgchartRoom.closest('.form-group[data-group="phongban"]'),
        $orgchartId = $form.find('#orgchart_id'),
        dataOrgchart = $orgchartId.data(),
        orgchartType = $orgchartType.get(0),
        orgchartRoom = $orgchartRoom.get(0),
        orgchartId = $orgchartId.get(0);
    if( !orgchartId ) return;

    orgchartId.$children = dataOrgchart.chartsTree;
    // $orgchartId.bind('changed.bs.select.chucdanh', function(ev, clickedIndex, checked, state) {
    $orgchartId.off('change.chucdanh').on('change.chucdanh', function(ev, clickedIndex, checked, state) {
        //console.debug({orgchartId: ["changed.bs.select.chucdanh", clickedIndex, checked, state]});
        var selectpicker = $orgchartId.data('selectpicker');
        // console.debug(arguments);
        return;
    });

    // $orgchartRoom.bind('changed.bs.select.phongban', function(ev, clickedIndex, checked, state) {
    $orgchartRoom.off('change.phongban').on('change.phongban', function(ev, clickedIndex, checked, state) {
        //console.debug({orgchartRoom: ["changed.bs.select.phongban", clickedIndex, checked, state]});
        var phongbanId = $orgchartRoom.val();
        var objRoom = orgchartId.$children.filter(function(item, idx){
            return item.id == phongbanId;
        });
        if( objRoom != undefined || objRoom != '' ){
            show_chuc_danh_theo_phongban(objRoom[0].room);
        }

        return;
    });
    var timeoutKeyupEmail = null;
    var timeoutKeyupCode = null;
    $('#user_email').off("keyup forcusout").on("keyup focusout",function(ev){
        clearTimeout( timeoutKeyupEmail );
        var $this = $(this);
        var self = this;
        timeoutKeyupEmail = setTimeout(function(){
            if( ('checkEmailUser' in self) && this.checkEmailUser && this.checkEmailUser.abort ) {
                self.checkCodeUser.abort('Cancel Request Check Code User');
            }
            var email = $this.val();
            var id = $this.closest('form').find('[name="ID"]').val();
            var $formGroup = $this.closest('.form-group');
            var options = {
                url: AJAX_URL,
                data: {action: 'check_email_user', email: email, ID: id},
                dataType: 'json',
                type: 'get',
                beforeSend: function (xhr, settings) {
                    $formGroup.find('.notification').remove();
                    return;
                },
                error: function (xhr, status, error) {
                    //$form.unblock(IMAGE_LOADING);
                    if (('responseJSON' in xhr) && ('message' in xhr.responseJSON)) {
                        error = xhr.responseJSON.message;
                    } else {
                        error = xhr.getResponseHeader('xhr-message') || error;
                        try {
                            error = JSON.parse(error);
                        } catch (e) {
                        }
                    }
                    var $label = $('<label class="notification error"></label>');
                    $label.html(error);
                    $formGroup.prepend($label);
                    return;
                },
                success: function (response, status, xhr) {
                    if ($.isPlainObject(response) && 'message' in response) {
                        var $label = $('<label class="notification success"></label>');
                        $label.html(response.message);
                        $formGroup.prepend($label);
                    }
                    return;
                },
                complete: function (xhr, status) {
                    return;
                }
            };
            self.checkEmailUser = $.ajax(options);
        },500);
    });
    $('#user_nicename').off("keyup forcusout").on("keyup focusout",function(ev){
        clearTimeout( timeoutKeyupCode );
        var $this = $(this);
        var self = this;
        timeoutKeyupCode = setTimeout(function(){
            if( ('checkCodeUser' in self) && this.checkCodeUser && this.checkCodeUser.abort ) {
                self.checkCodeUser.abort('Cancel Request Check Code User');
            }
            var manv = $this.val();
            var id = $this.closest('form').find('[name="ID"]').val();
            var $formGroup = $this.closest('.form-group');
            var options = {
                url: AJAX_URL,
                data: {action: 'check_code_user', code: manv, ID: id},
                dataType: 'json',
                type: 'get',
                beforeSend: function (xhr, settings) {
                    $formGroup.find('.notification').remove();
                    return;
                },
                error: function (xhr, status, error) {
                    //$form.unblock(IMAGE_LOADING);
                    if (('responseJSON' in xhr) && ('message' in xhr.responseJSON)) {
                        error = xhr.responseJSON.message;
                    } else {
                        error = xhr.getResponseHeader('xhr-message') || error;
                        try {
                            error = JSON.parse(error);
                        } catch (e) {
                        }
                    }
                    var $label = $('<label class="notification error"></label>');
                    $label.html(error);
                    $formGroup.prepend($label);
                    return;
                },
                success: function (response, status, xhr) {
                    if ($.isPlainObject(response) && 'message' in response) {
                        var $label = $('<label class="notification success"></label>');
                        $label.html(response.message);
                        $formGroup.prepend($label);
                    }
                    return;
                },
                complete: function (xhr, status) {
                    return;
                }
            };
            self.checkCodeUser = $.ajax(options);
        },500);
    });

    // $orgchartType.bind('changed.bs.select.type', function(ev, clickedIndex, checked, state) {
    $orgchartType.off('change.type').on('change.type', function(ev, clickedIndex, checked, state) {
        //console.debug({orgchartType: ["changed.bs.select.type", clickedIndex, checked, state]});
        var val = $orgchartType.val();
        if( val == 'congty' ) {
            $orgchartRoomContainer.addClass('hidden');
            show_chuc_danh_congty();
        } else {
            $orgchartRoomContainer.removeClass('hidden');
            show_chuc_danh_phongban();
        }
        return;
    });

    function show_chuc_danh_congty() {
        $orgchartId.empty();
        let chartID = '';
        var objRoom = orgchartId.$children.filter(function(item, idx){
            return item.alias == 'tapdoan' || item.alias == 'congty';
        });
        if(objRoom.length){
            objRoom.forEach(function (item, index) {
                if(chartID == ''){
                    chartID = item.id;
                }
                $orgchartId.append(render_option_name(item));
            });
        }
        $orgchartId.selectpicker('val', chartID);
        $orgchartId.selectpicker('refresh');
        return;
    }

    function find_tree_item(node, id) {
        var result = null;
        if(id == ''){
            result = node;
        } else if( node.id == id ) {
            result = node;
        } else {
            for(var i=0; i < node.children.length; i++) {
                result = find_tree_item(node.children[i], id);
                if( result ) {
                    break;
                }
            }
        }
        return result;
    }

    function loop_tree_values(node) {
        var result = [$.extend({}, node)];
        delete result[0]['children'];
        for(var i=0; i < node.children.length; i++) {
            result = result.concat( loop_tree_values(node.children[i]) );
        }
        return result;
    }

    function show_chuc_danh_phongban() {
        $orgchartRoom.empty();
        let chartID = '';
        if( $orgchartRoom.data('chart-id') != '' ){
            chartID = $orgchartRoom.data('chart-id');
        }
        let rooms = [];
        var objRoom = orgchartId.$children.filter(function(item, idx){
            return item.alias == 'phongban';
        });
        if(objRoom.length){
            objRoom.forEach(function (item, index) {
                if(chartID == ''){
                    chartID = item.id;
                }
                if( $.inArray(item.room, rooms) === -1 ) {
                    rooms.push(item.room);
                    $orgchartRoom.append(render_option_room(item));
                }
            });
        }
        $orgchartRoom.selectpicker('val', chartID);
        $orgchartRoom.selectpicker('refresh');
        $orgchartRoom.trigger('change');
        return;
    }
    function render_option_room(orgchartObj){
        return '<option value="' + orgchartObj.id + '">' + orgchartObj.room + '</option>';
    }
    function render_option_name(orgchartObj){
        return '<option value="' + orgchartObj.id + '">' + orgchartObj.name + '</option>';
    }
    function render_loop_option(obj, parent){
        var orgchart = [], html = '';
        Object.keys(obj).forEach(function(id){
            if( obj[id].parent == parent ){
                orgchart.push(obj[id]);
                //delete obj[id];
            }
        });
        if( orgchart ){
            orgchart.forEach(function(chart) {
                var outputChild = '';
                if (chart.parent > 0) {
                    html += '<option value="' +chart.id + '">' + chart.name + '</option>';
                }
                if (obj) {
                    outputChild += render_loop_option(obj, chart.id);
                }
                html += outputChild;
            });
        }
        return html;
    }
    function render_loop_option_room(obj, room){
        var orgchart = [], html = '';
        Object.keys(obj).forEach(function(id){
            if( obj[id].room == room ){
                orgchart.push(obj[id]);
                //delete obj[id];
            }
        });
        if( orgchart ){
            orgchart.forEach(function(chart) {
                var outputChild = '';
                if (chart.parent > 0) {
                    html += '<option value="' +chart.id + '">' + chart.name + '</option>';
                }
                if (obj) {
                    outputChild += render_loop_option(obj, chart.room);
                }
                html += outputChild;
            });
        }
        return html;
    }
    function show_chuc_danh_theo_phongban(room) {
        $orgchartId.empty();
        let options = render_loop_option_room(orgchartId.$children, room);
        if( options ){
            $orgchartId.html(options);
        }
        if( $orgchartId.data('chart-id') != '' ){
            $orgchartId.val($orgchartId.data('chart-id'));
        }
        $orgchartId.selectpicker('refresh');
        return;
    }

    function show_chuc_danh_mac_dinh() {
        $orgchartId.empty();
        var chartId = $orgchartId.data('chartId') || '', $selected = orgchartId.$children.filter('[value=""]');
        if( $selected.length ) {
            $orgchartId.append( $selected );
        }
        if( chartId > '' ) {
            $selected = orgchartId.$children.filter('[value="'+chartId+'"]');
            if( $selected.length && ($selected.val() > '') ) {
                $orgchartId.append( $selected );
            }
        }
        $orgchartId.selectpicker('val', chartId);
        $orgchartId.selectpicker('refresh');
        return;
    }

    $(document).ready(function($) {
        //show_chuc_danh_mac_dinh();
        show_chuc_danh_congty();
        $orgchartType.trigger('change');
        // $orgchartType.change();
        // $orgchartType.trigger('changed.bs.select');
    });

    return;


})(jQuery);
