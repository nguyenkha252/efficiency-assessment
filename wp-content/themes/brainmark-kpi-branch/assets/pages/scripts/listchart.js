(function($) {
    function ListOrgChart($root, opts){
        var self = this;
        var items = opts.items;
        self.options = opts;
        self.$root = $root;
        self.root = [];
        self.loaded = false;
        self.xhrSaving = false;

        if( typeof(items) == 'object' ) {
            if( items instanceof Array ) {
                var first = true, i, nodes = {};
                for(i = 0; i < items.length; i++) {
                    nodes[items[i].id] = new Node(self, items[i]);
                }
                $root.empty();
                // generate parent child tree
                for(i in nodes){
                    if( ('parent' in nodes[i]) ) {
                        if( nodes[i].data.parent == 0) {
                            self.root.push( nodes[i] );
                            $root.append( nodes[i].$element );
                            nodes[i].$element.addClass('level-root');
                        } else {
                            nodes[nodes[i].data.parent].append( nodes[i] );
                        }
                    }
                }
            } else if( $.isPlainObject(items) && ('children' in items) ) {

            } else {
                if( 'children' in items ) {
                    self.root.push( new Node(self, items) );
                    $root.append( self.root[0].$element );
                } else {
                    self.root.push( new Node(self, {id: 1, name: 'Root', parent: 0}) );
                    $root.append( self.root[0].$element );
                    console.log("Invalid data for root node item");
                }
            }
        } else {
            var processTree = function($children, parent) {
                $children.children('.node').each(function(idx, li) {
                    var $li = $(li), data = $li.data();
                    if( !('node' in data) ) {
                        data.$element = $li;
                        var node = new Node(self, data);
                        node.level = parent ? (parent.level + 1) : 0;
                        node.parent = parent;
                        if( !parent ) {
                            node.$element.addClass('level-root');
                        }
                        data.node = node;
                    }
                    processTree(data.node.$children, data.node);
                });
            }
            processTree($root, null);
        }

        this.initEvents();

        if( typeof(self.options.onInit) == 'function' ) {
            self.options.onInit.call(this);
        }

        this.update();
        this.$root.data('listOrgChart', this);
        self.loaded = true;
        return self;
    }

    ListOrgChart.prototype = {
        save: function() {
            var $ = jQuery, self = this, ids = [];
            if( !self.loaded ) {
                return self;
            }
            if( (typeof($) != 'function') ) {
                if( '_timeSave' in self && self._timeSave ) {
                    clearTimeout(self._timeSave);
                }
                self._timeSave = setTimeout(function() {
                    self.save();
                }, 100);
                return;
            }

            var processNode = function($list) {
                var results = [];
                $list.children('li.node').each(function() {
                    var node = $(this).data('node');
                    ids.push(node.data.id);
                    var result = {
                        id: node.data.id,
                        children: processNode(node.$children)
                    };
                    results.push(result);
                });
                return results;
            }

            var tree = processNode( this.$root );
            var url = this.$root.data('update-url');
            if( self.xhrSaving ) {
                self.xhrSaving.abort("Cancel");
            }
            self.xhrSaving = $.ajax({
                url: url,
                dataType: 'json',
                type: 'post',
                data: {
                    ids: ids,
                    tree: tree
                },
                beforeSend: function(xhr, settings) {
                    return;
                },
                error: function(xhr, status, throwErr) {
                    return;
                },
                success: function(response, status, xhr) {
                    return;
                },
                complete: function(xhr, status) {
                    self.xhrSaving = null;
                    return;
                }
            });
            return this;
        },
        update: function(){
            this.$root.children('li.node').each(function() {
                var node = $(this).data('node');
                node ? node.update() : null;
            });
            this.save();
            return this;
        },
        destroy: function() {
            this.$root.off('click.node.add', this.options.addCls);
            this.$root.off('click.node.remove', this.options.removeCls);
            this.$root.off('click.node', 'li.node > .node-data');
            this.$root.children('li.node').each(function() {
                var node = $(this).data('node');
                node ? node.disableDragable() : null;
            });

            this.$root.removeData('listOrgChart');
            return this;
        },
        initEvents: function() {
            var self = this;
            if( self.options.canAdd ) {
                self.$root.off('click.node.add', self.options.addCls)
                    .on('click.node.add', self.options.addCls, function(ev){
                        ev.preventDefault();
                        var node = $(this).closest('li.node').data('node');
                        var parent = node.parent;
                        var data = $.extend({}, self.options.defaultItem, {id: self.nextId()} );
                        data.name = data.name + ' ' + data.id;
                        var newNode = node.append( data );
                        if( typeof(self.options.onAddNode) == 'function' ) {
                            self.options.onAddNode.call(self, newNode, node, ev);
                        }
                    });
            }
            if( self.options.canEdit ) {
                self.$root.off('click.node.remove', self.options.removeCls)
                    .on('click.node.remove', self.options.removeCls, function (ev) {
                        ev.preventDefault();
                        var node = $(this).closest('li.node').data('node');
                        var parent = node.parent;
                        node.remove();
                        if (typeof(self.options.onDeleteNode) == 'function') {
                            self.options.onDeleteNode.call(self, node, parent, ev);
                        }
                    });
                self.$root.off('click.node', 'li.node > .node-data')
                    .on('click.node', 'li.node > .node-data', function (ev) {
                        ev.preventDefault();
                        var node = $(this).closest('li.node').data('node');
                        if (typeof(self.options.onClickNode) == 'function') {
                            self.options.onClickNode.call(self, node, ev);
                        }
                    });
                self.enableDragable();
            }
            return;
        },
        enableDragable: function(){
            if( this.options.canEdit ) {
                this.$root.children('li.node').each(function () {
                    var node = $(this).data('node');
                    node ? node.disableDragable() : null;
                    node ? node.enableDragable() : null;
                });
            }
            return this;
        },
        disableDragable: function() {
            if( this.options.canEdit ) {
                this.$root.children('li.node').each(function () {
                    var node = $(this).data('node');
                    node ? node.disableDragable() : null;
                });
            }
            return this;
        },
        nextId: function() {
            var self = this, nextId = 0;
            self.root.forEach(function(node) {
                nextId = Math.max(nextId, node.maxId());
            });
            return nextId + 1;
        },
        getIds: function() {
            var self = this, ids = [];
            var processTree = function($children) {
                $children.children('.node').each(function(idx, li) {
                    var $li = $(li), node = $li.data('node');
                    if( node ) {
                        ids.push( node.data.id );
                        processTree( node.$children );
                    }
                });
            }
            processTree( self.$root );
            return ids;
        },
        list: function(callback) {
            var self = this;
            var url = (self.$root.data('get-item-url') || '');
            url = url.replace(/\#ID\#/, '0');
            if( url > '' ) {
                $.ajax({
                    url: url,
                    type: 'get',
                    dataType: 'json',
                    data: {single: false, exclude: self.getIds()},
                    beforeSend: function(xhr, settings) {
                        return;
                    },
                    error: function(xhr, err, throwErr) {
                        callback.call(self, {xhr: xhr, err: err, throwErr: throwErr}, null);
                        return;
                    },
                    success: function(response, status, xhr) {
                        callback.call(self, null, {response: response, status: status, xhr: xhr});
                        return;
                    },
                    complete: function(xhr, status) {
                        return;
                    }
                });
            } else {
                console.log("Not config URL when load ajax for this tree");
            }
        }
    }

    function Node(chart, data) {
        var self = this;
        self.data = data;
        self.level = 0;
        self.chart = chart;
        self.root = null;
        self.parent = null;
        self.$holder = $('<li class="node node-holder"></li>');
        if( data ) {
            self.setData(data);
        }
        return self;
    }

    Node.prototype = {
        isRoot: function() {
            return this.parent == null;
        },
        maxId: function() {
            var self = this, max = this.data.id;
            self.$children.children('li.node').each(function() {
                var node = $(this).data('node');
                max = Math.max(max, node.maxId());
            });
            return max;
        },
        update: function() {
            var self = this;
            var $lis = self.$children.children('li.node');

            if( !$lis.length ) {
                self.$element.addClass('node-leaf');
            } else {
                self.$element.removeClass('node-leaf');
                $lis.each(function(){
                    var node = $(this).data('node');
                    node ? node.update() : null;
                });
                if( this.isRoot() ) {
                    this.$element.addClass('node-root');
                }
            }
            return self;
        },
        enableDragable: function() {
            var self = this;
            if( !self.chart.options.canEdit ) {
                return self;
            }
            // Drag Item
            if( false ) self.$element.draggable({
                handle: self.chart.options.dragHandle,
                cancel: self.chart.options.dragCancel, // clicking an icon won't initiate dragging
                revert: "invalid", // when not dropped, the item will revert back to its initial position
                containment: self.chart.$root,
                helper: "clone",
                cursor: "move",
                items: '> li.node.ui-widget-content',
                start: function(ev, ui) {
                    self.chart.$root.addClass('dragging');
                    self.$holder.css({width: self.$element.css('width'), height: self.$element.css('height')});
                    self.$holder.before(self.$element);

                    console.log({dragStart: ui.helper});
                },
                drag: function(ev, ui) {

                },
                stop: function(ev, ui) {
                    self.chart.$root.removeClass('dragging');
                    self.$holder.remove();
                    self.chart.update();
                    console.log({dragStop: ui.helper});
                }
            });

            if( false ) self.$children.sortable({
                handle: self.chart.options.dragHandle,
                cancel: self.chart.options.dragCancel,
                containment: self.chart.$root,
                helper: "clone",
                cursor: "move",
                items: '> li.node.ui-widget-content',
                placeholder: "ui-state-highlight",
                connectWith: "ul.node-children"
            });

            if( false ) self.$children.disableSelection();

            if( false ) self.$children.droppable({
                accept: "li.node.ui-widget-content",
                classes: {
                    "ui-droppable-active": "ui-state-active",
                    "ui-droppable-hover": "ui-state-hover"
                },
                drop: function( event, ui ) {
                    // $( this ).addClass( "ui-state-highlight" );
                    $( this ).append(ui.draggable);
                    var node = $(ui.draggable).data('node');
                    $(ui.draggable).removeAttr('style');
                    node ? node.updateParent() : null;
                    self.chart.update();
                    return;
                }
            });
            if( false ) self.$element.droppable({
                accept: "li.node.ui-widget-content",
                classes: {
                    "ui-droppable-active": "ui-state-active",
                    "ui-droppable-hover": "ui-state-hover"
                },
                drop: function( event, ui ) {
                    // $( this ).addClass( "ui-state-highlight" );
                    self.$children.append(ui.draggable);
                    var node = $(ui.draggable).data('node');
                    $(ui.draggable).removeAttr('style');
                    node ? node.updateParent() : null;
                    self.chart.update();
                    return;
                }
            });
            self.$children.children('li.node').each(function(){
                var node = $(this).data('node');
                node ? node.enableDragable() : null;
            });

            return self;
        },
        disableDragable: function() {
            var self = this;
            if( !self.chart.options.canEdit ) {
                return self;
            }
            try{
                if( false ) self.$element.draggable( "destroy" );
            } catch(e){}

            try{
                if( false ) self.$children.sortable( "destroy" );
            } catch(e){}

            try{
                if( false ) self.$children.droppable( "destroy" );
            } catch(e){}

            self.$children.children('li.node').each(function(){
                var node = $(this).data('node');
                node ? node.disableDragable() : null;
            });
            return self;
        },
        updateParent: function() {
            var node = this.$element.closest('li.node');
            this.parent = node ? node : null;
            this.root = null;
            this.chart.update();
            return this;
        },
        setData: function(data) {
            var self = this;
            self.data = data;
            if( !('$element' in self) ) {
                if( '$element' in data ) {
                    self.$element = data.$element;
                } else {
                    self.$element = $('<li class="node level-'+self.level+' ui-widget-content" ><i class="glyphicon glyphicon-move"></i><div class="node-data"></div><ul class="node-children"></ul></li>');
                }
            }

            self.$area = self.$element.children('div.node-data');
            self.$children = self.$element.children('ul.node-children');
            self.$element.data('node', self);

            if( ('cls' in data) && (data.cls > '') ) {
                self.$element.addClass(data.cls);
            }
            if( ('childCls' in data) && (data.childCls > '') ) {
                self.$children.addClass(data.childCls.replace(/\s{0,}float\s{0,}/, ''));
            }
            self.chart.options.renderNode.call(self.chart, self);

            data.children = (data.children || []);
            data.children.forEach(function(item) {
                self.append(item);
            });
            return self;
        },
        load: function(single, callback) {
            var self = this;
            if( ('xhr' in self) && self.xhr ) {
                setTimeout(function () {
                    self.load();
                }, 100);
                return;
            }
            var root = this.getRoot();
            var url = (self.chart.$root.data('get-item-url') || '').replace(/\#ID\#/, self.data.id || this.getRoot().data.id || 0);
            if( url > '' ) {
                self.xhr = $.ajax({
                    url: url,
                    type: 'get',
                    dataType: 'json',
                    data: {single: single ? true : false},
                    beforeSend: function(xhr, settings) {
                        self.$element.addClass('loading');
                        self.loading = true;
                        return;
                    },
                    error: function(xhr, err, throwErr) {
                        callback(err, null);
                        return;
                    },
                    success: function(response, status, xhr) {
                        if( single ) {
                            response.childCls = self.data.childCls;
                            self.setData(response);
                            callback(null, response);
                        } else {
                            /* response.forEach(function(item, idx) {
                                self.append(item);
                            }); */
                        }
                        return;
                    },
                    complete: function(xhr, status) {
                        self.loading = false;
                        self.$element.removeClass('loading');
                        self.xhr = null;
                        return;
                    }
                });
            } else {
                console.log("Not config URL when load ajax for this tree");
            }
            return this;
        },
        save: function() {
            var self = this;
            if( ('xhr' in self) && self.xhr ) {
                setTimeout(function () {
                    self.save();
                }, 100);
                return;
            }
            var root = this.getRoot();
            var url = (root.$element.data('item-url') || '').replace(/\#ID\#/, self.data.id || this.getRoot().data.id || 0);
            if( url > '' ) {
                self.xhr = $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'json',
                    data: {node: self.toJSON(true)},
                    beforeSend: function(xhr, settings) {
                        self.$element.addClass('saving');
                        self.saving = true;
                        return;
                    },
                    error: function(xhr, err, throwErr) {
                        return;
                    },
                    success: function(response, status, xhr) {
                        response.forEach(function(item, idx) {
                            self.append(item);
                        });
                        return;
                    },
                    complete: function(xhr, status) {
                        self.saving = false;
                        self.$element.removeClass('saving');
                        self.xhr = null;
                        return;
                    }
                });
            } else {
                console.log("Not config URL when load ajax for this tree");
            }
            return this;
        },
        getRoot: function() {
            if( !this.root ) {
                this.root = this;
                while(this.root.parent !== null) {
                    this.root = this.root.parent;
                }
            }
            return this.root;
        },
        remove: function() {
            if( this.isRoot() ) {
                return;
            }
            var parent = this.parent;
            this.$children.children('li.node').each(function() {
                parent.append( $(this).data('node') );
            });
            this.$element.remove();
            this.chart.update();

            return this;
        },
        append: function(node) {
            var self = this;
            if( $.isPlainObject(node) ) {
                node = new Node(self.chart, node);
            }
            node.parent = self;
            self.$children.append(node.$element);
            self.chart.options.renderNode.call(self.chart, node);
            node.$element.removeClass('level-' + node.level);
            node.level = self.level + 1;
            node.$element.addClass('level-' + node.level);

            if( node.$element.data('childCls') ) {
                node.$element.removeClass( node.$element.data('childCls') );
            }
            node.$element.removeData('childCls');

            if( ('childCls' in self.data) && (self.data.childCls > '' ) ) {
                node.$element.data('childCls', self.data.childCls.replace(/\s{0,}clearfix\s{0,}/, ''));
                node.$element.addClass(self.data.childCls.replace(/\s{0,}clearfix\s{0,}/, ''));
            }
            self.update();
            self.chart.save();
            return self;
        },
        before: function( node ) {
            var self = this;
            if( $.isPlainObject(node) ) {
                node = new Node(self.chart, node);
            }
            if( node.parent ) {
                self.$element.before( node.parent.$element );
                self.chart.options.renderNode.call(self.chart, node);
                self.$element.removeClass('level-' + self.level);
                self.level = node.level + 1;
                self.$element.addClass('level-' + self.level);

                if( node.$element.data('childCls') ) {
                    node.$element.removeClass( node.$element.data('childCls') );
                }
                node.$element.removeData('childCls');

                if( ('childCls' in self.data) && (self.data.childCls > '' ) ) {
                    node.$element.data('childCls', self.data.childCls.replace(/\s{0,}clearfix\s{0,}/, ''));
                    node.$element.addClass(self.data.childCls.replace(/\s{0,}clearfix\s{0,}/, ''));
                }
                self.parent ? self.parent.update() : null;
            }
            self.chart.save();
            return node;
        },
        after: function( node ) {
            var self = this;
            if( $.isPlainObject(node) ) {
                node = new Node(self.chart, node);
            }
            if( node.parent ) {
                self.$element.after( node.parent.$element );
                self.chart.options.renderNode.call(self.chart, node);
                self.$element.removeClass('level-' + self.level);
                self.level = node.level + 1;
                self.$element.addClass('level-' + self.level);

                if( node.$element.data('childCls') ) {
                    node.$element.removeClass( node.$element.data('childCls') );
                }
                node.$element.removeData('childCls');

                if( ('childCls' in self.data) && (self.data.childCls > '' ) ) {
                    node.$element.data('childCls', self.data.childCls.replace(/\s{0,}clearfix\s{0,}/, ''));
                    node.$element.addClass(self.data.childCls.replace(/\s{0,}clearfix\s{0,}/, ''));
                }
                self.parent ? self.parent.update() : null;
            }
            self.chart.save();
            return node;
        },
        toJSON: function(isSingle) {
            var self = this, result = $.extend({}, self.data);
            result.children = [];
            if( isSingle ) {
                delete result.children;
                return result;
            }
            self.$children.children('li.node').each(function(idx, li) {
                var $li = $(li), node = $li.data('node');
                result.children.push( node.toJSON() );
            });
            return result;
        }
    }

    $.fn.listOrgChart = function(options) {
        $(this).each(function() {
            var listOrgChart = $(this).data('listOrgChart');
            if( !listOrgChart ) {
                var opts = $.extend({}, $.fn.listOrgChart.defaults, $(this).data() || {}, options);
                listOrgChart = new ListOrgChart($(this), opts);
            }
            return listOrgChart;
        });
        return;
    }

    $.fn.listOrgChart.defaults = {
        items: [], // [{id:1, name:'Root', parent: 0}, {id:2, name:'Child 1 Level 1', parent: 1}, {id:2, name:'Child 2 Level 1', parent: 1}]
        showControls: false,
        allowEdit: false,
        dragHandle: ' > i.glyphicon.glyphicon-move',
        dragCancel: ' > div.node-data, ul.node-children',
        onAddNode: null,
        onDeleteNode: null,
        onClickNode: null,
        onInit: null,
        defaultItem: {id: 0, name: 'New Node', parent: 0, description: '', avatar: ''},
        addCls: 'li.node > .node-data .actions [data-event="add-node"]',
        removeCls: 'li.node > .node-data .actions [data-event="remove-node"]',
        addText: 'Add Child',
        removeText: 'Remove Child',
        canAdd: true,
        canEdit: true,
        renderNode: function(node) {
            var avatar = node.chart.options.defaultItem.avatar;
            node.$area.html('<div class="info"><a class="name-avatar" href="#">' +
                ( ('avatar' in node.data) ? ('<img class="avatar" '+((avatar > '') ? 'onerror="this.src=\''+avatar+'\'"' : '') +
                    ' src="'+node.data.avatar+'" width="150" alt="'+node.data.name+'"/>') : '') +
                '</a><br><span class="name">' + node.data.name + '</span><br><span class="description">' + node.data.description +
                '</span><br><span class="user-id">ID: ' + node.data.id + '</span></div>' +
                '<div class="actions clearfix">' +
                (node.chart.options.canAdd ? '<a class="btn-add" data-event="add-node" href="#">'+this.options.addText+'</a>&nbsp;' : '') +
                (node.chart.options.canEdit ? '<a class="btn-remove" data-event="remove-node" href="#">'+this.options.removeText+'</a>' : '') +
                '</div></div>');
            // node.$children
            return node;
        }
    };

    $.fn.listOrgChart.constructor = ListOrgChart;
    return;
})(jQuery);


