jQuery(function($){

    function EffaRepeater(element, options){
        var dataSet = $(this).data()|| {};
        this.options = $.extend({}, dataSet, options || {});
        this.$element = $(element);
        this.element = element;
        this.init();
    }

    EffaRepeater.prototype = {
        init: function(){
            var self = this, options = this.options, urlParams = (new URL(location)).searchParams;
            self.$formAddGroupTarget = $('form[data-add-group-event]');
            self.$popup = $('#popup-action-effa');
            self.initForm();
            self.eventRowItem();
            self.cloneButtonDelItem();
            self.standard();
            self.selectorBootstrap();
            self.editableBootstrap();
            self.initTooltipBootstrap();
            self.importAction();
            self.slideFormQuestion();
            self.initQuiz();
            self.exportReport();
            self.eventCollapGroup();
            if( 'type' in GETS && urlParams.has('type') && GETS.type == 'compare' ){
                self.initCompare();
            }
            if( 'type' in GETS && urlParams.has('type') && GETS.type == 'dictionary' ){
                self.initDictionary();
            }
            $('.floatthead').floatThead();
        },
        eventCollapGroup: function(){
            const self = this;
            $('.group-item-down').off('click.collapse').on('click.collapse', function(ev) {
                ev.preventDefault();
                let $icon = $(this).find('i'), data = $(this).data(), $table = $(this).closest('table');
                $('.row-tbody-content-item[data-group-item="' + data.groupItem + '"]', $table).slideToggle();
                $icon.toggleClass('fa-chevron-circle-down');

            });
        },
        initDictionary: function(){
            const self = this;

            $('.competency-for-department #department')
                .off('change.department')
                .on('change.department', function(ev){
                    ev.preventDefault();
                    if( this.value != 0 ){
                        $('#import-department').show();
                    }else{
                        $('#import-department').hide();
                    }
                    $('#popup-imports').find('input[name="department"]').val(this.value);
                    let $table2 = $('.competency-for-department #table-reponsive-2');
                    if (('xhr_abort_department' in self) && self.xhr_abort_department !== null) {
                        self.xhr_abort_department.abort("Cancel and Rerquest again");
                    }
                    let options = {
                        url: AJAX_URL,
                        type: 'get',
                        data: {action: 'effa_dictionary_for_department', department: this.value},
                        beforeSend: function () {
                            $table2.block(IMAGE_LOADING);
                        },
                        success: function(response, status, xhr){
                            if( 'html' in response ){
                                if( response.html == '' ){
                                    $('#table-reponsive-2').addClass('notboxshadow');
                                }else{
                                    $('#table-reponsive-2').removeClass('notboxshadow');
                                }
                                $table2.html(response.html);
                                $table2.find('table').floatThead();
                                self.slideFormQuestion();
                                self.editableBootstrap();
                                self.eventCollapGroup();
                            }
                            return;
                        },
                        error: function(xhr, status, errThrow){
                            var textError = '';
                            if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                                textError = xhr.responseJSON.error;
                            }else if( errThrow != '' ){
                                textError = xhr.getResponseHeader('xhr-message') || errThrow;
                                try{
                                    textError = JSON.parse(textError);
                                } catch(e){}
                            }
                            var $err = $('<label class="notification error"> ' + textError + ' </label>');
                            $table2.html( $err );
                            return;
                        },
                        complete: function(xhr, status){
                            self.xhr_abort_department = null;
                            $table2.unblock(IMAGE_LOADING);
                            return;
                        }
                    };
                    self.xhr_abort_department = $.ajax(options);
                });
        },
        cloneButtonDelItem: function(){
            var self = this;
            self.$btnDelItem = self.$popup.find('button[data-event="del-item"]');
            self.removeButtonDelItem();
        },
        removeButtonDelItem: function(){
            this.$btnDelItem.hide();
        },
        appendButtonDelItem: function(){
            var self = this;
            this.$btnDelItem.show();
        },
        submitFormAjax: function($form, validator, ev){
            var self = this;
            ev.preventDefault();
            var $response = $('.response-container', $form);
            self.xhr_about = {
                dataType: 'json',
                beforeSubmit: function(serialize, form, option) {
                    $response.find('.notification').remove();
                    return true;
                },
                beforeSend: function () {
                    $form.block(IMAGE_LOADING);
                },
                success: function(response, status, xhr){
                    if( $.isPlainObject(response) || 'success' in response ){
                        $form.closest('.modal[role="dialog"]').modal('hide');
                        window.location.reload();
                    }
                    return;
                },
                error: function(xhr, status, errThrow){
                    var textError = '';
                    if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                        textError = xhr.responseJSON.error;
                    }else if( errThrow != '' ){
                        textError = xhr.getResponseHeader('xhr-message') || errThrow;
                        try{
                            textError = JSON.parse(textError);
                        } catch(e){}
                    }
                    var $err = $('<label class="notification error"> ' + textError + ' </label>');
                    $response.find('.notification').remove();
                    $response.prepend( $err );
                    return;
                },
                complete: function(xhr, status){
                    self.xhr_about = null;
                    $form.unblock(IMAGE_LOADING);
                    return;
                }
            };
            $form.ajaxSubmit(self.xhr_about);
            return false;
        },
        formValidate: function(frm){
            var self = this;
            var $frm = $(frm);
            var $required = $([].slice.call(frm.elements)).filter('[required]');
            var validationRules = {
                // Specify validation rules
                //onsubmit: callajax ? false : true,
                focusCleanup: true,
                errorClass: 'error',
                validClass: 'valid',
                rules: {},
                // Specify validation error messages
                messages: {},
                errorElement: "em",
                errorPlacement: function ( error, $element ) {
                    // Add the `help-block` class to the error element
                    error.addClass( "help-block" );
                    var data = $element.data();
                    var errText = data.errRequired || 'Please input value for ' + $element.attr('placeholder');
                    error.text( errText );
                    if('$label' in $element[0]) {
                        error.insertAfter( $element[0].$label );
                    } else if ( $element.prop( "type" ) === "checkbox" ) {
                        error.insertAfter( $element.parent( "label" ) );
                    } else {
                        error.insertAfter( $element );
                    }
                    return false;
                },
                invalidHandler: function(ev) {
                    var validator = $frm.data('validator');
                    var elements = validator.invalidElements();
                    if( validator.errorList.length == 0 ) {
                        validator.errorList = [];
                        self.initFormValidate();
                        $frm.trigger('submit.validate');
                        return;
                    }
                    return;
                },
                submitHandler: function(form, ev) {
                    var $form = $(form);
                    var validator = this;
                    return self.submitFormAjax($form, validator, ev);
                }
            };
            $required.each(function(idx, elem) {
                validationRules.rules[elem.id] = 'required';
                try {
                    var $label = $frm.find('label[for="'+elem.id+'"]');
                    elem.$label = $label;
                    $label = $label.clone();
                    $label.find('span').remove();
                    var field = $label.text().trim();
                    var data = $(elem).data() || {};
                    validationRules.messages[elem.id] = {
                        required: data.errRequired || 'Please input value for '+ field
                    }
                }catch(e) {
                }
                return;
            });
            var validator = $frm.data('validator');
            if( validator ) {
                $frm
                    .off( ".validate" )
                    .removeData( "validator" )
                    .find( ".validate-equalTo-blur" )
                    .off( ".validate-equalTo" )
                    .removeClass( "validate-equalTo-blur" );
            }
            $frm.validate(validationRules);
        },
        initFormValidate: function(){
            var self = this;
            self.$formAddGroupTarget.each(function(index, form){
                self.formValidate( form );
            });

            return;
        },
        initForm: function(){
            var self = this;
            self.initFormValidate();
            return self;
        },
        initQuiz: function(){
          var self = this;
          $('.article-quiz .form-check-input', self.$element).off('change.answers').on('change.answers', function (ev){
              ev.preventDefault();
              let $question = $(this).closest('.question');
              $question.find('.question-title').addClass('been-answer');
          });
          function frmSubmit($form){
              let $response = $form.find('.response-container'), $elementLoading = $('body');
              $form.ajaxSubmit({
                  beforeSend: function(xhr, settings) {
                      $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                      $elementLoading.addClass('question-loading');
                      $elementLoading.block(IMAGE_LOADING);
                      return;
                  },
                  error: function(xhr, status, error) {
                      if( 'responseJSON' in xhr && 'error' in xhr.responseJSON ){
                          error = xhr.responseJSON.error || error;
                          $response.removeClass('hidden').addClass('error').html(error);
                      }
                      return;
                  },
                  success: function(response, status, xhr) {
                      if( xhr.status == 201 ) {
                          $response.removeClass('hidden').addClass('success').html("Lưu thành công");
                          location.reload();
                      }
                      return;
                  },
                  complete: function(xhr, status) {
                      $elementLoading.unblock(IMAGE_LOADING);
                      $elementLoading.removeClass('question-loading');
                      return;
                  }
              });
          };
          $('.frm-quiz #send-answers').off('click.send.answer').on('click.send.answer', function(ev){
              ev.preventDefault();
              let $form = $(this).closest("form");
              let $inputs_radio = $form.find('input[type="radio"]'), checked = true;
              $inputs_radio.each(function(){
                  var name = $(this).attr("name");
                  if($("input:radio[name="+name+"]:checked").length == 0){
                      checked = false;
                  }
              });
              if( !checked ){
                  let $dialogConfirm = $('#questionConfirm');
                  $dialogConfirm.off('hidden.bs.modal.confirm').on('hidden.bs.modal.confirm', function(ev){
                      $dialogConfirm.off('click.dialog.agree', '#questionConfirm .btn-yes');
                  });
                  $dialogConfirm.off('show.bs.modal.confirm').on('show.bs.modal.confirm', function(ev){
                      $dialogConfirm.off('click.dialog.agree', '.btn-yes').on('click.dialog.agree', '.btn-yes', function(event){
                          event.preventDefault();
                          frmSubmit($form);
                      });
                  });
                  $dialogConfirm.modal();
              }else{
                  frmSubmit($form);
              }
          });
        },
        slideFormQuestion: function(){
            var self = this;
            function hide_all($ele){
                $ele.slideUp();
                $('.show-hide-questions').removeClass('tr-show');
            }
            $('.frm-questions .btn-cancel', self.$element).off('click.close.questions').on('click.close.questions', function(ev){
                ev.preventDefault();
                let $tr = $(this).closest('tr'), $form = $(this).closest('form');
                $form.slideUp(function(){
                    $tr.removeClass('tr-show');
                });
            });
            $('.frm-questions .btn-save', self.$element).off('click.save.questions').on('click.save.questions', function(ev){
                ev.preventDefault();
                let $tr = $(this).closest('tr'), $form = $(this).closest('form');
                let $response = $('.response-container', $form);
                self.xhr_save_question = {
                    dataType: 'json',
                    beforeSubmit: function(serialize, form, option) {
                        $response.find('.notification').remove();
                        return true;
                    },
                    beforeSend: function () {
                        $form.block(IMAGE_LOADING);
                    },
                    success: function(response, status, xhr){
                        if( $.isPlainObject(response) || 'success' in response ){
                            window.location.reload();
                        }
                        return;
                    },
                    error: function(xhr, status, errThrow){
                        var textError = '';
                        if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                            textError = xhr.responseJSON.error;
                        }else if( errThrow != '' ){
                            textError = xhr.getResponseHeader('xhr-message') || errThrow;
                            try{
                                textError = JSON.parse(textError);
                            } catch(e){}
                        }
                        var $err = $('<label class="notification error"> ' + textError + ' </label>');
                        $response.find('.notification').remove();
                        $response.prepend( $err );
                        return;
                    },
                    complete: function(xhr, status){
                        self.xhr_save_question = null;
                        $form.unblock(IMAGE_LOADING);
                        return;
                    }
                };
                $form.ajaxSubmit(self.xhr_save_question);
            });
            $('.action-show-questions', self.$element).off('click.questions').on('click.questions', function(ev){
                let $this = $(this), data = $this.data();
                let $tr = $(data.target).closest('tr');
                if( $tr.hasClass('tr-show') ){
                    $(data.target).slideUp(function(){
                        $tr.removeClass('tr-show');
                    });
                }else{
                    hide_all($(data.target));
                    $tr.addClass('tr-show');
                    $(data.target).slideDown();
                }

            });
        },
        eventRowItem: function(){
            var self = this;
            function resetFormEffa($form){
                $form.clearForm();
                $form.find('input[name="post_parent"]').val("");
                $form.find('input[name="post_id"]').val("");
                $form.find('.code-post').html("");
                $form.find('[data-event="del-item"]').attr('data-id', "");
                $form.find('._effa_code').hide();
            }
            self.$popup.bind('show.bs.modal.row', function(ev) {
                var $btn = $(ev.relatedTarget), isAdd = false, data = $btn.data(),
                $form = $('form', $(this));
                resetFormEffa($form);
                var $response = $('.response-container', $form);
                $response.find('.notification').remove();
                var parent = 0;
                $('.effa-code', self.$popup).show();
                $form.find('._effa_code').show();
                if( $btn.data('event') == 'add-row-item' ) {
                    isAdd = true;
                    self.removeButtonDelItem();
                } else {
                    // Edit
                    var $tr = $btn.closest('tr');
                    data = $tr.data();
                    self.appendButtonDelItem();
                }
                if( 'parent' in data ){
                    parent = data.parent;
                }
                $('input[name="post_parent"]', self.$popup).val( parent );
                if( !isAdd ) {
                    if( ('xhr_abort' in self) && self.xhr_abort !== null ) {
                        self.xhr_abort.abort("Cancel and Rerquest again");
                    }
                    self.xhr_abort = $.ajax({
                        url: AJAX_URL,
                        data: {action: 'load_post_effa', id: data.id},
                        dataType: 'json',
                        type: 'get',
                        beforeSend: function () {
                            $response.find('.notification').remove();
                            self.$element.block(IMAGE_LOADING);
                        },
                        success: function(response, status, xhr){
                            if( $.isPlainObject(response) || 'data' in response ){
                                var resData = response.data;
                                Object.keys(resData).forEach( function(item){
                                    var key = item.replace(/(\[\])/g, '\$1' );
                                    var resValue = resData[item];
                                    if( item == 'post_id' ){
                                        self.$popup.find('[data-event="del-item"]').attr('data-id', resValue);
                                        self.$popup.find('[name="' + key + '"]').val(resValue);
                                    }else if( item == 'post_content' ){
                                        self.$popup.find('[name="' + key + '"]').val( resValue );
                                    }else{
                                        if( self.$popup.find('[name="' + key + '"]').length ) {
                                            self.$popup.find('[name="' + key + '"]').val(resValue);
                                        }
                                    }
                                    return;
                                });
                            }
                            return;
                        },
                        error: function(xhr, status, errThrow){
                            //window.location.reload();
                            var textError = '';
                            if( ('responseJSON' in xhr) && ( 'message' in xhr.responseJSON ) && (xhr.responseJSON.message != '') ){
                                textError = xhr.responseJSON.message;
                            }else if( errThrow != '' ){
                                textError = xhr.getResponseHeader('xhr-message') || errThrow;
                                try{
                                    textError = JSON.parse(textError);
                                } catch(e){}
                            }
                            var $err = $('<label class="notification error"> ' + textError + ' </label>');
                            $response.prepend( $err );
                            return;
                        },
                        complete: function(xhr, status){
                            self.xhr_abort = null;
                            self.$element.unblock(IMAGE_LOADING);
                            return;
                        }
                    });
                }
                return;
            });
            self.$popup.bind('hidden.bs.modal.row', function(ev){
                let $form = $('form', $(this));
                resetFormEffa($form);
            });

            // remove group
            self.$element.off('click.del_row_item_in_group', 'a[data-event="del-row-group"]').on('click.del_row_item_in_group', 'a[data-event="del-row-group"]', function(ev){
                ev.preventDefault();
                var data = $(this).data();
                self.bindEventDialog( data );
            });
            self.$popup.off('click.remove.row_item', '[data-event="del-item"]').on('click.remove.row_item', '[data-event="del-item"]', function(ev){
                ev.preventDefault();
                var data = $(this).data();
                self.bindEventDialog(data);
            });
            //change status
            function callAjaxChangeStatus($this, data){
                var $parentTD = $(this).closest('td .switch');
                var $parentTR = $(this).closest('tr');
                $.ajax({
                    url: AJAX_URL,
                    data: {action: 'change_status_post_effa', post_id: data.postId, chart_id: data.chartId, confirm: data.confirm},
                    dataType: 'json',
                    type: 'post',
                    beforeSend: function () {
                        $parentTR.block(IMAGE_LOADING);
                        $parentTD.find('.notification').remove();
                    },
                    success: function(response, status, xhr){
                        if( $.isPlainObject(response) || 'success' in response ){
                            //window.location.reload();
                            var $notification = $('<span class="notification success">' + response.success + '</span>');
                            $parentTD.append( $notification );
                            var notifiTimeout;
                            if( notifiTimeout !== undefined ){
                                clearTimeout( notifiTimeout );
                            }
                            notifiTimeout = setTimeout(function(){
                                $parentTD.find('.notification').remove();
                            }, 1000);
                        }
                        return;
                    },
                    error: function(xhr, status, errThrow){
                        //console.log( 'ERROR',xhr, status, errThrow );
                        var textError = '', checked = false;
                        if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                            textError = xhr.responseJSON.error;
                            checked = xhr.responseJSON.checked;
                        }else if( errThrow != '' ){
                            textError = xhr.getResponseHeader('xhr-message') || errThrow;
                            try{
                                textError = JSON.parse(textError);
                            } catch(e){}
                        }
                        var $err = $('<span class="notification error"> ' + textError + ' </span>');
                        $this.prop('checked', checked);
                        $parentTD.append( $err );
                        return;
                    },
                    complete: function(xhr, status){
                        $parentTR.unblock(IMAGE_LOADING);
                        return;
                    }
                });
            }
            self.$element.off('change.change.status', '.item-action-on-off .checkbox-status').on('change.change.status', '.item-action-on-off .checkbox-status', function(ev){
                ev.preventDefault();
                var $this = $(this);
                let checked = $this.prop('checked');
                var data = $(this).data();console.log(data.confirm);
                if(data.confirm !== '' && data.confirm !== undefined ){
                    let $dialogConfirm = $('#myDialogConfirm');
                    $dialogConfirm.off('hidden.bs.modal.confirm').on('hidden.bs.modal.confirm', function(ev){
                        $dialogConfirm.off('click.dialog.agree', '#myDialogConfirm .btn-yes');
                    });
                    $dialogConfirm.off('show.bs.modal.confirm').on('show.bs.modal.confirm', function(ev){
                        $dialogConfirm.off('click.dialog.agree', '.btn-yes').on('click.dialog.agree', '.btn-yes', function(event){
                            event.preventDefault();
                            callAjaxChangeStatus($this, data);
                            $dialogConfirm.modal('hide');
                        });
                        $dialogConfirm.off('click.dialog.no', '.btn-no').on('click.dialog.no', '.btn-no', function(event){
                            event.preventDefault();
                            $this.prop('checked', !checked);

                        });
                    });
                    $dialogConfirm.modal();
                }else{
                    callAjaxChangeStatus($this, data);
                }

                return this;
            });

        },
        standard: function(){
            var self = this;
            self.$element.off('change.standard', '.effa-standard-content .assign-effa .checkbox-status').on('change.standard', '.effa-standard-content .assign-effa .checkbox-status', function(ev){
                var $this = $(this);
                var data = $(this).data();
                var $parentTD = $(this).closest('td .switch');
                var $parentTR = $(this).closest('tr');
                $.ajax({
                    url: AJAX_URL,
                    data: data.params,
                    dataType: 'json',
                    type: 'post',
                    beforeSend: function () {
                        $parentTR.block(IMAGE_LOADING);
                        $parentTD.find('.notification').remove();
                    },
                    success: function(response, status, xhr){
                        if( $.isPlainObject(response) || 'success' in response ){
                            //window.location.reload();
                            var $notification = $('<span class="notification success">' + response.success + '</span>');
                            $parentTD.append( $notification );
                            var notifiTimeout;
                            if( notifiTimeout !== undefined ){
                                clearTimeout( notifiTimeout );
                            }
                            notifiTimeout = setTimeout(function(){
                                $parentTD.find('.notification').remove();
                            }, 1000);
                            $parentTR.find('.benchmark').html(response.benchmark);
                        }
                        return;
                    },
                    error: function(xhr, status, errThrow){
                        //console.log( 'ERROR',xhr, status, errThrow );
                        var textError = '', checked = false;
                        if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                            textError = xhr.responseJSON.error;
                            checked = xhr.responseJSON.checked;
                        }else if( errThrow != '' ){
                            textError = xhr.getResponseHeader('xhr-message') || errThrow;
                            try{
                                textError = JSON.parse(textError);
                            } catch(e){}
                        }
                        var $err = $('<span class="notification error"> ' + textError + ' </span>');
                        $this.prop('checked', checked);
                        $parentTD.append( $err );
                        return;
                    },
                    complete: function(xhr, status){
                        $parentTR.unblock(IMAGE_LOADING);
                        return;
                    }
                });
            });
        },
        bindEventDialog: function(data){
            var $dialogConfirm = $('#myDialogConfirm');
            if( $.isPlainObject(data) && 'title' in data ){
                $dialogConfirm.find('.modal-title').html(data.title);
            }else{
                $dialogConfirm.find('.modal-title').html($dialogConfirm.find('.modal-title').data('title'));
            }
            $dialogConfirm.off('hidden.bs.modal.confirm').on('hidden.bs.modal.confirm', function(ev){
                $dialogConfirm.off('click.dialog.agree', '#myDialogConfirm .btn-yes');
            });
            $dialogConfirm.off('show.bs.modal.confirm').on('show.bs.modal.confirm', function(ev){
                $dialogConfirm.find('.notification').remove();
                $dialogConfirm.off('click.dialog.agree', '.btn-yes').on('click.dialog.agree', '.btn-yes', function(event){
                    event.preventDefault();
                    if( !$.isPlainObject(data) && data === undefined ){
                        alert('Vui lòng nhấn nút xóa trước khi hiển thị form này.');
                    }else{
                        $.ajax({
                            url: AJAX_URL,
                            data: {action: data.action, _wpnonce: data.nonce, ID: data.id},
                            type: 'post',
                            dataType: 'json',
                            beforeSend: function () {
                                $dialogConfirm.find('.notification').remove();
                                $('body').block(IMAGE_LOADING);
                            },
                            success: function(response, status, xhr){
                                if( $.isPlainObject(response) || 'success' in response ){
                                    window.location.reload();
                                }
                                return;
                            },
                            error: function(xhr, status, errThrow){
                                var textError = '';
                                if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                                    textError = xhr.responseJSON.error;
                                }else if( errThrow != '' ){
                                    textError = xhr.getResponseHeader('xhr-message') || errThrow;
                                    try{
                                        textError = JSON.parse(textError);
                                    } catch(e){}
                                }
                                var $err = $('<label class="notification error"> ' + textError + ' </label>');
                                $dialogConfirm.find('.modal-body').prepend( $err );
                                return;
                            },
                            complete: function(xhr, status){
                                $('body').unblock(IMAGE_LOADING);
                                return;
                            }
                        });
                    }
                    return this;
                });
            });

            $dialogConfirm.modal();
        },
        initCompare: function(){
            var self = this;
            $('#implement-level-up' ,self.$element).off('click.submit.level.up').on('click.submit.level.up', function(ev){
                ev.preventDefault();
                let $form = $(this).closest('form');
                let $response = $('.response-container', $form);
                $form.ajaxSubmit({
                    dataType: 'json',
                    beforeSubmit: function(serialize, form, option) {
                        $response.find('.notification').remove();
                        return true;
                    },
                    beforeSend: function () {
                        $('body').block(IMAGE_LOADING);
                    },
                    success: function(response, status, xhr){
                        //if( $.isPlainObject(response) ){
                            window.location.reload();
                        //}
                        return;
                    },
                    error: function(xhr, status, errThrow){
                        var textError = '';
                        if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                            textError = xhr.responseJSON.error;
                        }else if( errThrow != '' ){
                            textError = xhr.getResponseHeader('xhr-message') || errThrow;
                            try{
                                textError = JSON.parse(textError);
                            } catch(e){}
                        }
                        var $err = $('<label class="notification error"> ' + textError + ' </label>');
                        $response.find('.notification').remove();
                        $response.prepend( $err );
                        return;
                    },
                    complete: function(xhr, status){
                        self.xhr_save_question = null;
                        $('body').unblock(IMAGE_LOADING);
                        return;
                    }
                });
            });
        },
        selectorBootstrap: function () {
            var $selectBootstrap = $('select[data-use-bootstrap]');
            $selectBootstrap.selectpicker();
            return this;
        },
        editableBootstrap: function () {
            var $editableBootstrap = $('[data-editable]');
            $.each($editableBootstrap, function(idx, ele){
                let data = $(ele).data();
                $(ele).editable('destroy');
                $(ele).editable({
                    type: data.type,
                    pk: data.pk,
                    url: data.url,
                    title: data.title,
                    params: data.params,
                    success: function(response, newValue) {
                        if( $.isPlainObject(response) && 'data_response' in response ){
                            let dataResponse = response.data_response;
                            $.each(dataResponse, function(key, idx){
                                $('#'+key).html(dataResponse[key]);
                            });
                        }
                    },
                    error: function(xhr){
                        let textError = '';
                        if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                            textError = xhr.responseJSON.error;
                        }
                        $('.editable-error-block.help-block').html(textError);
                    }
                });
            });
            return this;
        },
        initTooltipBootstrap: function(){
            $('[data-toggle="tooltip"]').tooltip();
        },
        importAction: function(){
            var self = this;
            self.$listFiles = $('.ul-upload-files');
            self.$fileItem = self.$listFiles.find('li').clone();
            self.$listFiles.find('li').remove();
            var $hasFiles = $('.modal-footer .has-files');
            function submitFormAjax($form){
                let $response = $form.find('.response-container');
                $form.ajaxSubmit({
                    beforeSend: function(xhr, settings) {
                        $response.addClass('hidden').removeClass('error').removeClass('success').html('');
                        $form.block(IMAGE_LOADING);
                        return;
                    },
                    error: function(xhr, status, error) {
                        if( ('responseJSON' in xhr) && ('code' in xhr.responseJSON) ) {
                            $response.removeClass('hidden').addClass('error').html(xhr.responseJSON.message);
                        } else {
                            error = xhr.getResponseHeader('xhr-message') || error;
                            try{
                                error = JSON.parse(error);
                            } catch(e){}
                            if( xhr.status >= 500 ){
                                error = "Vui lòng tải file mẫu ở trên";
                            }
                            $response.removeClass('hidden').addClass('error').html(error);
                        }
                        return;
                    },
                    success: function(response, status, xhr) {
                        if( xhr.status == 201 ) {
                            $response.removeClass('hidden').addClass('success').html("Lưu thành công");
                            location.reload();
                        }
                        return;
                    },
                    complete: function(xhr, status) {
                        $('#myDialogConfirm').unblock(IMAGE_LOADING);
                        $form.unblock(IMAGE_LOADING);
                        return;
                    }
                });
            }
            $('.popup-import-effa', self.$element).off('click.show.popup.import').on('click.show.popup.import', function(ev){
                ev.preventDefault();
                var data = $(this).data();
                var $popup = $(data.target);
                $popup.off('show.bs.modal').on('show.bs.modal', function(ev){
                    var $form = $('form', $(this));
                    $form.attr('action', data.action);
                    $form.find(".has-files").hide();
                    $form.resetForm();
                    var $response = $form.find('.response-container');
                    $form.off('submit.import.files').on('submit.import.files', function(){
                        let chooseOption = $('.left-option input[name="options"]:checked', $form).val();
                        if( chooseOption == 'removeall' ){
                            let $dialogConfirm = $('#myDialogConfirm');
                            $dialogConfirm.find('.modal-title').html('Bạn đang chọn chế độ "XOÁ DỮ LIỆU HIỆN TẠI".\n' +
                                'Bạn chấp nhận không?');
                            $dialogConfirm.off('hidden.bs.modal.confirm').on('hidden.bs.modal.confirm', function(ev){
                                $dialogConfirm.off('click.dialog.agree', '#myDialogConfirm .btn-yes');
                            });
                            $dialogConfirm.off('show.bs.modal.confirm').on('show.bs.modal.confirm', function(ev){
                                $dialogConfirm.find('.notification').remove();
                                $dialogConfirm.off('click.dialog.agree', '.btn-yes').on('click.dialog.agree', '.btn-yes', function(event){
                                    event.preventDefault();
                                    $dialogConfirm.block(IMAGE_LOADING);
                                    submitFormAjax($form);
                                    return this;
                                });
                            });
                            $dialogConfirm.modal();
                        }else{
                            submitFormAjax($form);
                        }
                        return false;
                    });
                });
            });
            $('#imports-effa', self.$element).off('change.import').on('change.import', function(ev){
                var $label = $(this).next();
                var $parent = $(this).parent();
                var $this = $(this);
                if( this.files && this.files.length > 0 ){
                    var data = $(this).data();
                    var files = this.files;
                    Object.keys(files).forEach(function (idx) {
                        var file = files[idx];
                        // 5M
                        if (file.size > 5242880) {
                            $parent.find('.response-container').text( data.errorSize );
                        }else{
                            var $eleFile = self.$fileItem;
                            $eleFile.find('.li-filename').text( file.name );
                            $('.btn-remove-file-import', $eleFile).off('click.remove.file.import').on('click.remove.file.import', function(ev){
                                ev.preventDefault();
                                var $li = $(this).closest('li');
                                $li.remove();
                                $this.val('');
                                $hasFiles.hide();
                            });
                            self.$listFiles.append( $eleFile );
                            $hasFiles.show();
                        }
                    });
                }else{

                }
            });

        },
        exportReport: function(){
            var self = this;
            $('.export-reports a', self.$element).off('click.export.reports').on('click.export.reports', function(ev){
                ev.preventDefault();
                var data = $(this).data();
                var options = {
                    url: AJAX_URL,
                    data: data.params,
                    dataType: 'json',
                    type: data.method || 'get',
                    beforeSend: function () {
                        $('body').block(IMAGE_LOADING);
                    },
                    success: function(response, status, xhr){
                        if( $.isPlainObject(response) || 'url_download' in response ){
                            window.open(response.url_download);
                        }
                        return;
                    },
                    error: function(xhr, status, errThrow){
                        var textError = '', checked = false;
                        if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                            textError = xhr.responseJSON.error;
                            checked = xhr.responseJSON.checked;
                        }else if( errThrow != '' ){
                            textError = xhr.getResponseHeader('xhr-message') || errThrow;
                            try{
                                textError = JSON.parse(textError);
                            } catch(e){}
                        }
                        //var $err = $('<span class="notification error"> ' + textError + ' </span>');
                        alert(textError);
                        return;
                    },
                    complete: function(xhr, status){
                        self.abort_export = null;
                        $('body').unblock(IMAGE_LOADING);
                        return;
                    }
                };
                if( ('abort_export' in self) && self.abort_export !== null ) {
                    self.abort_export.abort("Cancel and Request again");
                }
                self.abort_export = $.ajax(options);
            });
        },
    };

    $.fn.EffaRepeater = function( options ){
        this.each(function(){
            var effaRepeater = $(this).data('effaRepeater');
            if( !effaRepeater ){
                effaRepeater = new EffaRepeater( this, options );
                $(this).data('effaRepeater', effaRepeater);
            }
        });
        return this;
    }

    function EFFA(element, options){
        var dataSet = $(this).data()|| {};
        this.options = $.extend({}, dataSet, options || {});
        this.$parent_element = $(element);
        this.parent_element = element;
        this.init();
    }

    EFFA.prototype = {
        initRepeater: function(){
            this.$parent_element.EffaRepeater();
        },
        init: function(){
            var self =this;
            self.$menuLeft = $('.effa-menu-left.navbar-left');
            self.initRepeater();
            self.eventDropdown();
        },
        eventDropdown: function(){
            var self = this;
            self.$parent_element.off('click.dropdown', '.effa-menu-left.navbar-left a.event-dropdown i')
                .on('click.dropdown', '.effa-menu-left.navbar-left a.event-dropdown i', function(ev){
                    ev.preventDefault();
                    var $iconDropdown = $(this);
                    $iconDropdown.toggleClass('fa-caret-up');
                    $(this).closest('a').next('ul').slideToggle();
                });
        }
    };

    $.fn.Effa = function( options ){
        this.each(function(){
            var effa = $(this).data('Effa');
            if( !effa ){
                effa = new EFFA( this, options );
                $(this).data('Effa', effa);
            }
        });
        return this;
    }

    $(function(){
        $('.efficiency-assessment').Effa();
    });

    $(document).off('click', '.ok-submit').on('click', '.ok-submit', function(ev){
        ev.preventDefault();
        let parent = $(this).parent().parent();
        let prev = parent.prev();
        let params = prev.data('params');

        params.pk = prev.data('pk');

        var radio = $(this).parent().find("input:checked");

        params.value = radio.val();

        $.ajax({
            type: "POST",
            url: prev.data('url'),
            data: params,
            success: function(result){
                parent.html('');
               prev.html(params.value);
            }
        });
    });

    $(document).off('click', '.cancel-submit').on('click', '.cancel-submit', function(ev){
        ev.preventDefault();
        let parent = $(this).parent().parent();
        parent.html('');
    });

    $(document).off('click', '.regard-score').on('click', '.regard-score', function(ev){
        ev.preventDefault();
        let $regardScore = $(this);
        let current = parseInt($regardScore.html());

        let url = $regardScore.data('url');
        let params = $regardScore.data('params');

        let getURL = url.replace(/action=(.)*/g, 'action=assessment_levels');

        $.ajax({
            url: getURL + '&post_id=' + params.post_id,
            success: function(result){
                let html =  '<span class="popover customppover" style="width:400px; padding:5px; border-radius:5px">';
                html += '<span style="margin: 5px 0"><b>' + $regardScore.data('title') + '</b></span>';

                const object = result.data;

                for( let index in object) {
                    let score =  parseInt(index) + 1;

                    html += '<input type="radio" name="regardScore' + params.post_id +'" value="' + score + '"' + (current== score ? 'checked' : '')
                        + '> ' + score + ') ' + object[index] + '<br>';
                }

                html += '<a class="btn btn-primary btn-sm editable-submit ok-submit"><i class="glyphicon glyphicon-ok"></i></a>';
                html += '<a class="btn btn-default btn-sm editable-cancel cancel-submit"><i class="glyphicon glyphicon-remove" style="background-color: #0a6aa1"></i></a>';
                html += '</span>';

                $regardScore.next().html(html);
            }
        });
    });

    $(document).off('click', '.columnCompetencyEvent').on('click', '.columnCompetencyEvent', function(ev){
        ev.preventDefault();
        let parent = $(this).parent();
        parent.find('.regard-score').click();
    });

});