(function($){
    $(function() {

        var datascource = {
            "id":
            "name": "Tap Doan Brainmark",
            "title": "", "className": "level-root"
        };

        var __idx = 0, getId = function() {
            __idx ++;
            return __idx;//(new Date().getTime()) * 1000 + Math.floor(Math.random() * 1001);
        };

        function addNodeContent(datascource) {
            if( !('nodeContent' in datascource) ) {
                datascource.content = '<div class="description"></div><div class="controls"><a href="javascript:;" class="node-add"><i class=""></i>Add</a>'+
                    '<a href="javascript:;" class="node-edit"><i class=""></i>Edit</a>' +
                    '<a href="javascript:;" class="node-remove"><i class=""></i>Remove</a></div>';
            }
            if( ('children' in datascource) ) {
                datascource.children.forEach(function(item, idx) {
                    datascource.children[idx] = addNodeContent(item);
                });
            }
            return datascource;
        }
        var datascource2 = addNodeContent(datascource1);
        console.debug(datascource2);
        $('#chart-container').orgchart({
            'data': datascource2,
            'nodeTitle': 'name',
            'nodeContent': 'content',
            'verticalDepth': 7,
            'depth': 7,
            // 'parentId': '',
            /* 'nodeTemplate': function(nodeData, opts){
                var  that = this, html = '<div class="title">' + nodeData[opts.nodeTitle] + '</div>';
                if( typeof opts.nodeContent !== 'undefined' ) {
                    html += '<div class="content">' + (nodeData[opts.nodeContent] || '') + '</div>';
                }
                return html;
            }, */
            'createNode': function($nodeDiv, nodeData, level, opts, dtd) {
                var that = this, $title = $nodeDiv.children('.title'), $content = $nodeDiv.children('.content'),
                $nodeExtra = $content.length ? $content : $title;
                $nodeDiv.data('orgChart', that);
                $nodeDiv.data('nodeData', nodeData);
                /*
                var $controls = $('<div class="controls"><a href="javascript:;" class="node-add"><i class=""></i>Add</a>'+
                    '<a href="javascript:;" class="node-edit"><i class=""></i>Edit</a>' +
                    '<a href="javascript:;" class="node-remove"><i class=""></i>Remove</a></div>');
                $nodeExtra.after($controls);
                $nodeDiv.data('$controls', $controls);
                */

                $nodeDiv.off('click', '.controls a.node-add').on('click', '.controls a.node-add', function(ev) {
                    return that.options.onAddNode.call(that, ev, $nodeDiv, nodeData, level, opts);
                });

                $nodeDiv.off('click', '.controls a.node-edit').on('click', '.controls a.node-edit', function(ev) {
                    return that.options.onEditNode.call(that, ev, $nodeDiv, nodeData, level, opts);
                });

                $nodeDiv.off('click', '.controls a.node-remove').on('click', '.controls a.node-remove', function(ev) {
                    return that.options.onRemoveNode.call(that, ev, $nodeDiv, nodeData, level, opts);
                });
                /*
                var nodeVals = [];
                $('#new-nodelist').find('.new-node').each(function(index, item) {
                    var validVal = item.value.trim();
                    if (validVal.length) {
                        nodeVals.push(validVal);
                    }
                });
                var $node = $('#selected-node').data('node');
                if (!nodeVals.length) {
                    alert('Please input value for new node');
                    return;
                }
                var nodeType = $('input[name="node-type"]:checked');
                if (!nodeType.length) {
                    alert('Please select a node type');
                    return;
                }
                if (nodeType.val() !== 'parent' && !$('.orgchart').length) {
                    alert('Please creat the root node firstly when you want to build up the orgchart from the scratch');
                    return;
                }
                if (nodeType.val() !== 'parent' && !$node) {
                    alert('Please select one node in orgchart');
                    return;
                }
                if (nodeType.val() === 'parent') {
                    if ( !that.$chartContainer.children('.orgchart').length) {// if the original chart has been deleted
                        that = $chartContainer.orgchart({
                            'data' : { 'name': nodeVals[0] },
                            'exportButton': true,
                            'exportFilename': 'SportsChart',
                            'parentNodeSymbol': 'fa-th-large',
                            'createNode': function($node, data) {
                                $node[0].id = getId();
                            }
                        });
                        that.$chart.addClass('view-state');
                    } else {
                        that.addParent(that.$chartContainer.find('.node:first'), { 'name': nodeVals[0], 'id': getId() });
                    }
                } else if (nodeType.val() === 'siblings') {
                    that.addSiblings($node,
                        { 'siblings': nodeVals.map(function(item) { return { 'name': item, 'relationship': '110', 'id': getId() }; })
                        });
                } else {
                    var hasChild = $node.parent().attr('colspan') > 0 ? true : false;
                    if (!hasChild) {
                        var rel = nodeVals.length > 1 ? '110' : '100';
                        that.addChildren($node, {
                            'children': nodeVals.map(function(item) {
                                return { 'name': item, 'relationship': rel, 'id': getId() };
                            })
                        }, $.extend({}, that.$chartContainer.find('.orgchart').data('options'), { depth: 0 }));
                    } else {
                        that.addSiblings($node.closest('tr').siblings('.nodes').find('.node:first'),
                            { 'siblings': nodeVals.map(function(item) { return { 'name': item, 'relationship': '110', 'id': getId() }; })
                            });
                    }
                }
                */
                return;
            },
            'onCreateNode': function(ev, $nodeDiv, nodeData, level, opts) {
                return;
            },
            onAddNode: function(ev, $nodeDiv, nodeData, level, opts) {
                var that = this, hasChild = $nodeDiv.parent().attr('colspan') > 0 ? true : false,
                    id = getId(), nodeVals = ['New Node ' + id]; //, 'New Node ' + (id+1)]; getId();
                var rel = nodeVals.length > 1 ? '110' : '100';
                var options = that.options, options1 = that.$chartContainer.find('.orgchart').data('options');
                if( !('children' in nodeData) ) {
                    nodeData.children = [];
                }
                nodeVals = nodeVals.map(function(item) {
                    var result = { 'name': item, "title": "", "className": "level-" + (level + 1)  , 'relationship': rel, 'id': id };
                    nodeData.children.push(result);
                    return result;
                });
                if( !hasChild ) {
                    that.addChildren(
                        $nodeDiv,
                        {
                            'children': nodeVals
                        },
                        $.extend({}, options1, { depth: 0 })
                    );
                } else {
                    // var $node = $nodeDiv.closest('tr').siblings('.nodes').find('.node:first');
                    var $node = $nodeDiv.closest('tr').siblings('.nodes').find('.node:last');
                    that.addSiblings(
                        $node,
                        {
                            'siblings': nodeVals
                        }
                    );
                }
                return;
            },
            onEditNode: function(ev, $nodeDiv, nodeData, level, opts) {
                return;
            },
            onRemoveNode: function(ev, $nodeDiv, nodeData, level, opts) {
                this.removeNodes($nodeDiv);
                return;
            }
        });

    });
})(jQuery);
