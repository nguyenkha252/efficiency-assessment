jQuery(function($){
    $.fn.dashBoard = function (options) {
        this.each(function(){
            var dashboard = $(this).data('partDashBoard');
            if( !dashboard ){
                dashboard = new DashBoard(this, options);
                $(this).data('partDashBoard', dashboard);
            }
        });
        return this;
    }
    function DashBoard(element, options){
        var dataSet = $(element).data() || {};
        this.options = $.extend({

        }, dataSet, options || {}, {});
        this.$element = $(element);
        this.$form = $('form[data-chart-report]', $(element));
        this.init();
        return this;
    }
    DashBoard.defaultOptions = {

    }
    DashBoard.prototype = {
        initElement: function(){
            var self = this;
            self.$contentElement = $('.dashboard-content');
        },
        destroy: function() {
            var self = this;
            Object.keys(self.charts).forEach(function(chartName) {
                self.charts[chartName].destroy();
            });
            self.$form.unbind('submit.filter');
            return self;
        },
        initChart: function() {
            var self = this;
            //self.$forms.each( function(idx, form) {
                $('canvas.data-report', self.$element ).each(function () {
                    var $canvas = $(this);
                    $canvas.chartReport({selfParent: self, $elementParent: self.$element});
                    self.charts[$canvas.attr('id')] = $canvas.data('chartReport');
                });
            //});
            return this;
        },
        initTabs: function(){
            if( location.hash !== '' ){
                var currentTab = location.hash;
                $(currentTab).tab('show');
            }
        },
        initForms: function(){
            var self = this;
            $('form[data-approved]', self.$element).each(function(){
                var $form = $(this);
                self.ajaxSubmitForm($form);
            });
            $('form[data-update-result]', self.$element).each(function(){
                var $form = $(this);
                self.ajaxSubmitForm($form);
            });
        },
        ajaxSubmitForm: function($form){
            var self = this;
            var options = {
                dataType: 'json',
                beforeSubmit: function(serialize, form, option) {
                    $form.find('.notification').remove();
                    return true;
                },
                beforeSend: function () {
                    $form.block(IMAGE_LOADING);
                },
                success: function(response, status, xhr){
                    //console.log( 'SUCCESS',response, status, xhr );
                    if( (xhr.status == 201) && $.isPlainObject(response) && ( 'message' in response ) ){
                        var message = response.message;
                        window.location.reload();
                    }
                    return;
                },
                error: function(xhr, status, errThrow){
                    //console.log( 'ERROR',xhr, status, errThrow );
                    var textError = '';
                    if( ('responseJSON' in xhr) && ( 'message' in xhr.responseJSON ) && (xhr.responseJSON.message != '') ){
                        textError = xhr.responseJSON.message;
                    } else if( errThrow != '' ){
                        textError = xhr.getResponseHeader('xhr-message') || errThrow;
                        textError = JSON.parse(textError);
                    }
                    var $err = $('<label class="notification error"> ' + textError + ' </label>');
                    $form.find('.notification').remove();
                    $form.prepend( $err );
                    return;
                },
                complete: function(xhr, status){
                    //console.log( 'Complete', xhr, status  );
                    self.xhr_kpi = null;
                    $form.unblock(IMAGE_LOADING);
                    return;
                }
            };
            //$form.submit(options);
            $form.off('submit.change.approved').on('submit.change.approved', function(ev){
                ev.preventDefault();
                $(this).ajaxSubmit( options );
            });
        },
        eventCheckboxAll: function(){
            var self = this;
            function setChecked(input){
                var dataCheckall = $(input).data();
                if( dataCheckall != undefined ){
                    var $form = $('form#form-' + dataCheckall.checkall);
                    var $inputsItem = $form.find("input[data-approved-item='"+ dataCheckall.checkall +"']");
                    var ichecked = 0;
                    $.each( $inputsItem, function(index, item2){
                        var checked = $(item2).prop('checked');
                        if( checked ){
                            ichecked ++;
                        }
                    });
                    var isCheckAll;
                    if( ichecked == $inputsItem.length ){
                        isCheckAll = true;
                    }else{
                        isCheckAll = false;
                    }
                    $(input).attr('checked', isCheckAll);
                }
            }
            var $inputCheckall = $('input[data-checkall]');
            $.each($inputCheckall, function(idx, item){
                setChecked(item);
            });
            $('input[data-checkall]').change(function(ev) {
                var data = $(this).data();
                var $form = $(this).closest("#room-" + data.checkall).find('form#form-'+data.checkall);
                var $inputs = $form.find('input[data-approved-item="'+ data.checkall +'"]');
                var checked = $(this).prop('checked');
                $inputs.attr('checked', checked);
            });
            $(self.$element).off('change.change.approved', 'input[data-approved-item]').on('change.change.approved', 'input[data-approved-item]', function(ev){
                var data = $(this).data();
                var inputAll = "input[data-checkall='"+ data.approvedItem +"']";
                setChecked(inputAll);
            });
        },
        paging: function(){
            var self = this;
            $(document).off('click.paging', '.paginate a').on('click.paging', '.paginate a', function(ev){
                ev.preventDefault();
                var data = $(this).data();
                var $contentPaging = $(this).closest('.content-paging');
                var $parentContent = $contentPaging.parent();
                var $prevContent = $parentContent.prev();
                var options = {
                    url: this.href,
                    dataType: 'json',
                    type: 'get',
                    beforeSend: function () {
                        $contentPaging.find('.error').remove();
                        $contentPaging.block(IMAGE_LOADING);
                    },
                    success: function(response, status, xhr){
                        console.log( 'SUCCESS',response, status, xhr );
                        if( $.isPlainObject(response) || 'html' in response ){
                            $contentPaging.html( response.html );
                        }
                        return;
                    },
                    error: function(xhr, status, errThrow){
                        console.log( 'ERROR',xhr, status, errThrow );
                        //window.location.reload();
                        var textError = '';
                        if( ('responseJSON' in xhr) && ( 'error' in xhr.responseJSON ) && (xhr.responseJSON.error != '') ){
                            textError = xhr.responseJSON.error;
                        }else if( errThrow != '' ){
                            textError = xhr.getResponseHeader('xhr-message') || errThrow;
                        }
                        var $err = $('<label class="notification error"> ' + textError + ' </label>');
                        $contentPaging.find('.notification').remove();
                        $contentPaging.prepend( $err );
                        return;
                    },
                    complete: function(xhr, status){
                        //console.log( 'Complete', xhr, status  );
                        self.xhr_kpi = null;
                        $('.content-paging').unblock(IMAGE_LOADING);
                        return;
                    }
                };
                $.ajax(options);
            });
        },
        printPdf: function() {
            var self = this, $print = $('button[name="btn-print"]');
            $print.unbind('click.print-pdf').bind('click.print-pdf', function(ev) {
                location.href = '';
                return;
            });
            return self;
        },
        init: function(){
            var self = this;
            self.charts = {};
            //self.$forms = $( 'form[data-chart-report]', self.$element );
            self.initChart();
            self.initElement();
            self.initTabs();
            self.eventCheckboxAll();
            self.initForms();
            self.paging();
        }
    };

    $(function(){
        $('.apply-kpi-system').dashBoard();
    });
});