jQuery(function($){
    $.fn.kpiBank = function(options){
        this.each(function(){
          var kpiBank = $(this).data("kpiBank");
          if( !kpiBank ){
              var kpiBank = new KPIBank(this, options);
              $(this).data("kpiBank", kpiBank);
          }
        });
        return this;
    }
    function KPIBank ( element, options ){
        var dataSet = $(element).data() || {};
        this.options = $.extend({

        }, dataSet, options || {});
        this.$element = $(element);
        this.init();
        return this;
    };
    KPIBank.prototype = {
        repeater: function(){
            var self = this;
            $('.brainmark-repeater').brainmarkRepeater();
            return self;
        },
        init: function(){
            var self = this;
            self.repeater();
            return self;
        }
    };
    $('.brainmark-repeater').kpiBank();
});