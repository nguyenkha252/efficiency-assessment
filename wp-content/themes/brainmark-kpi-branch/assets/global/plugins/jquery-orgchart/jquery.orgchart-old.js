(function($) {
    $.fn.orgChart = function(options) {
        var opts = $.extend({}, $.fn.orgChart.defaults, options);
        return new OrgChart($(this), opts);        
    }

    $.fn.orgChart.defaults = {
        data: [{id:1, name:'Root', parent: 0}],
        showControls: false,
        allowEdit: false,
        onDraw: null,
        onFormatNode: null,
        onAddNode: null,
        onDeleteNode: null,
        onClickNode: null,
        newNodeText: 'Add Child'
    };

    function OrgChart($container, opts){
        var data = opts.data;
        var nodes = {};
        var rootNodes = [];
        this.opts = opts;
        this.$container = $container;
        var self = this;

        this.draw = function(){
            $container.empty().append(rootNodes[0].render(opts));
            $container.find('.node').click(function(e){
                if(self.opts.onClickNode !== null) {
                    var thisId = $(this).attr('node-id');
                    // self.opts.onClickNode(nodes[$(this).attr('node-id')]);
                    self.opts.onClickNode(this, self, thisId, nodes[thisId], e);
                }
            });

            if(opts.allowEdit){
                $container.find('.node h2').click(function(e){
                    var thisId = $(this).closest('.node[node-id]').attr('node-id');
                    self.startEdit(thisId);
                    e.stopPropagation();
                });
            }

            // add "add button" listener
            $container.find('.org-add-button').click(function(e){
                var thisId = $(this).closest('.node[node-id]').attr('node-id');

                if(self.opts.onAddNode !== null){
                    self.opts.onAddNode.call(this, self, thisId, nodes[thisId], e);
                }
                else{
                    self.newNode(thisId);
                }
                e.stopPropagation();
            });

            $container.find('.org-del-button').click(function(e){
                var thisId = $(this).closest('.node[node-id]').attr('node-id');

                if(self.opts.onDeleteNode !== null){
                    self.opts.onDeleteNode.call(this, self, thisId, nodes[thisId], e);
                }
                else{
                    self.deleteNode(thisId);
                }
                e.stopPropagation();
            });
            if( typeof(opts.onDraw) == 'function' ) {
                opts.onDraw.call(self, self, $container, nodes, rootNodes);
            }
            return self;
        }

        this.startEdit = function(id){
            var inputElement = $('<input class="org-input" type="text" value="'+nodes[id].data.name+'"/>');
            $container.find('div[node-id='+id+'] h2').replaceWith(inputElement);
            var commitChange = function(){
                var h2Element = $('<h2 class="name">'+nodes[id].data.name+'</h2>');
                if(opts.allowEdit){
                    h2Element.click(function(){
                        self.startEdit(id);
                    })
                }
                inputElement.replaceWith(h2Element);
            }  
            inputElement.focus();
            inputElement.keyup(function(event){
                if(event.which == 13){
                    commitChange();
                }
                else{
                    nodes[id].data.name = inputElement.val();
                }
            });
            inputElement.blur(function(event){
                commitChange();
            })
        }

        this.nextId = function() {
            var nextId = Object.keys(nodes).length;
            while(nextId in nodes){
                nextId++;
            }
            return nextId;
        }

        this.newNode = function(parentId){
            var nextId = this.nextId();

            self.addNode({id: nextId, name: '', parent: parentId});
        }

        this.addNode = function(data){
            var newNode = new Node(data);
            nodes[data.id] = newNode;
            nodes[data.parent].addChild(newNode);

            self.draw();
            self.startEdit(data.id);
        }

        this.deleteNode = function(id){
            for(var i=0;i<nodes[id].children.length;i++){
                self.deleteNode(nodes[id].children[i].data.id);
            }
            nodes[nodes[id].data.parent].removeChild(id);
            delete nodes[id];
            self.draw();
        }

        this.getData = function(){
            var outData = [];
            for(var i in nodes){
                outData.push(nodes[i].data);
            }
            return outData;
        }

        // constructor
        for(var i in data){
            var node = new Node(data[i]);
            nodes[data[i].id] = node;
        }

        // generate parent child tree
        for(var i in nodes){
            if(nodes[i].data.parent == 0){
                rootNodes.push(nodes[i]);
            }
            else{
                nodes[nodes[i].data.parent].addChild(nodes[i]);
            }
        }

        // draw org chart
        $container.addClass('orgChart');
        self.draw();
    }

    function Node(data){
        this.data = data;
        this.children = [];
        var self = this;

        this.addChild = function(childNode){
            this.children.push(childNode);
        }

        this.removeChild = function(id){
            for(var i=0;i<self.children.length;i++){
                if(self.children[i].data.id == id){
                    self.children.splice(i,1);
                    return;
                }
            }
        }

        this.render = function(opts){
            var childLength = self.children.length,
                mainTable;

            mainTable = "<table data-node-table='"+this.data.id+"' cellpadding='0' cellspacing='0' border='0'>";
            var nodeColspan = childLength>0?2*childLength:2;
            mainTable += "<tr><td colspan='"+nodeColspan+"'>"+self.formatNode(opts)+"</td></tr>";

            if(childLength > 0) {
                var downLineTable = "<table cellpadding='0' cellspacing='0' border='0'><tr class='lines x'><td class='line left half'></td><td class='line right half'></td></table>";
                mainTable += "<tr class='lines'><td colspan='"+childLength*2+"'>"+downLineTable+'</td></tr>';

                var linesCols = '';
                for(var i=0;i<childLength;i++){
                    if(childLength==1){
                        linesCols += "<td class='line left half'></td>";    // keep vertical lines aligned if there's only 1 child
                    }
                    else if(i==0){
                        linesCols += "<td class='line left'></td>";     // the first cell doesn't have a line in the top
                    }
                    else{
                        linesCols += "<td class='line left top'></td>";
                    }

                    if(childLength==1){
                        linesCols += "<td class='line right half'></td>";
                    }
                    else if(i==childLength-1){
                        linesCols += "<td class='line right'></td>";
                    }
                    else{
                        linesCols += "<td class='line right top'></td>";
                    }
                }
                mainTable += "<tr class='lines v'>"+linesCols+"</tr>";

                mainTable += "<tr data-node-children='"+this.data.id+"'>";
                /* for(var i in self.children){
                    mainTable += "<td colspan='2'>"+self.children[i].render(opts)+"</td>";
                } */
                mainTable += "</tr>";
            }
            mainTable += '</table>';

            var $mainTable = $(mainTable);
            $mainTable.data('node', this);
            $mainTable[0].node = this;
            if( childLength > 0 ) {
                var $td, $tr = $mainTable.find('tr[data-node-children="'+this.data.id+'"]');

                for(var i in self.children){
                    $td = $("<td colspan='2'></td>");
                    $tr.append($td);
                    $td.append( self.children[i].render(opts) );
                }
            }
            return $mainTable;
        }

        this.formatNode = function(opts){
            var nameString = '',
                descString = '',
                buttonsHtml = '';
            if(typeof data.name !== 'undefined'){
                nameString = '<h2 class="name">'+self.data.name+'</h2>';
            }
            if(typeof data.description !== 'undefined'){
                descString = '<div class="description">'+self.data.description+'</div>';
            }
            if(opts.showControls){
                buttonsHtml = "<div class='org-add-button'>"+opts.newNodeText+"</div>";
                if( this.data.parent > 0 ) {
                    buttonsHtml += "<div class='org-del-button'></div>";
                }
            }
            if( typeof(opts.onFormatNode) == 'function' ) {
                return "<div class='node' node-id='"+this.data.id+"'>" + opts.onFormatNode(this, nameString, descString, buttonsHtml, opts) + "</div>";
            }
            return "<div class='node' node-id='"+this.data.id+"'>"+nameString+descString+buttonsHtml+"</div>";
        }
    }

})(jQuery);

