jQuery(function($){
    $.widget( "custom.brainmarkRepeater", {
        options: {
            // Functions Callback
            init: null,
            removeRow: null,
            addRow: null,
            appendRow: null,
            cloneRow: null
        },
        _create: function() {
            if( 'inited' in this ) {
                return;
            }
            this.idx = 0;
            this.init();
        },
        _getCreateOptions: function() {
            return this.element.data();
        },
        init: function() {
            var self = this, options = this.options;
            this.inited = true;
            self.$list = self.element.find(' > .brainmark-table tbody');
            self.$cloneable = self.$list.children('tr[data-id="brainmarkcloneindex"]');
            self.$cloneable.remove();
            self.$cloneable.removeClass('brainmark-clone');
            self.$cloneable.removeAttr('data-id');
            self.updateIdx(self.$list);

            self.$list.on('click.repeater.item', '.brainmark-row .brainmark-row-handle > a[data-event]', function(ev){
                ev.preventDefault();
                var $anchor = $(this), $tr = $anchor.closest('.brainmark-row');
                var action = ($anchor.data('event') || '').replace(/\-row/, 'Row');
                self[action]($tr);
                return false;
            });
            self.element.on('click.addRow', '.brainmark-actions > a[data-event="add-row"]', function(ev){
                ev.preventDefault();
                self.appendRow();
                return false;
            });
            if( 'init' in options && typeof(options.init) == 'function' ) {
                options.init.call(self, self.$list);
            }
            return self;
        },
        updateIdx: function($tr) {
            var self = this;
            var $children = self.$list.children('tr'), maxIdx = $children.length;
            $children.each(function(idx) {
                $(this).attr('data-idx', idx);
                maxIdx = idx+1;
            });
            this.idx = maxIdx;
            if( 'updateIdx' in self.options && typeof(self.options.updateIdx) == 'function' ) {
                self.options.updateIdx.call(self, $tr);
            }
            return this;
        },
        removeRow: function($row) {
            var self = this;
            $row.remove();
            if( 'removeRow' in self.options && typeof(self.options.removeRow) == 'function' ) {
                self.options.removeRow.call(self, $tr);
            }
            self.updateIdx($tr);
            return this;
        },
        addRow: function($row) {
            var self = this, $tr = self.cloneRow();
            $row.after($tr);
            if( 'addRow' in self.options && typeof(self.options.addRow) == 'function' ) {
                self.options.addRow.call(self, $row, $tr);
            }
            self.updateIdx($tr);
            return this;
        },
        appendRow: function() {
            var self = this, $tr = self.cloneRow();
            self.$list.append($tr);
            if( 'appendRow' in self.options && typeof(self.options.appendRow) == 'function' ) {
                self.options.appendRow.call(self, $tr);
            }
            self.updateIdx($tr);
            return this;
        },
        cloneRow: function() {
            var self = this, $tr = this.$cloneable.clone();
            var $names = $tr.find('[name*="\[IDX\]"], [data-name*="[IDX]"], [id*="IDX"], [for*="IDX"]');
            $names.each(function(idx, item){
                var $this = $(this), name = $this.attr('name') || '',
                    dataName = $this.attr('data-name') || '',
                    _for = $this.attr('for') || '',
                    _id = $this.attr('id') || '';
                name = name.replace(/\[IDX\]/, '['+self.idx+']');
                dataName = dataName.replace(/\[IDX\]/, '['+self.idx+']');
                _for = _for.replace(/IDX/, self.idx);
                _id = _id.replace(/IDX/, self.idx);
                name ? $this.attr('name', name) : null;
                dataName ? $this.attr('data-name', dataName) : null;
                _for ? $this.attr('for', _for) : null;
                _id ? $this.attr('id', _id) : null;
                return;
            });
            $tr.attr('data-idx', this.idx);
            if( 'cloneRow' in self.options && typeof(self.options.cloneRow) == 'function' ) {
                self.options.cloneRow.call(self, $tr);
            }
            this.idx ++;
            return $tr;
        }
    });
    /*$.widget( $.custom.brainmarkRepeater, {
        cloneRow: function() {
            var self = this, $tr = $.custom.brainmarkRepeater.prototype.cloneRow.call(this);
            //$tr.find('[data-media-upload]').brainmarkMedia();
            return $tr;
        }
    });*/
});