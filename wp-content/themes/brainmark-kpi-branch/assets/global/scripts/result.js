jQuery(function($){
    var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };

    window.chartColors = {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'rgb(75, 192, 192)',
        blue: 'rgb(54, 162, 235)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(201, 203, 207)'
    };

    var color = Chart.helpers.color;
    var config = {
        type: 'radar',
        data: {
            labels: labels,
            datasets: [
                {
                    label: 'Điểm chuẩn',
                    backgroundColor: color(window.chartColors.red).alpha(0.2).rgbString(),
                    borderColor: window.chartColors.red,
                    pointBackgroundColor: window.chartColors.red,
                    data: standards
                },
                {
                    label: 'Tự đánh giá',
                    backgroundColor: color(window.chartColors.blue).alpha(0.2).rgbString(),
                    borderColor: window.chartColors.blue,
                    pointBackgroundColor: window.chartColors.blue,
                    data: scores
                },
                {
                    label: 'Quản lý đánh giá',
                    backgroundColor: color(window.chartColors.yellow).alpha(0.2).rgbString(),
                    borderColor: window.chartColors.yellow,
                    pointBackgroundColor: window.chartColors.yellow,
                    data: scores_of_manager
                },
                {
                    label: 'Hội đồng đánh giá',
                    backgroundColor: color(window.chartColors.purple).alpha(0.2).rgbString(),
                    borderColor: window.chartColors.purple,
                    pointBackgroundColor: window.chartColors.purple,
                    data: scores_of_counsil
                },
            ]
        },
        options: {
            legend: {
                position: 'bottom',
            },
            title: {
                display: false,
                text: ''
            },
            scale: {
                ticks: {
                    beginAtZero: true
                }
            }
        }
    };

    window.onload = function() {
        window.myRadar = new Chart(document.getElementById('canvas'), config);
    };
});