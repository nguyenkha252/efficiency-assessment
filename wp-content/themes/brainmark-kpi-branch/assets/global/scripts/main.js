
jQuery( document ).ready(function( $ ) {
    $(function(){
        $('table.user-list > tbody > tr[data-edit-url]').off('click.edituser', 'td:not(.id-col)').on('click.edituser', 'td:not(.id-col)', function(ev) {
            ev.preventDefault();
            var $tr = $(this).closest('tr'), url = $tr.data('edit-url');
            console.debug(url);
            location.href = url;
            return;
        });
        return;
    });

    // loaded.bs.select.user refreshed.bs.select.user
    $(document).on('rendered.bs.select', '.selectpicker.orgchart-dropdown', function(ev) {
        var $this = $(this), selectpicker = $this.data('selectpicker');
        if( !selectpicker ) return;
        var $list = selectpicker.findLis(), liObj = selectpicker.liObj,
            $selectOptions = $this.find('option'), startLevel = 0;

        $selectOptions.each(function (index) {
            var $op = $(this), $lis = $list.eq(liObj[index]),
                info = $op.data('info');
            if (info && (startLevel == 0) ) {
                startLevel = info.level;
                return false;
            }
        });
        $selectOptions.each(function (index) {
            var $op = $(this), $lis = $list.eq(liObj[index]),
                info = $op.data('info');
            if( info ) {
                $lis.attr('data-level', info.level - startLevel);
                $lis.attr('data-info', $op.attr('data-info'));
            }
            return;
        });
        return;
    });

    var self = window, call_ajax = function(options) {
        options.beforeSend = options.beforeSend || $.noop;
        options.error = options.error || $.noop;
        options.success = options.success || $.noop;
        options.complete = options.complete || $.noop;
        var params = $.extend({
            url: AJAX_URL,
            type: 'post',
            dataType: 'json',
            beforeSend: function(xhr, settings) {
                try{
                    if( '$target' in options ) {
                        $(options.$target).addClass('loading');
                    }
                    options.beforeSend.apply(this, arguments);
                }catch(e){
                    console.error(e);
                }
                return;
            },
            error: function(xhr, status, error) {
                try{
                    options.error.apply(this, arguments);
                }catch(e){
                    console.error(e);
                }
                return;
            },
            success: function(response, status, xhr) {
                try{
                    if( ('$html' in options) && ('html' in  response) ) {
                        $(options.$html).html( response.html );
                    }
                    options.success.apply(this, arguments);
                }catch(e){
                    console.error(e);
                }
                return;
            },
            complete: function(xhr, status) {
                try{
                    if( '$target' in options ) {
                        $(options.$target).removeClass('loading');
                    }
                    options.complete.apply(this, arguments);
                }catch(e){
                    console.error(e);
                }
                options.xhr = null;
                return;
            }
        }, options);
        options.xhr = $.ajax(params);
        return self;
    }

    $(document).off('click.ajax', 'a[data-ajax]')
        .on('click.ajax', 'a[data-ajax]', function(ev) {
            ev.preventDefault();

            var $this = $(this), method = $this.attr('data-ajax');
            if( typeof(self[method]) == 'function' ) {
                self[method]($this, ev);
            } else {
                var options = $.extend( {}, $this.data() );
                if( !('url' in options) && ('href' in this) ) {
                    options.url = this.href;
                }
                options.$target = $this;
                this._options = options;
                call_ajax.call($this, options);
            }
            return;
        });

    $(document).off('click.event', '[data-event]')
        .on('click.event', '[data-event]', function(ev) {
            ev.preventDefault();
            var $this = $(this), method = $this.attr('data-event');
            if( typeof(self[method]) == 'function' ) {
                self[method]($this, ev);
            } else {
                var options = $.extend( {}, $this.data() );
                options.$target = $this;
                call_ajax.call($this, options);
            }
            return;
        });

    $('.progress .progress-bar').css("width",
        function() {
            return $(this).attr("aria-valuenow") + "%";
        }
    );

    var $confirmDialog = $('#confirm-popup'), $confirmMsg = $confirmDialog.find('.modal-body > .message'),
        $confirmResponseMsg = $confirmDialog.find('.response-message');

    $confirmDialog.on('show.bs.modal.confirm', function(ev) {
        var $btn = $(ev.relatedTarget), $node = $btn.closest('.node'), nodeData = $node.data('nodeData');
        $confirmMsg.html( $btn.data('message') );
        var $loading = $btn.closest($btn.data('loadingElement'));
        $loading = $loading.length ? $loading : $btn;
        var title = $loading.data('title');
        if( !title ) {
            $loading.data('title', title = $loading.attr('title'));
        }
        var options = $.extend({
            beforeSend: function(xhr, settings) {
                $confirmResponseMsg.addClass('hidden').removeClass('success').removeClass('error').html('');
                $loading.block(IMAGE_LOADING);
                $loading.removeClass('success').removeClass('error').attr('title', title);
                return;
            },
            error: function(xhr, status, error) {
                $confirmResponseMsg.removeClass('hidden').addClass('error');
                var msg = '';
                try {
                    if( ('responseJSON' in xhr) && xhr.responseJSON ) {
                        $confirmResponseMsg.html( msg = xhr.responseJSON.message );
                    } else {
                        error = xhr.getResponseHeader('xhr-message') || error;
                        error = JSON.parse(error);
                        $confirmResponseMsg.html( msg = error );
                    }
                } catch(e){
                    $confirmResponseMsg.html( msg = error );
                }
                $loading.addClass('error').attr('title', msg);
                return;
            },
            success: function(response, status, xhr) {
                if( xhr.status > 200 ) {
                    $confirmResponseMsg.removeClass('hidden').addClass('success');
                    var message = response.message || '';
                    $confirmResponseMsg.html( message );
                    $loading.addClass('success').attr('title', message);
                    if( 'removeElement' in options ) {
                        $btn.closest(options.removeElement).remove();
                    }
                    location.reload();
                }
                return;
            },
            complete: function(xhr, status) {
                $loading.unblock(IMAGE_LOADING);
                return;
            }
        }, $btn.data());

        $confirmResponseMsg.addClass('hidden').removeClass('success').removeClass('error').html('');

        $confirmDialog.off('click.ok', '.btn.btn-primary').on('click.ok', '.btn.btn-primary', function() {
            $confirmDialog.modal('hide');
            call_ajax.call($btn, options);
            return;
        });
    });
    /*var $selectBootstrap = $('select[data-use-bootstrap]');
    $selectBootstrap.selectpicker();*/
    function loading_data(){
        if( LOADED == true ){
            setTimeout(function() {
                $('.wrapper-lazy-loading-data').fadeOut(1000);
                $('.wrapper-main-content').css('opacity', '1');
            }, 500);
        }else{
            setTimeout(arguments.callee, 100);
        }
    }
    loading_data();
});
    
