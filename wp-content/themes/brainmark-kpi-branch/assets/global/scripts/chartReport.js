jQuery(function($){
    Number.isInteger = Number.isInteger || function(value) {
        return typeof value === "number" &&
            isFinite(value) &&
            Math.floor(value) === value;
    };
    $.ucfirst = function(str){
        var parts = str.split(' '),
            len = parts.length,
            words = [];
        for (var i = 0; i < len; i++) {
            var part = parts[i];
            var first = part[0].toUpperCase();
            var rest = part.substring(1, part.length);
            var word = first + rest;
            words.push(word);
        }
        return words.join(' ');
    }
    function ChartReport( element, options ) {
        var dataSet = $(element).data() || {};
        this.options = $.extend({}, ChartReport.defaultOptions, dataSet, options || {});
        this.$element = $(element);
        this.element = element;
        this.init();
    }
    ChartReport.prototype = {
        initElement: function(){
            var self = this;
            self.ctx = self.element.getContext('2d');
            return self;
        },
        toolTipsCallbackLabel: function(tooltipItem, data){
            var dataset = data.datasets[tooltipItem.datasetIndex];
            var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                return previousValue + currentValue;
            });
            var currentValue = dataset.data[tooltipItem.index];
            var title = data.labels[tooltipItem.index];
            var precentage = currentValue * 100 / total;
            var countData = dataset.data.length;
            if( tooltipItem.index == countData - 1 ){
                var sum = 100;
                dataset.data.forEach(function(item, idx){
                    var valueIndex = dataset.data[idx];
                    var dataString = ( valueIndex * 100 / total ).toFixed(2);
                    dataString = eval(dataString);
                    if( idx == countData - 1 ){
                        precentage = sum;
                    }else{
                        sum -= dataString;
                    }
                });
            }else{

            }
            //return [title + ": " + precentage.toFixed(2) + "%", "(Amount): " + currentValue.toString() +")" ];
            return [precentage.toFixed(2) + "%" ];
        },
        toolTipsCallbackTitle: function(tooltipItems, data){
            return;
        },
        toolTipsCustom: function(tooltipModel){
            if (!tooltipModel) return;
            // disable displaying the color box;
            tooltipModel.displayColors = false;
        },
        pluginAfterDatasetsDraw: function(chart, easing){
            function wrapText (context, text_1, text_2, x, y){
                var words = [text_1, text_2];
                var line = '';
                var lineHeight = 15;
                var maxWidth = 30;
                for(var i = 0; i < words.length; i++) {
                    var metrics = context.measureText(words[i]);
                    var testWidth = metrics.width;
                    var testLine = line + words[i] + ' ';
                    if (testWidth > maxWidth && i > 0) {
                        context.fillText(line, x, y);
                        line = words[i] + ' ';
                        y += lineHeight;
                    }
                    else {
                        line = testLine;
                    }
                }
                context.fillText(line, x, y);
            }

            // To only draw at the end of animation, check for easing === 1
            var ctx = chart.ctx, sefl = this;
            chart.data.datasets.forEach(function (dataset, i) {
                var meta = chart.getDatasetMeta(i);
                var total = dataset.data.reduce(
                    function(a, b){
                        return a + b;
                    }
                );
                var countMeta = dataset.data.length;
                var percent = (100/total);
                var sum = 100;
                if (!meta.hidden) {
                    meta.data.forEach(function(element, index) {
                        // Draw the text in black, with the specified font
                        ctx.fillStyle = 'rgb(0, 0, 0)';
                        var fontSize = 12;
                        var fontStyle = 'normal';
                        var fontFamily = 'Helvetica Neue';
                        ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                        // Just naively convert to string for now
                        var valueIndex = dataset.data[index];
                        var dataString = ( valueIndex * percent ).toFixed(2);
                        try {
                            dataString = eval(dataString);
                            if( index == countMeta - 1 ){
                                dataString = sum;
                            } else {
                                sum -= dataString;
                            }
                            //console.log( 'metricsWidth', ctx.measureText(dataString.toFixed(2) +
                            // "% \n ("+Promanland._t('Count')+": "+ valueIndex.toString() +")").width )
                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';
                            ctx.fillStyle = '#fff';

                            var position = element.tooltipPosition();
                            var y = position.y;
                            var x = position.x;
                            var fixed = 0;
                            if( !Number.isInteger(dataString) ){
                                fixed = 1;
                            }
                            ctx.fillText( dataString.toFixed(fixed) + "%", x, y );
                            //wrapText(ctx, dataString.toFixed(2) + "%",  "("+Promanland._t('Count')+": "+ valueIndex.toString() +")", x, y);
                        } catch(ex){
                        }
                        return;
                    });
                }
            });
            return;
        },
        pluginChartRegister: function(){
            var self = this;
            Chart.plugins.register({
                afterDatasetsDraw: self.pluginAfterDatasetsDraw
            });
            return self;
        },
        datePicker: function(){
            var self = this;
            var $startDate = $('.start-date', this.options.$form);
            var $endDate = $('.end-date', this.options.$form);
            $.datetimepicker.setLocale('vi');
            $startDate.datetimepicker({
                format:'d/m/Y',
                maxDate: new Date(),
                timepicker: false,
                onSelectDate: function(date, element, dataInput){
                    var start_date = date.setDate( date.getDate() + 1 );
                    var $end_date = $(element).closest('.filter-date-item').find('.end-date');
                    $.datepicker._destroyDatepicker($end_date);
                    $end_date.datetimepicker({
                        format:'d/m/Y',
                        minDate: new Date( start_date ),
                        maxDate: new Date(),
                        timepicker: false,
                    });
                    if( $end_date.val() == '' ) {
                        $end_date.trigger('focus');
                    }
                }
            });
            $endDate.focus(function(event){
                var $start_date = $(this).closest('.filter-date-item').find('.start-date');
                if( $start_date.val() == '' ){
                    $start_date.trigger('focus');
                }
            });
            return self;
        },
        selectPicker: function(selector) {
            $(selector, this.options.$form).selectpicker();
            return this;
        },
        chartPie: function(){

        },
        chartDoughnuts: function(type, data){
            var self = this;
            Chart.defaults.derivedDoughnut = Chart.defaults.doughnut;
            var customDoughnut = Chart.controllers.doughnut.extend({
                draw: function(ease) {
                    Chart.controllers.doughnut.prototype.draw.apply(this, [ease]);
                    var width = this.chart.chart.width,
                        height = this.chart.chart.height,
                        titleHeight = this.chart.titleBlock.height,
                        legendHeight = this.chart.legend.height;
                    //var total = this.getMeta().total;
                    var fontSize = (height / 114).toFixed(2);
                    var ctx = this.chart.chart.ctx;
                    ctx.font = "1.5em Verdana";
                    ctx.textBaseline = "middle";
                    ctx.fillStyle = "#008fd5";
                    var total = '0';
                    if( 'total' in this.chart.config.data ){
                        total = this.chart.config.data.total;
                    }
                    var padding = 2.5;
                    if( total % 1 !== 0  ){
                        // is float
                        padding = 3;
                        total = total.toFixed(1);
                    }
                    if( total >= 100 ){
                        padding = 2.85;
                    }else if( total < 10 ){
                        padding = 2.5;
                    }else if( total >= 10 && total < 100 ){
                        padding = 2.7
                    }

                    total += "%";
                    var text = total,
                        textX = Math.round((width ) / padding ),
                        textY = (height+titleHeight+legendHeight) - 20;
                    ctx.fillText(text, textX, textY);
                }
            });
            Chart.controllers.derivedDoughnut = customDoughnut;
            var options = {
                circumference: 1.3 * Math.PI,
                rotation: 0.85 * Math.PI,
                cutoutPercentage: 60,
                title: self.options.title || {display: true, text: ''},
                legend: {
                    display: false,
                    onClick: function(event, elements){
                        //console.log(event, elements);
                    }
                },
                responsive: self.options.responsive || true,
                tooltips:{
                    enable: false,
                    callbacks:{
                        label: false,
                        title: false
                    },
                    custom: false,
                    /*callbacks: {
                        label: self.options.toolTipsCallbackLabel || self.toolTipsCallbackLabel,
                        title: self.options.toolTipsCallbackTitle || self.toolTipsCallbackTitle,
                    },
                    custom: self.options.toolTipsCustom || self.toolTipsCustom,*/
                },
                /*onClick: function(evt, elements){
                    console.log( evt, elements );
                }*/
            };
            Chart.defaults.global.tooltips.enabled = false;
            if( !('chart' in self) ) {
                self.chart = new Chart( self.ctx, {
                    type: type,
                    data: data,
                    options: options
                });
            } else {
                self.updateChart( options );
            }
        },
        renderChartPie: function(type, data){
            var self = this;
            var options = {
                title: {display: true, text: ''},
                legend: {
                    display: false,
                    onClick: function(event, elements){
                        //console.log(event, elements);
                    }
                },
                responsive: true,
                tooltips:{
                    enable: true,
                    callbacks: {
                        label: self.toolTipsCallbackLabel,
                        title: self.toolTipsCallbackTitle,
                    },
                    custom:  self.toolTipsCustom,
                },
                /*onClick: function(evt, elements){
                    console.log( evt, elements );
                }*/
            };
            self.pluginChartRegister();
            if( !('chart' in self) ) {
                self.chart = new Chart( self.ctx, {
                    type: type,
                    data: data,
                    options: options
                });
            } else {
                self.updateChart( options );
            }
        },
        renderChartDoughnut: function(type, data){

        },
        renderChartDerivedDoughnut: function(type, data){
            var self = this;
            self.chartDoughnuts(type, data);
        },
        renderChartGauge: function(type, data){
            var self = this;
            var chart_gauge_settings = {
                lines: 12,
                angle: 0,
                lineWidth: 0.4,
                pointer: {
                    length: 0.75,
                    strokeWidth: 0.042,
                    color: '#1D212A'
                },
                gradientType: 1,
                limitMax: 'false',
                colorStart: '#ff6457',
                colorStop: '#f8fd3a',
                strokeColor: '#F0F3F3',
                generateGradient: true
            }
            self.chart = new Gauge(self.element).setOptions(chart_gauge_settings);
            var old_render = self.chart.render;
            /*self.chart.render = function() {
                var result = old_render.apply(this, arguments);
                var w = this.canvas.width /2;
                var fillStyle = this.ctx.createLinearGradient(0, 0, w, 0);
                return result;
            }*/
            self.chart.maxValue = 100;
            self.chart.animationSpeed = 32;
            self.chart.set(60);
            return self;
        },
        renderChart: function(){
            var self = this;
            var data = self.options.datasets;
            self.$element.attr('data-datasets', '');
            var dataSet = self.$element.data();
            var type;
            if( $.isPlainObject(dataSet) && 'type' in dataSet ){
                type = dataSet.type || 'pie';
            }
            var callback = "renderChart" + $.ucfirst( type );
            if( typeof self[callback] === 'function' ){
                self[callback](type, data);
            }
            return self;
        },
        updateChart: function(config) {
            this.chart.reset();
            this.chart.data = config;
            var $parent = $(this.chart.canvas.parentElement);
            if( config == '' ){
                this.chart.clear();
                $parent.addClass('chart-not-found');
                $parent.prepend('<div class="not-found">Not found</div>');
            }else{
                $parent.removeClass('chart-not-found');
                $('.not-found', $parent).remove();
                this.chart.render(config);
                this.chart.update(config);
            }

            return this;
        },
        destroy: function() {
            this.chart.destroy();
            return this;
        },
        init: function(){
            var self = this;
            self.initElement();
            //self.selectPicker(self.options.classSelectPicker);
            //self.datePicker();
            //self.pluginChartRegister();
            self.renderChart();
            return self;
        },
    };
    ChartReport.defaultOptions = {
        legend: true,
        title: {
            display: false,
            text: ''
        },
        classSelectPicker: '.selectpicker-chart',
        $elementParent: $(),
        selfParent: null
    }
    $.fn.chartReport = function(options){
        this.each(function(){
            var chart = $(this).data('chartReport');
            if( !chart ){
                chart = new ChartReport(this, options);
                $(this).data('chartReport', chart);
            }
        });
        return this;
    }
});