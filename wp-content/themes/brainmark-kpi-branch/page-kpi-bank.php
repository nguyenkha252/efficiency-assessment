<?php
/**
 * Template Name: KPI Bank
 */

include_once __DIR__ . '/../brainmark-kpi-main/login.php';

if ( !is_user_logged_in() ) {
    get_header('login');
    get_template_part( 'contents/content', 'login' );
    get_footer('login');
} else {
    get_header();
	$currentUser = $GLOBALS['current_user'];
	if( !empty($currentUser) && !empty($currentUser->orgchart) &&
        !in_array( $currentUser->orgchart->alias, ['phongban', 'nhanvien'] ) ||
        !empty($currentUser->allcaps['level_9']) || $currentUser->has_cap('level_9') ):
	    # get_template_part('contents/content', 'left');
		if( !empty( $_GET['type'] ) && $_GET['type'] === 'formula' ){
			get_template_part( 'contents/content', 'formula' );
		} else {
			get_template_part( 'contents/content', 'kpi-bank' );
		}
	endif;
    get_footer();
}
