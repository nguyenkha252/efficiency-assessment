<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 12/12/18
 * Time: 1:57 PM
 */

function wp_ajax_get_effa_dictionary_for_department($params){
    global $wpdb;
    $year = date('Y', time());
    $user = wp_get_current_user();
    $params = wp_slash($params);
    $html = "";
    $department = isset($params['department']) ? trim($params['department']) : "";
    $department = str_replace("and", "&", $department);
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    if( !user_is_manager() ){
        $message = __('Bạn không có quyền thực hiện thao tác này' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],403, $message);
    }

    $charts = effa_site_orgchart_get_department($department);
    if( !empty($charts) ){
        $args_effa = [
            'post_type' => ["efficiency", "questions"],
            'post_status' => ['publish'],
            'posts_per_page' => -1,
            'orderby' => 'ID',
            'order' => 'ASC'
        ];
        add_filter('posts_clauses', function($clauses) use ($year, $department){
            global $wpdb;
            $clauses['orderby'] = "{$wpdb->posts}.post_code ASC, {$wpdb->posts}.ID ASC";
            $clauses['where'] .= $wpdb->prepare(" AND {$wpdb->posts}.post_year = %d AND LOWER(department) = %s", $year, mb_strtolower($department));
            return $clauses;
        }, 100);
        $wpQueryD = new WP_Query($args_effa);

        $postEffaD = $wpQueryD->get_posts();
        $arrTargetD = [];
        $arrGroupD = [];
        $postEfficiencyD = [];
        $postQuestionsD = [];
        if( !empty( $postEffaD ) ){
            $groupPostTypeD = array_group_by($postEffaD, 'post_type');
            if( isset($groupPostTypeD['efficiency']) ){
                $postEfficiencyD = $groupPostTypeD['efficiency'];
                $arrTargetD = array_group_by($postEfficiencyD, 'post_parent');
            }
            if( isset($groupPostTypeD['questions']) ){
                $postQuestionsD = $groupPostTypeD['questions'];
                $postQuestionsD = array_group_by($postQuestionsD, 'post_code');
            }
            if( !empty( $arrTargetD ) ){
                $arrGroupD = $arrTargetD[0];
                unset($arrTargetD[0]);
            }
        }

        if( isset($arrGroupD) && !empty( $arrGroupD ) ){
            ob_start();
            ?>
            <table class="col-md-12 table-bordered table-striped table-condensed cf floatthead">
                    <?php
                    $labelSTT = __('STT', TPL_DOMAIN_LANG);
                    $labelCode= __('Mã', TPL_DOMAIN_LANG);
                    $labelLabelEfficiency = __('Năng lực', TPL_DOMAIN_LANG);
                    $labelDefine = __('Định nghĩa', TPL_DOMAIN_LANG);#__('Definition');
                    $labelEffaLV1 = __('Tối thiểu', TPL_DOMAIN_LANG);
                    $labelEffaLV2 = __('Cơ bản', TPL_DOMAIN_LANG);
                    $labelEffaLV3 = __('Đạt yêu cầu', TPL_DOMAIN_LANG);
                    $labelEffaLV4 = __('Thành thạo', TPL_DOMAIN_LANG);
                    $labelEffaLV5 = __('Xuất sắc', TPL_DOMAIN_LANG);
                    $labelQS = __('Câu hỏi', TPL_DOMAIN_LANG);
                    ?>
                    <thead class="cf row-thead-title-parent">
                    <tr class="row-thead-title-parent">
                        <th><?php echo $labelSTT; ?></th>
                        <th><?php echo $labelCode; ?></th>
                        <th><?php echo $labelLabelEfficiency; ?></th>
                        <th class="width20percent"></th>
                    </tr>
                    </thead>
                    <?php
                    $idxG = 1;

                    $urleditGroup = add_query_arg(['action' => 'update_group_effa'], admin_url("admin-ajax.php"));
                    foreach ( $arrGroupD as $key => $item_group ):
                        #foreach ( $item_group as $k => $item ) {
                        $groupID = $item_group->ID;
                        $nameGroup = get_the_title($groupID);
                        $sttRoman = convert_number_2_roman($idxG);
                        $groupCode = isset( $item_group->post_code ) && !empty( $item_group->post_code ) ? $item_group->post_code : "";
                        $groupCode = isset( $item_group->post_code ) && !empty( $item_group->post_code ) ? $item_group->post_code : "";
                        $rowItem = '';
                        if( array_key_exists( $groupID, $arrTargetD ) ){
                            $targetItem = $arrTargetD[$groupID];
                            $idxTg = 0;
                            $rowItem = '<tbody class="row-tbody-content-item" data-group-item="'.$idxG.'" data-group="' . $idxG . '">';
                            foreach ( $targetItem as $k => $item ){
                                $idxTg++;
                                $sttQS = 1; $rowQS = "";
                                $targetID = $item->ID;
                                $titleTarget = str_replace(str_split("\|"), "", get_the_title($targetID));
                                $contentTarget = get_the_content($targetID);
                                $postCode = !empty( $item->post_code ) ? $item->post_code : "";
                                $effaLV1 = !empty( $item->_effa_assessment_level_1 ) ? $item->_effa_assessment_level_1 : "";
                                $effaLV2 = !empty( $item->_effa_assessment_level_2 ) ? $item->_effa_assessment_level_2 : "";
                                $effaLV3 = !empty( $item->_effa_assessment_level_3 ) ? $item->_effa_assessment_level_3 : "";
                                $effaLV4 = !empty( $item->_effa_assessment_level_4 ) ? $item->_effa_assessment_level_4 : "";
                                $effaLV5 = !empty( $item->_effa_assessment_level_5 ) ? $item->_effa_assessment_level_5 : "";

                                if( isset($postQuestionsD[$postCode]) ){
                                    foreach ( $postQuestionsD[$postCode] as $kQS => $itemQS ){
                                        $QSID = $itemQS->ID;
                                        $titleQS = str_replace(str_split("\|"), "", get_the_title($itemQS));
                                        $postCodeQS = !empty( $itemQS->post_code ) ? $itemQS->post_code : "";
                                        $QS_anwser_1 = !empty( $itemQS->_effa_anwser_1 ) ? $itemQS->_effa_anwser_1 : "";
                                        $QS_anwser_2 = !empty( $itemQS->_effa_anwser_2 ) ? $itemQS->_effa_anwser_2 : "";
                                        $QS_anwser_3 = !empty( $itemQS->_effa_anwser_3 ) ? $itemQS->_effa_anwser_3 : "";
                                        $QS_anwser_4 = !empty( $itemQS->_effa_anwser_4 ) ? $itemQS->_effa_anwser_4 : "";
                                        $QS_anwser_5 = !empty( $itemQS->_effa_anwser_5 ) ? $itemQS->_effa_anwser_5 : "";

                                        $rowQS .= "<div class='col-md-6 question-item'>
                                                <table>
                                                    <tr>
                                                        <td>".__('Câu hỏi', TPL_DOMAIN_LANG)." {$sttQS}</td>
                                                        <td><input type='hidden' name='posts[{$sttQS}][ID]' value='{$QSID}'> <textarea class=\"width100percent\" name=\"posts[{$sttQS}][post_title]\" placeholder=\"".__('Câu hỏi', TPL_DOMAIN_LANG)." {$sttQS}\" >{$titleQS}</textarea></td>
                                                    </tr>
                                                    <tr>
                                                        <td>{$labelEffaLV1}</td>
                                                        <td data-title=\"{$labelEffaLV1}\"><textarea class=\"width100percent\" name=\"posts[{$sttQS}][_effa_anwser_1]\" placeholder=\"{$labelEffaLV1}\" >{$QS_anwser_1}</textarea></td>
                                                    </tr>
                                                    <tr>
                                                        <td>{$labelEffaLV2}</td>
                                                        <td data-title=\"{$labelEffaLV2}\"><textarea class=\"width100percent\" name=\"posts[{$sttQS}][_effa_anwser_2]\" placeholder=\"{$labelEffaLV2}\" >{$QS_anwser_2}</textarea></td>
                                                    </tr>
                                                    <tr>
                                                        <td>{$labelEffaLV3}</td>
                                                        <td data-title=\"{$labelEffaLV3}\"><textarea class=\"width100percent\" type=\"text\" name=\"posts[{$sttQS}][_effa_anwser_3]\" placeholder=\"{$labelEffaLV3}\" >{$QS_anwser_3}</textarea></td>
                                                    </tr>
                                                    <tr>
                                                        <td>{$labelEffaLV4}</td>
                                                        <td data-title=\"{$labelEffaLV4}\"><textarea class=\"width100percent\" name=\"posts[{$sttQS}][_effa_anwser_4]\" placeholder=\"{$labelEffaLV4}\" >{$QS_anwser_4}</textarea></td>
                                                    </tr>
                                                    <tr>
                                                        <td>{$labelEffaLV5}</td>
                                                        <td data-title=\"{$labelEffaLV5}\"><textarea class=\"width100percent\" name=\"posts[{$sttQS}][_effa_anwser_5]\" placeholder=\"{$labelEffaLV5}\" >{$QS_anwser_5}</textarea></td>
                                                    </tr>
                                                </table>
                                            </div>";
                                        $sttQS++;
                                    }
                                    unset($postQuestionsD[$postCode]);
                                }
                                /**
                                 * Kiểm tra với mỗi năng lực có đủ 3 câu hỏi chưa
                                 * Nếu chưa thì show đủ 3 form câu hỏi để người dùng nhập liệu
                                 */
                                if( ($sttQS - 1) < 3 ){
                                    for ($sttQS; $sttQS <= 3; $sttQS++){
                                        $rowQS .= "<div class='col-md-6 question-item'>
                                                <table>
                                                    <tr>
                                                        <td>".__('Câu hỏi', TPL_DOMAIN_LANG)." {$sttQS}</td>
                                                        <td><textarea class=\"width100percent\" name=\"posts[{$sttQS}][post_title]\" placeholder=\"".__('Câu hỏi', TPL_DOMAIN_LANG)." {$sttQS}\" ></textarea></td>
                                                    </tr>
                                                    <tr>
                                                        <td>{$labelEffaLV1}</td>
                                                        <td data-title=\"{$labelEffaLV1}\"><textarea class=\"width100percent\" name=\"posts[{$sttQS}][_effa_anwser_1]\" placeholder=\"{$labelEffaLV1}\" ></textarea></td>
                                                    </tr>
                                                    <tr>
                                                        <td>{$labelEffaLV2}</td>
                                                        <td data-title=\"{$labelEffaLV2}\"><textarea class=\"width100percent\" name=\"posts[{$sttQS}][_effa_anwser_2]\" placeholder=\"{$labelEffaLV2}\" ></textarea></td>
                                                    </tr>
                                                    <tr>
                                                        <td>{$labelEffaLV3}</td>
                                                        <td data-title=\"{$labelEffaLV3}\"><textarea class=\"width100percent\" type=\"text\" name=\"posts[{$sttQS}][_effa_anwser_3]\" placeholder=\"{$labelEffaLV3}\" ></textarea></td>
                                                    </tr>
                                                    <tr>
                                                        <td>{$labelEffaLV4}</td>
                                                        <td data-title=\"{$labelEffaLV4}\"><textarea class=\"width100percent\" name=\"posts[{$sttQS}][_effa_anwser_4]\" placeholder=\"{$labelEffaLV4}\" ></textarea></td>
                                                    </tr>
                                                    <tr>
                                                        <td>{$labelEffaLV5}</td>
                                                        <td data-title=\"{$labelEffaLV5}\"><textarea class=\"width100percent\" name=\"posts[{$sttQS}][_effa_anwser_5]\" placeholder=\"{$labelEffaLV5}\" ></textarea></td>
                                                    </tr>
                                                </table>
                                            </div>";
                                    }
                                }
                                if( !empty($rowQS) ){
                                    $rowQS = "
                                        <tr class='show-hide-questions'>
                                            <td colspan='4'>
                                                <form id='question-{$postCode}' class='frm-questions' novalidate=\"novalidate\" action=\"".admin_url('admin-ajax.php')."\" method=\"post\">
                                                    <input type='hidden' name='action' value='update_question_effa'>
                                                    <input type='hidden' name='post_code' value='{$postCode}'>
                                                    <input type='hidden' name=\"_wpnonce\" value=\"".wp_create_nonce('action_question_'.$postCode)."\">
                                                    {$rowQS}
                                                    <div class='clearfix'></div>
                                                    <div class=\"response-container\"></div>
                                                    <div class=\"action-bot\">
                                                        <button type=\"submit\" class=\"btn-brb-default btn-save\" id=\"save-target-item\" name=\"btn-save\"><i class=\"fa fa-floppy-o\" aria-hidden=\"true\"></i> <span class=\"text-action\">".__('Cập nhật', TPL_DOMAIN_LANG)."</span></button>
                                                        <button type=\"button\"class=\"btn-brb-default btn-cancel\">".__('Đóng', TPL_DOMAIN_LANG)."</button>
                                                    </div>
                                                    
                                               </form> 
                                            </td>
                                        </tr>
                                            ";
                                }

                                $rowItem .= "
                            <tr class=\"effa-target-item\" data-id=\"{$targetID}\">
                                <td data-title=\"{$labelSTT}\">{$idxTg}</td>
                                <td data-title=\"{$labelCode}\">{$postCode}</td>
                                <td data-toggle=\"modal\" data-target=\"#popup-action-effa\" colspan=\"1\" class=\"column-content style-italic\" data-title=\"{$labelLabelEfficiency}\">{$titleTarget}</td>
                                <td align='right' data-title='{$labelQS}'><a href='javascript:;' class='action-show-questions' data-target='#question-{$postCode}' >{$labelQS}</a></td>
                            </tr>
                            
                            {$rowQS}
                        ";

                            }
                            unset( $arrTargetD[$groupID] );

                            $rowItem .= '</tbody>';
                        }
                        $params = [
                            'ID' => $groupID,
                            '_wpnonce' => wp_create_nonce("update_group_effa_{$groupID}"),
                        ];
                        $params = esc_json_attr($params);
                        ?>
                        <tbody class="row-thead-title-group" data-group="<?php echo $idxG; ?>"
                               data-idx-group="<?php echo $idxG; ?>">
                        <tr>
                            <td align="center">
                                <a href="javascript:;" data-group-item="<?php echo $idxG; ?>" class="group-item-down"><i class="fa fa-chevron-circle-down fa-chevron-circle-up"></i></a>
                            </td>
                            <td colspan="1" class="table-responsive-hidden row-group-title group-numerical-order"><?php echo $groupCode; ?>
                            </td>
                            <td data-title="<?php echo $groupCode; ?>"
                                class="row-group-title group-title td-editable"><span data-editable="" data-type="text" data-params="<?php echo $params; ?>" data-pk=\"1\" data-url="<?php echo $urleditGroup; ?>" data-title="<?php echo $labelLabelEfficiency; ?> <<?php esc_attr_e($nameGroup); ?>>"><?php esc_attr_e($nameGroup); ?></span></td>
                            <td colspan="1"
                                class="row-group-title action-add-row-target action-add-row-group">
                                <a href="javascript:;" data-toggle="modal"
                                   data-target="#popup-action-effa" data-parent="<?php echo $groupID; ?>"
                                   data-event="add-row-item"><?php _e('Thêm mục tiêu', TPL_DOMAIN_LANG); ?>
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                <?php
                                if( empty( $rowItem ) ):
                                    ?>
                                    <a href="javascript:;" data-action="del_group_post_effa" data-title="<?php _e('Bạn có chắc chắn xoá mục tiêu', TPL_DOMAIN_LANG); ?> <strong><?php echo mb_strtoupper($nameGroup); ?></strong>" data-id="<?php echo $groupID; ?>" data-event="del-row-group">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                <?php endif; ?>
                            </td>
                        </tr>
                        </tbody>
                        <?php
                        echo $rowItem;
                        $idxG++;
                        #}
                    endforeach;

                    ?>
                </table>
        <?php
            $html = ob_get_clean();
        }
    }
    $httpCode = 201;
    send_response_json(['code' => $httpCode, 'html' => $html], $httpCode, __('Thành công'));
}