<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/5/18
 * Time: 17:35
 */

function wp_ajax_post_update_group_effa( $params ){
    global $wpdb;
	$user = wp_get_current_user();
    $postarr = wp_slash($params);
    $postID = isset($postarr['ID']) ? $postarr['ID']: 0;
	if( !$user ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],401, $message);
	}
	if( !wp_verify_nonce($params['_wpnonce'], "update_group_effa_{$postID}") ){
		$message = __('Phiên bản đã hết hạn. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],408, $message);
	}
	if( empty($user->allcaps['create_posts']) && !$user->has_cap('create_posts') ) {
		$message = __('Bạn không được cấp quyền thêm bài viết' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],402, $message);
	}
    $post = get_post($postID);
	if( !$post ){
        $message = __('Không tìm thấy năng lực' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],404, $message);
    }
    $update = wp_update_post(['post_title' => $postarr['value'], 'ID' => $postID]);
    if( is_wp_error( $update ) || !$update ){
        # @TODO process error
	    $message = __('Sửa tên năng lực thất bại. Vui lòng tải lại trang và thử lại' ,TPL_DOMAIN_LANG );
	    send_response_json(['error' => $message],415, $message);
    }else{
    	$message = __('Thành công', TPL_DOMAIN_LANG);
        send_response_json(['success' => $message], 201, $message);
    }
}