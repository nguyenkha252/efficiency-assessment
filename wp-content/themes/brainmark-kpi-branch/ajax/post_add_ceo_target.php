<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/9/18
 * Time: 13:58
 */


function wp_ajax_post_add_ceo_target($params) {
    global $wpdb;
    require_once THEME_DIR . '/inc/lib-orgchart.php';
    require_once THEME_DIR . '/inc/lib-kpis.php';
    require_once THEME_DIR . '/inc/lib-years.php';
    require_once THEME_DIR . '/inc/lib-users.php';

    $params = wp_slash( $params );
    $results = [];
    $user = wp_get_current_user();
    $keys = ['type', 'year_id', 'bank_id', 'plan', 'receive', 'percent', 'unit', 'chart_id'];
    # 'time_type', 'time_value', 'departments';
    $nonce = isset($params['_wpnonce']) ? $params['_wpnonce'] : '';
    $kpiID = isset($params['kpiID']) ? $params['kpiID'] : 0;

    if( !wp_verify_nonce($nonce, 'add_target') ) {
        $msg = "Sai mã bảo vệ";
        send_response_json(['code' => 403, 'message' => $msg], 403, $msg);
    }

    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableKpi = "{$prefix}kpis";
    $tableKpiYear = "{$prefix}kpi_years";

    if( empty($params['year_id']) || !is_numeric($params['year_id']) ) {
        $msg = "Tạo trọng số cho từng loại mục tiêu trước khi triển khai";
        send_response_json(['code' => 400, 'message' => $msg], 400, $msg);
    }
    #print_r( $params );exit();
    # $year_id = intval($params['year_id']); # $wpdb->get_var($wpdb->prepare("SELECT id FROM {$tableKpiYear} WHERE id = %d", $params['year_id']) );

    $year = kpi_get_year_by_id( $params['year_id'] );
    if( empty( $year ) ){
        $msg = "KPI cho năm {$params['time_value']} không tồn tại";
        send_response_json(['code' => 400, 'message' => $msg], 400, $msg);
    }
    $year_id = $year['id'];
    $dataYear = ['year' => $year['year'], 'precious' => $year['precious'], 'month' => $year['month'],
        'parent' => $year['id'], 'status' => $year['status'], 'created' => date('Y-m-d H:i:s', time())
        ];
    if( !empty( $params['type'] ) ){
        $type = mb_strtolower( $params['type'] );
        switch ($type){
            case 'customer':
                $dataYear['customer'] = $year['customer'];
                break;
            case 'operate':
                $dataYear['operate'] = $year['operate'];
                break;
            case 'development':
                $dataYear['development'] = $year['development'];
                break;
            default:
                $dataYear['finance'] = $year['finance'];
                break;
        }
    }


    $data = [];
    foreach($keys as $k) {
        $data[$k] = isset($params[$k]) ? $params[$k] : null;
    }
    if( !in_array($params['time_type'], ['year', 'precious', 'month']) ) {
        $params['time_type'] = 'year';
    }
    $data['receive'] = kpi_mysql_date($params['receive']);

    # $data[$params['time_type']] = $params['time_value'];
    $data['parent'] = 0;

    $data['user_id'] = $user->ID;
    $data['aproved'] = 1;
    $data['create_by_node'] = KPI_CREATE_BY_NODE_START;
    $data['status'] = KPI_STATUS_RESULT;
    $data['required'] = 'yes';
    $data['created'] = date('Y-m-d H:i:s', time());
	$chuc_danh = $params['chuc_danh'];
    if( !empty($chuc_danh) ) {
        foreach ($chuc_danh as $dep_id => $dep) {
            if (!isset($dep['id']) || !isset($dep['plan'])) {
                unset($chuc_danh[$dep_id]);
            }
        }
    }

    if( empty($chuc_danh) ) {
        $msg = __("Bạn chưa chọn chức danh hoặc chưa điền trọng số nào cả", TPL_DOMAIN_LANG);
        send_response_json(['code' => 400, 'message' => $msg], 400, $msg);
    }
    $errorList = [];
    $successList = [];

    # update kpi
    $kpi_id = $wpdb->get_var($wpdb->prepare("SELECT k.id FROM `{$tableKpi}` as k " .
        "WHERE (k.`parent` = 0) AND (k.`type` LIKE %s) AND (k.`year_id` = %d) AND (k.bank_id = %d)",
        $data['type'], $year_id, $data['bank_id']));

    $ids = $wpdb->get_col($wpdb->prepare("SELECT k.id FROM {$tableKpi} as k " .
        "WHERE k.`parent` IN ( " .
        "   SELECT k.id FROM {$tableKpi} as k " .
        "   WHERE k.`parent` = %d)", $kpi_id), ARRAY_A);

    if( !empty($ids) ) {
        $_ids = $ids;
        $where_ids = implode(', ', $ids);
        $where_ids = "( k.`parent` IN ({$where_ids}) )";

        $_ids = $wpdb->get_col("SELECT k.id FROM {$tableKpi} as k " .
            "WHERE k.`parent` IN ( " .
            "   SELECT k.id FROM {$tableKpi} as k " .
            "   WHERE $where_ids)", ARRAY_A);
        if( !empty($_ids) ) {
            $ids = array_merge($ids, $_ids);
        }
    }
    $where_ids = '';
    if (!empty($ids)) {
        $where_ids = implode(', ', $ids);
        $where_ids = "OR (`id` IN ({$where_ids}) )";
    }


    # Remove for all NhanVien
    # Remove for all PhongBan
    # $wpdb->query("DELETE FROM {$tableKpi} WHERE (`parent` = %d) {$where_ids}", $kpi_id);

    if( !empty($kpiID) ) {
        $resultKpi = kpi_get_kpi_by_id( $kpiID );
        # extract($params);
        # $data = compact('bank_id', 'plan', 'unit', 'receive', 'percent');
        $data = ['bank_id' => $data['bank_id'], 'plan' => $data['plan'], 'unit' => $data['unit'], 'receive' => $data['receive'], 'percent' => $data['percent']];
        $data['receive'] = kpi_mysql_date($data['receive']);
        $result_kpi = 0;
        if( !empty( $resultKpi ) && !is_wp_error( $resultKpi ) ) {
            $wpdb->query('START TRANSACTION;');
            $result_kpi = $resultKpi['id'];
            $dataYear['parent'] = $resultKpi['year_id'];
            $parent = $wpdb->update($tableKpi, $data, ['id' => $result_kpi]);
            if( !empty( $wpdb->last_error ) ) {
                $errorList[$result_kpi] = __LINE__ . "\n{$wpdb->last_error}\n{$wpdb->last_query}";
            } else {
                $successList[$result_kpi] = "Cập nhật thành công";
            }
            $old_kpis = [];
            $rows = get_kpi_by_parent( $kpiID );
            $delYearIDs = [];
            foreach ($rows as $i => &$item ) {
                $old_kpis[$item['id']] = &$item;
                $delYearIDs[] = &$item['year_id'];
            }

            if( !empty($chuc_danh) ) {
                $delete_ids = [];
                foreach ($chuc_danh as $chart_id => $kpi_data) {
                    $users = kpi_get_all_users(['orgchart_id' => $chart_id]);
                    if( array_key_exists('id', $kpi_data) ) {
                        $kpi_id = $kpi_data['id'];
                        foreach ($users as $u) {
                            $kpi_insert = $data;
                            if( isset($kpi_insert['id']) ) {
                                unset($kpi_insert['id']);
                            }
                            #$kpi_insert['year_id']  = $year_id;
                            $kpi_insert['user_id']  = $u['ID'];
                            $kpi_insert['chart_id'] = $chart_id;
                            $kpi_insert['plan']     = $kpi_data['plan'];
                            $kpi_insert['bank_id']  = $kpi_data['bank_id'];
                            $kpi_insert['year_id']  = $kpi_data['year_id'];
                            $kpi_insert['unit']     = $kpi_data['unit'];
                            $kpi_insert['aproved']  = 1;
                            $kpi_insert['percent']  = doubleval($kpi_data['percent']);
                            $kpi_insert['parent']   = $result_kpi;
                            $kpi_insert['status']   = KPI_STATUS_RESULT;

                            $child_kpi_id = $wpdb->get_var($wpdb->prepare("SELECT k.id FROM `{$tableKpi}` as k " .
                                "WHERE (k.`parent` = %d) AND (k.`type` LIKE %s) AND (k.`user_id` = %d) ".
                                "AND (k.`chart_id` = %d) AND (k.`year_id` = %d) AND (k.bank_id = %d)",
                                $result_kpi, $params['type'], $u['ID'], $chart_id, $kpi_data['year_id'], $kpi_data['bank_id']));

                            if (!empty($child_kpi_id)) {
                                if( isset($old_kpis[$child_kpi_id]) ) {
                                    unset($old_kpis[$child_kpi_id]);
                                }
                                $id = $wpdb->update($tableKpi, $kpi_insert, ['id' => $child_kpi_id]);
                                if( $wpdb->last_error && (strpos($wpdb->last_error, "Duplicate entry") === false) ) {
                                    $errorList["{$result_kpi}_{$kpi_insert['user_id']}_{$kpi_insert['chart_id']}"] = __LINE__ . "\n{$wpdb->last_error}\n{$wpdb->last_query}";
                                } else {
                                    $successList["{$result_kpi}_{$kpi_insert['user_id']}_{$kpi_insert['chart_id']}"] = "Update KPI con thành công";
                                }
                            } else {
                                /*
                                $wpdb->insert( $tableKpiYear, $dataYear );
                                if( !empty($wpdb->last_error) ){
                                    $errorList[] = $wpdb->last_error;
                                }
                                $yearParent = $wpdb->insert_id;
                                $kpi_insert['year_id']  = $yearParent;
                                */
                                $id = $wpdb->insert($tableKpi, $kpi_insert);
                                if( $wpdb->last_error && (strpos($wpdb->last_error, "Duplicate entry") === false) ) {
                                    $errorList["{$result_kpi}_{$kpi_insert['user_id']}_{$kpi_insert['chart_id']}"] = __LINE__ . "\n{$wpdb->last_error}\n{$wpdb->last_query}";
                                } else {
                                    $successList["{$result_kpi}_{$kpi_insert['user_id']}_{$kpi_insert['chart_id']}"] = "Thêm KPI con thành công";
                                }
                            }
                        }
                    } else {
                        # Remove Items
                        $deleted = intval($kpi_data['deleted']);
                        # foreach ($users as $u) {
                        # }
                        if( $deleted > 0 ) {
                            $delete_ids[] = $deleted;
                        }
                    }
                }
                if( !empty($delete_ids) ) {
                    $delete_ids = implode(', ', $delete_ids);
                    $wpdb->query("DELETE FROM {$tableKpi} WHERE id IN ({$delete_ids})");
                    if( $wpdb->last_error ) {
                        $errorList["{$delete_ids}"] = "{$wpdb->last_error}\n{$wpdb->last_query}";
                    } else {
                        $successList["{$delete_ids}"] = "Xóa KPI con thành công";
                    }
                }
            }

            if( empty( $errorList ) ){
                $wpdb->query('COMMIT;');
                $msg = __('Lưu thành công', TPL_DOMAIN_LANG);
                $httpCode = 201;
            } else {
                $wpdb->query('rollback;');
                $msg = "Có lỗi xảy ra khi lưu KPI";
                $httpCode = 400;
            }
        } else {
            $msg = __("Không tìm thấy KPI", TPL_DOMAIN_LANG);
            $httpCode = 404;
        }
        send_response_json(['code' => $httpCode, 'message' => $msg, 'success' => $successList, 'error' => $errorList], $httpCode, $msg);
    } else {
        #insert kpi
        $wpdb->query("START TRANSACTION;");
        # CEO Insert KPI
        if( array_key_exists('kpiID', $data) ) {
            unset($data['kpiID']);
        }
        # status_header( 205, 'Debug' );
        $errorList = [];
        $successList = [];
        if( !empty($wpdb->last_error) ){
            $errorList[] = $wpdb->last_error;
        }
        $res_id = $wpdb->insert($tableKpi, $data);
        $insert_id = $wpdb->insert_id;

        if( $wpdb->last_error ) {
            if( (strpos($wpdb->last_error, "Duplicate entry") !== false) ) {
                $insert_id = $wpdb->get_var($wpdb->prepare("SELECT id FROM $tableKpi ".
                    "WHERE (`year_id` = %d) AND (`bank_id` = %d) AND (`chart_id` = %d) ".
                    "AND (`user_id` = %d) AND (`parent` = %d) AND (`type` = %s)",
                    $year_id, $data['bank_id'], $data['chart_id'], $data['user_id'],
                    $data['parent'], $data['type']));
                $res_id = 0;
            } else {
                $errorList[] = __LINE__ . "\n{$wpdb->last_error}\n{$wpdb->last_query}";
            }
        } else {
            $successList[$insert_id] = "Thêm KPI thành công";
        }

        if( $res_id !== false ) {
            $old_kpis = [];
            $rows = get_kpi_by_parent( $insert_id );
            foreach ($rows as $i => &$item ) {
                $old_kpis[$item['id']] = &$item;
            }
            if( !empty($chuc_danh) ) {
                foreach ($chuc_danh as $chart_id => $kpi_data) {
                    if( array_key_exists('id', $kpi_data) ) {
                        $users = kpi_get_all_users(['orgchart_id' => $chart_id]);
                        # $kpi_data;
                        foreach ($users as $u) {
                            $kpi_insert = $data;
                            if (isset($kpi_insert['id'])) {
                                unset($kpi_insert['id']);
                            }
                            #$kpi_insert['year_id']  = $year_id;
                            $kpi_insert['user_id']  = $u['ID'];
                            $kpi_insert['chart_id'] = $chart_id;
                            $kpi_insert['plan']     = $kpi_data['plan'];
                            $kpi_insert['bank_id']  = $kpi_data['bank_id'];
                            $kpi_insert['year_id']  = $kpi_data['year_id'];
                            $kpi_insert['unit']     = $kpi_data['unit'];
                            $kpi_insert['aproved']  = 1;
                            $kpi_insert['percent']  = doubleval($kpi_data['percent']);
                            $kpi_insert['parent']   = $insert_id;
                            $kpi_insert['status']   = KPI_STATUS_RESULT;

                            $child_kpi_id = $wpdb->get_var($wpdb->prepare("SELECT k.id FROM `{$tableKpi}` as k " .
                                "WHERE (k.`parent` = %d) AND (k.`type` LIKE %s) AND (k.`user_id` = %d) " .
                                "AND (k.`chart_id` = %d) AND (k.`year_id` = %d) AND (k.bank_id = %d)",
                                $insert_id, $params['type'], $u['ID'], $chart_id, $kpi_data['year_id'], $kpi_data['bank_id']));

                            if (!empty($child_kpi_id)) {
                                if( isset($old_kpis[$child_kpi_id]) ) {
                                    unset($old_kpis[$child_kpi_id]);
                                }
                                $id = $wpdb->update($tableKpi, $kpi_insert, ['id' => $child_kpi_id]);
                                if( $wpdb->last_error && (strpos($wpdb->last_error, "Duplicate entry") === false) ) {
                                    $errorList["{$insert_id}_{$kpi_insert['user_id']}_{$kpi_insert['chart_id']}_{$child_kpi_id}"] = __LINE__ . "\n{$wpdb->last_error}\n{$wpdb->last_query}";
                                } else {
                                    $successList["{$insert_id}_{$kpi_insert['user_id']}_{$kpi_insert['chart_id']}_{$child_kpi_id}"] = "Update KPI con thành công";
                                }
                            } else {
                                $id = $wpdb->insert($tableKpi, $kpi_insert);
                                if( $wpdb->last_error && (strpos($wpdb->last_error, "Duplicate entry") === false) ) {
                                    $errorList["{$insert_id}_{$kpi_insert['user_id']}_{$kpi_insert['chart_id']}"] = __LINE__ . "\n{$wpdb->last_error}\n{$wpdb->last_query}";
                                } else {
                                    $successList["{$insert_id}_{$kpi_insert['user_id']}_{$kpi_insert['chart_id']}"] = "Thêm KPI con thành công";
                                }
                            }
                        }
                    }
                }
            }
        }

        if( !empty($errorList) ) {
            $wpdb->query("rollback;");
            $msg = "Có lỗi xảy ra khi thêm KPI";
            send_response_json(['code' => 400, 'message' => $msg, 'success' => $successList, 'errors' => $errorList, 'success' => $successList], 400, $msg);
        }

        $wpdb->query("COMMIT;");
        $msg = "Thêm KPI thành công";
        send_response_json(['code' => 201, 'message' => $msg, 'success' => $successList], 201, $msg);
    }
}
