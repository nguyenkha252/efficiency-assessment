<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/15/18
 * Time: 11:26
 */

function wp_ajax_post_chuc_danh_update($params) {
    global $wpdb;
    $nonce = !empty($params['_wpnonce']) ? $params['_wpnonce'] : '';
    if( !wp_verify_nonce($nonce, 'chuc_danh_update') ) {
        $msg = 'Mã bảo vệ không đúng';
        send_response_json(['code' => 403, 'message' => $msg], 403, $msg);
    }
    $id = isset($params['ID']) ? intval($params['ID']) : 0;
    if( $id < 1 ) {
        $msg = 'Định danh của chức danh không tồn tại';
        send_response_json(['code' => 403, 'message' => $msg], 403, $msg);
    }
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableRoles = "{$prefix}kpi_roles";
    $keys = ['chuc_danh' => 'Tên chức danh không được bỏ trống', 'cap_bac' => 'Cấp bậc của chức danh không được bỏ trống'];
    $updateData = [];
    $error = [];

    foreach($keys as $k => $msg) {
        if( isset($params[$k]) ) {
            if( empty($params[$k]) ) {
                $error[$k] = $msg;
            } else {
                $updateData[$k] = $params[$k];
            }
        }
    }

    if( empty($error) ) {
        $res = $wpdb->update($tableRoles, $updateData, ['ID' => $id]);
        if( $res !== false ) {
            $updateData['ID'] = $id;
            send_response_json($updateData, 202, 'Cập nhật thành công');
        } else if( $res === 0 ) {
            $updateData['ID'] = $id;
            send_response_json($updateData, 203, 'Không có sự thay đổi');
        } else {
            if( $wpdb->last_error ) {
                $error['db'] = $wpdb->last_error;
            }
        }

    }
    if( !empty($error) ) {
        $msg = implode("\n", $error);
    } else {
        $msg = 'Có lỗi xảy ra';
    }

    send_response_json(['code' => 400, 'message' => $msg, 'errors' => $error], 400, 'Có lỗi xảy ra');
}