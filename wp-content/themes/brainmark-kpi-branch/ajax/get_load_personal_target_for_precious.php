<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/19/18
 * Time: 11:02
 */

function wp_ajax_get_load_personal_target_for_precious( $params ){
    require_once THEME_DIR . '/inc/lib-years.php';
    require_once THEME_DIR . '/inc/lib-kpis.php';
    require_once THEME_DIR . '/inc/lib-users.php';
    $params = wp_unslash($params);
    $kpiID = (int) $params['id'];
    $user = wp_get_current_user();
    $orgchart = user_load_orgchart( $user );
    $userID = $user->ID;
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
        $httpCode = 401;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }
    $result = get_kpi_and_post_by_kpi_id($kpiID);
    if( empty( $result ) ){
	    $message = __('Không tìm thấy KPI', TPL_DOMAIN_LANG );
	    $httpCode = 403;
	    send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }

    $data['plan'] = $result['plan'];
    $data['post_title'] = str_replace(str_split("\|"), "",$result['post_title']);
    $data['plan_on_level'] = !empty( $result['plan_on_level'] ) ? $result['plan_on_level'] : '';
    $data['parent'] = $kpiID;
    $data['receive'] = substr( kpi_format_date( $result['receive'] ), 0, 10 );
    $data['percent'] = $result['percent'];
    $data['unit'] = getUnit( $result['unit'] );
    $data['_wpnonce'] = wp_create_nonce('create_kpi_for_end_level');
    $data['year'] = $result['year'];
    # lấy id của year và năm của id sau đó lấy con của year đó
    #$resultsOrgChart = orgchart_check_children_by_id( $result['chart_id'] );
	$checkChartChild = check_subordinates_chart($orgchart->id, $result['chart_id']);
    if( !empty($checkChartChild) || $userID == $result['user_id'] || user_is_manager() ){
        $userID = $result['user_id'];
	    $kpiPrecious = get_kpi_by_year_precious( $result['year_id'], $result['id'], $userID );
        $output = ""; $outputTemp = "";
        $outputLeft = "";
        $outputRight = "";
        if( !empty( $kpiPrecious ) ){
            foreach ( $kpiPrecious as $key => $precious ){
	            $percent = '';
	            if( !empty($precious['plan']) ){
		            $percent = !empty($precious['percent']) ? $precious['percent'] : '';
	            }
	            $showStatusApprove = '';
                if( $userID != $user->ID ){
	                if( empty($precious['plan']) )
		                continue;
                    if( !in_array( $precious['status'], [KPI_STATUS_PENDING, KPI_STATUS_DRAFT] ) ){
                        $statusText = KPI_DA_DUYET;
                        $checked = "checked";
                    }else{
                        $statusText = KPI_CHUA_DUYET;
                        $checked = "";
                    }
                    $showStatus = "
                                    <input name=\"plan_precious[". $precious['precious'] ."][precious]\" value=\"". $precious['precious'] ."\" type=\"hidden\" />
                                    <input name=\"plan_precious[". $precious['precious'] ."][id]\" value=\"". $precious['id'] ."\" type=\"hidden\" />
                                    <span class=\"input-group-addon\" for=\"plan\" id=\"plan-". $precious['precious'] ."-". $precious['precious'] ."\">".__('Quý ', TPL_DOMAIN_LANG) . $precious['precious'] ."</span>
                                    <input type=\"text\" name=\"plan_precious[". $precious['precious'] ."][plan]\" id=\"plan-". $precious['precious'] ."-". $precious['precious'] ."\" class=\"plan-". $precious['precious'] ."-". $precious['precious'] ." form-control confirm-month-form-control\" placeholder=\"".__('Kế hoạch', TPL_DOMAIN_LANG)."\" value=\"". $precious['plan'] ."\" />
                                    ";
                    $showStatusApprove = "<div class=\"right-status-confirm-month\">
                                        <label class=\"switch\">
                                            <input class=\"checkbox-status hidden\" type=\"checkbox\" name=\"plan_precious[{$precious['precious']}][status]\" {$checked} value=\"1\"/>
                                            <span class=\"checkbox-slider\"></span>
                                        </label>
                                        <span>{$statusText}</span>
                                    </div>";
                }else{
                    if( !in_array( $precious['status'], [KPI_STATUS_PENDING, KPI_STATUS_DRAFT] ) ){
                        $showStatus = "
                                    <span class=\"input-group-addon\" for=\"plan\" id=\"plan-". $precious['precious'] ."-". $precious['precious'] ."\">".__('Quý ', TPL_DOMAIN_LANG) . $precious['precious'] ."</span>
                                    <input type=\"text\" disabled id=\"plan-". $precious['precious'] ."-". $precious['precious'] ."\" class=\"plan-". $precious['precious'] ."-". $precious['precious'] ." form-control\" placeholder=\"".__('Kế hoạch', TPL_DOMAIN_LANG)."\" value=\"". $precious['plan'] ."\" />
                    ";
                    }else{
                        $showStatus = "<input name=\"plan_precious[". $precious['precious'] ."][precious]\" value=\"". $precious['precious'] ."\" type=\"hidden\" />
                                    <input name=\"plan_precious[". $precious['precious'] ."][id]\" value=\"". $precious['id'] ."\" type=\"hidden\" />
                                    <span class=\"input-group-addon\" for=\"plan\" id=\"plan-". $precious['precious'] ."-". $precious['precious'] ."\">".__('Quý ', TPL_DOMAIN_LANG) . $precious['precious'] ."</span>
                                    <input type=\"text\" name=\"plan_precious[". $precious['precious'] ."][plan]\" id=\"plan-". $precious['precious'] ."-". $precious['precious'] ."\" class=\"plan-". $precious['precious'] ."-". $precious['precious'] ." form-control\" placeholder=\"".__('Kế hoạch', TPL_DOMAIN_LANG)."\" value=\"". $precious['plan'] ."\" />";
                    }
                }
				if( $userID != $user->ID ){
					$outputTemp = "
					<div class=\"item-form-group-for group-onlevel-approve\">
						<div class='onlevel-approve'>
							<div class=\"form-group input-group left-target-for\">
	                                {$showStatus}
	                            </div>
	                            <div class=\"form-group input-group right-target-percent\">
		                                                                    <span class=\"input-group-addon\" for=\"plan\" id=\"plan-" . $precious['precious'] . "-" . $precious['precious'] . "\">" . __( 'Trọng số ', TPL_DOMAIN_LANG ) . "</span>
		                                                                    <input type=\"text\" name=\"plan_precious[" . $precious['precious'] . "][percent]\" id=\"plan-" . $precious['precious'] . "-" . $precious['precious'] . "\" class=\"plan-" . $precious['precious'] . " form-control\" placeholder=\"" . __( 'Trọng số ', TPL_DOMAIN_LANG ) . "\" value=\"" . $percent . "\" />
		                        </div>
						</div>
						{$showStatusApprove}
					</div>
					";
                	if( $precious['precious'] <= 2 ){
                		$outputLeft .= $outputTemp;
	                }else {
		                $outputRight .= $outputTemp;
	                }
				}else {
					$disablePercent = '';
					if( !in_array( $precious['status'], [KPI_STATUS_PENDING, KPI_STATUS_DRAFT] ) ){
						$disablePercent = 'disabled';
					}
					$outputTemp = "
						<div class=\"item-form-group-for owner-register-for\">
							<div class=\"form-group input-group left-target-for\">
                                {$showStatus}
                            </div>
                            <div class=\"form-group input-group right-target-percent\">
	                                                                    <span class=\"input-group-addon\" for=\"plan\" id=\"plan-" . $precious['precious'] . "-" . $precious['precious'] . "\">" . __( 'Trọng số ', TPL_DOMAIN_LANG ) . "</span>
	                                                                    <input type=\"text\" name=\"plan_precious[" . $precious['precious'] . "][percent]\" {$disablePercent} id=\"plan-" . $precious['precious'] . "-" . $precious['precious'] . "\" class=\"plan-" . $precious['precious'] . " form-control\" placeholder=\"" . __( 'Trọng số ', TPL_DOMAIN_LANG ) . "\" value=\"" . $percent . "\" />
	                        </div>
                        </div>
                            ";
					if( $precious['precious'] <= 2 ){
						$outputLeft .= $outputTemp;
					}else {
						$outputRight .= $outputTemp;
					}
				}
            }
	        $output = "
                <table class=\"table-field-list\" border=\"0\">
                	<tr>
                		<td class=\"column-left\" valign=\"top\">{$outputLeft}</td>
                		<td class=\"column-right\" valign=\"top\">{$outputRight}</td>
					</tr>
				</table>
            ";
        }
        $message = __('Thành công', TPL_DOMAIN_LANG);
        $httpCode = 201;
        send_response_json(['code' => $httpCode, 'data_personal' => $data, 'kpi_precious' => $output], $httpCode, $message);
    } else {
        # nếu kpi hiện tại không phải là cấp dưới trực tiếp thì thông báo cho người dùng
        $_user = get_user_by('ID', $result['user_id']);
        $fullName = $_user->first_name . ' ' . $_user->last_name;
        $message = __("Bạn không phải là cấp trên của {$fullName}" ,TPL_DOMAIN_LANG );
        $httpCode = 404;
        send_response_json(['code' => $httpCode, 'message' => $message],$httpCode, $message);
    }


}