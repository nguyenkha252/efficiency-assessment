<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 14/03/2018
 * Time: 12:56
 */
require_once THEME_DIR . '/inc/lib-years.php';
function wp_ajax_post_behavior_apply_for_role( $params ){
	global $wpdb;
	$params = wp_slash($params);
	$user = wp_get_current_user();
	if (!$user) {
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG);
		$httpCode = 401;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	if( !user_is_manager() ){
		$message = __('Bạn không có quyền thêm/sửa', TPL_DOMAIN_LANG);
		$httpCode = 403;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	if( !wp_verify_nonce($params['_wpnonce'], 'behavior_apply_for_role') ){
		$message = __('Phiên bản đã hết hạn. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
		$httpCode = 408;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	$result = kpi_get_year_by_id( $params['year_id'] );
	if( $result ){
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpiYear = "{$prefix}kpi_years";
		if( array_key_exists('charts', $params) ){
			$orgCharts = orgchart_get_list_orgchart();
			$charts = $params['charts'];
			$arrDiff = [];
			foreach ( $orgCharts as $key => $item ){
				if( in_array($item->alias, ['congty', 'tapdoan']) )
					continue;
				if( !in_array( $item->id, $charts ) ){
					$arrDiff[] = $item->id;
				}
			}
			$apply_for = maybe_serialize( $arrDiff );
			$wpdb->update( $tableKpiYear, ['apply_for' => $apply_for], ['id' => $params['year_id']] );
			if( !empty( $wpdb->last_error ) ){
				$msg      = "Có lỗi xảy ra khi lưu";
				$httpCode = 400;
				send_response_json( [
					'code'    => $httpCode,
					'message' => $msg,
					'error'   => $wpdb->last_error
				], $httpCode, $msg );
			}
		}
		$message = "Thành công";
		$httpCode = 201;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}else{
		$message = "Không tìm thấy năm thái độ hành vi";
		$httpCode = 404;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
}