<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 23/03/2018
 * Time: 14:22
 */
require_once THEME_DIR . '/inc/lib-users.php';
function wp_ajax_get_check_email_user( $params ){
	$params = wp_unslash($params);
	$user = wp_get_current_user();
	if( !$user ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
		$httpCode = 401;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	if( !user_is_manager() ){
		$message = __('Bạn không có quyền chỉnh sửa', TPL_DOMAIN_LANG);
		$httpCode = 403;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	if( empty($params['email'])){
		$message = __('Email không được để trống', TPL_DOMAIN_LANG);
		$httpCode = 401;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	#email_exists();
	if( !is_email( $params['email'] ) ){
		$message = __('Email không đúng định dạng', TPL_DOMAIN_LANG);
		$httpCode = 406;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}else{
		if( email_exists( $params['email'] ) ){
			$message = __('Email đã tồn tại', TPL_DOMAIN_LANG);
			$httpCode = 406;
			send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
		}
		$message = __('Có thể sử dụng email này', TPL_DOMAIN_LANG);
		$httpCode = 201;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
}