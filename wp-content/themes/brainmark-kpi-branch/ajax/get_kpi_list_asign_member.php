<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/18/18
 * Time: 14:28
 */
function wp_ajax_get_kpi_list_asign_member( $params ){
    require_once THEME_DIR . '/inc/lib-kpis.php';
    $params = wp_slash( $params );
    $bank_id = $params['bank'];
    $year = $params['year'];
    $user = wp_get_current_user();
    $orgchart = user_load_orgchart($user);
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
        $httpCode = 401;
        send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
    }
    $result = get_kpi_year_orgchart_by_bank($year, $orgchart->id, $bank_id);
    if ($result) {
        $httpCode = 201;
        $result['receive'] = substr(kpi_format_date($result['receive']), 0, 10);
        $result['department_personal_plan'] = $result['personal_plan'];
        unset($result['personal_plan']);
        send_response_json( $result, $httpCode, $message );
    } else {
        $httpCode = 404;
        $message = __('Không tìm thấy ngân hàng KPI', TPL_DOMAIN_LANG);
        send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
    }


}