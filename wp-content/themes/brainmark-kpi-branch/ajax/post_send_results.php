<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 28/01/2018
 * Time: 13:24
 */
require_once THEME_DIR .'/ajax/post_personal_update_result.php';
function wp_ajax_post_send_results( $params ){
	global $wpdb;
	$user = wp_get_current_user();
	$orgchart = user_load_orgchart( $user );
	$postarr = wp_slash($params);
	if( !$user ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
		$httpCode = 401;
		send_response_json(['code' => $httpCode, 'message' => $message],$httpCode, $message);
	}
	$prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
	$tableKpi = "{$prefix}kpis";
	require_once THEME_DIR . '/inc/lib-users.php';
	require_once THEME_DIR . '/inc/lib-orgchart.php';
	require_once THEME_DIR . '/inc/lib-kpis.php';
	$isUpdate = update_result($postarr);
	if( !$isUpdate ){
		$msg = __('Có lỗi xảy ra vui lòng thử lại.', TPL_DOMAIN_LANG);
		$httpCode = 405;
		send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
	}

	if( !empty( $postarr['kpis'] ) ){
		$nodeEnd = check_user_is_node_end();
		$wpdb->query( "START TRANSACTION;" );
		foreach ( $postarr['kpis'] as $kpi){
			$kpiID = $kpi['ID'];
			$status = $kpi['status'];
			if( $status == 1 ){
				$dataUpdate['status'] = KPI_STATUS_WAITING;
			}else{
				$dataUpdate['status'] = KPI_STATUS_RESULT;
			}
			if( $nodeEnd ){
				$result = get_kpi_by_user_orgchart_kpi_id( $user->ID, $orgchart->id, $kpiID );
				if( $result ){
					$wpdb->update( $tableKpi, $dataUpdate, ['id' => $kpiID] );
					if( !empty( $wpdb->last_error ) ){
						$wpdb->query("ROLLBACK;");
						$message = __('Đã xảy ra lỗi. Vui lòng thử lại' ,TPL_DOMAIN_LANG );
						$httpCode = 404;
						send_response_json(['code' => $httpCode, 'message' => $message, 'error' => $wpdb->last_error],$httpCode, $message);
					}
				}else{
                    $wpdb->query("ROLLBACK;");
					$message = __('Không tìm thấy KPI' ,TPL_DOMAIN_LANG );
					$httpCode = 404;
					send_response_json(['code' => $httpCode, 'message' => $message],$httpCode, $message);
				}
			}else if( !$nodeEnd ){
				#BGD OR PHONGBAN
				$result = kpi_get_kpi_by_id($kpiID);
				if( $result ){
					$wpdb->update( $tableKpi, $dataUpdate, ['id' => $kpiID] );
					if( !empty( $wpdb->last_error ) ){
						$wpdb->query("ROLLBACK;");
						$message = __('Đã xảy ra lỗi. Vui lòng thử lại' ,TPL_DOMAIN_LANG );
						$httpCode = 404;
						send_response_json(['code' => $httpCode, 'message' => $message, 'error' => $wpdb->last_error],$httpCode, $message);
					}
				}else{
					$message = __('Không tìm thấy KPI' ,TPL_DOMAIN_LANG );
					$httpCode = 404;
					send_response_json(['code' => $httpCode, 'message' => $message],$httpCode, $message);
				}
			}
		}
		$wpdb->query("COMMIT;");
		$message = __('Lưu thành công', TPL_DOMAIN_LANG);
		$httpCode = 201;
		$location = '';
		send_response_json(
			['code' => $httpCode, 'message' => $message, 'location' => $location],
			$httpCode,
			$message
		);
	}else{
		$message = __('Không tìm thấy KPI' ,TPL_DOMAIN_LANG );
		$httpCode = 404;
		send_response_json(
			['code' => $httpCode, 'message' => $message],
			$httpCode,
			$message
		);
	}
}