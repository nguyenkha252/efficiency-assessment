<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/25/18
 * Time: 10:01
 */

function wp_ajax_post_remove_target_register( $params ){
    global $wpdb;
    require_once THEME_DIR . '/inc/lib-years.php';
    require_once THEME_DIR . '/inc/lib-kpis.php';
    require_once THEME_DIR . '/inc/lib-users.php';
    $params = wp_slash( $params );
    $kpiID = $params['id'];
    $result = kpi_get_kpi_by_id($kpiID);
    $user = wp_get_current_user();
    #$orgchart = user_load_orgchart($user);
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
        $httpCode = 401;
        send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
    }
    if( empty( $result ) ){
        $message = __('Không tìm thấy KPI', TPL_DOMAIN_LANG );
        $httpCode = 404;
        send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
    }
    $yearID = $result['year_id'];
    $year = kpi_get_year_by_id( $yearID );
    if( empty( $year ) ){
        $message = __('Không tìm thấy năm', TPL_DOMAIN_LANG );
        $httpCode = 404;
        send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
    }
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableKpi = "{$prefix}kpis";
    $tableKpiYear = "{$prefix}kpi_years";
    $wpdb->query("START TRANSACTION;");
    $wpdb->delete( $tableKpiYear, ['id' => $yearID] );
    if( !empty( $wpdb->last_error ) ){
        $message = __('Đã xảy ra lỗi. Vui lòng thử lại', TPL_DOMAIN_LANG );
        $httpCode = 404;
        $wpdb->query("ROLLBACK;");
        send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
    }else{
        $wpdb->delete( $tableKpi, ['id' => $kpiID] );
        if( !empty( $wpdb->last_error ) ){
            $message = __('Đã xảy ra lỗi. Vui lòng thử lại', TPL_DOMAIN_LANG );
            $httpCode = 404;
            $wpdb->query("ROLLBACK;");
            send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
        }
    }
    $wpdb->query("COMMIT;");
    $message = __('Thành công', TPL_DOMAIN_LANG );
    $httpCode = 201;
    send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
}