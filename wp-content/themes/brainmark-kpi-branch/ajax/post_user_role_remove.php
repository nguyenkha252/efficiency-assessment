<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/14/18
 * Time: 23:19
 */

function wp_ajax_post_user_role_remove($params) {
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableUsers = "{$prefix}users";
    $tableUserMeta = "{$prefix}usermeta";
    $sql = $wpdb->prepare("SELECT u.ID, u.display_name FROM {$tableUsers} as u WHERE (u.deleted = %d) AND (u.chart_id IS NULL OR u.chart_id = %d)", 0, 0);
    $users = $wpdb->get_results($sql);
    send_response_json($users, 202, 'OK');
}