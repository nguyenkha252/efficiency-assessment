<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/9/18
 * Time: 11:49
 */



function wp_ajax_get_kpi_list($params) {
    require_once THEME_DIR . '/inc/lib-kpis.php';

    global $wpdb;

    $results = [];
    $clauses = '';
    $last_error = '';
    $last_query = '';
    if( $params['cat'] ) {
        #$query = getKpisByCategory((int)$params['cat'], ['publish'], ['fields' => '*', 'posts_per_page' => 100, 'paged' => 1]);
	    $chartID = wp_slash( $params['cat'] );
	    $args_post = [
		    'posts_per_page' => '-1',
		    'order' => 'ASC',
		    'orderby' => 'ID',
		    'post_type' => 'post',
		    'post_status' => ['draft', 'publish']
	    ];
	    add_filter('posts_clauses_request', function($pieces, $wpquery) use ($chartID){
		    if( array_key_exists('where', $pieces) ){
			    global $wpdb;
			    $pieces['where'] .= $wpdb->prepare(" AND {$wpdb->posts}.chart_id = %d", $chartID);
		    }
		    return $pieces;
	    }, 10, 2);
	    $query = new WP_Query($args_post);
        foreach($query->posts as $post) {
            $results[$post->ID] = ['ID' => $post->ID, 'parent' => $post->post_parent, 'title' => $post->post_title, 'name' => $post->post_name];
        }
        $tax_query = $query->tax_query; /* @var $tax_query WP_Tax_Query */
        $clauses = $tax_query ? $tax_query->get_sql($wpdb->posts, 'ID') : '';
        $last_query = $wpdb->last_query;
        $last_error = $wpdb->last_error;
    }
    send_response_json(['items' => $results, 'query' => $last_query, 'error' => $last_error, 'clauses' => $clauses], 202);
}