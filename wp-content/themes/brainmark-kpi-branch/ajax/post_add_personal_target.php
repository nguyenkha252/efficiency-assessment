<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/17/18
 * Time: 17:12
 */

require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-orgchart.php';

function wp_ajax_post_add_personal_target( $params ){
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableKpi = "{$prefix}kpis";
    $tableKpiYear = "{$prefix}kpi_years";
    $user = wp_get_current_user();
    $orgchart = user_load_orgchart($user);
    $params = wp_slash($params);
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
        $httpCode = 401;
        send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
    }
    $nodeEnd = check_user_is_node_end();
    #get_kpi_by_conditions( $postID, $chartID, $yearID, $type )
    #$result = get_kpi_by_conditions( $bank_id, $params['chart_id'], $params['year_id'], $params['type'] );
    $resultKPI = kpi_get_kpi_by_id( $params['id'] );
    if( !empty($resultKPI ) ) {
        if ($resultKPI['status'] != KPI_STATUS_DRAFT && $nodeEnd) {
            $message = __('KPI đã gửi xét duyệt không được sửa', TPL_DOMAIN_LANG);
            $httpCode = 403;
            send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
        } elseif (in_array($resultKPI['status'], ['ket-qua-kpi', 'cho-duyet-kq-kpi', 'hoan-tat']) && $nodeEnd) {
            $message = __('KPI đã triển khai không được sửa', TPL_DOMAIN_LANG);
            $httpCode = 403;
            send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
        }
    }
    #print_r( $params );exit;
	$bank_id = $params['bank'];
	$post = get_post($bank_id);
	add_filter('wp_insert_post_data', function($data, $postarr) {
		if( isset($postarr['formula_id']) ) {
			$data['formula_id'] = $postarr['formula_id'];
		}
		if( isset($postarr['chart_id']) ) {
			$data['chart_id'] = $postarr['chart_id'];
		}
		return $data;
	}, 10, 2 );
	if( !$post ){
		if( $bank_id && array_key_exists('offer', $params) ){
			$sqlGetPost = $wpdb->prepare("
					SELECT * 
					FROM {$wpdb->posts} AS p
					WHERE p.post_title = %s AND p.post_author = %d AND p.chart_id = %d 
					AND p.post_parent = 0 AND p.post_type = 'post' 
			", "Đề xuất", $user->ID, $orgchart->id);
			$resultBank = $wpdb->get_row($sqlGetPost);
			if( !empty( $resultBank ) ){
				$post_ID = $resultBank->ID;
			}else{
				$post_ID = wp_insert_post(['post_title' => 'Đề xuất', 'post_status' => 'publish', "chart_id" => $orgchart->id ]);
			}
			$dataChildren = [
				"post_status" => "draft",
				"post_title" => "",
				"post_parent" => 0,
				"meta_input" => ["target" => ""],
				#"post_category" => [$params['cat']],
				"chart_id" => $orgchart->id
			];
			$offer = $params['offer'];
			$formula_id = !empty( $offer['formula_id'] ) ? $offer['formula_id'] : 0;
			$post_content = $offer['post_content'];
			$post_title = $offer['post_title'];
			$dataChildren["post_title"] = str_replace("&#8211;", "-", $post_title );
			$dataChildren['meta_input']['target'] = $post_content;
			$dataChildren["post_parent"] = (int)$post_ID;
			$dataChildren['formula_id'] = $formula_id;

			$bank_id = wp_insert_post($dataChildren);
		}
		#$message = __('Vui lòng chọn kpi để áp dụng', TPL_DOMAIN_LANG );
		#$httpCode = 404;
		#send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
	}
	if( empty($resultKPI) ){
	    #insert kpi for user
		$wpdb->query("START TRANSACTION;");
        $receive = kpi_mysql_date( $params['receive'] );
        $createTime = date('Y-m-d H:i:s', time());
        #$dataYear = ['year' => $resultYEAR['year'], 'kpi_time' => $resultYEAR['kpi_time'], 'kpi_type' => $resultYEAR['kpi_type'], 'status' => $resultYEAR['status'], 'parent' => $resultYEAR['id'], 'created' => $createTime ];
        $dataYear = ['year' => $params['time_year'], 'kpi_time' => $params['kpi_time'], 'kpi_type' => 'congty', 'status' => 'publish', 'parent' => 0, 'chart_id' => $orgchart->id, 'created' => $createTime ];
        $wpdb->insert($tableKpiYear, $dataYear);
        if( !empty( $wpdb->last_error ) ){
            $wpdb->query("ROLLBACK;");
            $message = __('Đã xảy ra lỗi vui lòng thử lại', TPL_DOMAIN_LANG );
            $httpCode = 415;
            send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
        }else{
            $dataKPI = ['bank_id' => $bank_id, 'plan' => $params['plan'], 'unit' => $params['unit'], 'parent' => 0, 'chart_id' => $orgchart->id, 'user_id' => $user->ID, 'type' => '', 'status' => KPI_STATUS_DRAFT, 'receive' => $receive, 'required' => 'no', 'percent' => $params['percent'], 'created' => $createTime, 'create_by_node' => KPI_CREATE_BY_NODE_END, 'owner' => 'yes' ];
	        $idYear = $wpdb->insert_id;
            $dataKPI['year_id'] = $idYear;
            $wpdb->insert( $tableKpi, $dataKPI );
            if( !empty( $wpdb->last_error ) ){
                $wpdb->query("ROLLBACK;");
                $message = __('Đã xảy ra lỗi vui lòng thử lại', TPL_DOMAIN_LANG );
                $httpCode = 415;
                send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
            }
        }
    }else{
	    #update kpi for user
        $status = $nodeEnd ? KPI_STATUS_DRAFT : $resultKPI['status'];
        $dataYear = ['kpi_time' => $params['kpi_time'] ];
        $wpdb->update($tableKpiYear, $dataYear, ['id' => $resultKPI['year_id'] ]);
        if( !empty( $wpdb->last_error ) ){
            $wpdb->query("ROLLBACK;");
            $message = __('Đã xảy ra lỗi vui lòng thử lại', TPL_DOMAIN_LANG );
            $httpCode = 415;
            send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
        }
        $dataKPI = ['bank_id' => $bank_id, 'plan' => $params['plan'], 'unit' => $params['unit'], 'parent' => 0, 'type' => '', 'status' => $status, 'percent' => $params['percent'] ];
        $wpdb->update( $tableKpi, $dataKPI, [ 'id' => $resultKPI['id'] ] );
        if( !empty( $wpdb->last_error ) ){
            $message = __('Đã xảy ra lỗi vui lòng thử lại', TPL_DOMAIN_LANG );
            $httpCode = 415;
            send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
        }
    }
    $wpdb->query("COMMIT;");
    $message = __('Lưu thành công', TPL_DOMAIN_LANG );
    $httpCode = 201;
    send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
}