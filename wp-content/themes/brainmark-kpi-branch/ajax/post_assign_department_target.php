<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/9/18
 * Time: 13:58
 */


function wp_ajax_post_assign_department_target($params) {
    global $wpdb;
    require_once THEME_DIR . '/inc/lib-orgchart.php';
    require_once THEME_DIR . '/inc/lib-kpis.php';
    require_once THEME_DIR . '/inc/lib-users.php';

    $params = wp_slash( $params );
    $results = [];
    $user = wp_get_current_user();

    # 'time_type', 'time_value', 'departments';
    $nonce = isset($params['_wpnonce']) ? $params['_wpnonce'] : '';
    $kpiID = isset($params['kpiID']) ? (int)$params['kpiID'] : 0;

    if( !wp_verify_nonce($nonce, 'assign_kpi_target') ) {
        $msg = "Sai mã bảo vệ";
        send_response_json(['code' => 403, 'message' => $msg], 403, $msg);
    }

    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableKpi = "{$prefix}kpis";
    $tableKpiYear = "{$prefix}kpi_years";

    $resultKPI = kpi_get_kpi_by_id( $kpiID );
    if( empty($resultKPI) || is_wp_error($resultKPI) ) {
        $msg = "Không tìm thấy KPI";
        send_response_json(['code' => 404, 'message' => $msg], 404, $msg);
    }

    $dataKPI['parent']     = $resultKPI['id'];
    $dataKPI['aproved']    = 1;
    $dataKPI['status']     = KPI_STATUS_RESULT;
    $dataKPI['required']   = $resultKPI['required'];
    $dataKPI['create_by_node'] = $resultKPI['create_by_node'];
    $dataKPI['year_id'] = $resultKPI['year_id'];
    $dataKPI['bank_id'] = $resultKPI['bank_id'];
    $dataKPI['unit'] = $resultKPI['unit'];
    $dataKPI['receive'] = $resultKPI['receive'];
    $dataKPI['created']    = date('Y-m-d H:i:s', time());

    $errorList = [];
    $successList = [];
    $old_kpis = [];
    $rows = get_kpi_by_parent( $kpiID );
    foreach ($rows as $i => &$item ) {
        $old_kpis[$item['id']] = &$item;
    }
    $wpdb->query('START TRANSACTION;');
    if( !empty($params['chuc_danh']) ) {
        foreach ($params['chuc_danh'] as $dep_id => $dep_data) {
            if( !empty($dep_data['id']) ) {
                $users = kpi_get_all_users(['orgchart_id' => $dep_id]);
                foreach ($users as $u) {
	                if( $resultKPI['user_id'] == $u['ID'] ){
                        $dataKPI['owner'] = 'yes';
                    }else{
                        $dataKPI['owner'] = 'no';
                        $dataKPI['required'] = 'yes';
	                }

	                $dep_insert = $dataKPI;
                    if( isset($dep_insert['id']) ) {
                        unset($dep_insert['id']);
                    }
                    $dep_insert['user_id']  = $u['ID'];
                    $dep_insert['chart_id'] = $dep_data['id'];
                    $dep_insert['plan']     = $dep_data['plan'];
                    $dep_insert['percent']  = doubleval($dep_data['percent']);

                    # $dep_insert['aproved']  = 1;
                    # $dep_insert['parent']   = $kpiID;
                    # $dep_insert['status']   = KPI_STATUS_RESULT;

                    $child_kpi_id = $wpdb->get_var($wpdb->prepare("SELECT k.id FROM `{$tableKpi}` as k " .
                        "WHERE (k.`parent` = %d) AND (k.`type` LIKE %s) AND (k.`user_id` = %d) ".
                        "AND (k.`chart_id` = %d) AND (k.`year_id` = %d) AND (k.bank_id = %d)",
                        $kpiID, $dep_insert['type'], $u['ID'], $dep_id, $dep_insert['year_id'], $dep_insert['bank_id']));

                    if (!empty($child_kpi_id)) {
                        if( isset($old_kpis[$child_kpi_id]) ) {
                            unset($old_kpis[$child_kpi_id]);
                        }
                        $id = $wpdb->update($tableKpi, $dep_insert, ['id' => $child_kpi_id]);
                        if( $wpdb->last_error && (strpos($wpdb->last_error, "Duplicate entry") === false) ) {
                            $errorList["{$kpiID}_{$dep_insert['user_id']}_{$dep_insert['chart_id']}"] = __LINE__ . "\n{$wpdb->last_error}\n{$wpdb->last_query}";
                        } else {
                            $successList["{$kpiID}_{$dep_insert['user_id']}_{$dep_insert['chart_id']}"] = "Update KPI con thành công";
                        }
                    } else {
                        $id = $wpdb->insert($tableKpi, $dep_insert);
                        if( $wpdb->last_error && (strpos($wpdb->last_error, "Duplicate entry") === false) ) {
                            $errorList["{$kpiID}_{$dep_insert['user_id']}_{$dep_insert['chart_id']}"] = __LINE__ . "\n{$wpdb->last_error}\n{$wpdb->last_query}";
                        } else {
                            $successList["{$kpiID}_{$dep_insert['user_id']}_{$dep_insert['chart_id']}"] = "Thêm KPI con thành công";
                        }
                    }
                }
            }
            if( isset($old_kpis[$dep_id]) ) {
                unset($old_kpis[$dep_id]);
            }
        }
    }

    if( !empty($old_kpis) ) {
        $delIDs = array_keys($old_kpis);
        # @TODO get children of kpi this
        # delete all kpi this
        foreach($old_kpis as $parent_id => $dep) {
            $resultsChilds = get_kpi_by_parent($parent_id);
            if( !empty($resultsChilds) ) {
                foreach ( $resultsChilds as $k => $child ){
                    $delIDs[] = $child['id'];
                }
            }
        }
        if( !empty( $delIDs ) ){
            $delIDs = implode(", ", $delIDs);
            $wpdb->query("DELETE FROM {$tableKpi} WHERE id IN ({$delIDs})");
            if( $wpdb->last_error ) {
                $errorList["{$delIDs}"] = "{$wpdb->last_error}\n{$wpdb->last_query}";
            } else {
                $successList["{$delIDs}"] = "Xóa KPI con thành công";
            }
        }
    }
    if( empty( $errorList ) ){
        $wpdb->query('COMMIT;');
        $msg = __('Lưu thành công', TPL_DOMAIN_LANG);
        $httpCode = 201;
    } else {
        $wpdb->query('rollback;');
        $msg = "Có lỗi xảy ra khi lưu KPI";
        $httpCode = 400;
    }

    $result_kpi = $resultKpi['id'];
    if (!empty($wpdb->last_error)) {
        $errorList[$result_kpi] = __LINE__ . "\n{$wpdb->last_error}\n{$wpdb->last_query}";
    } else {
        $successList[$result_kpi] = "Cập nhật thành công";
    }

    if (empty($errorList)) {
        $wpdb->query('COMMIT;');
        $msg = __('Lưu thành công', TPL_DOMAIN_LANG);
        $httpCode = 201;
    } else {
        $wpdb->query('rollback;');
        $msg = "Có lỗi xảy ra khi lưu KPI";
        $httpCode = 400;
    }
    send_response_json(['code' => $httpCode, 'message' => $msg, 'success' => $successList, 'error' => $errorList], $httpCode, $msg);
}
