<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/12/17
 * Time: 00:21
 */

require_once THEME_DIR . '/inc/lib-users.php';

function wp_ajax_post_create_user( $params ) {
    $user = wp_get_current_user();
    if( !wp_verify_nonce($params['_wpnonce'], 'create_user') ) {
        # return send_response_json(new WP_Error("invalid_nonce", __("Nonce invalid", TPL_DOMAIN_LANG)), 401);
    }
    if( empty($user->allcaps['create_users']) && !$user->has_cap('create_users') ) {
        # send_response_json(new WP_Error("invalid_permission", __("Not allow user permission", TPL_DOMAIN_LANG)), 402);
    }

    $user = kpi_user_create_new($params);
    if( is_wp_error($user) ) {
        # $user->add('user', $userData);
        send_response_json($user, 400);
    } else {
        global $wpdb;
        $prefix = $wpdb->get_blog_prefix($wpdb->blogid);
        $user_role = "{$prefix}user_level";
        $user = new WP_User($user);
	    user_send_mail_user( $user, $params['user_pass'] );
        $jsonData = [
            'id' => $user->ID,
            'name' => "{$user->first_name} {$user->last_name}",
            'parent' => empty($user->user_parent) ? 0 : $user->user_parent,
            'user_email' => $user->user_email,
            'user_login' => $user->user_login,
            'display_name' => $user->display_name,
            'avatar' => $user->user_url,
            'role' => $user->$user_role,
            'description' => !empty($user->description) ? $user->description : '',
            'cls' => empty($user->user_parent) ? 'full' : '',
            'childCls' => 'clearfix float'
        ];
        send_response_json($jsonData, 201);
    }
}