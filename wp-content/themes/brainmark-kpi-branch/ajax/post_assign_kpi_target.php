<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/9/18
 * Time: 13:58
 */


function wp_ajax_post_assign_kpi_target($params) {
    global $wpdb;
    require_once THEME_DIR . '/inc/lib-orgchart.php';
    $params = wp_slash( $params );
    $results = [];
    $user = wp_get_current_user();
    $keys = ['type', 'year_id', 'bank_id', 'plan', 'receive', 'percent', 'unit', 'chart_id'];
    # 'time_type', 'time_value', 'departments';
    $nonce = isset($params['_wpnonce']) ? $params['_wpnonce'] : '';
    $kpiID = isset($params['kpiID']) ? $params['kpiID'] : 0;

    if( !wp_verify_nonce($nonce, 'assign_target') ) {
        $msg = "Sai mã bảo vệ";
        send_response_json(['code' => 403, 'message' => $msg], 403, $msg);
    }

    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableKpi = "{$prefix}kpis";
    $tableKpiYear = "{$prefix}kpi_years";

    if( empty($params['year_id']) || !is_numeric($params['year_id']) ) {
        $msg = "Tạo trọng số cho từng loại mục tiêu trước khi triển khai";
        send_response_json(['code' => 400, 'message' => $msg], 400, $msg);
    }

    # $year_id = intval($params['year_id']); # $wpdb->get_var($wpdb->prepare("SELECT id FROM {$tableKpiYear} WHERE id = %d", $params['year_id']) );
    if( empty($params['year_id']) ) {
        $msg = "KPI cho năm {$params['time_value']} không tồn tại";
        send_response_json(['code' => 400, 'message' => $msg], 400, $msg);
    }
    $year_id = intval($params['year_id']);

    $data = [];
    foreach($keys as $k) {
        $data[$k] = isset($params[$k]) ? $params[$k] : null;
    }
    if( !in_array($params['time_type'], ['year', 'precious', 'month']) ) {
        $params['time_type'] = 'year';
    }
    $data['receive'] = kpi_mysql_date($params['receive']);

    # $data[$params['time_type']] = $params['time_value'];
    $data['parent'] = 0;

    $data['user_id'] = $user->ID;
    $data['aproved'] = 1;
    $data['status'] = KPI_STATUS_RESULT;
    $data['required'] = 'yes';
    $data['created'] = date('Y-m-d H:i:s', time());

    if( !empty($params['chuc_danh']) ) {
        foreach ($params['chuc_danh'] as $dep_id => $dep) {
            if (!isset($dep['id']) || !isset($dep['plan'])) {
                unset($params['chuc_danh'][$dep_id]);
            }
        }
    }

    if( empty($params['chuc_danh']) ) {
        $msg = __("Bạn chưa chọn chức danh hoặc chưa điền trọng số nào cả", TPL_DOMAIN_LANG);
        send_response_json(['code' => 400, 'message' => $msg], 400, $msg);
    }
    $errorList = [];
    $successList = [];

    # update kpi
    $kpi_id = $wpdb->get_var($wpdb->prepare("SELECT k.id FROM `{$tableKpi}` as k " .
        "WHERE (k.`parent` = 0) AND (k.`type` LIKE %s) AND (k.`year_id` = %d) AND (k.bank_id = %d)",
        $data['type'], $year_id, $data['bank_id']));

    $ids = $wpdb->get_col($wpdb->prepare("SELECT k.id FROM {$tableKpi} as k " .
        "WHERE k.`parent` IN ( " .
        "   SELECT k.id FROM {$tableKpi} as k " .
        "   WHERE k.`parent` = %d)", $kpi_id), ARRAY_A);

    if( !empty($ids) ) {
        $_ids = $ids;
        $where_ids = implode(', ', $ids);
        $where_ids = "( k.`parent` IN ({$where_ids}) )";

        $_ids = $wpdb->get_col("SELECT k.id FROM {$tableKpi} as k " .
            "WHERE k.`parent` IN ( " .
            "   SELECT k.id FROM {$tableKpi} as k " .
            "   WHERE $where_ids)", ARRAY_A);
        if( !empty($_ids) ) {
            $ids = array_merge($ids, $_ids);
        }
    }
    $where_ids = '';
    if (!empty($ids)) {
        $where_ids = implode(', ', $ids);
        $where_ids = "OR (`id` IN ({$where_ids}) )";
    }

    require_once THEME_DIR . '/inc/lib-users.php';

    # Remove for all NhanVien
    # Remove for all PhongBan
    # $wpdb->query("DELETE FROM {$tableKpi} WHERE (`parent` = %d) {$where_ids}", $kpi_id);

    if( !empty($kpiID) ) {
        require_once THEME_DIR . '/inc/lib-kpis.php';
        $resultKpi = kpi_get_kpi_by_id( $kpiID );
        extract($params);
        $data = compact('bank_id', 'plan', 'unit', 'receive', 'percent');
        $data['receive'] = kpi_mysql_date($data['receive']);
        $result_kpi = 0;
        if( !empty( $resultKpi ) && !is_wp_error( $resultKpi ) ) {
            $wpdb->query('START TRANSACTION;');
            $result_kpi = $resultKpi['id'];
            $parent = $wpdb->update($tableKpi, $data, ['id' => $result_kpi]);
            if( !empty( $wpdb->last_error ) ) {
                $errorList[$result_kpi] = __LINE__ . "\n{$wpdb->last_error}\n{$wpdb->last_query}";
            } else {
                $successList[$result_kpi] = "Cập nhật thành công";
            }
            $old_kpis = [];
            $rows = get_kpi_by_parent( $kpiID );
            foreach ($rows as $i => &$item ) {
                $old_kpis[$item['id']] = &$item;
            }

            if( !empty($params['chuc_danh']) ) {
                foreach ($params['chuc_danh'] as $dep_id => $dep_data) {
                    if( !empty($dep_data['id']) ) {
                        $users = kpi_get_all_users(['orgchart_id' => $dep_id]);
                        foreach ($users as $u) {
                            $dep_insert = $data;
                            if( isset($dep_insert['id']) ) {
                                unset($dep_insert['id']);
                            }
                            $dep_insert['year_id']  = $year_id;
                            $dep_insert['user_id']  = $u['ID'];
                            $dep_insert['chart_id'] = $dep_data['id'];
                            $dep_insert['plan']     = $dep_data['plan'];
                            $dep_insert['aproved']  = 1;
                            $dep_insert['percent']  = doubleval($dep_data['percent']);
                            $dep_insert['parent']   = $result_kpi;
                            $dep_insert['status']   = KPI_STATUS_RESULT;

                            $child_kpi_id = $wpdb->get_var($wpdb->prepare("SELECT k.id FROM `{$tableKpi}` as k " .
                                "WHERE (k.`parent` = %d) AND (k.`type` LIKE %s) AND (k.`user_id` = %d) ".
                                "AND (k.`chart_id` = %d) AND (k.`year_id` = %d) AND (k.bank_id = %d)",
                                $result_kpi, $params['type'], $u['ID'], $dep_id, $year_id, $data['bank_id']));

                            if (!empty($child_kpi_id)) {
                                if( isset($old_kpis[$child_kpi_id]) ) {
                                    unset($old_kpis[$child_kpi_id]);
                                }
                                $id = $wpdb->update($tableKpi, $dep_insert, ['id' => $child_kpi_id]);
                                if( $wpdb->last_error && (strpos($wpdb->last_error, "Duplicate entry") === false) ) {
                                    $errorList["{$result_kpi}_{$dep_insert['user_id']}_{$dep_insert['chart_id']}"] = __LINE__ . "\n{$wpdb->last_error}\n{$wpdb->last_query}";
                                } else {
                                    $successList["{$result_kpi}_{$dep_insert['user_id']}_{$dep_insert['chart_id']}"] = "Update KPI con thành công";
                                }
                            } else {
                                $id = $wpdb->insert($tableKpi, $dep_insert);
                                if( $wpdb->last_error && (strpos($wpdb->last_error, "Duplicate entry") === false) ) {
                                    $errorList["{$result_kpi}_{$dep_insert['user_id']}_{$dep_insert['chart_id']}"] = __LINE__ . "\n{$wpdb->last_error}\n{$wpdb->last_query}";
                                } else {
                                    $successList["{$result_kpi}_{$dep_insert['user_id']}_{$dep_insert['chart_id']}"] = "Thêm KPI con thành công";
                                }
                            }
                        }
                    }
                    if( isset($old_kpis[$dep_id]) ) {
                        unset($old_kpis[$dep_id]);
                    }
                }
            }

            if( !empty($old_kpis) ) {
                $delIDs = array_keys($old_kpis);
                # @TODO get children of kpi this
                # delete all kpi this
                foreach($old_kpis as $parent_id => $dep) {
                    $resultsChilds = get_kpi_by_parent($parent_id);
                    if( !empty($resultsChilds) ) {
                        foreach ( $resultsChilds as $k => $child ){
                            $delIDs[] = $child['id'];
                        }
                    }
                }
                if( !empty( $delIDs ) ){
                    $delIDs = implode(", ", $delIDs);
                    $wpdb->query("DELETE FROM {$tableKpi} WHERE id IN ({$delIDs})");
                    if( $wpdb->last_error ) {
                        $errorList["{$delIDs}"] = "{$wpdb->last_error}\n{$wpdb->last_query}";
                    } else {
                        $successList["{$delIDs}"] = "Delete KPI con thành công";
                    }
                }
            }
            if( empty( $errorList ) ){
                $wpdb->query('COMMIT;');
                $msg = __('Lưu thành công', TPL_DOMAIN_LANG);
                $httpCode = 201;
            } else {
                $wpdb->query('rollback;');
                $msg = "Có lỗi xảy ra khi lưu KPI";
                $httpCode = 400;
            }
        } else {
            $msg = __("Không tìm thấy KPI", TPL_DOMAIN_LANG);
            $httpCode = 404;
        }
        send_response_json(['code' => $httpCode, 'message' => $msg, 'success' => $successList, 'error' => $errorList], $httpCode, $msg);
    } else {
        #insert kpi
        $wpdb->query("START TRANSACTION;");
        # CEO Insert KPI
        if( array_key_exists('kpiID', $data) ) {
            unset($data['kpiID']);
        }
        # status_header( 205, 'Debug' );
        $errorList = [];
        $successList = [];
        $res_id = $wpdb->insert($tableKpi, $data);
        $insert_id = $wpdb->insert_id;

        if( $wpdb->last_error ) {
            if( (strpos($wpdb->last_error, "Duplicate entry") !== false) ) {
                $insert_id = $wpdb->get_var($wpdb->prepare("SELECT id FROM $tableKpi ".
                    "WHERE (`year_id` = %d) AND (`bank_id` = %d) AND (`chart_id` = %d) ".
                    "AND (`user_id` = %d) AND (`parent` = %d) AND (`type` = %s)",
                    $year_id, $data['bank_id'], $data['chart_id'], $data['user_id'],
                    $data['parent'], $data['type']));
                $res_id = 0;
            } else {
                $errorList[] = __LINE__ . "\n{$wpdb->last_error}\n{$wpdb->last_query}";
            }
        } else {

            $successList[$insert_id] = "Thêm KPI thành công";
        }

        if( $res_id !== false ) {
            $old_kpis = [];
            $rows = get_kpi_by_parent( $insert_id );
            foreach ($rows as $i => &$item ) {
                $old_kpis[$item['id']] = &$item;
            }
            if( !empty($params['chuc_danh']) ) {
                foreach ($params['chuc_danh'] as $dep_id => $dep_data) {
                    if( !empty($dep_data['id']) ) {
                        $users = kpi_get_all_users(['orgchart_id' => $dep_id]);
                        foreach ($users as $u) {
                            $dep_insert = $data;
                            if (isset($dep_insert['id'])) {
                                unset($dep_insert['id']);
                            }
                            $dep_insert['year_id']  = $year_id;
                            $dep_insert['user_id']  = $u['ID'];
                            $dep_insert['chart_id'] = $dep_data['id'];
                            $dep_insert['plan']     = $dep_data['plan'];
                            $dep_insert['aproved']  = 1;
                            $dep_insert['percent']  = doubleval($dep_data['percent']);
                            $dep_insert['parent']   = $insert_id;
                            $dep_insert['status']   = KPI_STATUS_RESULT;

                            $child_kpi_id = $wpdb->get_var($wpdb->prepare("SELECT k.id FROM `{$tableKpi}` as k " .
                                "WHERE (k.`parent` = %d) AND (k.`type` LIKE %s) AND (k.`user_id` = %d) " .
                                "AND (k.`chart_id` = %d) AND (k.`year_id` = %d) AND (k.bank_id = %d)",
                                $insert_id, $params['type'], $u['ID'], $dep_id, $year_id, $data['bank_id']));

                            if (!empty($child_kpi_id)) {
                                if( isset($old_kpis[$child_kpi_id]) ) {
                                    unset($old_kpis[$child_kpi_id]);
                                }
                                $id = $wpdb->update($tableKpi, $dep_insert, ['id' => $child_kpi_id]);
                                if( $wpdb->last_error && (strpos($wpdb->last_error, "Duplicate entry") === false) ) {
                                    $errorList["{$insert_id}_{$dep_insert['user_id']}_{$dep_insert['chart_id']}_{$child_kpi_id}"] = __LINE__ . "\n{$wpdb->last_error}\n{$wpdb->last_query}";
                                } else {
                                    $successList["{$insert_id}_{$dep_insert['user_id']}_{$dep_insert['chart_id']}_{$child_kpi_id}"] = "Update KPI con thành công";
                                }
                            } else {
                                $id = $wpdb->insert($tableKpi, $dep_insert);
                                if( $wpdb->last_error && (strpos($wpdb->last_error, "Duplicate entry") === false) ) {
                                    $errorList["{$insert_id}_{$dep_insert['user_id']}_{$dep_insert['chart_id']}"] = __LINE__ . "\n{$wpdb->last_error}\n{$wpdb->last_query}";
                                } else {
                                    $successList["{$insert_id}_{$dep_insert['user_id']}_{$dep_insert['chart_id']}"] = "Thêm KPI con thành công";
                                }
                            }
                        }
                    }
                }
            }
        }

        if( !empty($errorList) ) {
            $wpdb->query("rollback;");
            $msg = "Có lỗi xảy ra khi thêm KPI";
            send_response_json(['code' => 400, 'message' => $msg, 'success' => $successList, 'errors' => $errorList, 'success' => $successList], 400, $msg);
        }

        $wpdb->query("COMMIT;");
        $msg = "Thêm KPI thành công";
        send_response_json(['code' => 201, 'message' => $msg, 'success' => $successList], 202, $msg);
    }
}
