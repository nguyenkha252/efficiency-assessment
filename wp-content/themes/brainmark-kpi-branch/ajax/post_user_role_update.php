<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/14/18
 * Time: 23:19
 */

function wp_ajax_post_user_role_update($params) {
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableUsers = "{$prefix}users";
    $tableUserMeta = "{$prefix}usermeta";
    $tableCharts = "{$prefix}kpi_charts";
    $keys = ['assign_task' => 0, 'user_id' => 0, 'role_id' => 0, 'parent' => 0, 'x' => 0, 'y' => 0, 'is_primary' => 0, 'level' => 0, 'siblings' => '[]'];
    /*
    $params['assign_task'];
    $params['chuc_danh_truong']; # chuc danh id
    $params['chuc_danh_truong_old']; # chuc danh id
    $params['ten_chuc_danh_truong']; # user id
    $params['ten']; # chuc danh name

    $params['chuc_danh_pho'][0]['ten']; # chuc danh id
    $params['chuc_danh_pho'][0]['ten_old']; # old chuc danh id
    $params['chuc_danh_pho'][0]['user']; # user full name
    $params['chuc_danh_pho'][0]['user_id']; # user id
    $params['chuc_danh_pho'][0]['user_id_old']; # old user id
    */
    $insertData = $keys;
    $parent_id = isset($params['ID']) ? (int)$params['ID'] : 0;
    $insertData['assign_task']  = isset($params['assign_task']) ? intval($params['assign_task']) : 0;
    $insertData['user_id']      = (int)$params['ten_chuc_danh_truong'];
    $insertData['role_id']      = (int)$params['chuc_danh_truong'];
    $insertData['is_primary']   = 1;
    $insertData['parent']        = (int)$params['parent'];
    $insertData['level']        = (int)$params['level'];

    # $insertData['level_parent'] = $params['level_parent'];
    $errors = [];

    $params['ten']              = strip_tags($params['ten']);
    $insertData['name']         = $params['ten'];
    # $wpdb->query("START TRANSACTION;");
    $success = [];
    $id = $wpdb->update($tableCharts, $insertData, ['ID' => $parent_id]);
    if( is_numeric($id) ) {
        $d                  = $insertData;
        $d['siblings']      = [];
        $d['ID']            = $parent_id;
        $success[]          = $d;
        $insertData['is_primary']   = 0;
        $insertData['parent']       = $parent_id;
        $siblings = [];
        if( !empty($params['chuc_danh_pho']) ) {
            $insertData['name']        = $params['ten'] . " (phó)";
            $insertData['assign_task'] = 0;
            foreach($params['chuc_danh_pho'] as $info) {
                $insertData['user_id'] = (int)$info['user_id'];
                $insertData['role_id'] = (int)$info['ten'];
                if( empty($info['ID']) ) {
                    $info['ID'] = 0;
                } else {
                    $info['ID'] = (int)$info['ID'];
                }
                if( empty($info['user_id_old']) ) {
                    $info['user_id_old'] = 0;
                }
                if( empty($info['ten_old']) ) {
                    $info['ten_old'] = 0;
                }
                if( $info['ID'] > 0 ) {
                    $row_id = $wpdb->get_var($wpdb->prepare("SELECT ID FROM {$tableCharts} WHERE `ID` = %d ", $info['ID'] ) );
                } else {
                    $row_id = null;
                }
                if( !empty($row_id) ) {
                    # Update
                    $id = $wpdb->update($tableCharts, $insertData, ['ID' => $info['ID']]);
                    $insert_id = $info['ID'];
                } else {
                    # Insert
                    $id = $wpdb->insert($tableCharts, $insertData);
                    $insert_id = (int)$wpdb->insert_id;
                }

                if( is_numeric($id) ) {
                    $d = $insertData;
                    $d['ID']        = $insert_id;
                    $success[]      = $d;
                    $siblings[]     = $insert_id;
                } else if($wpdb->last_error) {
                    if( strpos($wpdb->last_error, 'Duplicate entry') === false ) {
                        $errors[] = $wpdb->last_error;
                    } else {
                        $row = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$tableCharts} ".
                            "WHERE `parent` = %d AND `user_id` = %d AND role_id = %d",
                            $parent_id, $insertData['user_id'], $insertData['role_id']) );
                        $success[] = $row;
                        if( !empty($row) && !in_array((int)$row['ID'], $siblings) ) {
                            $siblings[] = (int)$row['ID'];
                        }
                        $d          = $insertData;
                        $d['ID']    = $row['ID'];
                    }
                }
            }
        }
        if( !empty($siblings) ) {
            $wpdb->update($tableCharts, ['siblings' => json_encode($siblings)], ['ID' => $parent_id]);
        }
        $success[0]['siblings'] = $siblings;
    } else if($wpdb->last_error) {
        $errors[] = $wpdb->last_error;
    }
    if( empty($errors) ) {
        # $wpdb->query("COMMIT;");
        $msg = "Tạo thành công";
        send_response_json(['code' => 201, 'items' => $success], 201, $msg);
    } else {
        # $wpdb->query("rollback;");
        $msg = "Có lỗi xảy ra";
        send_response_json(['code' => 400, 'message' => $msg, 'error' => $errors, 'items' => $success], 400, $msg);
    }
}