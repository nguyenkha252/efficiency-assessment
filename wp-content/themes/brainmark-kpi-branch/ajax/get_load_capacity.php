<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 2/7/18
 * Time: 17:08
 */
require_once THEME_DIR . '/inc/lib-kpis.php';
function wp_ajax_get_load_capacity( $params ){
    global $wpdb;
    $params = wp_slash($params);
    $user = wp_get_current_user();
    if (!$user) {
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG);
        $httpCode = 401;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }
    if (!wp_verify_nonce($params['_wpnonce'], 'load_capacity')) {
        $message = __('Mã bảo vệ không hợp lệ', TPL_DOMAIN_LANG);
        $httpCode = 403;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }
    $result = capacity_get_kpi_and_bank_by_id( $params['id'] );
    if( !empty( $result ) ){
	    $result['status'] = $result['status'] != KPI_STATUS_DRAFT ? 1 : 0;
        $httpCode = 201;
        send_response_json(['code' => $httpCode, 'data' => $result], $httpCode, "OK");
    }else{
        $msg = "Không tìm thấy năng lực quản lý";
        $httpCode = 404;
        send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
    }
}