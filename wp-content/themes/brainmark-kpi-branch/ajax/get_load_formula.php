<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/22/18
 * Time: 08:55
 */

function wp_ajax_get_load_formula( $params ){
    require_once THEME_DIR . '/inc/lib-formulas.php';
    $user = wp_get_current_user();
    $params = wp_slash( $params );
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
        $httpCode = 401;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }
    $result = get_formula_by_id( $params['ID'] );
    if( empty( $result ) ){
        $msg = __('Không tìm thấy công thức', TPL_DOMAIN_LANG);
        $httpCode = 404;
        send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
    }
    $result->note = strip_tags( $result->note );
    $result->formulas = maybe_unserialize( $result->formulas );
    $msg = __('Thành công', TPL_DOMAIN_LANG);
    $httpCode = 201;
    send_response_json(['code' => $httpCode, 'data' => $result], $httpCode, $msg);
}