<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/5/18
 * Time: 17:35
 */

function wp_ajax_post_create_group_target( $params ){

    global $wpdb;
	$user = wp_get_current_user();
    $postarr = wp_slash($params);
	if( !$user ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],401, $message);
	}
	if( !wp_verify_nonce($params['_wpnonce'], 'create_posts') ){
		$message = __('Phiên bản đã hết hạn. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],408, $message);
	}
	if( empty($user->allcaps['create_posts']) && !$user->has_cap('create_posts') ) {
		$message = __('Bạn không được cấp quyền thêm bài viết' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],402, $message);
	}
    if( empty($postarr['post_title']) ){
        $message = __('Tên nhóm mục tiêu không được để trống' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    $postarr['post_status'] = 'publish';
    $postarr['post_parent'] = 0;
    extract( $postarr );
    $data = compact( 'post_title', 'post_parent', 'post_status', 'chart_id', 'post_category' );
	add_filter('wp_insert_post_data', function($data, $arr) use( $postarr ) {
		if( isset($postarr['chart_id']) ) {
			$data['chart_id'] = $postarr['chart_id'];
		}
		return $data;
	}, 10, 2 );
    $id = wp_insert_post( $data );
    if( is_wp_error( $id ) ){
        # @TODO process error
	    $message = __('Tạo nhóm mục tiêu KPI thất bại. Vui lòng thử lại' ,TPL_DOMAIN_LANG );
	    send_response_json(['error' => $message],415, $message);
    }else{
    	$message = __('Thành công', TPL_DOMAIN_LANG);
        send_response_json(['success' => $message], 200, $message);
    }
}