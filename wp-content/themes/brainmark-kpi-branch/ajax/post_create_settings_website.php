<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/12/18
 * Time: 17:22
 */

function wp_ajax_post_create_settings_website( $params ){
    $user = wp_get_current_user();
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    if( !wp_verify_nonce($params['_wpnonce'], 'manage_options') ){
        $message = __('Phiên bản đã hết hạn. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],403, $message);
    }
    if( empty($user->allcaps['level_9']) && !$user->has_cap('level_9') ) {
        $message = __('Bạn không được cấp quyền thêm' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],402, $message);
    }
    $params = esc_sql( $params );
    $upload = wp_upload_dir();
    if( isset($_FILES['logo_url']) && !empty( $_FILES['logo_url']['name'] ) ) {
        $_FILES['logo_url']['type'];
        $_FILES['logo_url']['error'];
        $_FILES['logo_url']['size'];
        if( $_FILES['logo_url']['error'] == UPLOAD_ERR_OK && $_FILES['logo_url']['size'] > 0 ) {
            $logo = "{$upload['basedir']}/logo/{$_FILES['logo_url']['name']}";
            $params['logo_url'] = "{$upload['baseurl']}/logo/{$_FILES['logo_url']['name']}";
            if( !is_dir("{$upload['basedir']}/logo") ) {
                mkdir("{$upload['basedir']}/logo", 0777, true);
            }
            move_uploaded_file($_FILES['logo_url']['tmp_name'], $logo);
        } else {
            if( isset($params['logo_url']) ) {
                unset($params['logo_url']);
            }
        }
    }
    if( isset($params['standard_benchmark']) ){
        $params['standard_benchmark'] = str_replace("%", "", $params['standard_benchmark']);
        $params['standard_benchmark'] = (int)$params['standard_benchmark'];
    }
    extract( $params );

    if( !empty($params['title_website']) ) {
        update_option('blogname', $params['title_website']);
    }
    $data = compact('logo_url', 'title_website', 'standard_benchmark',
        'color_header', 'background_header', 'background_header_table', 'description_reach', 'description_not_reach', 'email_hr_manager', 'email_template_content', 'email_template_subject', 'color_header_table');
    $options = get_option('settings_website', true);
    if( isset($data['email_hr_manager']) && !empty($data['email_hr_manager']) ){
        $emails = explode(", ", $data['email_hr_manager']);
        foreach ($emails as $k => $item){
            if( !is_email($item) ){
                $message = __('Email không đúng định dạng vui lòng kiểm tra lại', TPL_DOMAIN_LANG);
                send_response_json(['error' => $message], 401, $message);
            }
        }
    }
    if( !empty( $options ) ){
        if( empty( $data['logo_url'] ) ){
            $data['logo_url'] = $options['logo_url'];
        }
        update_option( 'settings_website', $data );
    }else{
        add_option( 'settings_website', $data );
    }
    $message = __('Thành công', TPL_DOMAIN_LANG);
    send_response_json(['success' => $message], 200, $message);
}