<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 11/19/18
 * Time: 4:51 PM
 */


require_once THEME_DIR . '/inc/effa.php';
require_once THEME_DIR . '/inc/evaluate.php';
require_once THEME_DIR . '/inc/lib-settings.php';
require_once THEME_DIR . '/inc/vendor/autoload.php';
/*require_once THEME_DIR . '/inc/vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';
require_once THEME_DIR . '/inc/vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once THEME_DIR . '/inc/vendor/phpoffice/phpexcel/Classes/PHPExcel/Worksheet/Drawing.php';*/
function wp_ajax_get_export_effa_report_for_company($params){
    global $wpdb;
    $year = date('Y', time());
    $user = wp_get_current_user();
    $params = wp_slash($params);
    if( !user_is_manager() ){
        $message = __('Bạn không có quyền thực hiện thao tác này' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],403, $message);
    }
    if( !wp_verify_nonce($params['_wpnonce'], 'export_effa_report_for_company') ){
        $message = __('Phiên bản đã hết hạn. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],408, $message);
    }
    $com = isset($_GET['com']) ? $_GET['com'] : "";
    if( $com == "" ){
        $message = __('Vui lòng chọn báo cáo theo năng lực' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }

    $fileTemplate = THEME_DIR . '/assets/template/exports/' . COMPETENCY_EXPORT_FOR_COMPANY;
    $upload = wp_upload_dir();
    $date = date('d-m-Y-H-i-s', current_time('timestamp') );
    $file_export = explode('.', COMPETENCY_EXPORT);
    $extra = $file_export[count($file_export) - 1];
    unset($file_export[count($file_export) - 1]);
    array_push($file_export, $date, uniqid(), $extra);
    $file_export = implode(".", $file_export);
    $fileCopy = "{$upload['basedir']}/exports/" . $file_export;
    $file_download = str_replace($upload['basedir'], $upload['baseurl'], $fileCopy);
    if (!is_dir("{$upload['basedir']}/exports")) {
        mkdir("{$upload['basedir']}/exports", 0777, true);
    }
    @copy($fileTemplate, $fileCopy);
    @chmod($fileCopy, 0777);
    $identify = PHPExcel_IOFactory::identify($fileCopy);
    $objReader = PHPExcel_IOFactory::createReader($identify);
    $objPHPExcel = $objReader->load($fileTemplate);
    $activeSheet = $objPHPExcel->getActiveSheet();
    $highestColumn = $activeSheet->getHighestColumn();
    $highestColumn++;
    $settings = get_settings_website();

    $logo_webiste = !empty( $settings['logo_url'] ) ? $settings['logo_url'] : THEME_URL . '/assets/images/logo-brainmark.png';
    $title_website = !empty($settings['title_website']) ? $settings['title_website'] : __('KPI SYSTEM', TPL_DOMAIN_LANG);
    $path_logo = str_replace($upload['baseurl'], $upload['basedir'], $logo_webiste);
    if( empty($settings['logo_url']) ){
        $path_logo = str_replace(THEME_URL, THEME_DIR, $logo_webiste);
    }
    $checkPathLogo = wp_remote_get($logo_webiste);
    if($checkPathLogo['response']['code'] != 404) {
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
        $objDrawing->setPath($path_logo);
        $objDrawing->setHeight(56);
        $objDrawing->setCoordinates('A1')
            ->setOffsetX(10)
            ->setOffsetY(5);
    }
    if( $com == 'bad' ){
        $text_description = __('DANH SÁCH 5 NĂNG LỰC THIẾU ĐIỂM CAO NHẤT', TPL_DOMAIN_LANG);
    }else{
        $text_description = __('DANH SÁCH 5 NĂNG LỰC ĐẠT ĐIỂM CAO NHẤT', TPL_DOMAIN_LANG);
    }
    $activeSheet
        ->setCellValue("A1", '')
        ->setCellValue("D1", $title_website )
        ->setCellValue("D5", $text_description)
    ;


    $start_row = 7;
    $row_added = 0;
    $standard_benchmark = $settings['standard_benchmark'] ? $settings['standard_benchmark'] : 0;
    if( $com == 'bad' ){
        $reports = effa_site_reports_filter_efficiency($year, $standard_benchmark, "<");
    }else{
        $reports = effa_site_reports_filter_efficiency($year, $standard_benchmark, ">=");
    }
    if(!empty($reports)) {
        foreach ($reports as $key => $item) {
            $row_added++;
            $this_row_item = $start_row + $row_added;
            $post_title = str_replace(str_split("\|"), "", $item->post_title);
            $activeSheet->insertNewRowBefore($this_row_item, 1);
            $activeSheet->setCellValue("A{$this_row_item}", ($row_added + 1))
                ->setCellValue("B{$this_row_item}", $item->post_code)
                ->setCellValue("C{$this_row_item}", $post_title)
                ->setCellValue("F{$this_row_item}", $item->summary_post);

        }
        for ($col = "A"; $col != $highestColumn; $col++) {
            $activeSheet->duplicateStyle($activeSheet->getStyle("{$col}" . ($start_row)), "{$col}" . ($start_row + 1) . ":{$col}" . ($start_row + $row_added));
        }
        $activeSheet->removeRow($start_row, 1);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $identify);
        $objWriter->save($fileCopy);
        send_response_json(['url_download' => $file_download], 201, __('OK', TPL_DOMAIN_LANG));
    }else{
        send_response_json(['code' => 401, 'error' => __('Không có dữ liệu', TPL_DOMAIN_LANG)], 401, __('Không có dữ liệu', TPL_DOMAIN_LANG));
    }
}