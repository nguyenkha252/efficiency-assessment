<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 08/02/2018
 * Time: 01:10
 */
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-behavior.php';
function wp_ajax_post_deployment_behavior( $params ){
	global $wpdb;
	$user = wp_get_current_user();
	$orgchart = user_load_orgchart($user);
	$params = wp_slash( $params );
	if( !$user ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],401, $message);
	}
	if( !user_is_manager() ){
		$message = __('Bạn không có quyền triển khai', TPL_DOMAIN_LANG);
		$httpCode = 403;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	if( !wp_verify_nonce($params['_wpnonce'], 'deployment_behavior') ){
		$message = __('Phiên bản đã hết hạn. Vui lòng tải lại trang.' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],408, $message);
	}
	$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
	$tableKpiBehavior = "{$prefix}behavior";
	$result = behavior_get_list_behavior_of_admin_by_year( $params['year'], 0 );
	$arrBehaviorIDs = [];
	foreach ( $result as $key => $item ){
		if( $item['status'] != KPI_STATUS_RESULT ){
			$arrBehaviorIDs[] = $item['id'];
		}
	}

	if( !empty($arrBehaviorIDs) ){
		$wpdb->query("START TRANSACTION;");
		$strBehaviorID = implode(", ", $arrBehaviorIDs);
		$sql = $wpdb->prepare(" UPDATE {$tableKpiBehavior} SET status = %s WHERE id IN ({$strBehaviorID}) ", KPI_STATUS_RESULT);
		$wpdb->query($sql);
		if( !empty( $wpdb->last_error ) ){
			$msg = "Có lỗi xảy ra khi triển khai";
			$httpCode = 400;
			send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
		}
		$wpdb->query("COMMIT;");
	}
	$msg = "Thành công";
	$httpCode = 201;
	send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
}