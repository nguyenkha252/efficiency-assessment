<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 2/6/18
 * Time: 11:24
 */

require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-users.php';
$pagination_file = THEME_DIR . '/inc/paging.php';
if( file_exists($pagination_file) )
    require_once $pagination_file;

function wp_ajax_get_news_feed( $params ){
    $params = wp_slash($params);
    $paged = isset( $params['pg'] ) ? $params['pg'] : 0;
    $year = TIME_YEAR_VALUE;
    $orgchartID = 0;
    if( array_key_exists( 'uid', $params ) ){
        $userID = (int)$params['uid'];
        $getUser = kpi_get_user_by_id($userID);
        $orgchartUser = user_load_orgchart($getUser);
        $orgchartID = $orgchartUser->id;
    }
    $results = get_newsfeed( 'year', $year, $paged, 8, $orgchartID);
    $newsItem = "";
    if( !empty( $results ) && !is_wp_error( $results ) && $results['founds'] > 0 ){
        foreach ( $results['posts'] as $key => $nf ){
            $date = preg_replace('#(\d{2,4})(\/|\-)(\d{1,2})(\/|\-)(\d{1,2})[\s](\d{1,2})(\:)(\d{1,2})(\:)(\d{1,2})#', '$5/$3/$1', $nf->created);
            $newsItem .="
            <tr>
                <td class=\"date\">{$date}</td>
                <td class=\"title\">{$nf->post_title}</td>
            </tr>
            ";
        }
    }
    $pagination = "";
    if(!empty($results) && !empty($results['pages'])):
        $pagination = "<div class=\"pagination\">".$results['pages']."</div>";
    endif;
    $output = "<div class=\"header-dashboard-kpi header-block\">
            <div class=\"name-block\">
                <h2>".__('Bản tin', TPL_DOMAIN_LANG)."</h2>
                <strong class=\"connect-for-time\"> - </strong>
                <span class=\"show-for-time\">
				".__('Ngày', TPL_DOMAIN_LANG). " " .date('d/m/Y', time())."
			</span>
            </div>
        </div>
        <div class=\"content-block content-block-dashboard-kpi\">
            <table class=\"list-news-feed\" cellpadding=\"0\" cellspacing=\"0\">
                <tbody>
                {$newsItem}
                </tbody>
            </table>
		    {$pagination}
        </div>";

    send_response_json( ['code' => 201, 'html' => $output], 201, 'Thành công' );
}