<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/12/17
 * Time: 00:21
 */

require_once THEME_DIR . '/inc/lib-orgchart.php';

function wp_ajax_post_remove_orgchart( $params ) {
    if( !wp_verify_nonce($params['_wpnonce'], 'remove_orgchart') ) {
        #$error = new WP_Error(401, __('Invalid nonce when save orgchart', TPL_DOMAIN_LANG) );
        #send_response_json(['code' => $error->get_error_code(), 'message' => $error->get_error_message()], $error->get_error_code(), $error->get_error_message());
    }
    $user = wp_get_current_user();
    if( !(!empty($user->allcaps['level_9']) || $user->has_cap('level_9') ) ) {
        $error = new WP_Error(403, __('You do not permission to edit orgchart', TPL_DOMAIN_LANG) );
        send_response_json(['code' => $error->get_error_code(), 'message' => $error->get_error_message()], $error->get_error_code(), $error->get_error_message());
    }
    $orgchart_id = kpi_orgchart_remove($params['id']);
    if( is_wp_error($orgchart_id) ) {
        send_response_json(['code' => $orgchart_id->get_error_code(), 'message' => $orgchart_id->get_error_message()], $orgchart_id->get_error_code(), $error->get_error_message());
    } else {
        send_response_json($orgchart_id, 200);
    }
}