<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/14/18
 * Time: 23:19
 */

function wp_ajax_get_list_users($params) {
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableUsers = "{$prefix}users";
    $tableUserMeta = "{$prefix}usermeta";
    $tableCharts = "{$prefix}kpi_charts";



    if( !empty($params['chart_id']) ) {
        $sql = $wpdb->prepare("SELECT u.ID, u.display_name FROM {$tableUsers} as u ".
            "INNER JOIN {$tableCharts} as c ON c.user_id = u.ID AND (c.ID = %d ) ".
            "WHERE (u.deleted = 0) ", (int)$params['chart_id'] );
    } else {
        $sql = "SELECT u.ID, u.display_name FROM {$tableUsers} as u ".
            "WHERE (u.deleted = 0) AND u.ID NOT IN (".
            "  SELECT u.ID FROM {$tableUsers} as u INNER JOIN {$tableCharts} as c ON c.user_id = u.ID".
            ") ";
    }

    /*
    $sql = $wpdb->prepare("SELECT u.ID, u.display_name FROM {$tableUsers} as u ".
        "WHERE (u.deleted = %d) AND ( (u.chart_id IS NULL OR u.chart_id = %d) ".
        "OR (u.ID NOT IN (SELECT c.user_id FROM {$tableCharts} as c WHERE 1 = 1) ) )", 0, 0);
    */
    $users = $wpdb->get_results($sql);

    if( $wpdb->last_error ) {
        $msg = "Có lỗi xảy ra";
        send_response_json(['code' => 400, 'message' => $msg, 'sql' => $wpdb->last_query, 'error' => $wpdb->last_error], 400, $msg);
    }
    send_response_json(['items' => $users, 'sql' => $wpdb->last_query, 'error' => $wpdb->last_error], 202, 'OK');
}
