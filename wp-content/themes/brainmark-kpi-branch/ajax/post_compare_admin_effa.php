<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/8/18
 * Time: 2:44 PM
 */

function wp_ajax_post_compare_admin_effa($params){
    global $wpdb;
    $user = wp_get_current_user();
    $orgChartID = isset($user->orgchart_id) ? $user->orgchart_id : 0;
    $params = $wpdb->escape($params);
    $getUser = effa_site_get_user_by_id($params['user_id']);
    $getOrgchart = effa_site_user_load_orgchart($getUser);
    $orgchart_id = $getOrgchart->id;
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    if( !user_is_manager() ){
        $message = __('Bạn không có quyền thực hiện thao tác này' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],403, $message);
    }
    if( empty($getUser) ){
        $message = __('Không tìm thấy nhân viên' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],404, $message);
    }
    $postID = $params['post_id'];
    $year = $params['year'];
    $post = get_post( $postID );
    if( !$post ){
        $message = __( 'Không tìm thấy mục tiêu. Vui lòng thử lại' ,TPL_DOMAIN_LANG);
        send_response_json(['error' => $message],404, $message);
    }
    $efficiency_lv = effa_site_level_up_eff_get_by_id($params['id']);
    if( empty($efficiency_lv) ){
        $message = __( 'Không tìm thấy mục tiêu của chức danh' ,TPL_DOMAIN_LANG);
        send_response_json(['error' => $message],404, $message);
    }
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableEfficiencyLevelUp = "{$prefix}efficiency_level_up";
    $errors = [];
    $wpdb->query("START TRANSACTION;");
    if( !empty($efficiency_lv) ){
        #update
        $data['manager_lv0'] = $params['value'];
        $wpdb->update($tableEfficiencyLevelUp, $data, ['id' => $efficiency_lv->id]);
        if( !empty($wpdb->last_error) ){
            $errors[] = $wpdb->last_error;
        }
    }else{
        $wpdb->query("ROLLBACK;");
        $message = __('Không tìm thấy năng lực' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],404, $message);
    }

    if( !empty($errors) ){
        # @TODO process error
        $wpdb->query("ROLLBACK;");
        $message = __('Đánh giá thất bại' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],415, $message);
    }else{
        $wpdb->query("COMMIT;");
        $result = effa_site_level_up_calculator_result($efficiency_lv->orgchart_id_new, $getUser->ID, $year);
        $totalResult = 0;
        $dataOutput = [];
        if( !empty($result) ){
            foreach ($result as $k => $item){
                $score = isset($item->score) ? $item->score : 0;
                $total_standard = isset($item->total_standard) ? $item->total_standard : 0;
                $percent = isset($item->percent) ? $item->percent : 0;
                $group_result = round($score / $total_standard * $percent, 0);
                if( $post->post_parent == $item->pID ) {
                    $dataOutput["group_score_{$item->pID}"] = $score;
                    $dataOutput["group_result_{$item->pID}"] = $group_result . "%";
                }
                $totalResult += $group_result;
            }
        }
        $thisCore = $params['value'] * $efficiency_lv->important;
        $dataOutput["score_{$post->ID}"] = $thisCore ;
        $dataOutput["result_{$post->ID}"] = (int)$params['value'] == 0 ? 0 : round( ($thisCore * 100 ) / ($efficiency_lv->important * $efficiency_lv->standard), 0 ) . "%";
        $dataOutput['total_result'] = $totalResult . "%";
        $message = __('Thành công', TPL_DOMAIN_LANG);
        send_response_json(['success' => $message, 'data_response' => $dataOutput], 201, $message);
    }
}