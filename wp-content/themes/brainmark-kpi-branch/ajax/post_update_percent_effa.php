<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/8/18
 * Time: 2:44 PM
 */

function wp_ajax_post_update_percent_effa($params){
    global $wpdb;
    $user = wp_get_current_user();
    $orgChartID = isset($user->orgchart_id) ? $user->orgchart_id : 0;
    $params = $wpdb->escape($params);
    $orgchart_id = $params['chart_id'];
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    $checkChildren = effa_site_orgchart_check_on_level($orgchart_id, $orgChartID);
    if( !user_is_manager() && empty($checkChildren) ){
        $message = __('Bạn không có quyền thực hiện thao tác này' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],403, $message);
    }
    $orgchart = effa_site_orgchart_get_chart_by_id($orgchart_id);
    if( empty($orgchart) ){
        $message = __( 'Không tìm thấy chức danh. Vui lòng thử lại' ,TPL_DOMAIN_LANG);
        send_response_json(['error' => $message],404, $message);
    }
    $postID = $params['post_id'];
    $post = get_post( $postID );
    if( !$post ){
        $message = __( 'Không tìm thấy mục tiêu. Vui lòng thử lại' ,TPL_DOMAIN_LANG);
        send_response_json(['error' => $message],404, $message);
    }
    $year = $params['year'];
    $percent = str_replace("%", "", $params['value']);
    if( !is_numeric($percent) ){
        $percent = 0;
    }
    $assign = effa_site_assign_get_by_postid_chartid_year($postID, $orgchart_id, $year, 'group');
    $created =  current_time( 'mysql' );
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableAssign = "{$prefix}assign_orgcharts";
    $errors = [];
    $sumPercent = effa_site_assign_sum_percent_by_orgchart($orgchart_id, $year);
    if( !empty($assign) ){
        #update
        if( isset($sumPercent->sumpercent) && ( $sumPercent->sumpercent + $percent - $assign->percent ) > 100 ){
            $message = __( 'Trọng số không vượt quá mức 100%' ,TPL_DOMAIN_LANG);
            send_response_json(['error' => $message],420, $message);
        }
        $data = [
            'percent' => $percent,
        ];
        $where = [
            'post_id' => $post->ID,
            'orgchart_id' => $orgchart_id,
            'year' => $year,
            'type' => 'group',
        ];
        $wpdb->update($tableAssign, $data, $where);
        if( !empty($wpdb->last_error) ){
            $errors[] = $wpdb->last_error;
        }
    }else{
        #insert
        if( isset($sumPercent->sumpercent) && ( $sumPercent->sumpercent + $percent ) > 100 ){
            $message = __( 'Trọng số không vượt quá mức 100%' ,TPL_DOMAIN_LANG);
            send_response_json(['error' => $message],420, $message);
        }
        $data = [
            'post_id' => $post->ID,
            'orgchart_id' => $orgchart_id,
            'year' => $year,
            'type' => 'group',
            'percent' => $params['value'],
            'created' => $created,
        ];
        $wpdb->insert($tableAssign, $data);
        if( !empty($wpdb->last_error) ){
            $errors[] = $wpdb->last_error;
        }

    }
    if( !empty($errors) ){
        # @TODO process error
        $wpdb->query("ROLLBACK;");
        $message = __('<i class="fa fas fa-times"></i>' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],415, $message);
    }else{
        $wpdb->query("COMMIT;");
        $message = __('Thành công', TPL_DOMAIN_LANG);
        send_response_json(['success' => $message], 201, $message);
    }
}