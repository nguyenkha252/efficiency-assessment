<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/6/18
 * Time: 14:13
 */

function wp_ajax_get_behavior_get_bank( $params ){
    global $wpdb;
    $user = wp_get_current_user();
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    if( !user_is_manager() ){
        $message = __('Bạn không có chỉnh sửa', TPL_DOMAIN_LANG);
        $httpCode = 403;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }
    $postarr = wp_slash($params);
    $post_id = (int)$postarr['ID'];
    $post = get_post( $post_id );
    if( !$post ){
        $httpCode = 404;
        $message = __('Không tìm thấy thái độ hành vi' ,TPL_DOMAIN_LANG );
        send_response_json(['code' => $httpCode, 'message' => $message],404, $message);
    }
    $targetID = $post->ID;
    $violation_assessment = $post->violation_assessment;
    $data = [
        'post_id' => $targetID,
        'violation_assessment' => $violation_assessment,

    ];
    send_response_json( ['data' => $data], 201, __('OK', TPL_DOMAIN_LANG ));
}