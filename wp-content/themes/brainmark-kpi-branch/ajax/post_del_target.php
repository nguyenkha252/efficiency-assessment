<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 08/01/2018
 * Time: 02:39
 */
require_once THEME_DIR . '/inc/lib-kpis.php';
function wp_ajax_post_del_target( $params ){
	global $wpdb;
	$user = wp_get_current_user();
	$postarr = $wpdb->escape($params);
	$post_id = (int)$postarr['ID'];
	if( !$user ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],401, $message);
	}
	if( empty($user->allcaps['delete_posts']) && !$user->has_cap('delete_posts') && !empty($post_id) ) {
		$message = __('Bạn không được cấp quyền xóa bài viết' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],402, $message);
	}
	$post = get_post( $post_id );
	if( !$post ){
		$message = __( 'Không tìm thấy mục tiêu KPI. Vui lòng thử lại' ,TPL_DOMAIN_LANG);
		send_response_json(['error' => $message],404, $message);
	}
	$resultKpi = kpi_get_kpi_by_bank($post_id);
	if( !empty( $resultKpi ) ){
		$message = __('Mục tiêu này đã được triển khai. Không thể xóa. Vui lòng xóa KPI trước khi xóa mục tiêu này.' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],415, $message);
	}
	$result = wp_delete_post($post_id, true);
	if( is_wp_error( $result ) ){
		# @TODO process error
		$message = __('Xóa mục tiêu KPI thất bại. Vui lòng thử lại' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],415, $message);
	}else{
		$message = __('Thành công', TPL_DOMAIN_LANG);
		send_response_json(['success' => $message], 200, $message);
	}
}