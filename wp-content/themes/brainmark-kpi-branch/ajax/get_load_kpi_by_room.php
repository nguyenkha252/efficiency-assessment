<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 30/03/2018
 * Time: 15:02
 */

function wp_ajax_get_load_kpi_by_room($params){
	require_once THEME_DIR . '/inc/lib-kpis.php';
	$params = wp_slash( $params );
	$user = wp_get_current_user();
	$orgchart = user_load_orgchart($user);
	if( !$user ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
		$httpCode = 401;
		send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
	}
	$results = kpi_get_kpi_by_chart_id( $params['id'], $params['year'], $params['type'] );
	$output = '';
	if( !empty( $results ) ){
		foreach ( $results as $key => $item ){
			$output .= sprintf("<option value='%d'>%s</option>", $item['id'], $item['post_title']);
		}
	}else{
		send_response_json([
			'message' => 'Không tìm thấy mục tiêu của phòng',
			'html' => '<option value="0" >Chọn mục tiêu</option>'],
			404,
			'Không tìm thấy mục tiêu của phòng');
	}
	send_response_json(['html' => $output], 201, 'Success');
}