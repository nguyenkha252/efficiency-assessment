<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/9/18
 * Time: 9:21 AM
 */

function wp_ajax_post_standard_effa($params){
    global $wpdb;
    $user = wp_get_current_user();
    $orgChartID = isset($user->orgchart_id) ? $user->orgchart_id : 0;
    $params = $wpdb->escape($params);
    $orgchart_id = $params['orgchart_id'];
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    $checkChildren = effa_site_orgchart_check_on_level($orgchart_id, $orgChartID);
    if( !user_is_manager() && empty($checkChildren) ){
        $message = __('<i class="fa fas fa-times"></i>' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],415, $message);
    }
    $orgchart = effa_site_orgchart_get_chart_by_id($orgchart_id);
    if( empty($orgchart) ){
        $message = __( 'Không tìm thấy chức danh. Vui lòng thử lại' ,TPL_DOMAIN_LANG);
        send_response_json(['error' => $message],404, $message);
    }
    $postID = $params['post_id'];
    $post = get_post( $postID );
    if( !$post ){
        $message = __( 'Không tìm thấy mục tiêu. Vui lòng thử lại' ,TPL_DOMAIN_LANG);
        send_response_json(['error' => $message],404, $message);
    }
    $year = $params['year'];
    $assign = effa_site_assign_get_by_postid_chartid_year($postID, $orgchart_id, $year);
    if( empty($assign) ){
        $message = __( 'Không tìm thấy mục tiêu được áp dụng cho chức danh' ,TPL_DOMAIN_LANG);
        send_response_json(['error' => $message],404, $message);
    }
    $efficiency = effa_site_efficiency_get_by_assign_id($assign->id, 0);
    $created =  current_time( 'mysql' );
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableEffa = "{$prefix}efficiency_assessment";
    $wpdb->query("START TRANSACTION;");
    $efficiency_after_id = 0;
    if( !empty( $efficiency ) ){
        #update
        if( isset($params['standard']) && !empty($params['standard']) ){
            $data['standard'] = $params['standard'];
        }
        if( isset($params['important']) && !empty($params['important']) ){
            $data['important'] = $params['important'];
        }
        $wpdb->update($tableEffa, $data, ['id' => $efficiency->id]);
        $efficiency_after_id = $efficiency->id;
        if( !empty($wpdb->last_error) ){
            $wpdb->query("ROLLBACK;");
            $message = __('<i class="fa fas fa-times"></i>' ,TPL_DOMAIN_LANG );
            send_response_json(['error' => $message],415, $message);
        }
    }else{
        #insert
        if( !isset($params['standard']) ){
            $params['standard'] = 1;
        }
        if( !isset($params['important']) ){
            $params['important'] = 1;
        }
        $data = [
            'assign_id' => $assign->id,
            'user_id' => 0,
            'important' => $params['important'],
            'standard' => $params['standard'],
            'created' => $created
        ];
        $wpdb->insert($tableEffa, $data);
        $efficiency_after_id = $wpdb->insert_id;
        if( !empty($wpdb->last_error) ){
            $wpdb->query("ROLLBACK;");
            $message = __('<i class="fa fas fa-times"></i>' ,TPL_DOMAIN_LANG );
            send_response_json(['error' => $message],415, $message);
        }
    }
    $effa_after_action = effa_site_efficiency_get_by_id($efficiency_after_id);
    $benchmark = 0;
    if(!empty($effa_after_action)){
        $benchmark = $effa_after_action->important * $effa_after_action->standard;
    }
    $wpdb->query("COMMIT;");
    $message = __('<i class="fa fas fa-check"></i>', TPL_DOMAIN_LANG);
    send_response_json(['success' => $message, 'benchmark' => $benchmark], 201, $message);

}