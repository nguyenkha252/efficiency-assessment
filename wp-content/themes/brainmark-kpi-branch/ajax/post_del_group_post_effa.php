<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/8/18
 * Time: 13:17
 */

function wp_ajax_post_del_group_post_effa( $params ){
    global $wpdb;
    $user = wp_get_current_user();
    $postarr = $wpdb->escape($params);
    $post_id = (int)$postarr['ID'];
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    if( empty($user->allcaps['delete_posts']) && !$user->has_cap('delete_posts') && !empty($post_id) ) {
        $message = __('Bạn không được cấp quyền xóa nhóm mục tiêu' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],402, $message);
    }
    $post = get_post( $post_id );
    if( !$post ){
        $message = __( 'Không tìm thấy nhóm mục tiêu. Vui lòng thử lại' ,TPL_DOMAIN_LANG);
        send_response_json(['error' => $message],404, $message);
    }

    $nameGroup = strtoupper( $post->post_title );
    $args = array(
        'post_parent' => $post_id,
        'post_type'   => 'efficiency',
        'post_status' => ['draft', 'publish'],
    );
    $children = get_children( $args );
    if( count( $children ) > 0 ){
        $message = __( "Vui lòng xoá hết mục tiêu của nhóm trước khi xoá nhóm &#34{$nameGroup}&#34" ,TPL_DOMAIN_LANG);
        send_response_json(['error' => $message],414, $message);
    }
    $result = wp_delete_post($post_id, true);
    if( is_wp_error( $result ) ){
        # @TODO process error
        $message = __('Xóa nhóm mục tiêu thất bại. Vui lòng thử lại' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],415, $message);
    }else{
        $message = __('Thành công', TPL_DOMAIN_LANG);
        send_response_json(['success' => $message], 201, $message);
    }
}