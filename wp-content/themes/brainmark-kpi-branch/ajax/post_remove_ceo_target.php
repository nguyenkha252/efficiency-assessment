<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 11/01/2018
 * Time: 23:51
 */
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-users.php';

function wp_ajax_post_remove_ceo_target($params){
	global $wpdb;
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableKpi = "{$prefix}kpis";

	$user = wp_get_current_user();
	$postarr = $wpdb->escape($params);
	$kpiID = (int)$postarr['kpi_id'];
	if( !$user ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],401, $message);
	}
	$checkCEO = kpi_is_user_ceo();
	if( !$checkCEO ){
		$message = __('Bạn không có quyền xóa mục tiêu này', TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],402, $message);
	}
	$result = kpi_get_kpi_by_id( $kpiID );

	#  Duyet tat ca cac item con va chau
    #$child_ids = $wpdb->prepare("SELECT id FROM {$tableKpi} WHERE `parent` = %d", $kpiID);
    #$child_child_ids = "SELECT id FROM {$tableKpi} WHERE `parent` IN ({$child_ids})";

    $wpdb->query("START TRANSACTION;");
    #$sql = ["DELETE FROM {$tableKpi} WHERE id IN ({$child_child_ids})"];
    #$sql[] = "DELETE FROM {$tableKpi} WHERE id IN ({$child_ids})";
    #$sql[] = "DELETE FROM {$tableKpi} WHERE id = {$kpiID}";
    # $msg = "Debug";
    # send_response_json(['code' => 400, 'message' => $msg, 'sql' => $sql], 400, $msg);
	$arrKPI_IDs = [$kpiID];
	$getAllChildrenKPI = kpi_get_all_kpi_by_parent( $kpiID );
	if( !empty( $getAllChildrenKPI ) ){
		foreach ( $getAllChildrenKPI as $key => $item ){
			$arrKPI_IDs[] = $item['id'];
		}
	}
	$strIDs = implode( ", ", $arrKPI_IDs );
    $wpdb->query("DELETE FROM {$tableKpi} WHERE id IN ({$strIDs})");
    if( $wpdb->last_error ) {
	    $msg = $wpdb->last_error;
        $wpdb->query("rollback;");
	    send_response_json(['code' => 400, 'message' => $msg], 400, $msg);
    } else {
        #$wpdb->query("DELETE FROM {$tableKpi} WHERE id IN ({$child_ids})");
        #if( $wpdb->last_error ) {
        #    $wpdb->query("rollback;");
        #} else {
        #    $wpdb->delete( $tableKpi, ['id' => $kpiID] );
        #    if( $wpdb->last_error ) {
        #        $wpdb->query("rollback;");
        #    } else {
        #        $wpdb->query("COMMIT;");
        #        $message = __('Xóa thành công', TPL_DOMAIN_LANG);
        #        send_response_json(['success' => $message], 202, $message);
        #    }
        #}
		$wpdb->query("COMMIT;");
		$message = __('Xóa thành công', TPL_DOMAIN_LANG);
		send_response_json(['success' => $message], 202, $message);
    }
//    if( $wpdb->last_error ) {
//        $msg = $wpdb->last_error;
//        send_response_json(['code' => 400, 'message' => $msg], 400, $msg);
//    } else {
//        $msg = "Đang cập nhật";
//        send_response_json(['code' => 400, 'message' => $msg], 400, $msg);
//    }
}

