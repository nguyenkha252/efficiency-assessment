<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/22/17
 * Time: 15:30
 */

function wp_ajax_post_update_tree($params) {
    update_option('orgchart_ids', $params['ids']);
    update_option('orgchart_tree', $params['tree']);
    send_response_json( $params, 200, 'OK' );
}