<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/12/18
 * Time: 11:37 PM
 */

function wp_ajax_post_finish_quiz($params){
    $params = wp_slash($params);
    $user = wp_get_current_user();
    if( !is_user_logged_in() ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],403, $message);
    }
    $nonce = $params['nonce'];
    $year = date('Y', time());
    if ( ! wp_verify_nonce( $nonce, 'finish_quiz') ) {
        $message = __('Phiên bản đã hết hạn. Vui lòng tải lại trang và thực hiện bài kiểm tra.' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],408, $message);
    }
    $answers = array();

    foreach ($params as $key => $param) {
        if ( preg_match('/question_(\w+)/', $key, $matches) ) {
            $answers[$matches[1]] = $param;
        }
    }
    update_user_meta($user->ID, "efficiency_{$year}", $answers);

    $message = __('Thành công' ,TPL_DOMAIN_LANG );
    send_response_json(['message' => $message],201, $message);
}