<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 15/03/2018
 * Time: 11:05
 */

function wp_ajax_get_formula_by_id( $params ){
	require_once THEME_DIR . '/inc/lib-formulas.php';
	require_once THEME_DIR . '/inc/config.php';
	$user = wp_get_current_user();
	$params = wp_slash( $params );
	if( !$user ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
		$httpCode = 401;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	$result = get_formula_by_id( $params['ID'] );
	if( empty( $result ) ){
		$msg = __('Không tìm thấy công thức', TPL_DOMAIN_LANG);
		$httpCode = 404;
		send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
	}
	$formulas = maybe_unserialize( $result->formulas );
	$type = $result->type;
	$des_formula = '';
	$measurementFormulas = getMeasurementFormulas();
	$measurementFormulas['!='] = __('KH # KQ', TPL_DOMAIN_LANG);
	$measurementFormulas['0'] = __('----------', TPL_DOMAIN_LANG);
	if( !empty( $formulas ) ) {
		if ( $type == 'on_off' ) {
			foreach ( $formulas as $key => $item ) {
				$condition   = $measurementFormulas[ $item['condition'] ];
				$value       = $item['value'];
				$des_formula .= "<span class='formula-des-item'>{$condition}: {$value}</span>";
			}
		} elseif ( $type == 'oscillate' ) {
			foreach ( $formulas as $key => $item ) {
				$condition   = $measurementFormulas[ $item['condition'] ];
				$oscillate   = $item['oscillate'];
				$value       = $item['value'];
				$des_formula .= "<span class='formula-des-item'>{$condition} + {$oscillate}: {$value}</span>";
			}
		} elseif ( $type == 'add_max_kq' ) {
			foreach ( $formulas as $key => $item ) {
				$condition   = $measurementFormulas[ $item['condition'] ];
				$max         = $item['max'];
				$value       = $item['value'];
				$des_formula .= "<span class='formula-des-item'>{$condition}: {$value} (Max KPI  {$max})</span>";
			}
		} elseif ( $type == 'add_between' ) {
			foreach ( $formulas as $key => $item ) {
				$condition_1 = getConditions( $item['condition_1'] );
				$condition_2 = getConditions( $item['condition_2'] );
				$kqmin       = $item['kq_min'];
				$kqmax       = $item['kq_max'];
				$value       = $item['value'];
				$des_formula .= "<span class='formula-des-item'>{$kqmin} {$condition_1} <strong>KQ</strong> {$condition_2} {$kqmax}: {$value}</span>";
			}
		}
	}
	$msg = __('Thành công', TPL_DOMAIN_LANG);
	$httpCode = 201;
	send_response_json(['code' => $httpCode, 'data' => $des_formula], $httpCode, $msg);
}