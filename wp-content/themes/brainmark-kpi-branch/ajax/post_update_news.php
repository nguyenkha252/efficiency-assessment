<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/2/18
 * Time: 11:45
 */


function wp_ajax_post_update_news($params) {
    $nonce = !empty($_REQUEST['update_news']) ? $_REQUEST['update_news'] : '';
    $error = null;
    $user = wp_get_current_user();
    $can_edit_post = false;
    if( !empty($user->allcaps['level_9']) || $user->has_cap('level_9') ) {
        $can_edit_post = true;
    }
    if( wp_verify_nonce($nonce, 'update_news') ) {
        $error = new WP_Error(401, __('Invalid nonce when save user', TPL_DOMAIN_LANG) );
        send_response_json(['code' => $error->get_error_code(), 'message' => $error->get_error_message()], $error->get_error_code(), $error->get_error_message());
    }
    # $error = new WP_Error(403, __('You do not permission to edit page', TPL_DOMAIN_LANG) );
    # send_response_json(['code' => $error->get_error_code(), 'message' => $error->get_error_message()], $error->get_error_code(), $error->get_error_message());
    if( !$can_edit_post ) {
        $error = new WP_Error(403, __('You do not permission to edit page', TPL_DOMAIN_LANG) );
        send_response_json(['code' => $error->get_error_code(), 'message' => $error->get_error_message()], $error->get_error_code(), $error->get_error_message());
    }
    $result = wp_update_post($params);
    if( is_wp_error($result) ) {
        send_response_json(['code' => $result->get_error_code(), 'message' => __('An error occured, please try again later', TPL_DOMAIN_LANG),
            'message1' => $result->get_error_message()], 400, $result->get_error_message());
    } else {
        send_response_json($result, 202, 'Updated');
    }
}