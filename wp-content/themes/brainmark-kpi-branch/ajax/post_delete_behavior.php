<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 08/02/2018
 * Time: 00:07
 */
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-behavior.php';
function wp_ajax_post_delete_behavior( $params ){
	global $wpdb;
	$params = wp_slash($params);
	$user = wp_get_current_user();
	if (!$user) {
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG);
		$httpCode = 401;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	if( !user_is_manager() ){
		$message = __('Bạn không có quyền xóa', TPL_DOMAIN_LANG);
		$httpCode = 403;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	/*if (!wp_verify_nonce($params['_wpnonce'], 'delete_capacity')) {
		$message = __('Mã bảo vệ không hợp lệ', TPL_DOMAIN_LANG);
		$httpCode = 403;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}*/
	$result = behavior_get_by_id( $params['id'] );
	if( !empty( $result ) ){
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpiBehavior = "{$prefix}behavior";
		$getAllBehavior = behavior_get_all_behavior_by_parent( $params['id'] );
		$arrDelIDs = [$params['id']];
		if(!empty( $getAllBehavior ) ){
			foreach ( $getAllBehavior as $key => $item ){
				$arrDelIDs[] = $item['id'];
			}
		}
		$strDelIDs = implode( ", ", $arrDelIDs );
		$wpdb->query("DELETE FROM {$tableKpiBehavior} WHERE id IN ({$strDelIDs})");
		#$wpdb->delete($tableKpiBehavior, ['id' => $params['id']]);
		$httpCode = 201;
		send_response_json(['code' => $httpCode, 'success' => 1], $httpCode, "OK");
	}else{
		$msg = "Không tìm thấy thái độ hành vi";
		$httpCode = 404;
		send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
	}
}