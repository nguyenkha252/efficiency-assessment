<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/19/18
 * Time: 10:36
 */

function wp_ajax_post_department_send_approved( $params ){
    global $wpdb;
    $user = wp_get_current_user();
    $orgchart = user_load_orgchart( $user );
    $postarr = wp_slash($params);
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        $httpCode = 401;
        send_response_json(['code' => $httpCode, 'message' => $message],$httpCode, $message);
    }
    $nonce = isset($postarr['_wpnonce']) ? $postarr['_wpnonce'] : '';
    if( !wp_verify_nonce($nonce, 'department_send_approved') ) {
        $msg = "Sai mã bảo vệ";
        send_response_json(['code' => 403, 'message' => $msg], 403, $msg);
    }
    if( !in_array($orgchart ? $orgchart->role : '', ['phongban', 'bgd'] ) ){
        $message = __('Bạn không có quyền chỉnh sửa' ,TPL_DOMAIN_LANG );
        $httpCode = 403;
        send_response_json(['code' => $httpCode, 'message' => $message],$httpCode, $message);
    }
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableKpi = "{$prefix}kpis";
    $location = '';
    require_once THEME_DIR . '/inc/lib-users.php';
    require_once THEME_DIR . '/inc/lib-orgchart.php';
    require_once THEME_DIR . '/inc/lib-kpis.php';
    #print_r( $postarr );exit();
    if( !empty( $postarr['register'] ) ){
        $dataUpdate = ['status' => 'cho-duyet-dang-ky'];
        if( !empty( $postarr ) && !empty( $postarr['_wp_http_referer'] ) ){
            $query_params = wp_get_referer();
            $location = site_url($query_params);
        }
        $nodeEnd = check_user_is_node_end();
        $wpdb->query( "START TRANSACTION;" );
        
        foreach ( $postarr['register'] as $kpi){
            $kpiID = $kpi['ID'];
            $status = $kpi['status'];
	            if( $status == KPI_STATUS_DRAFT ){
                $dataUpdate['status'] = KPI_STATUS_PENDING;
            }else{
                $dataUpdate['status'] = KPI_STATUS_DRAFT;
            }

            #BGD OR PHONGBAN
            $result = kpi_get_kpi_by_id($kpiID);
            if( $result ){
                $wpdb->update( $tableKpi, $dataUpdate, ['id' => $kpiID] );
                if( !empty( $wpdb->last_error ) ){
                    $wpdb->query("ROLLBACK;");
                    $message = __('Đã xảy ra lỗi. Vui lòng thử lại' ,TPL_DOMAIN_LANG );
                    $httpCode = 404;
                    send_response_json(['code' => $httpCode, 'message' => $message, 'error' => $wpdb->last_error],$httpCode, $message);
                }
            }else{
                $message = __('Không tìm thấy KPI' ,TPL_DOMAIN_LANG );
                $httpCode = 404;
                send_response_json(['code' => $httpCode, 'message' => $message],$httpCode, $message);
            }

        }
        $wpdb->query("COMMIT;");
        $message = __('Lưu thành công', TPL_DOMAIN_LANG);
        $httpCode = 201;
        send_response_json(
            ['code' => $httpCode, 'message' => $message, 'location' => $location],
            $httpCode,
            $message
        );
    }else{
        $message = __('Không tìm thấy KPI' ,TPL_DOMAIN_LANG );
        $httpCode = 404;
        send_response_json(
            ['code' => $httpCode, 'message' => $message],
            $httpCode,
            $message
        );
    }
}