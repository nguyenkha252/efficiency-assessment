<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 8/30/18
 * Time: 10:39 PM
 */
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-kpis.php';

/**
 * @param $date
 * @param $in_year
 * @return bool
 */

function set_receive_kpis($date, $thisMonth){
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableKpis = "{$prefix}kpis";
    $getKPIForMonth = kpi_get_kpi_by_month_and_year($thisMonth['year'], $thisMonth['month']);
    if( !empty($getKPIForMonth) ){
        $ids = [];
        foreach ( $getKPIForMonth as $k => $kpi ){
            $ids[] = $kpi['id'];
        }
        $ids_arr = implode(",", $ids);
        $sql = $wpdb->prepare("UPDATE {$tableKpis} SET receive = %s WHERE id IN (%s)", $date, $ids_arr);
        $wpdb->query($sql);
        //var_dump($wpdb->last_query, $wpdb->last_error);
    }
}

function check_input_date($date, $in_year, $in_month){
    $date_explode = explode("-", $date);
    if( count( $date_explode ) == 3 ){
        #DD/MM/YYYY
        $day = $date_explode[0];
        $month = $date_explode[1];
        $year = $date_explode[2];
        $date_2 = $year . "-" . $month . "-" . $day;
        if( $in_month != $month ){
            return false;
        }
        if( !strtotime($date_2) || (!checkdate($month, $day, $year)) ){
            $date_2 = false;
        }else{
            $date_2 = true;
        }
    }else if(count( $date_explode ) == 2){
        #DD/MM/YYYY
        $day = $date_explode[0];
        $month = $date_explode[1];
        $date_2 = $in_year . "-" . $month . "-" . $day;
        if( $in_month != $month ){
            return false;
        }
        if( !strtotime($date_2) || !checkdate($month, $day, $in_year) ){
            $date_2 = false;
        }else{
            $date_2 = true;
        }
    }else{
        $date_2 = false;
    }
    return $date_2;
}

function wp_ajax_post_lock_for_month($params){
    global $wpdb;
    $params = wp_slash($params);
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableKpiYear = "{$prefix}kpi_years";

    $nonce = isset($params['_wpnonce']) ? $params['_wpnonce'] : '';
    if( !wp_verify_nonce($nonce, 'lock_for_month') ) {
        $msg = 'Mã bảo vệ không hợp lệ';
        $status = 403;
        send_response_json(['code' => $status, 'message' => $msg], $status, $msg);
    }
    $user = wp_get_current_user();
    $is_admin = kpi_is_user_as_admin($user);
    if( !$is_admin ) {
        $msg = 'Bạn không có quyền thêm cấu hình khoá theo tháng cho công ty';
        $status = 403;
        send_response_json(['code' => $status, 'message' => $msg], $status, $msg);
    }
    $year = year_get_firt_year_by_chart_is_congty($params['nam']);
    if( empty($year) ){
        $msg = 'Không tìm thấy năm';
        $status = 404;
        send_response_json(['code' => $status, 'message' => $msg], $status, $msg);
    }
    $getMonth = year_get_month_settings($year['id']);
    $created = date('Y-m-d H:i:s', time());
    if( isset($params['settings_month']) && !empty($params['settings_month']) ){
        $wpdb->query("START TRANSACTION;");
        foreach ($params['settings_month'] as $key => $item){
            $month = $item['month'];
            $month_values = $item['values'];
            $date = check_input_date($month_values, $year['year'], $month);
            if( $date == false ){
                $wpdb->query("ROLLBACK;");
                $msg = 'Vui lòng nhập ngày tháng đúng định dạng';
                $status = 401;
                send_response_json(['code' => $status, 'message' => $msg], $status, $msg);
            }
            $date_explode = explode("-", $month_values);
            $date = $date_explode[2] . "-" . $date_explode[1] . "-" . $date_explode[0] ." 00:00:00";
            $precious = ceil($month / 3);
            $checkMonth = array_filter($getMonth, function($item) use ($month){
                return $item['month'] == $month;
            });
            #print_r($checkMonth);exit;
            if( !empty($checkMonth) ){
                #update
                $thisMonth = array_shift($checkMonth);
                $data['date_expired'] = $date;
                if( strtotime($date) == strtotime($thisMonth['date_expired']) )
                    continue;
                $wpdb->update($tableKpiYear, $data, ['id' => $thisMonth['id']]);
                if( !empty( $wpdb->last_error ) ){
                    #var_dump($data, $item, $wpdb->last_query, $wpdb->last_error);
                    $wpdb->query("ROLLBACK;");
                    $msg = 'Có lỗi khi tạo ngày hết hạn. Vui lòng thử lại.';
                    $status = 401;
                    send_response_json(['code' => $status, 'message' => $msg], $status, $msg);
                }
                set_receive_kpis($date, $thisMonth);
            }else{
                #insert
                $data = [
                    'year'          => $year['year'],
                    'precious'      => $precious,
                    'month'         => $month,
                    'parent'        => $year['id'],
                    'kpi_time'      => 'thang',
                    'kpi_type'      => 'settings',
                    'chart_id'      => $user->orgchart_id,
                    'status'        => 'publish',
                    'date_expired'  => $date,
                    'created'       => $created,
                ];
                $insert_id = $wpdb->insert($tableKpiYear, $data);

                if( !empty($wpdb->last_error) ){
                    #var_dump($data, $insert_id, $item, $wpdb->last_error);exit;
                    $wpdb->query("ROLLBACK;");
                    $msg = 'Có lỗi khi tạo ngày hết hạn. Vui lòng thử lại.';
                    $status = 401;
                    send_response_json(['code' => $status, 'message' => $msg], $status, $msg);
                }
                set_receive_kpis($date, ['year' => $year['year'], 'month' => $month]);
            }

        }
        $wpdb->query("COMMIT;");
        $msg = 'Thành công';
        $status = 201;
        send_response_json(['code' => $status, 'message' => $msg], $status, $msg);
    }else{
        $msg = 'Có lỗi khi tạo ngày hết hạn. Vui lòng thử lại.';
        $status = 401;
        send_response_json(['code' => $status, 'message' => $msg], $status, $msg);
    }

}