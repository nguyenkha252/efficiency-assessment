<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 12/03/2018
 * Time: 23:44
 */
require_once THEME_DIR . '/inc/lib-behavior.php';
function wp_ajax_get_list_behavior_for_user( $params ) {
	global $wpdb;
	$params = wp_slash( $params );
	$user   = wp_get_current_user();
	$orgchart = user_load_orgchart( $user );
	if ( ! $user ) {
		$message  = __( 'Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
		$httpCode = 401;
	}
	$precious = !empty( $params['precious'] ) ? $params['precious'] : 0;
	$month = !empty( $params['month'] ) ? $params['month'] : 0;
	$year = !empty( $params['year'] ) ? $params['year'] : 0;
	$year_id = !empty( $params['year_id'] ) ? $params['year_id'] : 0;
	if( empty( $precious ) ){
		$resultBehavior = behavior_get_total_number_for_year( $year, $user->ID, $orgchart->id );
		$resultBehavior = array_group_by( $resultBehavior, 'parent_1' );
	}else{
		$resultBehavior = behavior_get_behavior_by_user( $user->ID, $year, $precious, $month );
	}
	$groupBehavior = [];
	if( !empty( $resultBehavior ) ){
		$groupBehavior = $resultBehavior[0];
		unset($resultBehavior[0]);
	}
	$htmlGroup = '';
	if ( ! empty( $groupBehavior ) ):
		foreach ( $groupBehavior as $key => $itemGroup ):
			$htmlItem = '';
			$stt = 0;
			$getItemBehavior = [];
			if( array_key_exists( $itemGroup['id'], $resultBehavior ) ){
				$getItemBehavior = $resultBehavior[$itemGroup['id']];
				unset( $resultBehavior[$itemGroup['id']] );
			}
			foreach ( $getItemBehavior as $k => $item ) {
				$stt ++;
				$number = empty( $precious ) ? $item['total_number'] : $item['number'];
				$violation_assessment = $item['violation_assessment'];
				$kqvp = $number * $violation_assessment;
				if( !empty( $precious ) ){
					$inputPercent = "<input class='width60 align-center' type='number' name='behavior[".$item['id']."][number]' value='".esc_attr($item['number'])."' />
                                            <input type='hidden' name='behavior[".$item['id']."][id]' value='".$item['id']."' />";
				}else{
					$inputPercent = $number;
				}
				$htmlItem .= "
                                        <tr class='item-'>
                                        <td class=\"column-1 align-center\">{$stt}</td>
                                        <td class=\"column-2\">" . esc_attr( $item['post_title'] ) . "</td>
                                        <td class=\"column-3 align-center\">" . esc_attr( $violation_assessment ) . "%</td>
                                        <td class=\"column-4 align-center\">
                                            {$inputPercent}
                                        </td>
                                        <td class=\"column-5 align-center\">
                                            ".esc_attr($kqvp)."%
                                        </td>
                                    </tr>
                                        ";
			}
			if( !empty( $htmlItem ) ){
				$htmlItem = "<tbody>{$htmlItem}</tbody>";
			}
			$htmlGroup .= "
                                    <thead class='group-behavior-item'>
                                        <tr>
                                            <td class='column-1 align-center'>
                                                <a href='javascript:;' class='group-item-down'><i class=\"fa fa-chevron-circle-down fa-chevron-circle-up\"></i></a>
                                            </td>
                                            <td colspan='4' class='column-2'>{$itemGroup['post_title']}</td>
                                        </tr>
                                    </thead>
                                    {$htmlItem}
                                    ";
			?>
		<?php endforeach; ?>
	<?php endif;
	$httpCode = 201;
	send_response_json(['code' => $httpCode, 'html' => $htmlGroup], $httpCode, "OK");
}