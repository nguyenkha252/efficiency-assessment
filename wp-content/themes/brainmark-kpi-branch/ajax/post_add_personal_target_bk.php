<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/17/18
 * Time: 17:12
 */

require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-users.php';

function wp_ajax_post_add_personal_target( $params ){
    global $wpdb;
    $user = wp_get_current_user();
    $orgchart = user_load_orgchart($user);
    $params = wp_slash($params);
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
        $httpCode = 401;
        send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
    }
    $bank_id = $params['bank'];
    $post = get_post($bank_id);
    if( !$post ){
        $message = __('Vui lòng chọn kpi để áp dụng', TPL_DOMAIN_LANG );
        $httpCode = 404;
        send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
    }
    
    #get_kpi_by_conditions( $postID, $chartID, $yearID, $type )
    $result = get_kpi_by_conditions( $bank_id, $params['chart_id'], $params['year_id'], $params['type'] );
    $resultKPI = kpi_get_kpi_by_id( $params['kpiID'] );
    if( empty( $result ) || empty( $resultKPI )){
        $message = __('Không tìm thấy KPI', TPL_DOMAIN_LANG );
        $httpCode = 404;
        send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
    }
    $personal = kpi_is_user_nhanvien();
    if( $resultKPI['status'] == 'publish' && $resultKPI['aproved'] == 0 && $personal ){
        $message = __('KPI đã gửi xét duyệt không được sửa', TPL_DOMAIN_LANG );
        $httpCode = 403;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }elseif( $resultKPI['status'] == 'publish' && $resultKPI['aproved'] == 1 && $personal ){
        $message = __('KPI đã triển khai không được sửa', TPL_DOMAIN_LANG );
        $httpCode = 403;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }

    if( empty( (int)$params['time_month'] ) ){
        $message = __('Có lỗi xảy ra khi lưu. Vui lòng thử lại', TPL_DOMAIN_LANG );
        $httpCode = 400;
        send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
    }

    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableKpi = "{$prefix}kpis";
    $tableKpiYear = "{$prefix}kpi_years";
    if( empty( $params['kpiID'] ) ) {
        $month = (int)$params['time_month'];
        $precious = ceil($month / 3);
        $yearData['year'] = $result['year'];
        $yearData['precious'] = $precious;
        $yearData['month'] = $month;
        $yearData['finance'] = $result['finance'];
        $yearData['customer'] = $result['customer'];
        $yearData['operate'] = $result['operate'];
        $yearData['development'] = $result['development'];
        $yearData['parent'] = $result['yearID'];
        $yearData['status'] = 'publish';
        $yearData['created'] = date('Y-m-d H:i:s', time());

        #start transaction
        $wpdb->query("START TRANSACTION;");
        #insert year
        $wpdb->insert($tableKpiYear, $yearData);
        if (!empty($wpdb->last_error)) {
            $message = __('Có lỗi xảy ra khi lưu. Vui lòng thử lại', TPL_DOMAIN_LANG);
            $httpCode = 400;
            $wpdb->query('ROLLBACK;');
            send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
        }
        $year_id = $wpdb->insert_id;
        $kpiData['year_id'] = $year_id;
        $kpiData['personal_plan'] = $params['personal_plan'];
        $kpiData['type'] = $result['type'];
        $kpiData['chart_id'] = $orgchart->id;
        $kpiData['user_id'] = $user->ID;
        $kpiData['status'] = 'draft';
        $kpiData['aproved'] = 0;
        $kpiData['created'] = date('Y-m-d H:i:s', time());
        $kpiData['bank_id'] = $bank_id;
        $kpiData['company_plan'] = 0;
        $kpiData['department_plan'] = 0;
        $kpiData['unit'] = $result['unit'];
        $kpiData['receive'] = $result['receive'];
        $kpiData['percent'] = $result['percent'];
        $kpiData['parent'] = $result['id'];
        #insert kpi
        $wpdb->insert( $tableKpi, $kpiData );
    }else{
        $wpdb->query("START TRANSACTION;");
        $kpiData['personal_plan'] = $params['personal_plan'];
        $kpiData['bank_id'] = $bank_id;
        $wpdb->update( $tableKpi, $kpiData, ['id' => $resultKPI['id']] );
    }

    if( !empty( $wpdb->last_error ) ){
        $message = __('Có lỗi xảy ra khi lưu. Vui lòng thử lại', TPL_DOMAIN_LANG );
        $httpCode = 400;
        $wpdb->query('ROLLBACK;');
        send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
    }
    $wpdb->query('COMMIT;');
    $message = __('Thành công', TPL_DOMAIN_LANG);
    $httpCode = 201;
    send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, __('OK', TPL_DOMAIN_LANG) );



}