<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/29/18
 * Time: 16:50
 */
function wp_ajax_post_department_approved_kpi( $params ){
    global $wpdb;
    $user = wp_get_current_user();
    $orgchart = user_load_orgchart( $user );
    $postarr = wp_slash($params);
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
        $httpCode = 401;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }
    if( !wp_verify_nonce($postarr['_wpnonce'], 'department_approved_kpi') ) {
        $msg = "Phiên làm việc đã hết hạn. Vui lòng thử lại.";
        send_response_json(['code' => 405, 'message' => $msg], 405, $msg);
    }
    if( !in_array($orgchart ? $orgchart->role : '', ['phongban', 'bgd'] ) ){
        $message = __('Bạn không có quyền chỉnh sửa', TPL_DOMAIN_LANG );
        $httpCode = 403;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableKpi = "{$prefix}kpis";
    require_once THEME_DIR . '/inc/lib-users.php';
    require_once THEME_DIR . '/inc/lib-kpis.php';
    require_once THEME_DIR . '/inc/lib-orgchart.php';

    if( !empty( $postarr['register'] ) ){
        $wpdb->query( "START TRANSACTION;" );
        foreach ( $postarr['register'] as $kpi){
            $kpiID = $kpi['ID'];
            $status = $kpi['status'];
            if( $status == 1 ){
                $dataUpdate['status'] = KPI_STATUS_RESULT;
            }else{
                $dataUpdate['status'] = KPI_STATUS_PENDING;
            }
            if( ($orgchart ? $orgchart->role : '') === 'phongban' ){
                $result = kpi_get_kpi_by_id( $kpiID );
                if( $result ){
                    #kiểm tra xem kpi hiện tại của cấp dưới trực tiếp hay không
                    $resultsOrgChart = orgchart_check_children_by_id( $result['chart_id'] );
                    if( !empty( $resultsOrgChart ) && $resultsOrgChart->parent == $orgchart->id ){
                        $wpdb->update( $tableKpi, $dataUpdate, ['id' => $kpiID] );
                        if( !empty( $wpdb->last_error ) ){
                            $wpdb->query("ROLLBACK;");
                            $httpCode = 400;
                            $message = __("Có lỗi xảy ra", TPL_DOMAIN_LANG );
                            send_response_json(['code' => $httpCode, 'message' => $message, 'error' => $wpdb->last_error], $httpCode, $message);
                        }
                    } else {
                        # nếu kpi hiện tại không phải là cấp dưới trực tiếp thì thông báo cho người dùng
                        $_user = get_user_by('ID', $result['user_id']);
                        $fullName = $_user->first_name . ' ' . $_user->last_name;
                        $message = __("Bạn không phải là cấp trên của {$fullName}" ,TPL_DOMAIN_LANG );
                        $httpCode = 404;
                        send_response_json(['code' => $httpCode, 'message' => $message],$httpCode, $message);
                    }

                } else {
                    $message = __('Không tìm thấy KPI', TPL_DOMAIN_LANG );
                    $httpCode = 404;
                    send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
                }
            } else {
                #BGD
                $result = kpi_get_kpi_by_id($kpiID);
                if( $result ) {
                    $wpdb->update( $tableKpi, $dataUpdate, ['id' => $kpiID] );
                    if( !empty( $wpdb->last_error ) ){
                        $wpdb->query("ROLLBACK;");$httpCode = 400;
                        $message = __("Có lỗi xảy ra", TPL_DOMAIN_LANG );
                        send_response_json(['code' => $httpCode, 'message' => $message, 'error' => $wpdb->last_error], $httpCode, $message);
                    }
                } else {
                    $message = __('Không tìm thấy KPI', TPL_DOMAIN_LANG );
                    $httpCode = 404;
                    send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
                }
            }
        }
        $wpdb->query("COMMIT;");
        $message = __('Lưu thành công', TPL_DOMAIN_LANG);
        $httpCode = 201;
        send_response_json(
            ['code' => $httpCode, 'message' => $message], // , 'location' => $location
            $httpCode,
            $message
        );
    }else{
        $message = __('Không tìm thấy KPI' ,TPL_DOMAIN_LANG );
        $httpCode = 404;
        send_response_json(
            ['code' => $httpCode, 'message' => $message],
            $httpCode,
            $message
        );
    }
}