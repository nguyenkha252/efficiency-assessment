<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 2/7/18
 * Time: 16:02
 */

require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-behavior.php';
function wp_ajax_post_edit_management_behavior( $params ){
    global $wpdb;
    $user = wp_get_current_user();
    $orgchart = user_load_orgchart($user);
    $params = wp_slash( $params );
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
	if( !user_is_manager() ){
		$message = __('Bạn không có quyền thêm/sửa', TPL_DOMAIN_LANG);
		$httpCode = 403;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
    if( !wp_verify_nonce($params['_wpnonce'], 'add_management_behavior') ){
        $message = __('Phiên bản đã hết hạn. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],408, $message);
    }
    $resultYear = kpi_get_year_by_id( $params['year_id'] );
    if( empty( $resultYear ) ){
    	$message = __("Không tìm thấy năm của thái độ hành vi", TPL_DOMAIN_LANG);
	    send_response_json(['error' => $message],404, $message);
    }
    $post = get_post( $params['bank_id'] );
    if( !$post ){
	    $message = __("Không tìm thấy hành vi", TPL_DOMAIN_LANG);
	    send_response_json(['error' => $message],404, $message);
    }
	$result = behavior_get_by_id( $params['id'] );
    if( empty( $result ) ){
	    $msg = "Không tìm thấy thái độ hành vi";
	    $httpCode = 404;
	    send_response_json(['code' => $httpCode, 'error' => $msg], $httpCode, $msg);
    }
	$post_parent = get_post( $post->post_parent );
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableKpiBehavior = "{$prefix}behavior";
    extract( $params );
    $chart_id = $orgchart->id;
    $is_update = 0;
    $user_id = $user->ID;
	$object_user = '';
    if( $result['status'] == KPI_STATUS_RESULT ) {
	    $users    = user_get_users_by_alias( "'phongban','nhanvien'" );
	    $arrUsers = [];
	    if ( ! empty( $users ) ) {
		    foreach ( $users as $key => $value ) {
			    $arrUsers[] = $value['ID'];
		    }
	    }
	    if ( ! empty( $arrUsers ) ) {
		    $object_user = maybe_serialize( $arrUsers );
	    }
	    $is_update = 1;
    }
	$parentID = 0;
	$getYearBehavior = kpi_get_year_by_parent( $result['year_id'] );
	$arrParentIDs = [];
	if( $post_parent ){
		$group = behavior_get_group_behavior_by_post( $post_parent->ID, $params['year'] );
		if( !empty( $group ) ){
			$parentID = $group['id'];
		}else{
			$parent_1 = 0;
			$parent = 0;
			$dataParent = compact("year_id", "chart_id", "user_id", "parent", "parent_1", "status", "created");
			$dataParent['bank_id'] = $post_parent->ID;
			$wpdb->insert($tableKpiBehavior, $dataParent);
			if( !empty( $wpdb->last_error ) ){
				$msg = "Có lỗi xảy ra khi lưu";
				$httpCode = 400;
				send_response_json(['code' => $httpCode, 'message' => $msg, 'error' => $wpdb->last_error], $httpCode, $msg);
			}
			$parentID = $wpdb->insert_id;
			foreach ( $getYearBehavior as $key => $item ){
				$dataParent['parent'] = $parentID;
				$dataParent['year_id'] = $item['id'];
				$wpdb->insert($tableKpiBehavior, $dataParent);
				if( !empty( $wpdb->last_error ) ){
					$msg = "Có lỗi xảy ra khi lưu";
					$httpCode = 400;
					send_response_json(['code' => $httpCode, 'message' => $msg, 'error' => $wpdb->last_error], $httpCode, $msg);
				}
				$arrParentIDs[$item['id']] = $wpdb->insert_id;
			}
		}

	}

	$parent_1 = $parentID;
	#echo $parent_1;
	#print_r($arrParentIDs);
	#exit;
    $data = compact("bank_id", "object_user", "is_update", "parent_1");
    #update
    $wpdb->update($tableKpiBehavior, $data, ['id' => $result['id']]);
    if( !empty( $wpdb->last_error ) ){
        $msg = "Có lỗi xảy ra khi lưu";
        $httpCode = 400;
        send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
    }
	$getChild = behavior_get_by_parent( $result['id'] );
    foreach ( $getChild as $key => $item ){
	    $data['parent_1'] = !empty($arrParentIDs) ? $arrParentIDs[$item['year_id']] : $item['parent_1'];
	    $wpdb->update($tableKpiBehavior, $data, ['id' => $item['id']]);
	    if( !empty( $wpdb->last_error ) ){
		    $msg = "Có lỗi xảy ra khi lưu";
		    $httpCode = 400;
		    send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
	    }
    }


    $msg = "Thành công";
    $httpCode = 201;
    send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
}