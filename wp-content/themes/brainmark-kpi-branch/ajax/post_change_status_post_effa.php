<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/8/18
 * Time: 09:18
 */

function wp_ajax_post_change_status_post_effa( $params ){
    global $wpdb;
    $user = wp_get_current_user();
    $orgChartID = isset($user->orgchart_id) ? $user->orgchart_id : 0;
    $params = $wpdb->escape($params);
    $orgchart_id = $params['chart_id'];
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    $checkChildren = effa_site_orgchart_check_on_level($orgchart_id, $orgChartID);
    if( !user_is_manager() && empty($checkChildren) ){
        $message = __('<i class="fa fas fa-times"></i>' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],415, $message);
    }
    $orgchart = effa_site_orgchart_get_chart_by_id($orgchart_id);
    if( empty($orgchart) ){
        $message = __( 'Không tìm thấy chức danh. Vui lòng thử lại' ,TPL_DOMAIN_LANG);
        send_response_json(['error' => $message],404, $message);
    }
    $postID = $params['post_id'];
    $post = get_post( $postID );
    if( !$post ){
        $message = __( 'Không tìm thấy mục tiêu. Vui lòng thử lại' ,TPL_DOMAIN_LANG);
        send_response_json(['error' => $message],404, $message);
    }
    $year = date('Y', time());
    $created =  current_time( 'mysql' );
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableAssign = "{$prefix}assign_orgcharts";
    $tableEffa = "{$prefix}efficiency_assessment";
    $assignPostParent = effa_site_assign_check_post_group_by_post_children($postID, $orgchart_id, $year);
    $errors = [];
    $wpdb->query("START TRANSACTION;");
    $assign = effa_site_assign_get_by_postid_chartid_year($postID, $orgchart_id, $year);
    $insert = true;
    if( empty($assign) ){
        if( empty($assignPostParent) ){
            $dataParent = [
                'post_id' => $post->post_parent,
                'orgchart_id' => $orgchart_id,
                'year' => $year,
                'type' => 'group',
                'created' => $created
            ];
            $wpdb->insert($tableAssign, $dataParent);
            if( !empty($wpdb->last_error) ){
                $errors[] = $wpdb->last_error;
            }
        }
        //Add assign orgchart
        $data = [
            'post_id' => $postID,
            'orgchart_id' => $orgchart_id,
            'year' => $year,
            'type' => 'item',
            'created' => $created
        ];
        $wpdb->insert($tableAssign, $data);
        if( !empty($wpdb->last_error) ){
            $errors[] = $wpdb->last_error;
        }
    }else{
        //Delete Assign orgchart
        $insert = false;
        $wpdb->delete($tableEffa, ['assign_id' => $assign->id]);
        if( !empty($wpdb->last_error) ){
            $errors[] = $wpdb->last_error;
        }
        $data = [
            'post_id' => $postID,
            'orgchart_id' => $orgchart_id,
            'year' => $year,
            'type' => 'item',
        ];
        $wpdb->delete($tableAssign, $data);
        if( !empty($wpdb->last_error) ){
            $errors[] = $wpdb->last_error;
        }

        $findPostChildren = effa_site_assign_exists_post_children_by_post_parent($post->post_parent, $orgchart_id, $year);
        // Delete post group in table Assign
        if( empty($findPostChildren) ){
            $data = [
                'post_id' => $post->post_parent,
                'orgchart_id' => $orgchart_id,
                'year' => $year,
                'type' => 'group',
            ];
            $wpdb->delete($tableAssign, $data);
        }
        if( !empty($wpdb->last_error) ){
            $errors[] = $wpdb->last_error;
        }
    }
    if( !empty($errors) ){
        # @TODO process error
        $wpdb->query("ROLLBACK;");
        if( $insert ) {
            $checked = false;
        }else{
            $checked = true;
        }
        $message = __('<i class="fa fas fa-times"></i>' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message, 'checked' => $checked, $errors],415, $message);
    }else{
        $wpdb->query("COMMIT;");
        if( $insert ) {
            $message = __('<i class="fa fas fa-check"></i>', TPL_DOMAIN_LANG);
        }else{
            $message = __('<i class="fa fas fa-check"></i>', TPL_DOMAIN_LANG);
        }
        send_response_json(['success' => $message], 201, $message);
    }
}