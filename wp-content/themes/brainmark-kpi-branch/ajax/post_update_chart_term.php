<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 13/03/2018
 * Time: 23:23
 */

function wp_ajax_post_update_chart_term( $params ){
	$params = wp_slash($params);
	$user = wp_get_current_user();
	if (!$user) {
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG);
		$httpCode = 401;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	if( !user_is_manager() ){
		$message = __('Bạn không được phép chỉnh sửa.', TPL_DOMAIN_LANG);
		$httpCode = 403;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	add_filter('wp_update_term_data', function($data, $term_id, $taxonomy, $args) use ($params){
		$data['chart_id'] = $params['chart_id'];
		return $data;
	});
	$term = get_term_by("id", $params['term_id'], 'category');
	if( !empty( $term ) && !is_wp_error( $term ) ){
		$updateTerm = wp_update_term($term->term_id, 'category', $params);
		if( !empty( $updateTerm ) && !is_wp_error( $updateTerm ) ){
			$message = "Thành công";
			$httpCode = 201;
			send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
		}else{
			$message = "Có lỗi xảy ra vui lòng thử lại";
			$httpCode = 401;
			send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
		}
	}else{
		$message = "Không tìm thấy hệ thống chức danh";
		$httpCode = 404;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
}