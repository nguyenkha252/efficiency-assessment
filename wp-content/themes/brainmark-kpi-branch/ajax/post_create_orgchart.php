<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/12/17
 * Time: 00:21
 */

require_once THEME_DIR . '/inc/lib-orgchart.php';

function wp_ajax_post_create_orgchart( $params ) {
    $user = wp_get_current_user();
    if( !wp_verify_nonce($params['_wpnonce'], 'create_orgchart') ) {
        #$error = new WP_Error(401, __("Nonce invalid", TPL_DOMAIN_LANG));
        #send_response_json(['code' => $error->get_error_code(), 'message' => $error->get_error_message()], $error->get_error_code(), $error->get_error_message());
    }
    if( empty($user->allcaps['level_9']) && !$user->has_cap('level_9') ) {
        $error = new WP_Error(403, __("Not allow user permission", TPL_DOMAIN_LANG));
        send_response_json(['code' => $error->get_error_code(), 'message' => $error->get_error_message()], $error->get_error_code(), $error->get_error_message());
    }
    $orgchart_id = kpi_orgchart_insert($params['orgchart']);
    if( is_wp_error($orgchart_id) ) {
        send_response_json([
            'code' => $orgchart_id->get_error_code(), 'message' => $orgchart_id->get_error_message()
        ],
            $orgchart_id->get_error_code(), $orgchart_id->get_error_message());
    } else {
        if( $orgchart_id === false ) {
            send_response_json(__("Create failed", TPL_DOMAIN_LANG), 400, __("Create failed", TPL_DOMAIN_LANG));
        }
        $jsonData = kpi_orgchart_get_by('id', $orgchart_id);
        send_response_json($jsonData, 201, __("Created", TPL_DOMAIN_LANG));
    }
}