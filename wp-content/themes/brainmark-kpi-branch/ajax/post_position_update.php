<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/19/18
 * Time: 10:37
 */

function wp_ajax_post_position_update($params) {
    $nonce = !empty($params['_wpnonce']) ? $params['_wpnonce'] : '';
    if( !wp_verify_nonce($nonce, 'position_update') ) {
        $error = new WP_Error(401, __('Mã bảo vệ không hợp lệ', TPL_DOMAIN_LANG) );
        send_response_json(['code' => $error->get_error_code(), 'message' => $error->get_error_message()], $error->get_error_code(), $error->get_error_message());
    }
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableCharts = "{$prefix}kpi_charts";
    $result = $wpdb->update($tableCharts, ['x' => (int)$params['x'], 'y' => (int)$params['y']], ['ID' => (int)$params['ID']]);
    if( $wpdb->last_error ) {
        $msg = "Có lỗi xảy ra";
        send_response_json(['code' => 400, 'message' => $msg, 'sql' => $wpdb->last_query, 'errors' => ['db' => $wpdb->last_error]], 400, $msg);
    }
    send_response_json($result, 202, 'Updated');
}