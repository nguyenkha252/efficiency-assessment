<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/9/18
 * Time: 18:44
 */


function wp_ajax_post_kpi_year_update($params) {
    require_once THEME_DIR . '/ajax/post_kpi_year_add.php';
    $params['is_update'] = true;
    if( empty($params['year_id']) ) {
        $params['is_update'] = false;
    }
    return wp_ajax_post_kpi_year_add($params);
}