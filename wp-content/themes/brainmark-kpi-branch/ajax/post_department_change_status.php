<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 18/01/2018
 * Time: 23:44
 */
function wp_ajax_post_department_change_status( $params ){
	global $wpdb;
	$nonce = isset($params['_wpnonce']) ? $params['_wpnonce'] : '';

	if( !wp_verify_nonce($nonce, 'kpi_change_status') ) {
        $message = __('Sai mã bảo vệ', TPL_DOMAIN_LANG );
        $httpCode = 403;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }

	$user = wp_get_current_user();
	$orgchart = user_load_orgchart( $user );
	$postarr = wp_slash($params);
	if( !$user ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
		$httpCode = 401;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	if( !in_array($orgchart ? $orgchart->role : '', ['phongban', 'bgd'] ) ){
		$message = __('Bạn không có quyền chỉnh sửa', TPL_DOMAIN_LANG );
		$httpCode = 403;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	$prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
	$tableKpi = "{$prefix}kpis";
	$location = '';
	require_once THEME_DIR . '/inc/lib-users.php';
	require_once THEME_DIR . '/inc/lib-kpis.php';

	if( !empty( $postarr['kpiIDs'] ) ){
		$dataUpdate = ['aproved' => 1, 'status' => KPI_STATUS_PENDING];
		$wpdb->query( "START TRANSACTION;" );
		foreach ( $postarr['kpiIDs'] as $kpiID){
			if( ($orgchart ? $orgchart->role : '') === 'phongban' ){
				$result = get_kpi_by_user_orgchart_kpi_id( $user->ID, $user->orgchart_id, $kpiID );
				if( $result ){
					$wpdb->update( $tableKpi, $dataUpdate, ['id' => $kpiID] );
					if( !empty( $wpdb->last_error ) ){
						$wpdb->query("ROLLBACK;");
                        $httpCode = 400;
                        $message = __("Có lỗi xảy ra", TPL_DOMAIN_LANG );
                        send_response_json(['code' => $httpCode, 'message' => $message, 'error' => $wpdb->last_error], $httpCode, $message);
					}
				} else {
					$message = __('Không tìm thấy KPI', TPL_DOMAIN_LANG );
					$httpCode = 404;
					send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
				}
			} else {
				#BGD
				$result = kpi_get_kpi_by_id($kpiID);
				if( $result ) {
					$wpdb->update( $tableKpi, $dataUpdate, ['id' => $kpiID] );
					if( !empty( $wpdb->last_error ) ){
						$wpdb->query("ROLLBACK;");$httpCode = 400;
                        $message = __("Có lỗi xảy ra", TPL_DOMAIN_LANG );
                        send_response_json(['code' => $httpCode, 'message' => $message, 'error' => $wpdb->last_error], $httpCode, $message);
					}
				} else {
					$message = __('Không tìm thấy KPI', TPL_DOMAIN_LANG );
					$httpCode = 404;
					send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
				}
			}
		}
		$wpdb->query("COMMIT;");
		$message = __('Lưu thành công', TPL_DOMAIN_LANG);
		$httpCode = 201;
		send_response_json(
			['code' => $httpCode, 'message' => $message], // , 'location' => $location
			$httpCode,
			$message
		);
	}else{
		$message = __('Không tìm thấy KPI' ,TPL_DOMAIN_LANG );
		$httpCode = 404;
		send_response_json(
			['code' => $httpCode, 'message' => $message],
			$httpCode,
			$message
		);
	}

}