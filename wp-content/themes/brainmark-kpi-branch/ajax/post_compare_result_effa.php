<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/8/18
 * Time: 2:44 PM
 */

function wp_ajax_post_compare_result_effa($params){
    global $wpdb;
    $user = wp_get_current_user();
    $params = $wpdb->escape($params);
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    if( !user_is_manager() ){
        $message = __('Bạn không có quyền thực hiện thao tác này' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],403, $message);
    }
    if( !wp_verify_nonce($params['_wpnonce'], 'compare_result_effa') ){
        $message = __('Phiên bản đã hết hạn. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],408, $message);
    }
    $assign_lv_id = isset($params['lv']) ? $params['lv'] : 0;
    $value = isset($params['value']) ? $params['value'] : '';
    $assignLV = effa_site_level_up_assign_get_by_id($assign_lv_id);
    if( empty($assignLV) ){
        $message = __('Không tìm thấy năng lực' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],404, $message);
    }

    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableAssignLevelUp = "{$prefix}assign_level_up";
    $wpdb->query("START TRANSACTION;");
    $wpdb->update($tableAssignLevelUp, ['status' => $value], ['id' => $assignLV->id]);

    if( !empty($wpdb->last_error) ){
        # @TODO process error
        $wpdb->query("ROLLBACK;");
        $message = __('Đánh giá thất bại' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],415, $message);
    }else{
        $wpdb->query("COMMIT;");
        $message = __('Thành công', TPL_DOMAIN_LANG);
        send_response_json(['success' => $message], 201, $message);
    }
}