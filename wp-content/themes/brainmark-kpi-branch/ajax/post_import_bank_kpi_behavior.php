<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 2/5/18
 * Time: 15:43
 */

#require_once THEME_DIR . '/inc/vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';
require_once THEME_DIR . '/inc/vendor/autoload.php';
function wp_insert_post_data_violation_assessment($data_post, $postarr){
    if( array_key_exists('violation_assessment', $postarr)  ){
        $data_post['violation_assessment'] = $postarr['violation_assessment'];
    }
    return $data_post;
}
function wp_ajax_post_import_bank_kpi_behavior( $params ){
    global $wpdb;
    $user = wp_get_current_user();
    $params = wp_slash($params);
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    if( !wp_verify_nonce($params['_wpnonce'], 'import_bank') ){
        $message = __('Phiên bản đã hết hạn. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],408, $message);
    }
    if( empty($user->allcaps['create_posts']) && !$user->has_cap('create_posts') ) {
        $message = __('Bạn không được cấp quyền import file' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],402, $message);
    }elseif( empty($user->allcaps['edit_posts']) && !$user->has_cap('edit_posts') ){
        $message = __('Bạn không được cấp quyền import file' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],405, $message);
    }

    $upload = wp_upload_dir();
    $allowType = ['application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
    $files = $_FILES['imports'];
    $fileImport = "";
    if( isset($files) && !empty( $files['name'] ) ) {
        $typeFiles = $files['type'];
        $nameFiles = $files['name'];
        $errorFiles = $files['error'];
        $sizeFiles = $files['size'];
        if ($errorFiles == UPLOAD_ERR_OK && $sizeFiles > 0) {
            $fileImport = "{$upload['basedir']}/imports/{$nameFiles}";
            $params['imports'] = "{$upload['baseurl']}/imports/{$nameFiles}";
            if (!empty($typeFiles)) {
                foreach ($typeFiles as $ktype => $typefile) {
                    if (!in_array($typefile, $allowType) || $sizeFiles[$ktype] > 5242880 || $errorFiles[$ktype] > 0) {
                        $msg = !in_array($typefile, $allowType) ? __('Tải tập tin không đúng định dạng', TPL_DOMAIN_LANG) : __('Vui lòng tải tập tin < 5M', TPL_DOMAIN_LANG);
                        $httpCode = 420;
                        send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
                    }
                }
            }
            if (!is_dir("{$upload['basedir']}/imports")) {
                mkdir("{$upload['basedir']}/imports", 0777, true);
            }
            $isUpload = move_uploaded_file($_FILES['imports']['tmp_name'], $fileImport);
            if (!$isUpload) {
                $msg = __("Import Ngân hàng KPI thất bại. Vui lòng thử lại.", TPL_DOMAIN_LANG);
                $httpCode = 410;
                send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
            } else {
                try {
                    $identify = PHPExcel_IOFactory::identify($fileImport);
                    $objReader = PHPExcel_IOFactory::createReader($identify);
                    $objPHPExcel = $objReader->load($fileImport);

                } catch (Exception $e) {
                    die('Lỗi không thể đọc file "' . pathinfo($fileImport, PATHINFO_BASENAME) . '": ' . $e->getMessage());
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                $data = [];
                $groupID = 0;
                for ($row = 2; $row <= $highestRow; $row++) {
                    // Lấy dữ liệu từng dòng và đưa vào mảng $rowData
                    $dataTemp[$row] = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE)[0];
                    unset($dataTemp[$row][0]);
                    $dataRow = array_values($dataTemp[$row]);
                    if ($row == 2 && !empty($dataRow[1])) {
                        $groupID++;
                        $data[$groupID] = [
                            'group_name' => __("Nhóm thái độ hành vi", TPL_DOMAIN_LANG)
                        ];
                    } elseif ( $dataRow[1] === NULL) {
                        $groupID++;
                        $data[$groupID] = [
                            'group_name' => $dataRow[0]
                        ];
                    } else {
                        $data[$groupID]["group_item"][] = $dataRow;
                    }
                }
                # START TRANSACTION;

                if (!empty($data)) {
                    if( array_key_exists('options', $params) && $params['options'] == 'removeall' ){
                        $catID = $params["post_category"][0];
                        $args_post = [
                            'posts_per_page' => '-1',
                            'category__in' => $catID,
                            'order' => 'ASC',
                            'orderby' => 'post_date',
                            'post_type' => 'post',
                            'post_status' => ['draft', 'publish']
                        ];
                        $query = new WP_Query($args_post);
                        #print_r( $query->found_posts );
                        if( $query->found_posts > 0 ){
                            $wpdb->query("START TRANSACTION;");
                            foreach ( $query->get_posts() as $key => $item ){
                                $deleted = wp_delete_post($item->ID, true);
                                if( empty( $deleted ) ){
                                    $wpdb->query("ROLLBACK;");
                                    $msg = __('Import Ngân hàng KPI thất bại. Vui lòng thử lại', TPL_DOMAIN_LANG);
                                    $httpCode = 410;
                                    send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
                                }
                            }
                            $wpdb->query("COMMIT;");
                        }
                    }
                    $dataParent = [
                        "post_status" => "publish",
                        "post_title" => "",
                        "post_parent" => 0,
                        "post_category" => !empty($params["post_category"]) ? $params["post_category"] : "",
                    ];
                    $dataChildren = [
                        "post_status" => "draft",
                        "post_title" => "",
                        "post_parent" => 0,
                        "meta_input" => ["target" => ""],
                        "post_category" => !empty($params["post_category"]) ? $params["post_category"] : "",
                    ];
                    $wpdb->query("START TRANSACTION;");
                    foreach ($data as $key => $item) {
                        $dataGroup = $item['group_name'];
                        $dataParent["post_title"] = $dataGroup;
                        $post_ID = wp_insert_post($dataParent);
                        if (is_numeric($post_ID)) {
                            if (array_key_exists('group_item', $item)) {
                                foreach ($item['group_item'] as $k => $postarr) {
                                    $dataChildren["post_title"] = str_replace("&#8211;", "-", $postarr[0] );
                                    $dataChildren['violation_assessment'] = $postarr[1];
                                    $dataChildren["post_parent"] = (int)$post_ID;
                                    add_filter( 'wp_insert_post_data', 'wp_insert_post_data_violation_assessment', 10, 2);
                                    $postID = wp_insert_post($dataChildren);
                                    if (!is_numeric($postID)) {
                                        $wpdb->query("ROLLBACK;");
                                        $msg = __('Import Ngân hàng KPI thất bại. Vui lòng thử lại', TPL_DOMAIN_LANG);
                                        $httpCode = 410;
                                        send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
                                    }
                                    remove_filter( 'wp_insert_post_data', 'wp_insert_post_data_violation_assessment', 10, 2);
                                }
                            }
                        } else {
                            $wpdb->query("ROLLBACK;");
                            $msg = __('Import Ngân hàng KPI thất bại. Vui lòng thử lại', TPL_DOMAIN_LANG);
                            $httpCode = 410;
                            send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
                        }
                    }
                    $wpdb->query("COMMIT;");
                    $msg = __('Import Ngân hàng KPI thành công', TPL_DOMAIN_LANG);
                    $httpCode = 201;
                    send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
                }
            }

        } else {
            if (isset($params['imports'])) {
                unset($params['imports']);
            }
        }
    }else{
        $msg = __("Import Ngân hàng KPI thất bại. Vui lòng thử lại.", TPL_DOMAIN_LANG);
        $httpCode = 410;
        send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
    }
}