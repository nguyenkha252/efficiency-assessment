<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/22/18
 * Time: 10:53
 */
function wp_ajax_post_del_formula( $params ){
    global $wpdb;
    require_once THEME_DIR . '/inc/lib-formulas.php';
    $prefix       = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableKpiFormulas     = "{$prefix}kpi_formulas";
    $user = wp_get_current_user();
    $params = wp_slash( $params );
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
        $httpCode = 401;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }
    if( !wp_verify_nonce($params['_wpnonce'], 'delete_formula') ){
        $msg = __('Phiên bản đã hết hạn. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
        $httpCode = 408;
        send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
    }
    $result = get_formula_by_id( $params['ID'] );
    if( empty( $result ) ){
        $msg = __('Không tìm thấy công thức', TPL_DOMAIN_LANG);
        $httpCode = 404;
        send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
    }
    $wpdb->delete( $tableKpiFormulas, ['ID' => $params['ID']] );
    if( $wpdb->last_error ){
        $msg = __('Xóa công thức thất bại. Vui lòng thử lại' ,TPL_DOMAIN_LANG );
        $httpCode = 415;
        send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
    }
    $msg = __('Thành công', TPL_DOMAIN_LANG);
    $httpCode = 201;
    send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
}