<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/22/18
 * Time: 4:56 PM
 */

function wp_ajax_post_effa_add_level_up($params){
    global $wpdb;
    $params = wp_slash($params);
    $orgchart_new = isset($params['orgchart_new']) ? $params['orgchart_new'] : 0;
    $year = isset($params['year']) ? $params['year'] : 0;
    $user_ids = isset($params['user_id']) ? $params['user_id'] : [];
    $user = wp_get_current_user();
    if( !wp_verify_nonce($params['_wpnonce'], "effa_add_level_up") ){
        $message = __('Phiên bản đã hết hạn. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],408, $message);
    }
    if( empty($user_ids) ){
        $message = __('Vui lòng chọn ít nhất một nhân viên để thực hiện' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],404, $message);
    }
    if( !user_is_manager() ){
        $message = __('Bạn không có quyền thực hiện thao tác này' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],403, $message);
    }
    $getOrgchartNew = effa_site_orgchart_get_chart_by_id($orgchart_new);
    if( empty($getOrgchartNew) ){
        $message = __('Không tìm thấy chức danh' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],404, $message);
    }
    $efficiency = effa_site_efficiency_get_chartid_year($orgchart_new, $year);
    if( empty($efficiency) ){
        $message = __('Chức danh hiện tại chưa được triển khai năng lực năm ' ,TPL_DOMAIN_LANG ) . $year;
        send_response_json(['error' => $message],404, $message);
    }
    $created =  current_time( 'mysql' );
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $table_assign_level_up = "{$prefix}assign_level_up";
    $table_efficiency_level_up = "{$prefix}efficiency_level_up";
    $data_insert = [];
    $default_value = '___VALUE_LEVEL_UP___';
    foreach ($efficiency as $k => $item){
        $string_values_insert[] = "";
        $data_insert[] = [
            'assign_id' => $item->assign_id,
            'assign_level_up' => $default_value,
            'important' => $item->important,
            'standard' => $item->standard,
            'created' => $wpdb->prepare('%s', $created),
        ];
    }
    $wpdb->query("START TRANSACTION;");
    foreach ($user_ids as $k => $uid){
        $this_user = effa_site_get_user_by_id($uid);
        $this_orgchart = effa_site_user_load_orgchart($this_user);
        if( empty($this_user) ){
            $wpdb->query("ROLLBACK;");
            $message = __('Không tìm thấy nhân viên' ,TPL_DOMAIN_LANG );
            send_response_json(['error' => $message],404, $message);
        }
        if( $this_orgchart->level < $getOrgchartNew->level ){
            $wpdb->query("ROLLBACK;");
            $message = sprintf(__("Không thể thực hiện. Hiện tại nhân viên (%s) có chức danh cao hơn chức danh hiện tại" ,TPL_DOMAIN_LANG ), $this_user->display_name);
            send_response_json(['error' => $message],413, $message);
        }elseif($this_orgchart->level == $getOrgchartNew->level){
            $wpdb->query("ROLLBACK;");
            $message = sprintf(__("Không thể thực hiện. Hiện tại nhân viên (%s) có chức danh bằng chức danh hiện tại" ,TPL_DOMAIN_LANG ), $this_user->display_name);
            send_response_json(['error' => $message],414, $message);
        }
        //kiểm tra nhân viên đó có đang được so sánh với chức danh nào không
        $userALU = effa_site_level_up_get_by_user_status($this_user->ID);
        if( !empty($userALU) ){
            $wpdb->query("ROLLBACK;");
            $message = sprintf(__("Nhân viên (%s) đang được so sánh với chức danh khác. Vui lòng chọn nhân viên khác." ,TPL_DOMAIN_LANG ), $this_user->display_name);
            send_response_json(['error' => $message],415, $message);
        }
        $data_assign_level_up = [
            'assign_author'     => $user->ID,
            'user_id'           => $this_user->ID,
            'orgchart_id_old'   => $this_user->orgchart_id,
            'orgchart_id_new'   => $orgchart_new,
            'status'            => 'publish',
            'created'           => $created
        ];
        $wpdb->insert($table_assign_level_up, $data_assign_level_up);
        if( !empty($wpdb->last_error) ){
            $wpdb->query("ROLLBACK;");
            $message = __('Đã xảy ra lỗi trong quá trình so sánh. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
            send_response_json(['error' => $message],410, $message);
        }
        $assign_level_up_id = $wpdb->insert_id;
        $data_efficiency_level_up = array_map(function($item) use($default_value, $assign_level_up_id){
            $item['assign_level_up'] = str_replace($default_value, $assign_level_up_id, $item['assign_level_up']);
            $string = sprintf("(%s)", implode(", ", $item));
            return $string;
        }, $data_insert);
        if( !empty($data_efficiency_level_up) ){
            $str_insert = implode(", ", $data_efficiency_level_up);
            $sql_insert = "INSERT INTO `{$table_efficiency_level_up}` (`assign_id`, `assign_level_up`, `important`, `standard`, `created`) VALUES {$str_insert}";
            $wpdb->query($sql_insert);
            if( !empty($wpdb->last_error) ){
                $wpdb->query("ROLLBACK;");
                $message = __('Đã xảy ra lỗi trong quá trình so sánh. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
                send_response_json(['error' => $message],411, $message);
            }
        }else{
            $wpdb->query("ROLLBACK;");
            $message = __('Đã xảy ra lỗi trong quá trình so sánh. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
            send_response_json(['error' => $message],412, $message);
        }
    }
    $wpdb->query("COMMIT;");
    $message = __('Thành công.' ,TPL_DOMAIN_LANG );
    send_response_json(['message' => $message], 201, $message);

}