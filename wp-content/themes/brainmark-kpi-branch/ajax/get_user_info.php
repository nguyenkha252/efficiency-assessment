<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/12/17
 * Time: 00:21
 */

function kpi_get_user_by($field, $value) {
    $params = ['single' => true, 'field' => $field, 'value' => $value];
    return kpi_get_user_info($params);
}

function kpi_get_user_info( $params ) {
    $user = wp_get_current_user();
    global $wpdb;
    if( empty($params['id']) ) {
        $params['id'] = 0;
    }
    $single = isset($params['single']) ? boolval($params['single']) : false;
    if( $params['id'] === 0 ) {
        $single = false;
    }
    $table_prefix = $wpdb->get_blog_prefix( $wpdb->blogid );
    $user_level = $table_prefix."user_level";

    $where = '';
    if( isset($params['field']) && isset($params['value']) ) {
        $params[$params['field']] = $params['value'];
        if( $params['field'] != 'id' ) {
            $where = $wpdb->prepare("{$params['field']} = %s", $params['value']);
        }
    }
    $limit = '';
    if( isset($params['ids']) ) {
        $params['ids'] = (array)$params['ids'];
        if( !$single ) {
            $params['ids'] = array_map('intval', $params['ids']);
            $where .= ' AND u.ID IN (' . implode(', ', $params['ids']) . ") ";
            $params['paged'] = isset($params['paged']) ? intval($params['paged']) : 1;
            $params['limit'] = isset($params['limit']) ? intval($params['limit']) : 10;
            $params['paged'] = max(1, $params['paged']);
            $offset = ($params['paged']-1) * $params['limit'];
            $limit = " LIMIT {$offset}, {$params['limit']}";
        }
    } else {
        if( $single ) {
            $where .= $wpdb->prepare(' AND u.ID = %d ', $params['id']);
            $limit = " LIMIT 0, 1";
        }
    }

    if( !empty($params['exclude']) ) {
        $params['exclude'] = (array)$params['exclude'];
        $params['exclude'] = array_map('intval', $params['exclude']);
        $where .= " AND u.ID NOT IN (" . implode(', ', $params['exclude']) . ") ";
    }
    $blog_id = get_current_blog_id();
    $blog_prefix = $wpdb->get_blog_prefix($blog_id);

    $sql = $wpdb->prepare(
        "SELECT u.*, c.name as orgchart_name, m1.meta_value as `first_name`, m2.meta_value as `last_name`, \n".
        "c.parent as `orgchart_parent`, c.room as `orgchart_room`, c.`alias`, ".
        "m3.meta_value as `description`, m4.meta_value as `role` \n".
        "FROM `{$blog_prefix}users` as u \n".
        "LEFT JOIN `{$blog_prefix}usermeta` as m ON (m.user_id = u.ID) AND (m.meta_key LIKE 'user_parent') \n".
        "LEFT JOIN `{$blog_prefix}usermeta` as m1 ON (m1.user_id = u.ID) AND (m1.meta_key LIKE 'first_name') \n".
        "LEFT JOIN `{$blog_prefix}usermeta` as m2 ON (m2.user_id = u.ID) AND (m2.meta_key LIKE 'last_name') \n".
        "LEFT JOIN `{$blog_prefix}usermeta` as m3 ON (m3.user_id = u.ID) AND (m3.meta_key LIKE 'description') \n".
        "LEFT JOIN `{$blog_prefix}usermeta` as m4 ON (m4.user_id = u.ID) AND (m4.meta_key LIKE %s) \n".
        "LEFT JOIN `{$blog_prefix}org_charts` as c ON (c.id = u.orgchart_id) \n".
        "WHERE 1 = 1 {$where} ".
        "ORDER BY c.parent ASC {$limit}", $user_level); # exit("<pre>{$sql}</pre>");
    if( $single ) {
        $row = $wpdb->get_row( $sql );
        if( empty($row) ) {
            $jsonData = new WP_Error(404, sprintf( __('User not found with ID %s', TPL_DOMAIN_LANG), $params['id']) );
        } else {
            $jsonData = [
                'id' => $row->ID,
                'name' => "{$row->first_name} {$row->last_name}",
                'parent' => empty($row->orgchart_parent) ? 0 : $row->orgchart_parent,
                'user_email' => $row->user_email,
                'user_login' => $row->user_login,
                'user_nicename' => $row->user_nicename,
                'display_name' => $row->display_name,
                'room' => $row->orgchart_room,
                'role' => $row->alias,
                'avatar' => $row->user_url,
                'orgchart_id' => __($row->orgchart_id, TPL_DOMAIN_LANG),
                'orgchart_name' => __($row->orgchart_name, TPL_DOMAIN_LANG),
            ];
        }
    } else {
        $rows = $wpdb->get_results( $sql );
        # echo '<pre>' . "{$sql}\n\n"; var_dump($rows); exit('</pre>');
        if( empty($rows) ) {
            $jsonData = ['total' => 0, 'items' => []];
        } else {
            $jsonData = [];
            foreach($rows as $row) {
                $jsonData[] = [
                    'id' => $row->ID,
                    'name' => "{$row->first_name} {$row->last_name}",
                    'parent' => intval($row->orgchart_parent),
                    'user_email' => $row->user_email,
                    'user_login' => $row->user_login,
                    'user_nicename' => $row->user_nicename,
                    'display_name' => $row->display_name,
                    'room' => $row->orgchart_room,
                    'role' => $row->alias,
                    'avatar' => $row->user_url,
                    'orgchart_id' => __($row->orgchart_id, TPL_DOMAIN_LANG),
                    'orgchart_name' => __($row->orgchart_name, TPL_DOMAIN_LANG),
                ];
            }
            $jsonData = ['total' => count($jsonData), 'items' => $jsonData];
        }
    }
    return $jsonData;
}


function kpi_get_users($params = []) {
    global $wpdb;
    $params = wp_slash($params);
    $blog_id = get_current_blog_id();
    $blog_prefix = $wpdb->get_blog_prefix($blog_id);
    $paged = isset($params['trang']) ? max(1, intval($params['trang'])) : 1;
    $limit = isset($params['limit']) ? max(1, intval($params['limit'])) : 100;
    $params['room_id'] = isset($params['room_id']) ? intval($params['room_id']) : 0;
    $params['chart_id'] = isset($params['chart_id']) ? intval($params['chart_id']) : 0;
    $params['trang'] = $paged;
    $params['limit'] = $limit;
    $params['tim'] = isset($params['tim']) ? strip_tags($params['tim']) : '';

    $where = "";
    $chart_ids = [];
    if( $params['room_id'] > 0 ) {
        $chart_ids = [];
        $charts = kpi_get_list_org_charts($params['room_id'], true);
        foreach ($charts as $item) {
            if( !in_array($item['id'], $chart_ids) ) {
                $chart_ids[] = $item['id'];
            }
            if( !empty($item['children']) ){
                foreach ($item['children'] as $k => $item_2){
                    if( !in_array($item_2['id'], $chart_ids) ) {
                        $chart_ids[] = $item_2['id'];
                    }
                }
            }
        }
        if( !empty($chart_ids) ) {
            $ids = implode(', ', $chart_ids);
            $where .= " AND (u.`orgchart_id` IN ({$ids}) ) ";
        }
    }
    if( $params['chart_id'] > 0 ) {
        $chart_ids = [];
        $charts = kpi_get_list_org_charts($params['chart_id'], true);
        foreach ($charts as $item) {
            if( !in_array($item['id'], $chart_ids) ) {
                $chart_ids[] = $item['id'];
            }
        }
        if( !empty($chart_ids) ) {
            $ids = implode(', ', $chart_ids);
            $where .= " AND (u.`orgchart_id` IN ({$ids}) ) ";
        }
    }
    $user = wp_get_current_user();

    if( $params['room_id'] == 0 && $params['chart_id'] == 0 ) {
        if( !kpi_is_user_ceo() ) {
            $chart_ids = [];
            $charts = kpi_get_list_org_charts($user->orgchart_id, true);
            foreach ($charts as $item) {
                if( !in_array($item['id'], $chart_ids) ) {
                    $chart_ids[] = $item['id'];
                }
            }
            if( !empty($chart_ids) ) {
                $ids = implode(', ', $chart_ids);
                $where .= " AND (u.`orgchart_id` IN ({$ids}) ) ";
            }
        }
    }

    $offset = ($paged-1)*$limit;
    $limit = "LIMIT {$offset}, {$limit}";

    static $_results = [];

    $ids = array_map('intval', $chart_ids);
    $ids = implode(', ', $ids);

    $params['ids'] = $ids;

    $_key = md5( serialize($params) );

    if( !empty($params['tim']) ) {
        $chinhxac = isset($params['chinhxac']) ? intval($params['chinhxac']) : 0;
        if( $chinhxac ) {
            $chinhxac = "{$params['tim']}";
        } else {
            $chinhxac = "%{$params['tim']}%";
        }
        $where .= $wpdb->prepare(" AND ( (u.`display_name` LIKE %s) ".
            "OR (u.`user_email` LIKE %s) ".
            " ) ", # OR (c.`name` LIKE %s) OR (c.`room` LIKE %s)
            $chinhxac, $chinhxac); # , $chinhxac, $chinhxac
    }

    if( !isset($_results[$_key]) ) {
        $_results[$_key] = [];
        $_results[$_key]['items'] = $wpdb->get_results(
            "SELECT SQL_CALC_FOUND_ROWS u.*, c.`has_kpi`, c.`alias`, c.`room`, ".
            "(CASE ".
            "   WHEN (c.`alias` LIKE 'tapdoan') THEN 'bgd' ".
            "   WHEN (c.`alias` LIKE 'congty') THEN 'bgd' ".
            "   WHEN (c.`alias` LIKE 'phongban') THEN 'phongban' ".
            "   WHEN (c.`alias` LIKE 'nhanvien') THEN 'nhanvien' ".
            "END) as `role`, ".
            "c.name as orgchart_name, c.`level` as `orgchart_level`, c.parent as `orgchart_parent`, c1.name as `orgchart_parent_name` ".
            "FROM `{$blog_prefix}users` as u \n".
            "LEFT JOIN `{$blog_prefix}org_charts` as c ON (c.id = u.orgchart_id) \n".
            "LEFT JOIN `{$blog_prefix}org_charts` as c1 ON (c1.id = c.`parent`) \n".
            "INNER JOIN {$wpdb->usermeta} AS m ON m.user_id = u.ID \n" .
            "WHERE (1 = 1) AND (u.`spam` = 0) AND (u.`deleted` = 0)
                AND m.meta_key = '{$blog_prefix}capabilities' AND m.meta_value NOT LIKE '%user_employers%' AND m.meta_value NOT LIKE '%user_managers%'   
                {$where} ".
            "ORDER BY u.orgchart_id ASC, c.parent ASC, u.ID ASC {$limit}", ARRAY_A);

         #echo "<pre>{$wpdb->last_query}</pre>";

        $_results[$_key]['items'] = array_map(function ($item) {
            unset($item['user_pass']);
            unset($item['user_registered']);
            unset($item['user_activation_key']);
            unset($item['role_id']);
            unset($item['spam']);
            unset($item['deleted']);
            return $item;
        }, $_results[$_key]['items']);

        $_results[$_key]['sql'] = $wpdb->last_query;
        $_results[$_key]['error'] = $wpdb->last_error;
        $_results[$_key]['total'] = $wpdb->get_var("SELECT FOUND_ROWS()" );
        $_results[$_key]['trang'] = $paged;
        $_results[$_key]['limit'] = $params['limit'];
    }
    return $_results[$_key];
}

function kpi_render_user_row($user, $level, $hasChildren) {
    ob_start();
    $sLevel = '';
    for($i=1; $i <= $level; $i++) {
        $sLevel = "{$sLevel}<span class='space-{$i}'></span>";
    }
    $profile = get_page_by_path( PAGE_PROFILE_PATH );
    # $profile = get_page_by_path( PAGE_MANAGE_MEMBERS );
    $url = esc_url( add_query_arg( 'profile', $user['id'], apply_filters( 'the_permalink', get_permalink( $profile ), $profile ) ) );
    $default_avatar = THEME_URL . '/assets/images/no-photo.jpg';
    ?>
    <tr class="row-level-<?php echo $level . ($hasChildren ? ' has-children' : ' is-leaf'); ?>" data-edit-url="<?php echo $url; ?>">
        <td class="id-col"><span class="level-<?php echo $level; ?>"><?php echo $sLevel; ?></span><label><input class="id" type="checkbox" name="user_id[]" value="<?php echo $user['id']; ?>"><span><?php echo $user['id']; ?></span></label></td>
        <td class="avatar-img"><img class="avatar"
                                    onerror="this.src = this.getAttribute('data-src');"
                                    src="<?php echo empty($user['avatar']) ? $default_avatar : $user['avatar']; ?>"
                                    data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                    style="max-width: 32px; max-height: 32px"></td>
        <td class="name"><?php echo $user['name']; ?></td>
        <td class="email"><?php echo $user['user_email']; ?></td>
        <td class="login"><?php echo $user['user_login']; ?></td>
        <td class="display_name"><?php echo $user['display_name']; ?></td>
        <td class="position_name"><?php echo __($user['orgchart_name'], TPL_DOMAIN_LANG); ?></td>
    </tr>
    <?php
    return ob_get_clean();
}

function kpi_render_users($trees, $level = 0) {
    $html = '';
    if( !empty($trees) ) {
        foreach($trees as $id => $item) {
            $hasChildren = !empty($item['children']);
            if( !empty($item['members']) ) {
                foreach($item['members'] as $user)
                    $html .= kpi_render_user_row($user, $level, $hasChildren);
            }
            if( $hasChildren ) {
                if( !empty($item['children']) ) {
                    $html .= kpi_render_users($item['children'], $level + 1);
                }
            }
        }
    }
    return $html;
}

function wp_ajax_get_user_info( $params ) {
    $user = wp_get_current_user();
    if( !wp_verify_nonce($params['_wpnonce'], 'user_info') ) {
        $jsonData = new WP_Error(401, __("Nonce invalid", TPL_DOMAIN_LANG));
    } else if( empty($user->allcaps['list_users']) && !$user->has_cap('list_users') ) {
        $jsonData = new WP_Error(403, __("Not allow user permission", TPL_DOMAIN_LANG));
    } else {
        $jsonData = kpi_get_user_info($params);
    }
    if( is_wp_error($jsonData) ) {
        send_response_json($jsonData, $jsonData->get_error_code(), $jsonData->get_error_message());
    } else {
        send_response_json($jsonData, 200);
    }
}

