<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/10/18
 * Time: 10:41 AM
 */

function wp_ajax_post_update_question_effa($params){
    global $wpdb;
    $user = wp_get_current_user();
    $params = wp_slash($params);
    if( !$user ){
        $message = __('Please! Login to use application' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    $postCode = isset($params['post_code']) ? $params['post_code'] : "";
    if( !wp_verify_nonce($params['_wpnonce'], 'action_question_'.$postCode) ){
        $message = __('Time has expired' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],408, $message);
    }
    if( empty($user->allcaps['create_posts']) && !$user->has_cap('create_posts') ) {
        $message = __("You haven't permission" ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],402, $message);
    }elseif( empty($user->allcaps['edit_posts']) && !$user->has_cap('edit_posts') ){
        $message = __("You haven't permission" ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],405, $message);
    }
    $year = date('Y', time());
    # START TRANSACTION;
    $wpdb->query("START TRANSACTION;");
    if( !empty($params['posts']) ){
        add_filter( 'wp_insert_post_data', 'wp_insert_post_data_column_post_code', 10, 2);
        foreach ( $params['posts'] as $k => $post){
            if( $k > NUMBER_OF_QUESTION ){
                break;
            }
            $postID = isset($post['ID']) ? $post['ID'] : 0;
            $post_title = isset($post['post_title']) ? strip_tags($post['post_title']) : "";
            $post_title = str_replace(str_split("\|"), "", $post_title);
            $anwser_1 = isset($post['_effa_anwser_1']) ? strip_tags($post['_effa_anwser_1']) : "";
            $anwser_2 = isset($post['_effa_anwser_2']) ? strip_tags($post['_effa_anwser_2']) : "";
            $anwser_3 = isset($post['_effa_anwser_3']) ? strip_tags($post['_effa_anwser_3']) : "";
            $anwser_4 = isset($post['_effa_anwser_4']) ? strip_tags($post['_effa_anwser_4']) : "";
            $anwser_5 = isset($post['_effa_anwser_5']) ? strip_tags($post['_effa_anwser_5']) : "";
            if( $post_title == '' || $anwser_1 == '' || $anwser_2 == '' || $anwser_3 == '' || $anwser_4 == '' || $anwser_5 == '' ){
                $wpdb->query("ROLLBACK;");
                $message = __("Vui lòng điền đầy đủ thông tin câu hỏi và câu trả lời" ,TPL_DOMAIN_LANG );
                send_response_json(['error' => $message],406, $message);
            }
            $dataQS = [
                "post_status" => "publish",
                "post_title" => $post_title,
                "post_type" => "questions",
                "post_parent" => 0,
                "post_code" => $postCode,
                "post_year" => $year,
                'meta_input' => [
                    '_effa_anwser_1' => $anwser_1,
                    '_effa_anwser_2' => $anwser_2,
                    '_effa_anwser_3' => $anwser_3,
                    '_effa_anwser_4' => $anwser_4,
                    '_effa_anwser_5' => $anwser_5,
                ]
            ];
            if( $postID > 0 ){
                #update
                $get_post = get_post($postID);
                if( !$get_post ){
                    $wpdb->query("ROLLBACK;");
                    $msg = __('Không tìm thấy câu hỏi ', TPL_DOMAIN_LANG) . $post_title;
                    $httpCode = 410;
                    send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
                }
                unset($dataQS['post_year']);
                unset($dataQS['post_code']);
                unset($dataQS['post_type']);
                unset($dataQS['post_parent']);
                unset($dataQS['post_status']);
                $dataQS['ID'] = $postID;
                $update = wp_update_post($dataQS);
                unset($dataQS['ID']);
                if( $update == 0 || is_wp_error($update) ){
                    $wpdb->query("ROLLBACK;");
                    $msg = __('Cập nhật câu hỏi thất bại. Vui lòng thử lại', TPL_DOMAIN_LANG);
                    $httpCode = 411;
                    send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
                }
            }else{
                #insert
                $insert_id = wp_insert_post($dataQS);
                if( $insert_id == 0 || is_wp_error($insert_id) ){
                    $wpdb->query("ROLLBACK;");
                    $msg = __('Cập nhật câu hỏi thất bại. Vui lòng thử lại', TPL_DOMAIN_LANG);
                    $httpCode = 412;
                    send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
                }
            }
        }
        remove_filter( 'wp_insert_post_data', 'wp_insert_post_data_column_post_code', 10, 2);
    }
    $wpdb->query("COMMIT;");
    $msg = __('Thành công', TPL_DOMAIN_LANG);
    $httpCode = 201;
    send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
}