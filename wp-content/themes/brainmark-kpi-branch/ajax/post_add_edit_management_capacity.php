<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 2/7/18
 * Time: 16:02
 */
function render_html_capacity_manager($item){
	$checked = '';
	if ( $item['status'] != KPI_STATUS_DRAFT ) {
		$checked = "checked";
	}
	return "
                                <td class=\"column-1 align-center\">IDX</td>
                                <td class=\"column-2\">".esc_attr( $item['post_title'] )."</td>
                                <td class=\"column-3 align-center\">".esc_attr( $item['level_1'] )."%</td>
                                <td class=\"column-4 align-center\">".esc_attr( $item['level_2'] )."%</td>
                                <td class=\"column-5 align-center\">".esc_attr( $item['level_3'] )."%</td>
                                <td class=\"column-6 align-center\">
									".esc_attr( $item['note'] )."
                                </td>
                                <td class=\"column-8\">
                                	<input type=\"hidden\" name=\"kpi_capacity[{$item['id']}][id]\"                                           value=\"{$item['id']}\"/>
                                    <a href=\"javascript:;\" data-id=\"{$item['id']}\"
                                       data-wpnonce=\"".wp_create_nonce( 'load_capacity')."\"
                                       data-action=\"load_capacity\" data-method=\"get\" data-toggle=\"modal\"
                                       data-target=\"#add-management-capacity\" class=\"edit action-edit\"
                                       title=\"".__( 'Chỉnh sửa', TPL_DOMAIN_LANG )."\"> <span
                                                class=\"glyphicon glyphicon-pencil\"></span> ".__( 'Chỉnh sửa', TPL_DOMAIN_LANG )."
                                    </a><br>
                                    <a href=\"javascript:;\" data-nonce=\"". wp_create_nonce( 'delete_capacity' )."\"
                                       data-id=\"{$item['id']}\" data-action=\"delete_capacity\"
                                       data-title=\"Bạn có chắc xóa năng lực quản lý hiện tại?\"
                                       data-target=\"#confirm-apply-kpi\" data-method=\"post\" class=\"delete\"
                                       title=\"".__( 'Xóa', TPL_DOMAIN_LANG )."\"><span
                                                class=\"glyphicon glyphicon-trash\"></span> ".__( 'Xóa', TPL_DOMAIN_LANG )."
                                    </a>
                                </td>
                            ";
}
require_once THEME_DIR . '/inc/lib-kpis.php';
function wp_ajax_post_add_edit_management_capacity( $params ){
    global $wpdb;
    $user = wp_get_current_user();
    $orgchart = user_load_orgchart($user);
    $params = wp_slash( $params );
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
	if( !user_is_manager() ){
		$message = __('Bạn không có quyền thêm/sửa', TPL_DOMAIN_LANG);
		$httpCode = 403;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
    if( !wp_verify_nonce($params['_wpnonce'], 'add_edit_management_capacitys') ){
        $message = __('Phiên bản đã hết hạn. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],408, $message);
    }
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableKpiCP = "{$prefix}kpis_capacity";
    extract( $params );
    $type_note = "chitiet";
    $type = CAPACITY_TIEU_CHI;
    $chart_id = $orgchart->id;
    $user_id = $user->ID;
    $modified = date('Y-m-d H:i:s', time());
    $status = KPI_STATUS_DRAFT;
    if( $params['status'] == 1 ){
    	$status = KPI_STATUS_RESULT;
    }
    $data = compact("year_id", "bank_id", "chart_id", "user_id", /*"level_1", "level_2", "level_3",*/ "note", "type_note", "type", "apply_of", "status", "modified");
    $insert_id = 0;
    $data_output = [];
    if( empty( $params['id'] ) ){
        #insert
        $wpdb->insert($tableKpiCP, $data);
        if( !empty( $wpdb->last_error ) ){
            $msg = "Có lỗi xảy ra khi lưu";
            $httpCode = 400;
            send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
        }
	    $insert_id = $wpdb->insert_id;
	    $data_output['action'] = 'add';
    }else{
        #update
        $result = capacity_get_kpi_and_bank_by_id( $params['id'] );
        if( !empty( $result ) ){
            $wpdb->update($tableKpiCP, $data, ['id' => $result['id']]);
            if( !empty( $wpdb->last_error ) ){
                $msg = "Có lỗi xảy ra khi lưu";
                $httpCode = 400;
                send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
            }
	        $insert_id = $result['id'];
	        $data_output['action'] = 'update';
        }else{
            $msg = "Không tìm thấy năng lực quản lý";
            $httpCode = 404;
            send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
        }
    }
	$data_output['html'] = '';

    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
	$tableKpiCP = "{$prefix}kpis_capacity";
	$sql = $wpdb->prepare("
          SELECT kc.*, p.post_title, p.level_1, p.level_2, p.level_3  
          FROM {$tableKpiCP} AS kc 
          INNER JOIN {$wpdb->posts} AS p ON p.ID = kc.bank_id 
          WHERE kc.id = %d ", $insert_id);
	$getResult = $wpdb->get_row( $sql, ARRAY_A );

    if( !empty( $getResult ) ){
	    $data_output['html'] = render_html_capacity_manager( $getResult );
		if( $data_output['action'] == 'add' ){
			$data_output['html'] = "<tr>{$data_output['html']}</tr>";
		}
    }
    $msg = "Thành công";
    $httpCode = 201;
    send_response_json(['code' => $httpCode, 'message' => $msg, 'data' => $data_output], $httpCode, $msg);
}