<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/6/18
 * Time: 14:13
 */

function wp_ajax_get_capacity_get_bank( $params ){
    global $wpdb;
    $user = wp_get_current_user();
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    if( !user_is_manager() ){
        $message = __('Bạn không có quyền xóa', TPL_DOMAIN_LANG);
        $httpCode = 403;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }
    $postarr = $wpdb->escape($params);
    $post_id = (int)$postarr['ID'];
    $post = get_post( $post_id );
    if( !$post ){
        $httpCode = 404;
        $message = __('Không tìm thấy tiêu chí' ,TPL_DOMAIN_LANG );
        send_response_json(['code' => $httpCode, 'message' => $message],404, $message);
    }
    $targetID = $post->ID;
    $level_1 = $post->level_1;
    $level_2 = $post->level_2;
    $level_3 = $post->level_3;
    /*if( !empty( $termKPI ) && !is_wp_error( $termKPI ) && count( $termKPI ) > 0 ){
        $typesKPIName = $termKPI[0]->name;
        $typesKPIID = $termKPI[0]->term_id;
    }*/
    $data = [
        'post_id' => $targetID,
        'level_1' => $level_1 . "%",
        'level_2' => $level_2 . "%",
        'level_3' => $level_3 . "%",
        #'measurement_formulas' => $mF

    ];
    send_response_json( ['data' => $data], 201, __('OK', TPL_DOMAIN_LANG ));
}