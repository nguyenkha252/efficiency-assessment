<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 09/03/2018
 * Time: 00:23
 */
require_once THEME_DIR . '/inc/lib-years.php';
function wp_ajax_post_update_behavior_percent( $params ){
	global $wpdb;
	$params = wp_slash($params);
	$user = wp_get_current_user();
	if (!$user) {
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG);
		$httpCode = 401;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	if( !user_is_manager() ){
		$message = __('Bạn không được phép chỉnh sửa.', TPL_DOMAIN_LANG);
		$httpCode = 403;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	$id = $params['id'];
	$result = kpi_get_year_by_id( $id );
	if(!empty( $result ) ){
		$prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
		$tableKpiYear = "{$prefix}kpi_years";
		$behavior_percent = $params['behavior_percent'];
		$wpdb->update($tableKpiYear,
			['behavior_percent' => $behavior_percent],
			['id' => $id] );
		if( !empty( $wpdb->last_error ) ){
			$message = __('Có lỗi xảy ra vui lòng thử lại.', TPL_DOMAIN_LANG);
			$httpCode = 400;
			send_response_json(['code' => $httpCode, 'message' => $message, 'error' => $wpdb->last_error],$httpCode, $message);
		}else{
			$wpdb->update($tableKpiYear,
				['behavior_percent' => $behavior_percent],
				['parent' => $id] );
			if( !empty( $wpdb->last_error ) ){
				$message = __('Có lỗi xảy ra vui lòng thử lại.', TPL_DOMAIN_LANG);
				$httpCode = 400;
				send_response_json(['code' => $httpCode, 'message' => $message, 'error' => $wpdb->last_error],$httpCode, $message);
			}
		}
		$httpCode = 201;
		$message = "Thành công";
		send_response_json(['code' => $httpCode, 'message' => $message],$httpCode, $message);
	}else{
		$message = __('Không tìm thấy năm thái độ hành vi' ,TPL_DOMAIN_LANG );
		$httpCode = 404;
		send_response_json(['code' => $httpCode, 'message' => $message],$httpCode, $message);
	}

}