<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 23/03/2018
 * Time: 14:22
 */
require_once THEME_DIR . '/inc/lib-users.php';
function wp_ajax_get_check_code_user( $params ){
	$params = wp_unslash($params);
	$user = wp_get_current_user();
	if( !$user ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
		$httpCode = 401;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	if( !user_is_manager() ){
		$message = __('Bạn không có quyền chỉnh sửa', TPL_DOMAIN_LANG);
		$httpCode = 403;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	if( empty($params['code']) && $params['code'] != '0'){
		$message = __('Mã nhân viên không được để trống', TPL_DOMAIN_LANG);
		$httpCode = 401;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	$checkCode = check_code_user( $params['code'], $params['ID'] );
	if( $checkCode ){
		$message = __('Mã nhân viên đã được sử dụng. Vui lòng đổi mã khác', TPL_DOMAIN_LANG);
		$httpCode = 401;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}else{
		$message = __('Có thể sử dụng mã nhân viên này', TPL_DOMAIN_LANG);
		$httpCode = 201;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
}