<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/16/18
 * Time: 09:29
 */
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-users.php';

function wp_ajax_get_load_members_kpi_target( $params ) {
    global $wpdb;

    $params = wp_unslash($params);
    $kpiID = isset($params['kpi_id']) ? (int) $params['kpi_id'] : 0;
    $wpnonce = isset($params['_wpnonce']) ? $params['_wpnonce'] : '';
    $kpi_type = isset($params['kpi_type']) ? $params['kpi_type'] : '';
    $year_value = isset($params['year']) ? (int)$params['year'] : '';
    $year_id = isset($params['year_id']) ? (int)$params['year_id'] : 0;
    $bank_id = isset($params['bank_id']) ? (int)$params['bank_id'] : 0;

    if( !wp_verify_nonce($wpnonce, 'load-members-kpi-target') ) {
        $message = __('Mã bảo vệ không hợp lệ', TPL_DOMAIN_LANG );
        $httpCode = 403;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }

    $user = wp_get_current_user();
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
        $httpCode = 401;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }

    $result = kpi_get_kpi_by_id($kpiID);
    if( !empty( $result ) && !is_wp_error( $result ) ) {

        # Load Owner
        if( $result['user_id'] != $user->ID ) {
            $message = __('Bạn không có quyền đọc mục tiêu này', TPL_DOMAIN_LANG );
            $httpCode = 403;
            send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
        }

        $terms = wp_get_post_terms( $result['bank_id'], 'category', ['fields' => 'tt_ids'] );
        if( !empty( $terms ) && !is_wp_error( $terms ) ){
            if( count( $terms ) > 0 ){
                $terms = $terms[0];
            }
            $result['bank_cat'] = $terms;
        }

        $subcharts = kpi_get_list_of_charts($kpi_type, 'YEAR', $year_value, 0, $user->orgchart_id, $result['bank_id'], $result['id']);

        $result['last_query']   = $wpdb->last_query;
        $result['last_error']   = $wpdb->last_error;
        $result['time_value']   = $year_value;
        $result['chuc_danh']    = $subcharts;
        $result['unit'] = getUnit($result['unit']);
        $bankInfo               = get_post($result['bank_id']);
        $result['bankInfo']     = $bankInfo ? ['ID' => $bankInfo->ID,
            'parent' => $bankInfo->post_parent, 'title' => $bankInfo->post_title] : null;
        $httpCode = 202;
        $message = 'OK';
    } else {
        $message = __('Không tìm thấy', TPL_DOMAIN_LANG);
        $httpCode = 404;
    }

    if( $httpCode >= 300 ) {
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    } else {
        send_response_json(['code' => $httpCode, 'data' => $result], $httpCode, $message);
    }
}