<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/8/18
 * Time: 09:18
 */

function wp_ajax_post_change_status( $params ){
    global $wpdb;
    $user = wp_get_current_user();
    $postarr = $wpdb->escape($params);
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    if( empty($user->allcaps['publish_posts']) && !$user->has_cap('publish_posts') ) {
        $message = __('Bạn không được cấp quyền chỉnh sửa trạng thái mục tiêu' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],402, $message);
    }
    $postID = $postarr['post_id'];
    $post = get_post( $postID );
    if( !$post ){
        $message = __( 'Không tìm thấy mục tiêu KPI. Vui lòng thử lại' ,TPL_DOMAIN_LANG);
        send_response_json(['error' => $message],404, $message);
    }
    $postarr['ID'] = $postID;
    if( !empty( $postarr['post_status'] ) ){
        $postarr['post_status'] = 'publish';
    }else{
        $postarr['post_status'] = 'draft';
    }
    extract( $postarr );
    $data = compact( 'ID', 'post_status' );
    $id = wp_update_post($data);
    if( is_wp_error( $id ) ){
        # @TODO process error
        $message = __('Chỉnh sửa mục tiêu KPI thất bại. Vui lòng thử lại' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],415, $message);
    }else{
        if( $postarr['post_status'] === 'publish' ){
            $message = __('Đã đưa mục tiêu vào áp dụng', TPL_DOMAIN_LANG);
        }else{
            $message = __('Đã thay đổi mục tiêu thành chưa áp dụng', TPL_DOMAIN_LANG);
        }
        send_response_json(['success' => $message], 200, $message);
    }
}