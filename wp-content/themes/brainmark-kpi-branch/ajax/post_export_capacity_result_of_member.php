<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 25/02/2018
 * Time: 02:29
 */

require_once THEME_DIR . '/inc/phpexcel/Classes/PHPExcel/IOFactory.php';
require_once THEME_DIR . '/inc/phpexcel/Classes/PHPExcel.php';
require_once THEME_DIR . '/inc/lib-users.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-orgchart.php';

function wp_ajax_post_export_capacity_result_of_member($params){
	global $wpdb;
	$user = wp_get_current_user();
	$params = wp_slash($params);
	if( !$user ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],401, $message);
	}
	if( !wp_verify_nonce($params['_wpnonce'], 'export_capacity_result_of_member') ){
		$message = __('Phiên bản đã hết hạn. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],408, $message);
	}

	$yearID = $params['year_id'];
	$resultYear = kpi_get_year_by_id( $yearID );
	if( empty( $resultYear ) ){
		$msg      = "Không tìm thấy năm KPI";
		$httpCode = 404;
		send_response_json( [ 'code' => $httpCode, 'message' => $msg ], $httpCode, $msg );
	}

	$userID = $user->ID;
	if( array_key_exists('user_id', $params) ){
		$userID = $params['user_id'];
	}
	$fileTemplate = THEME_DIR . '/assets/template/exports/' . CAPACITY_EXPORT_EMPLOYEES;
	$upload = wp_upload_dir();
	$date = date('d.m.Y.H.i.s', time() );
	#$fileCopy = "{$upload['basedir']}/exports/" . $date . CAPACITY_EXPORT_EMPLOYEES;
	$fileCopy = "{$upload['basedir']}/exports/" . CAPACITY_EXPORT_EMPLOYEES;
	if (!is_dir("{$upload['basedir']}/exports")) {
		mkdir("{$upload['basedir']}/exports", 0777, true);
	}
	@copy($fileTemplate, $fileCopy);
	@chmod($fileCopy, 0777);
	$identify = PHPExcel_IOFactory::identify($fileCopy);
	$objReader = PHPExcel_IOFactory::createReader($identify);
	$objPHPExcel = $objReader->load($fileTemplate);
	$activeSheet = $objPHPExcel->getActiveSheet();

	$getCPYearOf = capacity_get_kpi_year_and_total_not_user( $yearID, APPLY_OF_EMPLOYEES, KPI_STATUS_RESULT );
	$getCPYearOf = array_group_by($getCPYearOf, 'type_of');
	$kpiForI = [];
	$kpiForIII = [];
	if ( ! empty( $getCPYearOf ) ) {
		$kpiForI   = $getCPYearOf['kpi_nam'];
		$kpiForIII = $getCPYearOf['kpi_kq'];
	}
	$tabs = ["'Finance'", "'Customer'", "'Operate'", "'Development'"];
	$strTabs = implode( ", ", $tabs );
	$groupDatas = kpi_get_group_kpi_by_year_cty( $strTabs, $resultYear['year'], 'no' );
	$arrTabs = kpi_get_total_percent( $groupDatas, 'no' );
	$totalPercent = array_sum( $arrTabs['total_percent'] );

    $getCPUser = capacity_get_list_kpi_by_user($resultYear['id'], APPLY_OF_EMPLOYEES, $userID, KPI_STATUS_RESULT);

	$groupDatasPB = kpi_get_group_kpi_by_year_orgchart_for_year( $strTabs, $resultYear['year'], $userID, 'no' );
	$arrTabsPB = kpi_get_total_percent($groupDatasPB);
	$totalPercentPB = array_sum( $arrTabsPB['total_percent'] );

	if( !empty( $kpiForI ) ){
		foreach ( $kpiForI as $key => $item ){
			$plan = $item['plan'];
			$percent = $item['percent'];
			$note = $item['note'];
			if( $item['type_kpis'] == 'cong-ty' ){
				$actual_gain = $totalPercent;
				$activeSheet->setCellValue('J19', $percent/100 );
				$activeSheet->setCellValue('L19', $plan/100 );
				$activeSheet->setCellValue('M19', $actual_gain/100 );
				$activeSheet->setCellValue('N19', "=M19/L19" );
				$activeSheet->setCellValue('O19', strip_tags($note) );
				$activeSheet->setCellValue('P19', "=J19*N19" );
			}else{
				$actual_gain = $totalPercentPB;
				$activeSheet->setCellValue('J20', $percent/100 );
				$activeSheet->setCellValue('L20', $plan/100 );
				$activeSheet->setCellValue('M20', $actual_gain/100 );
				$activeSheet->setCellValue('N20', "=M20/L20" );
				$activeSheet->setCellValue('O20', strip_tags($note) );
				$activeSheet->setCellValue('P20', "=J20*N20" );
			}
		}
	}

	$rowInsertMore = 0;

	# II. KPI CẢ NĂM CỦA NHÂN VIÊN

    $rowStartIII = 32;
    #$rowStartIII = 24;
    # III. ĐÁNH GIÁ NĂNG LỰC NHÂN VIÊN
    if( !empty( $getCPUser ) ){
        $totalActualGain = 0;
        $linkImageLv = THEME_URL . '/assets/images/';
        $sttIII = 0;
        foreach ( $getCPUser as $key => $capUser ) {
            $sttIII++;
            $row = $rowStartIII - 1;
            $post_title = $capUser['post_title'];
            $actual_gain = $capUser['actual_gain'];
            $level_1 = $capUser['level_1'];
            $level_2 = $capUser['level_2'];
            $level_3 = $capUser['level_3'];
            $note = strip_tags($capUser['note']);
            if ($actual_gain == '') {
                $trend = "";
            } else {
                $trend = "";
                $actual_gain = (int)$actual_gain;
                if ($actual_gain <= $level_1 || ($level_1 < $actual_gain && $actual_gain < $level_2)) {
                    $trend = $linkImageLv . 'di-xuong.png';
                } elseif ($level_2 <= $actual_gain && $actual_gain < $level_3) {
                    $trend = $linkImageLv . 'on-dinh.png';
                } elseif ($level_3 <= $actual_gain) {
                    $trend = $linkImageLv . 'di-len.png';;
                }
                $totalActualGain += $actual_gain;
            }
            $activeSheet->insertNewRowBefore(($rowStartIII),1);#:O" . ($rowStartIII + 1) ;. ":O" . $row
            $activeSheet->mergeCells("C{$rowStartIII}:I{$rowStartIII}");
            $activeSheet->mergeCells("J{$rowStartIII}:K{$rowStartIII}");
            $activeSheet->duplicateStyle( $activeSheet->getStyle("B" . ($rowStartIII + 1) ) , "B" . $rowStartIII  );
            $activeSheet->duplicateStyle( $activeSheet->getStyle("C" . ($rowStartIII + 1) ) , "C" . $rowStartIII  );
            $activeSheet->duplicateStyle( $activeSheet->getStyle("J" . ($rowStartIII + 1) ) , "J" . $rowStartIII  );
            $activeSheet->duplicateStyle( $activeSheet->getStyle("L" . ($rowStartIII + 1) ) , "L" . $rowStartIII  );
            $activeSheet->duplicateStyle( $activeSheet->getStyle("J" . ($rowStartIII + 1) ) , "J" . $rowStartIII  );
            $activeSheet->duplicateStyle( $activeSheet->getStyle("M" . ($rowStartIII + 1) ) , "M" . $rowStartIII  );
            $activeSheet->duplicateStyle( $activeSheet->getStyle("N" . ($rowStartIII + 1) ) , "N" . $rowStartIII  );
            $activeSheet->duplicateStyle( $activeSheet->getStyle("O" . ($rowStartIII + 1) ) , "O" . $rowStartIII  );
            $activeSheet->duplicateStyle( $activeSheet->getStyle("P" . ($rowStartIII + 1) ) , "P" . $rowStartIII  );
            $height = $activeSheet->getRowDimension( ($rowStartIII + 1 ) )->getRowHeight();
            $activeSheet->getRowDimension($row)->setRowHeight( $height );
            $activeSheet->setCellValue('B'.$row, $sttIII)
                ->setCellValue('C'.$row, $post_title)
                ->setCellValue('J'.$row, $level_1)
                ->setCellValue('L'.$row, $level_2)
                ->setCellValue("M".$row, $level_3)
                ->setCellValue("N".$row, $trend)
                ->setCellValue("O".$row, $note)
                ->setCellValue("P".$row, $actual_gain);
            /*$objPHPExcel->getActiveSheet()->getStyle( "B" . $row . ":O" . $row )
                ->getFill()->applyFromArray(
                [
                    'fill' => [
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => ['rgb' => 'ffffcf']
                    ]
                ]
            );*/
            $rowStartIII++;
            $rowInsertMore++;
        }
        $activeSheet->setCellValue("P22", "=SUM(P25, P" . ( $rowStartIII - 1 ) . ")");
        #$objPHPExcel->getActiveSheet()->removeRow($rowStartIII,1);
    }


	if( !empty( $kpiForIII ) ){
		$stt_for_III = 0;
		#$totalRT = 0;
		$rowIII_KQI = (36 + $rowInsertMore);
		$rowIII_KQII = $rowIII_KQI + 1;
		$rowIII_KQIII = $rowIII_KQI + 2;
		foreach ( $kpiForIII as $key => $item ){
			$stt_for_III++;
			$plan = $item['plan'];
			$percent = $item['percent'];
			$note = $item['note'];
			if( $stt_for_III == 1 ){
				$activeSheet->setCellValue("J{$rowIII_KQI}", "=P15" );
				$activeSheet->setCellValue("L{$rowIII_KQI}", $plan );
				$activeSheet->setCellValue("M{$rowIII_KQI}", "=J{$rowIII_KQI}*L{$rowIII_KQI}" );
			}elseif($stt_for_III == 2){
				$activeSheet->setCellValue("J{$rowIII_KQII}", "=P22" );
				$activeSheet->setCellValue("L{$rowIII_KQII}", $plan );
				$activeSheet->setCellValue("M{$rowIII_KQII}", "=J{$rowIII_KQII}*L{$rowIII_KQII}" );
			}else{
				$activeSheet->setCellValue("J{$rowIII_KQIII}", "" );
				$activeSheet->setCellValue("L{$rowIII_KQIII}", $plan );
				$activeSheet->setCellValue("M{$rowIII_KQIII}", "=J{$rowIII_KQIII}*L{$rowIII_KQIII}" );
			}
		}
		$totalRT = $activeSheet->getCell("M{$rowIII_KQI}");
		$totalRT += $activeSheet->getCell("M{$rowIII_KQII}");
		$totalRT += $activeSheet->getCell("M{$rowIII_KQIII}");
		if( $totalRT <= 50 ){
			$textRating = 'Kém';
		}elseif( $totalRT > 50 && $totalRT <= 60 ){
			$textRating = 'Cần cải thiện';
		}elseif( $totalRT > 60 && $totalRT <= 70 ){
			$textRating = 'Đạt';
		}elseif( $totalRT > 70 && $totalRT <= 90 ){
			$textRating = 'Rất tốt';
		}else{
			$textRating = 'Xuất sắc';
		}
		$activeSheet->setCellValue("N{$rowIII_KQI}", $textRating );
	}

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $identify);
	$objWriter->save($fileCopy);
}