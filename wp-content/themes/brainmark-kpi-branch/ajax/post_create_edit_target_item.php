<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/6/18
 * Time: 10:15
 */

function wp_insert_post_data_formula( $data, $postarr ){
    if( isset($postarr['formula_id']) ) {
        $data['formula_id'] = $postarr['formula_id'];
    }
    return $data;
}

function wp_ajax_post_create_edit_target_item( $params ){
    global $wpdb;
    $user = wp_get_current_user();
	$post_id = $params['post_id'];
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    if( !wp_verify_nonce($params['_wpnonce'], 'create_posts') ){
        $message = __('Phiên bản đã hết hạn. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],408, $message);
    }
    if( empty($user->allcaps['create_posts']) && !$user->has_cap('create_posts') && empty($post_id) ) {
        $message = __('Bạn không được cấp quyền thêm bài viết' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],402, $message);
    }elseif( empty($user->allcaps['edit_posts']) && !$user->has_cap('edit_posts') && !empty($post_id) ){
	    $message = __('Bạn không được cấp quyền chỉnh sửa bài viết' ,TPL_DOMAIN_LANG );
	    send_response_json(['error' => $message],405, $message);
    }
    $postarr = wp_slash($params);
    /*if( empty($postarr['post_title']) ){
        $message = __('Nội dung mục tiêu không được để trống' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }*//*elseif( empty($postarr['meta_input']['target']) ){
        $message = __('Chỉ tiêu không được để trống' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }*//*elseif( !is_numeric($postarr['meta_input']['target']) ){
        $message = __('Trường chỉ tiêu là số' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }*/
    $postParentID = (int)$postarr['post_parent'];
    $postParent = get_post( $postParentID );
    if( !$postParent ){
        $message = __('Không tìm thấy nhóm chỉ tiêu' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],404, $message);
    }

    if( isset( $postarr['post_status'] ) ){
        $postarr['post_status'] = 'publish';
    }else{
        $postarr['post_status'] = 'draft';
    }
	$postarr['ID'] = $postarr['post_id'];
    unset( $postarr['post_id'] );
    extract( $postarr );
    #$data = compact( 'ID', 'post_title', 'meta_input', 'post_parent', 'post_status', 'tax_input' , 'post_category', 'post_content' );
    $data = compact('ID', 'post_title', 'post_parent', 'post_status', 'chart_id', 'post_category', 'formula_id', 'meta_input');
    add_filter('wp_insert_post_data', function($data, $arr) use( $postarr ) {
        if( isset($postarr['formula_id']) ) {
            $data['formula_id'] = $postarr['formula_id'];
        }
	    if( isset($postarr['chart_id']) ) {
		    $data['chart_id'] = $postarr['chart_id'];
	    }
        return $data;
    }, 10, 2 );
    add_filter( 'wp_insert_post_data', function ($data_post, $postarr_post = []) use ($postarr){
        if( array_key_exists('level_1', $postarr) && array_key_exists('level_2', $postarr) && array_key_exists('level_3', $postarr) ){
            $data_post['level_1'] = $postarr['level_1'];
            $data_post['level_2'] = $postarr['level_2'];
            $data_post['level_3'] = $postarr['level_3'];
        }
        return $data_post;
    }, 100, 2);
    if( !empty( $post_id ) ){
    	$post = get_post( $post_id );
    	if( !$post ){
    		$message = __( 'Không tìm thấy mục tiêu KPI. Vui lòng thử lại' ,TPL_DOMAIN_LANG);
		    send_response_json(['error' => $message],404, $message);
	    }
	    if( array_key_exists('post_parent', $data) ) {
            unset($data['post_parent']);
        }
	    wp_update_post($data);
    	if( !empty( $wpdb->last_error ) ) {
		    $message = __( 'Chỉnh sửa mục tiêu KPI thất bại. Vui lòng thử lại', TPL_DOMAIN_LANG );
		    send_response_json( [ 'error' => $message ], 401, $message );
	    }
    }else {
    	unset($data['ID']);
	    wp_insert_post( $data );
	    if( !empty( $wpdb->last_error ) ) {
		    $message = __( 'Tạo mục tiêu KPI thất bại. Vui lòng thử lại', TPL_DOMAIN_LANG );
		    send_response_json( [ 'error' => $message ], 401, $message );
	    }
    }

    # remove_filter('wp_insert_post_data', 'wp_insert_post_data_formula');
    $message = __('Thành công', TPL_DOMAIN_LANG);
    send_response_json(['success' => $message], 200, $message);

}