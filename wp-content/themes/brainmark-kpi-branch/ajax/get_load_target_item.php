<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/6/18
 * Time: 14:13
 */

function wp_ajax_get_load_target_item( $params ){
    global $wpdb;
    $user = wp_get_current_user();
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    $postarr = $wpdb->escape($params);
    $post_id = (int)$postarr['id'];
    $post = get_post( $post_id );
    if( !$post ){
        $message = __('Không tìm thấy nhóm chỉ tiêu' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],404, $message);
    }
    $targetID = $post->ID;
    $titleTarget = html_entity_decode(str_replace(str_split("\|"), "", get_the_title($targetID)));
    $targetMeta = !empty( $post->target ) ? $post->target : "";
	$postParentID = $post->post_parent;
    $termKPI = get_the_terms($targetID, 'types_kpi');
    #$mF = $post->measurement_formulas;
    #$content = strip_tags( $post->post_content );
    #$typesKPIName = ''; $typesKPIID = 0;
    $formulaID = $post->formula_id;
    $level_1 = $post->level_1;
    $level_2 = $post->level_2;
    $level_3 = $post->level_3;
    $violation_assessment = $post->violation_assessment;
    $checked = $post->post_status === 'publish' ? "checked" : '';
    /*if( !empty( $termKPI ) && !is_wp_error( $termKPI ) && count( $termKPI ) > 0 ){
        $typesKPIName = $termKPI[0]->name;
        $typesKPIID = $termKPI[0]->term_id;
    }*/
    $data = [
        'post_title' => $titleTarget,
        'meta_input[target]' => $targetMeta,
        'formula_id' => $formulaID,
        #'post_content' => $content,
        'post_status' => $checked,
	    'post_parent' => $postParentID,
        'post_id' => $targetID,
        'level_1' => $level_1,
        'level_2' => $level_2,
        'level_3' => $level_3,
        'violation_assessment' => $violation_assessment,
        #'measurement_formulas' => $mF

    ];
    send_response_json( ['data' => $data], 200, __('OK', TPL_DOMAIN_LANG ));
}