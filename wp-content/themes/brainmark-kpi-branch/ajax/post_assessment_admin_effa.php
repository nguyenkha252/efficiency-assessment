<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/8/18
 * Time: 2:44 PM
 */

function wp_ajax_post_assessment_admin_effa($params){
    global $wpdb;
    $user = wp_get_current_user();
    $orgChartID = isset($user->orgchart_id) ? $user->orgchart_id : 0;
    $params = $wpdb->escape($params);
    $getUser = effa_site_get_user_by_id($params['user_id']);
    $getOrgchart = effa_site_user_load_orgchart($getUser);
    $orgchart_id = $getOrgchart->id;

    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    if( !user_is_manager() ){
        $message = __('Bạn không có quyền thực hiện thao tác này' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],403, $message);
    }
    if( empty($getUser) ){
        $message = __('Không tìm thấy nhân viên' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],404, $message);
    }
    $postID = $params['post_id'];
    $year = $params['year'];
    $post = get_post( $postID );
    if( !$post ){
        $message = __( 'Không tìm thấy mục tiêu. Vui lòng thử lại' ,TPL_DOMAIN_LANG);
        send_response_json(['error' => $message],404, $message);
    }
    $efficiency = effa_site_efficiency_get_by_id($params['id']);
    if( empty($efficiency) ){
        $message = __( 'Không tìm thấy mục tiêu của chức danh' ,TPL_DOMAIN_LANG);
        send_response_json(['error' => $message],404, $message);
    }
    $assign = effa_site_assign_get_by_postid_chartid_year($postID, $orgchart_id, $year);
    if( empty($assign) ){
        $message = __( 'Không tìm thấy mục tiêu của chức danh' ,TPL_DOMAIN_LANG);
        send_response_json(['error' => $message],404, $message);
    }
    $efficiencyUser = effa_site_efficiency_get_by_assign_id($assign->id, $getUser->ID);
    $created =  current_time( 'mysql' );
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableEffa = "{$prefix}efficiency_assessment";
    $errors = [];
    $wpdb->query("START TRANSACTION;");
    if( !empty($efficiencyUser) ){
        #update
        $data['manager_lv0'] = $params['value'];
        $wpdb->update($tableEffa, $data, ['id' => $efficiencyUser->id]);
        if( !empty($wpdb->last_error) ){
            $errors[] = $wpdb->last_error;
        }
        $efficiencyUserID = $efficiencyUser->id;
    }else{
        #insert
        $data = [
            'assign_id' => $efficiency->assign_id,
            'user_id' => $getUser->ID,
            'important' => $efficiency->important,
            'standard' => $efficiency->standard,
            'manager_lv0' => $params['value'],
            'created' => $created,
        ];
        $wpdb->insert($tableEffa, $data);
        if( !empty($wpdb->last_error) ){
            $errors[] = $wpdb->last_error;
        }
        $efficiencyUserID = $wpdb->insert_id;
    }

    if( !empty($errors) ){
        # @TODO process error
        $wpdb->query("ROLLBACK;");
        $message = __('Đánh giá thất bại' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],415, $message);
    }else{
        $wpdb->query("COMMIT;");
        $result = effa_site_efficiency_calculator_result($orgchart_id, $getUser->ID, $year);
        $totalResult = 0;
        $dataOutput = [];
        if( !empty($result) ){
            foreach ($result as $k => $item){
                $score = isset($item->score) ? $item->score : 0;
                $total_standard = isset($item->total_standard) ? $item->total_standard : 0;
                $percent = isset($item->percent) ? $item->percent : 0;
                $group_result = round($score / $total_standard * $percent, 0);
                if( $post->post_parent == $item->pID ) {
                    $dataOutput["group_score_{$item->pID}"] = $score;
                    $dataOutput["group_result_{$item->pID}"] = $group_result . "%";
                }
                $totalResult += $group_result;
            }
        }
        $thisCore = $params['value'] * $efficiency->important;
        $dataOutput["score_{$post->ID}"] = $thisCore ;
        $dataOutput["result_{$post->ID}"] = (int)$params['value'] == 0 ? 0 : round( ($thisCore * 100 ) / ($efficiency->important * $efficiency->standard), 0 ) . "%";
        $dataOutput['total_result'] = $totalResult . "%";
        $settings = get_option('settings_website');
        $standard_benchmark = $settings['standard_benchmark'] ? $settings['standard_benchmark'] : 0;
        $description_reach = isset($settings['description_reach']) ? "<div><strong class='description-rank'>({$settings['description_reach']})</strong></div>" : "";
        $description_not_reach = isset($settings['description_not_reach']) ? "<div><strong class='description-rank'>({$settings['description_not_reach']})</strong></div>" : "";
        $dataOutput['result_text'] = $totalResult >= $standard_benchmark ? __('Đạt  ', TPL_DOMAIN_LANG) . $description_reach : __('Không đạt', TPL_DOMAIN_LANG) . $description_not_reach;
        $message = __('Thành công', TPL_DOMAIN_LANG);
        send_response_json(['success' => $message, 'data_response' => $dataOutput], 201, $message);
    }
}