<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 9/4/18
 * Time: 3:49 PM
 */

require_once THEME_DIR . '/inc/lib-orgchart.php';
function wp_ajax_get_check_node_name($params){
    $params = wp_slash($params);
    if( empty(trim($params['node_name'])) ){
        $msg = 'Chức danh không được để trống';
        $status = 401;
        send_response_json(['code' => $status, 'message' => $msg], $status, $msg);
    }
    $getOrgchart = get_orgchart_by_name_lv_alias_room($params['node_name'], $params['node_alias'], $params['node_view_onlevel'], $params['node_room'], $params['node_id']);
    if( !empty($getOrgchart) ){
        $msg = 'Chức danh đã tồn tại. Vui lòng tạo chức danh khác';
        $status = 401;
        send_response_json(['code' => $status, 'message' => $msg], $status, $msg);
    }else{
        $msg = 'Chức danh này có thể sử dụng ';
        $status = 201;
        send_response_json(['code' => $status, 'message' => $msg], $status, $msg);
    }
}