<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/6/18
 * Time: 14:13
 */

function wp_ajax_get_assessment_levels( $params ){
    $post_id = (int) $params['post_id'];

    $level1 = get_post_meta( $post_id,  '_effa_assessment_level_1', true );
    $level2 = get_post_meta( $post_id,  '_effa_assessment_level_2', true );
    $level3 = get_post_meta( $post_id,  '_effa_assessment_level_3', true );
    $level4 = get_post_meta( $post_id,  '_effa_assessment_level_4', true );
    $level5 = get_post_meta( $post_id,  '_effa_assessment_level_5', true );

    if ( !empty($level1) && !empty($level2) && !empty($level3) && !empty($level4) && !empty($level5) ) {
        send_response_json( ['data' => [$level1, $level2, $level3, $level4, $level5] ], 201, __('OK', TPL_DOMAIN_LANG ));
    }

    send_response_json(['code' => 404],404, '');
}