<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/13/18
 * Time: 7:47 PM
 */

function wp_ajax_post_write_comment_effa($params){
    global $wpdb;
    $user = wp_get_current_user();
    $params = wp_slash($params);
    $orgChartID = isset($user->orgchart_id) ? $user->orgchart_id : 0;
    $userID = isset($params['user_id']) ? $params['user_id'] : 0;
    #$getUser = effa_site_get_user_by_id($userID);
    #$getOrgchart = effa_site_user_load_orgchart($getUser);
    #$orgchart_id = $getOrgchart->id;
    $year = date('Y', time());
    if( !$user ){
        $message = __('Please! Login to use application' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    #$checkChildren = effa_site_orgchart_check_on_level($orgchart_id, $orgChartID);
    if( !user_is_manager()){
        $message = __('Bạn không có quyền thực hiện thao tác này ' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],403, $message);
    }
    if( !wp_verify_nonce($params['_wpnonce'], 'write_comments') ){
        $message = __('Time has expired' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],408, $message);
    }
    $getUser = effa_site_get_user_by_id($userID);
    if( empty($getUser) ){
        $message = __('Không tìm thấy nhân viên' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],404, $message);
    }
    $comments = effa_site_comments_by_user($userID, $year, 'publish', 0);
    $comment_content = isset($params['value']) ? str_replace(str_split("\|"), "", $params['value']) : "";
    $comment_content = nl2br( htmlspecialchars( $comment_content ) );
    $update = false;
    $comment_type = isset($params['type']) ? $params['type'] : "assign_efficiency";
    $check_comment_type = effa_site_get_comment_type($comment_type);
    if( empty($check_comment_type) ){
        $message = __('Nhận xét thất bại. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],415, $message);
    }
    $wpdb->query("START TRANSACTION;");
    if( empty($comments) ){
        $created =  current_time( 'mysql' );
        #insert comment
        $data = [
            'comment_content'   => $comment_content,
            'comment_author'    => $user->ID,
            'user_id'           => $userID,
            'parent'            => 0,
            'comment_type'      => $comment_type,
            'comment_status'    => 'publish',
            'comment_year'      => $year,
            'created'           => $created
        ];
        $action_result = effa_site_comments_insert($data);
    }else{
        #update comment
        $data = [
            'comment_content' => $comment_content
        ];
        $update = true;
        $action_result = effa_site_comments_update($data, ['id' => $comments->id]);
    }
    if( is_array($action_result) && !empty($action_result) ){
        $wpdb->query("ROLLBACK;");
        $message = $update ? __('Cập nhật nhận xét thất bại. Vui lòng thử lại.' ,TPL_DOMAIN_LANG ) : __('Thêm nhận xét thất bại. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],415, $message);
    }
    $wpdb->query("COMMIT;");
    $message = __('Thành công', TPL_DOMAIN_LANG);
    send_response_json(['success' => $message], 201, $message);
}