<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/11/18
 * Time: 23:33
 */

function wp_ajax_post_add_department_target($params) {
    global $wpdb;
    require_once THEME_DIR . '/inc/lib-orgchart.php';

    $results = [];
    $user = wp_get_current_user();
    $messages = ['type' => '', 'year_id' => '', 'chart_id' => '',
        'bank_id' => 'Mục tiêu chưa được chọn',
        'department_plan' => 'Vui lòng nhập vào giá trị của Kế hoạch của phòng',
        'receive' => 'Vui lòng nhập vào Thời điểm nhận kết quả',
        'percent' => "Vui lòng nhập vào giá trị của Trọng số cho KPI này",
        'unit' => "Vui lòng nhập vào giá trị của Đơn vị tính"];
    $keys = array_keys($messages);
    $data = [];
    $errors = [];
    foreach($keys as $k) {
        if( empty($params[$k]) && !empty($messages[$k]) ) {
            $errors[] = "{$messages[$k]}";
        }
        $data[$k] = isset($params[$k]) ? $params[$k] : null;
    }
    if( !empty($errors) ) {
        $msg = implode("<br>", $errors);
        send_response_json(['code' => 400, 'message' => $msg, 'data' => $data], 400, "Error");
    }
    $data['receive'] = kpi_mysql_date($params['receive']);
    $data['company_plan'] = 0;
    $data['parent'] = 0;

    $data['user_id'] = $user->ID;
    $data['aproved'] = 0;
    $data['status'] = 'draft';
    $data['created'] = date('Y-m-d H:i:s', time());

    $nonce = isset($params['_wpnonce']) ? $params['_wpnonce'] : '';
    if( !wp_verify_nonce($nonce, 'add_department_target') ) {
        $msg = "Sai mã bảo vệ";
        send_response_json(['code' => 403, 'message' => $msg], 403, $msg);
    }

    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableKpi = "{$prefix}kpis";
    $tableKpiYear = "{$prefix}kpi_years";
    # Insert into KPI Year with parent is year_id
    $_type = strtolower($params['type']);
    if( !in_array($_type, ['finance', 'customer', 'operate', 'development']) ) {
        $msg = "Có lỗi xảy ra: " . $params['type'];
        send_response_json(['code' => 400, 'message' => $msg], 400, $msg);
    }

    $parentRow = $wpdb->get_row($wpdb->prepare("SELECT y.* FROM {$tableKpiYear} as y ".
        "WHERE y.`{$_type}` = %d AND y.`parent` = %d LIMIT 0, 1", $params['percent'], $params['year_id']), ARRAY_A);
    $dataInsert = null;
    $res = false;
    if( empty($parentRow) ) {
        $sql = $wpdb->prepare("SELECT y.* FROM {$tableKpiYear} as y WHERE y.`id` = %d LIMIT 0, 1", $params['year_id']);
        $parentRow = $wpdb->get_row($sql, ARRAY_A);
        $dataInsert = !empty($parentRow) ? $parentRow : [];

        if( isset($dataInsert['id']) ) {
            unset($dataInsert['id']);
        }
        $dataInsert['parent'] = $params['year_id'];
        $dataInsert['finance'] = 0;
        $dataInsert['customer'] = 0;
        $dataInsert['operate'] = 0;
        $dataInsert['development'] = 0;
        $dataInsert[$_type] = $params['percent'];
        $dataInsert['created'] = $data['created'];
        # send_response_json(['data' => $dataInsert, 'sql' => $wpdb->last_query], 501, 'Debug');
        $res = $wpdb->insert($tableKpiYear, $dataInsert);
        if( $res ) {
            $parentRow['id'] = $wpdb->insert_id;
        } else {
            $parentRow = null;
        }
    }
    # $res = false;
    if( !empty($parentRow) ) {
        $data['year_id'] = $parentRow['id'];
        # Duplicate entry '2017' for key 'year'
        # INSERT INTO `bmk_8_kpi_years` (`year`, `precious`, `month`, `finance`, `customer`, `operate`, `development`, `parent`, `status`, `created`) VALUES ('2017', NULL, NULL, '30', '0', '0', '0', 1, 'publish', '2018-01-12 02:49:38')
        $res = $wpdb->insert($tableKpi, $data);
        if( $res ) {
            send_response_json($wpdb->insert_id, 201, 'Lưu thành công');
        } else {
            $msg = $wpdb->last_error . "\n" . $wpdb->last_query;
            send_response_json(['code' => 400, 'message' => $msg, 'data' => $data], 400, "Có lỗi khi lưu KPI");
        }
    } else {
        $msg = $wpdb->last_error . "\n" . $wpdb->last_query;
        send_response_json(['code' => 400, 'message' => $msg, 'data' => $data, 'year' => $dataInsert], 400, "Có lỗi khi lưu KPI");
    }
}