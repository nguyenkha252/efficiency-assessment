<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/25/18
 * Time: 18:53
 */

function wp_ajax_post_onlevel_approved_personal_target( $params ){
    global $wpdb;
    $user = wp_get_current_user();
    $orgchart = user_load_orgchart( $user );
    $postarr = wp_slash($params);
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        $httpCode = 401;
        send_response_json(['code' => $httpCode, 'message' => $message],$httpCode, $message);
    }

    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableKpi = "{$prefix}kpis";
    $location = '';

    require_once THEME_DIR . '/inc/lib-users.php';
    require_once THEME_DIR . '/inc/lib-orgchart.php';
    require_once THEME_DIR . '/inc/lib-kpis.php';
    if( !empty( $postarr['apply'] ) ){
        $wpdb->query( "START TRANSACTION;" );
        foreach ( $postarr['apply'] as $kpi){
            $kpiID = $kpi['ID'];
            $result = kpi_get_kpi_by_id( $kpiID );
            if( !empty( $result ) ){
                $status = $kpi['status'];
                if( $status == 1 ){
                    $dataUpdate['status'] = 'ket-qua-kpi';
                }else{
                    $dataUpdate['status'] = 'cho-duyet-dang-ky';
                }
                #kiểm tra xem kpi hiện tại của cấp dưới trực tiếp hay không
                $resultsOrgChart = orgchart_check_children_by_id( $result['chart_id'] );
                if( $resultsOrgChart->parent == $orgchart->id || user_is_manager() ){
                    $wpdb->update( $tableKpi, $dataUpdate, ['id' => $kpiID] );
                    if( !empty( $wpdb->last_error ) ){
                        $wpdb->query("ROLLBACK;");
                        $message = __('Đã xảy ra lỗi. Vui lòng thử lại' ,TPL_DOMAIN_LANG );
                        $httpCode = 404;
                        send_response_json(['code' => $httpCode, 'message' => $message, 'error' => $wpdb->last_error],$httpCode, $message);
                    }   
                } else {
                    # nếu kpi hiện tại không phải là cấp dưới trực tiếp thì thông báo cho người dùng
                    $_user = get_user_by('ID', $result['user_id']);
                    $fullName = $_user->first_name . ' ' . $_user->last_name;
                    $message = __("Bạn không phải là cấp trên của {$fullName}" ,TPL_DOMAIN_LANG );
                    $httpCode = 404;
                    send_response_json(['code' => $httpCode, 'message' => $message],$httpCode, $message);
                }
            }else{
                $message = __('Không tìm thấy KPI' ,TPL_DOMAIN_LANG );
                $httpCode = 404;
                send_response_json(['code' => $httpCode, 'message' => $message],$httpCode, $message);
            }

        }
        $wpdb->query("COMMIT;");
        $message = __('Lưu thành công', TPL_DOMAIN_LANG);
        $httpCode = 201;
        send_response_json(
            ['code' => $httpCode, 'message' => $message, 'location' => $location],
            $httpCode,
            $message
        );
    }else{
        $message = __('Không tìm thấy KPI' ,TPL_DOMAIN_LANG );
        $httpCode = 404;
        send_response_json(
            ['code' => $httpCode, 'message' => $message],
            $httpCode,
            $message
        );
    }
}