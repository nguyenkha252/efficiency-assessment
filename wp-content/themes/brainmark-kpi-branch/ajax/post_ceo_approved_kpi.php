<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/15/18
 * Time: 18:52
 */

function wp_ajax_post_ceo_approved_kpi( $params ){
    require_once THEME_DIR . '/inc/lib-kpis.php';
    $params = esc_sql( $params );
    $user = wp_get_current_user();
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    if( !wp_verify_nonce($params['_wpnonce'], 'ceo_approved_kpi') ){
        $message = __('Phiên bản đã hết hạn. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],408, $message);
    }
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableKpi = "{$prefix}kpis";
    $location = '';

    if( !empty( $params ) && !empty( $params['_wp_http_referer'] ) ){
        $query_params = wp_get_referer();
        $location = site_url($query_params);
    }
    $has_error = false;
    $wpdb->query("START TRANSACTION;");
    if( !empty( $params ) && !empty( $params['register'] ) ){
        $postarr = $params['register'];
        //$location = $params
        foreach ( $postarr as $key => $item ) {
	        $result = kpi_get_kpi_by_id( $item['ID'] );
	        if ( ! empty( $result ) ) {
		        #$status = $item['status'];
		        #if ($status == 1) {
		        $dataUpdate['status'] = KPI_STATUS_RESULT;
		        #} else {
		        #    $dataUpdate['status'] = KPI_STATUS_PENDING;
		        #}
		        $id = $wpdb->update( $tableKpi, $dataUpdate, [ 'id' => $item['ID'] ] );
		        if ( ! $id ) {
			        if ( $wpdb->last_error ) {
				        $has_error = true;
				        $msg       = $wpdb->last_error;
			        }
		        }
	        } else {
		        $message  = __( 'Không tìm thấy KPI', TPL_DOMAIN_LANG );
		        $httpCode = 404;
		        send_response_json( [ 'code' => $httpCode, 'message' => $message ], $httpCode, $message );
	        }
	        #echo json_encode([$id, $data, $item, $wpdb->last_query]);
        }
        if( $has_error ){
            $wpdb->query('rollback;');
            //$msg = __('Có lỗi xảy ra vui lòng thử lại.', TPL_DOMAIN_LANG);
            $http_code = 400;
        }else{
            $wpdb->query('COMMIT;');
            $msg = __('Thành công', TPL_DOMAIN_LANG);
            $http_code = 201;
        }
    }else{
        $msg = __('Có lỗi xảy ra vui lòng thử lại.', TPL_DOMAIN_LANG);
        $http_code = 400;
    }
    send_response_json( ['code' => $http_code, 'message' => $msg, 'location' => $location], $http_code, $msg );
}