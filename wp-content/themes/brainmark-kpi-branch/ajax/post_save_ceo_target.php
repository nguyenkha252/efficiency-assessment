<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 2/5/18
 * Time: 16:26
 */
require_once THEME_DIR . '/inc/lib-orgchart.php';
function wp_ajax_post_save_ceo_target($params) {
    global $wpdb;
    $wpnonce = isset($params['_wpnonce']) ? $params['_wpnonce'] : '';
	$user = wp_get_current_user();
	$orgchart = user_load_orgchart( $user );
	$orgchart_id = $orgchart->id;
    if( !wp_verify_nonce($wpnonce, 'save-ceo-target') ) {
        $msg = "Mã bảo vệ không hợp lệ";
        send_response_json(['code' => 403, 'message' => $msg], 403, $msg);
    }

    $cat_slug = $params['cat_slug'];
    $kpi_type = $params['kpi_type'];
    $year_id = $params['year_id'];
    $postdata = wp_slash( $params['postdata'] );
    add_filter('wp_insert_post_data', function($data, $postarr) use($params, $postdata, $orgchart_id) {
        if( isset($postdata['formula_id']) ) {
            $data['formula_id'] = intval($postdata['formula_id']);
        }
	    $data['chart_id'] = $orgchart_id;
        return $data;
    }, 10, 2);
    // Insert Bank ID
	#print_r( $postdata );exit;
    if( empty($postdata['bank_id']) ) {
	    $insertData = [
		    'post_title'  => $postdata['post_title'],
		    'post_status' => 'publish',
		    'formula_id'  => $postdata['formula_id'],
		    #'post_parent' => isset($postdata['bank_parent']) ? (int)$postdata['bank_parent'] : 0,
	    ];
	    /*$term = get_term_by('slug', $cat_slug, 'category', OBJECT);
		if( $term !== false && !is_wp_error($term) ) {
			$term_id = $term->term_id;
			$insertData['post_category'] = [(int)$term_id];
		}*/
	    #$bank = get_page_by_title( $postdata['post_title'], OBJECT, 'post' );

	    /*add_filter( 'wp_insert_post_data', function ( $data, $postarr ) use ( $params, $postdata, $orgchart_id ) {
		    if ( isset( $postdata['formula_id'] ) ) {
			    $data['formula_id'] = intval( $postdata['formula_id'] );
		    }
		    $data['chart_id'] = $orgchart_id;

		    return $data;
	    }, 10, 2 );*/
	    #print_r( $insertData );exit;
	    /*if( $bank instanceof WP_Post ) {
			$insertData['ID'] = $bank->ID;
			if( isset($postdata['formula_id']) ) {
				$insertData['formula_id'] = $postdata['formula_id'];
			}
			if( $insertData['post_parent'] != $bank->ID ) {
				$postdata['bank_id'] = wp_update_post($insertData);
			} else {
				$postdata['bank_id'] = $bank->ID;
			}
		} else {*/
	    $title      = __( $params['kpi_type'], TPL_DOMAIN_LANG );
	    $sqlGetPost = $wpdb->prepare( "
					SELECT * 
					FROM {$wpdb->posts} AS p
					WHERE p.post_title = %s AND p.post_author = %d AND p.chart_id = %d 
					AND p.post_parent = 0 AND p.post_type = 'post' 
			", $title, $user->ID, $orgchart->id );
	    $resultBank = $wpdb->get_row( $sqlGetPost );
	    if ( ! empty( $resultBank ) ) {
		    $post_ID = $resultBank->ID;
	    } else {
		    $post_ID = wp_insert_post( [ 'post_title'  => $title,
		                                 'post_status' => 'publish',
		                                 "chart_id"    => $orgchart_id
		    ] );
	    }
	    $insertData['post_parent'] = $post_ID;
	    $postdata['bank_id']       = wp_insert_post( $insertData );
	    #}
    } else {
        // Update Bank ID
        $updateData = [
            'ID' => $postdata['bank_id'],
            'post_title' => $postdata['post_title'],
	        'formula_id' => $postdata['formula_id'],
            #'post_status' => 'publish',
            #'post_parent' => isset($postdata['bank_parent']) ? (int)$postdata['bank_parent'] : 0,
        ];
        if( $postdata['bank_id'] != $updateData['post_parent'] ) {
            $postdata['bank_id'] = wp_update_post($updateData);
        }
    }

    $status = 201;
    $msg = "Lưu thành công";

    if( is_numeric($postdata['bank_id']) ) {
        # $postdata['bank_id']
        $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
        $tableKpi = "{$prefix}kpis";
        $tableChart = "{$prefix}org_charts";
        $tableKpiYear = "{$prefix}kpi_years";
	    $tableKpiFormulas = "{$prefix}kpi_formulas";

        if( empty($params['year_id']) || !is_numeric($params['year_id']) ) {
            $msg = "Tạo trọng số cho từng loại mục tiêu trước khi triển khai";
            send_response_json(['code' => 400, 'message' => $msg], 400, $msg);
        }

        $dataInsert = [
            'bank_id' => $postdata['bank_id'], 'plan' => $postdata['plan'], 'unit' => $postdata['unit'], 'percent' => $postdata['percent'],
            'parent' => !empty($postdata['parent']) ? $postdata['parent'] : 0, 'receive' => kpi_mysql_date($postdata['receive']),
            'chart_id' => !empty($postdata['chart_id']) ? $postdata['chart_id'] : (!empty($params['cid']) ? $params['cid'] : $user->orgchart_id),
            'type' => $postdata['type'], 'status' => $postdata['status'], 'required' => $postdata['required'], 'owner' => $postdata['owner'],
            'create_by_node' => $postdata['create_by_node'], 'aproved' => $postdata['aproved'], 'user_id' => $postdata['user_id'],
            'year_id' => !empty($postdata['year_id']) ? $postdata['year_id'] : $year_id,

	        'influence' => !empty($postdata['influence']) ? 'yes' : 'no',
        ];
        if( !empty($postdata['id']) ) {
            // Update KPI
            $kpiResult = $wpdb->update($tableKpi, $dataInsert, ['id' => $postdata['id']]);
        } else {
            // Insert KPI
            $dataInsert['created'] = date('Y-m-d H:i:s', time());
            $kpiResult = $wpdb->insert($tableKpi, $dataInsert);
            $postdata['id'] = $wpdb->insert_id;
        }

        if( $kpiResult === false && !empty($wpdb->last_error) ) {
        	if( preg_match("/(^Duplicate entry)/i", $wpdb->last_error, $match) ){
        		$tab = __($dataInsert['type'], TPL_DOMAIN_LANG);
        		$orgchart = user_load_orgchart( $user );
        		$msg = "Nội dung mục tiêu không được trùng lặp ở mục " . $tab . " của chức danh " . $orgchart->name ;
	        }else{
		        $msg = "Có lỗi xảy ra khi lưu KPI";
	        }
            send_response_json(['code' => 400, 'message' => $msg, 'error' => $wpdb->last_error], 400, $msg);
        }
        $sql = $wpdb->prepare("SELECT k.*, c.name, c.name as chuc_danh, y.`year`, y.`precious`, y.`month`, p.post_title, p.post_parent as bank_parent, p.formula_id, fm.title AS formula_title, fm.type AS formula_type, fm.formulas, fm.note AS formula_note ".
            "FROM {$tableKpi} as k " .
            "INNER JOIN {$tableKpiYear} as y ON (k.year_id = y.id) " .
            "INNER JOIN {$wpdb->posts} as p ON (p.ID = k.bank_id) " .
            "LEFT JOIN {$tableKpiFormulas} AS fm ON (p.formula_id = fm.id) " .
            "INNER JOIN {$tableChart} as c ON (k.chart_id = c.id) " .
            "WHERE k.`id` = %d", $postdata['id']);

        $html = '';
        $postdata['kpi'] = $wpdb->get_row($sql, ARRAY_A);

        $item_html = '';
        if( !empty($postdata['kpi']) ) {
            require_once THEME_DIR . '/inc/render-kpi-item-row.php';
            $postdata['kpi']['bank_parent'] = (int)$postdata['kpi']['bank_parent'];
            $postdata['kpi']['year'] = (int)$postdata['kpi']['year'];
            $postdata['kpi']['bank_id'] = (int)$postdata['kpi']['bank_id'];
            $postdata['kpi']['formula_id'] = (int)$postdata['kpi']['formula_id'];
	        $postdata['kpi']['influence'] = (int)$postdata['kpi']['influence'];
            $postdata['kpi']['year_id'] = (int)$postdata['kpi']['year_id'];
            $postdata['kpi']['id'] = (int)$postdata['kpi']['id'];
            $chartParent = kpi_orgchart_get_chart_parent_of($_GET['cid']);
            $chartCurrent = kpi_get_orgchart_by_id($_GET['cid']);
            $html = kpi_render_ceo_row_item($postdata['kpi'], $postdata['type'], $chartCurrent, $chartParent, null);
            $item_html = '<tr data-id="'.$postdata['kpi']['id'].'">
                    <td class="column-1">{IDX}.</td>
                    <td class="column-2">'.($postdata['kpi']['post_title']) . '</td>
                    <td class="column-3">'.$postdata['kpi']['percent'].'%</td>
                </tr>';
        }
        send_response_json(['code' => $status, 'message' => $msg,
            'id' => !empty($postdata['kpi']) ? $postdata['kpi']['id'] : $postdata['id'],
            'sql' => $sql, 'last_query' => $wpdb->last_query,
            'last_error' => $wpdb->last_error, 'dataInsert' => $dataInsert,
            'postdata' => $postdata, 'html' => $html, 'item_html' => $item_html], $status, $msg);
    } else if( is_wp_error($postdata['bank_id']) ) {
        $status = 400;
        $msg = $postdata['bank_id']->get_error_message();
        send_response_json(['code' => 400, 'message' => $msg, 'error' => $wpdb->last_error], 400, $msg);
    }
}

