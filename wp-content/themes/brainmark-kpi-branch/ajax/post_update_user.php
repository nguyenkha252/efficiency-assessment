<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/12/17
 * Time: 00:21
 */

require_once THEME_DIR . '/inc/lib-users.php';

function wp_ajax_post_update_user( $params ) {
    $user = wp_get_current_user();
    if( !wp_verify_nonce($params['_wpnonce'], 'update_user') ) {
        # return send_response_json(new WP_Error("invalid_nonce", __("Nonce invalid", TPL_DOMAIN_LANG)), 401);
    }

    if( empty($user->allcaps['edit_users']) && !$user->has_cap('edit_users') ) {
        # send_response_json(new WP_Error("invalid_permission", __("Not allow user permission", TPL_DOMAIN_LANG)), 401);
    }
	$checkCode = check_code_user( $params['user_nicename'], $params['ID'] );
	if( $checkCode ){
		$message = __('Mã nhân viên đã được sử dụng. Vui lòng đổi mã khác', TPL_DOMAIN_LANG);
		$httpCode = 401;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
    $user = kpi_user_update_info($params);
    # send_response_json_success($userData, 201);exit;
    if( is_wp_error($user) ) {
        if( $user->get_error_code() == 'existing_user_login' ) {

        }
        # $user->add('user', $userData);
        send_response_json($user, 400);
    } else {
        $user = new WP_User($user);
        global $wpdb;
        $prefix = $wpdb->get_blog_prefix($wpdb->blogid);
        $user_role = "{$prefix}user_level";
        $jsonData = [
            'id' => $user->ID,
            'name' => "{$user->first_name} {$user->last_name}",
            'parent' => empty($user->user_parent) ? 0 : $user->user_parent,
            'user_email' => $user->user_email,
            'user_login' => $user->user_login,
            'display_name' => $user->display_name,
            'avatar' => $user->user_url,
            'role' => $user->$user_role,
            'description' => !empty($user->description) ? $user->description : '',
            'cls' => empty($user->user_parent) ? 'full' : '',
            'childCls' => 'clearfix float'
        ];
        send_response_json($jsonData, 200);
    }
    # exit(json_encode(['_POST' => $params, '_FILES' => $_FILES, 'upload' => $upload], JSON_PRETTY_PRINT));
}