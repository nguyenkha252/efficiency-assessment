<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/12/17
 * Time: 00:21
 */

require_once THEME_DIR . '/ajax/get_user_info.php';
require_once THEME_DIR . '/inc/lib-kpis.php';

function kpi_get_users_list() {
    $users = kpi_get_user_info(['id' => 0, 'single' => false, 'exclude' => [] ]);
    ob_start();
    if( is_wp_error($users) ):
        ?><tr><td class="error" colspan="7"><?php echo __("Error: ", TPL_DOMAIN_LANG) . $users->get_error_message(); ?></td></tr><?php
    elseif( empty($users) ):
        ?><tr><td class="notfound" colspan="7"><?php echo __("Not found", TPL_DOMAIN_LANG); ?></td></tr><?php
    else:
        $trees = [];
        $charts = [];
        $items = [];
        foreach($users['items'] as $idx => $item) {
            if( !isset($charts[$item['orgchart_id']]) ) {
                $charts[$item['orgchart_id']] = [
                    'id' => $item['orgchart_id'],
                    'parent' => intval($item['parent']),
                    'role' => intval($item['role']),
                    'members' => [
                        $item['id'] => $item
                    ],
                    'children' => []
                ];
            } else {
                $charts[$item['orgchart_id']]['members'][$item['id']] = $item;
            }
            unset($users['items'][$idx]);
        }

        foreach($charts as $orgchart_id => &$item) {
            if( empty($item['parent']) ) {
                $trees[$orgchart_id] = &$charts[$orgchart_id];
            } else{
                $charts[$item['parent']]['children'][$orgchart_id] = &$charts[$orgchart_id];
            }
        }
        # kpi_render_tree($trees, 0);
        # echo '<pre>'; var_dump($trees); echo '</pre>';
        echo kpi_render_users($trees, 0);
    endif;
    return ['error' => is_wp_error($users) ? $users : false, 'html' => ob_get_clean()];
}

function kpi_render_tree($tree, $level) {
    $has_child = count($tree) ? 'has-children' : '';
    echo '<ul class="level-'.$level.' '.$has_child.'">';
    foreach($tree as $chart_id => &$item) {
        $members = [];
        foreach($item['members'] as $member) {
            $members[] = sprintf("Chart: %s, Name: %s (ID: %d), Tên Hiển Thị: %s", $member['orgchart_name'], $member['name'], $member['id'], $member['display_name']);
        }
        echo sprintf('<li class=""><a href="#">Parent: %d , ID: %s, Chuc Vu: %s, <br>Members: %s</a>',
            $item['parent'], $item['id'], $item['role'], implode('<br>', $members));
        kpi_render_tree($item['children'], $level + 1);
        # echo '<pre>'; var_dump($item['children']); echo '</pre>';
        echo '</li>';
    }
    echo '</ul>';
}

function wp_ajax_get_users_list( $params ) {
    $user = wp_get_current_user();
    if( !wp_verify_nonce($params['_wpnonce'], 'users_list') ) {
        $jsonData = ['error' => new WP_Error(401, __("Nonce invalid", TPL_DOMAIN_LANG)), 'html' => ''];
    } else if( empty($user->allcaps['list_users']) && !$user->has_cap('list_users') ) {
        $jsonData = ['error' => new WP_Error(403, __("Not allow user permission", TPL_DOMAIN_LANG)), 'html' => ''];
    } else {
        $jsonData = kpi_get_users_list();
    }
    $error = $jsonData['error'];
    if( is_wp_error($error) ) {
        send_response_json($jsonData, $error->get_error_code(), $error->get_error_message());
    } else {
        send_response_json($jsonData, 200);
    }
}

function render_users_lv_2( $users, $parent ){
    $output = '';
    echo $parent .' <br>';
    foreach ( $users as $key => $user ){
        if( $user['parent'] == $parent ){
            $output = render_user_list_by_lv($user, 0, 'children-2');
        }
    }
    if(!empty( $output )){
        $output = "
        <tr>
            <td colspan='4'>
                <table>
                <thead>
                    <tr>
                        <th class=\"id-col\">". __('STT', TPL_DOMAIN_LANG) ."</th>
                        <th class=\"avatar-img\">". __('Avatar', TPL_DOMAIN_LANG) ."</th>
                        <th class=\"name\">". __('Name', TPL_DOMAIN_LANG) ."</th>
                        <th class=\"position_name\">". __('Position Name', TPL_DOMAIN_LANG) ."</th>
                    </tr>
                </thead>
                <tbody>
                    ". $output ."
                </tbody>
                </table>
            </td>
         </tr>
            ";
    }
    return $output;
}

function render_users_by_lv( $users, $parent = 0, $stt = 0, $roman = true, $link = 'duyet-dang-ky', $showAmount = true, $action_pb_cn = false ){
    $argsUser = []; $output='';
    $users = ($users instanceof WP_User) ? [$users] : $users;
    foreach( $users as $key => $user ){
        if( $user['parent'] == $parent ){
            $argsUser[] = $user;
            unset($users[$key]);
        }
    }
    if( !empty( $argsUser ) ){
        foreach ($argsUser as $key => $value) {
            if( $roman && !is_numeric($stt) ){
                $stt = convert_roman_2_number($stt);
            }
            $stt++;
            if (!empty($users)) {
                $userOrgchartID = $value['orgchart_id'];
                $userParent = $value['parent'];
                $tableChildrent = '';
                #if( $userParent == $parentOld ) {
                    $outputChildren = render_users_by_lv($users, $userOrgchartID, $stt, false, $link);
                #    $tableChildrent = render_users_lv_2($users, $userOrgchartID);
                #}
            }else{
                $outputChildren = '';
            }
            if( $roman ){
                $stt = convert_number_2_roman($stt);
            }
            $class = !empty($outputChildren) ? "parent" : "children";
            $output .= render_user_list_by_lv( $value, $stt, $class, $link, $showAmount, $action_pb_cn);
            $output .= $outputChildren;
            #$output .= $tableChildrent;
        }
    }
    return $output;
}

function render_users_by_lv_confirm($users, $link = 'duyet-dang-ky', $showAmount = true, $action_pb_cn = false ){
	$output = '';
	global $kpi_contents_parts;
	$applyKpiSystem = $kpi_contents_parts['apply-bsc-kpi-system'];
	$stt = 0;
	$applyKpiSystemUrl = $applyKpiSystem['url'];
	$url = add_query_arg('type', $link, $applyKpiSystemUrl);
	if( isset( $_GET['nam'] ) ){
		$nam = wp_slash( $_GET['nam'] );
		$url = add_query_arg('nam', $nam, $url);
	}
	if( isset( $_GET['quy'] ) ){
		$quy = wp_slash( $_GET['quy'] );
		$url = add_query_arg('quy', $quy, $url);
	}
	if( isset( $_GET['thang'] ) ){
		$thang = wp_slash( $_GET['thang'] );
		$url = add_query_arg('thang', $thang, $url);
	}
    foreach ($users as $key => $item){
        $tdChildren = "";
        if( !empty( $item ) ){
            foreach ($item as $k => $user){
                $stt++;
	            $codeUser = $user['user_nicename'];
	            $room = $user['room'];
	            $name = $user['display_name'];
	            $orgchartName = $user['orgchart_name'];
	            $htmlAmountPending = "";
	            if( $showAmount ) {
		            $status = $link == 'duyet-dang-ky' ? KPI_STATUS_PENDING : KPI_STATUS_WAITING;
		            $amountApproved = count_kpi_of_year_by_user(TIME_YEAR_VALUE, $user['ID'], $status);
		            if (!empty($amountApproved)) {
			            $amountPending = $amountApproved['amount_status'];
		            }
		            if (!empty($amountPending)) {
			            $htmlAmountPending = "<span class='user-amount amount-pending'>{$amountPending}</span>";
		            }
	            }
	            $url = add_query_arg('uid', $user['ID'], $url);
	            $td = '';
	            if( $action_pb_cn ){
		            $argsRoom = [
			            'type' => 'duyet-dang-ky',
			            'nam' => TIME_YEAR_VALUE,
		            ];
		            $urlRoom = add_query_arg( $argsRoom, $url );
		            $a = "<a href=\"{$urlRoom}\" title=\"Duyệt đăng ký cá nhân\">Cá nhân</a>";
		            if( $user['alias'] == 'phongban' ){
			            $argsRoom['approvereg'] = 1;
			            $argsRoom['cid'] = $user['orgchart_id'];
			            $urlRoom = add_query_arg( $argsRoom, $url );
			            $a .= "<a href=\"{$urlRoom}\" title=\"Duyệt đăng ký của phòng\">Phòng ban</a>";
		            }
		            $td = "<td>
                        <div class=\"viewkpiofroom approve-register\">
                            {$a}
                        </div>
                    </td>";
	            }else{
	                $td =
                       "<td>
                        <div class=\"viewkpiofroom approve-register\">
                            <a href=\"{$url}\" title=\"Duyệt kết quả\">Cá nhân</a>
                        </div>
                    </td>";
                }
                $tdChildren .= "
                    <tr class='children'>
                        <td>{$stt}</td>
                        <td>{$codeUser}</td>
                        <td>{$name} {$htmlAmountPending}</td>
                        <td>$orgchartName</td>
                        <td>{$room}</td>
                    ".$td."
                    </tr>";
            }
        }
	    $output .= "
	    <tr class='parent'>
	        <td colspan='6'>{$key}</td>
        </tr>
        {$tdChildren}
	    ";
    }

	return $output;
}

function render_user_list_by_lv( $user, $stt, $class='parent', $link = 'duyet-dang-ky', $showAmount = true, $action_pb_cn = false ){
    $output = '';
    global $kpi_contents_parts;
    $applyKpiSystem = $kpi_contents_parts['apply-bsc-kpi-system'];
    $codeUser = $user['user_nicename'];
    $room = $user['room'];
    $applyKpiSystemUrl = $applyKpiSystem['url'];
    $url = add_query_arg('type', $link, $applyKpiSystemUrl);
    $url = add_query_arg('uid', $user['id'], $url);
    if( isset( $_GET['nam'] ) ){
        $nam = wp_slash( $_GET['nam'] );
	    $url = add_query_arg('nam', $nam, $url);
    }
    if( isset( $_GET['quy'] ) ){
	    $quy = wp_slash( $_GET['quy'] );
	    $url = add_query_arg('quy', $quy, $url);
    }
	if( isset( $_GET['thang'] ) ){
		$thang = wp_slash( $_GET['thang'] );
		$url = add_query_arg('thang', $thang, $url);
    }
    if( !empty($user) ) {
        $name = $user['display_name'];
        $orgchartName = $user['orgchart_name'];
        $htmlAmountPending = "";
        if( $showAmount ) {
            $status = $link == 'duyet-dang-ky' ? KPI_STATUS_PENDING : KPI_STATUS_WAITING;
            $amountApproved = count_kpi_of_year_by_user(TIME_YEAR_VALUE, $user['id'], $status);
            if (!empty($amountApproved)) {
                $amountPending = $amountApproved['amount_status'];
            }
            if (!empty($amountPending)) {
                $htmlAmountPending = "<span class='user-amount amount-pending'>{$amountPending}</span>";
            }
        }
        $td = '';
        if( $action_pb_cn ){
            $argsRoom = [
                'type' => 'duyet-dang-ky',
                'uid' => isset($_GET['uid']) ? $_GET['uid'] : 0,
                'nam' => TIME_YEAR_VALUE,
                    ];
	        $urlRoom = add_query_arg( $argsRoom, site_url() );
            $a = "<a href=\"{$urlRoom}\" title=\"Duyệt đăng ký cá nhân\">Cá nhân</a>";
            if( $user['role'] == 'phongban' ){
                $argsRoom['approvereg'] = 1;
                $argsRoom['cid'] = $user['orgchart_id'];
	            $urlRoom = add_query_arg( $argsRoom, site_url() );
	            $a .= "<a href=\"{$urlRoom}\" title=\"Duyệt đăng ký của phòng\">Phòng ban</a>";
            }
            $td = "<td>
                        <div class=\"viewkpiofroom approve-register\">
                            {$a}
                        </div>
                    </td>";
        }
        $output = "
        <tr class='{$class}' data-edit-url='{$url}'>
            <td>{$stt}</td>
            <td>{$codeUser}</td>
            <td>{$name} {$htmlAmountPending}</td>
            <td>$orgchartName</td>
            <td>{$room}</td>
            ".$td."
        </tr>
    ";
    }
    return $output;
}

function render_users_by_lv_managers($users, $link = 'dang-ky-kpi' ){
	$output = '';
	global $kpi_contents_parts;
	$applyKpiSystem = $kpi_contents_parts['apply-bsc-kpi-system'];
	$stt = 0;
	$applyKpiSystemUrl = $applyKpiSystem['url'];
	$url = add_query_arg('type', $link, $applyKpiSystemUrl);
	if( isset( $_GET['nam'] ) ){
		$nam = wp_slash( $_GET['nam'] );
		$url = add_query_arg('nam', $nam, $url);
	}
	if( isset( $_GET['quy'] ) ){
		$quy = wp_slash( $_GET['quy'] );
		$url = add_query_arg('quy', $quy, $url);
	}
	if( isset( $_GET['thang'] ) ){
		$thang = wp_slash( $_GET['thang'] );
		$url = add_query_arg('thang', $thang, $url);
	}
	foreach ($users as $key => $item){
		$tdChildren = "";
		if( !empty( $item ) ){
			foreach ($item as $k => $user){
				$stt++;
				$codeUser = $user['user_nicename'];
				$room = $user['room'];
				$name = $user['display_name'];
				$orgchartName = $user['orgchart_name'];
				$htmlAmountPending = "";
				$url = add_query_arg('uid', $user['ID'], $url);
				    #$td = '';
					$argsRoom = [
						'type' => $link,
						'nam' => TIME_YEAR_VALUE,
					];
					$urlRoom = add_query_arg( $argsRoom, $url );
					$a = "<a href=\"{$urlRoom}\" title=\"Cá nhân\">Cá nhân</a>";
                if($user['orgchart_parent'] == 0){
	                $argsRoom['cid'] = $user['orgchart_id'];
	                $urlRoom = add_query_arg( $argsRoom, $url );
	                $td = "<td>
                        <div class=\"viewkpiofroom approve-register\">
                            <a href=\"{$urlRoom}\" title=\"Công ty\">Công ty</a>
                        </div>
                    </td>";
                }elseif( $user['alias'] == 'phongban' ){
						$argsRoom['cid'] = $user['orgchart_id'];
						$urlRoom = add_query_arg( $argsRoom, $url );
						$a .= "<a href=\"{$urlRoom}\" title=\"Phòng ban\">Phòng ban</a>";
					$td = "<td>
                        <div class=\"viewkpiofroom approve-register\">
                            {$a}
                        </div>
                    </td>";
				}else{
					$td =
						"<td>
                        <div class=\"viewkpiofroom approve-register\">
                            <a href=\"{$url}\" title=\"Cá nhân\">Cá nhân</a>
                        </div>
                    </td>";
				}
				$tdChildren .= "
                    <tr class='children'>
                        <td>{$stt}</td>
                        <td>{$codeUser}</td>
                        <td>{$name} {$htmlAmountPending}</td>
                        <td>$orgchartName</td>
                        <td>{$room}</td>
                    ".$td."
                    </tr>";
			}
		}
		$output .= "
	    <tr class='parent'>
	        <td colspan='6'>{$key}</td>
        </tr>
        {$tdChildren}
	    ";
	}

	return $output;
}

function render_users_by_lv_employee($users, $link = 'dang-ky-kpi' ){
	$output = '';
	global $kpi_contents_parts;
	$applyKpiSystem = $kpi_contents_parts['apply-bsc-kpi-system'];
	$stt = 0;
	$applyKpiSystemUrl = $applyKpiSystem['url'];
	$url = add_query_arg('type', $link, $applyKpiSystemUrl);
	if( isset( $_GET['nam'] ) ){
		$nam = wp_slash( $_GET['nam'] );
		$url = add_query_arg('nam', $nam, $url);
	}
	if( isset( $_GET['quy'] ) ){
		$quy = wp_slash( $_GET['quy'] );
		$url = add_query_arg('quy', $quy, $url);
	}
	if( isset( $_GET['thang'] ) ){
		$thang = wp_slash( $_GET['thang'] );
		$url = add_query_arg('thang', $thang, $url);
	}
	foreach ($users as $key => $item){
		$tdChildren = "";
		if( !empty( $item ) ){
			foreach ($item as $k => $user){
				$stt++;
				$codeUser = $user['user_nicename'];
				$room = $user['room'];
				$name = $user['display_name'];
				$orgchartName = $user['orgchart_name'];
				$htmlAmountPending = "";
				$url = add_query_arg('uid', $user['ID'], $url);
				#$td = '';
				$argsRoom = [
					'type' => $link,
					'nam' => TIME_YEAR_VALUE,
				];
                $td =
                    "<td>
                    <div class=\"viewkpiofroom approve-register\">
                        <a href=\"{$url}\" title=\"Cá nhân\">Cá nhân</a>
                    </div>
                </td>";

				$tdChildren .= "
                    <tr class='children'>
                        <td>{$stt}</td>
                        <td>{$codeUser}</td>
                        <td>{$name} {$htmlAmountPending}</td>
                        <td>$orgchartName</td>
                        <td>{$room}</td>
                    ".$td."
                    </tr>";
			}
		}
		$output .= "
	    <tr class='parent'>
	        <td colspan='6'>{$key}</td>
        </tr>
        {$tdChildren}
	    ";
	}

	return $output;
}
