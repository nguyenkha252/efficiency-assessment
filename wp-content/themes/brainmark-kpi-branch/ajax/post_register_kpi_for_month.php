<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/24/18
 * Time: 14:13
 */

function wp_ajax_post_register_kpi_for_month( $params ){
    require_once THEME_DIR . '/inc/lib-years.php';
    require_once THEME_DIR . '/inc/lib-kpis.php';
    require_once THEME_DIR . '/inc/lib-orgchart.php';
    global $wpdb;
    $user = wp_get_current_user();
    $orgchart = user_load_orgchart($user);
    $params = wp_slash($params);
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
        $httpCode = 401;
        send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
    }
    $result = kpi_get_kpi_by_id($params['parent']);
    if( empty( $result ) ){
        $message = __('Không tìm thấy KPI của năm', TPL_DOMAIN_LANG );
        $httpCode = 404;
        send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
    }
    $unit = $result['unit'];
    if( array_key_exists( 'plan_month', $params ) ){
        $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
        $tableKpi = "{$prefix}kpis";
        $tableKpiYear = "{$prefix}kpi_years";
        $planMonth = $params['plan_month'];
        $yearParent = $result['year_id'];
        $year = kpi_get_year_by_id( $yearParent );
        if ( empty($year) ) {
            $message = __('Không tìm thấy năm', TPL_DOMAIN_LANG);
            $httpCode = 404;
            send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
        }
        //Cập nhật tính năng settings date expired
        $year_root = year_get_firt_year_by_chart_is_congty($year['year']);
        if( empty($year_root) ){
            $msg = 'Không tìm thấy năm';
            $status = 404;
            send_response_json(['code' => $status, 'message' => $msg], $status, $msg);
        }
        $getMonth = year_get_month_settings($year_root['id']);

        $KpiChildren = kpi_get_year_by_parent_kpi( $params['parent'], $orgchart->id );
		$yearChildren = kpi_get_year_by_parent_by_chart_month( $year['id'], $orgchart->id );
        if( !empty( $KpiChildren ) ){
            #update
            $wpdb->query("START TRANSACTION;");
            $lastError = [];
            foreach ($planMonth as $key => $month){
            	$plan = $month['plan'];
	            $validatePlan = validate_input_before_save_db( $unit, $plan, 'plan' );
	            if( !empty($validatePlan) ){
		            $httpCode = 401;
		            $message = $validatePlan['textErrorUnit'];
		            send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
	            }
	            if( !empty($plan) ) {
		            $percent = preg_replace( "#(%)#i", "", $month['percent'] );
		            $percent = !empty( $percent ) ? (int)$percent : $result['percent'];
		            if ( !is_int( $percent ) ) {
			            $httpCode = 401;
			            $message  = __( 'Trọng số là số nguyên', TPL_DOMAIN_LANG );
			            send_response_json( [ 'code' => $httpCode, 'message' => $message ], $httpCode, $message );
		            }
	            }else{
	            	$plan = '';
	            	$percent = '';
	            }
                //cập nhật ngày expired
                $checkMonth = array_filter($getMonth, function($item) use ($month){
                    return $item['month'] == $month['month'];
                });
                if( !empty($checkMonth) ){
                    $thisMonth = array_shift($checkMonth);
                    $dataKPI['receive'] = $thisMonth['date_expired'];
                }
	            $dataKPI['percent'] = !empty($percent) ? $percent : '';
	            $dataKPI['plan'] = $plan;
                $idKPI = $month['id'];
                $getKPI = kpi_get_kpi_by_id( $idKPI );
                if( empty( $getKPI ) ){
                    $message = __('Không tìm thấy KPI', TPL_DOMAIN_LANG );
                    $httpCode = 404;
                    send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
                }else{

                    if( ( $getKPI['plan'] != $dataKPI['plan'] || $getKPI['percent'] != $dataKPI['percent'] ) && (in_array($getKPI['status'], [KPI_STATUS_DRAFT, KPI_STATUS_PENDING]) && $getKPI['user_id'] == $user->ID ) ) {
                        $wpdb->update($tableKpi, $dataKPI, ['id' => $idKPI]);
                        if (!empty($wpdb->last_error)) {
                            $lastError[] = $wpdb->last_error;
                        }
                    }
                }
            }
            if( !empty( $lastError ) ){
                $wpdb->query("ROLLBACK;");
                $message = __('Đã xảy ra lỗi vui lòng thử lại', TPL_DOMAIN_LANG );
                $httpCode = 415;
                send_response_json( ['code' => $httpCode, 'message' => $message, 'error' => $lastError], $httpCode, $message );
            }else{
                $wpdb->query("COMMIT;");
                $message = __('Lưu thành công', TPL_DOMAIN_LANG );
                $httpCode = 201;
                send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
            }
        }else {
            #insert
            $wpdb->query("START TRANSACTION;");
            $createTime = date('Y-m-d H:i:s', time());
            $dataYear = ['year' => $year['year'], 'finance' => $year['finance'], 'customer' => $year['customer'], 'operate' => $year['operate'], 'development' => $year['development'], 'status' => $year['status'], 'parent' => $year['id'], 'created' => $createTime, 'chart_id' => $orgchart->id, 'kpi_time' => $year['kpi_time'], 'kpi_type' => $year['kpi_type'] ];

            $dataKPI = ['bank_id' => $result['bank_id'], 'unit' => $result['unit'], 'receive' => $result['receive'], 'parent' => $result['id'], 'chart_id' => $orgchart->id, 'user_id' => $user->ID, 'type' => $result['type'], 'status' => KPI_STATUS_PENDING, 'required' => $result['required'], 'percent' => $result['percent'], 'created' => $createTime,  'owner' => 'yes', 'create_by_node' => $result['create_by_node'] ];
            foreach ($planMonth as $key => $month) {
                $mon = (int)$month['month'];
                $plan = $month['plan'];
                $dataYear['month'] = $mon;
                $dataYear['precious'] = ceil($mon / 3);

                //cập nhật ngày expired
                $checkMonth = array_filter($getMonth, function($item) use ($month){
                    return $item['month'] == $month;
                });
                if( !empty($checkMonth) ){
                    $thisMonth = array_shift($checkMonth);
                    $dataKPI['receive'] = $thisMonth['date_expired'];
                }

	            $validatePlan = validate_input_before_save_db( $unit, $plan, 'plan' );
                if( !empty($validatePlan) ){
                	$httpCode = 401;
	                $message = $validatePlan['textErrorUnit'];
                	send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
                }
	            if( !empty($plan) ) {
		            $percent = $month['percent'];
		            $percent = preg_replace( "#(%)#i", "", $percent );
		            $percent = !empty( $percent ) ? (int)$percent : $result['percent'];
		            if ( !is_int( $percent ) ) {
			            $httpCode = 401;
			            $message  = __( 'Trọng số là số nguyên', TPL_DOMAIN_LANG );
			            send_response_json( [ 'code' => $httpCode, 'message' => $message ], $httpCode, $message );
		            }
	            }else{
		            $plan = '';
		            $percent = '';
	            }
	            if( empty($yearChildren) ) {
		            $wpdb->insert( $tableKpiYear, $dataYear );
		            $idYear = $wpdb->insert_id;
	            }else{
		            $filterArray = array_filter($yearChildren, function($item) use ($mon){
			            return $mon == $item['month'];
		            });
		            $idYear = 0;
		            if( count($filterArray) == 1 ){
			            $filterArray = array_shift($filterArray);
		            	$idYear = $filterArray['id'];
		            }
	            }

                if( !empty( $wpdb->last_error ) ){
                	$error = $wpdb->last_error;
                	$query = $wpdb->last_query;
                    $wpdb->query("ROLLBACK;");
                    $message = __('Đã xảy ra lỗi vui lòng thử lại', TPL_DOMAIN_LANG );
                    $httpCode = 415;
                    send_response_json( ['code' => $httpCode, 'message' => $message, 'error' => $error, 'query' => $query], $httpCode, $message );
                }else{

                    $dataKPI['year_id'] = $idYear;
                    $dataKPI['plan'] = $plan;
	                $dataKPI['percent'] = !empty($percent) ? $percent : '';
                    $wpdb->insert( $tableKpi, $dataKPI );
                    if( !empty( $wpdb->last_error ) ){
                    	$error = $wpdb->last_error;
                        $wpdb->query("ROLLBACK;");
                        $message = __('Đã xảy ra lỗi vui lòng thử lại', TPL_DOMAIN_LANG );
                        $httpCode = 415;
                        send_response_json( ['code' => $httpCode, 'message' => $message, 'error' => $error], $httpCode, $message );
                    }
                }
            }
            $wpdb->query("COMMIT;");
            $message = __('Lưu thành công', TPL_DOMAIN_LANG );
            $httpCode = 201;
            send_response_json( ['code' => $httpCode, 'message' => $message], $httpCode, $message );
        }
    }
}