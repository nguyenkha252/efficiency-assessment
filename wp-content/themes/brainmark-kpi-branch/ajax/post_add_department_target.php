<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/9/18
 * Time: 13:58
 */


function wp_ajax_post_add_department_target($params) {
    global $wpdb;
    require_once THEME_DIR . '/inc/lib-orgchart.php';
    $params = wp_slash( $params );
    $results = [];
    $user = wp_get_current_user();
    $keys = ['type', 'year_id', 'bank_id', 'plan', 'receive', 'percent', 'unit', 'chart_id'];
    # 'time_type', 'time_value', 'departments';
    $nonce = isset($params['_wpnonce']) ? $params['_wpnonce'] : '';
    $kpiID = isset($params['kpiID']) ? $params['kpiID'] : 0;

    if( !wp_verify_nonce($nonce, 'add_department_target') ) {
        $msg = "Sai mã bảo vệ";
        send_response_json(['code' => 403, 'message' => $msg], 403, $msg);
    }

    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableKpi = "{$prefix}kpis";
    $tableKpiYear = "{$prefix}kpi_years";

    if( empty($params['year_id']) || !is_numeric($params['year_id']) ) {
        $msg = "Tạo trọng số cho từng loại mục tiêu trước khi triển khai";
        send_response_json(['code' => 400, 'message' => $msg], 400, $msg);
    }

    # $year_id = intval($params['year_id']);
    # $wpdb->get_var($wpdb->prepare("SELECT id FROM {$tableKpiYear} WHERE id = %d", $params['year_id']) );
    if( empty($params['year_id']) ) {
        $msg = "KPI cho năm {$params['time_value']} không tồn tại";
        send_response_json(['code' => 400, 'message' => $msg], 400, $msg);
    }
    $year_id = intval($params['year_id']);

    $data = [];
    foreach($keys as $k) {
        $data[$k] = isset($params[$k]) ? $params[$k] : null;
    }
    if( !in_array($params['time_type'], ['year', 'precious', 'month']) ) {
        $params['time_type'] = 'year';
    }
    $data['receive'] = kpi_mysql_date($params['receive']);

    # $data[$params['time_type']] = $params['time_value'];
    $data['parent']     = 0;
    $data['user_id']    = $user->ID;
    $data['aproved']    = 0;
    $data['create_by_node'] = KPI_CREATE_BY_NODE_MIDDLE;
    $data['status']     = KPI_STATUS_DRAFT;
    $data['required']   = 'no';
    $data['created']    = date('Y-m-d H:i:s', time());

    $errorList = [];
    $successList = [];

    # update kpi
    $kpi_id = $wpdb->get_var( $wpdb->prepare("SELECT k.id FROM `{$tableKpi}` as k " .
        "WHERE (k.`parent` = 0) AND (k.`type` LIKE %s) AND (k.`year_id` = %d) AND (k.bank_id = %d)",
        $data['type'], $year_id, $data['bank_id']) );

    if( !empty($kpiID) ) {
        require_once THEME_DIR . '/inc/lib-kpis.php';
        $resultKpi = kpi_get_kpi_by_id( $kpiID );
        extract($params);
        $data = compact('bank_id', 'plan', 'unit', 'receive', 'percent');
        $data['receive'] = kpi_mysql_date($data['receive']);
        $result_kpi = 0;
        if( !empty( $resultKpi ) && !is_wp_error( $resultKpi ) ) {
            $wpdb->query('START TRANSACTION;');
            $result_kpi = $resultKpi['id'];
            $parent = $wpdb->update($tableKpi, $data, ['id' => $result_kpi]);
            if( !empty( $wpdb->last_error ) ) {
                $errorList[$result_kpi] = __LINE__ . "\n{$wpdb->last_error}\n{$wpdb->last_query}";
            } else {
                $successList[$result_kpi] = "Cập nhật thành công";
            }

            if( empty( $errorList ) ) {
                $wpdb->query('COMMIT;');
                $msg = __('Lưu thành công', TPL_DOMAIN_LANG);
                $httpCode = 201;
            } else {
                $wpdb->query('rollback;');
                $msg = "Có lỗi xảy ra khi lưu KPI";
                $httpCode = 400;
            }
        } else {
            $msg = __("Không tìm thấy KPI", TPL_DOMAIN_LANG);
            $httpCode = 404;
        }
        send_response_json(['code' => $httpCode, 'message' => $msg, 'success' => $successList, 'error' => $errorList], $httpCode, $msg);
    } else {
        #insert kpi
        $wpdb->query("START TRANSACTION;");
        # CEO Insert KPI
        if( array_key_exists('kpiID', $data) ) {
            unset($data['kpiID']);
        }
        $res_id = $wpdb->insert($tableKpi, $data);
        $insert_id = $wpdb->insert_id;

        if( $wpdb->last_error ) {
            if( (strpos($wpdb->last_error, "Duplicate entry") !== false) ) {
                $insert_id = $wpdb->get_var($wpdb->prepare("SELECT id FROM $tableKpi ".
                    "WHERE (`year_id` = %d) AND (`bank_id` = %d) AND (`chart_id` = %d) ".
                    "AND (`user_id` = %d) AND (`parent` = %d) AND (`type` = %s)",
                    $year_id, $data['bank_id'], $data['chart_id'], $data['user_id'],
                    $data['parent'], $data['type']));
                $res_id = 0;
                if( is_numeric($insert_id) && $insert_id > 0 ) {
                    $res_id = $wpdb->update($tableKpi, $data, ['id' => $insert_id]);
                }
            } else {
                $errorList[] = __LINE__ . "\n{$wpdb->last_error}\n{$wpdb->last_query}";
            }
        } else {
            $successList[$insert_id] = "Thêm KPI thành công";
        }

        if( !empty($errorList) ) {
            $wpdb->query("rollback;");
            $msg = "Có lỗi xảy ra khi thêm KPI";
            send_response_json(['code' => 400, 'message' => $msg, 'success' => $successList, 'errors' => $errorList, 'success' => $successList], 400, $msg);
        }

        $wpdb->query("COMMIT;");
        $msg = "Thêm KPI thành công";
        send_response_json(['code' => 201, 'message' => $msg, 'success' => $successList], 201, $msg);
    }
}
