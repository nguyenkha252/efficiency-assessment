<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/6/18
 * Time: 10:15
 */


function wp_ajax_post_create_edit_post_effa( $params ){
    global $wpdb;
    $user = wp_get_current_user();
	$post_id = $params['post_id'];
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    if( !wp_verify_nonce($params['_wpnonce'], 'create_posts') ){
        $message = __('Phiên bản đã hết hạn. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],408, $message);
    }
    if( empty($user->allcaps['create_posts']) && !$user->has_cap('create_posts') && empty($post_id) ) {
        $message = __('Bạn không được cấp quyền thêm bài viết' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],402, $message);
    }elseif( empty($user->allcaps['edit_posts']) && !$user->has_cap('edit_posts') && !empty($post_id) ){
	    $message = __('Bạn không được cấp quyền chỉnh sửa bài viết' ,TPL_DOMAIN_LANG );
	    send_response_json(['error' => $message],405, $message);
    }
    $postarr = wp_slash($params);
	$postarr['ID'] = $post_id;
    $post_year = date('Y', time());
    unset( $postarr['post_id'] );
    extract( $postarr );
    $data = compact('ID', 'post_title', 'post_code', 'post_content', 'meta_input');
    add_filter( 'wp_insert_post_data', 'wp_insert_post_data_column_post_code', 10, 2);
    $wpdb->query("START TRANSACTION;");
    if( !empty( $post_id ) ){
    	$post = get_post( $post_id );
    	if( !$post ){
    		$message = __( 'Không tìm thấy mục tiêu. Vui lòng thử lại' ,TPL_DOMAIN_LANG);
		    send_response_json(['error' => $message],404, $message);
	    }
        if( $post->post_code != $data['post_code'] ){
            /**
             * Kiểm tra mã năng lực đã tồn tại theo năm
             * Nếu tồn tại thông báo cho người dùng biết để chỉnh sửa lại file import
             */
            if( effa_site_check_post_code('efficiency', $data["post_code"], $post_year) ){
                $wpdb->query("ROLLBACK;");
                $msg = __('Mã năng lực đã tồn tại. Vui lòng chọn mã khác.', TPL_DOMAIN_LANG);
                $httpCode = 410;
                send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
            }
        }
	    $update = wp_update_post($data);
    	if( $update == 0 || is_wp_error($update)) {
		    $message = __( 'Chỉnh sửa mục tiêu thất bại. Vui lòng thử lại', TPL_DOMAIN_LANG );
		    send_response_json( [ 'error' => $message ], 401, $message );
	    }
    }else {
    	unset($data['ID']);
        $data['post_year'] = $post_year;
        $data['post_parent'] = $postarr['post_parent'];
        $data['post_status'] = "publish";
        $data['post_type'] = "efficiency";
        /**
         * Kiểm tra mã năng lực đã tồn tại theo năm
         * Nếu tồn tại thông báo cho người dùng biết để chỉnh sửa lại file import
         */
        if( effa_site_check_post_code('efficiency', $data["post_code"], $post_year) ){
            $wpdb->query("ROLLBACK;");
            $msg = __('Mã năng lực đã tồn tại. Vui lòng chọn mã khác.', TPL_DOMAIN_LANG);
            $httpCode = 410;
            send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
        }
	    $insert_id = wp_insert_post( $data );
	    if( $insert_id == 0 || is_wp_error($insert_id) ) {
		    $message = __( 'Tạo mục tiêu thất bại. Vui lòng thử lại', TPL_DOMAIN_LANG );
		    send_response_json( [ 'error' => $message ], 401, $message );
	    }
    }
    remove_filter( 'wp_insert_post_data', 'wp_insert_post_data_column_post_code', 10, 2);
    $wpdb->query("COMMIT;");
    $message = __('Thành công', TPL_DOMAIN_LANG);
    send_response_json(['success' => $message], 201, $message);

}