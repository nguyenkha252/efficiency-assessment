<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 11/19/18
 * Time: 4:51 PM
 */


require_once THEME_DIR . '/inc/effa.php';
require_once THEME_DIR . '/inc/evaluate.php';
require_once THEME_DIR . '/inc/lib-settings.php';
require_once THEME_DIR . '/inc/vendor/autoload.php';
/*require_once THEME_DIR . '/inc/vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';
require_once THEME_DIR . '/inc/vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once THEME_DIR . '/inc/vendor/phpoffice/phpexcel/Classes/PHPExcel/Worksheet/Drawing.php';*/
function wp_ajax_get_export_effa_report_list_staff($params){
    global $wpdb;
    $year = date('Y', time());
    $user = wp_get_current_user();
    $params = wp_slash($params);
    if( !user_is_manager() ){
        $message = __('Bạn không có quyền thực hiện thao tác này' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],403, $message);
    }
    if( !wp_verify_nonce($params['_wpnonce'], 'export_effa_report_list_staff') ){
        $message = __('Phiên bản đã hết hạn. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],408, $message);
    }
    $rid = isset($params) ? (int)$params['rid'] : 0;
    $res = isset($params) ? (int)$params['res'] : 0;
    $effa = isset($params) ? (int)$params['effa'] : 0;

    if( $rid > 0 ){
        $orgchart = effa_site_orgchart_get_chart_by_id($rid);
        if( empty($orgchart) ){
            $message = __('Không tìm thấy chức danh vui lòng thử lại' ,TPL_DOMAIN_LANG );
            send_response_json(['error' => $message],404, $message);
        }
    }
    $post = [];
    if( $effa > 0 ){
        $post = get_post($effa);
        if( !$post ){
            $message = __('Không tìm thấy năng lực vui lòng thử lại' ,TPL_DOMAIN_LANG );
            send_response_json(['error' => $message],404, $message);
        }
    }

    $fileTemplate = THEME_DIR . '/assets/template/exports/' . COMPETENCY_EXPORT_LIST_STAFF;
    $upload = wp_upload_dir();
    $date = date('d-m-Y-H-i-s', current_time('timestamp') );
    $file_export = explode('.', COMPETENCY_EXPORT);
    $extra = $file_export[count($file_export) - 1];
    unset($file_export[count($file_export) - 1]);
    array_push($file_export, $date, uniqid(), $extra);
    $file_export = implode(".", $file_export);
    $fileCopy = "{$upload['basedir']}/exports/" . $file_export;
    $file_download = str_replace($upload['basedir'], $upload['baseurl'], $fileCopy);
    if (!is_dir("{$upload['basedir']}/exports")) {
        mkdir("{$upload['basedir']}/exports", 0777, true);
    }
    @copy($fileTemplate, $fileCopy);
    @chmod($fileCopy, 0777);
    $identify = PHPExcel_IOFactory::identify($fileCopy);
    $objReader = PHPExcel_IOFactory::createReader($identify);
    $objPHPExcel = $objReader->load($fileTemplate);
    $activeSheet = $objPHPExcel->getActiveSheet();
    $highestColumn = $activeSheet->getHighestColumn();
    $highestColumn++;
    $settings = get_settings_website();

    $logo_webiste = !empty( $settings['logo_url'] ) ? $settings['logo_url'] : THEME_URL . '/assets/images/logo-brainmark.png';
    $title_website = !empty($settings['title_website']) ? $settings['title_website'] : __('KPI SYSTEM', TPL_DOMAIN_LANG);
    $path_logo = str_replace($upload['baseurl'], $upload['basedir'], $logo_webiste);
    if( empty($settings['logo_url']) ){
        $path_logo = str_replace(THEME_URL, THEME_DIR, $logo_webiste);
    }
    $checkPathLogo = wp_remote_get($logo_webiste);
    if($checkPathLogo['response']['code'] != 404) {
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
        $objDrawing->setPath($path_logo);
        $objDrawing->setHeight(56);
        $objDrawing->setCoordinates('A1')
            ->setOffsetX(10)
            ->setOffsetY(5);
    }
    $text_res = "";
    if( $res == 1 ){
        $text_res = __('ĐẠT', TPL_DOMAIN_LANG);
        $text_descrip = "";
    }elseif($res == 2){
        $text_res = __('KHÔNG ĐẠT', TPL_DOMAIN_LANG);
        $text_descrip = __('CẦN ĐƯỢC ĐÀO TẠO', TPL_DOMAIN_LANG);
    }
    $activeSheet
        ->setCellValue("A1", '')
        ->setCellValue("D1", $title_website )
        ->setCellValue("F5", $text_res)
        ->setCellValue("B6", $text_descrip);
    ;
    if( $rid > 0 ){
        $activeSheet->setCellValue('E7', $orgchart->room);
    }else{
        $activeSheet->setCellValue('D7', "")
                    ->setCellValue('E7', "");
    }
    if( $effa > 0 ){
        $activeSheet->setCellValue('E8', str_replace(str_split("\|"), "", $post->post_title));
    }else{
        $activeSheet->setCellValue('D8', '')
                    ->setCellValue('E8', '');
    }


    $start_row = 10;
    $row_added = 0;

    $users = get_reported_users($rid, $res, $effa);
    foreach ($users as $key => $user) {
        $row_added++;
        $this_row_item = $start_row + $row_added;
        $activeSheet->insertNewRowBefore($this_row_item, 1);
        $activeSheet->setCellValue("A{$this_row_item}", ($row_added + 1) )
                    ->setCellValue("B{$this_row_item}", $user->user_nicename )
                    ->setCellValue("C{$this_row_item}", $user->display_name )
                    ->setCellValue("D{$this_row_item}", $user->name )
                    ->setCellValue("E{$this_row_item}", $user->room )
                    ->setCellValue("F{$this_row_item}", $user->total_percent . "%" )
        ;

    }
    for ($col = "A"; $col != $highestColumn; $col++){
        $activeSheet->duplicateStyle($activeSheet->getStyle("{$col}" . ($start_row)), "{$col}" . ($start_row + 1) . ":{$col}" . ($start_row + $row_added));
    }
    $activeSheet->removeRow($start_row, 1);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $identify);
    $objWriter->save($fileCopy);
    send_response_json( ['url_download' => $file_download], 201, __('OK', TPL_DOMAIN_LANG ));
}