<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/14/18
 * Time: 23:19
 */

function wp_ajax_post_user_role_add($params) {
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableUsers = "{$prefix}users";
    $tableUserMeta = "{$prefix}usermeta";
    $tableCharts = "{$prefix}kpi_charts";
    $keys = ['assign_task' => 0, 'user_id' => 0, 'role_id' => 0, 'parent' => 0, 'x' => 0, 'y' => 0, 'is_primary' => 0, 'level' => 0, 'siblings' => '[]'];

    $insertData = $keys;
    $insertData['created']      = date('Y-m-d H:i:s', time());
    $insertData['assign_task']  = isset($params['assign_task']) ? intval($params['assign_task']) : 0;
    $insertData['user_id']      = (int)$params['ten_chuc_danh_truong'];
    $insertData['role_id']      = (int)$params['chuc_danh_truong'];
    $insertData['is_primary']   = 1; # (int)$params['is_primary'];
    $insertData['level']        = (int)$params['level'];
    $insertData['parent']       = (int)$params['parent'];

    $errors = [];

    $params['ten']              = strip_tags($params['ten']);
    $insertData['name']         = $params['ten'];
    # $wpdb->query("START TRANSACTION;");
    $success = [];
    $id = $wpdb->insert($tableCharts, $insertData);
    if( is_numeric($id) ) {
        $parent_id          = $wpdb->insert_id;
        $d                  = $insertData;
        $d['siblings']      = [];
        $d['ID']            = $parent_id;
        $success[]          = $d;
        $insertData['is_primary']   = 0;
        $insertData['parent']       = $parent_id;
        $siblings = [];
        if( !empty($params['chuc_danh_pho']) ) {
            $insertData['name']        = $params['ten'] . " (phó)";
            $insertData['assign_task'] = 0;
            foreach($params['chuc_danh_pho'] as $info) {
                $insertData['user_id'] = $info['user_id'];
                $insertData['role_id'] = $info['ten'];
                $id = $wpdb->insert($tableCharts, $insertData);
                if( is_numeric($id) ) {
                    $d = $insertData;
                    $d['ID']        = $wpdb->insert_id;
                    $success[]      = $d;
                    $siblings[]     = (int)$wpdb->insert_id;
                } else if($wpdb->last_error) {
                    if( strpos($wpdb->last_error, 'Duplicate entry') === false ) {
                        $errors[] = $wpdb->last_error;
                    } else {
                        $row = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$tableCharts} ".
                            "WHERE `parent` = %d AND `user_id` = %d AND role_id = %d",
                            $parent_id, $insertData['user_id'], $insertData['role_id']) );
                        $success[] = $row;
                        if( !empty($row) && !in_array((int)$row['ID'], $siblings) ) {
                            $siblings[] = (int)$row['ID'];
                        }
                        $d          = $insertData;
                        $d['ID']    = $row['ID'];
                    }
                }
            }
        }
        if( !empty($siblings) ) {
            $wpdb->update($tableCharts, ['siblings' => json_encode($siblings)], ['ID' => $parent_id]);
        }
        $success[0]['siblings'] = $siblings;
    } else if($wpdb->last_error) {
        $errors[] = $wpdb->last_error;
    }
    if( empty($errors) ) {
        # $wpdb->query("COMMIT;");
        $msg = "Tạo thành công";
        send_response_json(['code' => 201, 'items' => $success], 201, $msg);
    } else {
        # $wpdb->query("rollback;");
        $msg = "Có lỗi xảy ra";
        send_response_json(['code' => 400, 'message' => $msg, 'error' => $errors, 'items' => $success], 400, $msg);
    }
}