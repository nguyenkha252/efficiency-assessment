<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 08/02/2018
 * Time: 00:07
 */
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-users.php';
function wp_ajax_post_delete_capacity( $params ){
	global $wpdb;
	$params = wp_slash($params);
	$user = wp_get_current_user();
	if (!$user) {
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG);
		$httpCode = 401;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	if( !user_is_manager() ){
		$message = __('Bạn không có quyền xóa', TPL_DOMAIN_LANG);
		$httpCode = 403;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	/*if (!wp_verify_nonce($params['_wpnonce'], 'delete_capacity')) {
		$message = __('Mã bảo vệ không hợp lệ', TPL_DOMAIN_LANG);
		$httpCode = 403;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}*/
	$result = capacity_get_kpi_and_bank_by_id( $params['id'] );
	if( !empty( $result ) ){
		$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
		$tableKpiCP = "{$prefix}kpis_capacity";
		$wpdb->delete($tableKpiCP, ['id' => $params['id']]);
		$httpCode = 201;
		send_response_json(['code' => $httpCode, 'success' => 1], $httpCode, "OK");
	}else{
		$msg = "Không tìm thấy năng lực quản lý";
		$httpCode = 404;
		send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
	}
}