<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/15/18
 * Time: 11:26
 */

function wp_ajax_post_chuc_danh_create($params) {
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );

    $nonce = !empty($params['_wpnonce']) ? $params['_wpnonce'] : '';
    if( !wp_verify_nonce($nonce, 'chuc_danh_create') ) {
        $msg = 'Mã bảo vệ không đúng';
        send_response_json(['code' => 403, 'message' => $msg], 403, $msg);
    }

    $tableRoles = "{$prefix}kpi_roles";
    $keys = ['chuc_danh' => 'Tên chức danh không được bỏ trống', 'cap_bac' => 'Cấp bậc của chức danh không được bỏ trống'];
    $insertData = [];
    $error = [];
    foreach($keys as $k => $msg) {
        if( isset($params[$k]) ) {
            if( empty($params[$k]) ) {
                $error[$k] = $msg;
            } else {
                $insertData[$k] = strip_tags($params[$k]);
            }
        }
    }

    if( empty($error) ) {
        $insertData['ngay_tao'] = date('Y-m-d H:i:s', time());
        $res = $wpdb->insert($tableRoles, $insertData);
        if( $res !== false ) {
            if( $wpdb->insert_id ) {
                $insertData['ID'] = $wpdb->insert_id;
                send_response_json($insertData, 201, 'Tạo thành công');
            }
        } else if( $res === 0 ) {
            send_response_json(['code' => 400, 'message' => 'Insert không thành công'], 400, 'Insert không thành công');
        } else {
            if( $wpdb->last_error ) {
                $error['db'] = $wpdb->last_error;
            }
        }

    }
    if( !empty($error) ) {
        $msg = implode("\n", $error);
    } else {
        $msg = 'Có lỗi xảy ra';
    }
    send_response_json(['code' => 400, 'message' => $msg, 'errors' => $error], 400, 'Có lỗi xảy ra');
}