<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/6/18
 * Time: 14:13
 */

function wp_ajax_get_load_post_effa( $params ){
    global $wpdb;
    $user = wp_get_current_user();
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['message' => $message],401, $message);
    }
    $postarr = $wpdb->escape($params);
    $post_id = (int)$postarr['id'];
    $post = get_post( $post_id );
    if( !$post ){
        $message = __('Không tìm thấy' ,TPL_DOMAIN_LANG );
        send_response_json(['message' => $message],404, $message);
    }
    $targetID = $post->ID;
    $titleTarget = html_entity_decode(str_replace(str_split("\|"), "", get_the_title($targetID)));
    $post_code = !empty( $post->post_code ) ? $post->post_code : "";
    $effa_assessment_level_1 = isset($post->_effa_assessment_level_1) ? $post->_effa_assessment_level_1 : "";
    $effa_assessment_level_2 = isset($post->_effa_assessment_level_2) ? $post->_effa_assessment_level_2 : "";
    $effa_assessment_level_3 = isset($post->_effa_assessment_level_3) ? $post->_effa_assessment_level_3 : "";
    $effa_assessment_level_4 = isset($post->_effa_assessment_level_4) ? $post->_effa_assessment_level_4 : "";
    $effa_assessment_level_5 = isset($post->_effa_assessment_level_5) ? $post->_effa_assessment_level_5 : "";
	$postParentID = $post->post_parent;
    $content = strip_tags( $post->post_content );
    $data = [
        'post_title' => $titleTarget,
        'post_code' => $post_code,
        'meta_input[_effa_assessment_level_1]' => $effa_assessment_level_1,
        'meta_input[_effa_assessment_level_2]' => $effa_assessment_level_2,
        'meta_input[_effa_assessment_level_3]' => $effa_assessment_level_3,
        'meta_input[_effa_assessment_level_4]' => $effa_assessment_level_4,
        'meta_input[_effa_assessment_level_5]' => $effa_assessment_level_5,
        'post_content' => $content,
	    'post_parent' => $postParentID,
        'post_id' => $targetID,

    ];
    send_response_json( ['message' => __('Thành công', TPL_DOMAIN_LANG), 'data' => $data], 201, __('OK', TPL_DOMAIN_LANG ));
}