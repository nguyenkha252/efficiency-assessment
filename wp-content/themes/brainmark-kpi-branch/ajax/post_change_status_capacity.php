<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/8/18
 * Time: 09:18
 */
require_once THEME_DIR . '/inc/lib-kpis.php';
function wp_ajax_post_change_status_capacity( $params ){
    global $wpdb;
    $user = wp_get_current_user();
	$params = wp_slash($params);
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
	if( !user_is_manager() ){
		$message = __('Bạn không có quyền xóa', TPL_DOMAIN_LANG);
		$httpCode = 403;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	$result = capacity_get_kpi_and_bank_by_id( $params['id'] );
	if( empty( $result ) ){
		$msg = "Không tìm thấy năng lực quản lý";
		$httpCode = 404;
		send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
	}
    if( !empty( $params['status'] ) ){
	    $params['status'] = KPI_STATUS_RESULT;
    }else{
	    $params['status'] = KPI_STATUS_DRAFT;
    }
    extract( $params );
    $data = compact( 'id', 'status' );
	$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
	$tableKpiCP = "{$prefix}kpis_capacity";
    $id = $wpdb->update( $tableKpiCP, ["status" => $data['status']], ['id' => $data['id']] );
    if( is_wp_error( $id ) ){
        # @TODO process error
        $message = __('Chỉnh sửa mục tiêu KPI thất bại. Vui lòng thử lại' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],415, $message);
    }else{
        if( $params['status'] === KPI_STATUS_RESULT ){
            $message = __('Đã áp dụng', TPL_DOMAIN_LANG);
        }else{
            $message = __('Chưa áp dụng', TPL_DOMAIN_LANG);
        }
        send_response_json(['success' => $message], 200, $message);
    }
}