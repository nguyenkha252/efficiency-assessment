<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 08/02/2018
 * Time: 01:10
 */
require_once THEME_DIR . '/inc/lib-kpis.php';
function wp_ajax_post_save_and_assign_capacity_member( $params ){
	global $wpdb;
	$user = wp_get_current_user();
	$orgchart = user_load_orgchart($user);
	$params = wp_slash( $params );
	if( !$user ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],401, $message);
	}
	if( !user_is_manager() ){
		$message = __('Bạn không có quyền xóa', TPL_DOMAIN_LANG);
		$httpCode = 403;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	if( !wp_verify_nonce($params['_wpnonce'], 'save_capacity_member') ){
		$message = __('Phiên bản đã hết hạn. Vui lòng tải lại trang.' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],408, $message);
	}

	$type_nam = isset($params['type_nam']) ? $params['type_nam'] : [];
	$type_kq = isset($params['type_kq']) ? $params['type_kq'] : [];
	$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
	$tableKpiCP = "{$prefix}kpis_capacity";
    $year_id = $params['year_id'];
    $apply_of = APPLY_OF_EMPLOYEES;
    $chart_id = $orgchart->id;
    $user_id = $user->ID;
	if( !empty( $type_nam ) ){
        $wpdb->query("START TRANSACTION;");
	    foreach ( $type_nam as $key => $itemNam ){
	        $type_kpis = $key == 'cty' ? 'cong-ty' : 'phong-ban';
	        $type = CAPACITY_CA_NAM;
	        $percent = $itemNam['percent'];
	        $plan = $itemNam['plan'];
	        $note = $itemNam['note'];

	        $id = $itemNam['ID'];
            $status = KPI_STATUS_RESULT;
            $modified = date('Y-m-d H:i:s', time() );
	        $dataNam = compact("type_kpis", "user_id", "chart_id", "apply_of", "plan", "percent", "year_id", "note", "type", "status", "modified");
            $result = capacity_get_kpi_by_id( $id );
            if( !empty( $result ) ){
                $wpdb->update( $tableKpiCP, $dataNam, ['id' => $id] );
            }else{
                $msg = "Không tìm thấy năng lực nhân viên";
                $httpCode = 404;
                send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
            }
            if( !empty( $wpdb->last_error ) ){
                $wpdb->query("ROLLBACK;");
                $msg = "Có lỗi xảy ra khi lưu";
                $httpCode = 400;
                send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
            }
        }
        $wpdb->query("COMMIT;");
    }
	if( !empty( $type_kq ) ){
        $wpdb->query("START TRANSACTION;");
	    foreach ( $type_kq as $key => $itemKQ ){
	        $type_kpis = $key == 'cty' ? 'cong-ty' : 'phong-ban';
	        $type = CAPACITY_KET_QUA_QUA_TRINH;
	        $percent = $itemKQ['percent'];
	        $id = $itemKQ['ID'];
            $status = KPI_STATUS_RESULT;
            $modified = date('Y-m-d H:i:s', time() );
	        $dataKQ = compact("type_kpis", "user_id", "chart_id", "apply_of", "percent", "year_id", "note", "type", "status", "modified");
            $result = capacity_get_kpi_by_id( $id );
            if( !empty( $result ) ) {
                $wpdb->update($tableKpiCP, $dataKQ, ['id' => $id]);
            }else{
                $msg = "Không tìm thấy năng lực quản lý";
                $httpCode = 404;
                send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
            }
            if( !empty( $wpdb->last_error ) ){
                $wpdb->query("ROLLBACK;");
                $msg = "Có lỗi xảy ra khi lưu";
                $httpCode = 400;
                send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
            }
        }
        $wpdb->query("COMMIT;");
    }

	if( array_key_exists('kpi_capacity', $params) ) {
		$wpdb->query("START TRANSACTION;");
		foreach ( $params['kpi_capacity'] as $key => $item ) {
			$id = $item['id'];
			$result = capacity_get_kpi_and_bank_by_id( $id );
			if( !empty( $result ) ){
				$wpdb->update($tableKpiCP, ['status' => KPI_STATUS_RESULT], ['id' => $id]);
				if( !empty( $wpdb->last_error ) ){
					$wpdb->query("ROLLBACK;");
					$msg = "Có lỗi xảy ra khi lưu";
					$httpCode = 400;
					send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
				}
			}else {
				$wpdb->query( "ROLLBACK;" );
				$msg      = "Không tìm thấy năng lực nhân viên";
				$httpCode = 404;
				send_response_json( [ 'code' => $httpCode, 'message' => $msg ], $httpCode, $msg );
			}
		}
        $wpdb->query("COMMIT;");
	}
	$msg = "Thành công";
	$httpCode = 201;
	send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
}