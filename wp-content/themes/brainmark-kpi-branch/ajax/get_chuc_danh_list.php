<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/17/18
 * Time: 00:19
 */

function wp_ajax_get_chuc_danh_list($params) {
    global $wpdb;
    $results = kpi_get_danh_sach_chuc_danh(!empty($params['chuc_danh']) ? $params['chuc_danh'] : '', !empty($params['cap_bac']) ? $params['cap_bac'] : '');

    send_response_json(['items' => $results, 'sql' => $wpdb->last_query, 'error' => $wpdb->last_error], 202, 'Thành công');
}