<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10/8/18
 * Time: 2:44 PM
 */

function wp_ajax_post_assessment_manager_effa($params){
    global $wpdb;
    $user = wp_get_current_user();
    $orgChartID = isset($user->orgchart_id) ? $user->orgchart_id : 0;
    $params = $wpdb->escape($params);
    $getUser = effa_site_get_user_by_id($params['user_id']);
    $getOrgchart = effa_site_user_load_orgchart($getUser);
    $orgchart_id = $getOrgchart->id;
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    $checkChildren = effa_site_orgchart_check_on_level($orgchart_id, $orgChartID);
    if( !user_is_manager() && empty($checkChildren) ){
        $message = __('Bạn không có quyền thực hiện thao tác này' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],403, $message);
    }
    $postID = $params['post_id'];
    $year = $params['year'];
    $post = get_post( $postID );
    if( !$post ){
        $message = __( 'Không tìm thấy mục tiêu. Vui lòng thử lại' ,TPL_DOMAIN_LANG);
        send_response_json(['error' => $message],404, $message);
    }
    $efficiency = effa_site_efficiency_get_by_id($params['id']);
    if( empty($efficiency) ){
        $message = __( 'Không tìm thấy mục tiêu của chức danh' ,TPL_DOMAIN_LANG);
        send_response_json(['error' => $message],404, $message);
    }
    $assign = effa_site_assign_get_by_postid_chartid_year($postID, $orgchart_id, $year);
    if( empty($assign) ){
        $message = __( 'Không tìm thấy mục tiêu của chức danh' ,TPL_DOMAIN_LANG);
        send_response_json(['error' => $message],404, $message);
    }
    $efficiencyUser = effa_site_efficiency_get_by_assign_id($assign->id, $getUser->ID);
    $created =  current_time( 'mysql' );
    $prefix = $wpdb->get_blog_prefix(get_current_blog_id());
    $tableEffa = "{$prefix}efficiency_assessment";
    $errors = [];
    $wpdb->query("START TRANSACTION;");
    if( !empty($efficiencyUser) ){
        #update
        $data['manager_lv1'] = $params['value'];
        $wpdb->update($tableEffa, $data, ['id' => $efficiencyUser->id]);
        if( !empty($wpdb->last_error) ){
            $errors[] = $wpdb->last_error;
        }
    }else{
        #insert
        $data = [
            'assign_id' => $efficiency->assign_id,
            'user_id' => $getUser->ID,
            'important' => $efficiency->important,
            'standard' => $efficiency->standard,
            'manager_lv1' => $params['value'],
            'created' => $created,
        ];
        $wpdb->insert($tableEffa, $data);
        if( !empty($wpdb->last_error) ){
            $errors[] = $wpdb->last_error;
        }
    }

    if( !empty($errors) ){
        # @TODO process error
        $wpdb->query("ROLLBACK;");
        $message = __('Đánh giá thất bại' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],415, $message);
    }else{
        $wpdb->query("COMMIT;");
        $message = __('Thành công', TPL_DOMAIN_LANG);
        send_response_json(['success' => $message], 201, $message);
    }
}