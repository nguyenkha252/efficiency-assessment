<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 7/10/18
 * Time: 10:55 PM
 */

function wp_ajax_post_remove_logo($params){
    $params = wp_unslash($params);
    $user = wp_get_current_user();
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['message' => $message],401, $message);
    }
    if( !user_is_manager() ) {
        $message = __('Bạn không được cấp quyền chỉnh sửa logo' ,TPL_DOMAIN_LANG );
        send_response_json(['message' => $message],403, $message);
    }
    $options = get_option('settings_website', true);
    if( !empty( $options ) ){
        $options['logo_url'] = '';
        update_option( 'settings_website', $options );
    }
    $message = __('Success' ,TPL_DOMAIN_LANG );
    send_response_json(['message' => $message],201, $message);
}