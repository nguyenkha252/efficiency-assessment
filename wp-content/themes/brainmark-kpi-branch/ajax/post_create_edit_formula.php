<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 21/01/2018
 * Time: 10:20
 */
function wp_ajax_post_create_edit_formula( $params ){
	global $wpdb;
	require_once THEME_DIR . '/inc/lib-formulas.php';
	$prefix       = $wpdb->get_blog_prefix( get_current_blog_id() );
	$tableKpiFormulas     = "{$prefix}kpi_formulas";
	$params = wp_slash( $params );
	$id = 0;
	if( !empty( $params ) && !empty( $params['formula'] ) ){
		$paramsFormula = $params['formula'];
		if( array_key_exists('ID', $paramsFormula) )
			$id = $paramsFormula['ID'];
	}
	$paramsFormula['note'] = nl2br( $paramsFormula['note'] );
	extract( $paramsFormula );
	$dataInput = compact('title', 'type', 'note');
	$metaInput = $params['meta_input'];
	if( array_key_exists($dataInput['type'], $metaInput) ){
		$metaInput = $metaInput[$dataInput['type']];
		$dataInput['formulas'] = maybe_serialize( $metaInput );
	}
	if( empty( $id ) ){
		$wpdb->insert($tableKpiFormulas, $dataInput);
	}else{
		$result = get_formula_by_id( $id );
		if( empty( $result ) ){
			$msg = __('Không tìm thấy công thức', TPL_DOMAIN_LANG);
			$httpCode = 404;
			send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
		}
		$wpdb->update($tableKpiFormulas, $dataInput, ['ID' => $id]);
	}
	if( !empty( $wpdb->last_error ) ){
		$msg = "Có lỗi xảy ra khi lưu KPI";
		$httpCode = 400;
		send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
	}else{
		$msg = __('Lưu thành công', TPL_DOMAIN_LANG);
		$httpCode = 201;
		send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
	}
}