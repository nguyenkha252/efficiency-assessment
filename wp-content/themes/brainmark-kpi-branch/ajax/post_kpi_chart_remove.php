<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/22/18
 * Time: 11:09
 */


function wp_ajax_post_kpi_chart_remove($params) {
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableCharts = "{$prefix}org_charts";
    $nonce = isset($params['nonce']) ? $params['nonce'] : '';
    if( !wp_verify_nonce($nonce, 'kpi_chart_remove') ) {
        $msg = 'Mã bảo vệ không hợp lệ';
        $status = 403;
        send_response_json(['code' => $status, 'message' => $msg], $status, $msg);
    }
    if( !empty($params['id']) ) {
        $ids = [(int)$params['id']];
        $childs = [(int)$params['id']];
        $results = ['ids' => $childs, 'sql' => []];
        while( true ) {
            $childs = implode(', ', $childs);
            $childs = $wpdb->get_col( "SELECT id FROM $tableCharts WHERE `parent` IN ({$childs})" );
            if( empty($childs) ) {
                break;
            }
            $results['ids'][] = $childs;
            $results['sql'][] = $wpdb->last_query;
            $ids = array_merge($ids, $childs);
        }
        # $msg = 'Debug';
        # send_response_json(['code' => 400, 'message' => implode(', ', $ids), 'results' => $results], 400, $msg);
        # $res = $wpdb->delete($tableCharts, ['id' => $ids]);

        $ids = implode(', ', $ids);
        $res = $wpdb->query("DELETE FROM $tableCharts WHERE `id` IN ({$ids})");

        if( $res === false ) {
            $msg = 'Có lỗi: Xóa chức danh thất bại';
            send_response_json(['code' => 400, 'message' => $msg], 400, $msg);
        } else {
            $success = "Xóa chức danh thành công";
            $charts = kpi_load_org_charts();
            send_response_json(['charts' => $charts, 'message' => $success], 202, $success);
        }
    } else {
        $status = 400;
        $msg = "Tham số gửi về không hợp lệ";
        send_response_json(['code' => $status, 'message' => $msg], $status, $msg);
    }

    /*
    $filename = THEME_DIR . '/contents/charts/version-demo.json';
    $node_id = $params['id'];
    $fileData = file_get_contents($filename);
    if( !empty($fileData) ) {
        $jsonData = json_decode($fileData, true);
        if( !empty($jsonData) ) {
            send_response_json($jsonData, 201, 'OK');
            # $jsonData = sprintf(' data-chart-data="%s"' . esc_json_attr($jsonData));
        }
    }
    send_response_json($jsonData, 400, 'Failed');
    */
    return;
}