<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/22/18
 * Time: 15:00
 */
function wp_ajax_get_load_file_upload_by_parent( $params ){
    require_once THEME_DIR . '/inc/lib-kpis.php';
    $params = wp_unslash($params);
    $user = wp_get_current_user();
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG );
        $httpCode = 401;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }
    $kpiID = (int) $params['id'];
    $result = kpi_get_kpi_by_id( $kpiID );
    if( empty( $result ) ){
        $message = __('Không tìm thấy KPI', TPL_DOMAIN_LANG );
        $httpCode = 401;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }
    $statusKPI = '';
    $resultParent = kpi_get_file_kpi_by_parent( $kpiID );
    if( ( $user->ID != $result['user_id'] && $result['status'] == KPI_STATUS_DONE ) || ( $user->ID == $result['user_id'] && $result['status'] == KPI_STATUS_DONE ) ){
        $statusKPI = 'done';
    }
    if( !empty( $resultParent ) ){
        $output = '';
        $stt = 1;
        foreach ( $resultParent as $key => $item ){
            $files = $item['files'];
            $files = maybe_unserialize( $files );
            if( !empty( $files ) && !empty( $files['files_url'] ) ){
                $upload = wp_upload_dir();
                foreach ( $files['files_url'] as $key_file => $file ){
                    $fileName = explode( "/", $file );
                    if( count( $fileName ) > 1 ) {
                        $fileName = $fileName[ count($fileName) - 1 ];
                    }else{
                        $fileName = $file;
                    }
                    $urlFile = $upload['baseurl'] . '/files/' . $fileName;
                    $output .= "<tr>";
                    $output .=  sprintf("<td class='column-1'>%d</td>", $stt);
                    $output .= sprintf("<td class='column-2'><a href='%s' title='%s'>%s</a></td>", $urlFile, $fileName, $fileName);
                    $output .= "</tr>";
                    $stt++;
                }

            }
        }

        send_response_json(['code' => 201, 'data' => [$output], 'complete' => $statusKPI], 201, __('Thành công', TPL_DOMAIN_LANG));
    }else{
        send_response_json(['code' => 404, 'message' => 'Không tìm thấy'], 201, __('Thành công', TPL_DOMAIN_LANG));
    }



}