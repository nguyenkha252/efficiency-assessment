<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/12/17
 * Time: 00:21
 */

function kpi_get_post_info( $params ) {
    $user = wp_get_current_user();
    global $wpdb;
    if( !empty($params['id']) ) {
        $post = get_post($params['id']);
        $post = $post->to_array();
    } else {
        $post = new WP_Error(404, __("Not found", TPL_DOMAIN_LANG));
    }
    return $post;
}


function kpi_render_posts() {
    global $wpdb;
    $user = wp_get_current_user();
    $can_edit_post = false;
    if( !empty($user->allcaps['level_9']) || $user->has_cap('level_9') ) {
        $can_edit_post = true;
    }
    $query = new WP_Query();
    $query->query(['post_type' => 'post', 'category_name' => __('notification', TPL_DOMAIN_LANG), 'post_status' => 'publish', 'order' => 'post_date', 'orderby' => 'DESC', 'posts_per_page' => 6]);

    $pageNews = get_page_by_path(PAGE_NEWS);
    $edit_link = apply_filters( 'the_permalink', get_permalink( $pageNews ), $pageNews );
    # echo $wpdb->posts . "\n{$query->request}";
    ob_start();
    if( $query->have_posts() ):
        while ($query->have_posts()): $query->the_post();
            $permalink = esc_url( apply_filters( 'the_permalink', get_permalink( $query->post ), $query->post ) );
            ?>
            <li class="post-entry col-lg-3 col-md-4 col-sm-6 post-<?php echo esc_attr($query->post->post_name); ?>">
                <div class="header">
                    <h2><a href="<?php echo $permalink; ?>"><?php echo get_the_title($query->post); ?></a></h2>
                    <?php
                    if( $can_edit_post ):
                        /* ?><a href="<?php echo admin_url('post.php?post='.$query->post->ID.'&action=edit'); ?>"><?php _e('Edit', TPL_DOMAIN_LANG); ?></a><?php */
                        ?><a href="<?php esc_attr_e( add_query_arg('pid', $query->post->ID, $edit_link) ); ?>"><?php _e('Edit', TPL_DOMAIN_LANG); ?></a><?php
                    endif;
                    ?></div>
                <div class="excerpt">
                    <?php
                    # echo apply_filters( 'the_excerpt', get_the_excerpt($query->post) );
                    # the_content();
                    $content = apply_filters( 'the_content', $query->post->post_content );
                    $content = str_replace( ']]>', ']]&gt;', $content );
                    echo $content;
                    ?>
                </div>
                <div class="footer"><a class="readmore" href="<?php echo $permalink; ?>"><?php _e('Chi tiết', TPL_DOMAIN_LANG); ?></a></div>
            </li>
        <?php endwhile;
        wp_reset_query();
    else: ?>
        <li class="notfound">
            <?php _e('Đang cập nhật..', TPL_DOMAIN_LANG); ?>
        </li>
    <?php endif;

    return ob_get_clean();
}

function wp_ajax_get_post_info( $params ) {
    $user = wp_get_current_user();
    if( !wp_verify_nonce($params['_wpnonce'], 'post_info') ) {
        $jsonData = new WP_Error(401, __("Nonce invalid", TPL_DOMAIN_LANG));
    } else if( empty($user->allcaps['list_posts']) && !$user->has_cap('list_posts') ) {
        $jsonData = new WP_Error(403, __("Not allow user permission", TPL_DOMAIN_LANG));
    } else {
        $jsonData = kpi_get_post_info($params);
    }
    if( is_wp_error($jsonData) ) {
        send_response_json($jsonData, $jsonData->get_error_code(), $jsonData->get_error_message());
    } else {
        send_response_json($jsonData, 200);
    }
}


