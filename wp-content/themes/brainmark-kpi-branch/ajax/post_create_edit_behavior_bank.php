<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 07/03/2018
 * Time: 16:28
 */

function wp_ajax_post_create_edit_behavior_bank( $params ){
	global $wpdb;
	$user = wp_get_current_user();
	$post_id = $params['post_id'];
	if( !$user ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],401, $message);
	}
	if( !wp_verify_nonce($params['_wpnonce'], 'create_posts') ){
		$message = __('Phiên bản đã hết hạn. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],408, $message);
	}
	if( empty($user->allcaps['create_posts']) && !$user->has_cap('create_posts') && empty($post_id) ) {
		$message = __('Bạn không được cấp quyền thêm bài viết' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],402, $message);
	}elseif( empty($user->allcaps['edit_posts']) && !$user->has_cap('edit_posts') && !empty($post_id) ){
		$message = __('Bạn không được cấp quyền chỉnh sửa bài viết' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],405, $message);
	}
	$postarr = wp_slash($params);
	$postParentID = (int)$postarr['post_parent'];
	$postParent = get_post( $postParentID );
	if( !$postParent ){
		$message = __('Không tìm thấy nhóm chỉ tiêu' ,TPL_DOMAIN_LANG );
		send_response_json(['error' => $message],404, $message);
	}

	if( isset( $postarr['post_status'] ) ){
		$postarr['post_status'] = 'publish';
	}else{
		$postarr['post_status'] = 'draft';
	}
	$postarr['ID'] = $postarr['post_id'];
	unset( $postarr['post_id'] );
	extract( $postarr );
	#$data = compact( 'ID', 'post_title', 'meta_input', 'post_parent', 'post_status', 'tax_input' , 'post_category', 'post_content' );
	$data = compact('ID', 'post_title', 'post_parent', 'post_status', 'post_category', 'violation_assessment', 'meta_input');
	# add_filter('wp_insert_post_data', 'wp_insert_post_data_formula', 10, 2 );
	add_filter('wp_insert_post_data', function($data, $arr) use( $postarr ) {
		if( isset($postarr['violation_assessment']) ) {
			$data['violation_assessment'] = $postarr['violation_assessment'];
		}
		return $data;
	}, 10, 2 );
	if( !empty( $post_id ) ){
		$post = get_post( $post_id );
		if( !$post ){
			$message = __( 'Không tìm thấy mục tiêu KPI. Vui lòng thử lại' ,TPL_DOMAIN_LANG);
			send_response_json(['error' => $message],404, $message);
		}
		if( array_key_exists('post_parent', $data) ) {
			unset($data['post_parent']);
		}
		wp_update_post($data);
		if( !empty( $wpdb->last_error ) ) {
			$message = __( 'Chỉnh sửa mục tiêu KPI thất bại. Vui lòng thử lại', TPL_DOMAIN_LANG );
			send_response_json( [ 'error' => $message ], 401, $message );
		}
	}else {
		unset($data['ID']);
		wp_insert_post( $data );
		if( !empty( $wpdb->last_error ) ) {
			$message = __( 'Tạo mục tiêu KPI thất bại. Vui lòng thử lại', TPL_DOMAIN_LANG );
			send_response_json( [ 'error' => $message ], 401, $message );
		}
	}

	# remove_filter('wp_insert_post_data', 'wp_insert_post_data_formula');
	$message = __('Thành công', TPL_DOMAIN_LANG);
	send_response_json(['success' => $message], 200, $message);
}