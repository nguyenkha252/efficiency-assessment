<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/15/18
 * Time: 13:18
 */

function wp_ajax_post_ceo_change_approved_room_kpi( $params ){
    $params = esc_sql( $params );
    $user = wp_get_current_user();
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    if( !wp_verify_nonce($params['_wpnonce'], 'ceo_change_approved_room_kpi') ){
        $message = __('Phiên bản đã hết hạn. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],408, $message);
    }
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableKpi = "{$prefix}kpis";
    $default = ['aproved' => 0];
    $location = '';

    if( !empty( $params ) && !empty( $params['_wp_http_referer'] ) ){
        $query_params = wp_get_referer();
        $location = site_url($query_params);

    }
    $wpdb->query("START TRANSACTION;");
    if( !empty( $params ) && !empty( $params['apply_kpi'] ) ){
        $postarr = $params['apply_kpi'];
        //$location = $params

        foreach ( $postarr as $key => $item ){
            $data = wp_parse_args($item, $default);
            extract($data);
            $data = compact('aproved');
            /*if( $data['aproved'] === 'on' )
                $data['aproved'] = 1;*/
            $id = $wpdb->update( $tableKpi, $data, ['id' => $item['kpi_id']] );
            if( !$id ){
                if( $wpdb->last_error ) {
                    $has_error = true;
                    $msg = $wpdb->last_error;
                }
            }
            #echo json_encode([$id, $data, $item, $wpdb->last_query]);
        }
        if( $has_error ){
            $wpdb->query('rollback;');
            //$msg = __('Có lỗi xảy ra vui lòng thử lại.', TPL_DOMAIN_LANG);
            $http_code = 400;
        }else{
            $wpdb->query('COMMIT;');
            $msg = __('Thành công', TPL_DOMAIN_LANG);
            $http_code = 201;
        }
    }else{
        $msg = __('Có lỗi xảy ra vui lòng thử lại.', TPL_DOMAIN_LANG);
        $http_code = 400;
    }
    send_response_json( ['code' => $http_code, 'message' => $msg, 'location' => $location], $http_code, $msg );
}