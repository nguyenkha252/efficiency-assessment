<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 2/12/18
 * Time: 11:50
 */

require_once THEME_DIR . '/inc/lib-kpis.php';
function wp_ajax_post_update_result_of_user( $params ){
    global $wpdb;
    $user = wp_get_current_user();
    $orgchart = user_load_orgchart($user);
    $params = wp_slash( $params );
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    if( !wp_verify_nonce($params['_wpnonce'], 'update_result_of_user') ){
        $message = __('Phiên bản đã hết hạn. Vui lòng tải lại trang.' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],408, $message);
    }
    $capacityOfUser = [];
	$prefix = $wpdb->get_blog_prefix(get_current_blog_id());
	$tableKpiCP = "{$prefix}kpis_capacity";
    if( array_key_exists( 'capacity', $params ) && !empty( $params['capacity'] ) ){
        $capacitys = $params['capacity'];
        foreach ( $capacitys as $key => $item ){
            $id = $item['ID'];
            $note = nl2br( htmlspecialchars( $item['note'] ) );
            $actual_gain = $item['actual_gain'];
            $data = compact("note", "actual_gain");
            $result = capacity_get_kpi_by_id( $id );
            if( !empty( $result )){
            	if( empty( $capacityOfUser ) ){
		            $capacityOfUser = $result;
	            }
                $wpdb->update( $tableKpiCP, $data, ['id' => $id] );
                if( !empty( $wpdb->last_error ) ){
                    $msg = "Có lỗi xảy ra khi lưu";
                    $httpCode = 400;
                    send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
                }
            }else{
                $msg = "Không tìm thấy năng lực KPI";
                $httpCode = 404;
                send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
            }
        }
    }
	if( array_key_exists( 'capacityfb', $params ) && !empty( $params['capacityfb'] ) ){
    	$capacityFB = $params['capacityfb'];
		if( empty( $capacityFB['ID'] ) ){
			#insert nhan xet chung
			$note = nl2br( htmlspecialchars( $capacityFB['note'] ) );
			$type_note = CAPACITY_TYPE_NOTE_NXC;
			$user_id = $capacityOfUser['user_id'];
			$chart_id = $capacityOfUser['chart_id'];
			$apply_of = $capacityOfUser['apply_of'];
			$year_id = $capacityOfUser['year_id'];
			$modified = date('Y-m-d H:i:s', time() );
			$status = KPI_STATUS_RESULT;
			$type = '';
			$trend = $capacityFB['trend'];
			$dataFB = compact('note', 'type_note', 'user_id', 'chart_id', 'apply_of', 'year_id', 'modified', 'status', 'type', 'trend');
			$wpdb->insert( $tableKpiCP, $dataFB );
			if( !empty( $wpdb->last_error ) ) {
				$msg      = "Có lỗi xảy ra khi lưu";
				$httpCode = 401;
				send_response_json( [ 'code' => $httpCode, 'message' => $msg ], $httpCode, $msg );
			}
		}else{
			#update nhan xet chung
			$resultFB = capacity_get_kpi_by_id( $capacityFB['ID'] );
			if( !empty( $resultFB )){
				$note = nl2br( htmlspecialchars( $capacityFB['note'] ) );
				$trend = $capacityFB['trend'];
				$dataFB = compact('note', 'trend');
				$wpdb->update($tableKpiCP, $dataFB, ['id' => $capacityFB['ID']]);
				if( !empty( $wpdb->last_error ) ) {
					$msg      = "Có lỗi xảy ra khi lưu";
					$httpCode = 402;
					send_response_json( [ 'code' => $httpCode, 'message' => $msg ], $httpCode, $msg );
				}
			}else{
				$msg = "Không tìm thấy nhận xét chung của KPI";
				$httpCode = 404;
				send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
			}
		}
	}
    $msg = "Thành công";
    $httpCode = 201;
    send_response_json(['code' => $httpCode, 'message' => $msg], $httpCode, $msg);
}