<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 11/16/18
 * Time: 4:10 PM
 */

#require_once THEME_DIR . '/inc/vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';
#require_once THEME_DIR . '/inc/vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once THEME_DIR . '/inc/effa.php';
require_once THEME_DIR . '/inc/lib-settings.php';
require_once THEME_DIR . '/inc/vendor/autoload.php';
function wp_ajax_get_export_effa_report($params){
    global $wpdb;
    $year = date('Y', time());
    $user = wp_get_current_user();
    $params = wp_slash($params);
    $userID = (int)$params['uid'];
    if( !$user ){
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],401, $message);
    }
    if( !user_is_manager() ){
        $message = __('Bạn không có quyền thực hiện thao tác này' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],403, $message);
    }
    if( !wp_verify_nonce($params['_wpnonce'], 'export_effa_report') ){
        $message = __('Phiên bản đã hết hạn. Vui lòng thử lại.' ,TPL_DOMAIN_LANG );
        send_response_json(['error' => $message],408, $message);
    }

    $fileTemplate = THEME_DIR . '/assets/template/exports/' . COMPETENCY_EXPORT;
    $upload = wp_upload_dir();
    $date = date('d-m-Y-H-i-s', current_time('timestamp') );
    $file_export = explode('.', COMPETENCY_EXPORT);
    $extra = $file_export[count($file_export) - 1];
    unset($file_export[count($file_export) - 1]);
    array_push($file_export, $date, uniqid(), $extra);
    $file_export = implode(".", $file_export);
    $fileCopy = "{$upload['basedir']}/exports/" . $file_export;
    $file_download = str_replace($upload['basedir'], $upload['baseurl'], $fileCopy);
    if (!is_dir("{$upload['basedir']}/exports")) {
        mkdir("{$upload['basedir']}/exports", 0777, true);
    }
    @copy($fileTemplate, $fileCopy);
    @chmod($fileCopy, 0777);
    $identify = PHPExcel_IOFactory::identify($fileCopy);
    $objReader = PHPExcel_IOFactory::createReader($identify);
    $objPHPExcel = $objReader->load($fileTemplate);
    $activeSheet = $objPHPExcel->getActiveSheet();
    $highestColumn = $activeSheet->getHighestColumn();
    $highestColumn++;
    $start_row_competency = 10;
    $start_row_competency_best = 18;
    $start_row_competency_bad = 23;
    $row_added = 0;

    $getUser = effa_site_get_user_by_id($userID);
    $thisPosition = effa_site_user_load_orgchart($getUser);
    $orgchartID = $thisPosition->id;
    $postEffa = effa_site_efficiency_get_by_orgchart_user($orgchartID, $userID, $year);
    $efficiency = get_user_meta($userID, 'efficiency_' . $year, true);
    $settings = get_settings_website();
    $standard_benchmark = $settings['standard_benchmark'] ? $settings['standard_benchmark'] : 0;
    if (!empty($postEffa)) {
        $arrTarget = array_group_by($postEffa, 'post_parent');
        if (!empty($arrTarget)) {
            $arrGroup = $arrTarget[0];
            unset($arrTarget[0]);
        }
    }
    $logo_webiste = !empty( $settings['logo_url'] ) ? $settings['logo_url'] : THEME_URL . '/assets/images/logo-brainmark.png';
    $title_website = !empty($settings['title_website']) ? $settings['title_website'] : __('KPI SYSTEM', TPL_DOMAIN_LANG);
    $path_logo = str_replace($upload['baseurl'], $upload['basedir'], $logo_webiste);
    if( empty($settings['logo_url']) ){
        $path_logo = str_replace(THEME_URL, THEME_DIR, $logo_webiste);
    }
    $checkPathLogo = wp_remote_get($logo_webiste);
    if($checkPathLogo['response']['code'] != 404) {
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
        $objDrawing->setPath($path_logo);
        $objDrawing->setHeight(56);
        $objDrawing->setCoordinates('A1');
    }
    $activeSheet
        ->setCellValue("A1", '')
        ->setCellValue("D2", $title_website )
        ->setCellValue("C5", $getUser->display_name)
        ->setCellValue("G5", $thisPosition->name)
        ->setCellValue("C6", $getUser->user_nicename)
        ->setCellValue("G6", $thisPosition->room)
    ;
    if( !empty( $arrGroup ) ){
        $idxG = 1;
        $totalPercent = 0;
        $totalResultPercent = 0;
        foreach ( $arrGroup as $key => $item_group ):
            $this_row_group = $start_row_competency + $row_added;
            $groupID = $item_group->pID;
            $nameGroup = get_the_title($groupID);
            $sttRoman = convert_number_2_roman($idxG);
            $groupCode = isset( $item_group->post_code ) && !empty( $item_group->post_code ) ? $item_group->post_code : "";
            $percentGroup = !empty($item_group->percent) ? $item_group->percent : 0;
            $totalPercent += $percentGroup;
            $sumBenchmark = 0;
            $group_score = 0;
            $activeSheet->insertNewRowBefore($this_row_group, 1);
            $activeSheet->setCellValue("A{$this_row_group}", $sttRoman)
                ->setCellValue("B{$this_row_group}", $nameGroup)
                ->setCellValue("E{$this_row_group}", $percentGroup . '%')
            ;
            $row_added++;
            if( array_key_exists( $groupID, $arrTarget ) ){
                $targetItem = $arrTarget[$groupID];
                $idxTg = 0;

                foreach ( $targetItem as $k => $item ){
                    $idxTg++;
                    $targetID = $item->pID;
                    $important = $item->important;
                    $standard = $item->standard;
                    $manager_lv1 = $item->manager_lv1 != NULL ? $item->manager_lv1 : 0; # manager
                    $manager_lv0 = $item->manager_lv0 != NULL ? $item->manager_lv0 : 0; # admin
                    $this_row_item =  $start_row_competency + $row_added;
                    $titleTarget = str_replace(str_split("\|"), "", get_the_title($targetID));
                    $targetMeta = !empty( $item->post_code ) ? $item->post_code : "";
                    $benchmark = $important * $standard;
                    $sumBenchmark += $benchmark;
                    $benchmark = $benchmark > 0 ? $benchmark : '';
                    $score = $important * $manager_lv0;
                    $group_score += $score;
                    $result_item = $benchmark == 0 ? 0 : round($score/$benchmark * 100, 0);
                    $testmark = isset($efficiency[$targetMeta] ) ? $efficiency[$targetMeta] : 0;
                    $activeSheet->insertNewRowBefore($this_row_item, 1);
                    $activeSheet->setCellValue("A{$this_row_item}", $idxTg)
                        ->setCellValue("B{$this_row_item}", $titleTarget)
                        ->setCellValue("E{$this_row_item}", '')
                        ->setCellValue("F{$this_row_item}", $important)
                        ->setCellValue("G{$this_row_item}", $standard)
                        ->setCellValue("H{$this_row_item}", $benchmark)
                        ->setCellValue("I{$this_row_item}", $testmark)
                        ->setCellValue("J{$this_row_item}", $manager_lv1)
                        ->setCellValue("K{$this_row_item}", $manager_lv0)
                        ->setCellValue("L{$this_row_item}", $score)
                        ->setCellValue("M{$this_row_item}", (int)$result_item . '%');
                    ;
                    #copy style từ file excel mẫu
                    for ($col = "A"; $col != $highestColumn; $col++){
                        $activeSheet->duplicateStyle($activeSheet->getStyle("{$col}" . ($start_row_competency - 1)), "{$col}{$this_row_item}:{$col}{$this_row_item}");
                    }
                    $row_added++;
                }
                $result_group = $sumBenchmark == 0 ? 0 : round($group_score/$sumBenchmark * $percentGroup, 0);
                $totalResultPercent += $result_group;
                unset( $arrTarget[$groupID] );
                $activeSheet->setCellValue("H{$this_row_group}", $sumBenchmark)
                    ->setCellValue("L{$this_row_group}", $group_score)
                    ->setCellValue("M{$this_row_group}", (int)$result_group . '%')
                ;
                #copy style từ file excel mẫu
                for ($col = "A"; $col != $highestColumn; $col++){
                    $activeSheet->duplicateStyle($activeSheet->getStyle("{$col}" . ($start_row_competency - 2)), "{$col}{$this_row_group}:{$col}{$this_row_group}");
                }
            }
            $idxG++;
        endforeach;
        /**
         * Ghi giá trị vào dòng tổng cộng
        */
        $activeSheet->setCellValue("E" . ($start_row_competency + $row_added), $totalPercent . '%')
            ->setCellValue("M" . ($start_row_competency + $row_added), $totalResultPercent . '%')
        ;
        if( $totalResultPercent >= $standard_benchmark ){
            $text_result = __('Đạt  ', TPL_DOMAIN_LANG);
            $description_result = isset($settings['description_reach']) ? "(" . $settings['description_reach'] . ")" : "";
        }else{
            $text_result = __('Không đạt  ', TPL_DOMAIN_LANG);
            $description_result = isset($settings['description_not_reach']) ? "(" . $settings['description_not_reach'] . ")" : "";
        }
        /**
         * Ghi giá trị vào dòng mô tả kết quả
         */
        $activeSheet->setCellValue("A" . ($start_row_competency + $row_added + 1), $text_result)
            ->setCellValue("A" . ($start_row_competency + $row_added + 2), $description_result);
        $activeSheet->removeRow($start_row_competency - 2, 2);
        $row_added = $row_added - 2;
    }

    $reports_best = effa_site_reports_filter_efficiency_by_user($year, $standard_benchmark, $userID, ">=");
    $this_row_best_copy = $start_row_competency_best + $row_added - 1;
    $start_row_competency_best_remove = $start_row_competency_best + $row_added;
    /**
     * Năng lực cần phát huy
     */
    if (!empty($reports_best)) {
        $arrTargetBest = array_group_by($reports_best, 'post_parent');
        if (!empty($arrTargetBest)) {
            $arrGroupBest = $arrTargetBest[0];
            unset($arrTargetBest[0]);
        }
    }

    if( !empty($arrGroupBest) ){
        $idxG_BEST = 1;
        foreach ($arrGroupBest as $k_g => $best_item_group){
            $best_group_ID = $best_item_group->ID;
            if( array_key_exists( $best_group_ID, $arrTargetBest ) ) {
                $this_row_best_group = $start_row_competency_best + $row_added;
                $sttRoman = convert_number_2_roman($idxG_BEST);
                $best_group_title = str_replace(str_split("\|"), "", $best_item_group->post_title);
                $activeSheet->insertNewRowBefore($this_row_best_group, 1);
                $activeSheet->setCellValue("A{$this_row_best_group}", $sttRoman)
                    ->setCellValue("B{$this_row_best_group}", $best_group_title);
                #copy style từ file excel mẫu
                for ($col = "A"; $col != $highestColumn; $col++) {
                    $activeSheet->duplicateStyle($activeSheet->getStyle("{$col}" . ($this_row_best_copy - 1)), "{$col}{$this_row_best_group}:{$col}{$this_row_best_group}");
                }
                $row_added++;
                $arrBestItem = $arrTargetBest[$best_group_ID];
                foreach ($arrBestItem as $k => $best_item) {
                    $this_row_best = $start_row_competency_best + $row_added;
                    $best_title = str_replace(str_split("\|"), "", $best_item->post_title);
                    $best_summary = $best_item->summary_post;
                    $activeSheet->insertNewRowBefore($this_row_best, 1);
                    $activeSheet->setCellValue("A{$this_row_best}", ($k + 1))
                        ->setCellValue("B{$this_row_best}", $best_title)
                        ->setCellValue("M{$this_row_best}", (int)$best_summary . "%");
                    for ($col = "A"; $col != $highestColumn; $col++) {
                        $activeSheet->duplicateStyle($activeSheet->getStyle("{$col}{$this_row_best_copy}"), "{$col}{$this_row_best}:{$col}{$this_row_best}");
                    }
                    $row_added++;
                }
            }

            $idxG_BEST++;
        }
        $activeSheet->removeRow($start_row_competency_best_remove - 2, 2);
        $row_added = $row_added - 2;
    }

    /**
     * Năng lực cần cải thiện
     */

    $reports_bad = effa_site_reports_filter_efficiency_by_user($year, $standard_benchmark, $userID, "<");
    $this_row_bad_copy = $start_row_competency_bad + $row_added - 1;
    $start_row_competency_bad_remove = $start_row_competency_bad + $row_added;
    if (!empty($reports_bad)) {
        $arrTargetBad = array_group_by($reports_bad, 'post_parent');
        if (!empty($arrTargetBad)) {
            $arrGroupBad = $arrTargetBad[0];
            unset($arrTargetBad[0]);
        }
    }

    if( !empty($arrGroupBad) ){
        $idxG_BAD = 1;
        foreach ($arrGroupBad as $k_g => $bad_item_group){
            $bad_group_ID = $bad_item_group->ID;
            if( array_key_exists( $bad_group_ID, $arrTargetBad ) ) {
                $this_row_bad_group = $start_row_competency_bad + $row_added;
                $sttRoman = convert_number_2_roman($idxG_BAD);
                $bad_group_title = str_replace(str_split("\|"), "", $bad_item_group->post_title);
                $activeSheet->insertNewRowBefore($this_row_bad_group, 1);
                $activeSheet->setCellValue("A{$this_row_bad_group}", $sttRoman)
                    ->setCellValue("B{$this_row_bad_group}", $bad_group_title);
                #copy style từ file excel mẫu
                for ($col = "A"; $col != $highestColumn; $col++) {
                    $activeSheet->duplicateStyle($activeSheet->getStyle("{$col}" . ($this_row_bad_copy - 1)), "{$col}{$this_row_bad_group}:{$col}{$this_row_bad_group}");
                }
                $row_added++;
                $arrBadItem = $arrTargetBad[$bad_group_ID];
                foreach ($arrBadItem as $k => $bad_item) {
                    $this_row_bad = $start_row_competency_bad + $row_added;
                    $bad_title = str_replace(str_split("\|"), "", $bad_item->post_title);
                    $bad_summary = $bad_item->summary_post;
                    $activeSheet->insertNewRowBefore($this_row_bad, 1);
                    $activeSheet->setCellValue("A{$this_row_bad}", ($k + 1))
                        ->setCellValue("B{$this_row_bad}", $bad_title)
                        ->setCellValue("M{$this_row_bad}", (int)$bad_summary . "%");
                    #copy style từ file excel mẫu
                    for ($col = "A"; $col != $highestColumn; $col++) {
                        $activeSheet->duplicateStyle($activeSheet->getStyle("{$col}{$this_row_bad_copy}"), "{$col}{$this_row_bad}:{$col}{$this_row_bad}");
                    }
                    $row_added++;
                }
            }

            $idxG_BAD++;
        }
        $activeSheet->removeRow($start_row_competency_bad_remove - 2, 2);
    }
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $identify);
    $objWriter->save($fileCopy);
    send_mail_report($getUser, $file_download);
    send_response_json( ['url_download' => $file_download], 201, __('OK', TPL_DOMAIN_LANG ));

}