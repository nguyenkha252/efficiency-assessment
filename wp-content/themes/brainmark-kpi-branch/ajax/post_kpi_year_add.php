<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/9/18
 * Time: 18:44
 */

function wp_ajax_post_kpi_year_add($params) {
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableKpiYear = "{$prefix}kpi_years";

    $results = [];
    $user = wp_get_current_user();
    $nonce = isset($params['_wpnonce']) ? $params['_wpnonce'] : '';
    if( !wp_verify_nonce($nonce, 'save_kpi_year') ) {
        $msg = "Sai mã bảo vệ";
        send_response_json(['code' => 403, 'message' => $msg], 403, $msg);
    }
    $current_id = isset($params['year_id']) ? intval($params['year_id']) : 0;
    $defaults = [
        'year' => 0, 'precious' => null, 'month' => null, 'kpi_time' => null, 'kpi_type' => 'congty',
        'finance' => 0, 'customer' => 0, 'operate' => 0, 'development' => 0,
        'parent' => 0, 'status' => 'publish', 'chart_id' => $user->orgchart_id,
        'created' => date('Y-m-d H:i:s', time())];
    $data = [];
    /*
    foreach($defaults as $key => $val) {
        if( isset($params[$key]) ) {
            $data[$key] = $params[$key];
        } else {
            $data[$key] = $defaults[$key];
        }
    } */
    $data = wp_parse_args($params, $defaults);

    foreach($data as $k => $val) {
        if( !isset($defaults[$k]) ) {
            unset($data[$k]);
        }
    }
    if( empty($params['is_update']) ) {
        $params['is_update'] = false;
    }
    if( $params['is_update'] && !empty($params['id']) ) {
        unset($data['created']);
    }
    $user = wp_get_current_user();

    $data['kpi_time'] = isset($params['kpi_time']) ? strval($params['kpi_time']) : 'quy';
    $data['kpi_type'] = isset($params['kpi_type']) ? strval($params['kpi_type']) : 'congty';
    $data['parent'] = isset($params['parent']) ? intval($params['parent']) : 0;
    $data['chart_id'] = isset($params['chart_id']) ? intval($params['chart_id']) : $user->orgchart_id;
    $data['year'] = isset($params['nam']) ? intval($params['nam']) : 0;
    $data['precious'] = isset($params['quy']) ? intval($params['quy']) : 0;
    $data['month'] = isset($params['thang']) ? intval($params['thang']) : 0;
    if( !empty($data['precious']) ) {
        $data['month'] = 0;
    }
    $cur = intval(date('Y', time())) - 1;

    if( $data['year'] < $cur ) {
        $msg = "Năm của KPI không được nhỏ hơn năm hiện tại 1 năm";
        send_response_json(['code' => 400, 'message' => $msg], 400, $msg);
    }

    $can_add_or_update = true;
    if( array_key_exists('year_id', $data) ) {
        unset($data['year_id']);
    }

    if( !empty($params['is_update']) ) {
        $can_add_or_update = true;
    }

    $logSuccess = [];
    $logError = [];
    $res = 0;

    $year_id = $current_id;
    if( $can_add_or_update ) {
        if( empty($current_id) ) {
            if( $data['kpi_time'] == 'nam' ) {
                $year_id = $wpdb->get_var( $wpdb->prepare("SELECT y.`id` FROM {$tableKpiYear} as y WHERE (y.`year` = %d) AND (y.`parent` = %d) AND (y.`chart_id` = %d)", $data['year'], $params['parent'], $data['chart_id']) );
            } else if( $data['kpi_time'] == 'quy' ) {
                $year_id = $wpdb->get_var( $wpdb->prepare("SELECT y.`id` FROM {$tableKpiYear} as y WHERE (y.`year` = %d) AND (y.`precious` = %d) AND (y.`parent` = %d) AND (y.`chart_id` = %d)", $data['year'], $data['precious'], $params['parent'], $data['chart_id']) );
            } else if( $data['kpi_time'] == 'thang' ) {
                $year_id = $wpdb->get_var( $wpdb->prepare("SELECT y.`id` FROM {$tableKpiYear} as y WHERE (y.`year` = %d) AND (y.`month` = %d) AND (y.`parent` = %d) AND (y.`chart_id` = %d)", $data['year'], $data['month'], $params['parent'], $data['chart_id']) );
            }
        }

        if( !empty($year_id) ) {
            $res = $wpdb->update($tableKpiYear, $data, ['id' => $year_id]);
            $data['id'] = $year_id;
            if( $wpdb->last_error ) {
                $logError[] = [__LINE__, $res, $wpdb->last_error];
            }
            $logSuccess[] = [__LINE__, $res, $wpdb->last_query];
        } else if( empty( $params['is_update'] ) ) {
            $res = $wpdb->insert($tableKpiYear, $data);
            if( $wpdb->last_error ) {
                $logError[] = [__LINE__, $res, $wpdb->last_error];
            }
            if( is_numeric($res) && $res > 0 ) {
                $year_id = $wpdb->insert_id;
                $data['id'] = $year_id;
            }

            # @TODO: Update fix code if is Chart Root and add Personal KPI, else ignore Personal KPI
            $chartRoot = kpi_get_orgchart_root();
            if( !empty($_GET['cid']) && !empty($chartRoot) && ($chartRoot['id'] == $_GET['cid']) ) {
                $dataCN = ['year' => $data['year'], 'kpi_time' => 'nam', 'kpi_type' => 'canhan', 'parent' => 0, 'chart_id' => $data['chart_id'], 'created' =>  $defaults['created'], 'status' => 'publish' ];
                $kpiCaNhan = $wpdb->insert( $tableKpiYear, $dataCN );
                if( is_numeric($kpiCaNhan) && ($kpiCaNhan > 0) && $wpdb->last_error ) {
                    $logError[] = [__LINE__, $res, $wpdb->last_error];
                }
	            $logSuccess[] = [__LINE__, $res, $wpdb->insert_id, $wpdb->last_query];
                # add year behavior
                $dataBehavior = ['year' => $data['year'], 'kpi_time' => $data['kpi_time'], 'kpi_type' => 'behavior', 'parent' => 0, 'chart_id' => $data['chart_id'], 'created' =>  $defaults['created'], 'status' => 'publish' ];
	            $behavior = $wpdb->insert( $tableKpiYear, $dataBehavior );
	            if( is_numeric($behavior) && ($behavior > 0) && $wpdb->last_error ) {
		            $logError[] = [__LINE__, $res, $wpdb->last_error];
	            }else{
		            $behaviorParent = $wpdb->insert_id;
		            $dataBehaviorItem = ['year' => $data['year'], 'parent' => $behaviorParent, 'kpi_time' => $data['kpi_time'], 'kpi_type' => 'behavior', 'chart_id' => $data['chart_id'], 'created' =>  $defaults['created'], 'status' => 'publish' ];
		            if( $data['kpi_time'] == 'quy' ){
		            	for( $i=1; $i<=4; $i++ ){
		            		$dataBehaviorItem['precious'] = $i;
				            $behavior = $wpdb->insert( $tableKpiYear, $dataBehaviorItem );
				            if( is_numeric($behavior) && ($behavior > 0) && $wpdb->last_error ) {
					            $logError[] = [__LINE__, $res, $wpdb->last_error];
				            }
			            }
		            }elseif( $data['kpi_time'] == 'thang' ) {
			            for( $i=1; $i<=12; $i++ ){
				            $dataBehaviorItem['precious'] = ceil($i / 3);
				            $dataBehaviorItem['month'] = $i;
				            $behavior = $wpdb->insert( $tableKpiYear, $dataBehaviorItem );
				            if( is_numeric($behavior) && ($behavior > 0) && $wpdb->last_error ) {
					            $logError[] = [__LINE__, $res, $wpdb->last_error];
				            }
			            }
		            }

	            }
                $logSuccess[] = [__LINE__, $res, $wpdb->insert_id, $wpdb->last_query];
            }
        }
    }


    if( $can_add_or_update && $res === false ) {
        $msg = "Có lỗi xảy ra khi đang lưu KPI của năm {$data['year']}";
        send_response_json(['code' => 400, 'message' => $msg, 'error' => $logError, 'success' => $logSuccess], 400, $msg);
    } else {
    	$year = 0;
    	if( $year_id ){
    		$resultYear = kpi_get_year_by_id( $year_id );
    		$year = $resultYear['year'];
	    }
        $msg = empty($logError) ? "Thêm/Cập nhật thành công" : "Có lỗi xảy ra";
        $code = empty($logError) ? 201 : 400;
        send_response_json(['code' => $code, 'message' => $msg, 'year' => $year_id, 'year_value' => $year, 'error' => $logError, 'success' => $logSuccess], $code, $msg);
    }
}