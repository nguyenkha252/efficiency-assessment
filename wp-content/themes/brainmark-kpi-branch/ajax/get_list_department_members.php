<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/10/18
 * Time: 20:18
 */

function wp_ajax_get_list_department_members($params) {

    require_once THEME_DIR . '/inc/lib-users.php';
    $user = wp_get_current_user();
    $nonce = isset($params['_wpnonce']) ? $params['_wpnonce'] : '';
    if( !wp_verify_nonce($nonce, 'list_department_members') ) {
        $msg = "Sai mã bảo vệ";
        send_response_json(['code' => 403, 'message' => $msg], 403, $msg);
    }

    $results = kpi_get_department_users($params['orgchart_id'], $params['kpi_id'], $params['bank_id'], $params['year_id']);
    send_response_json($results, 200, 'OK');
    return;
}