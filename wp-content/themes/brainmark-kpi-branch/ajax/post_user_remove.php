<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/12/17
 * Time: 00:21
 */

require_once THEME_DIR . '/inc/lib-users.php';

function wp_ajax_post_user_remove( $params ) {
    $user = wp_get_current_user();
    $params['_wpnonce'] = isset($params['_wpnonce']) ? $params['_wpnonce'] : '';
    if( !wp_verify_nonce($params['_wpnonce'], 'remove_user') ) {
        $msg = __("Nonce invalid", TPL_DOMAIN_LANG);
        send_response_json(['code' => 401, 'message' => $msg], 401, $msg);
    }

    if( empty($user->allcaps['edit_users']) && !$user->has_cap('edit_users') ) {
        $msg = __("Not allow user permission", TPL_DOMAIN_LANG);
        send_response_json(['code' => 401, 'message' => $msg], 401, $msg);
    }
    # wp_delete_user($params['uid']);
    global $wpdb;
    $res = $wpdb->update( $wpdb->users, ['deleted' => 1], ['ID' => isset($params['uid']) ? $params['uid'] : 0] );
    $status = ($res !== false) ? 202 : 400;
    $msg = ($res !== false) ? 'Xóa thành công' : "Có lỗi xảy ra";
    send_response_json(['code' => $status, 'message' => $msg, 'query' => $wpdb->last_query, 'error' => $wpdb->last_error], $status, $msg);
}