<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 12/27/17
 * Time: 00:36
 */


function kpi_get_orgchart_info( $params ) {
    # $user = wp_get_current_user();
    global $wpdb;

    if( empty($params['id']) ) {
        $params['id'] = 0;
    }
    # $table_prefix = $wpdb->get_blog_prefix( $wpdb->blogid );

    $table_org_chart = "{$wpdb->base_prefix}org_charts";
    if( !empty($params['single']) ) {
        $row = $wpdb->get_row(
            $wpdb->prepare( "SELECT o.* ".
            "FROM `{$table_org_chart}` as o \n".
            "WHERE (1 = 1) AND (id = %d) ".
            "ORDER BY `parent` ASC LIMIT 0, 1", $params['id']), ARRAY_A);

        if( empty($row) ) {
            $jsonData = new WP_Error(404, __('Not found', TPL_DOMAIN_LANG) );
        } else {
            $jsonData = [
                'id' => $row['id'],
                'name' => $row['name'],
                'parent' => intval($row['parent']),
                'role' => __($row['role'], TPL_DOMAIN_LANG),
                'description' => $row['description'],
                'avatar' => !empty($row['icon']) ? $row['icon'] : THEME_URL . '/assets/images/no-photo.jpg',
                'cls' => '',
                'childCls' => 'clearfix float'
            ];
        }
        return $jsonData;
    } else {
        $rows = $wpdb->get_results(
            "SELECT o.* ".
            "FROM `{$table_org_chart}` as o \n".
            "WHERE 1 = 1 ".
            "ORDER BY `parent` ASC LIMIT 0, 1000", ARRAY_A);

        if( empty($rows) ) {
            $jsonData = ['total' => 0, 'items' => []];
        } else {
            $jsonData = ['total' => count($rows), 'items' => []];
            foreach ($rows as $row) {
                $jsonData['items'][] = [
                    'id' => $row['id'],
                    'name' => $row['name'],
                    'parent' => intval($row['parent']),
                    'role' => __($row['role'], TPL_DOMAIN_LANG),
                    'description' => $row['description'],
                    'avatar' => !empty($row['icon']) ? $row['icon'] : THEME_URL . '/assets/images/no-photo.jpg',
                    'cls' => '',
                    'childCls' => 'clearfix float'
                ];
            }
        }
        return $jsonData;
    }
}

function wp_ajax_get_orgchart_info( $params ) {
    $user = wp_get_current_user();
    if( !wp_verify_nonce($params['_wpnonce'], 'user_info') ) {
        $jsonData = new WP_Error(401, __("Nonce invalid", TPL_DOMAIN_LANG));
    } else if( empty($user->allcaps['list_users']) && !$user->has_cap('list_users') ) {
        $jsonData = new WP_Error(403, __("Not allow user permission", TPL_DOMAIN_LANG));
    } else {
        $jsonData = kpi_get_orgchart_info($params);
    }
    if( is_wp_error($jsonData) ) {
        send_response_json($jsonData, $jsonData->get_error_code(), $jsonData->get_error_message());
    } else {
        send_response_json($jsonData, 200);
    }
}