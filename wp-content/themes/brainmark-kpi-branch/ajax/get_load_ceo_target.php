<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/16/18
 * Time: 09:29
 */
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-users.php';
function wp_ajax_get_load_ceo_target( $params )
{
    global $wpdb;

    $params = wp_unslash($params);
    $kpiID = isset($params['kpi_id']) ? (int)$params['kpi_id'] : 0;
    $wpnonce = isset($params['_wpnonce']) ? $params['_wpnonce'] : '';
    $kpi_type = isset($params['kpi_type']) ? $params['kpi_type'] : '';
    $year_value = isset($params['year']) ? $params['year'] : '';

    if (!wp_verify_nonce($wpnonce, 'load-ceo-target')) {
        $message = __('Mã bảo vệ không hợp lệ', TPL_DOMAIN_LANG);
        $httpCode = 403;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }

    $user = wp_get_current_user();
    if (!$user) {
        $message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG);
        $httpCode = 401;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }
    $checkCEO = kpi_is_user_ceo();
    if (!$checkCEO) {
        $message = __('Bạn không có quyền đọc mục tiêu này', TPL_DOMAIN_LANG);
        $httpCode = 403;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }

    $result = kpi_get_kpi_by_id($kpiID);

    $charts = kpi_get_list_org_charts($user->orgchart_id, false);
    $sql = $wpdb->last_query;
    $chart_ids = [];
    foreach ($charts as $chart_id => $chart) {
        if ($chart['is_leaf']) {
            unset($charts[$chart_id]);
            continue;
        }
        $chart_ids[] = $chart_id;
    }

    if( !empty( $result ) && !is_wp_error( $result ) ) {
        $terms = wp_get_post_terms( $result['bank_id'], 'category', ['fields' => 'tt_ids'] );
        if( !empty( $terms ) && !is_wp_error( $terms ) ){
            if( count( $terms ) > 0 ){
                $terms = $terms[0];
            }
            $result['bank_cat'] = $terms;
        }
        # $year = kpi_get_year_by_id( $result['year_id'] );
        # $departments = get_kpi_by_parent( $kpiID );
        # $subcharts = kpi_get_list_of_charts($kpi_type, 'YEAR', $year_value, 0, $user->orgchart_id, $result['bank_id']);

        # $subcharts = kpi_get_list_of_charts($kpi_type, '', $result['year_id'], 0, $user->orgchart_id, $result['bank_id']);
        $subcharts = kpi_get_list_of_charts($kpi_type, '', '', 0, $chart_ids, 0, $result['id']);
        # $subcharts = [];
        # $wpdb->query("SELECT * FROM ");

        $result['charts'] = $charts;
        $result['last_query1'] = $sql;
        $result['last_query2'] = $wpdb->last_query;
        $result['last_error'] = $wpdb->last_error;

        $result['time_value'] = $year_value;
        $result['chuc_danh'] = [];
        foreach($subcharts as $sub) {
            $result['chuc_danh'][$sub['chart_id']] = $sub;
        }
        $bankInfo = get_post($result['bank_id']);
        $result['receive'] = substr( kpi_format_date( $result['receive'] ), 0, 10);
        $result['bankInfo'] = $bankInfo ? ['ID' => $bankInfo->ID, 'parent' => $bankInfo->post_parent, 'title' => $bankInfo->post_title] : null;
        $httpCode = 202;
        $message = 'OK';
    }else{
        $message = __('Không tìm thấy', TPL_DOMAIN_LANG);
        $httpCode = 404;
    }
    if( $httpCode >= 300 ) {
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }else{
        send_response_json(['code' => $httpCode, 'data' => $result], $httpCode, $message);
    }
}