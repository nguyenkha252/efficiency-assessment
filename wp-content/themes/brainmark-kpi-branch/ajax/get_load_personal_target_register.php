<?php
/**
 * Created by PhpStorm.
 * User: richardnguyen
 * Date: 1/25/18
 * Time: 14:13
 */

function wp_ajax_get_load_personal_target_register( $params ){
    require_once THEME_DIR . '/inc/lib-years.php';
    require_once THEME_DIR . '/inc/lib-kpis.php';
    require_once THEME_DIR . '/inc/lib-users.php';
    $params = wp_unslash($params);
    $kpiID = (int) $params['id'];
    $user = wp_get_current_user();
    $orchart = user_load_orgchart( $user );
    $result = kpi_get_kpi_and_year_by_kpi_id($kpiID);
    if( empty( $result ) ){
        $message = __('Không tìm thấy KPI', TPL_DOMAIN_LANG );
        $httpCode = 403;
        send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
    }
    $post = get_post( $result['bank_id'] );
    #$terms = wp_get_post_terms( $result['bank_id'], 'category' );
    $cat = 0;
    if( !empty( $post ) ){
    	$cat = $post->chart_id;
    }
    $data['cat'] = $cat;
    $data['bank'] = $result['bank_id'];
    $data['plan'] = $result['plan'];
    $data['unit'] = !empty($result['unit']) ? getUnit($result['unit']) : '' ;
    $data['receive'] = substr( kpi_format_date( $result['receive'] ), 0, 10 );
    $data['percent'] = $result['percent'];
    $data['year_id'] = $result['year_id'];
    $data['kpi_time'] = $result['kpi_time'];
    $data['id'] = $result['id'];
    $message = __('Thành công', TPL_DOMAIN_LANG );
    $httpCode = 201;
    send_response_json( ['code' => $httpCode, 'data' => $data], $httpCode, $message );
}