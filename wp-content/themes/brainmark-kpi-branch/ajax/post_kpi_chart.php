<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/22/18
 * Time: 11:09
 */

require_once THEME_DIR . '/inc/lib-orgchart.php';

function wp_ajax_post_kpi_chart($params) {
    global $wpdb;
    $prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
    $tableCharts = "{$prefix}org_charts";
    $nonce = isset($params['nonce']) ? $params['nonce'] : '';
    if( !wp_verify_nonce($nonce, 'kpi_chart') ) {
        $msg = 'Mã bảo vệ không hợp lệ';
        $status = 403;
        send_response_json(['code' => $status, 'message' => $msg], $status, $msg);
    }
    $user = wp_get_current_user();
    $is_admin = kpi_is_user_as_admin($user);
    if( !$is_admin ) {
        $msg = 'Bạn không có quyền thêm chức danh hoặc cập nhật cơ cấu tổ chức của công ty';
        $status = 403;
        send_response_json(['code' => $status, 'message' => $msg], $status, $msg);
    }

    if( isset($params['node']) ) {
        $updateData = [
            'level' => $params['node']['level'],
            'parent' => $params['node']['parent'],
            'name' => $params['node']['name'],
            'alias' => $params['node']['alias'],
            'room' => $params['node']['room'],
            'has_kpi' => (int)$params['node']['has_kpi'] ? 1 : 0,
        ];
        $node_id = isset($params['node']['id']) ? $params['node']['id'] : 0;
        #kiểm tra chức danh có tồn tại chưa
        $getOrgchart = get_orgchart_by_name_lv_alias_room($updateData['name'], $updateData['alias'], $updateData['parent'], $updateData['room'], $node_id);
        if( !empty($getOrgchart) ){
            # nếu tồn tại thì thông báo ra màn hình
            $msg = 'Chức danh đã tồn tại. Vui lòng tạo chức danh khác';
            $status = 401;
            send_response_json(['code' => $status, 'message' => $msg], $status, $msg);
        }
        if( ($params['type'] == 'add') || empty($params['node']['id']) ) {
            $res = $wpdb->insert($tableCharts, $updateData);
            $msg = "Có lỗi xảy ra khi thêm mới chức danh";
            $success = 'Thêm mới chức danh thành công';
        } else {
            if( $params['node']['id'] != 1 ) {
                $updateData['parent'] = $params['node']['onlevel'];
            }
            $res = $wpdb->update($tableCharts, $updateData, ['id' => $params['node']['id']]);
            $msg = "Có lỗi xảy ra khi chỉnh sửa chức danh";
            $success = 'Chỉnh sửa chức danh thành công';
        }
        if( $res === false ) {
            $status = 400;
            send_response_json(['code' => $status, 'message' => $msg, 'error' => $wpdb->last_error], $status, $msg);
        } else {
            $status = 201;
            $charts = kpi_load_org_charts();
            send_response_json(['charts' => $charts, 'message' => $success], $status, $success);
        }
    } else {
        $status = 400;
        $msg = "Tham số gửi về không hợp lệ";
        send_response_json(['code' => $status, 'message' => $msg], $status, $msg);
    }
    /*
    $filename = THEME_DIR . '/contents/charts/version-demo.json';
    # $params['node'];
    file_put_contents($filename, json_encode($params['node'], JSON_PRETTY_PRINT));
    $fileData = file_get_contents($filename);
    if( !empty($fileData) ) {
        $jsonData = json_decode($fileData, true);
        if( !empty($jsonData) ) {
            send_response_json($jsonData, 201, 'OK');
            # $jsonData = sprintf(' data-chart-data="%s"' . esc_json_attr($jsonData));
        }
    }
    send_response_json($jsonData, 400, 'Failed');
    */
    return;
}