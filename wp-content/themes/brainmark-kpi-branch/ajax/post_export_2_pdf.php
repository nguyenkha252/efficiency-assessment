<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/31/18
 * Time: 10:02
 */

function wp_ajax_post_export_2_pdf($params) {
    $status = 200;
    $msg = "OK";
    $pdf_data = '';
    try {
        require_once THEME_DIR . '/inc/phpToPDF.php';
        $pdf_options = array(
            "source_type" => 'html',
            "source" => $params['html'],
            "action" => 'view',
            "save_directory" => '',
            "page_orientation" => "portrait", # 'portrait' or 'landscape'
            "page_size" => "A4", # 'letter', 'legal', 'A0', 'A1', 'A2', 'A3', 'A4', 'A5', 'A6', 'A7', 'A8', 'B0', 'B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B9', 'B10', 'ledger', 'tabloid'
            "encoding" => "UTF-8", # 'UTF-8', 'ISO-8859-1', 'JIS', 'Windows-1251', etc. More Here
            "file_name" => $params['filename']);

        // CALL THE phpToPDF FUNCTION WITH THE OPTIONS SET ABOVE
        $pdf_data = phptopdf($pdf_options, false);
    } catch(Exception $ex) {
        $status = 400;
        $msg = $ex->getMessage();
        $pdf_data = implode("<br>", [$ex->getCode(), $ex->getMessage(), $ex->getTraceAsString()]);
    }
    send_response_json(['code' => $status, 'message' => $msg, 'data' => $pdf_data], $status, $msg);
}
