<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 13/03/2018
 * Time: 00:10
 */
require_once THEME_DIR . '/inc/lib-kpis.php';
require_once THEME_DIR . '/inc/lib-years.php';
require_once THEME_DIR . '/inc/lib-behavior.php';
function wp_ajax_post_rollback_result_behavior( $params ){
	global $wpdb;
	$params = wp_slash($params);
	$user = wp_get_current_user();
	if (!$user) {
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng', TPL_DOMAIN_LANG);
		$httpCode = 401;
		send_response_json(['code' => $httpCode, 'message' => $message], $httpCode, $message);
	}
	$result = behavior_get_by_id( $params['id'] );
	if ( ! empty( $result ) ) {
		$prefix           = $wpdb->get_blog_prefix( get_current_blog_id() );
		$tableKpiBehavior = "{$prefix}behavior";
		$wpdb->update( $tableKpiBehavior, [
			'status' => KPI_STATUS_RESULT,
		], [ 'id' => $params['id'] ] );
		if ( ! empty( $wpdb->last_error ) ) {
			$msg      = "Có lỗi xảy ra khi lưu";
			$httpCode = 400;
			send_response_json( [
				'code'    => $httpCode,
				'message' => $msg,
				'error'   => $wpdb->last_error
			], $httpCode, $msg );
		}
	}else{
		$msg = "Không tìm thấy thái độ hành vi";
		send_response_json(['code' => 404, 'message' => $msg], 404, $msg);
	}
	$msg = "Thành công";
	$httpCode = 201;
	$location = '';
	if( !empty( $params ) && !empty( $params['_wp_http_referer'] ) ){
		$query_params = wp_get_referer();
		$location = site_url($query_params);
	}
	send_response_json(['code' => $httpCode, 'message' => $msg, 'location' => $location], $httpCode, $msg);
}