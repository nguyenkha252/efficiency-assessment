<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 18/01/2018
 * Time: 23:44
 */
function wp_ajax_post_personal_change_status( $params ){
	global $wpdb;
	$user = wp_get_current_user();
	$orgchart = user_load_orgchart( $user );
	$postarr = wp_slash($params);
	if( !$user ){
		$message = __('Vui lòng đăng nhập để sử dụng ứng dụng' ,TPL_DOMAIN_LANG );
		$httpCode = 401;
		send_response_json(['code' => $httpCode, 'message' => $message],$httpCode, $message);
	}
    $nonce = isset($postarr['_wpnonce']) ? $postarr['_wpnonce'] : '';
    if( !wp_verify_nonce($nonce, 'personal_change_status') ) {
        $msg = "Sai mã bảo vệ";
        send_response_json(['code' => 403, 'message' => $msg], 403, $msg);
    }
	if( !in_array($orgchart ? $orgchart->role : '', ['phongban', 'bgd', 'nhanvien'] ) ){
		$message = __('Bạn không có quyền chỉnh sửa' ,TPL_DOMAIN_LANG );
		$httpCode = 403;
		send_response_json(['code' => $httpCode, 'message' => $message],$httpCode, $message);
	}
	$prefix = $wpdb->get_blog_prefix( get_current_blog_id() );
	$tableKpi = "{$prefix}kpis";
	$location = '';
	require_once THEME_DIR . '/inc/lib-users.php';
	require_once THEME_DIR . '/inc/lib-kpis.php';
	if( !empty( $postarr['kpiID'] ) ){
		$dataUpdate = ['aproved' => 1, 'status' => 'publish'];
		if( !empty( $postarr ) && !empty( $postarr['_wp_http_referer'] ) ){
			$query_params = wp_get_referer();
			$location = site_url($query_params);
		}
		$wpdb->query( "START TRANSACTION;" );
		foreach ( $postarr['kpiID'] as $kpiID){
			if( ($orgchart ? $orgchart->role : '') === 'nhanvien' ){
				$result = get_kpi_by_user_orgchart_kpi_id( $user->ID, $orgchart->id, $kpiID );
				if( $result ){
					$wpdb->update( $tableKpi, $dataUpdate, ['id' => $kpiID] );
					if( !empty( $wpdb->last_error ) ){
						$wpdb->query("ROLLBACK;");
					}
				}else{
					$message = __('Không tìm thấy KPI' ,TPL_DOMAIN_LANG );
					$httpCode = 404;
					send_response_json(['code' => $httpCode, 'message' => $message],$httpCode, $message);
				}
			}else if( in_array($orgchart ? $orgchart->role : '', ['bgd', 'phongban']) ){
				#BGD OR PHONGBAN
				$result = kpi_get_kpi_by_id($kpiID);
				if( $result ){
					$wpdb->update( $tableKpi, $dataUpdate, ['id' => $kpiID] );
					if( !empty( $wpdb->last_error ) ){
						$wpdb->query("ROLLBACK;");
					}
				}else{
					$message = __('Không tìm thấy KPI' ,TPL_DOMAIN_LANG );
					$httpCode = 404;
					send_response_json(['code' => $httpCode, 'message' => $message],$httpCode, $message);
				}
			}
		}
		$wpdb->query("COMMIT;");
		$message = __('Lưu thành công', TPL_DOMAIN_LANG);
		$httpCode = 201;
		send_response_json(
			['code' => $httpCode, 'message' => $message, 'location' => $location],
			$httpCode,
			$message
		);
	}else{
		$message = __('Không tìm thấy KPI' ,TPL_DOMAIN_LANG );
		$httpCode = 404;
		send_response_json(
			['code' => $httpCode, 'message' => $message],
			$httpCode,
			$message
		);
	}

}