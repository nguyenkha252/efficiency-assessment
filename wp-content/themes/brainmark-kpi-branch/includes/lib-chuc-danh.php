<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/16/18
 * Time: 23:53
 */

function kpi_get_danh_sach_chuc_danh_by_id($id = '') {
    global $wpdb;
    $blog_id = get_current_blog_id();
    $prefix = $wpdb->get_blog_prefix( $blog_id );
    $tableRoles = "{$prefix}kpi_roles";
    return $wpdb->get_row( $wpdb->prepare("SELECT * FROM {$tableRoles} WHERE ID = %d", $id), ARRAY_A );
}
function kpi_get_cap_bac() {
    return [
        'CEO' => 'Lãnh Đạo',
        'PHONGBAN' => 'Phòng Ban',
        'NHANVIEN' => 'Nhân Viên'
    ];
}
function kpi_get_danh_sach_chuc_danh($chuc_danh = '', $cap_bac = '') {
    global $wpdb;
    $blog_id = get_current_blog_id();
    $prefix = $wpdb->get_blog_prefix( $blog_id );
    $tableRoles = "{$prefix}kpi_roles";
    $where = '';
    if( !empty($cap_bac) ) {
        $compare = '';
        if( preg_match('#\|#', $cap_bac) ) {
            $cap_bac = explode('|', $cap_bac);
            $compare = ' OR ';
        } else if( preg_match('#,#', $cap_bac) ) {
            $cap_bac = explode(',', $cap_bac);
            $compare = ' AND ';
        } else {
            $cap_bac = [$cap_bac];
        }
        $cap_bac = array_map('trim', $cap_bac);

        $items = [];
        foreach ($cap_bac as $cb) {
            if( !empty($cb) ) {
                $items[] = $wpdb->prepare(" (cap_bac LIKE %s)", $cb);
            }
        }
        if( !empty($items) ) {
            $where = " AND (" . implode($compare, $items) . ")";
        }
    }
    if( !empty($chuc_danh) ) {
        $compare = '';
        if( preg_match('#\|#', $chuc_danh) ) {
            $chuc_danh = explode('|', $chuc_danh);
            $compare = ' OR ';
        } else if( preg_match('#,#', $chuc_danh) ) {
            $chuc_danh = explode(',', $chuc_danh);
            $compare = ' AND ';
        } else {
            $chuc_danh = [$chuc_danh];
        }
        $chuc_danh = array_map('trim', $chuc_danh);
        $items = [];
        foreach ($chuc_danh as $item) {
            if( !empty($item) ) {
                $items[] = $wpdb->prepare(" (chuc_danh LIKE %s) ", $item);
            }
        }
        if( !empty($items) ) {
            $where .= " AND (" . implode($compare, $items) . ")";
        }
    }
    $results = $wpdb->get_results( "SELECT * FROM {$tableRoles} WHERE (1 = 1) $where ORDER BY cap_bac ASC, chuc_danh ASC", ARRAY_A );
    if( !empty($results) ) {
        $ints = ['ID'];
        foreach ($results as $k => &$v) {
            foreach ($ints as $key) {
                $results[$k][$key] = intval($v[$key]);
            }
        }
    }
    return $results;
}

