<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 1/13/18
 * Time: 23:51
 */

class KPICayCapBac {

}

class KPICapbac {
    /**
     * @var int
     */
    protected $id;
    /**
     * @var int
     */
    protected $x;
    /**
     * @var int
     */
    protected $y;
    /**
     * @var string
     */
    protected $ten = ''; # Role Name

    /**
     * @var int
     */
    protected $chuc_vu_truong = 0; # Truong: Giam Doc, Tong Giam Doc, Truong Phong

    /**
     * @var array
     */
    protected $chuc_vu_pho = []; # Pho: Pho Giam Doc, Pho Tong Giam Doc, Pho Phong

    /**
     * @var int|null
     */
    protected $parent = null;

    /**
     * @var int
     */
    protected $level = 0;

    /**
     * @var array
     */
    protected $children = [];

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param array $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return array
     */
    public function getChildren()
    {
        return $this->children;
    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @return mixed
     */
    public function getTen()
    {
        return $this->ten;
    }

    /**
     * @return null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return id
     */
    public function getChucVuTruong()
    {
        return $this->chuc_vu_truong;
    }

    /**
     * @return array
     */
    public function getChucVuPho()
    {
        return $this->chuc_vu_pho;
    }

    /**
     * @param int $level
     * @return $this
     */
    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }

    /**
     * @param string $ten
     * @return $this
     */
    public function setTen($ten)
    {
        $this->ten = $ten;
        return $this;
    }

    /**
     * @param int|null $parent
     * @return $this
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @param int $chuc_vu_truong
     * @return $this
     */
    public function setChucVuTruong($chuc_vu_truong)
    {
        $this->chuc_vu_truong = $chuc_vu_truong;
        return $this;
    }

    /**
     * @param array $chuc_vu_pho
     * @return $this
     */
    public function setChucVuPho($chuc_vu_pho)
    {
        $this->chuc_vu_pho = $chuc_vu_pho;
        return $this;
    }

    /**
     * @param int $user_id
     * @return $this
     */
    public function themChucVuPho($user_id) {
        $this->chuc_vu_pho[] = $user_id;
        return $this;
    }

    /**
     * @param int $user_id
     * @return $this
     */
    public function xoaChucVuPho($user_id) {
        $results = [];
        foreach ($this->chuc_vu_pho as $uid) {
            if( $user_id != $uid ) {
                $results[] = $uid;
            }
        }
        $this->chuc_vu_pho = $results;
        return $this;
    }

    /**
     * @param $ten
     * @return bool|WP_User
     */
    public function timChucVuPho($ten) {
        foreach ((array)$this->chuc_vu_pho as $uid) {
            $user = new WP_User($uid);
            if( $user->display_name = $ten ) {
                return $user;
            }
        }
        return false;
    }

    public function toJson($format = 0) {
        $result = json_encode(get_object_vars($this), $format);
        $blog_id = get_current_blog_id();
        $upload = wp_upload_dir();
        $dir = "{$upload['basedir']}/json/users";
        if( !is_dir($dir) ) {
            @mkdir($dir, 0777, true);
        }
        $filename = "{$dir}/{$this->id}.json";
        @file_put_contents($filename, $result);
        return $result;
    }

    /**
     * @param array|string|int $data
     * @return bool
     */
    public function fromJson($data) {
        global $wpdb;
        if( is_numeric($data) ) {
            $filename = "{$data}.json";
            return $this->fromJson($filename);
        } else if( is_string($data) && preg_match('#\.json$#', $data) ) {
            $blog_id = get_current_blog_id();
            $upload = wp_upload_dir();
            $dir = "{$upload['basedir']}/json/users";
            if( !is_dir($dir) ) {
                @mkdir($dir, 0777, true);
            }
            $filename = "{$dir}/{$data}";
            if( file_exists( $filename ) ) {
                $json = file_get_contents($filename);
                $jsonData = json_decode($json, true);
                if( is_array($jsonData) ) {
                    return $this->fromJson($jsonData);
                }
            }
        } else if( is_array($data) ) {
            $this->init($data);
            return true;
        }
        return false;
    }

    /**
     * @param array $data
     */
    public function init($data) {
        $keys = array_keys(get_object_vars($this));
        foreach($data as $key => $val) {
            if( in_array($key, $keys) ) {
                $this->$key = $val;
            }
        }
        return $this;
    }

    /**
     * @return int
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @return int
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param int $x
     */
    public function setX($x)
    {
        $this->x = $x;
        return $this;
    }

    /**
     * @param int $y
     */
    public function setY($y)
    {
        $this->y = $y;
        return $this;
    }
    public function setPosition($x, $y) {
        $this->setX($x);
        $this->setY($y);
        return $this;
    }
}


function kpi_list_charts_tree($parent = -1, $makeTree = true) {
    global $wpdb;
    $blog_id = get_current_blog_id();
    $prefix = $wpdb->get_blog_prefix( $blog_id );
    $tableUsers = "{$prefix}users";
    $tableUserMeta = "{$prefix}usermeta";
    $tableRoles = "{$prefix}kpi_roles";
    $tableCharts = "{$prefix}kpi_charts";
    $where = '';
    if( $parent < 0 ) {
        $where = $wpdb->prepare(" c.parent = %d", $parent);
    }

    $results = $wpdb->get_results( "SELECT r.chuc_danh, r.cap_bac, u.display_name, c.* ".
        "FROM {$tableCharts} as c ".
        "LEFT JOIN {$tableRoles} as r ON r.ID = c.role_id ".
        "LEFT JOIN {$tableUsers} as u ON u.ID = c.user_id " .
        "WHERE (1 = 1) {$where} ORDER BY c.parent, c.level "
    , ARRAY_A);

    $ints = ['ID', 'assign_task', 'user_id', 'role_id', 'parent', 'x', 'y', 'is_primary', 'level'];
    foreach ($results as $k => &$v) {
        foreach ($ints as $key) {
            $results[$k][$key] = intval($v[$key]);
        }
    }
    if( $makeTree && !empty($results) ) {
        return kpi_process_chart_tree($results);
    }
    return $results;
}

function kpi_process_chart_tree($results) {
    $treeItems = [];
    $_nodes = [];
    foreach($results as $k => $item) {
        $_nodes[$item['ID']] = $item;
        unset($results[$k]);
    }
    $results = $_nodes;
    foreach($results as $k => $item) {
        if( $item['is_primary'] ) {
            $siblings = json_decode($item['siblings'], true);
            if( is_null($siblings) ) {
                $siblings = [];
            }
            unset($item['siblings']);
            $item['siblings'] = $siblings;
            $item['children'] = [];
            $treeItems[$item['ID']] = $item;
            unset($results[$k]);
        } else {
            unset($results[$k]['siblings']);
            $results[$k]['siblings'] = [];
        }
    }
    foreach($treeItems as $k => &$node) {
        $siblings = [];
        foreach($node['siblings'] as $id ) {
            if( isset($results[$id]) ) {
                $siblings[$id] = $results[$id];
                unset($results[$id]); # unique user in a node
            }
        }
        $treeItems[$k]['siblings'] = $siblings;
    }
    # echo '<pre>'; var_dump($results); echo '</pre>';
    $tree = loop_items_to_tree($treeItems, $results, 0);
    # echo '<pre style="display: block !important; text-align: left">'; var_dump($results, '----------------', $treeItems, '----------------', $tree); echo '</pre>';
    return $tree;
}

function loop_items_to_tree(&$items, &$results, $parent = 0) {
    $tree = [];
    if( !empty($items) ) {
        foreach($items as $id => $item) {
            if( $item['parent'] == $parent ) {
                unset($items[$id]);
                $item['children'] = loop_items_to_tree($items, $results, $item['ID']);
                if( !empty($results) ) {
                    foreach($results as $k1 => $v) {
                        if( $v['parent'] == $id ) {
                            $item['children'][$v['ID']] = $v;
                            unset($results[$k1]);
                        }
                    }
                }
                $tree[$id] = $item;
            }
        }
        # echo '<pre style="display: block !important; text-align: left">'; var_dump($results, '----------------', $items, '----------------', $tree); echo '</pre>';
    }
    return $tree;
}

