<?php

/*
 *  Load scripts
 */
$kpi_contents_parts = [];
function is_current_page($slug) {
    global $kpi_contents_parts, $wp_the_query; /* @var $wp_the_query WP_Query */
    foreach($kpi_contents_parts as $page) {
        if( $page['key'] == $slug && $page['is_page'] ) {
            return true;
        }
    }
    return $wp_the_query->is_page($slug);
}

add_action('wp_head', function() {
    $version = '1.0.0';
    $min = (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG) ? '' : '.min';

    /*---Theme Styles----*/
    /*-- BEGIN GLOBAL MANDATORY STYLES --*/
    wp_enqueue_style('font-awesome-css', THEME_URL . '/assets/global/plugins/font-awesome/css/font-awesome'.$min.'.css', array(), $version, 'screen');
    wp_enqueue_style('editor-style-css', THEME_URL . '/editor-style.css', array(), $version, 'screen');
    wp_enqueue_style('google-font-css','//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all', array(), $version, 'screen');
    wp_enqueue_style('simple-line-icons-css', THEME_URL . '/assets/global/plugins/simple-line-icons/simple-line-icons'.$min.'.css', array(), $version, 'screen');
    wp_enqueue_style('bootstrap-css', THEME_URL . '/assets/global/plugins/bootstrap/css/bootstrap'.$min.'.css', array(), $version, 'screen');
    if( is_current_page(PAGE_EFFA_SYSTEM) ){
        wp_enqueue_style('bootstrap-editable-css', THEME_URL . '/assets/global/plugins/bootstrap/css/bootstrap-editable.css', array(), $version, 'screen');
    }
    if( is_current_page(PAGE_SETTINGS) ){
        wp_enqueue_style('bootstrap-colorpicker-css', THEME_URL . '/assets/global/plugins/bootstrap-color/css/bootstrap-colorpicker.min.css', array(), $version, 'screen');
    }
    wp_enqueue_style('bootstrap-select-css', THEME_URL . '/assets/global/plugins/bootstrap-selector/css/bootstrap-select'.$min.'.css', array(), $version, 'screen');
    wp_enqueue_style('bootstrap-switch-css', THEME_URL . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch'.$min.'.css', array(), $version, 'screen');

    /*-- END GLOBAL MANDATORY STYLES --*/

    /*-- BEGIN THEME GLOBAL STYLES --*/
    wp_enqueue_style('global-components-css', THEME_URL . '/assets/global/css/components'.$min.'.css', array(), $version, 'screen');
    wp_enqueue_style('global-plugins-css', THEME_URL . '/assets/global/css/plugins'.$min.'.css', array(), $version, 'screen');

    wp_enqueue_style('jquery-datetimepicker-css', THEME_URL . '/assets/global/plugins/jquery.datetimepicker.min.css', array(), $version, 'screen');
    # wp_enqueue_style('global-datetimepicker-css', THEME_URL . '/assets/global/plugins/build/css/bootstrap-datetimepicker'.$min.'.css', array(), $version, 'screen');
    # wp_enqueue_style('global-datetimepicker-standalone-css', THEME_URL . '/assets/global/plugins/build/css/bootstrap-datetimepicker-standalone.css', array(), $version, 'screen');
    /*-- END THEME GLOBAL STYLES --*/
    if ( is_user_logged_in() ) {
        /*-- BEGIN THEME LAYOUT STYLES --*/
        wp_enqueue_style('global-layout2-css', THEME_URL . '/assets/layouts/layout2/css/layout' . $min . '.css', array(), $version, 'screen');
        wp_enqueue_style('global-layout2-theme-blue-css', THEME_URL . '/assets/layouts/layout2/css/themes/grey' . $min . '.css', array(), $version, 'screen');
        wp_enqueue_style('global-layout2-custom-css', THEME_URL . '/assets/layouts/layout2/css/custom.css', array(), $version, 'screen');
        # wp_enqueue_style('global-jquery-ui-css', THEME_URL . '/assets/assets/global/css/jquery-ui.css', array(), $version, 'screen');

        wp_enqueue_style('bootstrap-fileinput-css', THEME_URL . '/assets/global/fileinput/css/fileinput.css');
        # <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>
        wp_enqueue_style('bootstrap-fileinput-css', THEME_URL . '/assets/global/fileinput/themes/explorer-fa/theme.css');

        # var_dump(is_current_page(PAGE_ORGANIZATIONAL_STRUCTURE)); exit;
        if ( is_current_page(PAGE_ORGANIZATIONAL_STRUCTURE) ) {
            /*-- BEGIN PAGE LEVEL STYLES --*/
            # wp_enqueue_style('jquery-orgchart-css', THEME_URL . '/assets/global/plugins/jquery-orgchart/jquery.orgchart'.$orgChart.'.css', array(), $version, 'screen');
            wp_enqueue_style('jquery-orgchart-css', THEME_URL . '/assets/global/plugins/orgchart/css/jquery.orgchart.css', array(), $version, 'screen');
            wp_enqueue_style('listorgchart-css', THEME_URL . '/assets/pages/css/listorgchart' . $min . '.css', array(), $version, 'screen');

            /*-- END PAGE LEVEL STYLES --*/
        }
        if( is_current_page(PAGE_KPI_BANK) ){
            wp_enqueue_style('page-kpi-bank-css', THEME_URL . '/assets/pages/css/kpi-bank.css');
        }
        if( is_current_page(PAGE_EFFA_SYSTEM) ){
            wp_enqueue_style('page-effa-css', THEME_URL . '/assets/pages/css/effa.css');
        }

        if( is_current_page(PAGE_APPLY_KPI_SYSTEM) ){
            wp_enqueue_style('page-apply-kpi-css', THEME_URL . '/assets/pages/css/apply-kpi.css', '1.0');
        }
    }
    if( !is_user_logged_in() ) {
        /*-- BEGIN PAGE LEVEL STYLES --*/
        wp_enqueue_style('login-4-css', THEME_URL . '/assets/pages/css/login-4.css', array(), $version, 'screen');
        /*-- END PAGE LEVEL STYLES --*/
    }
    return;
});

function lik_scripts() {

    $version = '1.0.0';

    $min = (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG) ? '' : '.min';
    // $orgChart = '';
    $orgChart = '-old';

    // Load CSS
    //-- CSS HTML here

    
    // Load JS

    /*-- BEGIN CORE PLUGINS --*/
    wp_enqueue_script('plugins-bootstrap-js', THEME_URL . '/assets/global/plugins/bootstrap/js/bootstrap'.$min.'.js', array('jquery'), $version, true);
    if( is_current_page(PAGE_EFFA_SYSTEM) ){
        wp_enqueue_script('bootstrap-editable', THEME_URL . '/assets/global/plugins/bootstrap/js/bootstrap-editable'.$min.'.js', array('jquery', 'plugins-bootstrap-js'), $version, true);
    }
    # wp_enqueue_script('bootstrap-moment', THEME_URL . '/assets/global/plugins/build/moment.js', array(), $version, true);
    # wp_enqueue_script('bootstrap-moment-vi', THEME_URL . '/assets/global/plugins/build/moment-vi.js', array(), $version, true);
    # wp_enqueue_script('bootstrap-datetimepicker', THEME_URL . '/assets/global/plugins/build/js/bootstrap-datetimepicker'.$min.'.js', array('plugins-bootstrap-js'), $version, true);
    wp_enqueue_script('jquery-mousewheel', THEME_URL . '/assets/global/plugins/jquery.mousewheel.min.js', array('jquery'), $version, true);
    wp_enqueue_script('jquery-datetimepicker', THEME_URL . '/assets/global/plugins/jquery.datetimepicker.full.js', array('jquery'), $version, true);

    # wp_enqueue_script('plugins-cookie-js', THEME_URL . '/assets/global/plugins/js.cookie'.$min.'.js', array('jquery'), $version, true);

    wp_enqueue_script('plugins-slimscroll-js', THEME_URL . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll'.$min.'.js', array('jquery'), $version, true);
    wp_enqueue_script('plugins-blockui-js', THEME_URL . '/assets/global/plugins/jquery.blockui'.$min.'.js', array('jquery'), $version, true);
    wp_enqueue_script('bootstrap-select-js', THEME_URL . '/assets/global/plugins/bootstrap-selector/js/bootstrap-select'.$min.'.js', array('jquery'), $version, true);
    wp_enqueue_script('bootstrap-select-i18n-js', THEME_URL . '/assets/global/plugins/bootstrap-selector/js/defaults-vi_VN'.$min.'.js', array('bootstrap-select-js'), $version, true);
    wp_enqueue_script('plugins-bootstrap-switch-js', THEME_URL . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch'.$min.'.js', array('jquery'), $version, true);
    /*-- END CORE PLUGINS --*/
    
    /*-- BEGIN THEME GLOBAL SCRIPTS --*/
    wp_enqueue_script('global-scripts-app-js', THEME_URL . '/assets/global/scripts/app'.$min.'.js', array('jquery'), $version, true);
    /*-- END THEME GLOBAL SCRIPTS --*/

    wp_enqueue_script('plugins-jquery-validate-js', THEME_URL . '/assets/global/plugins/jquery-validation/js/jquery.validate'.$min.'.js', array('jquery'), $version, true);
    wp_enqueue_script('plugins-additional-methods-js', THEME_URL . '/assets/global/plugins/jquery-validation/js/additional-methods'.$min.'.js', array('jquery'), $version, true);


    if ( !is_user_logged_in() ) {
        /*-- BEGIN PAGE LEVEL SCRIPTS --*/
        wp_enqueue_script('plugins-backstretch-js', THEME_URL . '/assets/global/plugins/backstretch/jquery.backstretch'.$min.'.js', array('jquery'), $version, true);
        wp_enqueue_script('pages-login-js', THEME_URL . '/assets/pages/scripts/login-4.js', array('jquery'), $version, true);
        /*-- END PAGE LEVEL SCRIPTS --*/
     }
    $html2canvas = THEME_URL . '/assets/global/plugins/html2canvas/html2canvas.min.js';
    $jspdf = THEME_URL . '/assets/global/plugins/pdf/jspdf.js';
    /*
    wp_add_inline_script('pdf-html2canvas', "\nvar JS_HTML2CANVAS_URL = '{$html2canvas}',\n\tJS_PDF_URL = '{$jspdf}';\n");
    $wp_scripts = wp_scripts();
    $wp_scripts->enqueue( 'pdf-html2canvas' );
    */

    if ( is_user_logged_in() ) {

        # <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        # wp_enqueue_script('jquery-sortable', THEME_URL . '/assets/global/fileinput/js/plugins/sortable.js');
        wp_enqueue_script('html2canvas', $html2canvas, $version, true);
        wp_enqueue_script('pdf', $jspdf, ['html2canvas'], $version, true);


        wp_enqueue_script('bootstrap-fileinput', THEME_URL . '/assets/global/fileinput/js/fileinput'.$min.'.js');
        wp_enqueue_script('bootstrap-fileinput-lang', THEME_URL . '/assets/global/fileinput/js/locales/vi.js');
        wp_enqueue_script('bootstrap-fileinput-explorer-fa', THEME_URL . '/assets/global/fileinput/themes/explorer-fa/theme'.$min.'.js');
        wp_enqueue_script('bootstrap-fileinput-themes', THEME_URL . '/assets/global/fileinput/themes/fa/theme'.$min.'.js');
        # wp_enqueue_script('jquery-popper', THEME_URL . '/assets/global/fileinput/js/plugins/popper'.$min.'.js');

        wp_enqueue_script('global-knockout-js', THEME_URL . '/assets/global/scripts/knockout-3.4.2.js', array('jquery'), $version, true);
        wp_enqueue_script('jquery.floatThead', THEME_URL . '/assets/global/plugins/floatThead/jquery.floatThead.min.js', array('jquery'), $version, true);
        #wp_enqueue_script('global-jquery-ui-js', THEME_URL . '/assets/global/plugins/jquery-ui.js', array('jquery'), $version, true);

        wp_enqueue_script('chartjs', THEME_URL . '/assets/global/scripts/chart'.$min.'.js', array('jquery'), $version, true);
        wp_enqueue_script('chart.gauge.js', THEME_URL . '/assets/global/plugins/chart-gauge/gauge'.$min.'.js', array('jquery'), $version, true);
        wp_enqueue_script('chart-report', THEME_URL . '/assets/global/scripts/chartReport.js', array('chartjs'), $version, true);

        # wp_enqueue_script('jquery-orgchart-js', THEME_URL . '/assets/pages/scripts/listorgchart'.$min.'.js', array('jquery', 'jquery-form', 'imagesloaded', 'masonry', 'jquery-masonry', 'global-knockout-js'), $version, true);
        # wp_enqueue_script('jquery-orgchart-js', THEME_URL . '/assets/pages/scripts/listorgchart'.$min.'.js', array('jquery', 'jquery-form', 'imagesloaded', 'masonry', 'jquery-masonry', 'global-knockout-js'), $version, true);

        if ( is_current_page(PAGE_ORGANIZATIONAL_STRUCTURE) ) {
            /*-- BEGIN PAGE LEVEL SCRIPTS --*/
            # wp_enqueue_script('jquery-orgchart-js', THEME_URL . '/assets/global/plugins/jquery-orgchart/jquery.orgchart'.$orgChart.'.js', array('jquery'), $version, true);
            # wp_enqueue_script('jquery-form', "/wp-includes/js/jquery/jquery.form{$min}.js", array('jquery'), '3.37.0', true );
            # wp_enqueue_script('jquery-orgchart-js', THEME_URL . '/assets/pages/scripts/listorgchart'.$min.'.js', array('jquery', 'jquery-form', 'imagesloaded', 'masonry', 'jquery-masonry', 'global-knockout-js'), $version, true);
            // wp_enqueue_script('jquery-orgchart-js', THEME_URL . '/assets/global/plugins/jquery-orgchart/jquery.orgchart.js', array('jquery', 'jquery-form', 'imagesloaded', 'masonry', 'jquery-masonry', 'global-knockout-js'), $version, true);
            // wp_enqueue_script('jquery-orgchart-js', THEME_URL . '/assets/global/plugins/orgchart/js/jquery.orgchart.js', array('jquery', 'jquery-form', 'imagesloaded', 'masonry', 'jquery-masonry', 'global-knockout-js'), $version, true);
            wp_enqueue_script('jquery-orgchart-js', THEME_URL . '/assets/global/plugins/orgchart/js/jquery.orgchart1.js', array('jquery', 'jquery-form', 'imagesloaded', 'masonry', 'jquery-masonry', 'global-knockout-js'), $version, true);
            wp_enqueue_script('dragscroll-js', THEME_URL . '/assets/global/plugins/dragscroll.js', ['jquery'], $version, true);
            wp_enqueue_script('my-orgchart-js', THEME_URL . '/assets/pages/scripts/orgchart-vertical.js', array('jquery-orgchart-js', 'dragscroll-js'), $version, true);
            /*-- END PAGE LEVEL SCRIPTS --*/
        }

        /*-- BEGIN THEME LAYOUT SCRIPTS --*/

        wp_enqueue_script('layout2-layout-js', THEME_URL . '/assets/layouts/layout2/scripts/layout'.$min.'.js', array('jquery'), $version, true);
        wp_enqueue_script('layout2-layout-js', THEME_URL . '/assets/layouts/layout2/scripts/layout'.$min.'.js', array('jquery'), $version, true);

        wp_enqueue_script('global-main-js', THEME_URL . '/assets/global/scripts/main.js', array('jquery', 'global-knockout-js'), $version, true);
        if( is_current_page(PAGE_KPI_BANK) ){
            wp_enqueue_script('widget-repeater-row', THEME_URL . '/assets/global/scripts/custom-repeater.js', array('jquery', 'jquery-form', 'imagesloaded', 'masonry', 'jquery-masonry', 'global-knockout-js'), $version, true);
	        if( !empty( $_GET['type'] ) && $_GET['type'] == 'formula' ){
		        wp_enqueue_script('page-kpi-bank-formula', THEME_URL . '/assets/pages/scripts/formula.js', array('jquery', 'jquery-form'), $version, true);
	        }
            wp_enqueue_script('page-kpi-bank', THEME_URL . '/assets/pages/scripts/kpi-bank.js', array('jquery', 'jquery-form', 'imagesloaded', 'masonry', 'jquery-masonry', 'global-knockout-js'), $version, true);
        } else if( is_current_page(PAGE_APPLY_KPI_SYSTEM) ){
            # wp_enqueue_script('bs-multiselect', THEME_URL . '/assets/global/plugins/bootstrap-multiselect.js', array('jquery', 'plugins-bootstrap-js'), $version, true);
            # wp_enqueue_script('bs-select', THEME_URL . '/assets/global/plugins/bootstrap-select'.$min.'.js', array('jquery', 'plugins-bootstrap-js'), $version, true);


            wp_enqueue_script('layout-dashboard', THEME_URL . '/assets/pages/scripts/apply-kpi.js', array('jquery', 'jquery-form'), $version, true);
            wp_enqueue_script('layout-input-kpi', THEME_URL . '/assets/pages/scripts/input-kpi.js', array('layout-dashboard', 'bootstrap-select-i18n-js'), $version, true);
        } else if( is_current_page(PAGE_PROFILE_PATH) || is_current_page( PAGE_USER_MANAGERS ) ) {
            wp_enqueue_script('user-kpi', THEME_URL . '/assets/pages/scripts/user-kpi.js', array('jquery', 'jquery-form'), $version, true);
        }elseif(is_current_page(PAGE_EFFA_SYSTEM)){
            wp_enqueue_script('page-effa-script', THEME_URL . '/assets/pages/scripts/effa-script.js', array('jquery', 'jquery-form', 'imagesloaded', 'masonry', 'jquery-masonry', 'global-knockout-js'), $version, true);
        }

        if( is_current_page(PAGE_DASHBOARD) ){

        }
        if( is_current_page(PAGE_SETTINGS) ){
            if( is_current_page(PAGE_SETTINGS) ){
                wp_enqueue_script('bootstrap-colorpicker-js', THEME_URL . '/assets/global/plugins/bootstrap-color/js/bootstrap-colorpicker.min.js', array('jquery'), $version, 'screen');
            }
            wp_enqueue_script('settings-page', THEME_URL . '/assets/pages/scripts/settings.js', array('jquery', 'jquery-form'), $version, true);
        }

        if( is_current_page(PAGE_NEWS) ){
            wp_enqueue_script('page-news', THEME_URL . '/assets/pages/scripts/news.js', array('jquery', 'jquery-form'), $version, true);
        }

        /*-- END THEME LAYOUT SCRIPTS --*/


    }
}

# add_action('wp_enqueue_scripts', 'lik_scripts');
add_action('wp_head', 'lik_scripts');

show_admin_bar( false );

add_action('init', 'bte_start_session', 1);

function bte_start_session() {
    if(!session_id()) {
        #session_start();
    }
}

function kpi_is_user_as_admin($user = null) {
    $user = $user ? $user : wp_get_current_user();
    $orgchart = user_load_orgchart($user);
    $member_role = $orgchart ? $orgchart->role : '';
    if( $member_role == CHART_ALIAS_BGD || $user->allcaps['level_9'] || $user->has_cap('level_9') ) {
        return true;
    }
    return false;
}

function kpi_get_chart_parents($chart_id, $getThis = false) {
    $charts = kpi_get_list_org_charts($chart_id, $getThis);
    return $charts;
}

function kpi_get_chart_parent_ids($chart_id, $getThis = false) {
    $charts = kpi_get_chart_parents($chart_id, $getThis);
    $chart_ids = [];
    if( !empty($charts) ) {
        foreach ($charts as $item):
            $chart_ids[] = $item['id'];
        endforeach;
    }
    $ids = array_map('intval', $chart_ids);
    return $ids;
}


function kpi_get_orgchart_by_id($id) {
    $charts = kpi_load_org_charts('query');
    if( !empty($charts) && isset($charts[$id]) ) {
        return $charts[$id];
    }
    return null;
}

function kpi_get_orgchart_phongban($id) {
    $charts = kpi_load_org_charts('query');
    $result = isset($charts[$id]) ? $charts[$id] : null;
    if( empty($result) ) {
        return null;
    }
    if( $result['role'] == 'phongban' ) {
        $parent = isset($charts[$result['parent']]) ? $charts[$result['parent']] : null;
        if( $parent['role'] == 'phongban' ) {
            return kpi_get_orgchart_phongban($parent['id']);
        }
        return $result;
    }
    return kpi_get_orgchart_phongban($result['parent']);
}

function kpi_get_orgchart_congty($id) {
    $charts = kpi_load_org_charts('query');
    $result = isset($charts[$id]) ? $charts[$id] : null;
    if( empty($result) ) {
        return null;
    }
    if( $result['alias'] == 'congty' ) {
        $parent = isset($charts[$result['parent']]) ? $charts[$result['parent']] : null;
        if( $parent['alias'] == 'congty' ) {
            return kpi_get_orgchart_phongban($parent['id']);
        }
        return $result;
    }
    return kpi_get_orgchart_phongban($result['parent']);
}

function kpi_get_orgchart_bgd($id) {
    $charts = kpi_load_org_charts('query');
    $result = isset($charts[$id]) ? $charts[$id] : null;
    if( empty($result) ) {
        return null;
    }
    if( $result['role'] == 'bgd' ) {
        $parent = isset($charts[$result['parent']]) ? $charts[$result['parent']] : null;
        if( $parent['role'] == 'bgd' ) {
            return kpi_get_orgchart_phongban($parent['id']);
        }
        return $result;
    }
    return kpi_get_orgchart_phongban($result['parent']);
}

function kpi_get_orgchart_tapdoan($id) {
    $charts = kpi_load_org_charts('query');
    $result = isset($charts[$id]) ? $charts[$id] : null;
    if( empty($result) ) {
        return null;
    }
    if( $result['alias'] == 'tapdoan' ) {
        $parent = isset($charts[$result['parent']]) ? $charts[$result['parent']] : null;
        if( $parent['alias'] == 'tapdoan' ) {
            return kpi_get_orgchart_phongban($parent['id']);
        }
        return $result;
    }
    return kpi_get_orgchart_phongban($result['parent']);
}

function kpi_get_orgchart_root() {
    $charts = kpi_load_org_charts('query');
    if( !empty($charts) ) {
        foreach ($charts as $chart) {
            if( $chart['parent'] === 0 ) {
                return $chart;
            }
        }
    }
    return null;
}

function kpi_find_chart_nodes_of_id($id) {
    $tree = kpi_load_org_charts(true);
    $node = kpi_loop_node_of_node_id($tree, $id);
    $results = [];
    if( !is_null($node) ) {
        $results[$id] = $node;
        if( array_key_exists( 'children', $node ) ){
        	$results = $node['children'];
        }
        #unset($results[$id]['children']);
        #$results = array_merge( $results, kpi_loop_node_has_kpi_of_id($node['children']) );
    }
    return $results;
}

function kpi_loop_node_of_node_id($node, $id) {
    if( $node['id'] == $id ) {
        return $node;
    } else {
        foreach ($node['children'] as $item) {
            $result = kpi_loop_node_of_node_id($item, $id);
            if( !is_null($result) ) {
                return $result;
            }
        }
    }
    return null;
}

function kpi_loop_node_has_kpi_of_id($nodes) {
    $results = [];
    if( !empty($nodes) ) {
        foreach ($nodes as $item) {
            if( !(int)$item['has_kpi'] ) {
                $results[$item['id']] = $item;
                unset($results[$item['id']]['children']);
                $results = array_merge($results, kpi_loop_node_has_kpi_of_id($item['children']) );
            }
        }
    }
    return $results;
}

function kpi_load_org_charts($isTree = true) {
    global $wpdb;
    static $__results = [];

    $key = is_bool($isTree) || is_numeric($isTree) ? ( (bool)$isTree ? '1' : '0') : 'query';

    if( !isset($__results[$key]) ) {
        if( !isset($__results['query']) ) {
            $blog_id = get_current_blog_id();
            $prefix = $wpdb->get_blog_prefix( $blog_id );
            $tableChart = "{$prefix}org_charts";
            $results = $wpdb->get_results("SELECT c.`id`, c.`name`, c.`parent`, c.`parent` as parentId, c.`level`, ".
                "(CASE ".
	            "   WHEN (c.`alias` LIKE 'tapdoan') THEN 'bgd' ".
                "   WHEN (c.`alias` LIKE 'congty') THEN 'bgd' ".
                "   WHEN (c.`alias` LIKE 'phongban') THEN 'phongban' ".
                "   WHEN (c.`alias` LIKE 'nhanvien') THEN 'nhanvien' ".
                "END) as `role`, ".
                "c.`has_kpi`, c.`alias`, c.`room` ".
                "FROM {$tableChart} as c WHERE 1 = 1 ".
                "ORDER BY c.`parent` ASC, c.`id` ASC", ARRAY_A);

            # echo '<pre>'; var_dump(__LINE__, __FILE__, $wpdb->last_error, $wpdb->last_query); echo '</pre>';

            $__results['query'] = [];
            if( !empty($results) ) {
                foreach ($results as $id => $item) {
                    $results[$id]['id'] = (int)$item['id'];
                    $results[$id]['parent'] = (int)$item['parent'];
                    $results[$id]['parentId'] = (int)$item['parentId'];
                    $results[$id]['level'] = (int)$item['level'];
                    $results[$id]['deep'] = (int)$item['level'];
                    $results[$id]['has_kpi'] = (int)$item['has_kpi'];
                    $results[$id]['alias'] = (string)$item['alias'];
                    $results[$id]['alias_text'] = (string)$item['alias'];
                    if( !empty($results[$id]['alias']) && isset($GLOBALS['charts_alias'][$results[$id]['alias']]) ) {
                        $results[$id]['alias_text'] = $GLOBALS['charts_alias'][$results[$id]['alias']];
                    }
                    $__results['query'][$item['id']] = $results[$id];
                }
            }
        }
        $__results['1'] = $__results['0'] = $__results['query'];
        if( !empty($__results['query']) ) {
            if( $isTree ) {
                $__results['1'] = kpi_make_tree($__results['query']);
            }
        }
    }
    return $__results[$key];
}

function kpi_make_tree($results) {
    static $root;
    if( !isset($root) ) {
        $lists = [];
        foreach($results as $item) {
            $item['children'] = [];
            $lists[$item['id']] = $item;
        }

        foreach($lists as $id => $item) {
            if( $item['parentId'] == 0 ) {
                $root = $item;
                unset($lists[$id]);
                break;
            }
        }

        if( !is_null($root) ) {
            foreach($lists as $id => $item) {
                if( $item['parentId'] == $root['id'] ) {
                    $root['children'][] = $item;
                    unset($lists[$id]);
                }
            }
            $root['children'] = kpi_make_tree_node($root['children'], $lists);
            $root['is_leaf'] = empty($root['children']);
        }
        $root = kpi_make_tree_level($root, 0);
    }
    return $root;
}

function kpi_make_tree_level(&$root, $level=0) {
    $root['level'] = $level;
    if( !empty($root['children']) ) {
        foreach ($root['children'] as $id => &$child) {
            $root['children'][$id] = kpi_make_tree_level($child, $level + 1);
        }
    }
    return $root;
}

function kpi_make_tree_node(&$items, &$lists) {
    if( !empty($lists) ) {
        foreach($items as $parentId => &$child) {
            if( !empty($lists) ) {
                foreach($lists as $id => $item) {
                    if( $child['id'] == $item['parentId'] ) {
                        $child['children'][] = $item;
                        unset($lists[$id]);
                    }
                }
            }
            $items[$parentId]['children'] = kpi_make_tree_node($child['children'], $lists);
            $items[$parentId]['is_leaf'] = empty($items[$parentId]['children']);
        }
    }
    return $items;
}

function kpi_loop_tree_to_list_node($items, $child, $number) {
    if( isset($child['children']) ) {
        $children = $child['children'];
        unset($child['children']);
        $items[] = $child;
        foreach($children as $id => $item) {
            $number ++;
            $items = kpi_loop_tree_to_list_node($items, $item, $number);
        }
    } else {
        $items[] = $child;
    }
    return $items;
}

function kpi_loop_get_children($parentId, &$node, $getThis = false) {
    $id = isset($node['id']) ? $node['id'] : '';
    if( $parentId == $id ) {
        $children = [];
        if( array_key_exists('children', $node) ) {
            $children = $node['children'];
        }
        return $getThis ? $node : $children;
    } else {
        if( array_key_exists('children', $node) ) {
            foreach($node['children'] as $sub) {
                $result = kpi_loop_get_children($parentId, $sub, $getThis);
                if( !empty($result) ) {
                    return $result;
                }
            }
        }
    }
    return [];
}

function kpi_loop_chart_to_list($nodes) {
    $items = [];
    if( array_key_exists('children', $nodes) ) {
        $children = $nodes['children'];
        unset($nodes['children']);
        $items[] = $nodes;
        foreach ($children as $id => $node) {
            $items = array_merge($items, kpi_loop_chart_to_list($node));
        }
    } else {
        $items[] = $nodes;
    }
    return $items;
}

function kpi_get_list_org_charts($parentId = 0, $getThis = false) {
    static $results, $wpdb, $tree, $items;

    if( !isset($tree) ) {
        $tree = kpi_load_org_charts(true);
    }
    $__key = md5( serialize( [$parentId, $getThis] ) );

    if( !isset($results[$__key]) ) {
        # $items = kpi_loop_tree_to_list_node($items, $tree, 1);
        $items = kpi_loop_get_children( $parentId, $tree, $getThis );
        $results[$__key] = [];
        # echo '<pre>'; var_dump(__LINE__, __FILE__, $items); echo '</pre>'; exit;

        if( !array_key_exists('children', $items) ) {
            foreach ($items as $id => &$item) {
                if( isset($item['id']) ) {
                    $results[$__key][$item['id']] = $item;
                }
            }
        } else {
            $results[$__key][$items['id']] = $items;
            unset($results[$__key][$items['id']]['children']);
            foreach ($items['children'] as $id => &$item) {
                if( isset($item['id']) ) {
                    $results[$__key][$item['id']] = $item;
                }
            }
        }
         #echo '<pre>'; var_dump(__LINE__, $results); echo '</pre>';
        # echo '<pre>'; var_dump(__LINE__, __FILE__, $parentId, $wpdb->last_error, $wpdb->last_query, $results[$__key], $items); echo '</pre>'; exit;
    }
    return $results[$__key];
}

function kpi_get_years_from_chart_ids($parentId, $chart_ids, $is_chart_key = true)
{
    global $wpdb;
    static $results;
    if (is_array($chart_ids)) {
        $chart_ids = array_map('intval', $chart_ids);
    } else {
        $chart_ids = [intval($chart_ids)];
    }
    $ids = implode(", ", $chart_ids);

    $__key = md5(serialize([$parentId, $ids]));

    if (!isset($results[$__key])) {
        $blog_id = get_current_blog_id();
        $prefix = $wpdb->get_blog_prefix($blog_id);
        $tableYear = "{$prefix}kpi_years";

        $rows = $wpdb->get_results($wpdb->prepare("SELECT * FROM {$tableYear} as y WHERE (y.`parent` = %d) AND (y.`chart_id` IN ({$ids}))", $parentId), ARRAY_A);
        $results[$__key] = [];
        if( $is_chart_key ) {
            foreach ($chart_ids as $id) {
                foreach ($rows as $i => $row) {
                    if( $row['chart_id'] == $id ) {
                        $results[$__key][$id] = $row;
                        unset($rows[$i]);
                    }
                }
            }
        } else {
            foreach ($rows as $row) {
                $results[$__key][$row['id']] = $row;
            }
        }
    }
    return $results[$__key];
}

function kpi_get_first_year_role($firstYear) {
    if( !empty($firstYear) && !isset($firstYear['chuc_danh'])) {
        $user = wp_get_current_user();
        $chuc_danh_con = kpi_get_list_org_charts( $user->orgchart_id, false );
        $chart_ids = [];
        foreach ($chuc_danh_con as $item):
            $chart_ids[] = $item['id'];
        endforeach;
        $childYears = kpi_get_years_from_chart_ids(($firstYear ? $firstYear['id'] : 0), $chart_ids, true);
        $firstYear['chuc_danh'] = $childYears;
    }
    return $firstYear;
}

function kpi_loop_render_option_remove_sub_alias($texts, $chart = [], $field = []) {
    $result = [];
    foreach ($field as $idx => $name) {
        if( $name == 'room' ) {
            if( !preg_match('#^(pho|phó|Pho|Phó)#i', $chart[$name] ) ) {
                $result[] = $texts[$idx];
            }
        }
    }
    return $result;
}


function kpi_init_chart_and_year() {
    global $wpdb;
    $user = wp_get_current_user();
    #process account managers
	if( ( has_cap($user,USER_CAP_MANAGERS) || has_cap( $user, USER_CAP_EMPLOYERS ) ) && !empty( $_REQUEST['uid'] ) && (int) $_REQUEST['uid'] > 0 ){
		$user = get_user_by("ID", $_REQUEST['uid']);
		if( empty($user) && is_wp_error( $user ) ){
			return false;
		}
	}
    $orgchart = user_load_orgchart($user);
    $member_role = $orgchart ? $orgchart->role : '';
    if( empty($member_role) ) {
        $member_role = '';
    } else {
        $member_role = strtolower($member_role);
    }
    $GLOBALS['user'] = $user;
    $GLOBALS['orgchart'] = $orgchart;
    $GLOBALS['member_role'] = $member_role;
    if( ($member_role == 'bgd') || $user->has_cap('level_9') ) {
        if( !isset($_REQUEST['cid']) ) {
            $chart = kpi_get_orgchart_by_id($user->orgchart_id);
            $_GET['cid'] = $_REQUEST['cid'] = !is_null($chart) ? (int)$chart['id'] : 0;
        }
        $orgchart_id = !empty($_GET['cid']) ? (int)$_GET['cid'] : $user->orgchart_id;
    } else {
        $orgchart_id = $_GET['cid'] = $_REQUEST['cid'] = (int)$user->orgchart_id;
    }
    $user_id = $user->ID;
    $user_id = 0;

    $chartRoot = kpi_get_orgchart_root();
    $chartParent = kpi_orgchart_get_chart_parent_of($_GET['cid']);
    $chartCurrent = kpi_get_orgchart_by_id($_GET['cid']);

    $chartCurrentName = (!empty($chartRoot) && ($chartRoot['id'] == $orgchart_id)) ? 'Công Ty' : (!empty($chartCurrent) ? $chartCurrent['name'] : '');
    $chartParentName =  (!empty($chartRoot) && !empty($chartParent) && ($chartRoot['id'] == $chartParent['id']) ) ? 'Công Ty' : (!empty($chartParent) ? $chartParent['name'] : '');
    $isChartRoot = false;
    if( !empty($chartRoot) && !empty($chartCurrent) && ($chartCurrent['id'] == $chartRoot['id']) ) {
        $isChartRoot = true;
    }
    $year_status = ( ($member_role == 'bgd') || $user->has_cap('level_9') ) ? '' : KPI_YEAR_STATUS_PUBLISH;
    define('USER_IS_ADMIN', ( ($member_role == 'bgd') || $user->has_cap('level_9') ) ? true : false);
    # $firstYear = kpi_get_first_year( 0, $year_status, $user->orgchart_id );
    $firstYear = kpi_get_first_year( 0, $year_status, $chartRoot ? $chartRoot['id'] : $user->orgchart_id, TIME_YEAR_VALUE );
    $GLOBALS['chartCurrentName'] = $chartCurrentName;
    $GLOBALS['chartParentName'] = $chartParentName;
    $GLOBALS['chartRoot'] = $chartRoot;
    $GLOBALS['chartParent'] = $chartParent;
    $GLOBALS['chartCurrent'] = $chartCurrent;
    $GLOBALS['isChartRoot'] = $isChartRoot;
    $GLOBALS['parent_year_id'] = (!$isChartRoot && $firstYear) ? $firstYear['id'] : 0;

    $yearInfo = kpi_get_first_year( $GLOBALS['parent_year_id'], $year_status, $_GET['cid'], TIME_YEAR_VALUE );
    # echo '<pre>'; var_dump(__LINE__, __FILE__, $wpdb->last_query, $wpdb->last_error, $GLOBALS['parent_year_id'], $firstYear, $yearInfo); echo '</pre>'; # exit;
    $yearInfo = kpi_get_first_year_role( $yearInfo );

    $yearEmpty = [
        'id' => '', 'year' => '', 'precious' => '', 'month' => '', 'parent' => 0,
        'finance' => '0', 'customer' => '0', 'customer' => '0', 'operate' => '0', 'development' => '0',
        'chart_id' => $_GET['cid'], 'chuc_danh' => [], 'kpi_time' => 'quy', 'kpi_type' => 'congty'
    ];
    if( empty($yearInfo) ) {
        $yearEmpty['year'] = $firstYear['year'];
        $yearEmpty['precious'] = $firstYear['precious'];
        $yearEmpty['month'] = $firstYear['month'];
        $yearEmpty['kpi_time'] = $firstYear['kpi_time'];
        $yearEmpty['kpi_type'] = !empty($firstYear) ? $firstYear['kpi_type'] : 'congty';
        $yearEmpty['parent'] = $firstYear['id'];
    }

    $GLOBALS['yearEmpty'] = $yearInfo ? $yearInfo : $yearEmpty;
    $GLOBALS['firstYear'] = $yearInfo;

    $time_values = [TIME_YEAR_VALUE];
    if( !empty($_GET['quy']) ) {
        $time_values[] = TIME_PRECIOUS_VALUE;
    } else if( !empty($_GET['thang']) ) {
        $time_values[] = TIME_MONTH_VALUE;
    }
    $GLOBALS['time_values'] = $time_values;
    /*
    if( !empty($yearInfo) && !empty($chartRoot) && !empty($chartCurrent) && ($chartRoot['id'] != $chartCurrent['id']) ) {
        # $newYearInfo = kpi_get_first_year( $yearInfo['id'], $year_status );
        echo "<pre>{$wpdb->last_query}\n{$wpdb->last_error}"; var_dump($yearInfo); echo "</pre>";
        $newYearInfo = kpi_get_yearinfo_by($yearInfo['id'], !empty($_GET['cid']) ? $_GET['cid'] : 0, $year_status);
        if( !empty($newYearInfo) ) {
            $yearInfo = $newYearInfo;
        } else {
            $yearInfo['parent'] = $yearInfo['id'];
            $yearInfo['id'] = '';
            $yearInfo['chart_id'] = !empty($_GET['cid']) ? $_GET['cid'] : $yearInfo['chart_id'];
        }
        # echo '<pre>'; var_dump(__LINE__, __FILE__, $yearInfo, $newYearInfo, $wpdb->last_query, $wpdb->last_error); echo '</pre>'; exit;
    } */
    # echo '<pre>'; var_dump(__LINE__, __FILE__, $wpdb->last_query, $wpdb->last_error, $GLOBALS['yearEmpty'], $yearInfo); echo '</pre>'; exit;
    $GLOBALS['yearInfo'] = $yearInfo;

    $exportPage = get_page_by_path( __(PAGE_EXPORTS, TPL_DOMAIN_LANG) );
    $export_url = apply_filters( 'the_permalink', get_permalink( $exportPage ), $exportPage );
    /* $params = ['nam' => $_REQUEST['nam'], 'quy' => $_REQUEST['quy'], 'thang' => $_REQUEST['thang']];
    if( isset($_REQUEST['cid']) ) {
        $params['cid'] = $_REQUEST['cid'];
    }
    if( isset($_REQUEST['type']) ) {
        $params['type'] = $_REQUEST['type'];
    } */
    $export_url = add_query_arg($_GET, $export_url);
    define('PAGE_EXPORTS_URL', $export_url);
    # echo esc_url( $export_url );
}

function orgchart_get_list_orgchart(){
	global $wpdb;
	$blog_id = get_current_blog_id();
	$prefix = $wpdb->get_blog_prefix( $blog_id );
	$tableChart = "{$prefix}org_charts";
	$_key = 'list_orgchart';
	static $_result = [];
	if( !isset($_result[$_key]) ){
		$sql = "SELECT * FROM {$tableChart} AS org ORDER BY org.id ASC";
		$_result[$_key] = $wpdb->get_results( $sql );
	}
	return $_result[$_key];
}

function orgchart_get_list_orgchart_not_nv(){
	global $wpdb;
	$blog_id = get_current_blog_id();
	$prefix = $wpdb->get_blog_prefix( $blog_id );
	$tableChart = "{$prefix}org_charts";
	$_key = 'list_orgchart';
	static $_result = [];
	if( !isset($_result[$_key]) ){
		$sql = "SELECT * FROM {$tableChart} AS org WHERE org.alias != 'nhanvien' ORDER BY org.id ASC";
		$_result[$_key] = $wpdb->get_results( $sql );
	}
	return $_result[$_key];
}

function year_get_year_this_of_congty( $year ){
	global $wpdb;
	$blog_id = get_current_blog_id();
	$prefix = $wpdb->get_blog_prefix( $blog_id );
	$tableYear = "{$prefix}kpi_years";
	$tableChart = "{$prefix}org_charts";
	$_key = md5(serialize(func_get_args()));
	static $_result = [];
	if( !isset($_result[$_key]) ){
		$sql = $wpdb->prepare("SELECT * 
				FROM {$tableYear} AS y 
				WHERE y.year = %d AND y.chart_id IN ( 
							SELECT org.id
							FROM {$tableChart} AS org
							WHERE org.parent = 0 
				 			) 
				AND y.kpi_type = 'congty' AND y.parent = 0 
				AND ( y.month IS NULL OR y.month = 0 ) AND ( y.precious IS NULL OR y.precious = 0 )  
				 			", $year);
		$_result[$_key] = $wpdb->get_row( $sql );
	}
	return $_result[$_key];
}

function orgchart_get_list_orgchart_skip_TGD(){
	global $wpdb;
	$blog_id = get_current_blog_id();
	$prefix = $wpdb->get_blog_prefix( $blog_id );
	$tableChart = "{$prefix}org_charts";
	$_key = 'list_orgchart';
	static $_result = [];
	if( !isset($_result[$_key]) ){
		$sql = "SELECT * FROM {$tableChart} AS org WHERE org.parent != 0 ORDER BY org.id ASC";
		$_result[$_key] = $wpdb->get_results( $sql );
	}
	return $_result[$_key];
}

function custom_get_posts( $chart_id ){
	global $wpdb;
	$_key = md5( serialize( func_get_args() ) );
	static $_result = [];
	if( !isset($_result[$_key]) ){
		$sql = $wpdb->prepare("SELECT p.* FROM {$wpdb->posts} AS p  WHERE 1=1  AND p.post_type = 'post' AND ((p.post_status = 'publish' OR p.post_status = 'draft')) AND p.chart_id = %d  ORDER BY p.post_date ASC ", $chart_id);
		$_result[$_key] = $wpdb->get_results( $sql );
	}
	return $_result[$_key];
}

function render_tree_chart( $listOrgChart ){
	$orgChartsGroup = array_group_by( $listOrgChart, 'room' );
	$optionsGroupHtml = '';
	if( !empty( $orgChartsGroup ) ){
		foreach ( $orgChartsGroup as $key => $chartItem ):
			$options = '';
			foreach ( $chartItem as $k => $item ){
				$options .= "<option value='{$item->id}'>{$item->name}</option>";
			}
			$optionsGroupHtml .= "<optgroup label=\"{$key}\">{$options}</optgroup>";
		endforeach;
	}
	return $optionsGroupHtml;
}
function render_tree_chart_show_room( $listOrgChart ){
	$orgChartsGroup = array_group_by( $listOrgChart, 'room' );
	$optionsGroupHtml = '';
	if( !empty( $orgChartsGroup ) ){
		foreach ( $orgChartsGroup as $key => $chartItem ):
			$options = '';
			foreach ( $chartItem as $k => $item ){
				$options .= "<option value='{$item->id}'>{$item->room}</option>";
			}
			$optionsGroupHtml .= "<optgroup label=\"{$key}\">{$options}</optgroup>";
		endforeach;
	}
	return $optionsGroupHtml;
}

/**
 * @param $_pdf_filename
 * @param $html
 * @param $logo_base64
 * @param int $y
 * @param string $type_output (I: print to screen html | D: download to my computer pdf)
 * @param int $width
 * @param string $height
 */
function save_pdf($_pdf_filename, $html, $logo_base64, $y = 12, $type_output = "D", $width = 50, $height = ''){
	class PDFG extends TCPDF
	{
		//PDF Page header
		public function Header()
		{

		}

		// PDF Page footer
		public function Footer()
		{

		}
	}
	$pdf = new PDFG(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf->Header();
	$pdf->SetFont('dejavuserifcondensed', '', 11, '', true);
	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	$pdf->AddPage('P', 'A4');
	// set JPEG quality
	$pdf->setJPEGQuality(75);
	$imgdata = base64_decode( $logo_base64 );
	$pdf->Image('@'.$imgdata, 12, $y, $width, $height, 'PNG');
	$pdf->writeHTML($html, true, false, true, false, '');
	$pdf->Output($_pdf_filename, $type_output);
}

function render_html_formula( $formula, $type ){
	$formulas = maybe_unserialize( $formula );
	$des_formula = '';
	$measurementFormulas = getMeasurementFormulas();
	$measurementFormulas['!='] = __('KH # KQ', TPL_DOMAIN_LANG);
	$measurementFormulas['0'] = __('----------', TPL_DOMAIN_LANG);
	if( !empty( $formulas ) ) {
		if ( $type == 'on_off' ) {
			foreach ( $formulas as $key => $item ) {
				$condition   = $measurementFormulas[ $item['condition'] ];
				$value       = $item['value'];
				$des_formula .= "<span class='formula-des-item'>{$condition}: {$value} (%)</span>";
			}
		} elseif ( $type == 'oscillate' ) {
			foreach ( $formulas as $key => $item ) {
				$condition   = $measurementFormulas[ $item['condition'] ];
				$oscillate   = !empty($item['oscillate']) ? " + {$item['oscillate']}" : "";
				$value       = $item['value'];
				$des_formula .= "<span class='formula-des-item'>{$condition}{$oscillate}: {$value} (%)</span>";
			}
		} elseif ( $type == 'add_max_kq' ) {
			foreach ( $formulas as $key => $item ) {
				$condition   = $measurementFormulas[ $item['condition'] ];
				$max         = $item['max'];
				$value       = $item['value'];
				$des_formula .= "<span class='formula-des-item'>{$condition}: {$value} (%) (Max KPI  {$max}) (%)</span>";
			}
		} elseif ( $type == 'add_between' ) {
			foreach ( $formulas as $key => $item ) {
				$condition_1 = getConditions( $item['condition_1'] );
				$condition_2 = getConditions( $item['condition_2'] );
				$kqmin       = $item['kq_min'];
				$kqmax       = $item['kq_max'];
				$value       = $item['value'];
				$des_formula .= "<span class='formula-des-item'>{$kqmin} {$condition_1} <strong>KQ</strong> {$condition_2} {$kqmax}: {$value} (%)</span>";
			}
		}
	}
	return $des_formula;
}

function validate_input_before_save_db( $unit, $value, $type = 'plan' ){
	$error = [];
	$dateNow = date("d/m/Y", time());
	$dayNow = date("d", time());
	$monthNow = date("m", time());
	$textType = '';
	if( $type == 'plan' ){
		$textType = __("Kế hoạch", TPL_DOMAIN_LANG);
	}elseif( $type == 'actual_gain' ){
		$textType = __("Kết quả", TPL_DOMAIN_LANG);
	}
	if( !empty( $value ) ) {
		if ( $unit == KPI_UNIT_THOI_GIAN ) {
			$formatDate = explode( "/", $value );
			if ( count( $formatDate ) == 3 ) {
				if ( ! is_numeric( $formatDate[0] ) ) {
					$error['textErrorUnit'] = __( "Ngày phải có giá trị hoặc có thể nhập là: <strong>01/MM/YYYY</strong>", TPL_DOMAIN_LANG );

					return $error;
				}
				if ( ( ! is_numeric( $formatDate[1] ) && $formatDate[1] != 'MM' ) ) {
					$error['textErrorUnit'] = __( "Nếu không có tháng cụ thể có thể nhập: <strong>MM</strong>. Ví dụ: 01/MM/YYYY", TPL_DOMAIN_LANG );

					return $error;
				}
				if ( ( ! is_numeric( $formatDate[2] ) && $formatDate[2] != 'YYYY' ) ) {
					$error['textErrorUnit'] = __( "Nếu không có năm cụ thể có thể nhập: <strong>YYYY</strong> Ví dụ: 01/MM/YYYY", TPL_DOMAIN_LANG );

					return $error;
				}
				if ( ! strtotime( $formatDate[2] . '/' . $formatDate[1] . '/' . $formatDate[0] ) && is_numeric( $formatDate[0] ) && is_numeric( $formatDate[1] ) && is_numeric( $formatDate[2] ) ) {
					$error['textErrorUnit'] = __( "Vui lòng nhập đúng định dạng ngày tháng. Ví dụ: {$dateNow} hoặc {$dayNow}/MM/YYYY hoặc {$dayNow}/$monthNow/YYYY ", TPL_DOMAIN_LANG );

					return $error;
				}
			} else {
				$error['textErrorUnit'] = __( "Vui lòng nhập đúng định dạng ngày tháng. Ví dụ: {$dateNow} hoặc {$dayNow}/MM/YYYY hoặc {$dayNow}/$monthNow/YYYY ", TPL_DOMAIN_LANG );

				return $error;
			}
		} else {
			if ( ! is_numeric( $value ) ) {
				$error['textErrorUnit'] = __( "<strong>{$textType}</strong> có giá trị phải là số", TPL_DOMAIN_LANG );
			}
		}
	}
	return $error;
}

function check_subordinates_chart( $currentOrgID, $chartChildID ){
	# Get all subordinates of this position
	$getAllChartChild = orgchart_get_all_kpi_by_parent($currentOrgID);
	$checkChartChild = [];
	if( !empty($getAllChartChild) ) {
		$checkChartChild = array_filter( $getAllChartChild, function ( $item ) use ( $chartChildID ) {
			return $item['id'] == $chartChildID;
		} );
	}
	return $checkChartChild;
}

function check_user_is_managers(){
	$user = wp_get_current_user();
	if( $user->has_cap('user_managers') || $user->has_cap("user_employers") ){
		return true;
	}
	return false;
}