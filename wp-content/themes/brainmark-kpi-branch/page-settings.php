<?php
/**
 * Template Name: Settings
 * Created by PhpStorm.
 * User: henry
 * Date: 12/27/17
 * Time: 14:29
 */


include_once __DIR__ . '/../brainmark-kpi-main/login.php';

if ( !is_user_logged_in() ) {
    get_header('login');
    get_template_part( 'contents/content', 'login' );
    get_footer('login');
} else {
    get_header();
    # get_template_part( 'admins/content', 'settings' );
    require_once THEME_DIR . '/admins/content-settings.php';
    get_footer();
}