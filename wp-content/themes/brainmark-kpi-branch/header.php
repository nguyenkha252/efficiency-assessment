<?php

include_once __DIR__ . '/../brainmark-kpi-main/login.php';

kpi_init_chart_and_year();

$user = wp_get_current_user();

if ( !is_user_logged_in() ) {
    get_header('login');
    get_template_part( 'contents/content', 'login' );
    get_footer('login');
    exit;
}

require THEME_DIR . '/inc/lib-settings.php';
$options = get_settings_website();

$logo_url = !empty( $options['logo_url'] ) ? $options['logo_url'] : THEME_URL . '/assets/images/logo-brainmark.png';
$background_header = isset($options['background_header']) && !empty($options['background_header']) ? $options['background_header'] : "#008fd5";
$color_header = isset($options['color_header']) && !empty($options['color_header']) ? $options['color_header'] : "#FFF";

$background_header_table = isset($options['background_header_table']) ? $options['background_header_table'] : "#008ED5";
$color_header_table = isset($options['color_header_table']) ? $options['color_header_table'] : "#FFF";

?>
<!DOCTYPE html>
<!--[if IE 8]> <html <?php language_attributes(); ?> class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html <?php language_attributes(); ?> class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html <?php language_attributes(); ?>>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <?php /* <title>Brainmark - KPI System </title> */ ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <link rel="shortcut icon" href="<?php echo THEME_URL; ?>/favicon.png?v=1" />
        <script type="text/javascript">
            var THEME_URL = '<?php echo THEME_URL; ?>';
            var AJAX_URL = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
            var IMAGE_LOADING = {
                message: '<img width="40" src="<?php echo get_template_directory_uri() ?>/assets/images/loading.gif">',
                    css: {border: '1px solid #ccc', padding: 'none', width: '40px', height: '40px'}
            }
        </script>
        <?php wp_head(); ?>
        <link rel='stylesheet' href="<?php echo THEME_URL; ?>/style_customize.css" />
        <link rel='stylesheet' href="<?php echo THEME_URL; ?>/style_bstyle.css" />
        <style type="text/css">
            <?php
            echo "
                .effa-menu-left.navbar-left li.nav-item.active > a,
                .effa-menu-left.navbar-left > li > a:hover{
                    background: none !important;
                    color:{$background_header} !important;
                    font-weight: 600;
                }
                .nav.nav-tabs li a{
                    color: $color_header !important;
                }
                .row-thead-title-parent tr{
                    background: linear-gradient(to right, {$background_header_table} , #eee) !important;

                }
                .row-thead-title-parent th, .row-thead-title-parent th a{
                    color:{$color_header_table} !important;
                }
            ";
            ?>
        </style>
        <script type="text/javascript">
            var AJAX_URL = "<?php echo admin_url('admin-ajax.php'); ?>";
        </script>
    </head>
    <!-- END HEAD -->
    <body <?php
    $class = 'page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid ';
    if( has_cap( $user, USER_CAP_MANAGERS ) ){
        $class .= "role-bgd role-managers";
    }else {
	    $class .= ( ( $GLOBALS['member_role'] == 'bgd' ) || $user->has_cap( 'level_9' ) ) ? 'role-bgd ' : "role-{$GLOBALS['member_role']} ";
    }
    $class .= USER_IS_ADMIN ? 'is-user-admin ' : '';
    body_class($class);
    ?>>
    <?php
    $opacity = "";
    if(is_current_page(PAGE_EFFA_SYSTEM) || is_current_page(PAGE_MANAGE_MEMBERS)){
        $opacity = "opacity: 0;";
        echo "<div class=\"wrapper-lazy-loading-data\">
        <div class=\"wrap-lazy-content\">
            <img style='display: none' src=\"".THEME_URL."/assets/images/img-loading.gif\"/>
        </div>
        <div class=\"wrap-lazy-overlay\"></div>
    </div>";
    }
    ?>
    <div class="wrapper-main-content" style="<?= $opacity; ?>">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="<?php echo esc_attr(site_url()); ?>">
                        <img src="<?php echo $logo_url; ?>" alt="logo" class="logo-default img-responsive" />
                    </a>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN PAGE TOP -->
                <div class="page-top">
                    <div class="pull-left htitle">
                        <h2><?php echo !empty($options['title_website']) ? $options['title_website'] : __('KPI SYSTEM', TPL_DOMAIN_LANG); ?></h2>
                    </div>
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                  <?php
                                  $avatar= "";
                                  if($user->exists()) {
                                    $user_id = $user->ID;
                                    $avatar = get_avatar( $user_id, 128 );
                                  }
                                  $orgchart = user_load_orgchart($user);
                                  # "{$user->first_name} {$user->last_name}"
                                  ?>
                                  <?php echo $avatar; ?>
                                    <span class="username username-hide-on-mobile" data-role="<?php echo $orgchart ? $orgchart->role : ''; ?>"><?php
                                        echo $user->exists() ? $user->display_name : __("User Account", TPL_DOMAIN_LANG);
                                        ?></span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <?php
                                $myAccount = get_page_by_path( PAGE_PROFILE_PATH );
                                $myCalendar = get_page_by_path( PAGE_MY_CAKENDAR_PATH );
                                $myInboxs = get_page_by_path( PAGE_MY_INBOXS_PATH );
                                $myTodo = get_page_by_path( PAGE_MY_TODO_PATH );
                                ?>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <?php
                                    if( user_is_manager() ):
                                        $pageUserManagers = get_page_by_path( PAGE_USER_MANAGERS, 'OBJECT', 'page' );
                                        if( !empty( $pageUserManagers ) && !is_wp_error( $pageUserManagers ) ):


                                        ?>
                                        <li>
                                            <a href="<?php echo get_the_permalink($pageUserManagers->ID); ?>"><i class="fa fa-users"></i><?php echo _ADMIN_ACCOUNT; //_e($pageUserManagers->post_title, TPL_DOMAIN_LANG ); ?></a>
                                        </li>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php
                                        $pageProfile = get_page_by_path( PAGE_PROFILE_PATH, 'OBJECT', 'page' );
                                        if( !empty( $pageProfile ) && !is_wp_error( $pageProfile ) ):
                                    ?>
                                        <li>
                                            <a href="<?php echo get_the_permalink($pageProfile->ID); ?>">
                                                <i class="fa fa-user"></i> <?php echo _PROFILE; ?></a>
                                        </li>
                                        <li>
                                            <a href="<?php echo esc_html(site_url("/")); ?>?action=logout">
                                                <i class="icon-key"></i> <?php echo _LOGOUT; ?></a>
                                        </li>
                                    <?php endif ?>
                                </ul>
                                <?php
                                /*
                                ?>
                                <li>
                                        <a href="<?php the_permalink($myAccount); ?>">
                                            <i class="icon-user"></i> <?php echo get_the_title($myAccount); ?></a>
                            </li>
                            <li>
                                <a href="<?php the_permalink($myCalendar); ?>">
                                    <i class="icon-calendar"></i> <?php echo get_the_title($myCalendar); ?></a>
                            </li>
                            <li>
                                <a href="<?php the_permalink($myInboxs); ?>">
                                    <i class="icon-envelope-open"></i> <?php echo get_the_title($myInboxs); ?>
                                    <span class="badge badge-danger"> 3 </span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php the_permalink($myTodo); ?>">
                                    <i class="icon-rocket"></i> <?php echo get_the_title($myTodo); ?>
                                    <span class="badge badge-success"> 7 </span>
                                </a>
                            </li>
                            <li class="divider"> </li>
                                */
                                ?>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->

                </div>
                <!-- END PAGE TOP -->
            </div>

            <!-- END HEADER INNER -->
            <ul class="nav nav-tabs nav-justified" style="background: <?php echo $background_header; ?>">
                <?php
                $kpi_contents_parts = &$GLOBALS['kpi_contents_parts'];
                foreach ($kpi_contents_parts as $k => $menu): ?>
                    <li role="presentation" class="nav-item <?php echo esc_attr( $menu['active'] ); ?>">
                        <a class="nav-link" href="<?php echo esc_attr( $menu['url'] ); ?>"><i class="icon-<?php echo esc_attr( $menu['icon'] ); ?>"></i>
                            <span class="title"><?php echo $menu['text']; ?></span></a>
                    </li>
                <?php
                endforeach;
                ?>
            </ul>
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <?php
                        /*echo sprintf('<h1 class="page-title">%s</h1>', $kpi_contents_parts[PARAM_PART_IDX]['text']);*/
    ?>
