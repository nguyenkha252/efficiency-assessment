<?php
$user = wp_get_current_user();
$orgchart = user_load_orgchart($user);

$member_role = $orgchart ? $orgchart->role : '';
if( empty($member_role) ) {
    $member_role = '';
} else {
    $member_role = strtolower($member_role);
}
$chartCurrent = $GLOBALS['chartCurrent'];
$yearInfo = isset($GLOBALS['yearInfo']) ? $GLOBALS['yearInfo'] : null;
$orgChartCty = orgchart_get_congty();
$strCty = '';
$arrChartCty = [];
foreach ($orgChartCty as $itemCty){
    $arrChartCty[] = $itemCty['id'];
}
$strCty = implode(", ", $arrChartCty);

/*if( !empty( $yearInfo) && $yearInfo['year'] != TIME_YEAR_VALUE ){
	#$yearInfo = [];
	$yearInfo = year_get_year_by_chart_id( $chartCurrent['id'], TIME_YEAR_VALUE );
}else{
	$yearEmpty = year_get_year_by_chart_id( $chartCurrent['id'], TIME_YEAR_VALUE );
}
$yearInfo = year_get_year_by_chart_id( $chartCurrent['id'], TIME_YEAR_VALUE );*/
$yearDefault = $yearInfo ? $yearInfo : $GLOBALS['yearEmpty'];
# echo '<pre>'; var_dump(__LINE__, __FILE__, $yearDefault); echo '</pre>';
/* if( !empty( $yearDefault ) && ( $yearDefault['year'] != TIME_YEAR_VALUE || $yearDefault['chart_id'] != $chartCurrent['id']) ){
    $yearDefault = year_get_year_by_year( TIME_YEAR_VALUE );
	$yearDefault['parent'] = $yearDefault['id'];
    $yearDefault['id'] = '';
} */


$yearTabs = [
    'finance' => [
        'title' => __('Finance', TPL_DOMAIN_LANG),
        'type' => 'Finance',
    ],
    'customer' => [
        'title' => __('Customer', TPL_DOMAIN_LANG),
        'type' => 'Customer',
    ],
    'operate' => [
        'title' => __('Operate', TPL_DOMAIN_LANG),
        'type' => 'Operate',
    ],
    'development' => [
        'title' => __('Development', TPL_DOMAIN_LANG),
        'type' => 'Development',
    ],
];


$year_status = ( ($member_role == 'bgd') || $user->has_cap('level_9') ) ? '' : KPI_YEAR_STATUS_PUBLISH;
$year_list = kpi_get_year_list(0, $year_status );
# $firstYear = kpi_get_first_year_role( kpi_get_first_year( 0, $year_status ) );

# $chuc_danh_con = kpi_get_list_org_charts( $user->orgchart_id, false );
# echo '<pre>'; var_dump(__LINE__, __FILE__, $yearDefault); echo '</pre>';

?>
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<script type="application/javascript">
    var GETS = {type: "<?php isset( $_GET['type'] ) ? esc_attr_e($_GET['type']) : 'ket-qua-kpi'; ?>"};
</script>
    <div class="page-footer">
        <div class="footer-content">
            <div class="footer-left col-md-6">
                <div class="footer-item footer-logo">
                    <a href="<?php echo esc_attr(site_url()); ?>">
                        <img src="<?php echo THEME_URL; ?>/assets/images/logo-brainmark.png" alt="logo" class="logo-default img-responsive" />
                    </a>
                </div>

            </div>
            <div class="footer-right col-md-6">
                <div class="footer-item footer-email">
                    <a href="mailto:consulting@brainmark.vn"> <i class="fa fa-envelope" aria-hidden="true"></i> <span>consulting@brainmark.vn</span> </a>
                </div>
                <div class="footer-item footer-phone">
                    <a href="tel:0909363363"> <i class="fa fa-phone" aria-hidden="true"></i> <span>0909 363 363</span> </a>
                </div>
            </div>
        </div>

        <!-- END FOOTER -->
    </div>

<?php
if( is_current_page(PAGE_APPLY_KPI_SYSTEM) && ( ($member_role == 'bgd') || $user->has_cap('level_9') ) ):
    $chartRoot = $GLOBALS['chartRoot'];
    $chartCurrent = $GLOBALS['chartCurrent'];
    $isChartRoot = $GLOBALS['isChartRoot'];
    $taoKPITitle = $isChartRoot ? 'Tạo KPI Công Ty' : 'Tạo KPI cho '. ($chartCurrent ? $chartCurrent['name'] : '');
    # echo '<pre>'; var_dump(__LINE__, __FILE__, $yearDefault, $yearInfo); echo '</pre>';
    $_GET['cid'] = !empty($_GET['cid']) ? $_GET['cid'] : ($chartCurrent ? $chartCurrent['id'] : 0);
    ?>
    <div id="tpl-kpi-year" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
        <form class="modal-dialog modal-lg" enctype="application/x-www-form-urlencoded" method="post"
              action="<?php echo admin_url('admin-ajax.php?action=kpi_year_add&cid='.$_GET['cid']); ?>"
              data-add-url="<?php echo admin_url('admin-ajax.php?action=kpi_year_add&cid='.$_GET['cid']); ?>"
              data-update-url="<?php echo admin_url('admin-ajax.php?action=kpi_year_update&cid='.$_GET['cid']); ?>"
              data-tab-info="<?php esc_json_attr_e($yearTabs); ?>">

            <input type="hidden" name="parent" value="<?php echo $yearDefault ? $yearDefault['parent'] : '0'; ?>">
            <input type="hidden" name="year_id" value="<?php echo ($yearDefault ? $yearDefault['id'] : ''); ?>" data-year-info="<?php esc_json_attr_e($yearDefault); ?>">
            <input type="hidden" name="kpi_type" value="<?php echo $yearDefault ? $yearDefault['kpi_type'] : 'congty'; ?>">
            <input type="hidden" name="chart_id" value="<?php echo ($chartCurrent ? $chartCurrent['id'] : ''); ?>">
            <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce('save_kpi_year'); ?>">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title"
                        data-add-title="<?php _e($taoKPITitle, TPL_DOMAIN_LANG); ?>"
                        data-update-title="<?php echo sprintf( __('Chỉnh sửa KPI của %s cho năm %s', TPL_DOMAIN_LANG),
                            ($isChartRoot ? 'Công Ty' : ($chartCurrent ? $chartCurrent['name'] : '')),
                            ( ($yearDefault && !empty($yearDefault['year'])) ? $yearDefault['year'] : '') ); ?>"
                    ><?php _e($taoKPITitle, TPL_DOMAIN_LANG); ?></h4>
                </div>
                <div class="modal-body clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="container-fluid">
                            <div class="col-md-4 selectpicker-nam">
                                <div class="input-group">
                                    <label class="input-group-addon" for="select-for-nam1" id="select-for-nam-id1"><?php _e('Năm', TPL_DOMAIN_LANG); ?></label>
                                    <select required class="selectpicker form-control select-for-nam" id="select-for-nam1" name="nam" aria-describedby="select-for-nam-id1">
                                        <option value="">-- Năm --</option>

                                        <?php
                                        $year = intval(date('Y', time()));
                                        $years = [$year - 1, $year + 3];
                                        if( !$isChartRoot ) {
                                            #$years = [$year, $year + 1];
                                            $years = [(int)$_GET['nam'], (int)$_GET['nam'] + 1];
                                        }
                                        for($y = $years[0]; $y < $years[1]; $y++ ) {
                                            $selected = ($y == $_GET['nam']) ? 'selected="selected"' : '';
                                            echo sprintf('<option value="%d" %s>Năm %d</option>', $y, $selected, $y);
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 selectpicker-type">
                                <div class="input-group">
                                    <label class="input-group-addon" for="select-for-quy1" id="select-for-quy-id1"><?php _e('Loại KPI', TPL_DOMAIN_LANG); ?></label>
                                    <select class="selectpicker form-control select-for-quy" id="select-for-quy1" required name="kpi_time" aria-describedby="select-for-quy-id1">
                                        <option value="0">-- Loại --</option>
                                        <?php

                                        foreach (['quy' => 'Quý', 'thang' => 'Tháng'] as $val => $text):
                                            $selected = (isset($yearDefault['kpi_time']) && ($yearDefault['kpi_time'] == $val) ) ? 'selected="selected"' : '';
                                            echo sprintf('<option value="%s" %s>%s</option>', $val, $selected, $text);
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <table class="table" >
                            <colgroup style="width: 28%"></colgroup>
                            <colgroup style="width: 18%"></colgroup>
                            <colgroup style="width: 18%"></colgroup>
                            <colgroup style="width: 18%"></colgroup>
                            <colgroup style="width: 18%"></colgroup>
                            <thead>
                                <tr>
                                    <th class="chuc-danh"><?php echo __('Role', TPL_DOMAIN_LANG); ?></th>
                                    <th class="finance"><?php echo __('Finance', TPL_DOMAIN_LANG); ?> (%)</th>
                                    <th class="customer"><?php echo __('Customer', TPL_DOMAIN_LANG); ?> (%)</th>
                                    <th class="operate"><?php echo __('Operate', TPL_DOMAIN_LANG); ?> (%)</th>
                                    <th class="development"><?php echo __('Development', TPL_DOMAIN_LANG); ?> (%)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $chartCurrent = isset($GLOBALS['chartCurrent']) ? $GLOBALS['chartCurrent'] : null;
                                $can_edit = ( ($member_role == 'bgd') || $user->has_cap('level_9') ) ? '' : 'disabled="disabled"'; ?>
                                <tr class="chuc-danh-hien-tai" data-id="<?php echo ($yearDefault ? $yearDefault['id'] : ''); ?>">
                                    <td class="chuc-danh"><span><?php echo $chartCurrent ? $chartCurrent['room'] : ''; ?></span></td>
                                    <?php
                                    $row = $chartCurrent ? $chartCurrent['id'] : '';
                                    foreach($yearTabs as $key => $data):
                                        ?><td class="<?php echo "{$member_role} {$key}"; ?>">
                                       <input class="form-control" <?php echo $can_edit;?> type="number" required
                                              placeholder="<?php echo esc_attr($data['title']); ?>"
                                              id="<?php echo "{$key}_{$row}"; ?>_id" name="<?php
                                            echo "{$key}"; ?>" value="<?php echo (($yearDefault && !empty($yearDefault[$key])) ? $yearDefault[$key] : ''); ?>">
                                        </td>
                                    <?php endforeach; ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-lg-12 col-md-12 col-sm-12 hidden response-container"></div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <button type="submit" class="btn btn-primary" data-add-text="Tạo" data-update-text="Lưu">Lưu</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
<?php endif;

?>

    <div id="confirm-popup" class="confirm-modal modal fade" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title"><?php _e('Xác nhận', TPL_DOMAIN_LANG); ?></h4>
            </div>
            <div class="modal-body">
                <h3 class="message" data-message=""></h3>
                <div class="response-message hidden"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"><?php _e('OK', TPL_DOMAIN_LANG); ?></button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', TPL_DOMAIN_LANG); ?></button>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
<?php
$units = getUnit();
?><script type="text/javascript">var UNIT_TEXTS = <?php echo json_encode($units); ?>;</script>
<script type="text/javascript">var LOADED = true;</script>
<?php
wp_footer(); ?>
</body>
</html>
<?php

