<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 9/27/18
 * Time: 11:32 PM
 */

$logo_url = !empty( $options['logo_url'] ) ? $options['logo_url'] : THEME_URL . '/assets/images/logo-brainmark.png';
$bloginfo = get_bloginfo();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <!-- This is a simple example template that you can edit to create your own custom templates -->
    <!--[if gte mso 15]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG />
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Email Tạo mới nhân viên</title>
</head>
<body>
<center>
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" id="bodyTable" style="height:100%;">
        <tr>
            <td align="center" valign="top" id="bodyCell">
                <!-- BEGIN TEMPLATE // -->
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                    <tr>
                        <td align="center" valign="top" width="600" style="width:600px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                <tr>
                                    <td colspan="2" style="background: #FFF;padding: 20px 0;">
                                        <a style="display:block;width:200px;" href="<?php echo site_url(); ?>" target="_blank"><img src="<?php echo $logo_url; ?>" style="display: block;" alt="<?php echo $bloginfo; ?>"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <p>Xin chào <strong><?php echo $full_name; ?></strong>,</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <h3>Bạn vừa được tạo tài khoản tại <?php echo $bloginfo; ?></h3>
                                        <h4>Thông tin tài khoản dùng để đăng nhập tại hệ thống là: </h4>
                                        <p><strong>Username: </strong><span><?php echo $username; ?></span></p>
                                        <p><strong>Password: </strong><span><?php echo $password; ?></span></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <p>Vui lòng click vào link bên dưới để đăng nhập vào hệ thống</p>
                                        <p><a style="background: #378ece;color: #FFF;padding: 10px 30px;font-size: 1.25rem;text-decoration: none;text-transform: uppercase;display: inline-block;padding-top: 10px;" href="<?php echo site_url(); ?>">Đăng nhập</a></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <p>Best,</p>
                                        <p><?php echo $bloginfo; ?> team</p>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</center>
</body>
</html>
