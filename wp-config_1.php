<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'effa');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0E(MG`&L:qFZKxQ3F!nZy0n}mi12d(j0qqb&Ib}f<HK[1@mhL-7oBVjMjOKyQQxx');
define('SECURE_AUTH_KEY',  '9/?y]ripCVfg6MzF;o-e5wfKvAYd@qsj/.j(.-DJA4~*$vqCH74_/<rwQ=`)$o65');
define('LOGGED_IN_KEY',    'niD6%vx H!DoR9u>K{8Ld>/Csr6M3 R&y <#|>V{N8G@a>#VxOqN^#~z^EY]M/v)');
define('NONCE_KEY',        'S~rlc0lO`*5%PIlr<1w0Bb 0%n)o_ngg;2m|H;g)Zt3RKdg;=tl>Z4=iJs&oNtxt');
define('AUTH_SALT',        ';bC6|W0~) THO?i+GRPoE!3or9(MRu.TCuhFwI42Y6f}*F[UiBv`/1 n6<WyMXGg');
define('SECURE_AUTH_SALT', '&*4^AV0d3=r~5/Z@vd0#Z(/6}D.7uYf97F#mW1id[UACeq(:_<Uo[2C61}nkwTyb');
define('LOGGED_IN_SALT',   'b>BJg`IR)ok79)^u{M4gO;:={D-K}ek,|EK=8UAq?wn1D3JNat^{P*cpZGYuWsw2');
define('NONCE_SALT',       ')8L_|_ffK; cR7o .ou#IvEqJJhFYlM)0=|v_{9PFFPMC!{%I3p^Hii;#rs5ntGi');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'effa_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */
/* Multisite */
/*define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', true);
define('DOMAIN_CURRENT_SITE', 'efficiency.com');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);*/

#define('WPLANG', 'vi');
define('SCRIPT_DEBUG', true);

#define( 'SUNRISE', 'on' );
#define( 'DOMAIN_MAPPING', 'on' );

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

define( 'WP_DEFAULT_THEME', 'effa-branch' );
/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
