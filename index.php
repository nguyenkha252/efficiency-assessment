<?php
/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress
 */

/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */
if( preg_match('#\.(jpg|jpeg|gif|png|ico)$#', $_SERVER['REQUEST_URI'], $match) ) {
    $mimetype = 'image/' . $match[1];
    $file = __DIR__ . $_SERVER['REQUEST_URI'];
    @header( 'Content-Type: ' . $mimetype ); // always send this
    @header( 'Content-Length: ' . file_exists($file) ? filesize( $file ) : 0 );
    @header( 'X-Sendfile: ' . $file );
    $last_modified = gmdate( 'D, d M Y H:i:s', file_exists($file) ? filemtime( $file ) : time() );
    $etag = '"' . md5( $last_modified ) . '"';
    @header( "Last-Modified: $last_modified GMT" );
    @header( 'ETag: ' . $etag );
    @header( 'Expires: ' . gmdate( 'D, d M Y H:i:s', time() + 100000000 ) . ' GMT' );
    // Support for Conditional GET - use stripslashes to avoid formatting.php dependency
    $client_etag = isset( $_SERVER['HTTP_IF_NONE_MATCH'] ) ? stripslashes( $_SERVER['HTTP_IF_NONE_MATCH'] ) : false;

    if ( ! isset( $_SERVER['HTTP_IF_MODIFIED_SINCE'] ) )
        $_SERVER['HTTP_IF_MODIFIED_SINCE'] = false;
    $client_last_modified = trim( $_SERVER['HTTP_IF_MODIFIED_SINCE'] );
    // If string is empty, return 0. If not, attempt to parse into a timestamp
    $client_modified_timestamp = $client_last_modified ? strtotime( $client_last_modified ) : 0;

    // Make a timestamp for our most recent modification...
    $modified_timestamp = strtotime($last_modified);

    if ( ( $client_last_modified && $client_etag )
        ? ( ( $client_modified_timestamp >= $modified_timestamp) && ( $client_etag == $etag ) )
        : ( ( $client_modified_timestamp >= $modified_timestamp) || ( $client_etag == $etag ) )
    ) {
        status_header( 304 );
        exit;
    }

    // If we made it this far, just serve the file
    file_exists($file) ? readfile( $file ) : '';
    flush();
    exit;
}
define('WP_USE_THEMES', true);

/** Loads the WordPress Environment and Template */
require( dirname( __FILE__ ) . '/wp-blog-header.php' );
